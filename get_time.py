from geopy.geocoders import Nominatim
from datetime import datetime
import pytz
from tzwhere import tzwhere

geolocator = Nominatim()
location = geolocator.geocode("Minnesota")


tzwhere = tzwhere.tzwhere()
timezone_str = tzwhere.tzNameAt(location.latitude, location.longitude)

timezone = pytz.timezone(timezone_str)
dt = datetime.now(timezone)
print(dt.hour,":",dt.minute)
