import subprocess

container_proc = subprocess.run(
    ['docker', 'system', "prune", "--yes"],
    shell=True,
    stderr=subprocess.STDOUT,
    stdout=subprocess.PIPE
)