#!/bin/bash
# This script is used to deploy updated code to a specified Lambda function. It is meant to be called from the root
# package directory, e.g. "scripts/update_lambda_function" and must be run on a machine where pip will work.
#
# The script works by first creating a virtual environment with required dependencies, using pip. It will then create a
# deployment package consisting of everything in the lib and configuration folders. It then copies the libraries in the
# virtual environment to the root of the deployment package.
# After creating this zipped deployment package, the function code for the specified lambda function is called.

set -e

LAMBDA_FUNCTION_NAME=$1
echo "Lambda function with name '$LAMBDA_FUNCTION_NAME' will be updated."

COBOT_DIR=$2
echo 'Cobot lambda directory' $2

CURRENT_DIR=`pwd`
echo 'Current directory: ' $CURRENT_DIR
shift;shift

STAGE="BETA"

while getopts ":s:" opt; do
    case "${opt}" in
        s) STAGE=$OPTARG;;
    esac
done
echo $STAGE

echo "Installing library using pip..."
rm -rf lib
echo -e "[install]\nprefix=" >> setup.cfg
pip3 install -t 'lib' -r $COBOT_DIR/lambda_requirements.txt

echo "Creating development package..."
rm -f lambda_package.zip

head -$(($(wc -l < setup.cfg) - 2)) setup.cfg > setup.temp
cat setup.temp > setup.cfg && rm setup.temp

zip -r9 lambda_package.zip cobot_core cobot_python_sdk cobot_common miniskills -x \*__pycache__\* \*cobot_common/docker\* \*cobot_common/data_processing\*

cd lib/
zip -r9 ../lambda_package.zip *
cd ..

cd $COBOT_DIR
zip -r9 $CURRENT_DIR/lambda_package.zip * -x \*docker\*
cd $CURRENT_DIR

echo "Updating lambda function..."
if [ "$STAGE" == "BETA" ]; then
    echo 'update beta lambda function'
    aws lambda update-function-code --function-name $LAMBDA_FUNCTION_NAME --zip-file fileb://lambda_package.zip
elif [ "$STAGE" == "PROD" ]; then
    echo 'update prod lambda function'
    VERSION=$(aws lambda update-function-code --function-name $LAMBDA_FUNCTION_NAME --zip-file fileb://lambda_package.zip --publish | jq -r '.Version')
    echo $VERSION
    aws lambda update-alias --function-name $LAMBDA_FUNCTION_NAME --name Prod --function-version $VERSION
else
    echo '-s has BETA or PROD'
fi
echo "Cleaning up deployment package"
rm lambda_package.zip
rm -rf lib
echo "Done!"
