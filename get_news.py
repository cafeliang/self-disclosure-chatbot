import redis
from itertools import zip_longest
from fuzzywuzzy import process
from datetime import datetime, timedelta
import logging
import signal

class RNews_Client:
    PERSON_KEY = "person"
    LOCATION_KEY = "loc"
    ORGANIZATION_KEY = "org"
    THEME_KEY = "theme"

    def __init__(self, addr='172.31.19.101', port='16517'):
        logging.getLogger().setLevel(logging.INFO)
        logging.info("[RNews] initialize...")
        self.now = datetime.now()
        self.rs = redis.StrictRedis(
            host=addr, port=port, db=0,
            password="alexaprize", decode_responses=True)

    def get_sources_with_timeout(self, batch_iter=10, timeout=2):
        try:
            with Timeout(timeout):
                return self.get_sources(batch_iter)
        except Timeout.Timeout:
            return None
        # TODO: handle corner case
        return None

    def get_sources(self, batch_iter=10):
        ret = []
        for keys in self._batcher(
            self.rs.scan_iter(self._gen_prim_key("*")), batch_iter):
            ret.extend(
                [key.split(":")[3] for key in keys if key is not None])
        return ret

    def get_person_with_timeout(
        self, category, input_name, limit=2, cutoff=80, timeout=2):
        return self.get_scheme_with_timeout(
            self.PERSON_KEY, category, input_name, limit, cutoff, timeout)

    def get_person(self, category, input_name, limit=2, cutoff=80):
        return self.get_scheme(self.PERSON_KEY, category, input_name, limit, cutoff)

    def get_pop_person_with_timeout(
        self, category, top=3, offset=0, timeout=2):
        return self.get_pop_scheme_with_timeout(
            self, self.PERSON_KEY, category, top, offset, timeout)

    def get_pop_person(self, category, top=3, offset=0):
        return self.get_pop_scheme(self, self.PERSON_KEY, category, top, offset)

    def get_person_news_with_timeout(
        self, category, input_name, limit=2, cutoff=80, days=7, timeout=2):
        return self.get_scheme_news_with_timeout(
            self.PERSON_KEY, category, input_name, limit, cutoff, days, timeout)

    def get_person_news(self, category, input_name, limit=2, cutoff=80, days=7):
        return self.get_scheme_news(
            self.PERSON_KEY, category, input_name, limit, cutoff, days)

    def get_location_with_timeout(
        self, category, input_name, limit=2, cutoff=80, timeout=2):
        return self.get_scheme_with_timeout(
            self.LOCATION_KEY, category, input_name, limit, cutoff, timeout)

    def get_location(self, category, input_name, limit=2, cutoff=80):
        return self.get_scheme(self.LOCATION_KEY, category, input_name, limit, cutoff)

    def get_pop_location_with_timeout(
        self, category, top=3, offset=0, timeout=2):
        return self.get_pop_scheme_with_timeout(
            self, self.LOCATION_KEY, category, top, offset, timeout)

    def get_pop_location(self, category, top=3, offset=0):
        return self.get_pop_scheme(self, self.LOCATION_KEY, category, top, offset)

    def get_location_news_with_timeout(
        self, category, input_name, limit=2, cutoff=80, days=7, timeout=2):
        return self.get_scheme_news_with_timeout(
            self.LOCATION_KEY, category, input_name, limit, cutoff, days, timeout)

    def get_location_news(self, category, input_name, limit=2, cutoff=80, days=7):
        return self.get_scheme_news(
            self.LOCATION_KEY, category, input_name, limit, cutoff, days)

    def get_org_with_timeout(
        self, category, input_name, limit=2, cutoff=80, timeout=2):
        return self.get_scheme_with_timeout(
            self.ORGANIZATION_KEY, category, input_name, limit, cutoff, timeout)

    def get_org(self, category, input_name, limit=2, cutoff=80):
        return self.get_scheme(self.ORGANIZATION_KEY, category, input_name, limit, cutoff)

    def get_pop_org_with_timeout(
        self, category, top=3, offset=0, timeout=2):
        return self.get_pop_scheme_with_timeout(
            self, self.ORGANIZATION_KEY, category, top, offset, timeout)

    def get_pop_org(self, category, top=3, offset=0):
        return self.get_pop_scheme(self, self.ORGANIZATION_KEY, category, top, offset)

    def get_org_news_with_timeout(
        self, category, input_name, limit=2, cutoff=80, days=7, timeout=2):
        return self.get_scheme_news_with_timeout(
            self.ORGANIZATION_KEY, category, input_name, limit, cutoff, days, timeout)

    def get_org_news(self, category, input_name, limit=2, cutoff=80, days=7):
        return self.get_scheme_news(
            self.ORGANIZATION_KEY, category, input_name, limit, cutoff, days)

    def get_theme_with_timeout(
        self, category, input_name, limit=2, cutoff=80, timeout=2):
        return self.get_scheme_with_timeout(
            self.THEME_KEY, category, input_name, limit, cutoff, timeout)

    def get_theme(self, category, input_name, limit=2, cutoff=80):
        return self.get_scheme(self.THEME_KEY, category, input_name, limit, cutoff)

    def get_pop_theme_with_timeout(
        self, category, top=3, offset=0, timeout=2):
        return self.get_pop_scheme_with_timeout(
            self, self.THEME_KEY, category, top, offset, timeout)

    def get_pop_theme(self, category, top=3, offset=0):
        return self.get_pop_scheme(self, self.THEME_KEY, category, top, offset)

    def get_theme_news_with_timeout(
        self, category, input_name, limit=2, cutoff=80, days=7, timeout=2):
        return self.get_scheme_news_with_timeout(
            self.THEME_KEY, category, input_name, limit, cutoff, days, timeout)

    def get_theme_news(self, category, input_name, limit=2, cutoff=80, days=7):
        return self.get_scheme_news(
            self.THEME_KEY, category, input_name, limit, cutoff, days)

    def get_scheme(
        self, scheme, category, input_name, limit=2, cutoff=80, iter_batch=100):
        logging.info(
            "[RNews] get scheme: {}, cat: {}, input: {}, limit: {}, cutoff: {}, batch: {}".format(
            scheme, category,input_name, limit, cutoff, iter_batch))
        all_name = []
        # FIXME: bottleneck
        for keys in self._batcher(self.rs.scan_iter(
            self._gen_subcata_key(scheme, category, "*")), iter_batch):
            all_name.extend(
                [key.split(":")[4] for key in keys if key is not None])
        return process.extractBests(
            input_name, all_name, limit=limit, score_cutoff=cutoff)

    def get_scheme_with_timeout(
        self, scheme, category, input_name, limit=2,
        cutoff=80, iter_batch=100, timeout=2):
        try:
            with Timeout(timeout):
                return self.get_scheme(
                    scheme, category, input_name, limit, cutoff)
        except Timeout.Timeout:
            return None
        # TODO: handle corner case
        return None

    def get_scheme_keys_with_timeout(self, timeout=2):
        try:
            with Timeout(timeout):
                return self.get_scheme_keys()
        except Timeout.Timeout:
            return None
        # TODO: handle corner case
        return None

    def get_scheme_keys(self):
        return self.rs.hkeys(self._gen_scheme_key())

    def get_pop_scheme_with_timeout(
        self, scheme, category, top=3, offset=0, timeout=2):
        try:
            with Timeout(timeout):
                return self.get_pop_scheme(
                    scheme, category, top=top, offset=offset)
        except Timeout.Timeout:
            return None
        # TODO: handle corner case
        return None

    def get_pop_scheme(self, scheme, category, top=3, offset=0):
        return self.rs.zrevrangebyscore(
            self._gen_cnt_key(scheme, category), '+inf', '-inf', start=offset, num=top)

    def get_scheme_news_with_timeout(
        self, scheme, category, input_name,
        limit=2, cutoff=80, days=7, timeout=2):
        try:
            with Timeout(timeout):
                return self.get_scheme_news(
                    scheme, category, input_name, limit=limit,
                    cutoff=cutoff, days=days)
        except Timeout.Timeout:
            return None
        # TODO: handle corner case
        return None

    def get_scheme_news(self, scheme, category, input_name, limit=2, cutoff=80, days=7):
        schs = self.get_scheme(scheme,category, input_name, limit, cutoff)
        ret = []
        print(schs)
        for sch_tuple in schs:
            sch, time = sch_tuple
            weekagots = int((self.now - timedelta(days)).timestamp())
            nowts = int(self.now.timestamp())
            # get hashurl list sortby ts desc
            hashurl_list = self.rs.zrevrangebyscore(
                self._gen_subcata_key(scheme, category, sch),
                nowts, weekagots)
            for hashurl in hashurl_list:
                ret.append(self.rs.hgetall(self._gen_hashurl_key(hashurl)))
        return ret

    # iterate a list in batches of size n
    def _batcher(self, iterable, n):
        args = [iter(iterable)] * n
        return zip_longest(*args)

    def _gen_prim_key(self, content_type):
        return "gunrock:news:prim:{}".format(content_type)

    def _gen_scheme_key(self):
       return "gunrock:news:scheme"

    def _gen_cnt_key(self, subcata_key, sec_type):
        return "gunrock:news:{key}:{sec}:cnt".format(
            key=subcata_key, sec=sec_type)

    def _gen_subcata_key(self, subcata_key, sec_type, subcata_data):
        return "gunrock:news:{key}:{sec}:{data}".format(
            key=subcata_key, sec=sec_type, data=subcata_data)

    def _gen_hashurl_key(self, hashurl):
        return "gunrock:news:hashurl:{}".format(hashurl)


# TODO: user timeout decorator
class Timeout():
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()


if __name__ == "__main__":
    client = RNews_Client(addr='107.23.189.246')
    print(client.get_sources_with_timeout())
    print(client.get_scheme_keys_with_timeout())
    print(client.get_person_with_timeout("sports", 'lebron james', limit=1))
    print(client.get_location_with_timeout("sports", 'san fran'))
    print(client.get_theme_news_with_timeout("sports", 'worldcup', timeout=5))
    print(client.get_theme_with_timeout("sports", 'worldcup'))
    # get current popular news related to a topic
    print(client.get_pop_scheme_with_timeout("person", "sports", top=2))
    news = client.get_person_news_with_timeout("sports", 'stef cury', limit=2, cutoff=70)
    if news is not None:
        print(news[0]["headline"])
        print(news[0]["blurb"])
    news = client.get_theme_news_with_timeout("sports", 'nba', limit=2, cutoff=85)
    if news is not None:
        print(news[0]["headline"])
        print(news[0]["blurb"])
