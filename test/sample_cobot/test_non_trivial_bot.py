import os
import sys

cobot_home = os.environ.get('COBOT_HOME', os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
print('COBOT_HOME: ', cobot_home)
sys.path.append(cobot_home)

# Run integration test, assuming modules are already up by running app.py
test_config={
    "NLP": "http://localhost:5001",
    "GREETER": "http://localhost:2000"
}

for k, v in test_config.items():
    os.environ[k] = v

from unittest import TestCase
from sample_cobot.non_trivial_bot import lambda_handler
from sample_cobot.sample_events import new_session_event, existing_session_event, retrieval_event, offensive_event, greeter_event


# Precondition: set environment variable for NLP and GREETER before running this test.
class TestBot(TestCase):
    def test_execute(self):
        result = lambda_handler(event=greeter_event,
                                context={})
        print(result)


if __name__ == '__main__':
    TestBot().test_execute()