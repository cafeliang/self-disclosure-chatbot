import sys

# add package AlexaPrizeCobotToolkit to sys path
sys.path.append("./../..")
print("sys path {}".format(sys.path))

import unittest
from sample_cobot.docker.greeter.app import app
import json


class TestFlaskApi(unittest.TestCase):
    """
    Tests can be run by python3 -m unittest test_greeter.py after you are at test/sample_cobot/
    Precondition: dialog models must exist in test/sample_cobot/models/dialogue directory
    """

    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()

    def test_hello_world(self):
        response = self.app.get('/')
        self.assertEqual(response.get_data().decode(sys.getdefaultencoding()), 'Welcome')

        response = self.app.post('/greeter', data=json.dumps({'text': 'good', 'intent': 'greet'}))
        print(json.loads(response.get_data().decode(sys.getdefaultencoding())))
        self.assertEqual(json.loads(response.get_data().decode(sys.getdefaultencoding())).get('response'),
                         {'recipient_id': 'default', 'text': 'Hey! How are you?'} )


if __name__ == "__main__":
    unittest.main()
