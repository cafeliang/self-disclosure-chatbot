import sys
#add package AlexaPrizeCobotToolkit to sys path
sys.path.append("./../..")
print("sys path {}".format(sys.path))

import unittest
from cobot_common.docker.nlp.app import app
import json



class TestFlaskApi(unittest.TestCase):
    """
    Test can be run by python3 -m unittest test_nlp.py after you are at test/cobot_common/
    Precondition: spacy must exist in pythonpath and 'en' model must exist in spacy,
    1. run python3 -m pip install spacy to install spacy
    2. run python3 -m spacy download en to install 'en' model in Terminal
    """
    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()

    def test_hello_world(self):
        response = self.app.get('/')
        self.assertEqual(response.get_data().decode(sys.getdefaultencoding()), 'Welcome')

        response = self.app.post('/nlp', data=json.dumps({'text': 'good'}))
        print(json.loads(response.get_data().decode(sys.getdefaultencoding())))
        self.assertEqual(json.loads(response.get_data().decode(sys.getdefaultencoding())).get('numOfSentences'), 1)


if __name__ == "__main__":
    unittest.main()
