from unittest import TestCase

from cobot_python_sdk.path_dict import PathDict

class TestPathDict(TestCase):

    def setUp(self):
        self.source = {
            'field_1': {
                'field_1_sub1': 'field_1_sub1_val'
            }
        }
        self.path_dict = PathDict(self.source)

    def test_get(self):
        result = self.path_dict.get('field_2')
        self.assertEqual(result, None)

        result = self.path_dict.get('field_2', 'default')
        self.assertEqual(result, 'default')

        result = self.path_dict.get('field_1.field_1_sub1')
        self.assertEqual(result, 'field_1_sub1_val')

    def test_source(self):
        source = self.path_dict.source
        self.assertEqual(source, self.source)
