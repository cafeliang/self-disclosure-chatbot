from unittest import TestCase

from cobot_python_sdk.intent_mapper import IntentMapper


class TestIntentMapper(TestCase):

    def setUp(self):
        self.intent_mapper = IntentMapper(
            event=None,
            context=None,
            response_builder=None,
            attributes=None
        )

    def test_select_handler_0(self):
        # Initialize handler mapper
        self.intent_mapper._named_handlers = {
            'NewSession': None,
            'NewSession_GUESS': None,
            'SomeIntent_START': None,
            'LaunchRequest_START': None,
            'Unhandled_START': None
        }
        
        # New session without mode
        feature = {
            'new': True,
            'mode': ''
        }

        selected_name = self.intent_mapper._select_handler(feature)

        self.assertEqual(selected_name, 'NewSession')

        # New session with mode
        feature = {
            'new': True,
            'mode': '_GUESS'
        }

        selected_name = self.intent_mapper._select_handler(feature)
        self.assertEqual(selected_name, 'NewSession_GUESS')

        # New session not in handler mapper
        feature = {
            'new': True,
            'mode': '_START',
            'request_type': 'IntentRequest',
            'intent': 'SomeIntent'
        }
        selected_name = self.intent_mapper._select_handler(feature)
        self.assertEqual(selected_name, 'SomeIntent_START')

        # Not new session and has intent
        feature = {
            'new': False,
            'mode': '_START',
            'request_type': 'IntentRequest',
            'intent': 'SomeIntent'
        }
        selected_name = self.intent_mapper._select_handler(feature)
        self.assertEqual(selected_name, 'SomeIntent_START')

        # Not new session and no intent
        feature = {
            'new': False,
            'mode': '_START',
            'request_type': 'LaunchRequest',
            'intent': 'SomeIntent'
        }
        selected_name = self.intent_mapper._select_handler(feature)
        self.assertEqual(selected_name, 'LaunchRequest_START')

        # unhandled handler
        feature = {
            'new': False,
            'mode': '_START',
            'request_type': 'Intent',
            'intent': 'UnhandledIntent'
        }
        selected_name = self.intent_mapper._select_handler(feature)
        self.assertEqual(selected_name, 'Unhandled_START')

        # No corresponding handler registered
        feature = {
            'new': False,
            'mode': '_GUESS',
            'request_type': 'Intent',
            'intent': 'NoHandlerIntent'
        }
        with self.assertRaises(KeyError):
            selected_name = self.intent_mapper._select_handler(feature)