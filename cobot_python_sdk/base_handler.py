from abc import ABC, abstractmethod
from typing import Callable, Any

class Handler(ABC):
    """
    A class wraps a set of handlers and proxying through to the IntentMapper object
    """

    def __init__(self, ctx):
        # type: (Any) -> None
        self._ctx = ctx
        self.handler = self.create_handler()

    def __getattr__(self, method_name: str) -> Any:
        method = getattr(self._ctx, method_name)
        return method

    @abstractmethod
    def create_handler(self) -> dict:
        """
        Generate a dictionary mapping intent name to handler function.
        """
        pass


class ModeHandler(Handler):
    """
    Declare a set of handlers under specific mode
    """
    
    def create_mode_handler(self, mode: str, handlers: dict) -> dict:
        """
        Create a correspondence between intent under specific mode and its handler function.
        """
        handler_map = {}
        for intent, handler in handlers.items():
            handler_map[intent + mode] = handler
        return handler_map
    
   
