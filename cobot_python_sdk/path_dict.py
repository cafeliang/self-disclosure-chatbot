from typing import Optional, Any, Union, Dict

class PathDict(object):
    """
    Wrap a dictionary and provide function to get nested item using a string path.
    """

    def __init__(self, init_data: dict) -> None:
        self._data = init_data

    def get(self, 
            path: str, 
            default_val: Optional[Any] = None
        ) -> Union[Optional[Any], Dict[Any, Any]]:
        """
        Given a string path, return nested value. If the string path is invalid, return default value.

        :param path: A string path separated by period. EX: 'request.intent.name'
        :return A nested item, if the path is valid. Or a default value, if the path is invalid.
        """
        key_list = path.split('.')
        cur: Union[Optional[Any], Dict[Any, Any]] = self._data
        for key in key_list:
            if isinstance(cur, dict) and key in cur:
                cur = cur.get(key)
            else:
                cur = default_val
                break
        return cur
    
    @property
    def source(self) -> dict:
        """
        Return original dictionary object
        """
        return self._data
