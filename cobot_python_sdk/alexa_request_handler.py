import warnings
from injector import Injector, InstanceProvider, Binder

from .path_dict import PathDict
from .event import Event
from .intent_mapper import IntentMapper
from .base_dependency_type import Context, AppId, TableName, SaveBeforeResponse
from .dynamodb_manager import DynamoDbManager
from .user_attributes_manager import UserAttributesManager
from .user_attributes import UserAttributes
from .analyzer import Analyzer
from .base_handler import Handler

class AlexaRequestHandler():
    """
    This a main class to build skill. It can validate request and setup bottom level dependencies.
    It also allows developers to register functions to handle different intents. The last function is to
    run skill logics to generate a json format Alexa response.
    """

    def __init__(self,
                 event={}, # type: dict
                 context={}, # type: dict
                 app_id=None, # type: str
                 table_name=None, # type: str
                 save_before_response=False # type: bool
                ):
        # type: (...) -> None
        """
        Validate user's request and setup bottom level dependencies.

        :param event: Event parameter of lambda handler function.
        :param context: Context parameter of lambda handler function.
        :param app_id: Skill's application id which used to validate request.
        :param table_name: The name DynamoDB to persist attributes
        :param save_before_response: If it is true, skill persists attributes at the end of each turn. 
                                     Otherwise, only at the last turn of whole session
        """
        _event = Event(event)
        
        # self._validate_request(_event, app_id)

        if table_name is not None:
            DynamoDbManager.ensure_table_exists(table_name, 'user')

        def inject_configure(binder: Binder) -> None:

            """
            Setup bottom dependencies
            """
            binder.bind(Event, to=_event)
            binder.bind(Context, to=PathDict(context))
            binder.bind(AppId, to=app_id)
            binder.bind(TableName, to=InstanceProvider(table_name))
            binder.bind(SaveBeforeResponse, to=save_before_response)
        
        self.injector = Injector(inject_configure)

    @classmethod
    def _validate_request(cls, event: Event, app_id: str) -> None:
        """
        Validate that this request originated from authorized source.
        """
        request_app_id = event.app_id
        if app_id is None:
            warnings.warn('Warning: Application ID is not set')
        if app_id is not None and (request_app_id != app_id):
            raise Exception("The applicationIds don't match: request_app_id {}, handler_app_id {}".format(request_app_id, app_id))

    def register_handlers(self, *handler_map_list: Handler) -> None:
        """
        Store correspondence between intent and its handler function.
        :param handler_map_list: A list of dictionary contains intent and its handler function.
        """
        intent_mapper = self.injector.get(IntentMapper)
        intent_mapper.register_handlers(*handler_map_list)

    def persist(self, should_end_session: bool) -> None:
        """
        Persist attributes to DynamoDB

        :param bool should_end_session: A boolean value with true meaning that the session should end after Alexa speaks the response, or false if the session should remain active.
        """
        user_attributes_manager = self.injector.get(UserAttributesManager)
        save_before_response = self.injector.get(SaveBeforeResponse)
        attributes = self.injector.get(UserAttributes)

        if user_attributes_manager.persistence_enabled and (should_end_session or save_before_response):
            user_attributes_manager.persist_user_attributes(attributes)

    def execute(self) -> dict:
        """
        Run skill logic and return json format Alexa response to users.

        :return json format Alexa response.
        :rtype: dict
        """
        feature = self.injector.get(Analyzer).analyze()
        response = self.injector.get(IntentMapper).execute(feature)
        self.persist(response['response']['shouldEndSession'])
        return response

