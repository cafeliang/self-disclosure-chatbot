from typing import Callable, Dict, List, Any
from injector import inject, singleton

from .analyzer import Analyzer
from .event import Event
from .base_dependency_type import Context
from .alexa_response_builder import AlexaResponseBuilder
from .user_attributes import UserAttributes
from .base_handler import Handler

@singleton
class IntentMapper(object):
    """
    This class stores the correspondance between intent and handler function.
    It can invoke handler function by processing features from `Analyzer` 
    It can also invoke defined handler function to process undefined intent or mode.
    """
    @inject(
        event=Event,
        context=Context,
        response_builder=AlexaResponseBuilder,
        attributes=UserAttributes
    )
    def __init__(self, 
                 event, # type: Event
                 context, # type: Context
                 response_builder, # type: AlexaResponseBuilder
                 attributes # type: UserAttributes
                ):
        # type: (...) -> None
        self._named_handlers: dict = {}
        # Following helper object will be used by handler function
        self.event = event
        self.context = context
        self.response_builder = response_builder
        self.attributes = attributes

    def register_handlers(self, *mode_handler_cls_list: Handler) -> None:
        for mode_handler_cls in mode_handler_cls_list:
            named_handlers = mode_handler_cls(self).handler
            self._named_handlers.update(named_handlers)

    def run_handler(self, handler_name: str, *pargs: Any, **kargs: Any):
        """
        Run a intent handler function and get json format Alexa response. 
        Pass self to the handler function, so the function can access developer's setting,
        lambda's parameter objects and run another handler function.

        :return Json format Alexa response
        """
        handler = self._named_handlers[handler_name]
        return handler(*pargs, **kargs)

    def run_handler_with_mode(self, handler_name: str, *pargs: Any, **kargs: Any) -> dict:
        selected_name = handler_name + self.attributes.mode
        if selected_name not in self._named_handlers:
            selected_name = 'Unhandled' + self.attributes.mode
        return self.run_handler(selected_name, *pargs, **kargs) 

    def _select_handler(self, feature: dict) -> str:
        mode = feature['mode']

        selected_name = ''
        if feature['new'] and ('NewSession' + mode) in self._named_handlers:
            selected_name = 'NewSession'
        elif feature['request_type'] == 'IntentRequest':
            selected_name = feature['intent']
        else:
            selected_name = feature['request_type']

        selected_name += mode

        handler_name = selected_name

        if selected_name not in self._named_handlers:
            selected_name = 'Unhandled' + mode

        if selected_name not in self._named_handlers:
            raise KeyError("In mode: {mode}. No handler function was defined for event {handler_name} and no 'Unhandled' function was defined.".format(
                mode=mode, handler_name=handler_name
            ))

        return selected_name

    def execute(self, feature: dict) -> dict:
        """
        Use `Analyzer` to fetch feature from lambda event object and attributes persisted in DynamoDB.
        Then invoke responding handler function to generate Alexa response.
        """
        selected_handler_name = self._select_handler(feature)
        return self.run_handler(selected_handler_name)

