from typing import Callable, Any, Dict, TypeVar
from injector import inject, singleton

from .user_attributes import UserAttributes


class AlexaResponseBuilder(object):
    """
    This class is to build response following alexa skill's json interface.
    Alexa Response format reference: https://developer.amazon.com/docs/custom-skills/request-and-response-json-reference.html#response-format
    
    example:
    builder = AlexaResponseBuilder()
    builder.speak('hello world').listen('say again').build()
    """

    @inject(
        attributes=UserAttributes,
    )
    def __init__(self, attributes):
        # type: (UserAttributes) -> None
        self.attributes = attributes

        self._response: Dict[str, Any] = {
            'version': '1.0',
            'response': {
                'shouldEndSession': True
            }
        }

    def speak(self, speech_output: str) -> 'AlexaResponseBuilder':
        """
        Provide the speech to render to the user

        :param str speech_output: ssml message without <speeak> wrapper
        """
        self._response['response']['outputSpeech'] = self._create_ssml_speechobject(speech_output)
        return self

    def listen(self, reprompt_speech: str) -> 'AlexaResponseBuilder':
        """
        Provide re-prompt speech to render to the user

        :param str reprompt_speech: ssml message without <speeak> wrapper
        """
        self._response['response']['reprompt'] = {
            'outputSpeech': self._create_ssml_speechobject(reprompt_speech)
        }
        self._response['response']['shouldEndSession'] = False
        return self

    def build(self) -> dict:
        """
        Get a JSON format alexa response
        
        :return Alexa response body
        :rtype dict
        """
        return self._response

    @classmethod
    def _create_ssml_speechobject(cls, ssml_message: str) -> dict:
        """
        User can use SSMLBuilder to generate a response text with SSML tags.

        :param ssml_message: ssml message without <speak> wrapper
        :return: OutputSpeech Object
        :rtype: dict
        """
        return {
            'type': 'SSML',
            'ssml': '<speak>{}</speak>'.format(ssml_message)
        }
