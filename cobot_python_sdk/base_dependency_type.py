from injector import Key

Context = Key('context')
AppId = Key('app_id')
TableName = Key('table_name')
SaveBeforeResponse = Key('save_before_response')