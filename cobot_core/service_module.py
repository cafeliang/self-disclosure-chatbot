from datetime import datetime
import requests
import json
from cobot_common.service_client import get_client
from typing import Union
import decimal
import os

from cobot_core.log.logger import LoggerFactory
from cobot_core.state_manager import StateManager
from cobot_core.service_module_request import ServiceModuleRequest
from cobot_core.service_module_response import ServiceModuleResponse

# working environment
WORKING_ENV = "local"
if os.environ.get('STAGE') == 'PROD' or os.environ.get('STAGE') == 'BETA':
    WORKING_ENV = os.environ.get('STAGE').lower()
else:
    WORKING_ENV = "local"

EMPTY_JSON = "\{\}"

class ServiceModule(object):
    """
    The ServiceModule class encapsulates local or remote execution capabilities. This can be used to implement a remote ResponseGenerator as well as other Cobot components (NLP, Sentiment Analyzer, and DialogManager components).
    """
    def __init__(self,
                 state_manager: StateManager,
                 module_name: str,
                 service_module_config
                 ) -> None:
        self.state_manager = state_manager
        self.module_name = module_name
        self.service_module_config = service_module_config
        self.url = self.service_module_config.url
        self.input_data = ServiceModuleRequest(self.service_module_config, self.state_manager).input_data
        self.logger = LoggerFactory.setup(self)
        self.timeout_in_millis = self.service_module_config.timeout_in_millis

    def execute_and_save(self):
        """
        Execute the main logic and save the response to the state manager
            (where response and additional context manager key-value pairs will be saved to its current state, and user attributes will be saved to its user attributes).
        :return: response
        """
        response = self.execute()
        if response:
            self.add_response_to_state_manager(response)
        return response

    def execute(self):
        """
        Execute the main logic and return the response.
        It is also used for multi-threading calls using concurrent.future.ThreadPoolExecutor, for detail, see CallModulesWithThreadPool class in pipeline.py
        :return: response
        """
        raise NotImplementedError('Not Implemented')

    # async def async_execute(self, client):
    # 	"""
    # 	async_execute method is also used for multi-threading calls using asyncio and aiohttp, for detail, see CallModulesWithAsyncio class in pipeline.py
    # 	"""
    # 	self.execute()

    def add_response_to_state_manager(self, response_object: Union[str, list, dict]):
        """
        Save response_object to state manager's current_state and user attributes.
        """
        service_module_response = ServiceModuleResponse(response_object)
        service_module_response.save_to_state_manager(self.state_manager, self.module_name)


class RemoteServiceModule(ServiceModule):
    """
    A remoted version of a ServiceModule
    """
    NLP_LIST = ["ner", "sentiment", "topic", 'npknowledge', 'intent_classify', 'profanity_check', 'coreference']

    def execute(self):

        """
        Execute the service on the serialized state information.
        """
        start = datetime.now()
        self.logger.info("Executing: %s", self.module_name)
        #self.logger.info("url: %s, data: %s", self.url, json.dumps(self.input_data, cls=DecimalEncoder))

        if self.module_name in self.NLP_LIST:
            compress_data = {
                'text': self.input_data['text'],
                'user_id': self.input_data['user_id'],
            }
            self.logger.info("url: %s, data: %s", self.url, json.dumps(compress_data, cls=DecimalEncoder))
            response = requests.post(self.url,
                                     data=json.dumps(compress_data, cls=DecimalEncoder),
                                     headers={'content-type': 'application/json'},
                                     timeout=self.timeout_in_millis / 1000.0)

        else:
            self.logger.info("url: %s, data: %s", self.url, json.dumps(self.input_data, cls=DecimalEncoder))

            #             if WORKING_ENV == 'local':
            # COBOT_HOME = os.environ.get('COBOT_HOME')
            #     write_input_data_to_file(self.input_data, os.path.join(COBOT_HOME, 'integration_log', 'remote_module_latest_input_data.txt'))

            response = requests.post(self.url,
                                     data=json.dumps(self.input_data, cls=DecimalEncoder),
                                     headers={'content-type': 'application/json'},
                                     timeout=self.timeout_in_millis / 1000.0)
        try:
            response.raise_for_status()
            end = datetime.now()
            if response is None:
                self.logger.warning("Remote Module {} returns nothing".format(self.module_name))
                return "\{\}"
            
            self.logger.info("Finished: {}, result: {}, latency: {}ms".format(self.module_name, response.json(),
                                                                (end - start).total_seconds() * 1000))
                        
            return response.json()
        except requests.exceptions.HTTPError as errh:
            self.logger.error("HTTPError err:  {}".format(errh))
            return EMPTY_JSON
        except requests.exceptions.ConnectionError as errc:
            self.logger.error("ConnectionError err:  {}".format(errc))
            return EMPTY_JSON
        except requests.exceptions.Timeout as errt:
            self.logger.error("Timeout err:  {}".format(errt))
            return EMPTY_JSON
        except requests.exceptions.RequestException as err:
            self.logger.error("RequestException err:  {}".format(err))
            return EMPTY_JSON
        except Exception as err:
            self.logger.error("Other err:  {}".format(err))
        return EMPTY_JSON




    # async def async_execute(self, client):
    #     start = datetime.now()
    #     print (start, "Executing: ", self.module_name)
    #     print("url: ", self.url, "data: ", json.dumps(self.input_data))
    #     with async_timeout.timeout(10):
    #         response = await client.request('post',
    #                                         self.url,
    #                                         data=json.dumps(self.input_data),
    #                                         headers={'content-type': 'application/json'})
    #         response_json = await response.json()
    #
    #         end = datetime.now()
    #         print("Finished: {}, result: {}, latency: {}ms".format(self.module_name, response_json, (end - start).total_seconds() * 1000 ))

def write_input_data_to_file(input_data, file_path):
    import pprint
    with open(file_path, 'w+') as f:
        # stdout is a bytes sequence and has '\n' at the end
        f.write(pprint.pformat(input_data))


class LocalServiceModule(ServiceModule):
    """
    A local version of a ServiceModule
    """


class ToolkitServiceModule(ServiceModule):
    api_key = None

    def __init__(self, state_manager, module_name, service_module_config):
        super().__init__(state_manager, module_name, service_module_config)
        if ToolkitServiceModule.api_key is None:
            raise Exception('Warning: API Key is not set. Please set it in your Cobot instance. Otherwise you cannot connect with Alexa Prize Toolkit Service')
        else:
            self.api_key = ToolkitServiceModule.api_key
            self.toolkit_service_client = get_client(api_key=self.api_key, timeout_in_millis=1000)


class DecimalEncoder(json.JSONEncoder):

    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)
