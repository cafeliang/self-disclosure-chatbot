from cobot_core import StateManager
from injector import inject

from cobot_core.log.logger import LoggerFactory
from cobot_python_sdk.alexa_response_builder import AlexaResponseBuilder

class RepromptBuilder(object):
    @inject
    def __init__(self, state_manager: StateManager):
        self.state_manager = state_manager
        self.logger = LoggerFactory.setup(self)

    def build(self, speech_output):
        return speech_output


class ResponseBuilder(object):
    """
    ResponseBuilder constructs a valid TTS response block for the ASK system
    """

    @inject
    def __init__(self, response_builder: AlexaResponseBuilder, state_manager: StateManager, reprompt_builder: RepromptBuilder):
        self.state_manager = state_manager
        self.response_builder = response_builder
        self.reprompt_builder = reprompt_builder
        self.logger = LoggerFactory.setup(self)

    def build(self, speech_output, end_session_flag):
        # type: (Dict) -> Dict

        self.state_manager.current_state.response = speech_output
        if end_session_flag:
            response = self.response_builder.speak(speech_output).build()
        else:
            reprompt = self.reprompt_builder.build(speech_output)
            self.logger.info('Reprompt: {}'.format(reprompt))
            response = self.response_builder.speak(speech_output).listen(reprompt).build()

        if 'sessionAttributes' not in response or response['sessionAttributes'] == None:
            response['sessionAttributes'] = {}
        response['sessionAttributes']['lastState'] = {}  # different from cobot2.1.1


        response['sessionAttributes']['conversationId'] = self.state_manager.current_state.conversation_id
        return response
