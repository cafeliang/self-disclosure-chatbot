from injector import inject

from cobot_core.log.logger import LoggerFactory
from .state_manager import StateManager
from typing import List

class RankingStrategy(object):
    """
    The BaseRankingStrategy returns the first element in a list of candidate responses. 
    
    In the case of a PolicySelectingStrategy or similar architecture where the optimal response mode is 
    determined prior to invoking ResponseGenerators, this default behavior is adequate.
    
    In the case of a Cobot implementation focused on generating multiple candidate responses and selecting
    optimal responses based on topical or semantic coherence, information content metrics, predicted engagement
    based on textual response analysis, much of the "heavy lifting" of the dialog strategy may occur in a
    metric-focused implementation of the RankingStrategy interface.
    """

    @inject
    def __init__(self, state_manager: StateManager):
        self.state_manager = state_manager
        self.logger = LoggerFactory.setup(self)

    def rank(self, output_responses):
        # type: (List[str]) -> str
        """
        From a list of candidate responses, return a single, best response based on a ranking mechanism
        """
        self.logger.info('Rank 1st from {}'.format(output_responses))
        return output_responses[0]

    def rank_advanced(self, candidate_responses: dict) -> str:
        return self.rank(list(candidate_responses.values()))


