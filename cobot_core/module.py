from injector import Module

from .global_intent_handler import GlobalIntentHandler
from .dialog_manager import DialogManager
from .ranking_strategy import RankingStrategy
from .selecting_strategy import SelectingStrategy
from .state_manager import StateManager
from .feature_extractor import FeatureExtractor

from cobot_python_sdk.analyzer import Analyzer
from cobot_python_sdk.intent_mapper import IntentMapper

class SocialbotBaseModule(Module):
    """
    Base class for all of the major module components.
    """

    def configure(self, binder):
        """
        Set a default list of bindings for components
        """
        binder.bind(GlobalIntentHandler, to=GlobalIntentHandler)
        binder.bind(Analyzer, to=FeatureExtractor)
        binder.bind(IntentMapper, to=DialogManager)
        binder.bind(StateManager, to=StateManager)
        binder.bind(SelectingStrategy, to=SelectingStrategy)
        binder.bind(RankingStrategy, to=RankingStrategy)
