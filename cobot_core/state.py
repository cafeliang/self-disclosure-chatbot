from typing import *
import json
import decimal
from datetime import datetime
from functools import singledispatch
from cobot_python_sdk.event import Event

class State(object):
    """
    Encapsulates the current state of the Cobot system, as managed by the StateManager
    """

    def __init__(self, user_id: str,
                 conversation_id: str,
                 session_id: str,
                 creation_date_time: str = None,
                 request_type: str = None,
                 intent: str = None,
                 slots: dict = {},
                 topic: str = None,
                 asr: List = [],
                 text: str = '',
                 response: dict = {},
                 mode: str = None) -> None:
        """
        Initialize a State object with provided fields.
        :param user_id: user id
        :param conversation_id: conversation id
        :param session_id: session id
        :param creation_date_time: state creation timestamp, default to None
        :param request_type: LaunchRequest, IntentRequest, or SessionEndedRequest, default to None
        :param intent: NLU intent, default to None
        :param topic: topic, default to None
        :param asr: request from ASK lambda function
        :param text: text extracted from highest confidence asr or raw TEXT slot
        :param response: generated response
        """
        self.user_id = user_id
        self.conversation_id = conversation_id
        self.session_id = session_id
        if creation_date_time is not None:
            self.creation_date_time = creation_date_time
        else:
            self.creation_date_time = str(datetime.utcnow().isoformat())
        self.request_type = request_type
        self.intent = intent
        self.slots = slots
        self.topic = topic
        self.asr = asr
        self.text = text
        self.response = response
        self.mode = mode

    def to_json(self) -> str:
        """
        Serialize State object to JSON string.
        :return: JSON serialized string
        """
        return json.dumps(self.__dict__, default=serialize_to_json)


    @classmethod
    def from_ask_request(cls, event: Event) -> 'State':
        """
        Initialize a State object from ASK request.
        :param event: ASK event
        :return: State object
        """
        # If request type is LaunchRequest, set intent to LaunchRequestIntent
        request_type = event.get('request.type', None)
        if request_type == 'LaunchRequest':
            intent = 'LaunchRequestIntent'
        else:
            intent = event.get('request.intent.name', None)
        asr = event.speech_recognition
        slots = event.get('request.intent.slots', None)
        text = State.extract_text_from_event(event)

        return State(user_id=event.user_id,
                     conversation_id=event.conversation_id,
                     session_id=event.get('session.sessionId', None),
                     request_type=request_type,
                     intent=intent,
                     slots=slots,
                     asr=asr,
                     text=text)

    @classmethod
    def extract_text_from_event(cls, event: Event):
        asr = event.speech_recognition
        n_best_asr_text = []
        for n_best in asr:
            tokens = n_best['tokens']
            text = ""
            for token in tokens:
                value = token['value']
                text = text + " " + value
            n_best_asr_text.append(text)
        # Rule 1: extract the 1st asr text
        if n_best_asr_text:
            return n_best_asr_text[0].strip()

        # Rule 2: extract raw text (AMAZON.LITERAL/AMAZON.RAW_TEXT) slot
        text = ''
        slots = event.get('request.intent.slots', None)
        if slots and type(slots) == dict:
            # TODO: delete, this is here to extract AMAZON.LITERAL/AMAZON.RAW_TEXT for greeter_bot and non_trivial_bot before AP team's skills are whitelisted for raw asr input.
            if 'text' in slots.keys() and 'value' in slots['text']:
                text = slots['text']['value']
            elif 'all_text' in slots.keys() and 'value' in slots['all_text']:
                text = slots['all_text']['value']
            elif 'topic' in slots.keys() and 'value' in slots['topic']:
                text = slots['topic']['value']
            elif 'question_text' in slots.keys() and 'question_word' in slots.keys():
                text = slots['question_word']['value'] + " " + slots['question_text']['value']
            elif 'name_text' in slots.keys():
                text = 'My name is ' + slots['name_text']['value']
            elif 'health_symptom' in slots.keys():
                text = 'I have ' + slots['health_symptom']['value']

        return text


    @classmethod
    def from_json(cls, json_str: str) -> 'State':
        """
        Initialize a State Object from JSON string.
        :param json_str: JSON string contains State information
        :return: State object
        """
        json_str = json_str.replace("'", "\"")
        json_dict = json.loads(json_str)
        return cls(**json_dict)

    def set_intent(self, intent: str) -> None:
        """
        Set Intent in the State object to the provided intent
        :param intent: NLU Intent value
        :return: None
        """
        self.intent = intent

    def set_topic(self, topic: str) -> None:
        """
        Set Topic value in the State object to the provided topic
        :param topic: Topic value
        :return: None
        """
        self.topic = topic

    def set_response(self, response: Any) -> None:
        """
        Set response in the State object to the provided response.
        :param response: ASK response
        :return: None
        """
        self.response = response

    def set_mode(self, mode: Any) -> None:
        """
        Set mode in the State object to the provided mode.
        :param mode: state mode, used for dialog manager
        :return: None
        """
        self.mode = mode

    def set_new_field(self, new_key: Any, new_value: Any) -> None:
        """
        set a new key-value pair to the State object.
        :param new_key: new key
        :param new_value: new value
        :return: None
        """
        setattr(self, new_key, new_value)

    def __str__(self):
        """
        Override the default string behavior
        :return: string representation
        """
        return self.to_json()


    def __repr__(self):
        """
        Override the default string behavior
        :return: string representation
        """
        return self.__str__()

@singledispatch
def serialize_to_json(val):
    return json.dumps(val.__dict__)

