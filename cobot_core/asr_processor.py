from cobot_core.log.logger import LoggerFactory
from cobot_core.offensive_speech_classifier import OffensiveSpeechClassifier
from cobot_core.prompt_constants import Prompt
from injector import inject
from .state_manager import StateManager

class ASRProcessor(object):
    @inject
    def __init__(self,
                 state_manager: StateManager,
                 offensive_speech_classifier: OffensiveSpeechClassifier):
        self.state_manager = state_manager
        self.offensive_speech_classifier = offensive_speech_classifier
        self.logger = LoggerFactory.setup(self)

    """
    Deal with low confidence ASR and offensive utterance
    """

    def process(self, input=None):
        text = self.state_manager.current_state.text

        # Case 1: Move on to the next step in the cobot handler workflow
        # when the request doesn't have text value
        if not text:
            return None

        # Case 2: suggest alternative topics when the topic contains offensive speech
        try:
            offensive_result = self.offensive_speech_classifier.classify([text])[0]
            setattr(self.state_manager.current_state, 'input_offensive', offensive_result)
            if offensive_result:
                return Prompt.topic_redirection_prompt
        except:
            self.logger.error('Exception in ASR Processor', exc_info=True)
            return None
        # Case 3: move on to the next step in the cobot handler workflow
        return None
