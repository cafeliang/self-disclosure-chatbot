from datetime import datetime
from injector import inject
from cobot_common.service_client import get_client
from cobot_core.log.logger import LoggerFactory
from cobot_core.service_module import ToolkitServiceModule
from cobot_core.state_manager import StateManager
from cobot_core.utils import parse_blacklist_class_from_profanity_response, parse_overall_class_from_profanity_response


class OffensiveSpeechClassifier(object):
    """
    Base class for OffensiveSpeechClassifier.
    If api_key is set in cobot handler, it uses the AP science team's offensive speech classifier model hosted by AlexaPrizeToolkitService.
    Otherwise, it always return False.
    """
    @inject
    def __init__(self,
                 state_manager: StateManager):
        self.state_manager = state_manager
        if ToolkitServiceModule.api_key is None:
            self.api_key = None
        else:
            self.api_key = ToolkitServiceModule.api_key
            self.toolkit_service_client = get_client(api_key=self.api_key, timeout_in_millis=800)
        self.logger = LoggerFactory.setup(self)

    def classify(self, input_data_list):
        """
        classify all texts in the input_data_list by calling toolkit service client in one call
        :param input_data_list: input texts, List of String
        :return: offensive results, 0 is not-offensive, 1 is offensive, List of Number
        """
        assert isinstance(input_data_list,
                          list), "input_data_list when calling OffensiveSpeechClassifier's batch_execute method must be list"
        # If api_key is not set, always return 0
        if self.api_key is None:
            return [0] * len(input_data_list)

        # Else, call AlexaPrizeToolkitService's profanity model
        start = datetime.now()
        response = self.toolkit_service_client.batch_detect_profanity(utterances=input_data_list)
        profanity_result = parse_blacklist_class_from_profanity_response(response)
        # profanity_result = parse_overall_class_from_profanity_response(response) #COBOT 2.1 Update, not applied
        end = datetime.now()
        self.logger.info("Finished batch Offensive Speech Classifier, result: {}, latency:{}ms".format(profanity_result, (
        end - start).total_seconds() * 1000))
        return profanity_result
