class Constant(object):
    response_generators_config_key = 'ResponseGenerators'
    nlp_pipeline_config_key = 'NLPPipeline'