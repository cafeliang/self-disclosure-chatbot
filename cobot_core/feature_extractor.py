from cobot_core.log.logger import LoggerFactory
from cobot_core.nlp.pipeline import NLPPipeline
from cobot_core.service_module_manager import ServiceModuleManager
from cobot_python_sdk.analyzer import Analyzer
from cobot_python_sdk.event import Event
from cobot_python_sdk.user_attributes import UserAttributes
from cobot_python_sdk.user_attributes_manager import UserAttributesManager
from injector import inject
from typing import Optional, Dict
from .state_manager import StateManager


class FeatureExtractor(Analyzer):
    """
    Extract intent, ner, sentiment, topic, etc features from inputs
    """

    @inject
    def __init__(self,
                 event: Event,
                 attributes: UserAttributes,
                 user_attributes_manager: UserAttributesManager,
                 state_manager: StateManager,
                 nlp_pipeline: NLPPipeline,
                 service_module_manager: ServiceModuleManager):
        super().__init__(event=event, attributes=attributes, user_attributes_manager=user_attributes_manager)
        self.state_manager = state_manager
        self.nlp_pipeline = nlp_pipeline
        self.service_module_manager = service_module_manager
        self.logger = LoggerFactory.setup(self)

    def analyze(self) -> Optional[Dict]:
        self.nlp_pipeline.run()
        # print('User attributes after merge with DynamoDB: {}'.format(self.state_manager.user_attributes))

        pipeline_names = self.service_module_manager.get_nlp_pipeline_names()

        # 'intent' feature is used for some selecting strategy in the dialog manager, so always add 'intent' to features
        unique_pipeline_names = set(pipeline_names)
        unique_pipeline_names.add('intent')

        features = {}
        for pipeline_name in unique_pipeline_names:
            features[pipeline_name] = getattr(self.state_manager.current_state, pipeline_name, None)
        self.state_manager.current_state.features = features
        self.logger.info('features: %s', features)
        return features
