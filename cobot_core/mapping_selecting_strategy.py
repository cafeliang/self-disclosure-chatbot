from injector import inject, Key

from cobot_core.service_module_manager import ServiceModuleManager
from cobot_core.state_manager import StateManager
from cobot_core.selecting_strategy import SelectingStrategy
from typing import Dict, List, Any

IntentMap = Key("intent_map")


class MappingSelectingStrategy(SelectingStrategy):

    """
    MappingSelectingStrategy is a simple SelectingStrategy implementation that uses a pre-defined map of intents for each ResponseGenerator.

    The source map is passed in at initialization time.
    
    """

    @inject
    def __init__(self,
                 intent_map: IntentMap,
                 state_manager: StateManager,
                 service_module_manager: ServiceModuleManager):
        self.intent_map = intent_map
        self.state_manager = state_manager
        self.service_module_manager = service_module_manager

    def select_response_mode(self, features:Dict[str,Any])->List[str]:
        """
        Returns the list of valid ResponseGenerator modules for the given input
        """
        intent = features['intent']
        rgs = self.intent_map.get(intent, self.service_module_manager.response_generator_names)
        if isinstance(rgs, str):
            rgs = [rgs]
        keys = self.service_module_manager.response_generator_names
        return list(set(keys).intersection(rgs))
