import redis
import time
import csv
import json
from datetime import datetime
import time
from tqdm import tqdm

KEYWORD_PREFIX = "gunrock:ic:holiday:"
KEYWORD_TS = "gunrock:ic:holiday:ts"
r = redis.StrictRedis(host='language.cs.ucdavis.edu', port=5012, db=0, password="alexaprize", decode_responses=True)


def write_holiday_map(ts, data):
    r.zadd(KEYWORD_TS, {data["Holiday"]: ts})
    r.hmset(KEYWORD_PREFIX + data["Holiday"], data)

def get_keyword_map(key):
    key_ts = r.zscore(KEYWORD_TS, key)
    now_ts = time.time()
    if int(key_ts) + 86400 < now_ts:  # 1 day
        return None
    return r.get(KEYWORD_PREFIX + key)

def get_holiday_by_time_range(ts_start='-inf', ts_end='inf'):
    return r.zrangebyscore(KEYWORD_TS, ts_start, ts_end, withscores=True)


if __name__ == '__main__':
    r.delete(KEYWORD_TS)
    with open('gunrock_cobot/response_templates/cleaned_holiday.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in tqdm(reader):
            ts = datetime.strptime(row["Date"], '%Y-%m-%d').strftime("%s")
            write_holiday_map(ts, row)
    # second_before_today = int(datetime.today().strftime("%s")) - 1
    # print(second_before_today)
    # print(get_holiday_by_time_range(ts_start=second_before_today)[0])
    # write_keyword_map(test_key, test_val)
    # print(get_keyword_map(test_key))
    # print(get_keywords_by_time_range(ts_start=0, ts_end=now_ts)) # input is timestamp
    # print(get_keywords_by_time_range())  # it gets everykeys no matter time

