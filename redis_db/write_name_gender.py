import redis
import time
import csv
import json
from datetime import datetime
import time
from tqdm import tqdm
import re

SERVER_IP = 'language.cs.ucdavis.edu'


NAME_KEY_PREFIX = "gunrock:name:"

CSV_COLUMN_NAME = "name"
CSV_COLUMN_GENDER = "gender"

REDIS_FIELD_GENDER = "gender"

r = redis.StrictRedis(host=SERVER_IP, port=5012,
                      socket_timeout=3,
                      socket_connect_timeout=1,
                      retry_on_timeout=True,
                      charset='utf-8',
                      db=0, password="alexaprize", decode_responses=True)


def preprocess_csv():
    with open('name_gender_raw.csv') as csvfile:
        reader = csv.DictReader(csvfile)

        row_list = []
        for row in tqdm(reader):
            p = re.compile('^[a-zA-Z]*$')
            if p.match(row['name']):
                row_list.append({'name': row['name'], 'gender': row['gender']})

        print(len(row_list))

    with open('name_gender_clean.csv', 'w+') as write_csv_file:
        fieldnames = ['name', 'gender']
        writer = csv.DictWriter(write_csv_file, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerows(row_list)


def write_csv_to_redis():
    with open('name_gender_clean.csv') as csvfile:
        reader = csv.DictReader(csvfile)

        for row in tqdm(reader):
            write_row_to_redis(row)


def write_row_to_redis(row):
    r.hset(NAME_KEY_PREFIX + row[CSV_COLUMN_NAME].lower(), REDIS_FIELD_GENDER, row[CSV_COLUMN_GENDER])
    print(row)

if __name__ == '__main__':
    # preprocess_csv()
    write_csv_to_redis()