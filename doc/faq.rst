.. _faq:

Frequently Asked Questions
==========================

Welcome to the Cobot Frequently Asked Questions (FAQ). 

-------
Prompts
-------

How do I change the opening prompt?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The prompt (and reprompt) are set in ``prompt_constants.py``. However, you can override the default prompt by using the `custom global intent handler <overriding_cobot_components.html#how-to-override-global-intent-handler>`_.

***Note:** Alexa Prize opening prompts must begin with: "Hi, this is an Alexa Prize Socialbot", and optionally offer suggested topics to talk about.

----------------
Built-in Intents
----------------

What built-in intents should I use?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Amazon provides `many useful built-in intents <https://developer.amazon.com/docs/custom-skills/implement-the-built-in-intents.html>`_ for use in your skills. For Cobot, there are at least a couple you will want to integrate into your intent schema.

- AMAZON.StopIntent

Allows the user to completely exit the skill. **Note:** Alexa Prize teams must implement AMAZON.StopIntent, and may not reply with any message when the user says "stop", "quit", "shut up", etc. 

- AMAZON.RepeatIntent

Allows the user to say "repeat," "say that again," or "repeat that." This is a useful intent to have in your skill, so that you don't have to build in this functionality yourself.

For more details, refer to https://developer.amazon.com/docs/custom-skills/implement-the-built-in-intents.html.

--------------
Making Changes
--------------

.. _ask_skill_deploy:

How do I add/remove/update intents?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Making updates to your intent schema should be done through your CoBot's en-US.json file. Then, submit your changes via the ASK CLI.

.. code-block:: bash

  ask api update-model -s amzn1.ask.skill.<ask-app-id> -f path/to/models/en-US.json -l en-US


How do I update my skill's invocation name?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

From your CoBot's skill directory (e.g., [my_cobot]/skill), run:

.. code-block:: bash

  ask deploy -t skill


What's the difference between ``git push``, ``cobot update``, and ``cobot deploy``?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
There are two types of updates within CoBot: 

- **Code changes**, which are Lambda code and remote Docker module changes to the code that operates your CoBot, and
- **Infrastructure changes**, which are changes to the infrastructure/stack on which your CoBot lives

1. **When you wish to push code changes, just use** ``git push``.

.. code-block:: bash

  git add .
  git commit -m 'Details about the update...'
  git push

This kicks off your CodePipeline deployment. (Be sure to visit your CodePipeline console to review the change(s).)

2. **When you make a change to your infrastructure/stack, use,** ``cobot update``.

This includes adding/removing any Docker modules, or making any resource changes to existing Docker modules, like modifying the instance size or adjusting your memory allocation.
If you're just making code changes *inside* a Docker module, you only need to use `git push` as described above in #1.

.. code-block:: bash

  cobot update <your-bot-name>

3. **Finally, only use** ``cobot deploy`` **when you deploy your CoBot for the very first time.**

This will Deploy your docker modules and code you have committed to your Git repo.

.. code-block:: bash

   cobot deploy <your-bot-name>


-----------------------------
Troubleshooting Common Errors
-----------------------------

What should I do if I see "an unexpected error occurred while waiting for pipeline execution to complete. Visit AWS CodePipeline to check the status of pipeline execution."
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally, this means you need to visit your CodePipeline page and approve the deployment to your prod infrastructure.

What should I do when I get a "model build failure" when updating my skill?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It's likely your invocation name is still set to <CHANGEME> or has other invalid characters. Check the invocation name in your CoBot's config.yaml.

What should I do if I see "unknown output type" while deploying my skill?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use ``aws configure`` to set your output type to "json":

.. code-block:: bash

  aws configure
  AWS Access Key ID [****************ABCD]: 
  AWS Secret Access Key [****************EFGH]: 
  Default region name [us-east-1]: 
  Default output format [None]: json

I see "stack already exists" when doing a ``cobot deploy``. What does this mean?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

  Creating root stack...

  An error occurred (AlreadyExistsException) when calling the CreateStack operation: Stack [cobot-stack] already exists
  Unable to initiate the requested stack operation.
  Stack creation for stack cobot-stack failed.
  Exiting...

Only use ``cobot deploy`` to create a new stack. To update an existing stack, use ``cobot update``. To create a new stack with the same name, delete the old stack first using
``cobot delete <sample-cobot> -t stack``.

Wait for the stack to finish deleting before running ``cobot deploy`` again.

How do I fix an "AWS region different from your pipeline" error?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You will probably see this error when you run ``cobot deploy`` CLI.
Ensure that your CodePipeline and S3 bucket are in the same region. 
Run ``aws configure`` to set the default region to "us-east-1" and choose a new S3 bucket name in your config.yaml file. 
Then run ``cobot deploy`` to generate a new S3 bucket in us-east-1. **Note: Alexa Prize teams should exclusively use the "us-east-1" region.**

How do I fix a "RequestEntityTooLargeException" during skill deploy?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

  Model deployment finished.
  Create Lambda error.
  RequestEntityTooLargeException: Request must be smaller than 69905067 bytes for the CreateFunction operation
  Unable to deploy skill.

Your Lambda code is too large to be deployed. You will need to move any large datasets or libraries into remote modules.

What does "URL doesn't exist in loader" mean?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:::

  GREETER doesn't exist in loader 
  {'NLP': 'http://cobot-LoadB-1SL8JLQ5T696-223061507.us-east-1.elb.amazonaws.com', 
  'EVEBOT': 'http://cobot-LoadB-V91CD233HYT4-1249276479.us-east-1.elb.amazonaws.com',
  'RETRIEVAL': 'http://cobot-LoadB-UJU9JXSCJIN2-721739448.us-east-1.elb.amazonaws.com',
  ...

While creating a remote response generator, you likely missed one of the 5 steps:

1) Create a template for your Response Generator 

.. code-block:: bash

  bin/add-response-generator <response_generator_name> <cobot_directory>

2) Define your Response Generator module in your Lambda_handler

.. code-block:: bash

  RetrievalWikiBot = {
    'name': "RETRIEVALWIKI",    
    'class': RemoteServiceModule,    
    'url': ServiceURLLoader.get_url_for_module("RETRIEVALWIKI"),    
    'context_manager_keys': ['intent','slots']
  }

3) Add to list of response generators

.. code-block:: bash

  cobot.add_response_generators([RetrievalWikiBot])

4) Add your response generator to modules.yaml

.. code-block:: bash

  cobot_modules:
    - custom
    - retrievalwiki

5) Deploy the generator:

.. code-block:: bash

  git add <cobot_directory>/docker/<response_generator_name>
  git commit -m 'Commit message...'
  git push 
  cobot update <cobot_directory>
