.. _testing_cobot_components:

Testing Cobot Components
========================
.. _cobot_quick-test:

---------------------
Docker Module Testing
---------------------

Using cobot quick-test CLI
^^^^^^^^^^^^^^^^^^^^^^^^^^

A simple mechanism for testing during development is to create a file called test_script in your ``<cobot-name>/docker/<module-name>/app`` directory and use the quick-test script included in the ``cobot_common/docker/template/app``.
If you create your docker module by copying it from cobot_common/docker/template, quick-test script will be included in your <cobot-name>/docker/<module-name>/app directory.
This doesn't do the detailed output validation that unit tests do, but is a simple mechanism to check for breakages during development.

Under the hood, it kicks off app.py, and sends your data specified in test_script to your local Flask server at localhost:port and prints response to Terminal, and finally terminates app.py.

From within the ``app`` directory you can run:

.. code-block:: bash

   ./quick-test

or use the ``cobot quick-test`` CLI to invoke a quick-test:

.. code-block:: bash

   cobot quick-test <botname> <modulename>

.. _docker_module_test:

Using docker CLI
^^^^^^^^^^^^^^^^
After creating your Docker Module, you can test it by sending a curl command to a RUNNING docker container.

1. Install and setup Docker: https://docs.docker.com/install/ [Required once only]

2. Build a docker image

.. code-block:: bash

   docker build -t <image_name> directory

3. Run a docker container

.. code-block:: bash

   docker run -it -p <host_port>:80 <image_name>

4. Send a curl command to the local docker container

.. code-block:: bash

   curl http://localhost:<host_port>/ --header 'content-type: application/json' --data '<data>' -X POST

5. Validate the response from Terminal

See more detail on docker CLI: https://docs.docker.com/engine/reference/commandline/cli/


When to use quick-test Vs docker CLI
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
cobot quick-test:

Pros: Fast

Cons: You need to install all the dependencies in requirements.txt

docker CLI:

Pros: Install all the dependencies automatically and tests more closed to actual ECS service

Cons: Slow, specifically for the first time when you need to build a docker image.

---------------------------------
End-to-End Local Integration Test
---------------------------------

It's always good practice to test Lambda function code locally before you push to CodeCommit Repository. With ``cobot local-test`` and ``cobot interactive-mode`` CLIs, you can validate responses from Lambda function for a sample multi-turn conversation locally.

There are two types of docker modules:
* Remote docker service is service already running on EC2 hosts through CodePipeline, whose URL can be found from Lambda function's environment variables on the Lambda function console.
* Local docker service is service which will run on your local machine, you need to build the DockerFile and run the docker container and terminate it once the test is done. (our local_test.py already handles this for you)

If you change some Flask-based docker service modules and want to test it with local Lambda function, there is one caveat.

**Caveat**:
You may not run more than 5 local docker services on one machine, the exact maximal docker service number depends on your machine's memory/disk setting and each docker service's size.
If it runs out of memory or disk space, consider using remote docker services.

Prerequisites
-------------

1. Install and setup Docker: https://docs.docker.com/install/

2. Several python packages need to be installed in your local environment
before using the local test python script:

-  injector==0.12.1
-  requests==2.14.2
-  boto3

.. code-block:: bash

   python3 -m pip install injector==0.12.1
   python3 -m pip install requests==2.14.2
   python3 -m pip install boto3

.. _local_test:

cobot local-test CLI
^^^^^^^^^^^^^^^^^^^^
Steps
-----

1. Set up COBOT_HOME if you haven't done it. See `Getting Started Installation`__ Step 4

.. __: getting_started.html#installation

2. Create a sample events json file which contains multi-turn ASK requests.
   see bin/test_event.json as an example. You can copy each ASK request from ASK Test console.
   We are working on to support testing multi-turn utterances (texts) directly, rather than multi-turn ASK requests.

3. Configure local/remote service modules' config.
   see bin/test_service_module_config.json as an example.

   * **For local service module, configure its unique host_port and Dockerfile's directory. Each local service must have different host_ports**.
   * **For remote service module, configure its url**.

4. If you want to test the lambda function locating at sample_cobot.non_trivial_bot (don't include .py), simple run cobot CLI from  anywhere, as long as your COBOT_HOME is setup correctly. Please make sure your API_KEY is set in your non_trivial_bot.

.. code-block:: bash

   cobot local-test sample_cobot.non_trivial_bot bin/test_event_local_test_basic_example.json bin/test_service_module_config.json bin/test_conversation_file.txt

.. code-block:: bash

   usage: cobot local-test <lambda_handler_path> <events_file> <config_file> <output_file>

   Options:
   <lambda_handler_path> is the lambda handler class, i.e. sample_cobot.non_trivial_bot. Note: use the relative class path with regard to COBOT_HOME,
   <events_file> is the ASK request events json file relative to COBOT_HOME,
   <config_file> is the service module json file relative to COBOT_HOME.
      For remote service module, it configures each module\'s url, which can be found from Lambda function\'s environment variables.
      For local service module, it configures each module\'s unique host_port and Dockerfile\'s directory.
   <output_file> is the output txt file relative to COBOT_HOME, for each event in the events_file, it will have 2 output lines, where one line is ASR (request text), and another line is TTS (response text).

**Note**:

1. In the config_file, the service_module_name must be the same as the one you specified in the modules.yaml.

2. For the first local-test run, it will take several minutes to build and run local docker modules. But afterwards, it will take less than 1 minute.

3. If you look at the output file after the run, the ASR line will be

-  the highest confident ASR text if your skill is whitelisted for n-best ASR feature
-  some specific (topic, text, all-text, question_word, question_text) AMAZON.RAW_TEXT slot values
-  "None" for LaunchRequest without n-best ASR feature
-  GlobalIntent (STOPIntent, CancelIntent, etc)

.. _local_test_log:

Logging and Terminal Outputs
----------------------------

1. Cobot local-test CLI prints out all the warnings and preliminary validation results on Terminal.

.. image:: images/local_test_basic_example.png

2. Cobot local-test CLI logs all the messages (which have severity level of INFO or higher) to ``local_test_logs`` directory in your Cobot directory.
All local testing scripts and lambda function logs are in ``local_test_logs.log`` file. Log filenames with ``_container.log`` suffix are from local docker modules.
After each turn, new logs will be appended to the same log files.

3. Cobot local-test CLI adds preliminary input and output validations to sample conversations.
You can find the default validation logic at ``COBOT_HOME/bin/local_test_validate.py``.
Feel free to override validate function (local_test_validate.py Line 15) to accommodate your use cases.

4. If you are seeing timeout while spinning up all the services locally,
consider changing number of gunicorn workers to 1. You can find config in sample_cobot/docker/<module>/config/supervisord.conf.
Change -w from 9 to 1. Please remember to change it back to 9 for prod use case for better performance.

.. code-block:: bash

    command=/usr/bin/gunicorn app:app -w 1 -b localhost:5001 --log-level info

.. _interactive_mode:

cobot interactive-mode CLI
^^^^^^^^^^^^^^^^^^^^^^^^^^

You can test your CoBot via an interactive mode, as well. This allows you to interact with your CoBot locally on a turn-by-turn basis, via text.

Prerequisites
-------------
Interactive mode uses ASK CLI to interact with ASK.
Please ensure COBOT_HOME is correctly set. You can run which cobot to confirm it is pointing to the your codebase.
Please run following command for ask authorization.

.. code-block:: bash

   ask init

Steps
-----
To begin interactive mode, use the following command:

.. code-block:: bash

    cobot interactive-mode -c <path-to-config.yaml> -l <lambda-handler> -s [path-to-service-module-config] -u [user id]

There are 4 required parameters.

 ``-c`` should be the path to your CoBot's config.yaml file.
 ``-l`` should be your main CoBot's Lambda handler. ``-s`` should be path to service module json file.
 ``-u`` is the user id in alexa request which will be sent to lambda handler by event parameter. It can be any unique constant string.
To exit interactive mode, simply give "stop" as a response

**Important Note:** Please note that interactive mode calls your ASK skill which is backed by the remote cobot lambda handler in order to generate the input for the local cobot lambda handler. It makes one invocation to your remote lambda handler and can create two records in the DynamoDB table.

**Important Note:** Please change the invocation name and skill id in your config.yaml to be same as your skill's invocation name in ask console. Also check you have the right interaction model deployed in your ASK console.


Non Trivial Bot
--------------

.. code-block:: bash

     cobot interactive-mode -c sample_cobot/config.yaml -l sample_cobot.non_trivial_bot  -s bin/test_service_module_non_trivial_bot_config.json

Example configuration is provided in test_service_module_non_trivial_bot.json.

.. code-block:: bash

        {
            "nlp": {
                "port": "3000",
                "directory": "cobot_common/docker/nlp"
            },
            "greeter": {
                "port": "4000",
                "directory": "sample_cobot/docker/greeter"
            },
            "retrieval": {
                "port": "5000",
                "directory": "sample_cobot/docker/retrieval"
            }
        }

Baseline Bot
------------
.. code-block:: bash

     cobot interactive-mode -c sample_cobot/config.yaml -l sample_cobot.baseline_bot  -s bin/test_service_module_baseline_bot_config.json

Example configuration is provided in test_service_module_baseline_bot.json.

.. code-block:: bash

        {
            "nlp": {
                "url": "<CHANGEME>"
            },
            "asrcorr": {
                "url": "<CHANGEME>"
            },
            "coref": {
                "url": "<CHANGEME>"
            },
            "nounphrases": {
                "url": "<CHANGEME>"
            }
        }

**Important Note:** Please change your remote module's urls from your lambda's environment as shown in the image.

.. image:: images/baseline_bot_remote_url_config.png


.. _interactive_mode_log:

Logging
-------

All log files are moved to ``interactive_mode_logs`` directory in your Cobot directory. All interactive mode scripts and lambda function logs are in ``interactive_mode.log`` file. Log filenames with ``_container.log`` suffix are from local docker modules. After each turn, new logs will be appended to the same log files.


When to use cobot interactive-mode vs cobot local-test
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Both CLIs test your local Lambda function and Docker modules end-to-end,
the key difference is that interactive-mode can work with plain text as the input, while local-test currently needs ASK request (JSON) as the input.

If you want to automate your integration test, we will recommend you to use cobot local-test.

If you want to interactively integration test your local socialbot's experience, we will recommend you to use cobot interactive-mode.

.. _ask_simulate_cli:

----------------
ASK Simulate CLI
----------------

This test simulates a skill and can test the remote cobot lambda handler. See `ask simulate CLI <https://developer.amazon.com/docs/smapi/ask-cli-command-reference.html#simulate-command>`_ for details about this command.

In order to invoke the skill, use "open <invocation_name> and <utterance>" as the input text.

*Note: N-best ASR is not enabled for the skill when running this command.*

Prerequisites
-------------

Enable the skill for beta testing following `this instruction <https://developer.amazon.com/docs/custom-skills/skills-beta-testing-for-alexa-skills.html#h2_create-test-for-skill>`_.
