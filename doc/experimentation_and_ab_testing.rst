.. _experimentation_and_ab_testing:

Experimentation and A/B Testing
===============================

You can use the Cobot Toolkit to run A/B tests on alternately configured CobotHandlers (each with their own set of components) within a single Cobot deployment.
We currently support A/B testing for the following components: Dialog Manager, Feature Extractor, ASR Processor, Selecting Strategy, Ranking Strategy, Response Generator, and NLP Module, in any combination.
Service modules in an A/B test can be either local or remote.

-----------------------
Configuring A/B Testing
-----------------------
To add an A/B test to your lambda handler: import the ABTest class, create a new AB Test inside lambda_handler, and run the test.

Replace `<APP_ID>` with your ASK skill's application id, `<API_KEY>` with your team's API key.

.. code-block:: python

    from cobot_core import ABTest
    def lambda_handler(event, context):
        # Create a new AB Test
        abtest = ABTest(event=event,
                        context=context,
                        app_id='<APP_ID>',
                        user_table_name='UserTable',
                        save_before_response=True,
                        state_table_name='StateTable',
                        level='conversation',
                        test_case_config=test_case_config,
                        api_key='<API_KEY>'
                        )
        # Run the AB Test and return the result
        return abtest.run()

* level (String) can be either 'conversation' or 'utterance' to designate A/B testing at the conversation level or utterance level.
* test_case_config (List[Dict]) is a list of configurations that define the components in each A/B test case. Each configuration is a dictionary with keys 'name', 'ratio', 'overrides', 'response_generators', and 'nlp_modules'.

    * name (String) is the name of the test case. This name will appear in the state table under the a_b_test column when the corresponding test case is selected. The names of all test cases should be unique. If no name is provided, a default name is chosen from A-Z.
    * ratio (Int) is the percent of traffic to be routed to the test case. The ratio must be an integer in the range of [0,100]. The ratios for all test cases should sum to 100. The ratio is a required input.
    * overrides (Function) contains custom overrides for dialog manager components. If no overrides are provided, the default dialog manager components are used.
    * response_generators (List[Dict]) contains a list of response generator definitions for the test case. If no response generators are provided, no response generators will be added to the test case.
    * nlp_modules (List[List[Dict]]]) contains a list of lists of nlp module definitions. Modules in a previous list run before modules in a later list: [['a', 'b'],['c']] => module a and b are run in parallel before module c,and module c can have access to module a and b's outputs. To include one of the built-in modules, add a definition with only the 'name' key. All nlp module definitions must contain the 'name' key. If no nlp modules are provided, the default modules [[“intent”, “ner”, “sentiment”, “topic”]] will be used.

For more details on the other parameters, see `the cobot_core ABTest documentation`__.

.. __: cobot_core.html#module-cobot_core.abtest

-------------
Local Testing
-------------
For simple examples of how to use the ABTest class, see the demos in example/a_b_test. Each demo can be run in lambda by using the sample_cobot/sample_events/existing_session_event as the test event. The demos can also be run locally by following the workflow defined in the `Testing Cobot Components`__ documentation. Example command line invocations for each demo are provided below:

.. __: testing_cobot_components.html#end-to-end-local-lambda-function-tests-with-remote-local-service-modules

.. code-block:: bash

    cobot local-test example.a_b_test.demo_ab_dialog_manager bin/test_event.json bin/test_service_module_config.json bin/test_conversation_file.txt
    cobot local-test example.a_b_test.demo_ab_response_generator bin/test_event.json bin/test_service_module_config.json bin/test_conversation_file.txt
    cobot local-test example.a_b_test.demo_ab_nlu_module bin/test_event.json bin/test_service_module_config.json bin/test_conversation_file.txt
    cobot local-test example.a_b_test.demo_ab_multivariate bin/test_event.json bin/test_service_module_config.json bin/test_conversation_file.txt

The demo for A/B testing a Dialog Manager can also be adapted to test a Feature Extractor, ASR Processor, Selecting Strategy, and Ranking Strategy. To test these components in combination, just add them to the overrides:

.. code-block:: python

    # Add dialog manager components to overrides for Cobot A
    def overrides_a(binder):
        binder.bind(DialogManager, to=CustomDialogManager_a)
        binder.bind(Analyzer, to=FeatureExtractor_a)

The demos for Dialog Manager, Response Generator, and NLP Module can also be combined in the same file. Simply add all desired components to the test case config:

.. code-block:: python

    # Create config for test case A
    config_a = {'name': 'A',
                'ratio': 50,
                'overrides': overrides_a,
                'response_generators': response_generators_a,
                'nlp_modules': nlp_modules_a}

-----------------------------------
Testing Multiple Bot Configurations
-----------------------------------

To test more than two bot configurations, create a config for each test case and add it to the list of configs.
Specify a ratio for each test case and be sure the total ratios sum to 100.
An example multivariate A/B test is shown below and in example/a_b_test/demo_ab_multivariate.

Replace `<APP_ID>` with your ASK skill's application id, `<API_KEY>` with your team's API key.

.. code-block:: python

    import logging
    from cobot_core.service_module import LocalServiceModule
    from cobot_core import ABTest


    # Add your custom Response Generator A here
    class CustomResponseGenerator_a(LocalServiceModule):
        def execute(self):
            return "custom response generator a"


    # Add your custom Response Generator B here
    class CustomResponseGenerator_b(LocalServiceModule):
        def execute(self):
            return "custom response generator b"


    # Add your custom Response Generator C here
    class CustomResponseGenerator_c(LocalServiceModule):
        def execute(self):
            return "custom response generator c"


    def lambda_handler(event, context):
        # Add the definition for Response Generator A here
        ResponseGenerator_a = {
            'name': "RG_A",
            'class': CustomResponseGenerator_a,
            'url': 'local'
        }

        # Add the definition for Response Generator B here
        ResponseGenerator_b = {
            'name': "RG_B",
            'class': CustomResponseGenerator_b,
            'url': 'local'
        }

        # Add the definition for Response Generator C here
        ResponseGenerator_c = {
            'name': "RG_C",
            'class': CustomResponseGenerator_c,
            'url': 'local'
        }

        # Add Response Generator A to list of response generator definitions for Cobot A
        response_generators_a = [ResponseGenerator_a]

        # Add Response Generator B to list of response generator definitions for Cobot B
        response_generators_b = [ResponseGenerator_b]

        # Add Response Generator C to list of response generator definitions for Cobot C
        response_generators_c = [ResponseGenerator_c]

        # Create config for test case A
        config_a = {'name': 'A',
                    'ratio': 33,
                    'response_generators': response_generators_a}

        # Create config for test case B
        config_b = {'name': 'B',
                    'ratio': 33,
                    'response_generators': response_generators_b}

        # Create config for test case C
        config_c = {'name': 'C',
                    'ratio': 34,
                    'response_generators': response_generators_c}

        # Create list of configs for each test case
        test_case_config = [config_a, config_b, config_c]

        # Create a new AB Test with config_a, config_b, and config_c
        abtest = ABTest(event=event,
                        context=context,
                        app_id='<APP_ID>',
                        user_table_name='UserTable',
                        save_before_response=True,
                        state_table_name='StateTable',
                        test_case_config=test_case_config,
                        level='conversation',
                        api_key='<API_KEY>'
                        )

        # Run the AB Test and return the result
        return abtest.run()

-------------------------------
Viewing A/B Test Configurations
-------------------------------
When you run an A/B test, the configuration will be stored in a DynamoDB table named ABConfigTable for easy viewing.
For each test case, the table contains the test name, the NLP module definitions, the overrides, the rotation ratio, and the response generator definitions.
If you run a new A/B test with the same test case names, the old configuration will be overwritten.
To avoid overwriting old configurations, change the 'name' key in the test case config to something new.
An example view of the ABConfigTable is shown below.

.. image:: images/ab_config_table.png

