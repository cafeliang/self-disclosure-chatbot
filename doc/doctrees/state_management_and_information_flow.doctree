���Y      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�*.. _state_management_and_information_flow:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��%state-management-and-information-flow�u�tagname�h	�line�K�parent�hhh�source��G/Users/raeferg/cobot_base/doc/state_management_and_information_flow.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�%State Management and Information Flow�h]�h �Text����%State Management and Information Flow�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh#)��}�(hhh]�(h()��}�(h�Accessing State Information�h]�h.�Accessing State Information�����}�(hh>hh<hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh9hhh h!hK	ubh �	paragraph���)��}�(hX.  State Manager is the core class that provides fast, transparent state access. Generally, state information is stored in-memory, loaded at the beginning of each turn of interaction, and stored at the end of the turn for persistence. The persistent store that underlies the state management system is DynamoDB, a very fast key-value datastore provided in AWS. This model of state management ensures low latency, consistent and reliable data, and is flexible, allowing you to add additional key/value pairs to the State information from your own custom modules.�h]�h.X.  State Manager is the core class that provides fast, transparent state access. Generally, state information is stored in-memory, loaded at the beginning of each turn of interaction, and stored at the end of the turn for persistence. The persistent store that underlies the state management system is DynamoDB, a very fast key-value datastore provided in AWS. This model of state management ensures low latency, consistent and reliable data, and is flexible, allowing you to add additional key/value pairs to the State information from your own custom modules.�����}�(hhNhhLhhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubhK)��}�(h�6CoBot's State Manager stores the following properties:�h]�h.�8CoBot’s State Manager stores the following properties:�����}�(hh\hhZhhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubh �enumerated_list���)��}�(hhh]�(h �	list_item���)��}�(h��current_state: State, see state.py_ for all the default attributes. These attributes will be added in the Cobot NLP Pipeline and Dialog Manager: input_offensive, topic, ner, sentiment, candidate_responses.�h]�hK)��}�(hhqh]�(h.�current_state: State, see �����}�(h�current_state: State, see �hhsubh �	reference���)��}�(h�	state.py_�h]�h.�state.py�����}�(hhhh}ubah}�(h]�h]�h]�h]�h]��name��state.py��refuri��'cobot_core.html#module-cobot_core.state�uhh{hhs�resolved�Kubh.�� for all the default attributes. These attributes will be added in the Cobot NLP Pipeline and Dialog Manager: input_offensive, topic, ner, sentiment, candidate_responses.�����}�(h�� for all the default attributes. These attributes will be added in the Cobot NLP Pipeline and Dialog Manager: input_offensive, topic, ner, sentiment, candidate_responses.�hhsubeh}�(h]�h]�h]�h]�h]�uhhJh h!hKhhoubah}�(h]�h]�h]�h]�h]�uhhmhhjhhh h!hNubhn)��}�(h�session_history: List<Dict>�h]�hK)��}�(hh�h]�h.�session_history: List<Dict>�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhhmhhjhhh h!hNubhn)��}�(h�last_state: Dict�h]�hK)��}�(hh�h]�h.�last_state: Dict�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhhmhhjhhh h!hNubhn)��}�(h�Uuser_attributes: UserAttributes, refer to user_attributes.py_ for all the attributes
�h]�hK)��}�(h�Tuser_attributes: UserAttributes, refer to user_attributes.py_ for all the attributes�h]�(h.�*user_attributes: UserAttributes, refer to �����}�(h�*user_attributes: UserAttributes, refer to �hh�ubh|)��}�(h�user_attributes.py_�h]�h.�user_attributes.py�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��name��user_attributes.py�h��=cobot_python_sdk.html#module-cobot_python_sdk.user_attributes�uhh{hh�h�Kubh.� for all the attributes�����}�(h� for all the attributes�hh�ubeh}�(h]�h]�h]�h]�h]�uhhJh h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhhmhhjhhh h!hNubeh}�(h]�h]�h]�h]�h]��enumtype��arabic��prefix�h�suffix��.�uhhhhh9hhh h!hKubhK)��}�(h��The current_state, last_state, and session history all reference to information related to the current turn and previous turns in this user interaction, respectively.�h]�h.��The current_state, last_state, and session history all reference to information related to the current turn and previous turns in this user interaction, respectively.�����}�(hj  hj	  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubhK)��}�(h��The user_attributes value can be used to store cross-turn and cross-interaction information about a user, their likes, wants or preferences.�h]�h.��The user_attributes value can be used to store cross-turn and cross-interaction information about a user, their likes, wants or preferences.�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubh
)��}�(h�5.. _state.py: cobot_core.html#module-cobot_core.state�h]�h}�(h]��state-py�ah]�h]��state.py�ah]�h]�h�h�uhh	hKhh9hhh h!�
referenced�Kubh
)��}�(h�U.. _user_attributes.py: cobot_python_sdk.html#module-cobot_python_sdk.user_attributes�h]�h}�(h]��user-attributes-py�ah]�h]��user_attributes.py�ah]�h]�h�h�uhh	hKhh9hhh h!j1  Kubeh}�(h]��accessing-state-information�ah]�h]��accessing state information�ah]�h]�uhh"hh$hhh h!hK	ubh#)��}�(hhh]�(h()��}�(h�Standard Keys�h]�h.�Standard Keys�����}�(hjK  hjI  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hjF  hhh h!hKubhK)��}�(h��The standard keys most commonly referenced throughout the documentation and example code are 'intent', 'slots', 'ner', 'sentiment', 'asr', 'text'.�h]�h.��The standard keys most commonly referenced throughout the documentation and example code are ‘intent’, ‘slots’, ‘ner’, ‘sentiment’, ‘asr’, ‘text’.�����}�(hjY  hjW  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhjF  hhubhK)��}�(hX;  These key values are set by FeatureExtractor and the NLP pipeline. 'text' is a special value - this is extracted from the n-best ASR values ('asr') if they are available. If not, it will check the current slots to see if any match an Amazon.TEXT or Amazon.LITERAL slot type in the current intent model definition. If so, that will be used as the 'text' value. In case you are using those slots in another way and you are not whitelisted for n-best ASR access, the 'text' key may not contain what you expect. If none of those values are present, then 'text' will be empty.�h]�h.XO  These key values are set by FeatureExtractor and the NLP pipeline. ‘text’ is a special value - this is extracted from the n-best ASR values (‘asr’) if they are available. If not, it will check the current slots to see if any match an Amazon.TEXT or Amazon.LITERAL slot type in the current intent model definition. If so, that will be used as the ‘text’ value. In case you are using those slots in another way and you are not whitelisted for n-best ASR access, the ‘text’ key may not contain what you expect. If none of those values are present, then ‘text’ will be empty.�����}�(hjg  hje  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK!hjF  hhubeh}�(h]��standard-keys�ah]�h]��standard keys�ah]�h]�uhh"hh$hhh h!hKubh#)��}�(hhh]�(h()��}�(h�$Getting a Reference to State Manager�h]�h.�$Getting a Reference to State Manager�����}�(hj�  hj~  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj{  hhh h!hK&ubhK)��}�(h�6**When your class is not a subclass of ServiceModule**�h]�h �strong���)��}�(hj�  h]�h.�2When your class is not a subclass of ServiceModule�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhJh h!hK(hj{  hhubhK)��}�(h�4These include RankingStrategy and SelectingStrategy.�h]�h.�4These include RankingStrategy and SelectingStrategy.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK*hj{  hhubhK)��}�(h�YThese classes will have state manager in the constructor by inheriting from parent class.�h]�h.�YThese classes will have state manager in the constructor by inheriting from parent class.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK,hj{  hhubh �literal_block���)��}�(hX�  class CustomRankingStrategy(Cobot.RankingStrategy):

    def rank(self, responses):
        # Get current state's attribute: i.e.
        intent = self.state_manager.current_state.intent
        # Get session history's attributes: i.e.
        past_topics = [turn.get('topic', None) for turn in self.state_manager.session_history]
        # Get last state's attributes: i.e.
        last_response = self.state_manager.last_state.get('response', None) if self.state_manager.last_state is not None else None
        # Get user attributes: i.e.
        user_topic_preference = self.state_manager.user_attributes.favorite_topic
        print(intent, past_topics, last_response, user_topic_preference)
        return responses[0]�h]�h.X�  class CustomRankingStrategy(Cobot.RankingStrategy):

    def rank(self, responses):
        # Get current state's attribute: i.e.
        intent = self.state_manager.current_state.intent
        # Get session history's attributes: i.e.
        past_topics = [turn.get('topic', None) for turn in self.state_manager.session_history]
        # Get last state's attributes: i.e.
        last_response = self.state_manager.last_state.get('response', None) if self.state_manager.last_state is not None else None
        # Get user attributes: i.e.
        user_topic_preference = self.state_manager.user_attributes.favorite_topic
        print(intent, past_topics, last_response, user_topic_preference)
        return responses[0]�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve��language��python��linenos���highlight_args�}�uhj�  h h!hK.hj{  hhubhK)��}�(h�2**When your class is a subclass of ServiceModule**�h]�j�  )��}�(hj�  h]�h.�.When your class is a subclass of ServiceModule�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhJh h!hK>hj{  hhubhK)��}�(h�FLocal response generators and NLP modules inherit from Service Module.�h]�h.�FLocal response generators and NLP modules inherit from Service Module.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK@hj{  hhubhK)��}�(hXm  In the service config dictionary, you can specify a list of n history_turns' context manager keys.
As seen in the EviBot example, it copies current state's intent, slots, and text from the state manager to EviResponseGeneratorFromService's input_data. You can then access them from input_data, see EviResponseGeneratorFromService's's execute() method for reference.�h]�h.Xw  In the service config dictionary, you can specify a list of n history_turns’ context manager keys.
As seen in the EviBot example, it copies current state’s intent, slots, and text from the state manager to EviResponseGeneratorFromService’s input_data. You can then access them from input_data, see EviResponseGeneratorFromService’s‘s execute() method for reference.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKBhj{  hhubhK)��}�(h�wcontext_manager_keys: a list of attributes in the State, if absent, *all the attributes* will be included in input_data�h]�(h.�Dcontext_manager_keys: a list of attributes in the State, if absent, �����}�(h�Dcontext_manager_keys: a list of attributes in the State, if absent, �hj  hhh NhNubh �emphasis���)��}�(h�*all the attributes*�h]�h.�all the attributes�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhj  hj  ubh.� will be included in input_data�����}�(h� will be included in input_data�hj  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hKEhj{  hhubhK)��}�(h��history_turns: how many historical turns data for the same session, if absent, *0* historical turn will be included in input_data.�h]�(h.�Ohistory_turns: how many historical turns data for the same session, if absent, �����}�(h�Ohistory_turns: how many historical turns data for the same session, if absent, �hj/  hhh NhNubj  )��}�(h�*0*�h]�h.�0�����}�(hhhj8  ubah}�(h]�h]�h]�h]�h]�uhj  hj/  ubh.�0 historical turn will be included in input_data.�����}�(h�0 historical turn will be included in input_data.�hj/  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hKGhj{  hhubhK)��}�(h��If history_turns = 0, input_data: Dict<String, String>, where key is each context_manager_key
Else if history_turns > 0, input_data: Dict<String, List<String>>, where key is each context_manager_key�h]�h.��If history_turns = 0, input_data: Dict<String, String>, where key is each context_manager_key
Else if history_turns > 0, input_data: Dict<String, List<String>>, where key is each context_manager_key�����}�(hjS  hjQ  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKIhj{  hhubj�  )��}�(hX�  EviBot = {
        'name': "EVI",
        'class': EviResponseGeneratorFromService,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text'],
        'history_turns': 0
    }

class EviResponseGeneratorFromService(ToolkitServiceModule):
    def execute(self):
        isWhitelisted = False
        if not isWhitelisted:
            intent = self.input_data['intent']
            slots = self.input_data['slots']
            question_word = slots['question_word']['value']
            question_text = question_word + ' ' + slots['question_text']['value']
        else:
            question_text = self.input_data['text']

        print('Evi question_text: ', question_text)
        r = self.toolkit_service_client.get_answer(question=question_text)
        result = ''
        if 'response' in r:
            result = r['response']
        elif 'message' in r:
            result = r['message']
        print('Evi response: ', result)
        return result�h]�h.X�  EviBot = {
        'name': "EVI",
        'class': EviResponseGeneratorFromService,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text'],
        'history_turns': 0
    }

class EviResponseGeneratorFromService(ToolkitServiceModule):
    def execute(self):
        isWhitelisted = False
        if not isWhitelisted:
            intent = self.input_data['intent']
            slots = self.input_data['slots']
            question_word = slots['question_word']['value']
            question_text = question_word + ' ' + slots['question_text']['value']
        else:
            question_text = self.input_data['text']

        print('Evi question_text: ', question_text)
        r = self.toolkit_service_client.get_answer(question=question_text)
        result = ''
        if 'response' in r:
            result = r['response']
        elif 'message' in r:
            result = r['message']
        print('Evi response: ', result)
        return result�����}�(hhhj_  ubah}�(h]�h]�h]�h]�h]�j�  j�  j�  �python�j�  �j�  }�uhj�  h h!hKMhj{  hhubeh}�(h]��$getting-a-reference-to-state-manager�ah]�h]��$getting a reference to state manager�ah]�h]�uhh"hh$hhh h!hK&ubh#)��}�(hhh]�(h()��}�(h�0Accessing State Information From a Remote Module�h]�h.�0Accessing State Information From a Remote Module�����}�(hj|  hjz  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hjw  hhh h!hKnubhK)��}�(hXU  Remote modules do not currently have direct access to State Manager. Rather, they receive a subset of the state information from state manager in their remote calls. This subset is determined from the list of "context_manager_keys" in their remote service module definition, which is passed into add_response_generators() or upsert_module().�h]�h.XY  Remote modules do not currently have direct access to State Manager. Rather, they receive a subset of the state information from state manager in their remote calls. This subset is determined from the list of “context_manager_keys” in their remote service module definition, which is passed into add_response_generators() or upsert_module().�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKphjw  hhubhK)��}�(h��For example, if the context_manager_keys provided are "intent", "slots", and "text", the input sent to the remote service module via POST will look like:�h]�h.��For example, if the context_manager_keys provided are “intent”, “slots”, and “text”, the input sent to the remote service module via POST will look like:�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKrhjw  hhubj�  )��}�(h��{
   "text":"what do you think about roger federer"
   "intent":"retrieval",
   "slots":{
      "text":{
         "name":"text",
         "value":"what do you think about roger federer"
      }
   }
}�h]�h.��{
   "text":"what do you think about roger federer"
   "intent":"retrieval",
   "slots":{
      "text":{
         "name":"text",
         "value":"what do you think about roger federer"
      }
   }
}�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  j�  �bash�j�  �j�  }�uhj�  h h!hKthjw  hhubeh}�(h]��0accessing-state-information-from-a-remote-module�ah]�h]��0accessing state information from a remote module�ah]�h]�uhh"hh$hhh h!hKnubeh}�(h]�(h�id1�eh]�h]�(�%state management and information flow��%state_management_and_information_flow�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�j�  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�N�character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}�(�state.py�]�h}a�user_attributes.py�]�h�au�refids�}�h]�has�nameids�}�(j�  hj�  j�  jC  j@  j.  j+  j;  j8  jx  ju  jt  jq  j�  j�  u�	nametypes�}�(j�  �j�  NjC  Nj.  �j;  �jx  Njt  Nj�  Nuh}�(hh$j�  h$j@  h9j+  j%  j8  j2  ju  jF  jq  j{  j�  jw  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�(h �system_message���)��}�(hhh]�(hK)��}�(h�Title underline too short.�h]�h.�Title underline too short.�����}�(hhhjM  ubah}�(h]�h]�h]�h]�h]�uhhJhjJ  ubj�  )��}�(h�3State Management and Information Flow
=============�h]�h.�3State Management and Information Flow
=============�����}�(hhhj[  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hjJ  ubeh}�(h]�h]�h]�h]�h]��level�K�type��WARNING��line�K�source�h!uhjH  hh$hhh h!hKubjI  )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhjw  ubah}�(h]�h]�h]�h]�h]�uhhJhjt  ubj�  )��}�(h�?-----------------
Accessing State Information
-----------------�h]�h.�?-----------------
Accessing State Information
-----------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hjt  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jq  �line�K�source�h!uhjH  hh9hhh h!hK	ubjI  )��}�(hhh]�(hK)��}�(hhh]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubj�  )��}�(h�H-----------------
Getting a Reference to State Manager
-----------------�h]�h.�H-----------------
Getting a Reference to State Manager
-----------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hj�  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jq  �line�K$�source�h!uhjH  ubjI  )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubj�  )��}�(h�H-----------------
Getting a Reference to State Manager
-----------------�h]�h.�H-----------------
Getting a Reference to State Manager
-----------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hj�  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jq  �line�K$�source�h!uhjH  hj{  hhh h!hK&ubjI  )��}�(hhh]�(hK)��}�(hhh]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubj�  )��}�(h�T-----------------
Accessing State Information From a Remote Module
-----------------�h]�h.�T-----------------
Accessing State Information From a Remote Module
-----------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hj�  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jq  �line�Kl�source�h!uhjH  ubjI  )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhJhj  ubj�  )��}�(h�T-----------------
Accessing State Information From a Remote Module
-----------------�h]�h.�T-----------------
Accessing State Information From a Remote Module
-----------------�����}�(hhhj'  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hj  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jq  �line�Kl�source�h!uhjH  hjw  hhh h!hKnube�transform_messages�]�jI  )��}�(hhh]�hK)��}�(hhh]�h.�KHyperlink target "state-management-and-information-flow" is not referenced.�����}�(hhhjD  ubah}�(h]�h]�h]�h]�h]�uhhJhjA  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�KuhjH  uba�transformer�N�
decoration�Nhhub.