.. _debugging_and_log_access:

Debugging And Log Access
========================

.. _use_cloudwatch_for_log_access:

-------------------------------
Using CloudWatch for Log Access
-------------------------------

CloudWatch Logs provides monitoring, storage and access to your logs from across your Cobot components. That means you can get logs from your Lambda, infrastructure modules, and other Cobot modules in one place. This makes debugging complicated issues much easier.

.. image:: cloudwatch-logs.png

Your Lambda logs will show up as a log group named /aws/lambda/<botname>-lambda, and your Docker module service logs will show up as log group named <stackname>-DeploymentPipeline-<pipelineid>-<modulename>-Service-container-log. These are the most commonly used logs for tracking and diagnosing issues with your Lambda and main Docker modules. In each log group, there will be multiple log streams, grouped by time - the most recent logs will appear first.

You can configure your Cobot's logging level in the cobot instance, by default all the logging messages which have severity level of INFO or higher will be emitted to CloudWatch.
You can find logging level list at https://docs.python.org/3/library/logging.html#levels.

To override your Cobot's logging level to DEBUG, update cobot_core/log/logging.conf file:

::

    [logger_root]
    level=DEBUG
    formatter_prefix=conversation_id,session_id

By default, it adds conversation_id and session_id to each CloudWatch log message, for easy query and access.
To add custom information to each log message (i.e. user_id), update ``cobot_core/log/logging.conf`` file:

::

    [logger_root]
    level=INFO
    formatter_prefix=conversation_id,session_id,user_id


Note: Custom information (i.e. app_id, user_id, conversation_id) must exist in event or state manager's current state (i.e. intent), otherwise, it will be "".

If you want to log messages in your custom class within CoBot, initialize the logger class as below

.. code-block:: python

    @inject
    def __init__(self):
        self.logger = LoggerFactory.setup(self)

    def method_a():
        self.logger.info('INFO log')

.. _transcriber:

--------------------------
Using the cobot transcribe
--------------------------
The CoBot CLI comes with a transcription service that will let you retrieve a conversation from your DynamoDB state table. On first run, it will create one or two indexes of your state table, allowing you to search for a conversation by session_id and conversation_id. To invoke the transcriber, use the following command:

.. code-block:: bash

    cobot transcribe -t <state-table-name>  -s [session-id] -c [conversation-id] [verbosity]

    usage: cobot transcribe -t <state-table-name> -s [session-id] -c [conversation-id] [verbosity]
    Options:
    <state-table-name> is name of the DynamoDB state table
    [session-id] is the session (defaults to latest session)
    [conversation-id] is the conversation id (defaults to latest conversation)
    [verbosity], the optional verbosity level (-v, -vv)

There is 1 required parameter.``-t`` should be the name of the DynamoDB table in which your state is stored, usually *StateTable*.

``-s`` is the session ID of the conversation, ``-c`` is the conversation ID of the conversation. Only one of them is required to transcribe a conversation, but not both.
If you know the session ID of the conversation you wish to view, pass it with the ``-s`` parameter, or use ``-s latest`` to pull the most recent conversation.
If you know the conversation ID of the conversation you wish to view, pass it with the ``-c`` parameter, or use ``-c latest`` to pull the most recent conversation.

Finally, the transcriber has a verbosity setting. Passing no verbosity parameter simply prints the user utterance and your CoBot response. A single `-v` also prints the intent and candidate responses for the response. A `-vv` prints the full JSON object for each turn in the conversation.

.. _query_cloudwatch_logs:

---------------------
Query CloudWatch Logs
---------------------

aws logs filter-log-events CLI supports listing CloudWatch logs from the specified log group.
You can list all the log events or filter the results using a filter pattern, a time range, and the name of the log stream. For detail, refer to https://docs.aws.amazon.com/cli/latest/reference/logs/filter-log-events.html

**Important Note**: If filter-pattern contains '-', remember to include the filter-pattern within '"<filter-pattern>"'. "" will treat <filter-pattern> as a string, rather than treat "-" as "exclude" a term in log filter syntax.

Here are some useful commands:

::

    1. If you want to query logs for one conversation_id:
        aws logs filter-log-events --log-group-name /aws/lambda/cobot-lambda --filter-pattern  '<conversation_id>'

    2. If you want to query logs for one session_id:
        aws logs filter-log-events --log-group-name /aws/lambda/cobot-lambda --filter-pattern  '"<session_id>"'

    3. If you want to filter out all the loadtest conversations:
        aws logs filter-log-events --log-group-name /aws/lambda/cobot-lambda --filter-pattern  '- loadtest'

    4. If you want to print out the output in text:
        aws logs filter-log-events --log-group-name /aws/lambda/cobot-lambda --filter-pattern  '"<session_id>"' --output text

--------------------------------
Module Deployment Issues and ECS
--------------------------------

There are separate logs related to the build and deployment process to be found in your Cloudwatch console.

There is a CodeBuild log for each module in your CodePipeline, so you can see if there are errors in any of the build stages in your pipeline. These will be named /aws/codebuild/<stackname>-DeploymentPipeline-<pipelineid>-<componentname>, where component name is Skill, LambdaFunction or your Docker module name. You should only need to reference these if you see an error in a Build stage in CodePipeline.

Debugging Docker and ECS issues can be challenging. Cloudwatch makes this easier - ecs-agent and docker logs are present for each ECS cluster, which are helpful diagnosing problems with ECS deployment - for example, insufficient memory on an ECS host to deploy the required containers will result in an error in the ECS-agent logs. Failures in the Deploy stages, or situations where one of your Docker services doesn't start properly and has no logs available can often by diagnosed by referring to these logs.


-------------------
Bot Troubleshooting
-------------------

1. My Cobot Lambda is returning errors, help!

Did you look at the Lambda logs in CloudWatch? This should point you to where your Lambda is hitting an error.

2. These Pipelines take a long time to deploy! How am I supposed to build and test this way?

You need to verify that any module changes you are making work and that there are no typos or simple syntax errors before you do a "git push" and re-deploy your whole Cobot. Redeploying your Cobot is really for running end-to-end and integration tests of your full experience.

The best development workflow is to write module tests in the tests/<cobotname> directory, and run them from the bin directory via ./run-tests <botname> <modulename>, and only redeploy your bot once those tests are running.

3. My Docker module just isn't responding, help!

First, does the service pass module tests locally on your development machine? If so, then check the CloudWatch logs for the Service. If you don't see any activity or can't find the logs at all, there was probably an error with deployment on ECS - see the ECS logs in CloudWatch, or the AWS ECS console for more information.

4. I don't see any pipelines in CodePipeline.

There was a problem with your CloudFormation deployment. You'll need to go to the AWS console under CloudFormation and find the event that triggered a rollback to find the root cause of the issue.

5. I tried to run cobot update and it hit a problem. I rolled back the update, but it keeps happening.

Your best bet may be to delete your stack and start over, as long as it is not a production stack. If there is a particular cluster or load balancer that is causing issues, you may have luck by shutting the resource down manually through AWS console, then retrying the update. If that fails, please contact alexaprizesupport@amazon.com.

Still can't figure out a problem? Contact us at alexaprizesupport@amazon.com.
