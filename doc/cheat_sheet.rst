..  _cheat_sheet:

Cheat Sheet
===========

-------------------
System Architecture
-------------------

.. image:: images/logical_architecture_update.jpg

--------------------------
Frequently Asked Questions
--------------------------
 #. :ref:`Frequently Asked Questions <faq>`

-----------
Quick Guide
-----------
  #. :ref:`Building a Sample Socialbot <building_a_sample_socialbot>`
  #. :ref:`Expanding Your Socialbot <expanding_your_socialbot>`
  #. :ref:`Building a Non-trivial Socialbot <non_trivial_bot_example>`
  #. :ref:`Building a Dialog Manager hosted by AWS SageMaker <dialog_management_and_sagemaker>`

---------------------
Development Deep Dive
---------------------
  #. :ref:`Configure ASK skill <main_configuration>`
  #. :ref:`Configure Lambda Function <main_configuration>`
  #. :ref:`Configure Remote Docker modules <module_configuration>`
  #. :ref:`Creating an Intent Model  <intent_model>`
  #. :ref:`Accessing AlexaPrizeCoBotToolkitService Client <accessing_toolkit_service>`
  #. :ref:`Querying the Alexa Information Knowledge Graph <alexa_information_knowledge_graph>`
  #. :ref:`Experimentation and A/B testing <experimentation_and_ab_testing>`
  #. :ref:`SSH to CoBot EC2 instance <ssh_to_ec2>`
  #. :ref:`Handling Launch Request <handling_launch_requests>`
  #. :ref:`State Management and Information Flow <state_management_and_information_flow>`

     #. :ref:`Remote Service Module <state_manager_access_on_remote_service_module>`
     #. :ref:`Local Service Module <state_manager_access_on_local_service_module>`
  #. :ref:`Overriding CoBot Components <overriding_cobot_components>`

     #. :ref:`Global Intent Handler <override_global_intent_handler>`
     #. :ref:`ASR Processor <override_asr_processor>`
     #. :ref:`Offensive Speech Classifier <override_offensive_speech_classifier>`
     #. :ref:`Feature Extractor <override_feature_extractor>`
     #. :ref:`NLP Modules <override_nlp_modules>`
     #. :ref:`Dialog Manager <override_dialog_manager>`
     #. :ref:`Selecting Strategy <override_selecting_strategy>`
     #. :ref:`Response Generators Runner <override_response_generators_runner>`
     #. :ref:`Response Generators <add_response_generators>`
     #. :ref:`Ranking Strategy <override_ranking_strategy>`
     #. :ref:`Response Builder <override_response_builder>`
     #. :ref:`State Manager <customize_state_manager>`

-------
Testing
-------

  #. Docker module Testing

     #. :ref:`cobot quick-test CLI <cobot_quick-test>`
     #. :ref:`Manual Docker module Testing <docker_module_test>`
  #. :ref:`End-to-End Interactive-mode Testing <interactive_mode>`
  #. :ref:`End-to-End Local Testing <local_test>`
  #. :ref:`ASK simulate CLI <ask_simulate_cli>`
  #. Manual Integration Testing (talk to Echo)

------------------------
Debugging and Log Access
------------------------
  #. :ref:`Customize logging level and pattern on CloudWatch Logs <use_cloudwatch_for_log_access>`
  #. :ref:`Logging in Local Testing <local_test_log>`
  #. :ref:`Logging in Interactive-mode Testing <interactive_mode_log>`
  #. :ref:`Logging in remote CodePipeline <use_cloudwatch_for_log_access>`
  #. :ref:`Query CloudWatch Logs <query_cloudwatch_logs>`
  #. :ref:`cobot transcribe CLI <transcriber>`

----------
Deployment
----------
  #. :ref:`Beta and Prod Deployment  <beta_stage_for_cobot>`
  #. :ref:`Deploy ASK Skill <ask_skill_deploy>`

--------------
ToolkitService
--------------
  #. :ref:`Accessing Toolkit Service Client  <accessing_toolkit_service>`
  #. :ref:`Model Release Materials  <model_release_material>`
-----------------------
Workflow Recommendation
-----------------------
  #. :ref:`Workflow Recommendation <workflow_recommendation>`

---------
CoBot CLI
---------
Run ``cobot help`` for more detail, <arg> means required argument, [arg] means optional argument.

  1. | ``cobot deploy <cobot-name>``
     | Sets up a new CoBot instance from a local directory

  2. | ``cobot update <cobot-name>``
     | Updates CoBot stacks and configurations after making changes to modules.yaml or config.yaml

  3. | ``cobot validate <cobot-name>``
     | Verifies local files run successfully. Should be performed before a git push

  4. | ``cobot delete <cobot-name> -t <target>``
     | Valid values for <target> are: 'stack', 'lambda', 'repo' or 'all'
    -  | ``cobot delete <cobot-name> -t lambda``
       | Deletes the lambda function
    -  | ``cobot delete <cobot-name> -t repo``
       | Deletes the CodeCommit repository.
       | NOTE: Use EXTRA precaution while running this command. You might end up losing access to your entire codebase.
    -  | ``cobot delete <cobot-name> -t stack``
       | Deletes all CloudFormation stacks
    -  | ``cobot delete <cobot-name> -t all``
       | Deletes stacks and lambdas. It will NOT delete the CodeCommit repository.
  5. | ``cobot quick-test <cobot-name> <module-name>``
     | :ref:`Quick tests <cobot_quick-test>` docker module in cobot directory

  6. | ``cobot interactive-mode -c <config-file> -l <lambda-handler> -s <service-config-file>``
     | :ref:`Interactively <interactive_mode>` tests local Lambda function and docker modules

  7. | ``cobot local-test <lambda_handler_path> <events_file> <config_file> <output_file> [api_key]``
     | :ref:`Local tests <local_test>` Lambda function and docker modules

  8. | ``cobot transcribe -t <state-table-name> -s [session-id] -c [conversation-id] [verbosity]``
     | :ref:`Transcribes <transcriber>` one session detail or conversation detail from DynamoDB State table

  9. | ``cobot dynamodb-to-s3 deploy -t <state_table_name> -s <dynamodb_table_stream_arn> -l [lambda_func_file_path] -c [cloudformation_template_file_path]``
     | :ref:`Deploys<dynamodb_to_s3_deploy>` a data analytics pipeline from a DynamoDB table to S3

  10. | ``cobot dynamodb-to-s3 backfill -t <state_table_name> -s <output_s3_bucket>``
      | :ref:`Backfills<dynamodb_to_s3_backfill>` a DynamoDB table to S3

----------------------
Data Tools and Dataset
----------------------
  #. :ref:`Cleaning Data and Using Annotator <cleansing_data_and_using_annotator>`
  #. :ref:`Using Included Big Data Module: EnWiki, Dbpedia, YodaLight <using_included_big_data>`
  #. :ref:`Data Analytics and Visualization with AWS Athena and QuickSight <data_analytics_visualization>`
  #. Amazonian Conversation Dataset: s3://alexaprize/alexaprize_2017_conversation_subset.json
  #. Common Alexa Prize Chats (CAPC): s3://alexaprize/CAPC.tsv
  #. Washington Post API

-----------
Release Log
-----------
  #. :ref:`Release Log <change_log>`