.. _intent_model:

Creating an Intent Model
========================


-----------------------
Intent Model Definition
-----------------------

A normal ASK skill is dependent on the Alexa Skills Kit intent model to interpret user voice interactions. A user interaction will be converted to text, and that text will then be interpreted into a high level intent, and potentially slot values, to define details around the user request.

If your ASK skill is whitelisted for additional features, you can directly access the text of user interactions and create your own mechanism for interpreting user interactions, as well as accessing *n-best* alternate interpretations from the Alexa ASR (Automated Speech Recognition) system. However, you will still need a minimal intent model to pass through the text to your Cobot's analysis pipeline. You will also need support for the required StopIntent, which we recommend implementing in your ASK intent model.

In the case of our sample Cobot's greeter module, the intents understood are defined in the ASK intent model. The model input is located in sample_cobot/cobot-skill/models/en-US.json.

.. code-block:: bash

   {
     "interactionModel": {
       "languageModel": {
         "invocationName": "greeter",
         "types": [],
         "intents": [
   ...
                 {
             "name": "greet",
             "slots": [],
             "samples": [
               "hey",
               "hello",
               "hi",
               "hello there",
               "good morning",
               "good evening"
             ]
           },
   ...
         ]
       }
     }
   }
