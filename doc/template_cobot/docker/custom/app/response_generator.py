
required_context = ['intent','slots']

def get_required_context():
    return required_context

def handle_message(msg):
    #your response generator model should operate on the text or other context information here
    input_text = msg['slots']['text']['value']
    return input_text
