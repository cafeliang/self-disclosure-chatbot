import cobot_core as Cobot
from cobot_core.service_module import RemoteServiceModule
from cobot_core.service_url_loader import ServiceURLLoader


class CustomSelectingStrategy(Cobot.SelectingStrategy):
    def __init__(self):
        pass

    def select_response_mode(self, input):
        return ["CUSTOM"]

def overrides(binder):
    binder.bind(Cobot.SelectingStrategy, to=CustomSelectingStrategy)

def lambda_handler(event, context):

	# app_id: replace with your ASK skill id to validate ask request. None means skipping ASK request validation.
	# user_table_name: replace with a DynamoDB table name to store user preference data. We will auto create the DynamoDB table if the table name doesn’t exist.
	#                   None means user preference data won’t be persisted in DynamoDB.
	# save_before_response: If it is true, skill persists user preference data at the end of each turn.
    #                       Otherwise, only at the last turn of whole session.
	# state_table_name: replace with a DynamoDB table name to store session state data. We will auto create the DynamoDB table if the table name doesn’t exist.
	#                   None means session state data won’t be persisted in DynamoDB.
	# overrides: provide custom override for dialog manager components.
	cobot = Cobot.handler(event,
	                      context,
	                      app_id=None,
	                      user_table_name='UserTable',
	                      save_before_response=True,
	                      state_table_name='StateTable',
	                      overrides=overrides)

	# Remote Response Generator Module
	# name: response generator name in capital letter
	# class: python class implementation. Use RemoteServiceModule for Remote Response Generator if no method override is required.
	# url: remote service url. If a remote service is setup by cobot-deploy script: call ServiceURLLoader.get_url_for_module("module_name") to fetch url from service load balancer's endpoint
	#                          Otherwise provide a custom url
	# context_manager_keys: a list of state keys to pass to service module.
	CustomBot = {
		'name': "CUSTOM",
		'class': RemoteServiceModule,
		'url': ServiceURLLoader.get_url_for_module("CUSTOM"),
		'context_manager_keys': ['intent','slots']
	}

	cobot.add_response_generators([CustomBot])
	return cobot.execute()


# if __name__ == '__main__':
# 	lambda_handler(event=existing_session_event, context={})
