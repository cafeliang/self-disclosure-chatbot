.. _getting_started:

Getting Started
===============

-----------------
Requirements
-----------------

  * An `Amazon Developer Account <http://developer.amazon.com/>`_
  * An `AWS Account <http://aws.amazon.com/>`_
  * `AWS CLI`__
  * Python_ 3.6+
  * Node.js_ 6.0 or greater and Node Package Manager (npm), which installs with Node.js. (Required for ask-cli tool)
  * git
  * pip3 - v19.0 or higher versions 
  * ASK CLI -  v1.7 or higher


**Step 1.** Verify that your system meets these requirements by running the following one-line command:

.. code-block:: bash

   bash <(curl -H "x-api-key:rRiCFn1bFQ5Qa2tLqgypS2lMuGsAum4u4dkoZths" -s https://olbp4j9ali.execute-api.us-east-1.amazonaws.com/prod/)   

.. __: http://docs.aws.amazon.com/cli/latest/userguide/installing.html
.. _Python: http://www.python.org/
.. _Node.js: http://www.nodejs.org/

-------------------
Preparing for Cobot
-------------------

As a competitor in the Alexa Prize, you will receive access to free services from Amazon Web Services (AWS). With AWS, you will launch the virtual machines, databases, and infrastructure to host and run your socialbot. Your team lead created an AWS account on your team's behalf
and he/she will use this account to create additional users for your team members. Follow the `create an IAM user in your AWS account <http://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html>`_, instructions to do so.

Then, you will need to `obtain the AWS Access Key ID and Secret Access Key associated with the IAM user you just created <http://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html>`_.

**Step 2.** You will enter those credentials into the AWS command line tool. You should leave the default region, so your bot will be deployed to us-east-1.

.. code-block:: bash

   aws configure
   AWS Access Key ID [****************XXXX]:
   AWS Secret Access Key [****************XXXX]:
   Default region name [us-east-1]:
   Default output format [None]:

This creates a new file in your home directory, ~/.aws/credentials.

-----------------
Installation
-----------------

Now that we've set up AWS access, let's pull the Cobot code from Amazon's git repository.

**Step 3.** Alexa Prize team emailed Git credentials to your team lead. Use those credentials while cloning the Cobot Toolkit from the upstream repository.

.. code-block:: bash

  git -c credential.helper= clone https://git-codecommit.us-east-1.amazonaws.com/v1/repos/AlexaPrizeCobotToolkit ~/cobot

*Internal Amazon Users:* You will be able to pull the base Cobot snapshot from code.amazon.com (via git pull ssh://git.amazon.com/pkg/AlexaPrizeCobotToolkit)

**Step 4.** Once you've downloaded the Cobot code, you'll need to install a few required Python packages.

.. code-block:: bash

  cd ~/cobot
  pip3 install -r requirements.txt

**Step 5.** Set up your environment by adding the following line to ~/.profile (Mac) or ~/.zshrc (Mac, if you are using zsh as default editor) or ~/.bash_profile (Linux):

.. code-block:: bash

   export COBOT_HOME=~/cobot; export PATH=$COBOT_HOME/bin:$PATH

where COBOT_HOME is the full path of the directory you just pulled via git.

By default, cobot deployment commands will use the default AWS profile. If you have multiple AWS accounts and want to configure one AWS account profile to deploy cobot, you can override the AWS_PROFILE in the ~/.profile (Mac) or ~/.zshrc (Mac, if you are using zsh as default editor) or ~/.bash_profile (Linux):

.. code-block:: bash

   export AWS_PROFILE=<AWS_PROFILE_NAME>

You can find all the AWS profiles in ~/.aws/credentials.

.. code-block:: bash

   [AWS_PROFILE_NAME]
   aws_access_key_id = <>
   aws_secret_access_key = <>
   [AWS_PROFILE_NAME]
   aws_access_key_id = <>
   aws_secret_access_key = <>

Then reload the file via "source ~/.profile" or "source ~/.zshrc" or "source ~/.bash_profile". You will now be able to run cobot deployment commands from any directory with the configured AWS profile.

---------------------
Deploy Your First Bot
---------------------

**Step 6.** Ensure the S3 bucket-name is set to a unique value, add your existing skill Id and set the desired invocation phrase for your skill in config.yaml. All of the other default values should be fine for a simple bot.

.. code-block:: bash

   emacs config.yaml

**Step 7.** **(Only one team member should perform this step)** Log into your AWS account and create a new Key Pair named cobot (all lowercase) in the EC2 console. This will allow you to SSH to your Cobot instances after deployment; refer to :ref:`SSH to CoBot EC2 instance <ssh_to_ec2>` for details.

**Step 8.** **(Only one team member should perform this step)**

Finally, we need to deploy the bot:

.. code-block:: bash

   cobot deploy sample_cobot

This will create a new CodeCommit repository under your AWS account, and configure build and deployment pipelines for the lambda, and all of the specified modules (Docker components) defined in your sample_cobot directory.

------------------
What Comes Next
------------------

You will need to wait about 20 minutes for your Cobot to come online - we recommend checking CloudFormation and CodePipeline in your AWS console to see the deployment status. Once the CloudFormation stage of bot deployment is complete, you will see two pipelines in CodePipeline.

The first is your Cobot pipeline, which builds and deploys your Lambda and any bot-specific Docker modules every time there is a new commit to your CodeCommit git repository:

.. image:: pipeline.png

The second is your Infrastructure pipeline. This contains the NLP and Sentiment Analysis docker modules, as well as any data modules you are using, which are unlikely to change frequently. This pipeline will only rebuild on a manual request.

.. image:: infra_pipeline.png

Once CodePipeline shows all stages of both as green, your bot is fully built and deployed.

You should now be able to launch your bot by saying "Alexa, open <invocation phrase>" with the phrase you configured above.

That's it! Deploying a simple Cobot is very easy.

------------------------------------
Whitelisting for Text and N-Best ASR
------------------------------------

These capabilities are not available to general ASK skills. You will need to contact Alexa Prize Support with your Skill ID and your Developer Account `Vendor ID`__ to get access to the "text" and "n-best" values in ASK. Once whitelisted, these will become available automatically in Cobot, in your bot context.

.. __: https://developer.amazon.com/mycid.html

