..  _beta_stage_for_cobot:

Cobot Beta and Prod Deployment
==============================

------------
CodePipeline
------------

CoBot uses AWS CodePipeline to achieve continuous integration and deployment for infrastructure updates.

Every time there is a code change (triggered by git push or cobot update), CodePipeline builds ECS services, deploys Lambda Function and ECS services to Beta Stack (CoBotBeta Cluster).

Once it gets "approval" from a developer, CodePipeline automatically deploys Lambda Function (by pointing Prod Alias to the latest version of Lambda Function), and ECS services to Prod Stack (CoBot Cluster).

After you run ``cobot update`` or ``cobot upgrade``, you will see CloudFormation stack updates as below.

    .. image:: images/pipeline_status_staging.png

After CloudFormation updates, you can dive deep into CodePipeline, ECS Cluster, and SNS on the AWS Console.

* CodePipeline Dive Deep
    1. Go to CodePipeline in the AWS console:
    2. Then go to the `cobot-Pipeline` CodePipeline and check that the pipeline has the following stage and actions:

        - Beta Stage with **DeployToProdApproval** action
        - Prod Stage

    The CodePipeline should look like this image:

    .. image:: images/beta_prod_codepipeline.png
    3. Then go to the `cobot-InfrastructurePipeline` CodePipeline. It tracks infrastructure branch of the CodeCommit Repository, builds and deploys nlp services, including sentiment, NER.
    The cobot-Infrastructure-Pipeline should look like this image:

    .. image:: images/infra_codepipeline.png


* ECS Cluster Dive Deep
    1. Go to ECS in the AWS console
    2. Then go to Clusters and check that there are only **cobot-stack-CobotCluster-#** and **cobot-stack-BetaCobotCluster-#**

    The new ECS Cluster should look like this image:

    .. image:: images/ecs_cluster.png

* Lambda Function Prod Alias Dive Deep
    1. Go to Lambda in the AWS console
    2. Go to the **cobot-lambda** lambda function

    .. image:: images/lambda.png


    3. Click **Qualifiers**, select **Alias** and then click **Prod**

    .. image:: images/lambda_prod.png


    4. Make sure that the **Prod** Alias has **Alexa Skills Kit** as a trigger

    .. image:: images/lambda_prod_trigger.png

* SNS Dive Deep

    1. Go to Simple Notification Service in the AWS console
    2. Then go to **Topics** tab and check that there is **Cobot-Pipeline-SNS** Topic

    The new SNS should look like this image:

    .. image:: images/sns_topic.png

**Set SNS Topic Subscription**

You now need to add your team's or your team members' email(s) in the SNS Topic Subscription list, so that the email addresses will get an approval action reminder notification.

To subscribe the SNS topic
    1. Select **Cobot-Pipeline-SNS**
    2. Click Actions and select **subscribe to topic**

    .. image:: images/sns_topic_subscription.png


    3. In the popup window, select Email as **Protocol** and input email address in **Endpoint**.

    .. image:: images/sns_topic_subscription_popup.png


    4. You will receive an email to confirm subscription

    .. image:: images/sns_sub_email.png

-----------------------------------------------------------------------
STAGE Environment Variable in your Lambda Function and Docker Container
-----------------------------------------------------------------------

We add a new **STAGE** environment variable in Lambda function. We add a new **STAGE** environment variable in Lambda function. If **STAGE** value is **BETA**, the Lambda function connects to Beta resources. If **STAGE** value is **PROD**, the Lambda function connects to Prod resources. You can use **STAGE** to setup different DynamoDB tables in Beta and Prod environments.

For example:

.. code-block:: python

    import os

    def lambda_handler(event, context):
        if os.environ.get('STAGE') == 'PROD':
            USER_TABLE_NAME='UserTable'
            STATE_TABLE_NAME='StateTable'
        else:
            USER_TABLE_NAME='UserTableBeta'
            STATE_TABLE_NAME='StateTableBeta'

        cobot = Cobot.handler(event,
                            context,
                            app_id=None,
                            user_table_name=USER_TABLE_NAME,
                            save_before_response=True,
                            state_table_name=STATE_TABLE_NAME,
                            overrides=overrides,
                            api_key=None)


We also pass **STAGE** environment variable into Docker container. You can write similar code to configure different resources in Beta and Prod environment.

