.. _bot_configuration:

Bot and Module Configuration
============================

.. _main_configuration:

------------------
Main Configuration
------------------

Your code and bot description are all hosted in your *cobot instance* directory. The **sample_cobot** directory provides a template to get started from for building your own cobot. Two configuration files describe the high level structure of your conversational bot, config.yaml and modules.yaml. 

Config.yaml describes the entry point for your conversational bot skill

.. code-block:: bash

    #the following values must be changed
    bucket: <CHANGEME>                          # S3 bucket for template storage - any unique, valid S3 bucket name
    skill_id: <CHANGEME>                        # ASK Skill ID - this should be in the format amzn1.ask.skill.12345678-1234-1234-123456789123
    invocation_name: '<CHANGEME>'               # ASK Invocation name - the phrase you will use to launch your bot

    #the following values can be left to their defaults
    botname: cobot                              # Cobot name, can contain only alphanumeric characters (case sensitive) and hyphens.
    repository: cobot                           # CodeCommit Repository for basic cobot toolkit
    region: us-east-1                           # S3 region
    lambda_name: cobot-lambda                   # Lambda function name attached with ASK skill
    lambda_runtime: python3.6                   # Lambda function runtime, default to python3.6, valid options may be found at: https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html
    lambda_handler: greeter_bot.lambda_handler  # Lambda function handler class
    sns_topic: Cobot-Pipeline-SNS               # SNS Topic name for CodePipeline Approval Manual Action

The lambda_handler entry describes a bot where the lambda_handler function in greeter_bot.py will be launched whenever a launch request or intent request comes in from the Alexa Skills Kit environment.

You will also want to set your invocation_name to a phrase that will work well for testing your bot.

Once you submit a Skill ID for whitelisting, **add this ID** to config.yaml to keep using the same skill.

.. _module_configuration:

--------------------
Module Configuration
--------------------

The other configuration file, modules.yaml, describes the set of Docker modules to be deployed in this Cobot instance. The Cobot Modules will be deployed in your Cobot pipeline - these are located in the docker subdirectory of your cobot instance. Infrastructure Modules will be deployed in your Infrastructure pipeline - these are located in the cobot_common/docker directory.

.. code-block:: bash

   cobot_modules:
     - greeter

   infrastructure_modules:
     - nlp


To configure each module's advanced parameters (i.e. memory, desired_count, health_check_grace_period_seconds):

.. code-block:: bash

   memory_reservation: The soft limit (in MiB) of memory to reserve for the container, which means the lower bound of memory to reserve for the container. Default: 4096
   desired_count: The number of simultaneous tasks that you want to run on the cluster. This configuration is operational, after auto-scaling is enabled. Default: 1
   health_check_grace_period_seconds: The period of time, in seconds, that the Amazon ECS service scheduler should ignore unhealthy Elastic Load Balancing target health checks after a task has first started. Default: 0
   processor_type: A placement constraint that restricts the module to only run on instances of this processor type. The only option is gpu. The default is that the module can run on any processor type.
   number_of_gpu: The number of physical GPUs the Amazon ECS container agent will reserve for the container. The number of GPUs reserved should not exceed the number of available GPUs on the container instance the task is launched on. This value is ignored if you do not specify processor_type: gpu. Default: 1

See more detailed explanations at https://docs.aws.amazon.com/AmazonECS/latest/developerguide/service_definition_parameters.html

.. code-block:: bash

   cobot_modules:
       - name: greeter
         memory_reservation: 100
         desired_count: 1
         health_check_grace_period_seconds: 1200

It will create a greeter ECS service with 1 task, reserve 100MiB memory for the Docker container, and wait 1200s until Load Balancer sends PING health-check requests to target group.

**Recommended steps to find a proper memory reservation value**

1. Run command ``docker run --rm -p <local host port>:<docker port> --name <container name> <image name>`` to start a docker module.
For example, to see nlp module's memory usage: ``docker run --rm -p 3000:80 --name nlp nlp``

2. Open a new console window and run command ``docker stats <container name>`` to display a live stream of docker module resource usage statistics. You will see the memory usage value in ``MEM USAGE`` column, which is a proper memory reservation value in module configuration yaml file.
For example, to see nlp module: ``docker stats nlp``

.. image:: images/docker_stats.png

**Recommended steps to find a proper health check grace period seconds value**

If there is an error from docker module like: ``[ERROR] gunicorn.error: WORKER TIMEOUT (pid:23475)``, it means that docker module spends more than 30 seconds to start a flask instance. To address this problem, try the following steps:

1. Add timeout parameter gunicorn command in ``docker_module_fold/config/supervisord.conf`` file. You may need to try different timeout paramter numbers until the timeout error is eliminated. In following example, use paramter ``-t <timeout in second>`` to configure timeout in second.

.. code-block:: bash

  [program:gunicorn]
  command=/usr/bin/gunicorn app:app -t 1200 -w 1 -b localhost:5001 --log-level info

2. Use the same number to configure health check grace period seconds, so that ECS can ignore health checking after a task has first started.

---------------------------
CoBot Cluster Configuration
---------------------------
In the CoBot pipeline, all Beta ECS services are running on CoBotBeta Cluster, and all Prod ECS services are running on CoBot Cluster.

Both CoBotBeta and CoBot clusters use m4.xlarge EC2 instance type by default and have 1 EC2 instance by default.

To configure the advanced cluster parameters (i.e. instance_type, cluster_size, key_name):

.. code-block:: bash

   instance_type: The CPU instance type. All CPU instances in the cluster will be of this instance type. Default: m4.xlarge
   gpu_instance_type: The GPU instance type. All GPU instances in the cluster will be of this instance type. If you do not specify gpu_instance_type, no GPU instances will be added to your cluster. No default
   cpu_cluster_size: The desired number of CPU instances per cluster. Managed by auto-scaling policies post-deployment. Default: 1
   gpu_cluster_size: The desired number of GPU instances per cluster. Managed by auto-scaling policies post-deployment. This value is ignored if you do not specify gpu_instance_type. Default: 1
   key_name: An Amazon EC2 key pair name. You must create the key pair manually in the EC2 console before deploying with this parameter. No default

.. code-block:: bash

   instance_type: m4.xlarge
   gpu_instance_type: p2.xlarge
   cpu_cluster_size: 1
   gpu_cluster_size: 2
   key_name: <my-key-pair>

Refer to `ssh_to_ec2`_ for more detail on how to create a key pair.

Refer to https://aws.amazon.com/ec2/instance-types/ to view different EC2 instance types' CPU and memory resources.

-----------------
GPU Configuration
-----------------

There are 4 main parameters you'll need to set in your modules.yaml in order to run modules on GPU.
For each module that will run on GPU, you must set processor_type: gpu in the module configuration.
The processor_type parameter will ensure that the module only runs on EC2 instances with a GPU processor.
The second module configuration parameter related to GPU is number_of_gpu.
By default, number_of_gpu will be 1 if the module has specified processor_type: gpu.
If your module requires more than 1 GPU, you should increase number_of_gpu for that module.

You also need to set two cluster configuration parameters.
Use gpu_instance_type to specify the type of GPU instance to include in your cluster.
You may only have one type of CPU instance and one type of GPU instance in your cluster.
All GPU instances in your cluster will be of instance type gpu_instance_type.
The gpu_instance_type parameter is required; there is no default gpu_instance_type.
Keep in mind that your instance must have enough GPUs to satisfy the number_of_gpu requirement.
For example, if you have set number_of_gpu to 2, you must choose an instance type that has 2 or more GPUs.

The second cluster configuration parameter is gpu_cluster_size.
We recommend that you set gpu_cluster_size so that you have enough GPUs and memory available to run 1 extra copy of your largest GPU module.
The reasoning behind this recommendation is that whenever you deploy a new version of your module, ECS will keep the old version running during the deployment.
So your old version continues running, and the new version spins up on the spare GPU.
The old version is terminated once the new version becomes stable.
Another option is to choose a gpu_instance_type that is large enough to fit two copies of your largest GPU module (including number_of_gpu and memory_reservation requirements).
Having enough GPUs to run 1 extra copy of your largest GPU module should suffice, even if you have multiple GPU modules, since the module deployments can take turns using the spare GPU sequentially.

If you do not have a spare GPU in your cluster, your module deployments will get stuck while ECS tries and fails to place the new version, eventually timing out after a period of time.
If you set gpu_cluster_size: 2, the expectation is that your BetaCobotCluster will have 2 GPU instances and your prod CobotCluster will have 2 GPU instances, for a total of 4 GPU instances between both clusters.
We recognize that having a spare GPU in each stage of your pipeline is not ideal from a cost perspective and will work to reduce costs in a future release.

In summary, processor_type, gpu_instance_type, and gpu_cluster_size should be considered required parameters to run modules on GPU.
The example modules.yaml file below will spin up a total of 4 p2.xlarge instances (2 in beta and 2 in prod) and run the nounphrases module on GPU.

.. code-block:: bash

    cobot_modules:
        - name: nounphrases
          processor_type: gpu
          number_of_gpu: 1

    gpu_instance_type: p2.xlarge
    gpu_cluster_size: 2
    key_name: cobot

**Tips for GPU Development**

1. If you are using Cobot's remote module template, you will need to edit your Dockerfile to use a GPU-based image.
If you do not use the cobot_base image, you may have to add python3-pip to the installations in your Dockerfile.

.. code-block:: bash

   RUN apt-get install -y nginx supervisor gcc g++ python3-pip

2. Cobot's remote module template uses 9 workers by default (-w 9).
You will need to tune the number of workers and your memory_reservation parameter to ensure you reserve enough memory for all workers to start running.
Refer to http://docs.gunicorn.org/en/stable/design.html to learn more.

.. _ssh_to_ec2:

-------------------
SSH to EC2 Instance
-------------------

To configure the key pair name and instance type of EC2 cluster, so that you can ssh to EC2 instances within the cluster.

Follow 6 steps as below:

1. create a key pair on the AWS EC2 console,
2. download the key pair file, and run chmod 400 [my-key-pair.pem]
3. replace the key pair name with your key pair name.

.. code-block:: bash

   instance_type: m4.xlarge
   key_name: <my-key-pair>

4. run cobot deploy/update/upgrade CLI
5. Find the security group for the EC2 instance you want to ssh and add a new InBound Rule to the security group.

   .. image:: images/add_inbound_rule.png

6. run ssh -i [my-key-pair.pem]  ec2-user@[ec2 instance's public DNS]
