.. _accessing_toolkit_service:

Accessing AlexaPrizeToolkitService Client
===========================================

AlexaPrizeToolkitService provides common functionality in NLP, response generation, etc. that is needed to build socialbots. It's exposed through AWS API Gateway and can be accessed using the API Gateway client in cobot_common package. The client code is located in package cobot_common.service_client_. An example usage of the client can be found in demo_client.py_ file.

COBOT_API_KEY is the API key in API Gateway. It should be provided by Alexa Prize team.

---------------------
Initializing a Client
---------------------
A Client can be initialized by calling method get_client. To import the method, use

.. code-block:: python

    from cobot_common.service_client import get_client

Here is an example usage of this method,

.. code-block:: python

    toolkit_service_client = get_client(api_key=<API_KEY>, timeout_in_millis=1000)

API_KEY needs to be provided and it should be the API key of AlexaToolkitService API Gateway. timeout_in_millis is optional and if not set, a default value of 1000 is used. All API calls are defaulted to have this timeout value if no API level timeout is specified.

-----------------
Sending a Request
-----------------
An API can be called by using client.__getattr__. The attribute name should map to the API method name that is called. For example,

.. code-block:: python

    answer = toolkit_service_client.get_answer(question="who are you", timeout_in_millis=1000)

The parameters of this method should be the input parameters for the API. In the above example, the API called is EVI get_answer, and the input parameter is 'question'. timeout_in_millis is optional and is not part of the API input. It's defaulted to 1000. It controls how many milli seconds the method would wait for the response.

The models of the APIs are located in folder cobot_common/service_client/models.

--------
Response
--------
See demo_client.py_ for details about response structures.

.. _cobot_common.service_client: cobot_common.service_client.html
.. _demo_client.py: demo_client_link.html
