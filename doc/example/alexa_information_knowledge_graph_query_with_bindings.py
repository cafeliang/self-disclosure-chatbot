from pprint import pprint
from cobot_common.service_client import get_client

# Client object to call AlexaPrizeToolkitService
client = get_client(api_key='<API_KEY>', timeout_in_millis=2000)

# Define a query to find who directed the movie 'm'
query = {"text": "query d|d <aio:directed> m"}

for movie in ['aie:pulp_fiction_movie', 'aie:jaws', 'aie:inception_3']:
    # Bind the entity identifier of the movie to the variable 'm'
    variableBindings = [
        {"variable": "m", "dataType": "aio:Entity", "value": movie}
    ]

    # Execute the query against the knowledge graph
    response = client.get_knowledge_query_answer(query=query, variableBindings=variableBindings)

    # Dump the content of the response to standard output
    pprint(response)
