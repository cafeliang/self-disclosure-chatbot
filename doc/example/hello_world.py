# from sample_cobot.sample_events import new_session_event
import cobot_core as Cobot
from cobot_core.service_module import LocalServiceModule


class BaselineResponseGeneratorModule(LocalServiceModule):
	def execute(self):
		print('Run in Local Baseline Response Generator')
		return "Local Baseline Response Generator"

def lambda_handler(event, context):
    cobot = Cobot.handler(event,
                          context,
                          app_id = None,
                          user_table_name = "UserTable",
                          save_before_response = True,
                          state_table_name="StateTable",
                          api_key=None)
    # Local Response Generator Module
    Baseline = {
        'name': "BASELINE",
        'class': BaselineResponseGeneratorModule,
        'context_manager_keys': ['asr']
    }
    cobot.add_response_generators([Baseline])

    return cobot.execute()

# if __name__ == '__main__':
#     lambda_handler(event=new_session_event, context={})