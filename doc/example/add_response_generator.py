import cobot_core as Cobot
# from sample_cobot.sample_events import new_session_event
from cobot_core.service_module import RemoteServiceModule, LocalServiceModule
from cobot_core.service_url_loader import ServiceURLLoader


class LocalMoviebotModule(LocalServiceModule):
	def execute(self):
		print('Run in Local Moviebot')
		return "Local Moviebot response generator"


def lambda_handler(event, context):
	cobot = Cobot.handler(event,
	                      context,
	                      app_id=None,
	                      user_table_name='UserTable',
	                      save_before_response=True,
	                      state_table_name='StateTable',
	                      overrides=None,
	                      api_key=None)

	# Local Response Generator Module
	Moviebot = {
		'name': "LOCALMOVIEBOT",
		'class': LocalMoviebotModule,
		'context_manager_keys': ['asr']
	}

	# Remote Response Generator Module
	# Note on how to setup 'url' field:
	# 1. If a remote service is setup by cobot-deploy script: call ServiceURLLoader.get_url_for_module("module_name") to fetch url from service load balancer's endpoint
	# 2. Otherwise provide a custom url
	# 3. For local testing, you can create an environment variable LOAD_BALANCER_ENDPOINTS or provide a custom url, see ServiceURLLoader for more detail
	RemoteMoviebot = {
		'name': "REMOTEMOVIEBOT",
		'class': RemoteServiceModule,
		'url': ServiceURLLoader.get_url_for_module("NLP"),
		'context_manager_keys': ['asr']
	}

	cobot.add_response_generators([Moviebot, RemoteMoviebot])
	return cobot.execute()


# if __name__ == '__main__':
# 	lambda_handler(event=new_session_event, context={})
