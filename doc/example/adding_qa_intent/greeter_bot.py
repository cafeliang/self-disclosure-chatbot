import cobot_core as Cobot
import requests
# from sample_cobot.sample_events import new_session_event, existing_session_event, greeter
from bs4 import BeautifulSoup
from cobot_core.service_module import RemoteServiceModule, LocalServiceModule
from cobot_core.service_url_loader import ServiceURLLoader


class CustomSelectingStrategy(Cobot.SelectingStrategy):
    def __init__(self):
        pass

    def select_response_mode(self, input):
        return ["GREETER"]


def overrides(binder):
    binder.bind(Cobot.SelectingStrategy, to=CustomSelectingStrategy)

class EviResponseGenerator(LocalServiceModule):
    def execute(self):
        intent = self.extra_data['intent']
        slots = self.extra_data['slots']
        question_word = slots['question_word']['value']
        question_text = slots['question_text']['value']
        question_text = question_text.replace(' ','_')
        url = "http://www.evi.com/q/"+question_text
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        result = soup.find('div', attrs={'class': 'tk_common'})
        print('result:', result.string.strip())
        return result.string.strip()


def lambda_handler(event, context):

	# app_id: replace with your ASK skill id to validate ask request. None means skipping ASK request validation.
	# user_table_name: replace with a DynamoDB table name to store user preference data. We will auto create the DynamoDB table if the table name doesn’t exist.
	#                   None means user preference data won’t be persisted in DynamoDB.
	# save_before_response: If it is true, skill persists user preference data at the end of each turn.
    #                       Otherwise, only at the last turn of whole session.
	# state_table_name: replace with a DynamoDB table name to store session state data. We will auto create the DynamoDB table if the table name doesn’t exist.
	#                   None means session state data won’t be persisted in DynamoDB.
	# overrides: provide custom override for dialog manager components.
	cobot = Cobot.handler(event,
	                      context,
	                      app_id=None,
	                      user_table_name='UserTable',
	                      save_before_response=True,
	                      state_table_name='StateTable',
	                      overrides=overrides)

	# Remote Response Generator Module
	# name: response generator name in capital letter
	# class: python class implementation. Use RemoteServiceModule for Remote Response Generator if no method override is required.
	# url: remote service url. If a remote service is setup by cobot-deploy script: call ServiceURLLoader.get_url_for_module("module_name") to fetch url from service load balancer's endpoint
	#                          Otherwise provide a custom url
	# context_manager_keys: a list of state keys to pass to service module.
	RemoteGreeterBot = {
		'name': "GREETER",
		'class': RemoteServiceModule,
		'url': ServiceURLLoader.get_url_for_module("GREETER"),
		'context_manager_keys': ['intent','slots']
	}

	EviBot = {
		'name': "EVI",
		'class': EviResponseGenerator,
		'url': 'local',
		'context_manager_keys': ['intent', 'slots']
	}

	cobot.add_response_generators([RemoteGreeterBot, EviBot])
	return cobot.execute()


# if __name__ == '__main__':
# 	lambda_handler(event=existing_session_event, context={})
