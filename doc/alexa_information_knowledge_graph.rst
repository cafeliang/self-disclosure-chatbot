.. _alexa_information_knowledge_graph:

Querying the Alexa Information Knowledge Graph
==============================================

The Alexa Information Knowledge Graph is one of the sources of knowledge behind Alexa's question answering capabilities.
For the Alexa Prize competition, a slice of that knowledge graph is available to contestants. Let's see how to execute
Alexa Information Query Language (AIQL) queries to obtain data from the knowledge graph.

Cobot Toolkit API
-----------------

Knowledge Graph Query Service
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, here's a code snippet showing how to send a query through the AlexaPrizeToolkitService.

.. literalinclude:: example/alexa_information_knowledge_graph_query.py

**Output**

.. code-block:: Python

  {'requestId': 'c46ba252-b3a3-11e9-b81b-47f12b25e1fe',
   'results': [{'bindingList': [{'dataType': 'aio:Entity',
                                 'value': 'aie:quentin_tarantino',
                                 'variable': 'd'}]}],
   'status': 'COMPLETENESS_UNKNOWN'}

The result set contains the values found in the knowledge graph that satisfy the query. Here, ``d`` is bound to the
entity ``aie:quentin_tarantino``, because the knowledge graph contains the fact:

.. code-block::

   <aie:quentin_tarantino> <aio:directed> <aie:pulp_fiction_movie>

The next section, :ref:`query_language_tutorial`, describes in more details how the query language works.

The API supports an alternative way to specify entities in the query, using *variable bindings*. The next code snippet
shows how a query is defined once and then reused to find the director of three different movies.

.. literalinclude:: example/alexa_information_knowledge_graph_query_with_bindings.py

**Output**

.. code-block:: Python

  {'requestId': '49c09918-b3b2-11e9-bdef-25734bd5b763',
   'results': [{'bindingList': [{'dataType': 'aio:Entity',
                                 'value': 'aie:quentin_tarantino',
                                 'variable': 'd'}]}],
   'status': 'COMPLETENESS_UNKNOWN'}
  {'requestId': '4a0b5d24-b3b2-11e9-8c23-8be0126ae27d',
   'results': [{'bindingList': [{'dataType': 'aio:Entity',
                                 'value': 'aie:steven_spielberg',
                                 'variable': 'd'}]}],
   'status': 'COMPLETENESS_UNKNOWN'}
  {'requestId': '4a8d5e90-b3b2-11e9-b265-cd604e2a9183',
   'results': [{'bindingList': [{'dataType': 'aio:Entity',
                                 'value': 'aie:christopher_nolan',
                                 'variable': 'd'}]}],
   'status': 'COMPLETENESS_UNKNOWN'}

Entity Resolution service
~~~~~~~~~~~~~~~~~~~~~~~~~

Looking at the previous AIQL queries, you may wonder how you would have determined that ``aie:pulp_fiction_movie``
identifies the entity for the Pulp Fiction movie in the knowledge graph. That is what the Entity Resolution (ER) service
is for. It takes a natural language text and returns a list of matching entity identifiers. Optionally, a list of class
constraints can be provided to restrict the search.

.. literalinclude:: example/alexa_information_knowledge_graph_entity_resolution_with_constraints.py

**Output**

.. code-block:: Python

  {'requestId': 'e016d74f-c084-11e9-948d-3b09b754127f',
   'resolvedEntities': [{'dataType': 'aio:Entity',
                         'value': 'aie:harry_potter_and_the_philosophers_stone'}]}

Note that if the class constraint was omitted, additional entities would have been returned, including one for the
fictional character (``aie:harry_potter``) and another one for the movie
(``aie:harry_potter_and_the_philosophers_stone_film``).

.. _query_language_tutorial:

Alexa Information Query Language
--------------------------------

The aim of this section is to get you writing AIQL queries as quickly as possible, not to give an exhaustive formal
specification of the language. A formal specification of the syntax can found in the :ref:`query_language_syntax`
section below.

We're going to proceed by a typical workflow example, starting with a single object, retrieving some information on it,
and finally grabbing an output string for it.

Identifiers and Namespaces
~~~~~~~~~~~~~~~~~~~~~~~~~~

AIQL queries are made up of variables, literal values and identifiers. Identifiers may represent ontological concepts or
individuals.

Namespace ``aio`` for *ontological concepts* (e.g. classes or properties)
  They are documented in the :ref:`ontology`. You may think of it as the schema of the knowledge graph. It is sometimes
  referred to as the "Tbox". (You may notice that the ontology also includes a small set of individuals. Those are
  typically members of a class with a small and finite set of entities.)

  Examples: ``aio:Movie``, ``aio:isAStarIn``, ``aio:wasPublishedAtTimepoint``, etc.

Namespace ``aie`` for *individuals*
  They can be looked up using the *Entity Resolution* service. You may think of those entities as the content of the
  knowledge graph. It is sometimes referred to as the "ABox".

  Examples: ``aie:pulp_fiction_movie``, ``aie:quentin_tarantino``, ``aie:uma_thurman``, etc.

While the ontology is occasionally updated and expanded, individuals are constantly added, updated or removed. The Alexa
Information Knowledge Graph contains millions of facts and new facts are continually ingested.

Entity Resolution
~~~~~~~~~~~~~~~~~

Let's assume you want to find out about the movie Pulp Fiction, because the Alexa customer mentioned it and "pulp
fiction" is present in the ASR text. We must determine its entity identifier before we can include it in AIQL queries.

The Entity Resolution service provides an API to map a text to a entity identifier. Names often denote multiple
entities, some of which may be completely unrelated. The API allows clients to provide a list of class constraints to
require that each entity returned be a member of (i.e. ``aio:isAnInstanceOf``) all the specified classes.

Back to our example. Call the ER service with "pulp fiction" as the *mention* and ``aio:Movie`` as a *class constraint*
and the response will include the entity ``aie:pulp_fiction_movie``.

Classes
~~~~~~~

We can find out all the classes ``aie:pulp_fiction_movie`` is a member of (everything it is a type of) with the
following query:

.. code-block::

  query cls
  <aie:pulp_fiction_movie> <aio:isAnInstanceOf> cls

Here ``query`` is the keyword which begins every query. It functions analagously to a SQL or SPARQL "SELECT". ``cls`` is
a variable. Variables must be lowercase, and may contain numbers or underscores, but must begin with a letter. (``CLS``
and ``2cls`` are not variables, but ``cls_2`` and ``cls2`` are.) ``query cls`` constitutes the "query header". You can
read it as "give me all values of variable ``cls`` such that..."

The next line is a triple expression, consisting of a left object, a relation, and a right object - in that order. The
left object is sometimes referred to as the subject, the relation is sometimes referred to as the property or predicate,
and the right object is sometimes referred to as the object. In this particular line, we have an entity ID in the left
object position. Entity IDs consist of a namespace - ``aie`` - and an entity name, which must be lowercase and
under_scored - it may contain numbers, but must start with a letter. This is then enclosed in angled brackets: ``<``,
``>``. Entity IDs can appear on the left or right of the relation.

The relation position is occupied by a relation ID denoting the instance-of relation. Relation IDs consist of a
namespace - ``aio`` - and a relation name, which must be camelCased (again, it may contain numbers, but must start with
a letter). These are then enclosed in angled brackets: ``<``, ``>``.

Finally, we have the variable ``cls`` in right object position. We are looking for values of the ``cls`` which result in
a true triple expression - things Pulp Fiction is an instance of.

Instances of a class
~~~~~~~~~~~~~~~~~~~~

Having found all the classes Pulp Fiction is in, you may want to find objects which are similar to Pulp Fiction - things
in the same classes. Among the the classes returned by query above is the movie class: ``aio:Movie``. We can now run
this query to find other movies:

.. code-block::

  query mov
  mov <aio:isAnInstanceOf> <aio:Movie>

Here, ``mov`` is variable (this time in left object position), while in right object position we have a class ID. Class
IDs consist of the namespace ``aio`` and class name, which must be PascalCased. Again, it may contain numbers but must
begin with a letter.

We can read this query as "give me all values of ``mov`` such that mov is an instance of movie" - give me movies!

Query line order
~~~~~~~~~~~~~~~~

Suppose we want to grab the director of Pulp Fiction and then grab all everything directed by that person. We would write

.. code-block::

  query mov
  dir <aio:directed> <aie:pulp_fiction_movie>
  dir <aio:directed> mov

This query demonstrates the importance of query line order. AIQL does not allow for the querying of two variables
simultaneously. But we have two variables in the second line, so what's going on?

The AIQL engine processes query lines in sequence, rather than simultaneously. The first line grabs all values of
``dir`` which are movies. These values are then substituted for ``dir`` in any subsequent lines in which ``dir``
appears. So, in effect, the last line being run is

.. code-block::

  <aie:quentin_tarantino> <aio:directed> mov

Flipping the query lines would result in an invalid query:

.. code-block::

  query mov
  dir <aio:directed> mov
  dir <aio:directed> <aie:pulp_fiction_movie>

Filtering
~~~~~~~~~

In general, you can provide arbitrarily many filters on your results - just by adding query lines. For instance having
grabbed everything directed by the director of Pulp Fiction, we can add the constraint that the results are movies
(filter out anything that is not a movie).

.. code-block::

  query mov
  dir <aio:directed> <aie:pulp_fiction_movie>
  dir <aio:directed> mov
  mov <aio:isAnInstanceOf> <aio:Movie>

This is results in a smaller results set, since Tarantino has directed TV episodes as well as movies. We can filter this
even further, by restricting to movies published in a given year. Years are literals within the ``DateTime`` class. They
consist of a string (denoting the DateTime in YYYY-MM-DD format), followed by the name of their datatype:

.. code-block::

  query mov
  dir <aio:directed> <aie:pulp_fiction_movie>
  dir <aio:directed> mov
  mov <aio:isAnInstanceOf> <aio:Movie>
  mov <aio:wasPublishedAtTimepoint> <"1994"^^aio:DateTime>

DateTimes can be denoted with varying levels of precision - to the year, to month, or to the day. For instance, the
following three queries each return the result ``aie:jackie_brown_movie``.

.. code-block::

  query mov
  <aie:quentin_tarantino> <aio:directed> mov
  mov <aio:wasPublishedAtTimepoint> <"1997-12-25"^^aio:DateTime>

.. code-block::

  query mov
  <aie:quentin_tarantino> <aio:directed> mov
  mov <aio:wasPublishedAtTimepoint> <"1997-12"^^aio:DateTime>

.. code-block::

  query mov
  <aie:quentin_tarantino> <aio:directed> mov
  mov <aio:wasPublishedAtTimepoint> <"1997"^^aio:DateTime>

Time
~~~~

The knowledge graph doesn't just represent release times - it includes a full temporal model. Facts are true at given
times, rather than simply true or false. In most cases, facts are either true forever, have a start and end date, or
have a start date but are true indefinitely after. The query engine has inferencing rules to automatically handle
intuitive entailments between these cases. For instance, if a fact has a start date of 1990 and an end date of 2000,
then there's a rule which says that for any point between 1990 and 2000, the fact is true at that point.

Most of the time you won't need to worry about the temporal model. It's there in the background doing its work without
you worrying about it. Within queries, all statements default to truth at the current moment of time. For instance

.. code-block::

  query act
  act <aio:isAnInstanceOf> <aio:Actor>

Is equivalent to the query (assuming today is 8th of August 2019)

.. code-block::

  query act
  act <aio:isAnInstanceOf> <aio:Actor> @ <"2019-08-08"^^aio:DateTime>

(Being an actor is a role which a human being fulfills for a time - just like any other vocation. Though it will always
be true that Brad Pitt acted in Se7en, when Brad Pitt retires, it will cease to be true that he is an actor.)

We can query for facts which have ever been true with the construction

.. code-block::

  query act
  act <aio:isAnInstanceOf> <aio:Actor> @ <aio:anytime>

This will return anyone who has ever been an actor - whether or not they are an actor now.

Queries for multiple variables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If a query is for two or more variables, the query lines must be simultaneously satisfied by the variable values.

For instance, the query:

.. code-block::

  query mov, dir
  mov <aio:isAnInstanceOf> <aio:Movie>
  dir <aio:directed> mov

Will return pairs of objects like

.. code-block::

  mov: <aie:jaws>
  dir: <aie:steven_spielberg>

.. code-block::

  mov: <aie:pulp_fiction_movie>
  dir: <aie:quentin_tarantino>

It will not return

.. code-block::

  mov: <aie:pulp_fiction_movie>
  dir: <aie:steven_spielberg>

because

.. code-block::

  <aie:steven_spielberg> <aio:directed> <aie:jaws>

is false.

Foreign keys
~~~~~~~~~~~~

You can grab foreign keys - IDs for objects in other databases - with the following relations:

.. code-block::

  <aio:isTheWikidataIdOf>
  <aio:isTheImdbIdOf>
  <aio:isTheMusicbrainzIdFor>
  <aio:isTheGoodReadsIdFor>

Try using the a simple query like:

.. code-block::

  query id
  id <aio:isTheImdbIdOf> <aie:pulp_fiction_movie>

This will allow you to grab the ID for an entity in our graph, and then run queries against that object in other
databases. For instance, we may know a lot about a movie, but not know its producer. That's fine - just go and look it
up in IMDB!

Labels
~~~~~~

You can look up the preferred term for objects in the graph with the ``aio:prefLabel`` relation. This will give you a
term that you can slot into sentence positions (most of the time!). The query

.. code-block::

  query lab
  <aie:pulp_fiction_movie> <aio:prefLabel> lab

will return the string "Pulp Fiction".

Quantities
~~~~~~~~~~

A quantity is a measurable amount of something, such as a distance, age, or amount of money. As a rule of thumb,
anything which can be represented as a number (or value) together with a unit, is a quantity.

In the Alexa Information Ontology, we represent quantities as a value string (a number) concatenated with the
identifier of the relevant unit. For instance, the query

.. code-block::

  query alt
  alt <aio:isTheElevationOf> <aie:hollywood_ca>

returns the altitude of Hollywood (California) as an ``aio:Quantity``.

.. code-block::

  alt: "108<aio:meter>"

Units, such as ``aio:meter``, and the type quantities they express (``aio:Length``) are documented in the Alexa
Information Ontology (see ``unitsontology-core.ttl``).

.. tip::

  Take inspiration from the following code snippet to decompose quantities returned by the API as strings.

  .. code-block::

    def parse_quantity(value):
        m = re.match(r"([+-]?\d*\.?\d*)<([a-z0-9-]+:\w+)>", value)
        if m:
            return {
                "value": m.group(1),
                "unit": m.group(2)
            }
        else:
            return None

Reference Documentation
-----------------------

.. _ontology:

Ontology
~~~~~~~~

The ontology describes how concepts are organized and represented in the knowledge graph. Use it to search for the
identifiers of classes and properties appearing in your queries.

.. toctree::

   alexa_information_ontology/index

Machine-readable version of the ontology (OWL, serialization format: RDF Turtle):

* `doc/alexa_information_ontology/entertainmentontology-core.ttl <../alexa_information_ontology/entertainmentontology-core.ttl>`_
* `doc/alexa_information_ontology/unitsontology-core.ttl <../alexa_information_ontology/unitsontology-core.ttl>`_

.. _query_language_syntax:

Query Language Syntax
~~~~~~~~~~~~~~~~~~~~~

The `ANTLR <https://www.antlr.org/>`_ grammar of the Alexa Information Query Language is included below for clients
wanting to validate the syntax of their queries.

**Parser rules:**

.. code-block:: ANTLR

  parser grammar AIQLParser;

  options { tokenVocab = AIQLLexer; }

  query
      : header body EOF
      ;

  header
      : QueryKeyword declarations?
      ;

  declarations
      : Variable (VariableSeparator Variable)*
      ;

  body
      : (LineSeparator queryLine)+
      ;

  queryLine
      : object (Negation)? object object (Modifier object)?
      ;

  object
      : AtomicObject # AtomicObject
      | Variable # Variable
      | AtomicLiteral # AtomicLiteral
      ;

**Lexer rules:**

.. code-block:: ANTLR

  lexer grammar AIQLLexer;

  QueryKeyword
      : 'query' -> mode(QUERY)
      ;

  mode QUERY;

  Negation
      : '~'
      ;

  Variable
      : [a-z]([A-Z]|[a-z]|[0-9]|'_')*
      ;

  AtomicObject
      : '<' (Namespace ':') (Class|Relation|Individual) '>'
      ;

  fragment Namespace
      : ([a-z]|[0-9]) (([a-z]|[0-9]|'-')* ([a-z]|[0-9]))?
      ;

  fragment Class
      : ([A-Z]) ([a-z]|[A-Z]|[0-9]|'_')*
      ;

  fragment Relation
      : ([a-z]) ([a-z]|[A-Z]|[0-9]|'_')*
      ;

  fragment Individual
      : ([a-z]|[A-Z]|[0-9]|'-'|'_'|'.'|'~')+
      ;

  AtomicLiteral
      : '<"' (LiteralContent) '"^^' (Namespace) ':' (Class) '>'
      ;

  fragment LiteralContent
      : (.)*?
      ;

  VariableSeparator
      : ','
      ;

  LineSeparator
      : '\n'+
      | '|'
      ;

  Modifier
      : '@'
      ;

  Spaces
      : ' '+ -> skip
      ;
