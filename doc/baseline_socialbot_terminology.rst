.. _baseline_socialbot_terminology:

Baseline Socialbot Terminology
==============================

----------------------------------------------------
Additional State Manager Fields In Intent Classifier
----------------------------------------------------

.. image:: images/state_manager_new_fields.png

Note: Additional information on require_confirmation

Set: switch_topic, switch_miniskill, no_dynamo_db_pull, pull_different_topic_in_thread, start with “did you know” for pull_thread_for_topic handlers, global_invalid_handler

Used: register_user_response_information (in Intent Classifier), global_invalid_handler_selection_features_factory

The purpose of having require_confirmation stored in the StateManager is to check whether or not we should run an instance of EstimateUserDecision.  
There are some regexes in EstimateUserDecision which overlap with the other user information estimators and if the previous response expects a 
yes/no response, we want the program to prioritize checking for and classifying the utterance as user decision.  Therefore, 
register_user_response_information checks if require_confirmation from last state is true, only runs EstimateUserDecision if it is and skips all 
other user response information checking if the utterance is a user decision.  It is used in global_invalid_handler_selection_features_factory to 
determine whether or not we should suggest to continue the conversation on the old topic.  As the responses are made in the handlers, we set 
require_confirmation to true in the handler whenever the response will expect a yes/no answer.  

--------------------------
Inputs In The NLP Pipeline
--------------------------

Current Turn
^^^^^^^^^^^^

corrected_topic: self.state_manager.current_state.corrected_topic
    the corrected topic used in the intent classifier

current_corrected_ask_intent: self.state_manager.current_state.corrected_ask_intent
    the ask intent we have corrected in step 4 of the intent classifier

current_intent: self.state_manager.current_state.intent
    the intent from the ASK console

miniskill_to_turn_used: self.state_manager.current_state.miniskill_to_turn_used
    a dictionary with the most recent turn number of each miniskill used

saved_intent: self.state_manager.current_state.saved_intent
    if there is a topic exhaustion in step 6 of the intent classifier, we need to know which miniskill was picked first to generate the response

text: self.state_manager.current_state.text
    the best ASR text from current turn

text_from_navigation: self.state_manager.current_state.slots['navigation']['value']
    text from the navigation slot, which is filled if the user utterance does not trigger any other intent in the intent schema

slots: self.state_manager.current_state.slots
    the slots from the intent schema in the ASK console


Last Turn 
^^^^^^^^^

last_state_corrected_ask_intent: self.state_manager.last_state.get('corrected_ask_intent')
    the corrected ASK intent from the last state

last_state_intent: self.state_manager.last_state.get('intent')
    the intent from the last state

last_state_new_topic: self.state_manager.last_state.get('new_topic')
    the new topic from the last turn

last_state_old_topic: self.state_manager.last_state.get('old_topic')
    the old topic from the last state

last_state_require_confirmation: self.state_manager.last_state.get('require_confirmation')
    whether or not the last state is expecting a confirmation response (yes/no)

last_state_response: self.state_manager.last_state.get('response')
    bot response from the last state

last_state_saved_intent: self.state_manager.last_state.get('saved_intent')
    | any intent we want saved, we mainly use this to save intents from previous turns to be used again after an invalid state
    | for a turn after an invalid state, we will use logic to try and pass this intent through our intent classifier checks
    (for topic existance, etc) to reuse the same miniskill the user was on previously

last_state_user_response_information: self.state_manager.last_state.get('user_response_information')
    the user response information dictionary from last turn

last_state_visited_threads: self.state_manager.last_state.get('visited_threads')
    all visited threads up until the previous turn

turn_number: self.state_manager.last_state.get('turn_number')
    last state's turn number

Clarification between old_topic, new_topic, and corrected_topic:

Old_topic is the user topic from last turn we want to save for future turns

New_topic is the topic Alexa recommends for this turn

Corrected_topic is the topic we edit to in the IntentClassifier