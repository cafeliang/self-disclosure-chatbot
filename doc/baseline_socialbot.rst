.. _baseline_socialbot:

Baseline Socialbot
==================

-------------------------------
What Is The Baseline Socialbot?
-------------------------------

The baseline socialbot is meant to act as an example socialbot for you to play around with and develop off of.
It has examples of modules that you can override, add more of, or remove.  See `Building The Baseline Socialbot <baseline_socialbot_build.html>`_ for how to build the
bot from scratch.  Also, refer to `Extending The Baseline Socialbot <baseline_socialbot_extension.html>`_ for more information on how to create and add a very simple miniskill.

-------------------------------
Baseline Socialbot Architecture
-------------------------------

Below, we will walk through the architecture and explain how each module works.  We will also link to
documentation on how to override these modules.

.. image:: images/cobot_architecture.png

ASR Handler
^^^^^^^^^^^

The `ASR handler`__ is where you can make edits to the ASR text passed from the ASK console before it enters
the rest of the pipeline.

.. __: overriding_cobot_components.html#overriding-asr-processor

Global Intent Handler
^^^^^^^^^^^^^^^^^^^^^

The `Global Intent Handler`__ handles some intents that do not need the text to go through the entire pipeline, for example,
LaunchRequest, StopIntent, or CancelIntent.  Feel free to customize the output for the LaunchRequest,
however; remember not to mention any identifying characteristics for your bot (or else it's a content violation :o).
Be sure to also keep the welcome prompt!

.. __: overriding_cobot_components.html#overriding-global-intent-handler

NLP Pipeline
^^^^^^^^^^^^

Refer to `Baseline Socialbot NLP Pipeline`__ 

.. __: baseline_socialbot_nlp_pipeline.html

Dialog Manager
^^^^^^^^^^^^^^

We have only overridden the `selecting strategy`__ in the baseline socialbot.  We can find this file in miniskills/custom_nlp/custom_selecting_strategy.py.
The selecting strategy is quite similar to the existing selecting strategy.  However, we are checking the corrected_ask_intent from the state manager 
instead of the ASK intent to pick a response generator.  The mapping of the intents to the response generators can be found in the intent_map in 
miniskills/constants/miniskill_list.py.  For our reference, a table is shown below:

.. __: baseline_socialbot_build.html#overriding-the-selecting-strategy

.. image:: images/intent_to_miniskill_map.png

Response Generators
^^^^^^^^^^^^^^^^^^^

Refer to `Baseline Socialbot Response Generators`__

.. __: baseline_socialbot_response_generators.html

Response Builder
^^^^^^^^^^^^^^^^

We have built a custom reprompt builder that can be found in miniskills/custom_nlp/miniskill_reprompt_builder.py.  We can set our reprompts
in the NLG handlers and save them to the state manager while we are creating our responses for further customization.  Our reprompt builder 
overrides the build method from RepromptBuilder and returns the reprompt we saved in the state manager.

To potentially combine responses of miniskills, we would override first the SelectingStrategy to choose more than one 
miniskill, then the RankingStrategy to combine responses as they come in.