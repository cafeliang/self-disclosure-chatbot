.. _baseline_socialbot_extension:

Extending on the Baseline Socialbot
===================================

-------------------------
Adding A Simple Miniskill
-------------------------

In this section, we will be showing how to add a simple miniskill which will leverage
the knowledge the Evi knowledge module has pulled.

Intent Schema
^^^^^^^^^^^^^

We will want to go to the ASK developer console for this step.  On the left hand side, we can see that there are all the different intents for our socialbot.
Now, we can add a new intent called AskQuestionIntent, which will map to our miniskill.

Under the section for RequestFactIntent, we can add the following:

.. code-block:: json

    {
        "name": "AskQuestionIntent",
        "slots": [
            {
                "name": "continuer",
                "type": "LIST_OF_CONTINUER"
            },
            {
                "name": "wh_word",
                "type": "LIST_OF_WH_WORD"
            },
            {
                "name": "topic",
                "type": "LIST_OF_TOPIC"
            },
            {
                "name": "text",
                "type": "AMAZON.RAW_TEXT"
            }
        ],
        "samples": [
            "{wh_word} is {text}",
            "{wh_word} is {topic}",
            "{wh_word} are {topic}",
            "do you know {wh_word} is {topic}",
            "do you know {wh_word} are {topic}",
            "do you know {wh_word} {topic} is {topic}",
            "tell me {wh_word} is {topic}",
            "tell me {wh_word} are {topic}",
            "i want to know {wh_word} is {topic}",
            "i want to know {wh_word} are {topic}",
            "i wonder {wh_word} is {topic}",
            "i wonder {wh_word} are {topic}",
            "i'm curious {wh_word} is {topic}",
            "i'm curious {wh_word} are {topic}"
        ]
    },

We can see that we've also added a new slot type called "wh_word".  We will now define this slot type under "types".

.. code-block:: json

    {
        "name": "LIST_OF_WH_WORD",
        "values": [
            {
                "id": "wh_word",
                "name": {
                    "value": "who",
                    "synonyms": [
                        "where",
                        "what"
                    ]
                }
            }
        ]
    },

Now we can use these words and sample utterances to initially route to our miniskill!

Edit the constant files
^^^^^^^^^^^^^^^^^^^^^^^

We will be calling this miniskill AskQuestion, and the corresponding intent will be AskQuestionIntent.

1. miniskills/constants/miniskills.py

Define the name of the miniskill on line 10.

.. code-block:: python

    ASKQUESTION = 'AskQuestion'

2. miniskills/constants/intent_names.py

We will add the intent to the list of intent names on line 21.

.. code-block:: python

    ask_question_intent = 'AskQuestionIntent'

3. miniskills/constants/intent_to_miniskill_map.py

Next, make sure to map the intent to the corresponding miniskill on line 12.
This will be used for all miniskills with intents (including the global response generators).

.. code-block:: python

    IntentNames.ask_question_intent: Miniskills.ASKQUESTION

4. miniskills/constants/miniskill_list.py

Since we want to put the miniskill in rotation, we want to add it to skill_list_config in line 8, and the intent map in line 30.
However, we will not put it as an informative miniskill.
This intent map will be used for all miniskills made from the response generator template we have provided.

.. code-block:: python

    skill_list_config = [Miniskills.SHAREFACTS, Miniskills.SHOWERTHOUGHTS, Miniskills.NEWS, Miniskills.MOVIES, Miniskills.ASKQUESTION]

.. code-block:: python

    elif skill == Miniskills.ASKQUESTION:
  		intent_map['AskQuestionIntent'] = 'ASKQUESTION'

5. miniskills/constants/responses.py

Let's have some custom responses for this miniskill!  At the end of the file, line 690, let's add the following:

.. code-block:: python

    THEY_WERE_A = ['They were a ']

    NO_EVI_RESPONSE_APOLOGY = ['Sorry, I don\'t know, ask me about something else?']

    EVI_REPROMPT = ['Can I answer anything else?']


Extending From Base Handlers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We have two types of `handlers`__ to implement: an information retrieval handler and an NLG handler.  The information retrieval handler does what its name implies;
it retrieves information from some source which you can define.  The NLG handler will use this information to craft a response.  The response generator will take advantage
of both of these types of handlers to ultimately output a response.

Let's create a new folder under miniskills with the name of our miniskill: miniskills/ask_question.

.. __: baseline_socialbot_explanation.html

Information Retrieval Handler
-----------------------------

Each handler has two main methods, can_handle and handle.  Can_handle determines whether or not this handler is called and handle executes the action.
For the handler we are making, we will extend from NoDynamoDbPullHandler, which is a base handler with some premade methods found in miniskills/base_handlers/no_dynamo_db_pull_handler.py.  
We will be keeping can_handle the same and changing handle.  Handle calls dynamo_db_pull, which handles any information pull (and in this case, the lack of one), and returns to the central dispatcher.  
Now, let's create the file miniskills/ask_question/ask_question_no_dynamo_db_pull_handler.py with the code below.

.. code-block:: python

    from miniskills.base_handlers.no_dynamo_db_pull_handler import NoDynamoDbPullHandler
    from miniskills.constants.responses import Responses
    import random
    
    class AskQuestionNoDynamoDbPullHandler(NoDynamoDbPullHandler):
        """
        Basic template to handle no dynamodb pull instances
        """
    
        def handle(self, handler_dispatcher, handler_selection_features):
            self.dynamo_db_pull(handler_selection_features)
            return handler_dispatcher.dispatch(handler_dispatcher, handler_selection_features)

Dynamo_db_pull will pull information from our Evi knowledge module that we've created previously.

.. code-block:: python

    def dynamo_db_pull(self, handler_selection_features):
        self._copy_previous_state_new_attributes_to_current_state(handler_selection_features.state_manager)
        handler_selection_features.state_manager.current_state.require_confirmation = False
        if handler_selection_features.state_manager.current_state.features['evi']['asr_text'] is not None:
            handler_selection_features.nlg_content =  handler_selection_features.state_manager.current_state.features['evi']['asr_text']
        elif handler_selection_features.state_manager.current_state.features['evi']['current_topic_from_slots'] is not None:
            handler_selection_features.nlg_content = handler_selection_features.state_manager.current_state.features['evi']['current_topic_from_slots']
        else:
            handler_selection_features.nlg_content = random.choice(Responses.NO_EVI_RESPONSE_APOLOGY)

NLG Handler
-----------

We will once again extend from a handler template, NoDynamoDbPullNlgHandler, found in miniskills/base_handlers/no_dynamo_db_pull_nlg_handler.py.  We will keep this handler's can_handle method, but change handle.
Handle calls nlg_template.  Let's put the code below in a file called miniskills/ask_question/ask_question_nlg_handlers.py

.. code-block:: python

    from miniskills.base_nlg_handlers.no_dynamo_db_pull_nlg_handler import NoDynamoDbPullNlgHandler
    from miniskills.constants.responses import Responses
 
    class AskQuestionNoDynamoDbPullNlgHandler(NoDynamoDbPullNlgHandler):
        """
        Handle no dynamodb pulls for EVIQuestion Answer
        """
        def handle(self, handler_dispatcher, handler_selection_features):
            """
            Return a completed response, input HandlerSelectionFeatures object and output response string
            """
            return self.nlg_template(handler_selection_features)

In nlg_template, we will take the Evi response we have retrieved in the information retrieval handler and return it as a response.

.. code-block:: python

    def nlg_template(self, handler_selection_features):
        reprompt = Responses.EVI_REPROMPT
        response_and_reprompt = {
            'response': handler_selection_features.nlg_content,
            'reprompt': reprompt
        }
    return response_and_reprompt

Response will now be the response we return to the user.

Creating The Response Generator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Similar to the ShareFacts response generator, this response generator will also inherit from HandlerDispatchResponseGenerator.
We will be implementing two `handlers`__, AskQuestionNoDynamoDbPullHandler and AskQuestionNoDynamoDbPullNlgHandler since we will not be pulling from 
DynamoDB tables.  Let's call this file ask_question/ask_question_response_generator.py.

.. __: baseline_socialbot_explanation.html

.. code-block:: python

    from miniskills.handler_dispatch_generic_response_generator.handler_dispatch_response_generator import HandlerDispatchResponseGenerator
    from miniskills.ask_question.ask_question_nlg_handlers import AskQuestionNoDynamoDbPullNlgHandler
    from miniskills.ask_question.ask_question_no_dynamo_db_pull_handler import  AskQuestionNoDynamoDbPullHandler

    class AskQuestionResponseGenerator(HandlerDispatchResponseGenerator):
        """
        Initialize list of possible handlers, run dispatch, return just the EVI Question Answer
        """
    
        list_of_handlers = [AskQuestionNoDynamoDbPullHandler(), AskQuestionNoDynamoDbPullNlgHandler()]
    
        def __init__(self, state_manager, module_name, service_module_config, handler_dispatcher = None, list_of_handlers = None):
            super(AskQuestionResponseGenerator, self).__init__(state_manager, module_name, service_module_config, handler_dispatcher, self.list_of_handlers)

Adding The Response Generator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We will now add the response generator into [your_cobot_directory]/baseline_bot.py on line 196.  

.. code-block:: python

    from miniskills.ask_question.ask_question_response_generator import AskQuestionResponseGenerator

    AskQuestion = {
        'name': 'ASKQUESTION',
        'class': AskQuestionResponseGenerator,
        'url': 'local'
    }

Then we will add it to the list of response generators in line 202 of the same file.

.. code-block:: python

    cobot.add_response_generators([ShareFacts, Launch, Invalid, SpecialUserUtterance, ShowerThoughts, News, Greeting, Goodbye, Movies, AskQuestion])

Connecting The Response Generator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Intent Classifier
-----------------

First, we need to make sure that we can route to the correct intent for this miniskill.  Each miniskill is associated with a single intent so our selecting strategy
only has the simple logic of mapping the intent to the miniskill with a dictionary.

We can see in miniskills/custom_nlp/corrected_ask_intent_classifier.py that there are 7 steps that go into creating the corrected_ask_intent to send to the selecting strategy.
However, we will only concern ourselves with two of these steps for now, step 4, which generates the corrected_ask_intent, and step 5, which checks if the topic exists and edits the intent accordingly. 

For step 4, going to the file miniskills/custom_nlp/generate_corrected_ask_intent.py, on line 36, we will want to edit in this way:

.. code-block:: python

    elif current_intent == IntentNames.amazon_stop_intent or current_intent == IntentNames.ask_question_intent:
        temp_corrected_ask_intent = current_intent

This ensures that the generated corrected_ask_intent will remain ask_question_intent.

For step 5, returning to miniskills/custom_nlp/corrected_ask_intent_classifier.py, we will want to add the following on line 122:

.. code-block:: python

    if current_corrected_ask_intent == IntentNames.ask_question_intent:
        return None

This ensures that we will not change the intent if the topic is not found in the DynamoDB tables (since we want Evi to return responses).

Handler Dispatch
----------------

Now that we can route to the correct response generator, we will want to make sure that the handler_dispatch system can route to the correct handlers.
We will go to miniskills/handler_dispatch_generic_response_generator/update_action_and_speech_name.py and add the following on line 162:

.. code-block:: python

    # For any final overrides on action and speech action name, one can add the override statement here.
    # current_corrected_ask_intent is the current intent that user utterance got mapped to after running through our intent classifier.
    if current_corrected_ask_intent == IntentNames.ask_question_intent:
        action_name = ActionNames.NO_DYNAMO_DB_PULL
        speech_action_name = SpeechActionNames.CONTINUE
        action_and_speech_action_names.append(action_name)
        action_and_speech_action_names.append(speech_action_name)

This overrides anything that has been done before, and will ensure that we route to the two handlers we have specified in the config list in our response generator.

Once we've done this, we are all set to launch our new miniskill!  Run update lambda and test out your miniskill with the sample utterances :)