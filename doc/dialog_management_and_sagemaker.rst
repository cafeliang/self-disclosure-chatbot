.. _dialog_management_and_sagemaker:

Building a Dialog Manager hosted by AWS SageMaker
=================================================

This example serves two purposes - first, it illustrates a basic strategy for building a LearnedSelectingStrategy. The LearnedSelectingStrategy we will build maps between intent sequences and response generators, from a set of example inputs.

Second, it illustrates the use of AWS SageMaker to train and host models for inference within a Cobot deployment.
 
-------------
AWS SageMaker
-------------

SageMaker provides two major functions - 1) integrated modeling and at-scale training of Tensorflow, MXNet, and Keras models 2) hosting and inference with those models.

Internally, SageMaker inference is very similar to using a Cobot remote module - inference calls are serialized into an HTTP invocation to a Flask service, hosted within a Docker module deployed in ECS. However, for relatively straightforward modeling needs, SageMaker requires somewhat less code to manage and handles model training, which is not managed directly in the Cobot toolkit.

Our simple learned Dialog Manager uses MXNet/Gluon and the custom training/hosting interface in SageMaker. Building a SageMaker model file requires implementing four methods in your model Python module:

.. code-block:: python

   # Training functions
   def train(channel_input_dirs, hyperparameters, hosts, num_gpus, **kwargs):
       ...

   def save(net, model_dir):
       ...

   # Inference/hosting functions
   def model_fn(model_dir):
       """
       Loads a model from disk, reading from model_dir. Called once by each inference service worker when it is started.
       :param model_dir: The Amazon SageMaker model directory.
       :return: The deserialized Gluon model
       """
       ...

   def transform_fn(net, data, input_content_type, output_content_type):
       """
       Transform a request using the Gluon model. Called once per request.

       :param net: The Gluon model, as loaded by model_fn
       :param data: The request payload.
       :param input_content_type: The request content type.
       :param output_content_type: The (desired) response content type.
       :return: response payload and content type.
       """

For more details, see https://docs.aws.amazon.com/sagemaker/latest/dg/mxnet-training-inference-code-template.html. Complete source code for the SageMaker model is located in sample_cobot/dialog_model.py.


---------------------
Training Control Code
---------------------

The training control code is found in sample_cobot/dialog_train.py.

.. code-block:: python

   from sagemaker.mxnet import MXNet
   import boto3
   from cobot_common.vectorize import Vectorizer
   import numpy

   ##########
   # Data Preparation
   ##########
   intent_map = { 'QAIntent':'EVI', 'greet':'GREETER', 'yes_intent':'GREETER', 'topic_request':'GREETER', 'more_info_request':'GREETER',
                  'no_intent':'GREETER', 'mood_great':'GREETER', 'mood_unhappy':'GREETER', 'love':'GREETER', 'hate':'GREETER', 'opinion_request':'GREETER',
                  'frustration':'GREETER', 'error':'GREETER' }
   turns = []
   for i in intent_map.keys():
       turns.append([i,intent_map[i]]) 

   vectorizer = Vectorizer(['EVI','GREETER'],model_file="cobot-skill/models/en-US.json")
   (inputs,outputs) = vectorizer.vectorize_turns(turns)
   input_arr = numpy.array(inputs)
   output_arr = numpy.array(outputs)

   numpy.savez('train.npz',inputs=input_arr,outputs=output_arr)
   numpy.savez('test.npz',inputs=input_arr,outputs=output_arr)

   ##########
   # Model Training
   ##########
   train = open('train.npz', 'rb')
   test = open('test.npz', 'rb')
   s3 = boto3.resource('s3')
   s3.Bucket('rcg-cobot-sagemaker-data').put_object(Key='train.npz', Body=train)
   s3.Bucket('rcg-cobot-sagemaker-data').put_object(Key='test.npz', Body=test)

   mxnet_estimator = MXNet("dialog_model.py", role="SageMakerRole", train_instance_type="ml.m4.xlarge", train_instance_count=1)
   mxnet_estimator.fit("s3://rcg-cobot-sagemaker-data/")

   predictor = mxnet_estimator.deploy(instance_type="ml.m4.xlarge",
                                      initial_instance_count=1,
                                      endpoint_name="sagemaker-dialog-model-test")

   data = numpy.load(open('train.npz', 'rb'))
   inputs = data['inputs']
   outputs = data['outputs']

   for row in zip(inputs,outputs):
       print(row[0].tolist())
       print(row[1].tolist())
       print(predictor.predict([row[0].tolist()]))

dialog_train.py is run locally, and sets up and runs the training process in your AWS account, then deploys a SageMaker estimator hosting the dialog model at the sagemaker-dialog-model-test endpoint described above. You'll need to set your own S3 bucket name above. For large scale training jobs, you will want to use GPU instance types.


---------------------------
Running Your Model in Cobot
---------------------------

Now that we've trained a simple model, we want to deploy it into our Cobot instance.

.. code-block:: python

   from cobot_core.service_module_config import ServiceModules
   from injector import inject, Key
   from cobot_core.state_manager import StateManager
   from cobot_core.selecting_strategy import SelectingStrategy
   from typing import Dict, List, Any

   EndpointName = Key("endpoint_name")

   class LearnedSelectingStrategy(SelectingStrategy):

       """
       LearnedSelectingStrategy is a SelectingStrategy that learns a mapping from intent sequences (and potentially other state information) to ResponseGenerator modules.

       This implementation uses a SageMaker endpoint, which is trained and deployed offline (see dialog_train.py). The endpoint name is passed in as an argument at initialization time.

       """

       @inject
       def __init__(self, endpoint_name: EndpointName, state_manager: StateManager):
           self.endpoint_name = endpoint_name
           self.state_manager = state_manager

       def select_response_mode(self, features:Dict[str,Any])->List[str]:
           """
           Returns the list of valid ResponseGenerator modules for the given input
           """
           intent = features['intent']
           all_rgs = ServiceModules.ResponseGeneratorModulesMap.keys()

           vectorizer = Vectorizer(all_rgs,model_file="cobot-skill/models/en-US.json")
           turns = [[intent,'']]

           (inputs,outputs) = vectorizer.vectorize_turns(turns)
           predictor = MXNetPredictor('sagemaker-dialog-model-test')

           input = inputs[0]
           result = int(predictor.predict([input]))
           #print(result)
           result = vectorizer.invert_output(numpy.array([result]))
           #print(result)

           return result

And we'll need to update our main bot module's overrides as follows to replace the default Dialog Manager's SelectingStrategy:

.. code-block:: python

   def overrides(binder):
       binder.bind(Cobot.SelectingStrategy, to=LearnedSelectingStrategy)

In this example, very little logic lives within the LearnedSelectingStrategy - we left the vectorization of our model inputs there, but otherwise, it is merely a lightweight frontend to an MXNetPredictor call to our SageMaker endpoint.