
Direct super classes:
        * aio:BiologicalTaxon

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isAPhylumInTheKingdom
