
Direct super classes:
        * aio:CreativeWork

Direct sub classes:
        * aio:VideoGame

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
