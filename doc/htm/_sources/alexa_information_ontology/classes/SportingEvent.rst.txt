
Direct super classes:
        * aio:Event

Direct sub classes:
        * aio:OlympicGames

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
