
Direct super classes:
        * aio:PopulatedPlace

Direct sub classes:
        * aio:UkParliamentaryConstituency

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
