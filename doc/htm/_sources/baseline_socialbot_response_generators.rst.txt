.. _baseline_socialbot_response_generators:

Baseline Socialbot Response Generators
======================================

------------------------
Response Generator Types
------------------------

.. image:: images/handler_dispatch_system.png

Handler Dispatch Response Generator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are four components to this response generator template: the response generator itself
(HandlerDispatchResponseGenerator), the factory which creates HandlerSelectionFeatures which is used 
to determine which handlers will be called (HandlerSelectionFeaturesFactory), the dispatcher which
acts as the flow director between different handlers (HandlerDispatcher), and all the different handlers (Handler).
All files referenced below are within the miniskills/handler_dispatch_generic_response_generator folder.

The HandlerDispatchResponseGenerator will first call the HandlerSelectionFeaturesFactory to create the HandlerSelectionFeatures.
A caveat to this is that we save state manager in handler_selection_features, and we may save extra state manager variables throughout our handlers to it.
Here, there are three steps we have to go through before creating the HandlerSelectionFeatures.  The first step is to select the DynamoDB tables we will
be looking at by calling the execute method of SelectDynamoDbTables.  We will use the current_corrected_ask_intent, current_corrected_topic, last_state_old_topic,
and visited_threads as input to the method.  This method returns a list where the first entry is the topic table name and second entry is the thread
table name.  We will then initialize the topic table and the thread table based on our method into topic_table_name 
and thread_table_name in the HandlerSelectionFeatures object.  A full mapping of the miniskills to the DynamoDB tables can be found in miniskills/constants/intent_to_dynamo_db_table_map.py.  

The second step is to update the action and speech action name.  These will be the main variables used in the handlers.  We will include the 
user_response_information, topic_table_name, last_state_require_confirmation, last_state_saved_intent, current_corrected_ask_intent,
last_state_corrected_ask_intent, last_state_intent, current_intent, last_state_response, and current_corrected_topic as inputs to the execute method of 
UpdateActionAndSpeechActionName.  This will return a list with the first entry as the action name and the second entry as the speech action name, which we will 
then set in action_name and speech_action_name in the HandlerSelectionFeatures object.

Below are a few tables which show all possible mappings between inputs and action mappings.

User Satisfaction Action Map 

.. image:: images/user_satisfaction_action_map.png

User Side Action Map

.. image:: images/user_side_action_map.png

User Knowledge Action Map

.. image:: images/user_knowledge_action_map.png

User Decision Action Map

.. image:: images/user_decision_action_map.png

Navigation Action Map

.. image:: images/navigation_action_map.png

Other Inputs Action Map

.. image:: images/extras_action_map.png

The third and last step is to update the corrected topic if we have changed it in any previous step.  We will use user_response_information,
last_state_saved_intent, current_corrected_ask_intent, last_state_corrected_ask_intent, last_state_intent, last_state_user_response_information,
last_state_new_topic, last_state_old_topic, current_intent, and current_corrected_topic as inputs to the execute method of UpdateCorrectedTopic.  
This method has some logic that will change the topic we talk about based on a set of rules, and outputs a new topic if there is one.

Then, it will call HandlerDispatcher's dispatch method, which will go through each Handler we have specified for the specific
response generator and call can_handle.  The main variables checked in can_handle are action_name, which describes what kind of 
action will be taken, and nlg_content, which has the information needed to craft responses.  Each combination of action_name and nlg_content
applies to one handler only.  

There are two types of handlers: information retrieval handlers and NLG handlers.  We have set up this template so that
we can dispatch multiple times to information retrieval handlers until we have gotten satisfactory information, and at that point we will
populate the nlg_content variable and send it off to one NLG handler class, each of which serve as an endpoint for various paths.  
These NLG handlers then return the final response for the response generator.

A mapping of the action_name to the DynamoDB actions (which can be found in miniskills/miniskill_dynamo_db_manager.py) is below.  
These correspond to the information retrieval handlers.

.. image:: images/action_to_dynamodb_action.png

Here are some of the possible paths through the handlers that the response generator can take while creating the response.

.. image:: images/action_dispatch_map.png

Now, we will look at the mapping of speech_action_names to the different types of templates and the information expected.  These mappings 
are used in the NLG handlers.

.. image:: images/speech_action_map.png

Response Generator Model
^^^^^^^^^^^^^^^^^^^^^^^^

This is the neural response generator.

Team's Custom Response Generator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You may wish to have response generators with other structures, so feel free to do so!  The handler dispatch response generator
merely serves as a template for those who wish to implement response generators in the same style.