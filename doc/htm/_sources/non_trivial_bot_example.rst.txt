.. _non_trivial_bot_example:

Building A Non-Trivial Socialbot
================================

This is a Quickstart Guide to building a non-trivial socialbot, starting from the sample_cobot example code. This guide is for the experienced developer or researcher who wants to jump right into building an advanced experience and doesn't want to wade through the full documentation.

0. Install Cobot

Make sure your environment is set up and your initial Cobot source has been pulled following the instructions in `Getting Started`__.

.. __: getting_started.html

1. Set Up Intent Model

Following the instructions from `the intent model section`__, we will replace cobot-skill/models/en-US.json with the following:

.. __: intent_model.html

.. code-block:: bash

  {
  "interactionModel": {
    "languageModel": {
      "invocationName": "greeter",
      "types": [
        {
          "name": "QUESTION_WORD",
          "values": [
            {
              "id": "WHO",
              "name": {
                "value": "who",
                "synonyms": []
              }
            },
            {
              "id": "WHAT",
              "name": {
                "value": "what",
                "synonyms": []
              }
            },
            {
              "id": "WHEN",
              "name": {
                "value": "when",
                "synonyms": []
              }
            },
            {
              "id": "WHERE",
              "name": {
                "value": "where",
                "synonyms": []
              }
            },
            {
              "id": "WHY",
              "name": {
                "value": "why",
                "synonyms": []
              }
            },
            {
              "id": "HOW_OLD",
              "name": {
                "value": "how old",
                "synonyms": []
              }
            }
          ]
        }
      ],
      "intents": [
        {
          "name": "AMAZON.CancelIntent",
          "samples": []
        },
        {
          "name": "AMAZON.HelpIntent",
          "samples": []
        },
        {
          "name": "AMAZON.StopIntent",
          "samples": []
        },
        {
          "name": "greet",
          "slots": [],
          "samples": [
            "hey",
            "hello",
            "hi",
            "hello there",
            "good morning",
            "good evening"
          ]
        },
        {
          "name": "topic_request",
          "slots": [
            {
              "name": "topic",
              "type": "AMAZON.RAW_TEXT"
            }
          ],
          "samples": [
            "let's chat about {topic}",
            "can we talk about {topic}",
            "tell me about {topic}",
            "what can you tell me about {topic}",
            "can we chat about {topic}",
            "can you say something about {topic}",
            "let's talk about {topic}",
            "can we discuss {topic}"
          ]
        },
        {
          "name": "QAIntent",
          "slots": [
            {
              "name": "question_word",
              "type": "QUESTION_WORD"
            },
            {
              "name": "question_text",
              "type": "AMAZON.RAW_TEXT"
            }
          ],
          "samples": [
            "{question_word} {question_text}"
          ]
        },
        {
          "name": "retrieval",
          "slots": [
            {
              "name": "text",
              "type": "AMAZON.RAW_TEXT"
            }
          ],
          "samples": [
            "{text}"
          ]
        }
      ]
    }
    }
  }

2. Ensure that your modules.yaml contains the following:

.. code-block:: bash

   cobot_modules:
       - greeter
       - retrieval

   infrastructure_modules:
       - nlp

3. Edit your greeter_bot.py file to contain the following:

.. code-block:: python

   import cobot_core as Cobot
   from cobot_core.service_url_loader import ServiceURLLoader
   from cobot_core.service_module import RemoteServiceModule
   from cobot_core.service_module import LocalServiceModule
   import random
   import requests
   from bs4 import BeautifulSoup

   ...

   class EviResponseGenerator(LocalServiceModule):
       def execute(self):
	   isWhitelisted = False
	   question_text = ''
	   if not isWhitelisted:
               intent = self.input_data['intent']
               slots = self.input_data['slots']
	       question_word = slots['question_word']['value']
               question_text = question_word+' '+slots['question_text']['value']
           else:
  	       question_text = self.input_data['asr']
               question_text = question_text.replace(' ','_')

           url = "http://www.evi.com/q/"+question_text
           response = requests.get(url)
           soup = BeautifulSoup(response.text, 'html.parser')
           result = soup.find('div', attrs={'class': 'tk_common'})
           print('result:', result.string.strip())
           return result.string.strip()

   def overrides(binder):
       intent_map = { 'QAIntent':'EVI', 'greet':'RULES', 'topic_request':'RULES', 'retrieval':'RETRIEVAL' }
       binder.bind(Cobot.IntentMap, to=intent_map)
       binder.bind(Cobot.SelectingStrategy, to=Cobot.MappingSelectingStrategy)

See `the Extending Dialog Manager documentation`__ for more details.

.. __: expanding_your_bot.html#extending-dialog-manager

4. You will also need to add the following to greeter_bot.py:

.. code-block:: python

       EviBot = {
                'name': "EVI",
                'class': EviResponseGenerator,
                'url': 'local',
                'context_manager_keys': ['intent', 'slots']
       }

       RemoteRuleBot = {
                'name': "RULES",
                'class': RemoteServiceModule,
                'url': ServiceURLLoader.get_url_for_module("GREETER"),
                'context_manager_keys': ['intent','slots']
       }

       RetrievalBot = {
                'name': "RETRIEVAL",
                'class': RemoteServiceModule,
                'url': ServiceURLLoader.get_url_for_module("RETRIEVAL"),
                'context_manager_keys': ['intent','slots']
       }

       cobot.add_response_generators([RemoteRuleBot, EviBot, RetrievalBot])


See `the add_response_generators method documentation`__ for more details.

.. __: cobot_core.html#cobot_core.cobot_handler.CobotHandler.add_response_generators

5. Add the new retrieval module. From the command line run the following:

.. code-block:: bash

   add-response-generator retrieval sample_cobot
   cd $COBOT_HOME/sample_cobot/docker/retrieval/app

6. Edit the ``handle_message(msg)`` method in ``response_generator.py`` to implement the model of your choice. For example:

.. code-block:: python

   from rake_nltk import Rake
   import random

   required_context = ['intent','slots']

   def get_required_context():
       return required_context

   RAKE = Rake()
   REDDIT_DATA_PATH = "data/reddit_retrieval_data.txt"

   def process_user_utterance(utterance):
       RAKE.extract_keywords_from_text(utterance)
       keywords = RAKE.get_ranked_phrases()
       to_remove = set(["chat", "talk", "let"])
       result = []
       for item in list(set(keywords) - to_remove):
           result = result + item.split()
       return result

   def retrieve_subreddits(utterance, entities=None):
       if not entities:
           entities = process_user_utterance(utterance)
       keywords_set = set(entities)
       reddit_data = open(REDDIT_DATA_PATH, "r")
       best_score = 0.5
       best_resp = None
       utterances_with_keywords = set()
       for sent in reddit_data:
           #match_ratio = fuzz.ratio(utterance, sent)
           match_ratio = len(keywords_set.intersection(set(sent.split())))
           if match_ratio > best_score and len(sent.split()) < 15 and len(sent.split()) >5:
               #best_score = match_ratio
               #best_resp = sent
               utterances_with_keywords.add((sent,match_ratio))

       utterances_with_keywords = sorted(utterances_with_keywords,key=lambda x: x[1], reverse=True)[0:20]

       return random.choice(list(utterances_with_keywords))[0]


   def is_black(response, black_list):
       for word in black_list:
           if word in response.split():
               return True
       return False

   def handle_message(msg):
       utterance = msg['slots']['text']['value']
       response_is_black = True
       response = None
       entities = None
       black_list = set(["fuck", "fuk", "sex", "idiot", "maniac", "dick", "butt"])
       while response_is_black:
           response = retrieve_subreddits(utterance, entities)
           if not is_black(response, black_list):
               return response

6. Copy data for the retrieval module. From the ``docker/retrieval/app`` directory, type:

.. code-block:: bash

   cp -R <project-root-directory>/doc/reddit_example_data ./data

7. Add dependencies into requirements.txt:

.. code-block:: bash

   nltk==3.2.5
   rake_nltk==1.0.2
   beautifulsoup4==4.6.0

8. Add new files to git, and add any other modified files as well:

.. code-block:: bash

   git add .
   git commit
   git push "https://git-codecommit.us-east-1.amazonaws.com/v1/repos/cobot"

9. Run cobot update to reflect changed module configuration (note that this step is only required when modules.yaml has been edited):

.. code-block:: bash

  cobot update sample_cobot
