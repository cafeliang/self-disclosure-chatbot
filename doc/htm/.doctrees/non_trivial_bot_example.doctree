���u      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�.. _non_trivial_bot_example:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��non-trivial-bot-example�u�tagname�h	�line�K�parent�hhh�source��[/Users/gottardi/Documents/AlexaPrize/AlexaPrizeCobotToolkit/doc/non_trivial_bot_example.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h� Building A Non-Trivial Socialbot�h]�h �Text���� Building A Non-Trivial Socialbot�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh �	paragraph���)��}�(hX  This is a Quickstart Guide to building a non-trivial socialbot, starting from the sample_cobot example code. This guide is for the experienced developer or researcher who wants to jump right into building an advanced experience and doesn't want to wade through the full documentation.�h]�h.X  This is a Quickstart Guide to building a non-trivial socialbot, starting from the sample_cobot example code. This guide is for the experienced developer or researcher who wants to jump right into building an advanced experience and doesn’t want to wade through the full documentation.�����}�(hh=hh;hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh �enumerated_list���)��}�(hhh]�h �	list_item���)��}�(h�Install Cobot
�h]�h:)��}�(h�Install Cobot�h]�h.�Install Cobot�����}�(hhVhhTubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhhPubah}�(h]�h]�h]�h]�h]�uhhNhhKhhh h!hNubah}�(h]�h]�h]�h]�h]��enumtype��arabic��prefix�h�suffix��.��start�K uhhIhh$hhh h!hKubh:)��}�(h��Make sure your environment is set up and your initial Cobot source has been pulled following the instructions in `Getting Started`__.�h]�(h.�qMake sure your environment is set up and your initial Cobot source has been pulled following the instructions in �����}�(h�qMake sure your environment is set up and your initial Cobot source has been pulled following the instructions in �hhthhh NhNubh �	reference���)��}�(h�`Getting Started`__�h]�h.�Getting Started�����}�(hhhhubah}�(h]�h]�h]�h]�h]��name��Getting Started��	anonymous�K�refuri��getting_started.html�uhh}hht�resolved�Kubh.�.�����}�(hhrhhthhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK
hh$hhubh
)��}�(h�.. __: getting_started.html�h]�h}�(h]��id1�ah]�h]�h]�h]�h�h�h�Kuhh	hKhh$hhh h!�
referenced�KubhJ)��}�(hhh]�hO)��}�(h�Set Up Intent Model
�h]�h:)��}�(h�Set Up Intent Model�h]�h.�Set Up Intent Model�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhhNhh�hhh h!hNubah}�(h]�h]�h]�h]�h]�hnhohphhqhruhhIhh$hhh h!hKubh:)��}�(h�Following the instructions from `the intent model section`__, we will replace cobot-skill/models/en-US.json with the following:�h]�(h.� Following the instructions from �����}�(h� Following the instructions from �hh�hhh NhNubh~)��}�(h�`the intent model section`__�h]�h.�the intent model section�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��name��the intent model section�h�Kh��intent_model.html�uhh}hh�h�Kubh.�C, we will replace cobot-skill/models/en-US.json with the following:�����}�(h�C, we will replace cobot-skill/models/en-US.json with the following:�hh�hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh
)��}�(h�.. __: intent_model.html�h]�h}�(h]��id2�ah]�h]�h]�h]�h�h�h�Kuhh	hKhh$hhh h!h�Kubh �literal_block���)��}�(hXI
  {
"interactionModel": {
  "languageModel": {
    "invocationName": "greeter",
    "types": [
      {
        "name": "QUESTION_WORD",
        "values": [
          {
            "id": "WHO",
            "name": {
              "value": "who",
              "synonyms": []
            }
          },
          {
            "id": "WHAT",
            "name": {
              "value": "what",
              "synonyms": []
            }
          },
          {
            "id": "WHEN",
            "name": {
              "value": "when",
              "synonyms": []
            }
          },
          {
            "id": "WHERE",
            "name": {
              "value": "where",
              "synonyms": []
            }
          },
          {
            "id": "WHY",
            "name": {
              "value": "why",
              "synonyms": []
            }
          },
          {
            "id": "HOW_OLD",
            "name": {
              "value": "how old",
              "synonyms": []
            }
          }
        ]
      }
    ],
    "intents": [
      {
        "name": "AMAZON.CancelIntent",
        "samples": []
      },
      {
        "name": "AMAZON.HelpIntent",
        "samples": []
      },
      {
        "name": "AMAZON.StopIntent",
        "samples": []
      },
      {
        "name": "greet",
        "slots": [],
        "samples": [
          "hey",
          "hello",
          "hi",
          "hello there",
          "good morning",
          "good evening"
        ]
      },
      {
        "name": "topic_request",
        "slots": [
          {
            "name": "topic",
            "type": "AMAZON.RAW_TEXT"
          }
        ],
        "samples": [
          "let's chat about {topic}",
          "can we talk about {topic}",
          "tell me about {topic}",
          "what can you tell me about {topic}",
          "can we chat about {topic}",
          "can you say something about {topic}",
          "let's talk about {topic}",
          "can we discuss {topic}"
        ]
      },
      {
        "name": "QAIntent",
        "slots": [
          {
            "name": "question_word",
            "type": "QUESTION_WORD"
          },
          {
            "name": "question_text",
            "type": "AMAZON.RAW_TEXT"
          }
        ],
        "samples": [
          "{question_word} {question_text}"
        ]
      },
      {
        "name": "retrieval",
        "slots": [
          {
            "name": "text",
            "type": "AMAZON.RAW_TEXT"
          }
        ],
        "samples": [
          "{text}"
        ]
      }
    ]
  }
  }
}�h]�h.XI
  {
"interactionModel": {
  "languageModel": {
    "invocationName": "greeter",
    "types": [
      {
        "name": "QUESTION_WORD",
        "values": [
          {
            "id": "WHO",
            "name": {
              "value": "who",
              "synonyms": []
            }
          },
          {
            "id": "WHAT",
            "name": {
              "value": "what",
              "synonyms": []
            }
          },
          {
            "id": "WHEN",
            "name": {
              "value": "when",
              "synonyms": []
            }
          },
          {
            "id": "WHERE",
            "name": {
              "value": "where",
              "synonyms": []
            }
          },
          {
            "id": "WHY",
            "name": {
              "value": "why",
              "synonyms": []
            }
          },
          {
            "id": "HOW_OLD",
            "name": {
              "value": "how old",
              "synonyms": []
            }
          }
        ]
      }
    ],
    "intents": [
      {
        "name": "AMAZON.CancelIntent",
        "samples": []
      },
      {
        "name": "AMAZON.HelpIntent",
        "samples": []
      },
      {
        "name": "AMAZON.StopIntent",
        "samples": []
      },
      {
        "name": "greet",
        "slots": [],
        "samples": [
          "hey",
          "hello",
          "hi",
          "hello there",
          "good morning",
          "good evening"
        ]
      },
      {
        "name": "topic_request",
        "slots": [
          {
            "name": "topic",
            "type": "AMAZON.RAW_TEXT"
          }
        ],
        "samples": [
          "let's chat about {topic}",
          "can we talk about {topic}",
          "tell me about {topic}",
          "what can you tell me about {topic}",
          "can we chat about {topic}",
          "can you say something about {topic}",
          "let's talk about {topic}",
          "can we discuss {topic}"
        ]
      },
      {
        "name": "QAIntent",
        "slots": [
          {
            "name": "question_word",
            "type": "QUESTION_WORD"
          },
          {
            "name": "question_text",
            "type": "AMAZON.RAW_TEXT"
          }
        ],
        "samples": [
          "{question_word} {question_text}"
        ]
      },
      {
        "name": "retrieval",
        "slots": [
          {
            "name": "text",
            "type": "AMAZON.RAW_TEXT"
          }
        ],
        "samples": [
          "{text}"
        ]
      }
    ]
  }
  }
}�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve��force���language��bash��highlight_args�}�uhh�h h!hKhh$hhubhJ)��}�(hhh]�hO)��}�(h�6Ensure that your modules.yaml contains the following:
�h]�h:)��}�(h�5Ensure that your modules.yaml contains the following:�h]�h.�5Ensure that your modules.yaml contains the following:�����}�(hj  hj  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj  ubah}�(h]�h]�h]�h]�h]�uhhNhj  hhh h!hNubah}�(h]�h]�h]�h]�h]�hnhohphhqhrhsKuhhIhh$hhh h!hK�ubh�)��}�(h�Ocobot_modules:
    - greeter
    - retrieval

infrastructure_modules:
    - nlp�h]�h.�Ocobot_modules:
    - greeter
    - retrieval

infrastructure_modules:
    - nlp�����}�(hhhj2  ubah}�(h]�h]�h]�h]�h]�j
  j  j  �j  �bash�j  }�uhh�h h!hK�hh$hhubhJ)��}�(hhh]�hO)��}�(h�8Edit your greeter_bot.py file to contain the following:
�h]�h:)��}�(h�7Edit your greeter_bot.py file to contain the following:�h]�h.�7Edit your greeter_bot.py file to contain the following:�����}�(hjK  hjI  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hK�hjE  ubah}�(h]�h]�h]�h]�h]�uhhNhjB  hhh h!hNubah}�(h]�h]�h]�h]�h]�hnhohphhqhrhsKuhhIhh$hhh h!hK�ubh�)��}�(hX1  import cobot_core as Cobot
from cobot_core.service_url_loader import ServiceURLLoader
from cobot_core.service_module import RemoteServiceModule
from cobot_core.service_module import LocalServiceModule
import random
import requests
from bs4 import BeautifulSoup

...

class EviResponseGenerator(LocalServiceModule):
    def execute(self):
        isWhitelisted = False
        question_text = ''
        if not isWhitelisted:
            intent = self.input_data['intent']
            slots = self.input_data['slots']
            question_word = slots['question_word']['value']
            question_text = question_word+' '+slots['question_text']['value']
        else:
            question_text = self.input_data['asr']
            question_text = question_text.replace(' ','_')

        url = "http://www.evi.com/q/"+question_text
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        result = soup.find('div', attrs={'class': 'tk_common'})
        print('result:', result.string.strip())
        return result.string.strip()

def overrides(binder):
    intent_map = { 'QAIntent':'EVI', 'greet':'RULES', 'topic_request':'RULES', 'retrieval':'RETRIEVAL' }
    binder.bind(Cobot.IntentMap, to=intent_map)
    binder.bind(Cobot.SelectingStrategy, to=Cobot.MappingSelectingStrategy)�h]�h.X1  import cobot_core as Cobot
from cobot_core.service_url_loader import ServiceURLLoader
from cobot_core.service_module import RemoteServiceModule
from cobot_core.service_module import LocalServiceModule
import random
import requests
from bs4 import BeautifulSoup

...

class EviResponseGenerator(LocalServiceModule):
    def execute(self):
        isWhitelisted = False
        question_text = ''
        if not isWhitelisted:
            intent = self.input_data['intent']
            slots = self.input_data['slots']
            question_word = slots['question_word']['value']
            question_text = question_word+' '+slots['question_text']['value']
        else:
            question_text = self.input_data['asr']
            question_text = question_text.replace(' ','_')

        url = "http://www.evi.com/q/"+question_text
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        result = soup.find('div', attrs={'class': 'tk_common'})
        print('result:', result.string.strip())
        return result.string.strip()

def overrides(binder):
    intent_map = { 'QAIntent':'EVI', 'greet':'RULES', 'topic_request':'RULES', 'retrieval':'RETRIEVAL' }
    binder.bind(Cobot.IntentMap, to=intent_map)
    binder.bind(Cobot.SelectingStrategy, to=Cobot.MappingSelectingStrategy)�����}�(hhhjc  ubah}�(h]�h]�h]�h]�h]�j
  j  j  �j  �python�j  }�uhh�h h!hK�hh$hhubh:)��}�(h�DSee `the Extending Dialog Manager documentation`__ for more details.�h]�(h.�See �����}�(h�See �hjs  hhh NhNubh~)��}�(h�.`the Extending Dialog Manager documentation`__�h]�h.�*the Extending Dialog Manager documentation�����}�(hhhj|  ubah}�(h]�h]�h]�h]�h]��name��*the Extending Dialog Manager documentation�h�Kh��0expanding_your_bot.html#extending-dialog-manager�uhh}hjs  h�Kubh.� for more details.�����}�(h� for more details.�hjs  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK�hh$hhubh
)��}�(h�7.. __: expanding_your_bot.html#extending-dialog-manager�h]�h}�(h]��id3�ah]�h]�h]�h]�h�j�  h�Kuhh	hK�hh$hhh h!h�KubhJ)��}�(hhh]�hO)��}�(h�;You will also need to add the following to greeter_bot.py:
�h]�h:)��}�(h�:You will also need to add the following to greeter_bot.py:�h]�h.�:You will also need to add the following to greeter_bot.py:�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhhNhj�  hhh h!hNubah}�(h]�h]�h]�h]�h]�hnhohphhqhrhsKuhhIhh$hhh h!hK�ubh�)��}�(hXy  EviBot = {
         'name': "EVI",
         'class': EviResponseGenerator,
         'url': 'local',
         'context_manager_keys': ['intent', 'slots']
}

RemoteRuleBot = {
         'name': "RULES",
         'class': RemoteServiceModule,
         'url': ServiceURLLoader.get_url_for_module("GREETER"),
         'context_manager_keys': ['intent','slots']
}

RetrievalBot = {
         'name': "RETRIEVAL",
         'class': RemoteServiceModule,
         'url': ServiceURLLoader.get_url_for_module("RETRIEVAL"),
         'context_manager_keys': ['intent','slots']
}

cobot.add_response_generators([RemoteRuleBot, EviBot, RetrievalBot])�h]�h.Xy  EviBot = {
         'name': "EVI",
         'class': EviResponseGenerator,
         'url': 'local',
         'context_manager_keys': ['intent', 'slots']
}

RemoteRuleBot = {
         'name': "RULES",
         'class': RemoteServiceModule,
         'url': ServiceURLLoader.get_url_for_module("GREETER"),
         'context_manager_keys': ['intent','slots']
}

RetrievalBot = {
         'name': "RETRIEVAL",
         'class': RemoteServiceModule,
         'url': ServiceURLLoader.get_url_for_module("RETRIEVAL"),
         'context_manager_keys': ['intent','slots']
}

cobot.add_response_generators([RemoteRuleBot, EviBot, RetrievalBot])�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j
  j  j  �j  �python�j  }�uhh�h h!hK�hh$hhubh:)��}�(h�JSee `the add_response_generators method documentation`__ for more details.�h]�(h.�See �����}�(h�See �hj�  hhh NhNubh~)��}�(h�4`the add_response_generators method documentation`__�h]�h.�0the add_response_generators method documentation�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��0the add_response_generators method documentation�h�Kh��Mcobot_core.html#cobot_core.cobot_handler.CobotHandler.add_response_generators�uhh}hj�  h�Kubh.� for more details.�����}�(h� for more details.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK�hh$hhubh
)��}�(h�T.. __: cobot_core.html#cobot_core.cobot_handler.CobotHandler.add_response_generators�h]�h}�(h]��id4�ah]�h]�h]�h]�h�j�  h�Kuhh	hK�hh$hhh h!h�KubhJ)��}�(hhh]�hO)��}�(h�GAdd the new retrieval module. From the command line run the following:
�h]�h:)��}�(h�FAdd the new retrieval module. From the command line run the following:�h]�h.�FAdd the new retrieval module. From the command line run the following:�����}�(hj  hj  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj  ubah}�(h]�h]�h]�h]�h]�uhhNhj  hhh h!hNubah}�(h]�h]�h]�h]�h]�hnhohphhqhrhsKuhhIhh$hhh h!hK�ubh�)��}�(h�^add-response-generator retrieval sample_cobot
cd $COBOT_HOME/sample_cobot/docker/retrieval/app�h]�h.�^add-response-generator retrieval sample_cobot
cd $COBOT_HOME/sample_cobot/docker/retrieval/app�����}�(hhhj%  ubah}�(h]�h]�h]�h]�h]�j
  j  j  �j  �bash�j  }�uhh�h h!hK�hh$hhubhJ)��}�(hhh]�hO)��}�(h�yEdit the ``handle_message(msg)`` method in ``response_generator.py`` to implement the model of your choice. For example:
�h]�h:)��}�(h�xEdit the ``handle_message(msg)`` method in ``response_generator.py`` to implement the model of your choice. For example:�h]�(h.�	Edit the �����}�(h�	Edit the �hj<  ubh �literal���)��}�(h�``handle_message(msg)``�h]�h.�handle_message(msg)�����}�(hhhjG  ubah}�(h]�h]�h]�h]�h]�uhjE  hj<  ubh.� method in �����}�(h� method in �hj<  ubjF  )��}�(h�``response_generator.py``�h]�h.�response_generator.py�����}�(hhhjZ  ubah}�(h]�h]�h]�h]�h]�uhjE  hj<  ubh.�4 to implement the model of your choice. For example:�����}�(h�4 to implement the model of your choice. For example:�hj<  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj8  ubah}�(h]�h]�h]�h]�h]�uhhNhj5  hhh h!hNubah}�(h]�h]�h]�h]�h]�hnhohphhqhrhsKuhhIhh$hhh h!hK�ubh�)��}�(hX6  from rake_nltk import Rake
import random

required_context = ['intent','slots']

def get_required_context():
    return required_context

RAKE = Rake()
REDDIT_DATA_PATH = "data/reddit_retrieval_data.txt"

def process_user_utterance(utterance):
    RAKE.extract_keywords_from_text(utterance)
    keywords = RAKE.get_ranked_phrases()
    to_remove = set(["chat", "talk", "let"])
    result = []
    for item in list(set(keywords) - to_remove):
        result = result + item.split()
    return result

def retrieve_subreddits(utterance, entities=None):
    if not entities:
        entities = process_user_utterance(utterance)
    keywords_set = set(entities)
    reddit_data = open(REDDIT_DATA_PATH, "r")
    best_score = 0.5
    best_resp = None
    utterances_with_keywords = set()
    for sent in reddit_data:
        #match_ratio = fuzz.ratio(utterance, sent)
        match_ratio = len(keywords_set.intersection(set(sent.split())))
        if match_ratio > best_score and len(sent.split()) < 15 and len(sent.split()) >5:
            #best_score = match_ratio
            #best_resp = sent
            utterances_with_keywords.add((sent,match_ratio))

    utterances_with_keywords = sorted(utterances_with_keywords,key=lambda x: x[1], reverse=True)[0:20]

    return random.choice(list(utterances_with_keywords))[0]


def is_black(response, black_list):
    for word in black_list:
        if word in response.split():
            return True
    return False

def handle_message(msg):
    utterance = msg['slots']['text']['value']
    response_is_black = True
    response = None
    entities = None
    black_list = set(["fuck", "fuk", "sex", "idiot", "maniac", "dick", "butt"])
    while response_is_black:
        response = retrieve_subreddits(utterance, entities)
        if not is_black(response, black_list):
            return response�h]�h.X6  from rake_nltk import Rake
import random

required_context = ['intent','slots']

def get_required_context():
    return required_context

RAKE = Rake()
REDDIT_DATA_PATH = "data/reddit_retrieval_data.txt"

def process_user_utterance(utterance):
    RAKE.extract_keywords_from_text(utterance)
    keywords = RAKE.get_ranked_phrases()
    to_remove = set(["chat", "talk", "let"])
    result = []
    for item in list(set(keywords) - to_remove):
        result = result + item.split()
    return result

def retrieve_subreddits(utterance, entities=None):
    if not entities:
        entities = process_user_utterance(utterance)
    keywords_set = set(entities)
    reddit_data = open(REDDIT_DATA_PATH, "r")
    best_score = 0.5
    best_resp = None
    utterances_with_keywords = set()
    for sent in reddit_data:
        #match_ratio = fuzz.ratio(utterance, sent)
        match_ratio = len(keywords_set.intersection(set(sent.split())))
        if match_ratio > best_score and len(sent.split()) < 15 and len(sent.split()) >5:
            #best_score = match_ratio
            #best_resp = sent
            utterances_with_keywords.add((sent,match_ratio))

    utterances_with_keywords = sorted(utterances_with_keywords,key=lambda x: x[1], reverse=True)[0:20]

    return random.choice(list(utterances_with_keywords))[0]


def is_black(response, black_list):
    for word in black_list:
        if word in response.split():
            return True
    return False

def handle_message(msg):
    utterance = msg['slots']['text']['value']
    response_is_black = True
    response = None
    entities = None
    black_list = set(["fuck", "fuk", "sex", "idiot", "maniac", "dick", "butt"])
    while response_is_black:
        response = retrieve_subreddits(utterance, entities)
        if not is_black(response, black_list):
            return response�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�j
  j  j  �j  �python�j  }�uhh�h h!hK�hh$hhubhJ)��}�(hhh]�hO)��}�(h�WCopy data for the retrieval module. From the ``docker/retrieval/app`` directory, type:
�h]�h:)��}�(h�VCopy data for the retrieval module. From the ``docker/retrieval/app`` directory, type:�h]�(h.�-Copy data for the retrieval module. From the �����}�(h�-Copy data for the retrieval module. From the �hj�  ubjF  )��}�(h�``docker/retrieval/app``�h]�h.�docker/retrieval/app�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhjE  hj�  ubh.� directory, type:�����}�(h� directory, type:�hj�  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hM3hj�  ubah}�(h]�h]�h]�h]�h]�uhhNhj�  hhh h!hNubah}�(h]�h]�h]�h]�h]�hnhohphhqhrhsKuhhIhh$hhh h!hM3ubh�)��}�(h�=cp -R <project-root-directory>/doc/reddit_example_data ./data�h]�h.�=cp -R <project-root-directory>/doc/reddit_example_data ./data�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j
  j  j  �j  �bash�j  }�uhh�h h!hM5hh$hhubhJ)��}�(hhh]�hO)��}�(h�(Add dependencies into requirements.txt:
�h]�h:)��}�(h�'Add dependencies into requirements.txt:�h]�h.�'Add dependencies into requirements.txt:�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hM9hj�  ubah}�(h]�h]�h]�h]�h]�uhhNhj�  hhh h!hNubah}�(h]�h]�h]�h]�h]�hnhohphhqhrhsKuhhIhh$hhh h!hM9ubh�)��}�(h�2nltk==3.2.5
rake_nltk==1.0.2
beautifulsoup4==4.6.0�h]�h.�2nltk==3.2.5
rake_nltk==1.0.2
beautifulsoup4==4.6.0�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j
  j  j  �j  �bash�j  }�uhh�h h!hM;hh$hhubhJ)��}�(hhh]�hO)��}�(h�@Add new files to git, and add any other modified files as well:
�h]�h:)��}�(h�?Add new files to git, and add any other modified files as well:�h]�h.�?Add new files to git, and add any other modified files as well:�����}�(hj  hj  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hMAhj  ubah}�(h]�h]�h]�h]�h]�uhhNhj  hhh h!hNubah}�(h]�h]�h]�h]�h]�hnhohphhqhrhsKuhhIhh$hhh h!hMAubh�)��}�(h�]git add .
git commit
git push "https://git-codecommit.us-east-1.amazonaws.com/v1/repos/cobot"�h]�h.�]git add .
git commit
git push "https://git-codecommit.us-east-1.amazonaws.com/v1/repos/cobot"�����}�(hhhj&  ubah}�(h]�h]�h]�h]�h]�j
  j  j  �j  �bash�j  }�uhh�h h!hMChh$hhubhJ)��}�(hhh]�hO)��}�(h��Run cobot update to reflect changed module configuration (note that this step is only required when modules.yaml has been edited):
�h]�h:)��}�(h��Run cobot update to reflect changed module configuration (note that this step is only required when modules.yaml has been edited):�h]�h.��Run cobot update to reflect changed module configuration (note that this step is only required when modules.yaml has been edited):�����}�(hj?  hj=  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hMIhj9  ubah}�(h]�h]�h]�h]�h]�uhhNhj6  hhh h!hNubah}�(h]�h]�h]�h]�h]�hnhohphhqhrhsK	uhhIhh$hhh h!hMIubh�)��}�(h�cobot update sample_cobot�h]�h.�cobot update sample_cobot�����}�(hhhjW  ubah}�(h]�h]�h]�h]�h]�j
  j  j  �j  �bash�j  }�uhh�h h!hMKhh$hhubeh}�(h]�(� building-a-non-trivial-socialbot�heh]�h]�(� building a non-trivial socialbot��non_trivial_bot_example�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�jm  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(jm  hjl  ji  u�	nametypes�}�(jm  �jl  Nuh}�(hh$ji  h$h�h�h�h�j�  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�(h �system_message���)��}�(hhh]�h:)��}�(h�:Enumerated list start value not ordinal-1: "0" (ordinal 0)�h]�h.�>Enumerated list start value not ordinal-1: “0” (ordinal 0)�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhj�  hh$hhh h!hKubj�  )��}�(hhh]�h:)��}�(h�:Enumerated list start value not ordinal-1: "2" (ordinal 2)�h]�h.�>Enumerated list start value not ordinal-1: “2” (ordinal 2)�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh9hj  ubah}�(h]�h]�h]�h]�h]��level�K�type�j
  �source�h!�line�Kuhj�  hh$hhh h!hK�ubj�  )��}�(hhh]�h:)��}�(h�:Enumerated list start value not ordinal-1: "3" (ordinal 3)�h]�h.�>Enumerated list start value not ordinal-1: “3” (ordinal 3)�����}�(hhhj+  ubah}�(h]�h]�h]�h]�h]�uhh9hj(  ubah}�(h]�h]�h]�h]�h]��level�K�type�j
  �source�h!�line�Kuhj�  hh$hhh h!hK�ubj�  )��}�(hhh]�h:)��}�(h�:Enumerated list start value not ordinal-1: "4" (ordinal 4)�h]�h.�>Enumerated list start value not ordinal-1: “4” (ordinal 4)�����}�(hhhjF  ubah}�(h]�h]�h]�h]�h]�uhh9hjC  ubah}�(h]�h]�h]�h]�h]��level�K�type�j
  �source�h!�line�Kuhj�  hh$hhh h!hK�ubj�  )��}�(hhh]�h:)��}�(h�:Enumerated list start value not ordinal-1: "5" (ordinal 5)�h]�h.�>Enumerated list start value not ordinal-1: “5” (ordinal 5)�����}�(hhhja  ubah}�(h]�h]�h]�h]�h]�uhh9hj^  ubah}�(h]�h]�h]�h]�h]��level�K�type�j
  �source�h!�line�Kuhj�  hh$hhh h!hK�ubj�  )��}�(hhh]�h:)��}�(h�:Enumerated list start value not ordinal-1: "6" (ordinal 6)�h]�h.�>Enumerated list start value not ordinal-1: “6” (ordinal 6)�����}�(hhhj|  ubah}�(h]�h]�h]�h]�h]�uhh9hjy  ubah}�(h]�h]�h]�h]�h]��level�K�type�j
  �source�h!�line�Kuhj�  hh$hhh h!hK�ubj�  )��}�(hhh]�h:)��}�(h�:Enumerated list start value not ordinal-1: "6" (ordinal 6)�h]�h.�>Enumerated list start value not ordinal-1: “6” (ordinal 6)�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j
  �source�h!�line�Kuhj�  hh$hhh h!hM3ubj�  )��}�(hhh]�h:)��}�(h�:Enumerated list start value not ordinal-1: "7" (ordinal 7)�h]�h.�>Enumerated list start value not ordinal-1: “7” (ordinal 7)�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j
  �source�h!�line�Kuhj�  hh$hhh h!hM9ubj�  )��}�(hhh]�h:)��}�(h�:Enumerated list start value not ordinal-1: "8" (ordinal 8)�h]�h.�>Enumerated list start value not ordinal-1: “8” (ordinal 8)�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j
  �source�h!�line�Kuhj�  hh$hhh h!hMAubj�  )��}�(hhh]�h:)��}�(h�:Enumerated list start value not ordinal-1: "9" (ordinal 9)�h]�h.�>Enumerated list start value not ordinal-1: “9” (ordinal 9)�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j
  �source�h!�line�Kuhj�  hh$hhh h!hMIube�transform_messages�]�j�  )��}�(hhh]�h:)��}�(hhh]�h.�=Hyperlink target "non-trivial-bot-example" is not referenced.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh9hj  ubah}�(h]�h]�h]�h]�h]��level�K�type�j
  �source�h!�line�Kuhj�  uba�transformer�N�
decoration�Nhhub.