��=      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�.. _intent_model:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��intent-model�u�tagname�h	�line�K�parent�hhh�source��P/Users/gottardi/Documents/AlexaPrize/AlexaPrizeCobotToolkit/doc/intent_model.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Creating an Intent Model�h]�h �Text����Creating an Intent Model�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh#)��}�(hhh]�(h()��}�(h�Intent Model Definition�h]�h.�Intent Model Definition�����}�(hh>hh<hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh9hhh h!hK	ubh �	paragraph���)��}�(hX!  A normal ASK skill is dependent on the Alexa Skills Kit intent model to interpret user voice interactions. A user interaction will be converted to text, and that text will then be interpreted into a high level intent, and potentially slot values, to define details around the user request.�h]�h.X!  A normal ASK skill is dependent on the Alexa Skills Kit intent model to interpret user voice interactions. A user interaction will be converted to text, and that text will then be interpreted into a high level intent, and potentially slot values, to define details around the user request.�����}�(hhNhhLhhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubhK)��}�(hX  If your ASK skill is whitelisted for additional features, you can directly access the text of user interactions and create your own mechanism for interpreting user interactions, as well as accessing *n-best* alternate interpretations from the Alexa ASR (Automated Speech Recognition) system. However, you will still need a minimal intent model to pass through the text to your Cobot's analysis pipeline. You will also need support for the required StopIntent, which we recommend implementing in your ASK intent model.�h]�(h.��If your ASK skill is whitelisted for additional features, you can directly access the text of user interactions and create your own mechanism for interpreting user interactions, as well as accessing �����}�(h��If your ASK skill is whitelisted for additional features, you can directly access the text of user interactions and create your own mechanism for interpreting user interactions, as well as accessing �hhZhhh NhNubh �emphasis���)��}�(h�*n-best*�h]�h.�n-best�����}�(hhhheubah}�(h]�h]�h]�h]�h]�uhhchhZubh.X8   alternate interpretations from the Alexa ASR (Automated Speech Recognition) system. However, you will still need a minimal intent model to pass through the text to your Cobot’s analysis pipeline. You will also need support for the required StopIntent, which we recommend implementing in your ASK intent model.�����}�(hX6   alternate interpretations from the Alexa ASR (Automated Speech Recognition) system. However, you will still need a minimal intent model to pass through the text to your Cobot's analysis pipeline. You will also need support for the required StopIntent, which we recommend implementing in your ASK intent model.�hhZhhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubhK)��}�(h��In the case of our sample Cobot's greeter module, the intents understood are defined in the ASK intent model. The model input is located in sample_cobot/cobot-skill/models/en-US.json.�h]�h.��In the case of our sample Cobot’s greeter module, the intents understood are defined in the ASK intent model. The model input is located in sample_cobot/cobot-skill/models/en-US.json.�����}�(hh�hh~hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubh �literal_block���)��}�(hX�  {
  "interactionModel": {
    "languageModel": {
      "invocationName": "greeter",
      "types": [],
      "intents": [
...
              {
          "name": "greet",
          "slots": [],
          "samples": [
            "hey",
            "hello",
            "hi",
            "hello there",
            "good morning",
            "good evening"
          ]
        },
...
      ]
    }
  }
}�h]�h.X�  {
  "interactionModel": {
    "languageModel": {
      "invocationName": "greeter",
      "types": [],
      "intents": [
...
              {
          "name": "greet",
          "slots": [],
          "samples": [
            "hey",
            "hello",
            "hi",
            "hello there",
            "good morning",
            "good evening"
          ]
        },
...
      ]
    }
  }
}�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve��force���language��bash��highlight_args�}�uhh�h h!hKhh9hhubeh}�(h]��intent-model-definition�ah]�h]��intent model definition�ah]�h]�uhh"hh$hhh h!hK	ubeh}�(h]�(�creating-an-intent-model�heh]�h]�(�creating an intent model��intent_model�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�h�hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�hیerror_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(h�hh�h�h�h�u�	nametypes�}�(h��h�Nh�Nuh}�(hh$h�h$h�h9u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�hK)��}�(hhh]�h.�2Hyperlink target "intent-model" is not referenced.�����}�(hhhj:  ubah}�(h]�h]�h]�h]�h]�uhhJhj7  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhj5  uba�transformer�N�
decoration�Nhhub.