.. _baseline_socialbot_nlp_pipeline:

Baseline Socialbot NLP Pipeline
===============================

.. image:: images/nlp_pipeline.png

------------------
Initial Processing
------------------

Initial processing is for the first processing steps.  It has one step, extracting noun phrases.  Currently, this 
`remote module <baseline_socialbot_build.html#adding-a-remote-module>`_ uses spacy along with an ignore list to extract the relevant noun phrases from the current user utterance 
and bot response from the previous turn.  

-----------------------
Correctional Processing
-----------------------

Correctional processing is used for the remaining processing steps that require the output of the first processing steps.
Currently, there are two modules: coreference resolution and ASR correction.

Coreference Resolution
^^^^^^^^^^^^^^^^^^^^^^

`This module <baseline_socialbot_build.html#coreference-module>`_ uses neuralcoref to extract the coreference clusters from the previous and current turn.  The input to neuralcoref
is the user's utterance from the previous and current turn along with the bot's previous response.  This returns lists with 
the pronoun referenced along with the noun it points to.

ASR Correction
^^^^^^^^^^^^^^

`This module <baseline_socialbot_build.html#asr-correction-module>`_ uses double metaphone and manaually generated rules along with the category of the topic to correct any issues with the ASR 
passed in from the ASK console.  It will only correct the noun phrases extracted from the previous module and is only triggered
when the ASR confidence is below 0.4 for the entire utterance.  It returns a dictionary with the original utterance as the key and a
list of suggested corrections.

----------------------
Additional Information
----------------------

Hosted Modules
^^^^^^^^^^^^^^

These hosted modules include sentiment, offensive speech, NER, and dialog act.  

User Response Information
^^^^^^^^^^^^^^^^^^^^^^^^^

This module acts as a rudimentary sentiment model with more specific categories analyzed.  It takes in `last_state_require_confirmation,
last_state_response, current_intent, text_from_navigation, last_state_corrected_ask_intent <baseline_socialbot_terminology.html#inputs-in-the-nlp-pipeline>`_
 and outputs a UserResponseInformation object.  This UserResponseInformation contains user_decision (if the user says yes/no or other related words to Alexa's prompts),
user_knowledge (if the user states that they know/don't know the information Alexa has given),
user_satisfaction (how satisfied the user is with the information Alexa has given),
and user_side (if the user agrees with what Alexa has said).

Let's now walk through the workflow of this `module <baseline_socialbot_build.html#adding-a-new-local-nlp-module>`_.  We can find all relevant files under miniskills/custom_nlp/user_response_module.
First, the execute method is called in UserResponseModule and if there is a last state (it is not the first turn), 
it will call the execute method of RegisterUserResponseInformation.  This class will go through and create a UserResponseInformation 
object, calling the estimate method for each class that handles creating one variable in the object.  Each of these classes has
two ways of setting the variable: using context and intent or a set of regexes.  Using context and intent overrides any 
regex checking and is used for more specific dialog flows.  For example, if the bot asks "Did you know that Taylor Swift
was a country singer?", we want yes to mean that the user already knew this instead of a confirmation that the user wants to hear
more about this topic.  

-----------------
Intent Classifier
-----------------

Several new `state manager fields <baseline_socialbot_terminology.html#additional-state-manager-fields-in-intent-classifier>`_ are set in the intent classifier, 
descriptions of which are linked.

The intent classifier has access to all fields of the state manager and outputs a corrected_ask_intent.  It will go through 
seven steps before the final output.  All associated files can be found under miniskills/custom_nlp.

Step 1: Initialize miniskill_to_turn_used
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This first step initializes this turn's miniskill_to_turn_used from last turn.  An additional check here is that
if the current intent is the launch request intent then we can skip the rest of the steps in the intent classifier.
We will either initialize miniskill_to_turn_used with 0's for every miniskill when launch request intent is triggered 
or copy over last state's miniskill_to_turn_used dictionary.

Step 2: Modify ASK Intent
^^^^^^^^^^^^^^^^^^^^^^^^^

This second step will do some additional correction of the ASK intent given by the console.  It will capture additional 
ways to say yes/no, ignored/deflected topics, some navigational intents such as repeat, continue, cancel, and stop, and 
ways to talk about movies.  This step calls the execute method of ModifyAskIntent with last_state_require_confirmation, 
last_state_corrected_ask_intent, current_intent, topic, saved_intent, text_from_navigation, and text and returns the intent.  
Most of the logic in this class is with regexes.

Step 3: Set Corrected Topic
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This third step will be looking at the topic given from the slots and editing it if necessary.  This is used for pulling
a more specific topic if a general topic was given, such as "Christmas" for "holidays".  It also checks if this topic exists
in the DynamoDB tables associated with the current miniskill.  This calls the execute method of SetCorrectedTopic with 
inputs of slots, current_intent, text, last_state_old_topic, last_state_corrected_ask_intent, turn_number, and visited_threads 
and returns the corrected topic to the state manager.

Step 4: Generate Corrected ASK Intent
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The fourth step is where the bulk of the logic occurs for the intent classifier.  We will be using user_response_information, current_intent,
last_state_corrected_ask_intent, last_state_intent, miniskill_to_turn_used, last_state_saved_intent, text, corrected_topic,
last_state_old_topic, last_state_response, last_state_new_topic, turn_number, and visited_threads to get our final
corrected_ask_intent for the current state by calling the execute method of GenerateCorrectedAskIntent.  An additional note,
if the last turn was a launch request and the current turn is a special user utterance (which is usually ignored or deflected topics),
we will want to reprompt the user with the launch response.  If the corrected_ask_intent after this turn is the greeting_intent 
or launch_request_intent, there is no need to run the rest of the steps in the intent classifier, so we return None.

The below tables are current mappings of all rules used to map to various miniskill intents.

AMAZON Intents Without User Response Information

.. image:: images/amazon_intents_no_uri.png

AMAZON Intents With User Response Information

.. image:: images/amazon_intents_uri.png

Intents Directly Correlating to Miniskills

.. image:: images/regular_intents.png

Navigation Intent (Catch-All) Without User Response Information

.. image:: images/navigation_extra.png

Navigation Intent (Catch-All) With User Response Information

.. image:: images/navigation_uri.png

Step 5: Check Topic Exists
^^^^^^^^^^^^^^^^^^^^^^^^^^

In this fifth step, we want to make sure that the topic we have chosen previously in step 3 is in the DynamoDB table 
that is associated with the miniskill we chose in step 4.  This takes corrected_topic (step 3),
current_corrected_ask_intent (step 4), miniskill_to_turn_used, last_state_visited_threads, last_state_new_topic, and
last_state_corrected_ask_intent.  There is an additional check that we want to make: if the number of turns on the same 
miniskill exceeds our limit (which is currently 3), we will revert the corrected_topic to the old_topic from the previous state
and not include the current miniskill.  This will make it so that we choose a different miniskill with that topic.
This method calls the no_topic_switch_miniskill method of the MiniskillChooser class.  This will take the inputs stated above
and look in the corresponding DynamoDB tables for the miniskill we've chosen, and if there are no threads returned, 
it will look at all other possible miniskills (either all miniskills we have in rotation or only informative miniskills which 
are ShareFacts or ShowerThoughts currently).  If no other miniskill's DynamoDb tables have threads on this topic, the same
miniskill will be chosen and later a topic exhausted response will be given.

An additional features of this is that there exists an artificial deflation of the news miniskill since it queries 7 DynamoDB
tables currently while other miniskills query 1.  If the movies miniskill can be called, we also artificially inflate it
slightly so it is chosen more often.  This is so users can be routed to a more complex miniskill.

If the current_corrected_ask_intent is RequestMovieIntent or AskQuestionIntent, we want to make sure that this step doesn't return 
another miniskill because it could either be in the middle of a turn sequence for movies or not need to be checked because
Evi does not depend on any DynamoDB table.

If we choose another miniskill during this time, we will want to save the corrected_ask_intent we are coming from into saved_intent 
and if we aren't including the miniskill we switched from, we will want to update the corrected_topic in the state manager to the 
last state's old_topic (it takes the place of the current topic in our response template).

Step 6: Update miniskill_to_turn_used
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

During this step, we will just be updating the miniskill_to_turn_used dictionary with the corrected_ask_intent that has been 
chosen after all the steps above.  If the corrected_ask_intent has not been set yet, we will route to the invalid intent because 
something would have gone wrong in the steps before.

Step 7: Update number_of_turns_on_same_miniskill
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We will increment this number that is being tracked in the state manager if the same miniskill was selected, or set it to 0 if 
a new miniskill was selected.

Topic Classifier
^^^^^^^^^^^^^^^^

This is the topic model that will be provided.  You can refer to the `Annotator Class`__ for more information.

.. __: cleansing_data_and_using_annotator.html#annotator-class

-------------------
Knowledge Retrieval
-------------------

Currently, knowledge modules are located in `big data modules <using_included_big_data.html>`_, the evi_module folder, and the knowledge_module folder.
The Evi module queries Evi in our `Toolkit Service <accessing_toolkit_service_client.html>`_ and the knowledge_module pulls from the News DynamoDB table with
the topic in every turn or in Evi's case, in addition to the entire ASR text.