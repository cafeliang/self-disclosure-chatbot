.. _cleansing_data_and_using_annotator:

Cleansing Data and Using Annotator
==================================

While it’s not necessary to use the Cobot toolkit, we have provided a
convenience API for cleansing, preparing and annotating data sets. These
classes also provide convenient wrappers for the two services we
provide, the topic and profanity classification toolkit service, which
may also be directly invoked via the **cobot_common.service_client**
package.

Prerequisites
-------------

Several python packages need to be installed in your local environment
before using the Data Annotation classes (install with pip3):

-  textblob = 0.15.1
-  nltk = 3.2.5
-  spacy = 2.0.7
-  requests = 2.18.4

You will also need to ensure that a spaCy language model is installed:

::

    python3 -m spacy download en

Cleansing via Blacklist
-----------------------

The scrub_with_blacklist() method in the Cleaner class will load a
blacklist of profane or offensive words and phrases, removing all
utterances from a conversational data set that contain matches with the
offensive words. The list of cleaned entries will be returned by the
Cleaner object’s output() method. Also, you can get a python list of
cleaned texts by the Cleaner object’s result() method.

::

    from cobot_common.data_processing import Cleaner

    cleaner = Cleaner(file_path="<Input_File_Absolute_Path>")
    cleaner.clean_text() \
           .scrub_with_blacklist() \
           .output('<Absolute_Output_File_Path>')

By default Cleaner object scans the input file against an internal blacklist file that contains a list
of words and phrases that must be scrubbed from your data sets before
modeling or training. While not all of these words and phrases are
offensive in all contexts, these are all associated with offensive
content with high enough frequency that it is unwise to allow them into
training or retrieval data sets.

Annotator Class
---------------

The Annotator object is a wrapper for the models exposed by the Cobot
Toolkit API. The topic and offensive speech classifier models are hosted
remotely and invoked by a web service call on data batches. This is
handled automatically for you - a single Annotator result() call will
lead to the input data set being batched, transmitted, processed and
returned.

Passing large data sets (tens of megabytes or larger) may take a
significant amount of time to process (NOTE: a security fix is being
deployed soon that will improve model performance by ~200%). Once completed, you should save the results of a large
annotation job promptly to avoid having to re-process the same data
again. Hosted models are subject to throttling rate limits and thus
running multiple jobs in separate processes will not, in general, speed
up an annotation job.

Currently available Annotator models include entities, topics, and
offensive_statements:

::

    from cobot_common.data_processing import Annotator

    #currently, the topic model, sentiment model and offensive statement model are hosted - other annotations run local models from Spacy
    result = Annotator(file_path=source_file_path, api_key='<API_KEY>') \
        .detect_ner() \
        .detect_topic() \
        .detect_profanity() \
        .detect_sentiment() \
        .output('<Absolute_Output_File_Path>')  

Annotator object’s output() method will write the return results to a
JSON file. The JSON schema is:

::

    [
        {
            "text": "string",
            "ner": [
                {
                    "text": "entity text",
                    "start": 58, // The character offset for the start of the entity
                    "end": 65, // The character offset for the end of the entity
                    "label": "GPE" // Entity's label
                }
            ],
            "topic": {
                "topicClass": "History", // Topic label
                "confidence": 0.223
            },
            "profanity_class": true // True: profanity text, False: not profanity text
        }
    ]

Integrating Cleaner and Annotator
---------------------------------

You can obtain a list of cleaned text by using the output of Cleaner object’s
result() method. Then pass the cleaned text list to an Annotator object.

::

    from cobot_common.data_processing import Cleaner, Annotator

    cleaner = Cleaner(file_path="<Input_File_Absolute_Path>")

    cleaned_text_list = cleaner.clean_text() \
           .scrub_with_blacklist() \
           .result()

    Annotator(text_list=cleaned_text_list, api_key="<API_KEY>") \
        .detect_ner() \
        .detect_topic() \
        .detect_profanity() \
        .detect_sentiment() \
        .output("<Output_File_Absolute_Path>")
