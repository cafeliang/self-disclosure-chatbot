aio:isTheStateCapitalOf
  is the state capital of. E.g., Phoenix is the state capital of Arizona.

  .. include:: properties/isTheStateCapitalOf.rst

aio:isThePopulationDensityOf
  is the population density of. For example, 1828 per square kilometer is the population density of Edinburgh.

  .. include:: properties/isThePopulationDensityOf.rst

aio:isTheMonarchOf
  is the monarch of. Elizabeth II is the monarch of the UK.

  .. include:: properties/isTheMonarchOf.rst

aio:isAMusicPublicationBy
  is a music publication created by the musical act.

  .. include:: properties/isAMusicPublicationBy.rst

aio:isAnIso3166CodeForAllOrPartOf
  is an iso 3166 code for all or part of

  .. include:: properties/isAnIso3166CodeForAllOrPartOf.rst

aio:isSomeoneWhoReleased
  is someone who released (the published work). E.g., Bertrand Russell is someone who released Principia Mathematica. Quentin Tarantino is someone who released Pulp Fiction. Nirvana is someone who released Nevermind.

  .. include:: properties/isSomeoneWhoReleased.rst

aio:isBasedOn
  is based on. E.g., Linux is based on Unix.

  .. include:: properties/isBasedOn.rst

aio:isTheFounderOfThePlace
  is the founder of the place. E.g., Samuel Brannan is the founder of the place Sacramento, CA.

  .. include:: properties/isTheFounderOfThePlace.rst

aio:isTheCountryOfOriginOf
  is the country of origin of

  .. include:: properties/isTheCountryOfOriginOf.rst

aio:wasTheFirstToLandOn
  was the first to land on. E.g., Neil Armstrong was the first to land on the Moon.

  .. include:: properties/wasTheFirstToLandOn.rst

aio:isThePlaceNameAbbreviationFor
  is the place-name abbreviation for. E.g., "USA" is a place-name abbreviation for the United States of America.

  .. include:: properties/isThePlaceNameAbbreviationFor.rst

aio:isRelatedTo
  is (familially) related to. E.g., Michelle Obama is related to Barack Obama.

  .. include:: properties/isRelatedTo.rst

aio:isTheMusicalComposerForTheMovie
  is the person who composed the music for the movie

  .. include:: properties/isTheMusicalComposerForTheMovie.rst

aio:wasFormerlyCalled
  was formerly called. E.g., Istanbul was formerly called Constantinople. Language specific to English.

  .. include:: properties/wasFormerlyCalled.rst

aio:isTheSportOfTheTeam
  is the sport of the team. E.g., baseball is the sport of the team Boston Red Sox.

  .. include:: properties/isTheSportOfTheTeam.rst

aio:isTheCollectiveNounForTheClass
  is the collective noun for the class. E.g., "Crash" is the collective noun for the class rhinoceros.

  .. include:: properties/isTheCollectiveNounForTheClass.rst

aio:isTheImdbIdOf
  is the IMDb ID of the movie, actor or film-related entity. This foreign key property allows linking to entities in IMDb. For example, '0795421' is the IMDb id of the movie Mamma Mia and '0000658' is the IMDb id of Meryl Streep.

  .. include:: properties/isTheImdbIdOf.rst

aio:isTheDateOfCreationOf
  is the date of creation of. 1947 is the date of creation of India.

  .. include:: properties/isTheDateOfCreationOf.rst

aio:isATrackVersionOf
  is a track version of the audio work.

  .. include:: properties/isATrackVersionOf.rst

aio:wasPublishedAtTimepoint
  was published on the date

  .. include:: properties/wasPublishedAtTimepoint.rst

aio:isAnAudioPublicationBy
  is an audio publication, such as an album, created by the audio act

  .. include:: properties/isAnAudioPublicationBy.rst

aio:isTheVicePresidentDuringThePresidencyOf
  is the vice president during the presidency of

  .. include:: properties/isTheVicePresidentDuringThePresidencyOf.rst

aio:isTheSystemOfGovernmentUsedIn
  is the system of government used in

  .. include:: properties/isTheSystemOfGovernmentUsedIn.rst

aio:isTheMinisterOfEnergyOf
  is the minister of energy of

  .. include:: properties/isTheMinisterOfEnergyOf.rst

aio:isTheDistanceFromOurSunTo
  is the distance from our sun to. E.g., 228 million kilometers is the distance from our sun to Mars.

  .. include:: properties/isTheDistanceFromOurSunTo.rst

aio:gaveBirthToThePerson
  gave birth to the person. E.g., Olympias gave birth to the person Alexander the Great.

  .. include:: properties/gaveBirthToThePerson.rst

aio:becameIndependentOn
  became independent on. E.g., Ghana became independent on March 6, 1957.

  .. include:: properties/becameIndependentOn.rst

aio:isAReleaseTrackOn
  is a track version found on the album version.

  .. include:: properties/isAReleaseTrackOn.rst

aio:isSomeoneWhoMurdered
  is someone who murdered. E.g., Lee Harvey Oswald is someone who murdered JF Kennedy.

  .. include:: properties/isSomeoneWhoMurdered.rst

aio:isAnOfficialLanguageOf
  is an official language of

  .. include:: properties/isAnOfficialLanguageOf.rst

aio:wasMajorlyInvolvedIn
  is a person or organization that was majorly involved with an object. This is a general property which can be inferred from several more specific subproperties.

  .. include:: properties/wasMajorlyInvolvedIn.rst

aio:isACandidateInTheElection
  is a candidate in the election

  .. include:: properties/isACandidateInTheElection.rst

aio:represents
  represents (is the political representative of). E.g., Jonathan Mason represents Massachusetts

  .. include:: properties/represents.rst

aio:isSomeoneWhoContacted
  is someone who contacted. E.g., Billy Bragg is someone who contacted Jeff Tweedy.

  .. include:: properties/isSomeoneWhoContacted.rst

aio:isTheCauseOfDeathOf
  is the cause of death of. E.g., defenestration is the cause of death of Franz Ferdinand.

  .. include:: properties/isTheCauseOfDeathOf.rst

aio:isThePatronSaintOf
  is the patron saint of. E.g., Saint Patrick is the patron saint of Cingoli, Italy.

  .. include:: properties/isThePatronSaintOf.rst

aio:isAnInstrumentalVersionOf
  is an instrumental version of the audio work

  .. include:: properties/isAnInstrumentalVersionOf.rst

aio:directed
  is the director of the recorded or theatrical production.

  .. include:: properties/directed.rst

aio:isACreatorOfTheTvSeries
  is a creator of the tv series. E.g., Joss Whedon is a creator of the TV series Firefly.

  .. include:: properties/isACreatorOfTheTvSeries.rst

aio:isTheFullOfficialNameOf
  is the English-language string for the full official name of a person.

  .. include:: properties/isTheFullOfficialNameOf.rst

aio:wentTo
  went to. E.g., Joseph Stalin went to the United Kingdom

  .. include:: properties/wentTo.rst

aio:isTheColorOf
  is the color of. E.g., Blue is the color of the Sky

  .. include:: properties/isTheColorOf.rst

aio:isGeographicallyLocatedWithin
  is geographically located within. E.g., Seattle is geographically located within Washington, which is geographically located within the United States.

  .. include:: properties/isGeographicallyLocatedWithin.rst

aio:isASingerWith
  is a singer with the ensemble (i.e. musical group).

  .. include:: properties/isASingerWith.rst

aio:isFilmedIn
  is filmed in. E.g., Octopussy is filmed in India.

  .. include:: properties/isFilmedIn.rst

aio:isTheDeputyLeaderOfTheGovernmentInTheSenateOf
  is the deputy leader of the government in the senate of

  .. include:: properties/isTheDeputyLeaderOfTheGovernmentInTheSenateOf.rst

aio:isInPoliticalOfficeIn
  is in political office in

  .. include:: properties/isInPoliticalOfficeIn.rst

aio:isTheLeaderOfTheOppositionIn
  is the leader of the opposition in

  .. include:: properties/isTheLeaderOfTheOppositionIn.rst

aio:isTheLastNameOf
  is the last name of. E.g., "Wheeler" is the last name of Harold Wheeler.

  .. include:: properties/isTheLastNameOf.rst

aio:performed
  performed a piece of music. This holds when a musical act sang or played a piece of music, whether or not a recording was made or released.

  .. include:: properties/performed.rst

aio:isStarringIn
  is starring in. E.g., Eric Bana is starring in King Arthur: Legend of the Sword.

  .. include:: properties/isStarringIn.rst

aio:isSomewhereRelatedToTheLocationOf
  is somewhere related to the location of. E.g., The University of Phoenix Stadium is somewhere related to Super Bowl XLIX.

  .. include:: properties/isSomewhereRelatedToTheLocationOf.rst

aio:isTheAttorneyGeneralOf
  is the attorney general of

  .. include:: properties/isTheAttorneyGeneralOf.rst

aio:isACitizenOf
  is a citizen of (the country)

  .. include:: properties/isACitizenOf.rst

aio:isAVersionOf
  is a version of. This is a general property which can be inferred from more specific subproperties, such as :isAVersionOfThePublishedWork.

  .. include:: properties/isAVersionOf.rst

aio:isThePlaceOfDeathOf
  is the place of death of. E.g., Cambridgeshire is the place of death of Henry Sidgwick.

  .. include:: properties/isThePlaceOfDeathOf.rst

aio:isAQuoteAbout
  is a quote about. E.g., "It is the mark of an educated mind to be able to entertain a thought without accepting it." is a quote about education.

  .. include:: properties/isAQuoteAbout.rst

aio:isTheFatherOf
  is the father of. E.g., Prince Charles is the father of Prince William.

  .. include:: properties/isTheFatherOf.rst

aio:isTheVolumeOf
  is the volume of. E.g. 163 billion cubic kilometers is the volume of Mars.

  .. include:: properties/isTheVolumeOf.rst

aio:isATrackWorkBy
  is a track work by the audio act.

  .. include:: properties/isATrackWorkBy.rst

aio:isSomethingThatOriginatedInThePlace
  is something that originated in the place. This is a general property, with more specific subproperties.

  .. include:: properties/isSomethingThatOriginatedInThePlace.rst

aio:isTheDeputyFirstMinisterOf
  is the deputy first minister of

  .. include:: properties/isTheDeputyFirstMinisterOf.rst

aio:prefLabel
  is denoted by the output label

  .. include:: properties/prefLabel.rst

aio:isTheStateTreeOf
  is the state tree of. E.g., sugar maple is the state tree of New York State.

  .. include:: properties/isTheStateTreeOf.rst

aio:isTheElevationOf
  is the elevation of. E.g., 34 meters is the elevation of Berlin.

  .. include:: properties/isTheElevationOf.rst

aio:isAStarIn
  is someone who is a member of the main cast of the film or theatrical production.

  .. include:: properties/isAStarIn.rst

aio:isANationStateSharingABorderWith
  is a nation state sharing a border with

  .. include:: properties/isANationStateSharingABorderWith.rst

aio:isTheMinisterOfHealthAndFamilyWelfareOf
  is the minister of health and family welfare of

  .. include:: properties/isTheMinisterOfHealthAndFamilyWelfareOf.rst

aio:isASongPerformedBy
  is a song performed by. E.g., Bohemian Rhapsody is a song performed by Queen.

  .. include:: properties/isASongPerformedBy.rst

aio:isAnAudioActFeaturedOnTheWork
  is an audio performer who created an audio work on the audio publication

  .. include:: properties/isAnAudioActFeaturedOnTheWork.rst

aio:isAPhylumInTheKingdom
  is a taxon at the rank of phylum in the kingdom. Chordate is a phylum in the kingdom animal.

  .. include:: properties/isAPhylumInTheKingdom.rst

aio:isSomeoneWhoMarried
  is someone who married. E.g., Brad Pitt is someone who married Angelina Jolie.

  .. include:: properties/isSomeoneWhoMarried.rst

aio:isTheAuthorOf
  is the person or organization which is the author of the creative work

  .. include:: properties/isTheAuthorOf.rst

aio:isTheMinisterOfRoadTransportAndHighwaysOf
  is the minister of road transport and highways of

  .. include:: properties/isTheMinisterOfRoadTransportAndHighwaysOf.rst

aio:isNear
  is near. E.g., Mudgee is near Sydney.

  .. include:: properties/isNear.rst

aio:isTheMotherOf
  is the mother of. E.g., Olympias is the mother of Alexander the Great.

  .. include:: properties/isTheMotherOf.rst

aio:isTheMinisterForInfrastructureAndTransportOf
  is the minister for infrastructure and transport of

  .. include:: properties/isTheMinisterForInfrastructureAndTransportOf.rst

aio:isTheGovernorOf
  is the governor of

  .. include:: properties/isTheGovernorOf.rst

aio:isTheSameAsOrASubclassOf
  is the same as or a subclass of. This is equivalent to rdfs:subClassOf. See also :isASubClassOf

  .. include:: properties/isTheSameAsOrASubclassOf.rst

aio:isTheMinisterOfPowerOf
  is the minister of power of

  .. include:: properties/isTheMinisterOfPowerOf.rst

aio:isTheMassOf
  is the mass of. E.g., 7.3459 x 10^22 kilograms is the mass of the moon.

  .. include:: properties/isTheMassOf.rst

aio:isTheClassOfUnitsWhichExpresses
  is the class of units which expresses a type of scalar quantity. For example, unit of length :isTheClassOfUnitsWhichExpresses length.

  .. include:: properties/isTheClassOfUnitsWhichExpresses.rst

aio:isTheMinisterOfDefenceOf
  is the minister of defence of

  .. include:: properties/isTheMinisterOfDefenceOf.rst

aio:isTheManufacturerOfEvery
  is the manufacturer of every. E.g., Volvo is the manufacturer of every Volvo c70 Class.

  .. include:: properties/isTheManufacturerOfEvery.rst

aio:isTheLeaderOfTheOppositionInLokSabhaIn
  is the leader of the opposition in lok sabha in

  .. include:: properties/isTheLeaderOfTheOppositionInLokSabhaIn.rst

aio:isAParentOf
  is a parent of. E.g., Barack Obama is a parent of Sasha Obama.

  .. include:: properties/isAParentOf.rst

aio:isAFamilyInTheOrder
  is a taxon at the rank of family in the order. Canidae is a family in the order carnivora.

  .. include:: properties/isAFamilyInTheOrder.rst

aio:isIllegalIn
  is illegal in. E.g., Cannabis is illegal in Yemen.

  .. include:: properties/isIllegalIn.rst

aio:isAnAuntOf
  is an aunt (sister or sister-in-law of a parent) of

  .. include:: properties/isAnAuntOf.rst

aio:isTheCeoOf
  is the Chief Executive Officer of the company. For example, Jeff Bezos is the CEO of Amazon.

  .. include:: properties/isTheCeoOf.rst

aio:isTheHusbandOf
  is a man who is married to a woman in a heterosexual monogamous marriage. This is a subproperty of :isMarriedTo.

  .. include:: properties/isTheHusbandOf.rst

aio:isTheFirstMinisterOf
  is the first minister of

  .. include:: properties/isTheFirstMinisterOf.rst

aio:isTheMinisterOfEnvironmentForestAndClimateChangeOf
  is the minister of environment forest and climate change of

  .. include:: properties/isTheMinisterOfEnvironmentForestAndClimateChangeOf.rst

aio:isTheDateOfTheEstablishmentOf
  is the date of the establishment of. E.g., 1938 is the date of the establishment Myrtle Beach.

  .. include:: properties/isTheDateOfTheEstablishmentOf.rst

aio:isTheLandAreaOf
  is the land area of. For example, 227.2 square miles is the land area of Chicago.

  .. include:: properties/isTheLandAreaOf.rst

aio:isTheAverageAnnualRainfallOf
  is the average annual rainfall of. For example, 43 inches is the average annual rainfall of New Hampshire.

  .. include:: properties/isTheAverageAnnualRainfallOf.rst

aio:isMarriedTo
  is the person who is recognized as legally married to the person, in a legal jurisdiction which enforces monogamy. This does not place any restriction on the gender of the people married, but does not include polygamous marriage relations.

  .. include:: properties/isMarriedTo.rst

aio:isTheSloganFor
  is the slogan for. E.g., "Per ardua ad astra" is the slogan for the Royal New Zealand Air Force.

  .. include:: properties/isTheSloganFor.rst

aio:isTheAverageTemperatureOf
  is the average temperature of. For example, 49.65 degrees Fahrenheit is the average temperature of Chicago.

  .. include:: properties/isTheAverageTemperatureOf.rst

aio:isAFanOf
  is a fan of. E.g., Russell Crowe is a fan of NFL.

  .. include:: properties/isAFanOf.rst

aio:wasMajorlyCreativelyInvolvedIn
  is a person or organization that was significantly involved in the creation of an object. This is a general property which can be inferred from several more specific subproperties.

  .. include:: properties/wasMajorlyCreativelyInvolvedIn.rst

aio:isTheChiefJusticeOf
  is the chief justice of

  .. include:: properties/isTheChiefJusticeOf.rst

aio:isTheLaunchDateOfTheProduct
  is the launch date of the product. E.g., 1973 is the launch date of the product mobile phone.

  .. include:: properties/isTheLaunchDateOfTheProduct.rst

aio:isASymbolFor
  is a symbol for. E.g., The falcon is a symbol for Seattle Pacific University.

  .. include:: properties/isASymbolFor.rst

aio:isTheMinisterOfHomeAffairsOf
  is the minister of home affairs of

  .. include:: properties/isTheMinisterOfHomeAffairsOf.rst

aio:isEngagedTo
  is engaged to. E.g., Cooke Maroney is engaged to Jennifer Lawrence.

  .. include:: properties/isEngagedTo.rst

aio:isTheMusicbrainzTrackWorkKeyFor
  is the Musicbrainz id for the track work. This foreign key property allows linking to track works in Musicbrainz.

  .. include:: properties/isTheMusicbrainzTrackWorkKeyFor.rst

aio:isTheInternationalVehicleRegistrationCodeFor
  is the international vehicle registration code for

  .. include:: properties/isTheInternationalVehicleRegistrationCodeFor.rst

aio:isBasedIn
  is based in. E.g., The University of Warwick is based in Coventry.

  .. include:: properties/isBasedIn.rst

aio:wasTheFirstPrimeMinisterOf
  was the first prime minister of. E.g., William Pitt the Younger was the first prime minister of Great Britain.

  .. include:: properties/wasTheFirstPrimeMinisterOf.rst

aio:isSomethingThatInvaded
  is something that invaded. E.g., The Hunnic Empire is something that invaded the Roman Empire.

  .. include:: properties/isSomethingThatInvaded.rst

aio:isThePresidentOfTheRegion
  is the president of the region

  .. include:: properties/isThePresidentOfTheRegion.rst

aio:isThePrimeMinisterOf
  is the prime minister of. E.g., Boris Johnson is the Prime Minister of the UK.

  .. include:: properties/isThePrimeMinisterOf.rst

aio:isTheStateSenatorOf
  is the state senator of. E.g., Ted Cruz and John Cornyn are the state senators of Texas.

  .. include:: properties/isTheStateSenatorOf.rst

aio:wroteTheLyricsTo
  wrote the lyrics to

  .. include:: properties/wroteTheLyricsTo.rst

aio:isAManufacturerOfAtLeastOne
  is a manufacturer of at least one. E.g., Oral-B is a manufacturer of at least one toothbrush.

  .. include:: properties/isAManufacturerOfAtLeastOne.rst

aio:isInARomanticRelationshipWith
  is in a romantic relationship with. E.g., Will is in a romantic relationship with Kate.

  .. include:: properties/isInARomanticRelationshipWith.rst

aio:isTheRuntimeOfTheMovie
  is the length of time which is the duration of the movie

  .. include:: properties/isTheRuntimeOfTheMovie.rst

aio:isASeriesFirstPublishedAtTimepoint
  is a series first published at timepoint. E.g., Harry Potter is a series first published at timepoint 1997.

  .. include:: properties/isASeriesFirstPublishedAtTimepoint.rst

aio:isTheImdbScoreFor
  is the IMDb score (a decimal numeric quality rating between 0 and 10) for the movie or TV show

  .. include:: properties/isTheImdbScoreFor.rst

aio:appearedIn
  appeared in. PE.g., Paul Casey appeared in Planet of the Dead.

  .. include:: properties/appearedIn.rst

aio:isAnAlbumTrackOn
  is a track on the album

  .. include:: properties/isAnAlbumTrackOn.rst

aio:isAppearingIn
  is appearing in. E.g., Jessica Alba is appearing in 13 Reasons Why.

  .. include:: properties/isAppearingIn.rst

aio:actedIn
  is someone who acted in the production.

  .. include:: properties/actedIn.rst

aio:isTheGovernor-GeneralOf
  is the governor-general of

  .. include:: properties/isTheGovernor-GeneralOf.rst

aio:isTheUnitUsedIn
  is the unit used in a scalar quantity. For example, meter is the unit used in 10 meters.

  .. include:: properties/isTheUnitUsedIn.rst

aio:isASubclassOf
  is a proper subclass of. Note that this property is irreflexive: it does not hold that C :isASubclassOf C. Since :isASubclassOf is irreflexive, it has different semantics from rdfs:subClassOf, which is reflexive. See also :isTheSameAsOrASubclassOf

  .. include:: properties/isASubclassOf.rst

aio:isTheOrganisationOperating
  is the organisation operating. E.g., Malaysia Airports is the organisation operating Kuala Lumpur International Airport.

  .. include:: properties/isTheOrganisationOperating.rst

aio:isTheMusicbrainzIdFor
  is the string which is the Musicbrainz id for. This foreign key property allows linking to entities in Musicbrainz.

  .. include:: properties/isTheMusicbrainzIdFor.rst

aio:isTheMinisterOfHumanResourceDevelopmentOf
  is the minister of human resource development of

  .. include:: properties/isTheMinisterOfHumanResourceDevelopmentOf.rst

aio:isTheLeaderOfTheGovernmentInTheSenateOf
  is the leader of the government in the senate of

  .. include:: properties/isTheLeaderOfTheGovernmentInTheSenateOf.rst

aio:isAChildOf
  is a child of

  .. include:: properties/isAChildOf.rst

aio:isTheBoyfriendOf
  is the boyfriend of. E.g Mac Miller is the boyfriend of Ariana Grande.

  .. include:: properties/isTheBoyfriendOf.rst

aio:admires
  admires

  .. include:: properties/admires.rst

aio:isTheHighestPointIn
  Everest is the highest point in Asia.

  .. include:: properties/isTheHighestPointIn.rst

aio:isAnAllyOf
  is an ally of. E.g., Romania is an ally of Nazi Germany.

  .. include:: properties/isAnAllyOf.rst

aio:isAEurovisionContestantRepresenting
  is a eurovision contestant representing (the country)

  .. include:: properties/isAEurovisionContestantRepresenting.rst

aio:isThePresidentOfTheProvince
  is the president of the province

  .. include:: properties/isThePresidentOfTheProvince.rst

aio:publishedTheFirstEditionOf
  is the publisher of the first edition of

  .. include:: properties/publishedTheFirstEditionOf.rst

aio:isASingleBy
  is a single by. E.g., Bohemian Rhapsody is a single by Queen.

  .. include:: properties/isASingleBy.rst

aio:isAShortDescriptionOf
  is a short English-language description of the object. Where available, this is sometimes the first sentence of the English-language wikipedia page for the object.

  .. include:: properties/isAShortDescriptionOf.rst

aio:isAnAdaptationOf
  is a creative work which is an adaption of the creative work.

  .. include:: properties/isAnAdaptationOf.rst

aio:isTheLeaderOfTheHouseOf
  is the leader of the house of

  .. include:: properties/isTheLeaderOfTheHouseOf.rst

aio:isTheMinisterForForeignPolicyOf
  is the minister for foreign policy of. E.g., Dominic Raab is the minister for foreign policy of the UK.

  .. include:: properties/isTheMinisterForForeignPolicyOf.rst

aio:isTheAverageSurfaceTemperatureOf
  is the average surface temperature of. E.g., -224 degrees celsius is the average surface temperature of Uranus.

  .. include:: properties/isTheAverageSurfaceTemperatureOf.rst

aio:isADrummerWith
  is a person who plays drums with the musical act.

  .. include:: properties/isADrummerWith.rst

aio:isATypeOfAnimalThatEats
  is a type of animal that eats. For example, dog is a type of animal that eats meat.

  .. include:: properties/isATypeOfAnimalThatEats.rst

aio:isAMemberOf
  is a person or organization which is a member of the group. For example: Mick Jagger is a member of the Rolling Stones; Ghana is a member of the United Nation.

  .. include:: properties/isAMemberOf.rst

aio:isTheCapitalOf
  is the capital of. E.g., Paris is the Capital of France, Albany is the Capital of New York.

  .. include:: properties/isTheCapitalOf.rst

aio:isTheQuarterbackOf
  is the quarterback of. E.g., Tom Brady is the quarterback of the Patriots

  .. include:: properties/isTheQuarterbackOf.rst

aio:isAFormerPresidentOf
  is a former president of

  .. include:: properties/isAFormerPresidentOf.rst

aio:isAStudioAlbumBy
  is a studio album by. E.g., Nevermind is a studio album by Nirvana.

  .. include:: properties/isAStudioAlbumBy.rst

aio:isABookBy
  is a book by. E.g., The Communist Manifesto is a book by Friedrich Engels.

  .. include:: properties/isABookBy.rst

aio:isThePresidentOf
  is the president of. E.g., Trump is the President of the United States.

  .. include:: properties/isThePresidentOf.rst

aio:isFamousFor
  is famous for. E.g., Switzerland is famous for Chocolate.

  .. include:: properties/isFamousFor.rst

aio:isThe2LetterIsoCountryCodeFor
  is the 2-letter iso country code for. "IR" is the 2-letter ISO country code for Iran.

  .. include:: properties/isThe2LetterIsoCountryCodeFor.rst

aio:isTheOfficialCurrencyUsedWithin
  is the official currency used within. For example, US dollar is the official currency used within the United States.

  .. include:: properties/isTheOfficialCurrencyUsedWithin.rst

aio:isTheDeputyLeaderOfTheHouseOf
  is the deputy leader of the house of

  .. include:: properties/isTheDeputyLeaderOfTheHouseOf.rst

aio:isAFullSisterOf
  is a full sister of, i.e. is a female sibling sharing two parents. E.g., Anne Bronte is a full sister of Charlotte Bronte. (Expressly does not contain half-sister relationships.)

  .. include:: properties/isAFullSisterOf.rst

aio:isATaxonUnder
  is a taxon (in the biological taxonomy) subsumed under the taxon. This relation has more specific subproperties which connect individual ranks of the taxonomic hierarchy - such as isASpeciesInTheGenus or isAGenusInTheFamily. This relation chains its more specific properties. e.g Canis (genus) is a taxon under chordate (phylum).

  .. include:: properties/isATaxonUnder.rst

aio:isTheChiefMinisterOf
  is the chief minister of

  .. include:: properties/isTheChiefMinisterOf.rst

aio:isTheWifeOf
  is a woman who is married to a man in a monogamous heterosexual marriage. This is a subproperty of :isMarriedTo.

  .. include:: properties/isTheWifeOf.rst

aio:isTheTopSpeedOf
  is the top speed of

  .. include:: properties/isTheTopSpeedOf.rst

aio:isAQuoteFrom
  is a quote from. E.g., "The greatest teacher, failure is" is a quote from The Last Jedi.

  .. include:: properties/isAQuoteFrom.rst

aio:isLikelyToSellMembersOf
  is likely to sell members of. E.g., College Town Bagels is likely to sell members of the class bagel.

  .. include:: properties/isLikelyToSellMembersOf.rst

aio:isTheOriginalVersionOf
  is the first-published version of the work

  .. include:: properties/isTheOriginalVersionOf.rst

aio:isAvailableInTheFormat
  is available in the format. E.g., Touch Detective is available in Nintendo Game Card.

  .. include:: properties/isAvailableInTheFormat.rst

aio:isTheBirthdateOf
  is the birthdate of

  .. include:: properties/isTheBirthdateOf.rst

aio:isThePlaceOfOriginOfTheClassOfObjects
  is the place of origin of the class of objects. E.g., The United States is the place of origin of the joystick.

  .. include:: properties/isThePlaceOfOriginOfTheClassOfObjects.rst

aio:isASingerOf
  is a singer of. E.g., Frank Sinatra is a singer of "You Go to My Head".

  .. include:: properties/isASingerOf.rst

aio:isLivingIn
  is a person who is living in the geographical area. For example, Taylor Swift lives in Nashville, Tennessee.

  .. include:: properties/isLivingIn.rst

aio:isAClassOfThingsFrequentlyFoundIn
  is a class of things frequently found in. E.g., The Coyote is a class of things frequently found in Central America.

  .. include:: properties/isAClassOfThingsFrequentlyFoundIn.rst

aio:isTheZipCodeOf
  is the zip code of. E.g., "50069" is the zip code of the 50069 zip code area.

  .. include:: properties/isTheZipCodeOf.rst

aio:isStrictlyEqualTo
  A property which checks strict equivalence. For example, :avm_genre_comedy :isStrictlyEqualTo :avm_genre_comedy. Reifying strict equality as a property like this can sometimes be useful in constructing queries.

  .. include:: properties/isStrictlyEqualTo.rst

aio:isATownIn
  is a town in. E.g., Swindon is a town in England.

  .. include:: properties/isATownIn.rst

aio:isASongFrom
  is a song from. E.g., "Breaking free" is a song from High School Musical.

  .. include:: properties/isASongFrom.rst

aio:isARandomFactAbout
  is a random English-language string expressing a fact about the object. Since these random facts are dynamically generated when queried for, they will vary over time.

  .. include:: properties/isARandomFactAbout.rst

aio:hasTheNationalAnthem
  has the national anthem

  .. include:: properties/hasTheNationalAnthem.rst

aio:isAVersionOfThePublishedWork
  is a version of the published work.

  .. include:: properties/isAVersionOfThePublishedWork.rst

aio:isTheCfoOf
  is the cfo of. E.g., James A Bell is the CFO of Boeing.

  .. include:: properties/isTheCfoOf.rst

aio:isTheComposerOf
  is the composer of the piece of music

  .. include:: properties/isTheComposerOf.rst

aio:isDiscoveredBy
  is discovered by. Penicillin is discovered by Alexander Fleming.

  .. include:: properties/isDiscoveredBy.rst

aio:isABookInTheSeries
  is a book in the series of books.

  .. include:: properties/isABookInTheSeries.rst

aio:isInHighPoliticalOfficeIn
  is in high political office in

  .. include:: properties/isInHighPoliticalOfficeIn.rst

aio:isTheChancellorOf
  is the chancellor of

  .. include:: properties/isTheChancellorOf.rst

aio:isTheBirthplaceOf
  is the place of birth of

  .. include:: properties/isTheBirthplaceOf.rst

aio:isTheGenreOf
  is the artistic category containing

  .. include:: properties/isTheGenreOf.rst

aio:isASpeciesInTheGenus
  is a taxon at the rank of species in the genus. The wolf is a species in the genus canis.

  .. include:: properties/isASpeciesInTheGenus.rst

aio:isTheRecordLabelOf
  is the record label representing the audio act

  .. include:: properties/isTheRecordLabelOf.rst

aio:isAGrandfatherOf
  is a grandfather of. E.g., Prince Philip is a grandfather of Prince Harry.

  .. include:: properties/isAGrandfatherOf.rst

aio:isTheMinisterOfFinanceOf
  is the minister of finance of

  .. include:: properties/isTheMinisterOfFinanceOf.rst

aio:isTheBassPlayerOf
  plays bass with the musical act

  .. include:: properties/isTheBassPlayerOf.rst

aio:hasAMajorRoleInTheMovie
  has a major role in the movie. E.g., Samuel L. Jackson has a major role in the movie Pulp Fiction.

  .. include:: properties/hasAMajorRoleInTheMovie.rst

aio:isThePrefixedImdbIdOf
  is the prefixed IMDb ID of the movie, actor or film-related entity. Unlike :isTheImdbIdOf, the two letter prefix of the IMDb ID is included. This foreign key property allows linking to entities in IMDb. For example, 'tt0795421' is the prefixed IMDb id of the movie Mamma Mia and 'nm0000658' is the IMDb id of Meryl Streep.

  .. include:: properties/isThePrefixedImdbIdOf.rst

aio:isSignificantlySpokenIn
  is significantly spoken in. E.g., Spanish is significantly spoken in the United States.

  .. include:: properties/isSignificantlySpokenIn.rst

aio:isLegalIn
  is legal in. E.g., Cannabis is legal in Uruguay.

  .. include:: properties/isLegalIn.rst

aio:isThePostcodeZipcodeOf
  is the postcode zipcode of. E.g., 90036 is the postcode zipcode of the Los Angeles Museum of Art.

  .. include:: properties/isThePostcodeZipcodeOf.rst

aio:isAFounderOf
  is a human being who established or co-founded an organization. For example, Reshma Saujani is a founder of Girls Who Code.

  .. include:: properties/isAFounderOf.rst

aio:isWithinTheAstronomicalObject
  is within the astronomical object. E.g., Mars is within the astronomical object the Solar System.

  .. include:: properties/isWithinTheAstronomicalObject.rst

aio:isAGuitaristWith
  is a person who plays guitar with the musical act.

  .. include:: properties/isAGuitaristWith.rst

aio:isAClassInThePhylum
  is a taxon at the rank of class in the phylum. Mammal is a class in the phylum chordate.

  .. include:: properties/isAClassInThePhylum.rst

aio:isGeographicallyLocatedWithinOrIsEqualTo
  is geographically located within or is equal to. Wales is geographically located within or is equal to the UK. The pair of this relation and isGeographicallyLocatedWithin function analagously to isTheSameAsOrASubclassOf and isASubclassOf

  .. include:: properties/isGeographicallyLocatedWithinOrIsEqualTo.rst

aio:isAGenusInTheFamily
  is a taxon at the rank of genus in the family. Canis is a genus in the family canidae.

  .. include:: properties/isAGenusInTheFamily.rst

aio:isAFullSiblingOf
  is a full sibling of, i.e. a full sister or a full brother of, sharing two parents.

  .. include:: properties/isAFullSiblingOf.rst

aio:isTheKingOf
  is the king of. E.g., Felipe VI is the King of Spain

  .. include:: properties/isTheKingOf.rst

aio:isTheIso3166-1Alpha-2CodeFor
  is the iso 3166-1 alpha-2 code for

  .. include:: properties/isTheIso3166-1Alpha-2CodeFor.rst

aio:isTheLeadGuitaristIn
  plays lead guitar with the musical act

  .. include:: properties/isTheLeadGuitaristIn.rst

aio:isTheDensityOfTheObject
  is the density of the object. For example, 687 kg per cubic meter is the density of the object Saturn.

  .. include:: properties/isTheDensityOfTheObject.rst

aio:isASequelTo
  is a sequel to. Examples include both books and films.

  .. include:: properties/isASequelTo.rst

aio:isARandomInstanceOf
  is a random instance of. E.g., Greenland is a random instance of country.

  .. include:: properties/isARandomInstanceOf.rst

aio:isAGrandmotherOf
  is a grandmother of. E.g., Queen Elizabeth is a grandmother of Prince Harry.

  .. include:: properties/isAGrandmotherOf.rst

aio:won
  won. E.g., Barack Obama won the Nobel Peace Prize, The Patriots won the Super Bowl.

  .. include:: properties/won.rst

aio:isTheWikidataIdOf
  is the wikidata id of the entity. This foreign key property allows linking to entities in Wikidata. Note that these Wikidata Ids are all lower-cased. For example, 'q873' is the Wikidata Id of Meryl Streep.

  .. include:: properties/isTheWikidataIdOf.rst

aio:isTheChemicalSymbolFor
  is the chemical symbol for. For example, 'NaH' is the chemical symbol for sodium hydride.

  .. include:: properties/isTheChemicalSymbolFor.rst

aio:isAQuoteBy
  is a quote by. E.g., "Most jokes state a bitter truth" is a quote by Larry Gelbart.

  .. include:: properties/isAQuoteBy.rst

aio:isTheAuthorAsinFor
  is the Amazon Standard Identification Number of the author

  .. include:: properties/isTheAuthorAsinFor.rst

aio:isEmployedBy
  is employed by. E.g., Jonathan Ross is employed by the BBC.

  .. include:: properties/isEmployedBy.rst

aio:isAGrandparentOf
  is a grandparent of. E.g., Queen Elizabeth and Prince Philips are both grandparents of Prince Harry.

  .. include:: properties/isAGrandparentOf.rst

aio:isTheGoodreadsIdFor
  is the Goodreads identification number for. This foreign key property allows linking to entities in Goodreads. For example, '1896276' is the Goodreads Id for Sapiens: A Brief History of Humankind

  .. include:: properties/isTheGoodreadsIdFor.rst

aio:grewUpIn
  is a person who grew up in the geographical area.

  .. include:: properties/grewUpIn.rst

aio:isTheUsableDurationForATypicalMemberOfTheClass
  is the usable duration for a typical member of the class. E.g., 5 days is the usuable duration for a typical member of the class banana.

  .. include:: properties/isTheUsableDurationForATypicalMemberOfTheClass.rst

aio:isThePredominantLanguageOf
  is the predominant language of. E.g., English is the predominant language of the UK.

  .. include:: properties/isThePredominantLanguageOf.rst

aio:isTheDeputyLeaderOfTheLiberalPartyOf
  is the deputy leader of the liberal party of

  .. include:: properties/isTheDeputyLeaderOfTheLiberalPartyOf.rst

aio:isAGovernmentMinisterOf
  is a government minister of

  .. include:: properties/isAGovernmentMinisterOf.rst

aio:isSomeoneWhoKilledThePerson
  is someone who killed the person. E.g., Mark David Chapman is someone who killed the person John Lennon.

  .. include:: properties/isSomeoneWhoKilledThePerson.rst

aio:isTheHeadquartersOf
  is the headquarters of. E.g., The UK is the headquarters of Little Chef.

  .. include:: properties/isTheHeadquartersOf.rst

aio:isTheLeadSingerOf
  is the lead singer of the ensemble

  .. include:: properties/isTheLeadSingerOf.rst

aio:isTheLeaderOfTheNationalPartyInTheSenateOf
  is the leader of the national party in the senate of

  .. include:: properties/isTheLeaderOfTheNationalPartyInTheSenateOf.rst

aio:wasDiscoveredOn
  was discovered on. E.g., Cobalt was discovered on (the date) 1737.

  .. include:: properties/wasDiscoveredOn.rst

aio:isAMovieFrom
  is a movie from. E.g., Slumdog Millionaire is a movie from the UK

  .. include:: properties/isAMovieFrom.rst

aio:isTheNationalAnthemOf
  is the national anthem of

  .. include:: properties/isTheNationalAnthemOf.rst

aio:assassinated
  assassinated. E.g., Lee Harvey Oswald assassinated John F. Kennedy.

  .. include:: properties/assassinated.rst

aio:isAnEpisodeOf
  is an episode of the tv series

  .. include:: properties/isAnEpisodeOf.rst

aio:isTheLeaderOfTheOppositionInRajyaSabhaIn
  is the leader of the opposition in rajya sabha in

  .. include:: properties/isTheLeaderOfTheOppositionInRajyaSabhaIn.rst

aio:isAnAmbassadorOf
  is an ambassador of

  .. include:: properties/isAnAmbassadorOf.rst

aio:isAMemberOfTheSeniorManagementTeamOf
  is a member of the senior management team of. E.g., Jeff Bezos is a member fo the senior management team of the Washington Post.

  .. include:: properties/isAMemberOfTheSeniorManagementTeamOf.rst

aio:isThePopulationOf
  is the population of

  .. include:: properties/isThePopulationOf.rst

aio:isTheMinisterOfCorporateAffairsOf
  is the minister of corporate affairs of

  .. include:: properties/isTheMinisterOfCorporateAffairsOf.rst

aio:isTheMinisterForHealthOf
  is the minister for health of

  .. include:: properties/isTheMinisterForHealthOf.rst

aio:isAWrittenLanguageSignificantlyUsedIn
  Written French is a written language significantly used in France.

  .. include:: properties/isAWrittenLanguageSignificantlyUsedIn.rst

aio:isThePoliticalLeaderOf
  is the political leader of

  .. include:: properties/isThePoliticalLeaderOf.rst

aio:isTheGirlfriendOf
  is the girlfriend of

  .. include:: properties/isTheGirlfriendOf.rst

aio:isAnAudioPublicationVersionBy
  is an audio publication version (a version of an audio publication) by the audio act

  .. include:: properties/isAnAudioPublicationVersionBy.rst

aio:isBuriedIn
  is buried in. E.g., Milton Joseph Cunningham is buried in Lousiana.

  .. include:: properties/isBuriedIn.rst

aio:isTheVicePresidentOfTheCountry
  is the vice president of the country

  .. include:: properties/isTheVicePresidentOfTheCountry.rst

aio:isTheQueenOf
  is the queen of. Elizabeth II is the Queen of England

  .. include:: properties/isTheQueenOf.rst

aio:isTheStateBirdOf
  is the state bird of. E.g., purple finch is the state bird of New Hampshire.

  .. include:: properties/isTheStateBirdOf.rst

aio:isACountryWherePeopleSpeak
  is a country where people speak

  .. include:: properties/isACountryWherePeopleSpeak.rst

aio:isTheMinisterOfEducationOf
  is the minister of education of

  .. include:: properties/isTheMinisterOfEducationOf.rst

aio:playsKeyBoardWith
  plays keyboard with the musical act

  .. include:: properties/playsKeyBoardWith.rst

aio:isTheDeputyChiefMinisterOf
  is the deputy chief minister of

  .. include:: properties/isTheDeputyChiefMinisterOf.rst

aio:isTheNationalSongOf
  is the national song of

  .. include:: properties/isTheNationalSongOf.rst

aio:isNamedAfter
  is named after. E.g., Raisova is named after Karel Vaclav Rais.

  .. include:: properties/isNamedAfter.rst

aio:isAnAlbumBy
  is an album by the musical act

  .. include:: properties/isAnAlbumBy.rst

aio:isASectionOfTheComposition
  is a creative work which can be seen as a section of the larger creative work. This is a general property which can be inferred from more specific subproperties.

  .. include:: properties/isASectionOfTheComposition.rst

aio:isTheEmperorOf
  is the emperor of. E.g., Naruhito is the Emperor of Japan.

  .. include:: properties/isTheEmperorOf.rst

aio:shot
  shot

  .. include:: properties/shot.rst

aio:isTheNicknameFor
  is the nickname for. E.g., "The City that Never Sleeps" is the nickname for New York City.

  .. include:: properties/isTheNicknameFor.rst

aio:isASpokenFormOf
  is a spoken form of. E.g., English is the spoken form of written English.

  .. include:: properties/isASpokenFormOf.rst

aio:isTheCountryOfNationalityOf
  is the country of nationality of

  .. include:: properties/isTheCountryOfNationalityOf.rst

aio:isAnUncleOf
  is an uncle (brother or brother-in-law of a parent) of. For example, Prince Andrew is an uncle of Prince Harry.

  .. include:: properties/isAnUncleOf.rst

aio:isTheDepthOf
  is the depth of. For example, 43 meters is the depth of the Sea of Galilee.

  .. include:: properties/isTheDepthOf.rst

aio:joined
  joined. E.g., Bhutan joined the United Nations.

  .. include:: properties/joined.rst

aio:isASpecialEnglishLanguageTermFor
  is a special english-language term for. E.g., "scorrie" is a special english-language term for an infant gull.

  .. include:: properties/isASpecialEnglishLanguageTermFor.rst

aio:isTheDeputyPrimeMinisterOf
  is the deputy prime minister of

  .. include:: properties/isTheDeputyPrimeMinisterOf.rst

aio:uses
  uses. E.g., The British Army uses the Land Rover Defender.

  .. include:: properties/uses.rst

aio:isTheChiefOfTheArmyStaffOf
  is the chief of the army staff of

  .. include:: properties/isTheChiefOfTheArmyStaffOf.rst

aio:isAPoliticalPartyIn
  is a political party in

  .. include:: properties/isAPoliticalPartyIn.rst

aio:performsWith
  performs with the musical act

  .. include:: properties/performsWith.rst

aio:isTheStateFishOf
  is the state fish of. E.g., rainbow trout is the state fish of Washington State.

  .. include:: properties/isTheStateFishOf.rst

aio:isADaughterOf
  is a daughter of. E.g., Malia Obama is a daughter of Barack Obama.

  .. include:: properties/isADaughterOf.rst

aio:isASiblingOf
  is a sibling of, i.e. shares at least one parent with.

  .. include:: properties/isASiblingOf.rst

aio:isAnInstanceOf
  Equivalent to rdf:type.

  .. include:: properties/isAnInstanceOf.rst

aio:wasFoundedIn
  was founded in (the place). E.g., Rocket Child was founded in Chile.

  .. include:: properties/wasFoundedIn.rst

aio:hasABranchIn
  has a branch in. E.g., Starbucks has a branch in Seattle.

  .. include:: properties/hasABranchIn.rst

aio:isTheDateOfDeathOf
  is the date of death of

  .. include:: properties/isTheDateOfDeathOf.rst

aio:isTheHeightOf
  is the height of. E.g., 1,250 feet is the height of the Empire State Building.

  .. include:: properties/isTheHeightOf.rst

aio:isASonOf
  is a son of. E.g., Alexander the Great is a son of Olympias.

  .. include:: properties/isASonOf.rst

aio:isAComponentOf
  is a component of. E.g., Wales is a component of the United Kingdom.

  .. include:: properties/isAComponentOf.rst

aio:isTheOriginOf
  is the origin of. E.g., Birminham UK is the origin of Black Sabbath.

  .. include:: properties/isTheOriginOf.rst

aio:isTheAstronomicalDistanceTo
  is the astronomical distance to. For example, 149.6 million km is the distance to the sun.

  .. include:: properties/isTheAstronomicalDistanceTo.rst

aio:built
  built. E.g., Ingalis Shipbuilding built the USS Connolly.

  .. include:: properties/built.rst

aio:isAPlaceSharingABorderWith
  is a place sharing a border with. E.g., Wales is a place sharing a border with England.

  .. include:: properties/isAPlaceSharingABorderWith.rst

aio:created
  is a person or organization that created the creative work, manufactured product or organization. This is a general property which can be inferred from several more specific subproperties.

  .. include:: properties/created.rst

aio:isTheRecordLabelThatProduced
  is the record label that produced the audio publication

  .. include:: properties/isTheRecordLabelThatProduced.rst

aio:isTheMinisterOfExternalAffairsOf
  is the minister of external affairs of

  .. include:: properties/isTheMinisterOfExternalAffairsOf.rst

aio:isAMiddleNameOf
  is a middle-name of. E.g., "Benjamin" is a middle-name of "Anthony Benjamin Charles".

  .. include:: properties/isAMiddleNameOf.rst

aio:isAMemberOfTheBand
  is a member of the band. E.g., Dave Grohl is a member of the band Nirvana.

  .. include:: properties/isAMemberOfTheBand.rst

aio:isTheIqOnAStandardDeviation15TestOf
  is the iq on a standard deviation 15 test of. E.g., 160 is the IQ on a standard deviation 15 test of Stephen Hawking.

  .. include:: properties/isTheIqOnAStandardDeviation15TestOf.rst

aio:isTheDeputyLeaderOfTheNationalsPartyOf
  is the deputy leader of the nationals party of

  .. include:: properties/isTheDeputyLeaderOfTheNationalsPartyOf.rst

aio:isTheDateOfTheFoundingOf
  is the date of the founding of

  .. include:: properties/isTheDateOfTheFoundingOf.rst

aio:isAnOrderInTheClass
  is a taxon at the rank of order in the class. Carnivora is an order in the class mammal.

  .. include:: properties/isAnOrderInTheClass.rst

aio:isInTheCastOfTheScreenedProduction
  is in the cast of the screened production. E.g., Leonard Nimoy is in the cast of the screened production Star Trek.

  .. include:: properties/isInTheCastOfTheScreenedProduction.rst

