aio:hectare
  hectare

  .. include:: individuals/hectare.rst

aio:avm_genre_childrens
  The childrens genre for animated visual media

  .. include:: individuals/avm_genre_childrens.rst

aio:avm_genre_coming_of_age
  The coming of age genre for animated visual media

  .. include:: individuals/avm_genre_coming_of_age.rst

aio:calories_per_100g
  Calories per 100g

  .. include:: individuals/calories_per_100g.rst

aio:avm_genre_fantasy
  The fantasy genre for animated visual media

  .. include:: individuals/avm_genre_fantasy.rst

aio:mohs_hardness
  Mohs hardness

  .. include:: individuals/mohs_hardness.rst

aio:centimeter
  centimetre

  .. include:: individuals/centimeter.rst

aio:milligram
  milligram

  .. include:: individuals/milligram.rst

aio:kilometers_per_hour
  kilometres per hour

  .. include:: individuals/kilometers_per_hour.rst

aio:grams_per_cubic_centimeter
  grams per cubic centimeter

  .. include:: individuals/grams_per_cubic_centimeter.rst

aio:avm_genre_horror
  The horror genre for animated visual media

  .. include:: individuals/avm_genre_horror.rst

aio:avm_genre_disaster
  The disaster genre for animated visual media

  .. include:: individuals/avm_genre_disaster.rst

aio:lux
  lux

  .. include:: individuals/lux.rst

aio:degree_fahrenheit
  degree Fahrenheit

  .. include:: individuals/degree_fahrenheit.rst

aio:month
  month

  .. include:: individuals/month.rst

aio:meter_per_second
  metre per second

  .. include:: individuals/meter_per_second.rst

aio:miles_per_hour
  miles per hour

  .. include:: individuals/miles_per_hour.rst

aio:cubic_meter
  cubic metre

  .. include:: individuals/cubic_meter.rst

aio:ohm
  ohm

  .. include:: individuals/ohm.rst

aio:avm_genre_spy
  The spy genre for animated visual media

  .. include:: individuals/avm_genre_spy.rst

aio:lightyear
  light-year

  .. include:: individuals/lightyear.rst

aio:avm_genre_adventure
  The adventure genre for animated visual media

  .. include:: individuals/avm_genre_adventure.rst

aio:degree
  degree

  .. include:: individuals/degree.rst

aio:avm_genre_zombie
  The zombie genre for animated visual media

  .. include:: individuals/avm_genre_zombie.rst

aio:day
  day

  .. include:: individuals/day.rst

aio:mile
  mile

  .. include:: individuals/mile.rst

aio:avm_genre_anime
  The anime genre for animated visual media

  .. include:: individuals/avm_genre_anime.rst

aio:avm_genre_documentary
  The documentary genre for animated visual media

  .. include:: individuals/avm_genre_documentary.rst

aio:avm_genre_mockumentary
  The mockumentary genre for animated visual media

  .. include:: individuals/avm_genre_mockumentary.rst

aio:kilogram_per_liter
  kilogram per litre

  .. include:: individuals/kilogram_per_liter.rst

aio:liters_per_kilometer
  liters per kilometer

  .. include:: individuals/liters_per_kilometer.rst

aio:avm_genre_short
  The short genre for animated visual media

  .. include:: individuals/avm_genre_short.rst

aio:gigabyte
  gigabyte

  .. include:: individuals/gigabyte.rst

aio:second
  second

  .. include:: individuals/second.rst

aio:cubic_mile
  cubic mile

  .. include:: individuals/cubic_mile.rst

aio:newton
  newton

  .. include:: individuals/newton.rst

aio:inch
  inch

  .. include:: individuals/inch.rst

aio:avm_genre_spaghetti_western
  The spaghetti western genre for animated visual media

  .. include:: individuals/avm_genre_spaghetti_western.rst

aio:avm_genre_slasher
  The slasher genre for animated visual media

  .. include:: individuals/avm_genre_slasher.rst

aio:square_mile
  square mile

  .. include:: individuals/square_mile.rst

aio:avm_genre_family
  The family genre for animated visual media

  .. include:: individuals/avm_genre_family.rst

aio:picometer
  picometre

  .. include:: individuals/picometer.rst

aio:avm_genre_wuxia
  The wuxia genre for animated visual media

  .. include:: individuals/avm_genre_wuxia.rst

aio:m_unit_of_brightness
  m unit of brightness

  .. include:: individuals/m_unit_of_brightness.rst

aio:volt
  volt

  .. include:: individuals/volt.rst

aio:calorie
  Calorie

  .. include:: individuals/calorie.rst

aio:us_dollar
  US dollar

  .. include:: individuals/us_dollar.rst

aio:pound_sterling
  pound sterling

  .. include:: individuals/pound_sterling.rst

aio:kelvin
  kelvin

  .. include:: individuals/kelvin.rst

aio:avm_genre_war
  The war genre for animated visual media

  .. include:: individuals/avm_genre_war.rst

aio:avm_genre_silent
  The silent genre for animated visual media

  .. include:: individuals/avm_genre_silent.rst

aio:number_per_square_kilometer
  number per square kilometer

  .. include:: individuals/number_per_square_kilometer.rst

aio:square_meter
  square metre

  .. include:: individuals/square_meter.rst

aio:kilohertz
  kilohertz

  .. include:: individuals/kilohertz.rst

aio:avm_genre_satire
  The satire genre for animated visual media

  .. include:: individuals/avm_genre_satire.rst

aio:avm_genre_sports
  The sports genre for animated visual media

  .. include:: individuals/avm_genre_sports.rst

aio:candela_unit_of_luminous_intensity
  candela

  .. include:: individuals/candela_unit_of_luminous_intensity.rst

aio:year
  year

  .. include:: individuals/year.rst

aio:pixel_unit
  pixel

  .. include:: individuals/pixel_unit.rst

aio:gram
  gram

  .. include:: individuals/gram.rst

aio:ounce
  ounce

  .. include:: individuals/ounce.rst

aio:square_kilometer
  square kilometre

  .. include:: individuals/square_kilometer.rst

aio:grams_per_100g
  grams per 100g

  .. include:: individuals/grams_per_100g.rst

aio:kilogram
  kilogram

  .. include:: individuals/kilogram.rst

aio:avm_genre_teen
  The teen genre for animated visual media

  .. include:: individuals/avm_genre_teen.rst

aio:millimeter
  millimetre

  .. include:: individuals/millimeter.rst

aio:unified_atomic_mass_unit
  unified atomic mass unit

  .. include:: individuals/unified_atomic_mass_unit.rst

aio:avm_genre_black_comedy
  The black comedy genre for animated visual media

  .. include:: individuals/avm_genre_black_comedy.rst

aio:watt
  watt

  .. include:: individuals/watt.rst

aio:solar_mass
  solar mass

  .. include:: individuals/solar_mass.rst

aio:foot
  foot

  .. include:: individuals/foot.rst

aio:grams_per_mole
  grams per mole

  .. include:: individuals/grams_per_mole.rst

aio:kilometer
  kilometre

  .. include:: individuals/kilometer.rst

aio:hour
  hou

  .. include:: individuals/hour.rst

aio:minute
  minute

  .. include:: individuals/minute.rst

aio:avm_genre_crime
  The crime genre for animated visual media

  .. include:: individuals/avm_genre_crime.rst

aio:scoville_heat_unit
  Scoville heat unit

  .. include:: individuals/scoville_heat_unit.rst

aio:avm_genre_romance
  The romance genre for animated visual media

  .. include:: individuals/avm_genre_romance.rst

aio:hertz
  hertz

  .. include:: individuals/hertz.rst

aio:astronomical_unit
  astronomical unit

  .. include:: individuals/astronomical_unit.rst

aio:kilogram_per_cubic_meter
  kilogram per cubic metre

  .. include:: individuals/kilogram_per_cubic_meter.rst

aio:number_per_square_mile
  number per square mile

  .. include:: individuals/number_per_square_mile.rst

aio:calories_per_hour
  Calories per hour

  .. include:: individuals/calories_per_hour.rst

aio:avm_genre_film-noir
  The film-noir genre for animated visual media

  .. include:: individuals/avm_genre_film-noir.rst

aio:avm_genre_indie
  The indie genre for animated visual media

  .. include:: individuals/avm_genre_indie.rst

aio:week
  week

  .. include:: individuals/week.rst

aio:avm_genre_stop_motion
  The stop motion genre for animated visual media

  .. include:: individuals/avm_genre_stop_motion.rst

aio:avm_genre_martial_arts
  The martial arts genre for animated visual media

  .. include:: individuals/avm_genre_martial_arts.rst

aio:avm_genre_superhero
  The superhero genre for animated visual media

  .. include:: individuals/avm_genre_superhero.rst

aio:bar_unit_of_pressure
  bar

  .. include:: individuals/bar_unit_of_pressure.rst

aio:megapixel
  megapixel

  .. include:: individuals/megapixel.rst

aio:newton_meter
  newton meter

  .. include:: individuals/newton_meter.rst

aio:avm_genre_action
  The action genre for animated visual media

  .. include:: individuals/avm_genre_action.rst

aio:us_gallon
  US gallon

  .. include:: individuals/us_gallon.rst

aio:avm_genre_musical
  The musical genre for animated visual media

  .. include:: individuals/avm_genre_musical.rst

aio:avm_genre_western
  The western genre for animated visual media

  .. include:: individuals/avm_genre_western.rst

aio:euro
  Euro

  .. include:: individuals/euro.rst

aio:meter
  meter

  .. include:: individuals/meter.rst

aio:avm_genre_animation
  The animation genre for animated visual media

  .. include:: individuals/avm_genre_animation.rst

aio:avm_genre_drama
  The drama genre for animated visual media

  .. include:: individuals/avm_genre_drama.rst

aio:avm_genre_romantic_comedy
  The romantic comedy genre for animated visual media

  .. include:: individuals/avm_genre_romantic_comedy.rst

aio:degree_celsius
  degree Celsius

  .. include:: individuals/degree_celsius.rst

aio:avm_genre_parody
  The parody genre for animated visual media

  .. include:: individuals/avm_genre_parody.rst

aio:pound_avoirdupois
  pound

  .. include:: individuals/pound_avoirdupois.rst

aio:knot_unit
  knot

  .. include:: individuals/knot_unit.rst

aio:acre
  acre

  .. include:: individuals/acre.rst

aio:avm_genre_thriller
  The thriller genre for animated visual media

  .. include:: individuals/avm_genre_thriller.rst

aio:avm_genre_comedy
  The comedy genre for animated visual media

  .. include:: individuals/avm_genre_comedy.rst

aio:megahertz
  megahertz

  .. include:: individuals/megahertz.rst

aio:avm_genre_mystery
  The mystery genre for animated visual media

  .. include:: individuals/avm_genre_mystery.rst

aio:square_centimeter
  square centimetre

  .. include:: individuals/square_centimeter.rst

aio:avm_genre_sci-fi
  The sci-fi genre for animated visual media

  .. include:: individuals/avm_genre_sci-fi.rst

aio:avm_genre_biographical
  The biographical genre for animated visual media

  .. include:: individuals/avm_genre_biographical.rst

