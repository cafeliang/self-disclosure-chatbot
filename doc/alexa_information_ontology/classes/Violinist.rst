
Direct super classes:
        * aio:StringPlayer

Direct sub classes:
        * aio:JazzViolinist

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
