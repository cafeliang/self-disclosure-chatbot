
Direct super classes:
        * aio:AudioPublication

Direct sub classes:
        * aio:CompilationAlbum
        * aio:StudioAlbum
        * aio:CoverAlbum
        * aio:TributeAlbum
        * aio:SoundtrackToAMusical
        * aio:FilmScoreAlbum
        * aio:SoundtrackAlbum
        * aio:TelevisionThemeAlbum
        * aio:LiveAlbum

Used in the *domain* of the following properties:
        * aio:isAStudioAlbumBy

Used in the *range* of the following properties:
        * aio:isAnAlbumTrackOn
