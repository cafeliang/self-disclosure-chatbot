
Direct super classes:
        * aio:AgreementMakingEntity

Direct sub classes:
        * aio:MusicalAct

Used in the *domain* of the following properties:
        * aio:isAnAudioActFeaturedOnTheWork

Used in the *range* of the following properties:
        * aio:isAnAudioPublicationVersionBy
        * aio:isAStudioAlbumBy
        * aio:isTheRecordLabelOf
        * aio:isAnAlbumBy
        * aio:isAnAudioPublicationBy
        * aio:isATrackWorkBy
