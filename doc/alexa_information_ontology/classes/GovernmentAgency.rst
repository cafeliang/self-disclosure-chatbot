
Direct super classes:
        * aio:GovernmentalOrganisation

Direct sub classes:
        * aio:UsGovernmentAgency

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
