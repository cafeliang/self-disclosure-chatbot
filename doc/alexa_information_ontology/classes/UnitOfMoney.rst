
Direct super classes:
        * aio:UnitOfMeasurement

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isTheOfficialCurrencyUsedWithin

Used in the *range* of the following properties:
        * *None*
