
Direct super classes:
        * aio:Thing

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isSomewhereRelatedToTheLocationOf
        * aio:isNear
        * aio:isGeographicallyLocatedWithin

Used in the *range* of the following properties:
        * aio:isSomewhereRelatedToTheLocationOf
        * aio:isNear
        * aio:isThePostcodeZipcodeOf
