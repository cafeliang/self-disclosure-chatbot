
Direct super classes:
        * aio:BasketballPlayer

Direct sub classes:
        * aio:SmallForward
        * aio:PowerForward

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
