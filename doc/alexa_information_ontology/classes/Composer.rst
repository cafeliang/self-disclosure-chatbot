
Direct super classes:
        * aio:Musician

Direct sub classes:
        * aio:SingerSongwriter
        * aio:OperaComposer

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
