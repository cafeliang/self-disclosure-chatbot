
Direct super classes:
        * aio:Class

Direct sub classes:
        * aio:SoccerPosition
        * aio:HockeyPosition
        * aio:CricketPosition
        * aio:BasketballPosition
        * aio:AmericanFootballPosition

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
