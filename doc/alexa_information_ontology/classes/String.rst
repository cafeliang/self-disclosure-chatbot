
Direct super classes:
        * aio:Thing

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isThe2LetterIsoCountryCodeFor
        * aio:isTheIso3166-1Alpha-2CodeFor
        * aio:isTheLastNameOf
        * aio:isThePostcodeZipcodeOf
        * aio:isTheCollectiveNounForTheClass
        * aio:isThePrefixedImdbIdOf
        * aio:isAMiddleNameOf
        * aio:isTheZipCodeOf
        * aio:isAQuoteBy
        * aio:isTheFullOfficialNameOf
        * aio:isTheMusicbrainzTrackWorkKeyFor
        * aio:isAQuoteFrom
        * aio:isTheAuthorAsinFor
        * aio:isTheSloganFor
        * aio:isTheNicknameFor
        * aio:isAnIso3166CodeForAllOrPartOf
        * aio:isTheInternationalVehicleRegistrationCodeFor
        * aio:isThePlaceNameAbbreviationFor
        * aio:isTheGoodreadsIdFor
        * aio:isTheWikidataIdOf
        * aio:isTheMusicbrainzIdFor
        * aio:isASpecialEnglishLanguageTermFor
        * aio:isARandomFactAbout
        * aio:isTheChemicalSymbolFor
        * aio:isAQuoteAbout
        * aio:isAShortDescriptionOf
        * aio:isTheImdbIdOf

Used in the *range* of the following properties:
        * aio:wasFormerlyCalled
        * aio:prefLabel
