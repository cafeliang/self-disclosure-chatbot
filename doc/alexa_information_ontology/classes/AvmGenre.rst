
Direct super classes:
        * aio:Genre

Direct sub classes:
        * aio:FilmGenre

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
