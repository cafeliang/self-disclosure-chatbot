
Direct super classes:
        * aio:GovernmentalOrganisation

Direct sub classes:
        * aio:Navy
        * aio:Army
        * aio:AirForce

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
