
Direct super classes:
        * aio:PhysicalObject

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isTheMassOf
