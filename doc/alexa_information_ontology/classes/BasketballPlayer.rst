
Direct super classes:
        * aio:Athlete

Direct sub classes:
        * aio:NbaPlayer
        * aio:Guard
        * aio:Center
        * aio:BasketballForward

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
