
Direct super classes:
        * aio:Politician

Direct sub classes:
        * aio:UsSenator
        * aio:UsRepresentative

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
