
Direct super classes:
        * aio:Athlete

Direct sub classes:
        * aio:ProfessionalWrestler

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
