
Direct super classes:
        * aio:Athlete

Direct sub classes:
        * aio:ThirdBaseman
        * aio:SecondBaseman
        * aio:Pitcher
        * aio:Outfielder
        * aio:FirstBaseman
        * aio:Catcher
        * aio:BaseballShortstop

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
