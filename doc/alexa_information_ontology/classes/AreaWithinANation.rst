
Direct super classes:
        * aio:GeographicalArea

Direct sub classes:
        * aio:AdministrativeAreaWithinANation

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
