
Direct super classes:
        * aio:Thing

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isTheDateOfCreationOf
        * aio:isTheBirthdateOf
        * aio:isTheDateOfTheFoundingOf
        * aio:isTheDateOfTheEstablishmentOf
        * aio:isTheDateOfDeathOf
        * aio:isTheLaunchDateOfTheProduct

Used in the *range* of the following properties:
        * aio:wasDiscoveredOn
        * aio:wasPublishedAtTimepoint
        * aio:isASeriesFirstPublishedAtTimepoint
        * aio:becameIndependentOn
