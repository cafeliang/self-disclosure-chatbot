
Direct super classes:
        * aio:Organism

Direct sub classes:
        * aio:RaceHorse
        * aio:HumanBeing

Used in the *domain* of the following properties:
        * aio:isInTheCastOfTheScreenedProduction
        * aio:isAppearingIn
        * aio:appearedIn
        * aio:actedIn

Used in the *range* of the following properties:
        * aio:isTheBirthplaceOf
        * aio:isTheBirthdateOf
        * aio:isTheDateOfDeathOf
