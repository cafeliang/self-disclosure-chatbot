
Direct super classes:
        * aio:MusicPublication

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isASingleBy

Used in the *range* of the following properties:
        * *None*
