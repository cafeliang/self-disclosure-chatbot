
Direct super classes:
        * aio:Instrumentalist

Direct sub classes:
        * aio:Violist
        * aio:Violinist
        * aio:Guitarist
        * aio:Bassist

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
