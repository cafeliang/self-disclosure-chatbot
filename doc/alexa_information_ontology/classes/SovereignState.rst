
Direct super classes:
        * aio:Country

Direct sub classes:
        * aio:Republic
        * aio:NationState
        * aio:Monarchy
        * aio:DemocraticState
        * aio:CommunistCountry

Used in the *domain* of the following properties:
        * aio:becameIndependentOn

Used in the *range* of the following properties:
        * aio:isTheMinisterForForeignPolicyOf
        * aio:isTheEmperorOf
