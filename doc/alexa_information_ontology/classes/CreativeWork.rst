
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:Movie
        * aio:AnimatedVisualMedium
        * aio:PieceOfSoftware
        * aio:AudioWork
        * aio:PublishedWrittenWork
        * aio:PublishedThing

Used in the *domain* of the following properties:
        * aio:isASectionOfTheComposition
        * aio:isAnAdaptationOf

Used in the *range* of the following properties:
        * aio:isTheGenreOf
        * aio:isTheAuthorOf
        * aio:isASectionOfTheComposition
        * aio:isAnAdaptationOf
