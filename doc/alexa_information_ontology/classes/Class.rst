
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:SportsPosition

Used in the *domain* of the following properties:
        * aio:isAClassOfThingsFrequentlyFoundIn
        * aio:isTheSameAsOrASubclassOf

Used in the *range* of the following properties:
        * aio:isTheUsableDurationForATypicalMemberOfTheClass
        * aio:isTheSameAsOrASubclassOf
        * aio:isAManufacturerOfAtLeastOne
        * aio:isLikelyToSellMembersOf
        * aio:isAnInstanceOf
        * aio:isTheCollectiveNounForTheClass
        * aio:isAvailableInTheFormat
        * aio:isTheManufacturerOfEvery
        * aio:isThePlaceOfOriginOfTheClassOfObjects
        * aio:isTheLaunchDateOfTheProduct
        * aio:isARandomInstanceOf
