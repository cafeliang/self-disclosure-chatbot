
Direct super classes:
        * aio:StringPlayer

Direct sub classes:
        * aio:LeadGuitarPlayer
        * aio:JazzGuitarist
        * aio:BluesGuitarist
        * aio:AcousticGuitarist

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
