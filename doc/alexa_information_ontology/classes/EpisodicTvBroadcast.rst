
Direct super classes:
        * aio:AnimatedVisualMedium

Direct sub classes:
        * aio:TvSeries

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isAnEpisodeOf
