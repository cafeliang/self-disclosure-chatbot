
Direct super classes:
        * aio:GeographicalArea

Direct sub classes:
        * aio:AdministrativeAreaWithinANation
        * aio:GeographicalAreaOfSignificantSize
        * aio:Constituency
        * aio:Settlement

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isThePopulationOf
        * aio:isThePresidentOfTheRegion
        * aio:isThePresidentOfTheProvince
        * aio:isTheGovernor-GeneralOf
        * aio:isTheAttorneyGeneralOf
        * aio:isInHighPoliticalOfficeIn
        * aio:isTheGovernorOf
        * aio:isTheZipCodeOf
        * aio:isThePoliticalLeaderOf
        * aio:isThePopulationDensityOf
        * aio:isTheFounderOfThePlace
        * aio:isTheDateOfTheEstablishmentOf
        * aio:isInPoliticalOfficeIn
        * aio:isThePresidentOf
