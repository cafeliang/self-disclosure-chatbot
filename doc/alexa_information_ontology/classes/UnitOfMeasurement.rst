
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:UnitOfVoltage
        * aio:UnitOfPower
        * aio:MetricUnitOfMeasurement
        * aio:UsCustomaryUnitOfMeasurement
        * aio:UnitOfAngle
        * aio:UnitOfArea
        * aio:UnitOfElectricalResistance
        * aio:UnitOfIllumination
        * aio:UnitOfTemperature
        * aio:UnitOfMass
        * aio:UnitOfData
        * aio:UnitOfPungency
        * aio:UnitOfCalorificValuePerWeightOrVolume
        * aio:UnitOfSpeed
        * aio:UnitOfVolume
        * aio:UnitOfTorque
        * aio:UnitOfVolumeDistanceRatio
        * aio:UnitOfMoney
        * aio:ImperialUnitOfMeasurement
        * aio:UnitOfPressure
        * aio:UnitOfTime
        * aio:UnitOfSurfaceDensity
        * aio:UnitOfEnergyPerUnitOfTime
        * aio:UnitOfMassPerWeightOrVolume
        * aio:UnitOfLength
        * aio:UnitOfForce
        * aio:UnitOfEnergy
        * aio:UnitOfConcentration
        * aio:UkCustomaryUnitOfMeasurement
        * aio:UnitOfMolecularWeight
        * aio:UnitOfDigitalImaging
        * aio:UnitOfHardness
        * aio:UnitOfDensity
        * aio:UnitOfFrequency
        * aio:UnitOfLuminousIntensity
        * aio:UnitOfBrightness

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
