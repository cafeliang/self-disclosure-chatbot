
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:TerroristAct
        * aio:SportingEvent
        * aio:Referendum
        * aio:MilitaryEvent
        * aio:Election

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
