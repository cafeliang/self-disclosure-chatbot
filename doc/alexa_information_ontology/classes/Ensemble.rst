
Direct super classes:
        * aio:MusicalAct
        * aio:GroupWithMembers

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isASingerWith
        * aio:isAMemberOfTheBand
        * aio:isTheLeadGuitaristIn
        * aio:isTheLeadSingerOf
