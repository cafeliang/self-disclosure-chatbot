
Direct super classes:
        * aio:AmericanFootballer

Direct sub classes:
        * aio:TightEnd
        * aio:DefensiveEnd

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
