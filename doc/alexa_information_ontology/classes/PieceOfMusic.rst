
Direct super classes:
        * aio:AudioWork

Direct sub classes:
        * aio:MusicPublication

Used in the *domain* of the following properties:
        * aio:isASongFrom

Used in the *range* of the following properties:
        * aio:isTheComposerOf
        * aio:performed
