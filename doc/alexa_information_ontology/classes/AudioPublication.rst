
Direct super classes:
        * aio:AudioWork

Direct sub classes:
        * aio:MusicPublication
        * aio:Album

Used in the *domain* of the following properties:
        * aio:isAnAudioPublicationBy

Used in the *range* of the following properties:
        * *None*
