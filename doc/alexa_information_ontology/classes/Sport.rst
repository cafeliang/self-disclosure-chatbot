
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:TeamSport

Used in the *domain* of the following properties:
        * aio:isTheSportOfTheTeam

Used in the *range* of the following properties:
        * *None*
