
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:PublishedWrittenWork

Used in the *domain* of the following properties:
        * aio:isTheOriginalVersionOf

Used in the *range* of the following properties:
        * aio:isTheOriginalVersionOf
        * aio:isAVersionOfThePublishedWork
        * aio:isSomeoneWhoReleased
        * aio:isAQuoteFrom
