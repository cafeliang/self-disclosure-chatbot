
Direct super classes:
        * aio:MemberOfRoyalty

Direct sub classes:
        * aio:Queen
        * aio:King

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
