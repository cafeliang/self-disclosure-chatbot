
Direct super classes:
        * aio:PublishedThing

Direct sub classes:
        * aio:BookPublication

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
