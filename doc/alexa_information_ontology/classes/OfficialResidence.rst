
Direct super classes:
        * aio:GeographicalArea

Direct sub classes:
        * aio:PresidentialResidence
        * aio:Palace

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
