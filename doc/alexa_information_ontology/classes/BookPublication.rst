
Direct super classes:
        * aio:WrittenPublication

Direct sub classes:
        * aio:BookEdition

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
