
Direct super classes:
        * aio:Athlete

Direct sub classes:
        * aio:DefensiveBack
        * aio:DefensiveTackle
        * aio:RunningBack
        * aio:Fullback
        * aio:OffensiveTackle
        * aio:AmericanFootballGuard
        * aio:AmericanFootballEnd
        * aio:Quarterback
        * aio:Halfback
        * aio:Linebacker
        * aio:Lineman
        * aio:Cornerback
        * aio:WideReceiver

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
