
Direct super classes:
        * aio:EpisodicTvBroadcast

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isAppearingIn
        * aio:isACreatorOfTheTvSeries
