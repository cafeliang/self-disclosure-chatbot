
Direct super classes:
        * aio:PopulatedPlace
        * aio:AreaWithinANation

Direct sub classes:
        * aio:USState

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
