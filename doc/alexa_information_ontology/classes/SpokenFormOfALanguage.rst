
Direct super classes:
        * aio:Thing

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isThePredominantLanguageOf
        * aio:isASpokenFormOf
        * aio:isSignificantlySpokenIn

Used in the *range* of the following properties:
        * aio:isACountryWherePeopleSpeak
