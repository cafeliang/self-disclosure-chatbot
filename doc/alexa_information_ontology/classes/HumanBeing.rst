
Direct super classes:
        * aio:Animal
        * aio:AgreementMakingEntity

Direct sub classes:
        * aio:Businessperson
        * aio:Soldier
        * aio:Actor
        * aio:RecordProducer
        * aio:FashionDesigner
        * aio:Musician
        * aio:FirstLadyOfTheUnitedStates
        * aio:Engineer
        * aio:Academic
        * aio:FilmProducer
        * aio:Teacher
        * aio:UsDemocrat
        * aio:Inventor
        * aio:Terrorist
        * aio:Celebrity
        * aio:UsRepublican
        * aio:Athlete
        * aio:TvProducer
        * aio:TvPersonality
        * aio:Comedian
        * aio:Activist
        * aio:Anarchist
        * aio:Director
        * aio:Pacifist
        * aio:SportsCoach
        * aio:MakeupArtist
        * aio:Model
        * aio:TradeUnionist
        * aio:MemberOfRoyalty
        * aio:Socialist
        * aio:BaseballManager
        * aio:Feminist
        * aio:Scientist
        * aio:Diplomat
        * aio:Writer
        * aio:Politician
        * aio:Communist

Used in the *domain* of the following properties:
        * aio:isAGuitaristWith
        * aio:isTheVicePresidentOfTheCountry
        * aio:isThePresidentOfTheProvince
        * aio:isEngagedTo
        * aio:isADrummerWith
        * aio:isTheBoyfriendOf
        * aio:isTheLeadSingerOf
        * aio:isTheCeoOf
        * aio:isTheHusbandOf
        * aio:isAGrandmotherOf
        * aio:isRelatedTo
        * aio:isAParentOf
        * aio:isTheMinisterOfHumanResourceDevelopmentOf
        * aio:admires
        * aio:isTheEmperorOf
        * aio:isAFullSiblingOf
        * aio:isAFormerPresidentOf
        * aio:isMarriedTo
        * aio:isTheMinisterOfEducationOf
        * aio:isTheMinisterOfDefenceOf
        * aio:isTheGovernor-GeneralOf
        * aio:assassinated
        * aio:isAnAuntOf
        * aio:isTheChiefMinisterOf
        * aio:isInHighPoliticalOfficeIn
        * aio:isTheVicePresidentDuringThePresidencyOf
        * aio:isTheBassPlayerOf
        * aio:isASiblingOf
        * aio:isTheDeputyPrimeMinisterOf
        * aio:wentTo
        * aio:isInPoliticalOfficeIn
        * aio:isTheLeaderOfTheOppositionIn
        * aio:isThePresidentOf
        * aio:hasAMajorRoleInTheMovie
        * aio:isEmployedBy
        * aio:isTheMinisterOfHealthAndFamilyWelfareOf
        * aio:isThePrimeMinisterOf
        * aio:isTheDeputyFirstMinisterOf
        * aio:isTheMusicalComposerForTheMovie
        * aio:isTheLeaderOfTheOppositionInRajyaSabhaIn
        * aio:isTheMotherOf
        * aio:isTheMinisterForHealthOf
        * aio:isTheDeputyLeaderOfTheHouseOf
        * aio:isSomeoneWhoMarried
        * aio:isTheDeputyChiefMinisterOf
        * aio:isTheStateSenatorOf
        * aio:isASonOf
        * aio:playsKeyBoardWith
        * aio:isTheChiefOfTheArmyStaffOf
        * aio:isTheMinisterOfCorporateAffairsOf
        * aio:performsWith
        * aio:isAnAmbassadorOf
        * aio:isTheMinisterOfEnergyOf
        * aio:isTheQueenOf
        * aio:isTheChancellorOf
        * aio:isACitizenOf
        * aio:isAnUncleOf
        * aio:wroteTheLyricsTo
        * aio:wasTheFirstToLandOn
        * aio:isAGovernmentMinisterOf
        * aio:isTheMinisterOfExternalAffairsOf
        * aio:isTheQuarterbackOf
        * aio:isTheMinisterOfEnvironmentForestAndClimateChangeOf
        * aio:isTheKingOf
        * aio:isAMemberOfTheBand
        * aio:isTheMonarchOf
        * aio:isStarringIn
        * aio:isTheMinisterForForeignPolicyOf
        * aio:isTheCfoOf
        * aio:isAFounderOf
        * aio:isAFanOf
        * aio:isTheWifeOf
        * aio:isAMemberOfTheSeniorManagementTeamOf
        * aio:isADaughterOf
        * aio:isSomeoneWhoMurdered
        * aio:isTheDeputyLeaderOfTheLiberalPartyOf
        * aio:isTheComposerOf
        * aio:isLivingIn
        * aio:isTheMinisterOfPowerOf
        * aio:isTheLeadGuitaristIn
        * aio:isBuriedIn
        * aio:gaveBirthToThePerson
        * aio:isTheMinisterOfRoadTransportAndHighwaysOf
        * aio:isTheFirstMinisterOf
        * aio:isTheLeaderOfTheGovernmentInTheSenateOf
        * aio:isThePresidentOfTheRegion
        * aio:isAChildOf
        * aio:isTheFatherOf
        * aio:directed
        * aio:isTheMinisterForInfrastructureAndTransportOf
        * aio:isTheGirlfriendOf
        * aio:isTheMinisterOfFinanceOf
        * aio:isAGrandparentOf
        * aio:isThePatronSaintOf
        * aio:isTheLeaderOfTheHouseOf
        * aio:isTheDeputyLeaderOfTheGovernmentInTheSenateOf
        * aio:isTheMinisterOfHomeAffairsOf
        * aio:isThePoliticalLeaderOf
        * aio:wasTheFirstPrimeMinisterOf
        * aio:isTheLeaderOfTheOppositionInLokSabhaIn
        * aio:isInARomanticRelationshipWith
        * aio:isTheDeputyLeaderOfTheNationalsPartyOf
        * aio:isTheAttorneyGeneralOf
        * aio:isASingerWith
        * aio:isAFullSisterOf
        * aio:isTheGovernorOf
        * aio:grewUpIn
        * aio:isTheChiefJusticeOf
        * aio:isTheLeaderOfTheNationalPartyInTheSenateOf
        * aio:isAGrandfatherOf

Used in the *range* of the following properties:
        * aio:isAChildOf
        * aio:isEngagedTo
        * aio:isTheLastNameOf
        * aio:isTheFatherOf
        * aio:isSomeoneWhoKilledThePerson
        * aio:isTheBoyfriendOf
        * aio:isTheGirlfriendOf
        * aio:isAGrandparentOf
        * aio:isDiscoveredBy
        * aio:isTheHusbandOf
        * aio:isTheMotherOf
        * aio:isThePlaceOfDeathOf
        * aio:isAMiddleNameOf
        * aio:isAQuoteBy
        * aio:isAGrandmotherOf
        * aio:isRelatedTo
        * aio:isTheFullOfficialNameOf
        * aio:isAParentOf
        * aio:admires
        * aio:isAFullSiblingOf
        * aio:isTheAuthorAsinFor
        * aio:isSomeoneWhoMarried
        * aio:isASonOf
        * aio:isMarriedTo
        * aio:isInARomanticRelationshipWith
        * aio:isADaughterOf
        * aio:isTheWifeOf
        * aio:isSomeoneWhoMurdered
        * aio:assassinated
        * aio:isAnAuntOf
        * aio:isAFullSisterOf
        * aio:isAnUncleOf
        * aio:isTheVicePresidentDuringThePresidencyOf
        * aio:isASiblingOf
        * aio:gaveBirthToThePerson
        * aio:isAGrandfatherOf
        * aio:isTheCountryOfNationalityOf
