
Direct super classes:
        * aio:AgreementMakingEntity

Direct sub classes:
        * aio:Ensemble

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isAMemberOf
