
Direct super classes:
        * aio:HumanBeing

Direct sub classes:
        * aio:FilmDirector

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
