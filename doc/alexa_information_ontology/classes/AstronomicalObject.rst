
Direct super classes:
        * aio:PhysicalObject

Direct sub classes:
        * aio:Star
        * aio:Moon
        * aio:IauPlanet

Used in the *domain* of the following properties:
        * aio:isWithinTheAstronomicalObject

Used in the *range* of the following properties:
        * aio:isTheAstronomicalDistanceTo
        * aio:isWithinTheAstronomicalObject
        * aio:isTheAverageSurfaceTemperatureOf
        * aio:isTheDistanceFromOurSunTo
