
Direct super classes:
        * aio:PopulatedPlace

Direct sub classes:
        * aio:USState
        * aio:Country

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isTheLeaderOfTheGovernmentInTheSenateOf
        * aio:isTheMinisterOfExternalAffairsOf
        * aio:isTheMinisterOfHealthAndFamilyWelfareOf
        * aio:isTheMinisterForInfrastructureAndTransportOf
        * aio:isTheMinisterOfEnvironmentForestAndClimateChangeOf
        * aio:isThePrimeMinisterOf
        * aio:isTheDeputyFirstMinisterOf
        * aio:isTheMinisterOfFinanceOf
        * aio:isTheLeaderOfTheHouseOf
        * aio:isTheMinisterForHealthOf
        * aio:isTheDeputyLeaderOfTheGovernmentInTheSenateOf
        * aio:isTheMinisterOfHomeAffairsOf
        * aio:isTheMinisterOfHumanResourceDevelopmentOf
        * aio:isTheDeputyLeaderOfTheHouseOf
        * aio:isTheDeputyChiefMinisterOf
        * aio:isTheMinisterOfEducationOf
        * aio:isTheMinisterOfDefenceOf
        * aio:isTheChiefOfTheArmyStaffOf
        * aio:isTheMinisterOfCorporateAffairsOf
        * aio:isTheDeputyLeaderOfTheNationalsPartyOf
        * aio:isTheDeputyLeaderOfTheLiberalPartyOf
        * aio:isTheMinisterOfEnergyOf
        * aio:isTheChancellorOf
        * aio:isTheChiefMinisterOf
        * aio:isTheMinisterOfPowerOf
        * aio:isTheChiefJusticeOf
        * aio:isTheCapitalOf
        * aio:isTheDeputyPrimeMinisterOf
        * aio:isTheMinisterOfRoadTransportAndHighwaysOf
        * aio:isTheLeaderOfTheNationalPartyInTheSenateOf
        * aio:isTheFirstMinisterOf
