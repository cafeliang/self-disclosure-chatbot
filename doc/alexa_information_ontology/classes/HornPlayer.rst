
Direct super classes:
        * aio:Instrumentalist

Direct sub classes:
        * aio:TrumpetPlayer
        * aio:Saxophonist

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
