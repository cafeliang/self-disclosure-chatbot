
Direct super classes:
        * aio:Quantity

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isTheAverageTemperatureOf
        * aio:isTheAverageSurfaceTemperatureOf

Used in the *range* of the following properties:
        * *None*
