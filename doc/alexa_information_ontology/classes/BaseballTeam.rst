
Direct super classes:
        * aio:SportsTeam

Direct sub classes:
        * aio:MajorLeagueBaseballTeam

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
