
Direct super classes:
        * aio:ArtsEntertainmentOrRecreationBusiness

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isTheRecordLabelThatProduced
        * aio:isTheRecordLabelOf

Used in the *range* of the following properties:
        * *None*
