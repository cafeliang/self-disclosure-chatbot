
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:HumanBeing
        * aio:Organisation
        * aio:AudioAct
        * aio:GroupWithMembers

Used in the *domain* of the following properties:
        * aio:represents
        * aio:joined
        * aio:shot
        * aio:isTheAuthorOf
        * aio:isSomeoneWhoContacted
        * aio:isACandidateInTheElection
        * aio:isSomeoneWhoReleased
        * aio:isAManufacturerOfAtLeastOne
        * aio:isAnAllyOf
        * aio:isTheFounderOfThePlace
        * aio:publishedTheFirstEditionOf
        * aio:isTheManufacturerOfEvery
        * aio:isAMemberOf

Used in the *range* of the following properties:
        * aio:isAFanOf
        * aio:isAnAllyOf
        * aio:isEmployedBy
        * aio:isABookBy
        * aio:isASingleBy
        * aio:isSomeoneWhoContacted
