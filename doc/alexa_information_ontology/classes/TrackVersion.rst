
Direct super classes:
        * aio:AudioPublicationVersion

Direct sub classes:
        * aio:SongVersion

Used in the *domain* of the following properties:
        * aio:isAReleaseTrackOn
        * aio:isATrackVersionOf

Used in the *range* of the following properties:
        * *None*
