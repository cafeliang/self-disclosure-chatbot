
Direct super classes:
        * aio:AudioAct

Direct sub classes:
        * aio:Musician
        * aio:Ensemble
        * aio:DiscJockey

Used in the *domain* of the following properties:
        * aio:isASingerOf
        * aio:isAEurovisionContestantRepresenting
        * aio:performed

Used in the *range* of the following properties:
        * aio:isAMusicPublicationBy
        * aio:playsKeyBoardWith
        * aio:isAGuitaristWith
        * aio:performsWith
        * aio:isTheBassPlayerOf
        * aio:isADrummerWith
        * aio:isASongPerformedBy
