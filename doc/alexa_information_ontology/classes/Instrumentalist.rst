
Direct super classes:
        * aio:Musician

Direct sub classes:
        * aio:WoodwindPlayer
        * aio:StringPlayer
        * aio:Percussionist
        * aio:Keyboardist
        * aio:HornPlayer
        * aio:BrassPlayer
        * aio:Accordionist

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
