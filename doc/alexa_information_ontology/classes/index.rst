aio:Legislature
  A governmental organisation which is a deliberative assembly with the power to make laws for a political entity such as a country or city.

  .. include:: classes/Legislature.rst

aio:UkCustomaryUnitOfMeasurement
  UK customary unit

  .. include:: classes/UkCustomaryUnitOfMeasurement.rst

aio:UsConstitutionalAmendment
  An amendment to the Consitution of the United States

  .. include:: classes/UsConstitutionalAmendment.rst

aio:PremiershipFootballer
  A soccer player who plays in the English Premier League.

  .. include:: classes/PremiershipFootballer.rst

aio:FilmGenre
  An AVM genre which is applicable to a film.

  .. include:: classes/FilmGenre.rst

aio:Lineman
  An American footballer who plays in the position of lineman.

  .. include:: classes/Lineman.rst

aio:SportsTeam
  An organisation which is a group of people formed for the purpose of playing a sport or sports.

  .. include:: classes/SportsTeam.rst

aio:ExtendedPlayMusicPublication
  A music publication which contains more tracks than a single, but is usually not qualified as an album or LP.

  .. include:: classes/ExtendedPlayMusicPublication.rst

aio:Politician
  A human being who is active in party politics, or who holds or seeks office in government.

  .. include:: classes/Politician.rst

aio:Footballer
  An athlete who competes in football.

  .. include:: classes/Footballer.rst

aio:Composer
  A musician who composes music.

  .. include:: classes/Composer.rst

aio:Ensemble
  A group of people who perform music together, typically under a distinct name, such as Led Zeppelin or the London Symphony Orchestra.

  .. include:: classes/Ensemble.rst

aio:DefensiveBack
  An American footballer who plays in the position of defensive back

  .. include:: classes/DefensiveBack.rst

aio:UsRepresentative
  A US Congressperson who is a Member of the United States House of Represenatives.

  .. include:: classes/UsRepresentative.rst

aio:AirForce
  An armed force which fights primarily in the air.

  .. include:: classes/AirForce.rst

aio:Legislator
  A politician who writes and passes laws, usually as a member of a legislature.

  .. include:: classes/Legislator.rst

aio:SoccerPlayer
  An athlete who competes in soccer (association football).

  .. include:: classes/SoccerPlayer.rst

aio:SportsGoverningBody
  An organisation with a regulatory or sanctioning function for a sport or sports.

  .. include:: classes/SportsGoverningBody.rst

aio:FilmProducer
  A person who oversees production on a film.

  .. include:: classes/FilmProducer.rst

aio:SingleTrackMusicPublication
  A music publication featuring a single track.

  .. include:: classes/SingleTrackMusicPublication.rst

aio:Pressure
  pressure

  .. include:: classes/Pressure.rst

aio:CauseOfDeath
  An object which can cause death. This class includes events and medical conditions.

  .. include:: classes/CauseOfDeath.rst

aio:Business
  An organisation which produces or buys goods or services, and sells them for profit.

  .. include:: classes/Business.rst

aio:MusicPublication
  An audio publication featueing music.

  .. include:: classes/MusicPublication.rst

aio:King
  A monarch who is male.

  .. include:: classes/King.rst

aio:UnitOfVolumeDistanceRatio
  unit of volume-distance ratio

  .. include:: classes/UnitOfVolumeDistanceRatio.rst

aio:UnitOfDensity
  unit of density

  .. include:: classes/UnitOfDensity.rst

aio:PixelCount
  pixel count

  .. include:: classes/PixelCount.rst

aio:Quantity
  quantity

  .. include:: classes/Quantity.rst

aio:ObjectWithAMass
  An object where it makes sense to talk about its physical mass. Instances include raindrops on roses and whiskers on kittens, but not silver-white winters that melt into springs.

  .. include:: classes/ObjectWithAMass.rst

aio:SoccerTeam
  A sports team which plays soccer (association football)

  .. include:: classes/SoccerTeam.rst

aio:Quarterback
  An American footballer who plays in the position of quarterback.

  .. include:: classes/Quarterback.rst

aio:SystemOfGovernment
  A system of government, such as republic or monarchy.

  .. include:: classes/SystemOfGovernment.rst

aio:Kickboxer
  An athlete who competes in kickboxing

  .. include:: classes/Kickboxer.rst

aio:NationalSoccerTeam
  A soccer team which represents a nation in international competition.

  .. include:: classes/NationalSoccerTeam.rst

aio:TradeUnionist
  A human being who is active in a trade union.

  .. include:: classes/TradeUnionist.rst

aio:UnitOfSurfaceDensity
  unit of speed

  .. include:: classes/UnitOfSurfaceDensity.rst

aio:FullTimeSprintCupNascarDriver
  A Nascar driver who competes in full time sprint cup events

  .. include:: classes/FullTimeSprintCupNascarDriver.rst

aio:UsCongressperson
  A politician who is a Congressperson in the United States. This class includes both Senators and Representatives.

  .. include:: classes/UsCongressperson.rst

aio:UsCustomaryUnitOfMeasurement
  US customary unit

  .. include:: classes/UsCustomaryUnitOfMeasurement.rst

aio:DefensiveTackle
  An American footballer who plays in the position of defensive tackle

  .. include:: classes/DefensiveTackle.rst

aio:UnitOfPower
  unit of power

  .. include:: classes/UnitOfPower.rst

aio:MetricUnitOfMeasurement
  metric unit

  .. include:: classes/MetricUnitOfMeasurement.rst

aio:Disease
  disease

  .. include:: classes/Disease.rst

aio:Monarchy
  A sovereign state in which a legal person, the monarch, holds sovereign authority.

  .. include:: classes/Monarchy.rst

aio:PoliticalParty
  A political organisation which consists of people with shared political views and which fields candidates for political elections.

  .. include:: classes/PoliticalParty.rst

aio:PointGuard
  A basketball guard who playes in the position of point guard

  .. include:: classes/PointGuard.rst

aio:SportsVenue
  A geographical area at which sport is played. Examples include running tracks or swimming pools.

  .. include:: classes/SportsVenue.rst

aio:BeachVolleyballPlayer
  An athlete who competes in beach volleyball.

  .. include:: classes/BeachVolleyballPlayer.rst

aio:Defenceman
  An ice hockey player who plays in the position of defenceman

  .. include:: classes/Defenceman.rst

aio:Model
  A human being who poses or performs to promote, display or advertise commercial products.

  .. include:: classes/Model.rst

aio:BaseballVenue
  A sports venue at which baseball is played.

  .. include:: classes/BaseballVenue.rst

aio:Substance
  Something which is a physical stuff, of which one may have a varying amount. Substances are often referred to by concrete mass nouns, unlike the individuated entities which count nouns or proper nouns can refer to. For example, gold and water are instances of substance. Sports team and Donald Trump are not instances of substance.

  .. include:: classes/Substance.rst

aio:AmericanFootballEnd
  An American footballer who plays in the position of end.

  .. include:: classes/AmericanFootballEnd.rst

aio:Movie
  An animated visual medium which is a sequence of moving images for showing in a cinema or on television. Also known as film or motion picture.

  .. include:: classes/Movie.rst

aio:Gas
  A substance which is a compressible fluid, where atoms are able to move freely in relation to each other.

  .. include:: classes/Gas.rst

aio:HockeyPosition
  A sports position pertaining to the sport of hockey.

  .. include:: classes/HockeyPosition.rst

aio:AstronomicalObject
  A physical object identified by astronomers as existing in the observable universe. Also known as a celestial object. Our planet is an astronomical object, but not its surface features like rivers or buildings.

  .. include:: classes/AstronomicalObject.rst

aio:TrackAthlete
  An athlete who competes in track sports.

  .. include:: classes/TrackAthlete.rst

aio:Force
  force

  .. include:: classes/Force.rst

aio:OlympicGames
  An international sporting event featuring a variety of competitions in winter or summer sports.

  .. include:: classes/OlympicGames.rst

aio:ChemicalCompound
  A substance which is composed of identical molecules consisting of atoms of two or more chemical elements

  .. include:: classes/ChemicalCompound.rst

aio:BaseballPlayer
  An athlete who competes in baseball.

  .. include:: classes/BaseballPlayer.rst

aio:PublishedWorkOrVersion
  A published work, or version of a published work.

  .. include:: classes/PublishedWorkOrVersion.rst

aio:DateTime
  A point on the UTC timeline. Examples of DateTimes include: the year 1999; the day May 13th 1999; the moment of time at 153 milliseconds after 08:51:23 on May 13th 1999. DateTimes may be expressed to varying degrees of precision.

  .. include:: classes/DateTime.rst

aio:IceHockeyTeam
  A sports team which plays ice hockey.

  .. include:: classes/IceHockeyTeam.rst

aio:Communist
  A human being who supports communism, a political philosophy advocating the communal ownership of property.

  .. include:: classes/Communist.rst

aio:WorldWar
  A war which is sufficiently geographically extended to involve or affect the entire globe

  .. include:: classes/WorldWar.rst

aio:TelevisionThemeAlbum
  A album comprising tracks which are television theme tunes.

  .. include:: classes/TelevisionThemeAlbum.rst

aio:JazzViolinist
  A violinist whose primary genre is jazz.

  .. include:: classes/JazzViolinist.rst

aio:BiologicalFamily
  https://en.wikipedia.org/wiki/Family_(biology)

  .. include:: classes/BiologicalFamily.rst

aio:RecordProducer
  A human being who oversees the sound recording or production of recorded music.

  .. include:: classes/RecordProducer.rst

aio:TrackVersion
  A version of a track that appears on a version of an album.

  .. include:: classes/TrackVersion.rst

aio:Goalkeeper
  An athlete who plays in the position of goalkeeper.

  .. include:: classes/Goalkeeper.rst

aio:Class
  Not documented

  .. include:: classes/Class.rst

aio:UnitOfBrightness
  unit of brightness

  .. include:: classes/UnitOfBrightness.rst

aio:Outfielder
  A baseball player who plays in an outfielder position.

  .. include:: classes/Outfielder.rst

aio:UkParliamentaryConstituency
  A constituency within the United Kingdom which elects a single Member of Parliament to the UK Parliament.

  .. include:: classes/UkParliamentaryConstituency.rst

aio:SongVersion
  A track version which is also a song version.

  .. include:: classes/SongVersion.rst

aio:FashionDesigner
  A human being who designs fashion garmets.

  .. include:: classes/FashionDesigner.rst

aio:ProfessionalWrestler
  A wrestler who takes part in wrestling professionally.

  .. include:: classes/ProfessionalWrestler.rst

aio:Comedian
  A human being who seeks to entertain an audience by making them laugh.

  .. include:: classes/Comedian.rst

aio:MilitaryCombatant
  One side in a war or battle.

  .. include:: classes/MilitaryCombatant.rst

aio:UnitOfEnergyPerUnitOfTime
  unit of energy per unit of time

  .. include:: classes/UnitOfEnergyPerUnitOfTime.rst

aio:ObjectWithAHeight
  An object which has a height

  .. include:: classes/ObjectWithAHeight.rst

aio:Density
  density

  .. include:: classes/Density.rst

aio:GroupWithMembers
  A group of human beings, or group whose members comprise human beings, with a non-arbitrary identity. Examples include: political parties, businesses and similar organisations, The United Nations, sports teams, and music groups.

  .. include:: classes/GroupWithMembers.rst

aio:RaceHorse
  An animal which is a horse competing in horse-racing.

  .. include:: classes/RaceHorse.rst

aio:Guitarist
  A musician who plays a guitar instrument.

  .. include:: classes/Guitarist.rst

aio:UsPresidentialElection
  An election in which citizens of the United States elect their President.

  .. include:: classes/UsPresidentialElection.rst

aio:AmericanFootballTeam
  A sports team which plays American football.

  .. include:: classes/AmericanFootballTeam.rst

aio:SingerSongwriter
  A musician who composes and performs their own music, typically with vocals.

  .. include:: classes/SingerSongwriter.rst

aio:Star
  An astronomical object consisting of a luminous spherois of plasma held together by its own gravity.

  .. include:: classes/Star.rst

aio:Rapper
  A vocalist who raps.

  .. include:: classes/Rapper.rst

aio:WoodwindPlayer
  A musician who plays a woodwind instrument.

  .. include:: classes/WoodwindPlayer.rst

aio:CreativeWork
  Something which can be entirely represented as a piece of text, but is not necessarily intended to be written or read.

  .. include:: classes/CreativeWork.rst

aio:SecondBaseman
  A baseball player who plays in the position of second baseman

  .. include:: classes/SecondBaseman.rst

aio:UnitOfPressure
  unit of pressure

  .. include:: classes/UnitOfPressure.rst

aio:TeamSport
  A sport in which teams, rather than individuals, participate to compete with each other.

  .. include:: classes/TeamSport.rst

aio:SportingEvent
  An event which is centred around sport, such as a championship or a sports draft.

  .. include:: classes/SportingEvent.rst

aio:WrittenPublication
  A written work which is published any form, such as books, magazines, ebooks.

  .. include:: classes/WrittenPublication.rst

aio:BiologicalGenus
  https://en.wikipedia.org/wiki/Genus

  .. include:: classes/BiologicalGenus.rst

aio:SoccerManager
  A sports team manager who manages a soccer team.

  .. include:: classes/SoccerManager.rst

aio:State
  A geographical area with its own territory, government and people. This may or may not lie within the authority of a larger state or federal union. Examples would include the country France and the US state Alabama.

  .. include:: classes/State.rst

aio:UnitOfMolecularWeight
  unit of molecular weight

  .. include:: classes/UnitOfMolecularWeight.rst

aio:Constitution
  A collection of principles or precedents which constitute the legal basis of an organisation or polity, and constrain how that entity is to be governed.

  .. include:: classes/Constitution.rst

aio:Cornerback
  An American footballer who plays in the position of cornerback.

  .. include:: classes/Cornerback.rst

aio:Academic
  A person who is employed in the university sector, such as a lecturer, researcher or professor.

  .. include:: classes/Academic.rst

aio:CricketPosition
  A sports position pertaining to the sport of cricket.

  .. include:: classes/CricketPosition.rst

aio:UnitOfElectricalResistance
  unit of electrical resistance

  .. include:: classes/UnitOfElectricalResistance.rst

aio:TerroristAct
  An event which is an act of terrorism.

  .. include:: classes/TerroristAct.rst

aio:Government
  An organisation which is a system or group of people governing an organized community such as a state.

  .. include:: classes/Government.rst

aio:CollegeSportsCoach
  A sports coach who coaches college sports.

  .. include:: classes/CollegeSportsCoach.rst

aio:Stadium
  A geographical area which is the venue for outdoor sports, concerts or other events. Often a field or stage is partly or completely surrounded by a tiered built structure designed to allow spectators to view events.

  .. include:: classes/Stadium.rst

aio:PoliticalOrganisation
  An organisation concerned with political activity.

  .. include:: classes/PoliticalOrganisation.rst

aio:Palace
  An official residence which is a grand residence, often that of a member of royalty or some similarly non-elected high-status individual.

  .. include:: classes/Palace.rst

aio:Mass
  mass

  .. include:: classes/Mass.rst

aio:Country
  A geographical area of significant size which is a current or former state, and still internationally recognized as a political or cultural entity.

  .. include:: classes/Country.rst

aio:UnitOfDigitalImaging
  unit of digital imaging

  .. include:: classes/UnitOfDigitalImaging.rst

aio:Power
  power

  .. include:: classes/Power.rst

aio:VolumeDistanceRatio
  volume-distance ratio

  .. include:: classes/VolumeDistanceRatio.rst

aio:Album
  A collection of audio recordings issued as a collection on compact disc, vinyl, audio tape, or other medium.

  .. include:: classes/Album.rst

aio:SovereignState
  A country with a centralized government having sovereignty over the governed geographical area.

  .. include:: classes/SovereignState.rst

aio:Hardness
  scratch hardness

  .. include:: classes/Hardness.rst

aio:SoccerPosition
  A sports position pertaining to the sport of soccer (association football).

  .. include:: classes/SoccerPosition.rst

aio:Monarch
  A member of royalty who is a hereditary sovereign, such as a king, queen or emperor.

  .. include:: classes/Monarch.rst

aio:GeographicalAreaOfSignificantSize
  A geographical area which is of significant size, such as a county, state, country or continent. Small villages, streets or buildings would not be in this class.

  .. include:: classes/GeographicalAreaOfSignificantSize.rst

aio:UnitOfIllumination
  unit of illumination

  .. include:: classes/UnitOfIllumination.rst

aio:SoundtrackToAMusical
  An album containing the soundtrack to a musical.

  .. include:: classes/SoundtrackToAMusical.rst

aio:TvPersonality
  A person who is well known for their television appearances.

  .. include:: classes/TvPersonality.rst

aio:Pitcher
  A baseball player who plays in the position of pitcher

  .. include:: classes/Pitcher.rst

aio:Referendum
  An event in which an entire electorate votes directly on a particular proposal.

  .. include:: classes/Referendum.rst

aio:MedicalCondition
  medical condition

  .. include:: classes/MedicalCondition.rst

aio:HeadOfGovernment
  A politician who leads the executive branch of a state, often presiding over a cabinet.

  .. include:: classes/HeadOfGovernment.rst

aio:Ambassador
  A diplomat who has the rank of ambassador, the highest rank of diplomat accredited to a foreign government or international organisation.

  .. include:: classes/Ambassador.rst

aio:Singer
  A vocalist who sings.

  .. include:: classes/Singer.rst

aio:TennisPlayer
  An athlete who competes in lawn tennis.

  .. include:: classes/TennisPlayer.rst

aio:BasketballPlayer
  An athlete who competes in basketball.

  .. include:: classes/BasketballPlayer.rst

aio:Illumination
  illuminance

  .. include:: classes/Illumination.rst

aio:GovernmentAgency
  A governmental organisation which is responsible for the oversight or administration of specific functions of government, e.g. a government ministry or intelligence agency.

  .. include:: classes/GovernmentAgency.rst

aio:BrassPlayer
  A musician who plays a brass instrument.

  .. include:: classes/BrassPlayer.rst

aio:Guard
  A basketball player who plays in a guard position

  .. include:: classes/Guard.rst

aio:Feminist
  A human being who supports feminism.

  .. include:: classes/Feminist.rst

aio:SportsTeamLocation
  A geographical area which is represented by a sports team location

  .. include:: classes/SportsTeamLocation.rst

aio:Halfback
  An American footballer who plays in the position of halfback.

  .. include:: classes/Halfback.rst

aio:CalorificValuePerWeightOrVolume
  calorific value per weight or volume

  .. include:: classes/CalorificValuePerWeightOrVolume.rst

aio:IndoorIceHockeyVenue
  A sports venue at which indoor ice hockey is played.

  .. include:: classes/IndoorIceHockeyVenue.rst

aio:UnitOfMoney
  unit of money

  .. include:: classes/UnitOfMoney.rst

aio:WrittenLanguage
  The representation of a spoken or gestural language by means of a writing system. Distinguished from :SpokenFormOfALanguage.

  .. include:: classes/WrittenLanguage.rst

aio:Cricketer
  An athlete who competes in cricket.

  .. include:: classes/Cricketer.rst

aio:UnitOfConcentration
  unit of concentration

  .. include:: classes/UnitOfConcentration.rst

aio:UsPresidentialCandidate
  A politician who attempts to be elected President of the United States

  .. include:: classes/UsPresidentialCandidate.rst

aio:Organisation
  Not documented

  .. include:: classes/Organisation.rst

aio:ImperialUnitOfMeasurement
  imperial unit

  .. include:: classes/ImperialUnitOfMeasurement.rst

aio:HornPlayer
  An instrumentalist musician who plays a horn. Examples of horn players include saxophonists, trumpeters, and trombonists.

  .. include:: classes/HornPlayer.rst

aio:ShootingGuard
  A basketball guard who playes in the position of shooting guard

  .. include:: classes/ShootingGuard.rst

aio:Anarchist
  A human being who supports anarchism, a political philosophy opposing government and advocating the organization of society on a voluntary, cooperative basis without recourse to force or compulsion.

  .. include:: classes/Anarchist.rst

aio:BaseballManager
  A sports team manager who manages a baseball team.

  .. include:: classes/BaseballManager.rst

aio:Flautist
  A musician who plays the flute.

  .. include:: classes/Flautist.rst

aio:CurrencyZone
  A geographical area which consists of all places using the same local currency 

  .. include:: classes/CurrencyZone.rst

aio:TenorSinger
  A singer who sings with tenor range.

  .. include:: classes/TenorSinger.rst

aio:WideReceiver
  An American footballer who plays in the position of wide receiver.

  .. include:: classes/WideReceiver.rst

aio:LuminousIntensity
  luminous intensity

  .. include:: classes/LuminousIntensity.rst

aio:Engineer
  A human being who whose job is to design or build machines, engines, or electrical equipment, or things such as roads, railways, or bridges, using scientific principles.

  .. include:: classes/Engineer.rst

aio:CollectionOfBooks
  A set of distinct books which have something in common.

  .. include:: classes/CollectionOfBooks.rst

aio:MilitaryRank
  An occupational role within the hierarchical organisation of armed forces or other organisations organised along military lines, such as some intelligence or police agencies.

  .. include:: classes/MilitaryRank.rst

aio:UkMemberOfParliament
  A legislator who is a Member of Parliament in the United Kingdom.

  .. include:: classes/UkMemberOfParliament.rst

aio:TrumpetPlayer
  A musician who plays the trumpet.

  .. include:: classes/TrumpetPlayer.rst

aio:Conservationist
  A scientist who who works in the field of conservation biology.

  .. include:: classes/Conservationist.rst

aio:Actor
  A human being who portrays a character in a film or theatrical performance.

  .. include:: classes/Actor.rst

aio:NationState
  A sovereign state which is recognized by the United Nations.

  .. include:: classes/NationState.rst

aio:DiscJockey
  A musical act who plays or remixes existing recorded music for a live audience. Disc jockeys are also often referred to by the abbreviation DJ.

  .. include:: classes/DiscJockey.rst

aio:Writer
  A person who uses written words to communicate their ideas.

  .. include:: classes/Writer.rst

aio:RecordLabel
  A business which markets, produces, or sells, music publications.

  .. include:: classes/RecordLabel.rst

aio:DataCapacity
  data capacity

  .. include:: classes/DataCapacity.rst

aio:Keyboardist
  An instrumentalist musician who plays a keyboard instrument.

  .. include:: classes/Keyboardist.rst

aio:RugbyPlayer
  An athlete who competes in rugby

  .. include:: classes/RugbyPlayer.rst

aio:MusicalAct
  An audio act which performs music.

  .. include:: classes/MusicalAct.rst

aio:AgreementMakingEntity
  Any entity which is capable of entering an agreement, broadly understood. Examples include: businesses and similar organisations, nation states, and individual human beings.

  .. include:: classes/AgreementMakingEntity.rst

aio:SoccerGoalkeeper
  A soccer player who plays in the position of goalkeeper

  .. include:: classes/SoccerGoalkeeper.rst

aio:Center
  A basketball player who plays in the position of center

  .. include:: classes/Center.rst

aio:OccupationalRole
  A defined position or role within an organisation or company.

  .. include:: classes/OccupationalRole.rst

aio:OfficialResidence
  A geographical area which is the prescribed residence at which a senior official within an organization, such as a head of state or religious leader, resides. 

  .. include:: classes/OfficialResidence.rst

aio:Moon
  An astronomical object that orbits a planet or minor planet.

  .. include:: classes/Moon.rst

aio:SoccerDefender
  A soccer player who plays in the position of defender

  .. include:: classes/SoccerDefender.rst

aio:Sport
  An active diversion requiring physical exertion and often competition.

  .. include:: classes/Sport.rst

aio:LengthOfTime
  A quantity of time not anchored to a particular timeperiod, such as three hours or two minutes and twenty-five seconds. 12:00-13:00 and 13:00-14:00 both have the same length of time.

  .. include:: classes/LengthOfTime.rst

aio:NascarDriver
  An athlete who competes in Nascar racing.

  .. include:: classes/NascarDriver.rst

aio:LiveAlbum
  An album comprising recordings of live performances.

  .. include:: classes/LiveAlbum.rst

aio:DefensiveLineman
  A lineman who playes in the position of defensive lineman.

  .. include:: classes/DefensiveLineman.rst

aio:EntityCapableOfDeliberateAction
  An entity that is capable of deliberate action, such as a person, animal, company, or intelligent robot.

  .. include:: classes/EntityCapableOfDeliberateAction.rst

aio:LeadGuitarPlayer
  A guitarist with an ensemble who, typically, plays the lead melodies and guitar solos.

  .. include:: classes/LeadGuitarPlayer.rst

aio:Athlete
  A buman being who is proficient in competitive sport.

  .. include:: classes/Athlete.rst

aio:CoverAlbum
  An album comprising cover versions of tracks.

  .. include:: classes/CoverAlbum.rst

aio:AmericanFootballGuard
  An American footballer who plays in the position of guard.

  .. include:: classes/AmericanFootballGuard.rst

aio:UnitOfAngle
  unit of angle

  .. include:: classes/UnitOfAngle.rst

aio:UnitOfMassPerWeightOrVolume
  unit of mass per weight or volume

  .. include:: classes/UnitOfMassPerWeightOrVolume.rst

aio:VicePresident
  A politician who holds the role of vice president, an office below that of president in rank.

  .. include:: classes/VicePresident.rst

aio:UnitOfVoltage
  unit of voltage

  .. include:: classes/UnitOfVoltage.rst

aio:GovernmentalOrganisation
  An organisation which operates within or on behalf of the government of a single country.

  .. include:: classes/GovernmentalOrganisation.rst

aio:Brightness
  brightness

  .. include:: classes/Brightness.rst

aio:Treaty
  A formal written agreement entered into by sovereign states and intergovernmental organisations.

  .. include:: classes/Treaty.rst

aio:Terrorist
  A human being who practices the systematic use of terror as a means of political coercion or persuasion.

  .. include:: classes/Terrorist.rst

aio:Animal
  An organism which is multicellular and Eukaryotic. Part of the Animal Kingdom. Instances of this class are individual instances of animal, like Larry the Downing Street cat. (Not the same as :TypeOfAnimal.)

  .. include:: classes/Animal.rst

aio:UnitOfTemperature
  unit of temperature

  .. include:: classes/UnitOfTemperature.rst

aio:IceHockeyPlayer
  An athlete who competes in ice hockey.

  .. include:: classes/IceHockeyPlayer.rst

aio:Boxer
  An athlete who competes in boxing.

  .. include:: classes/Boxer.rst

aio:TradeUnion
  A membership organisation of employees which bargains or lobbies for better terms of employment.

  .. include:: classes/TradeUnion.rst

aio:UsSenator
  A US Congressperson who is a Member of the United States Senate.

  .. include:: classes/UsSenator.rst

aio:BiologicalSpecies
  https://en.wikipedia.org/wiki/Species

  .. include:: classes/BiologicalSpecies.rst

aio:CommunistCountry
  A sovereign state which is communist, having a socioeconomic structure based on the common ownership of means of production.

  .. include:: classes/CommunistCountry.rst

aio:SportsTeamManager
  A human being who manages a sports team.

  .. include:: classes/SportsTeamManager.rst

aio:UsRepublican
  A human being who supports the Republican Party, one of the two main political parties in the United States.

  .. include:: classes/UsRepublican.rst

aio:FirstLadyOfTheUnitedStates
  A human being who holds the title of hostess of the White House, usually the wife of the United States president, while the president holds term in office.

  .. include:: classes/FirstLadyOfTheUnitedStates.rst

aio:Director
  A human being who is director of a creative endeavour (such as a play or movie)

  .. include:: classes/Director.rst

aio:Celebrity
  A human being who achieves a status of fame through mass media attention.

  .. include:: classes/Celebrity.rst

aio:FirstBaseman
  A baseball player who plays in the position of first baseman

  .. include:: classes/FirstBaseman.rst

aio:CenterFielder
  An outfielder who plays in the position of center fielder.

  .. include:: classes/CenterFielder.rst

aio:MusicGroupMember
  A member of a band, pop group, or other music ensemble.

  .. include:: classes/MusicGroupMember.rst

aio:LeftFielder
  An outfielder who plays in the position of left fielder

  .. include:: classes/LeftFielder.rst

aio:AudioPublicationVersion
  A version of an audio publication.

  .. include:: classes/AudioPublicationVersion.rst

aio:Journalist
  A writer who writes for newspapers or magazines.

  .. include:: classes/Journalist.rst

aio:SportingGoodsManufacturer
  A business which manufactures sporting goods.

  .. include:: classes/SportingGoodsManufacturer.rst

aio:DemocraticMonarchy
  A monarchy in which the sovereign exercises authority in accordance with a written or unwritten democratic constitution.

  .. include:: classes/DemocraticMonarchy.rst

aio:SoundtrackAlbum
  An album comprising tracks featured in an animated visual medium.

  .. include:: classes/SoundtrackAlbum.rst

aio:Soldier
  A human being who fights as part of an army.

  .. include:: classes/Soldier.rst

aio:DefensiveEnd
  An American football end who plays in the position of defensive end

  .. include:: classes/DefensiveEnd.rst

aio:StudioAlbum
  An album comprising tracks recorded in a studio.

  .. include:: classes/StudioAlbum.rst

aio:PublishedWrittenWork
  A published work in a written medium of which there may be differing versions.

  .. include:: classes/PublishedWrittenWork.rst

aio:SmallForward
  A basketball forward who plays in the position of small forward

  .. include:: classes/SmallForward.rst

aio:ThingWithACreationDate
  Something which came into being at some time, like a country, organisation, or person, rather than something which has always existed, such as a mathematical object.

  .. include:: classes/ThingWithACreationDate.rst

aio:MusicArranger
  A musician who arranges music.

  .. include:: classes/MusicArranger.rst

aio:Election
  An event which is a formal decision-making process where a population chooses an individual to hold office.

  .. include:: classes/Election.rst

aio:BaseballShortstop
  A baseball player who plays in the position of shortstop

  .. include:: classes/BaseballShortstop.rst

aio:Socialist
  A human being who supports socialism, a range of social and political systems characterized by social ownership of the means of production or workplace democracy.

  .. include:: classes/Socialist.rst

aio:WorldHeritageSite
  world heritage site

  .. include:: classes/WorldHeritageSite.rst

aio:UnitOfTime
  unit of time

  .. include:: classes/UnitOfTime.rst

aio:SpokenFormOfALanguage
  A language produced by articulated sounds. Distinguished from :WrittenLanguage.

  .. include:: classes/SpokenFormOfALanguage.rst

aio:War
  A military event which is an ongoing armed conflict between states, governments, societies and informal paramilitary groups.

  .. include:: classes/War.rst

aio:SportsPosition
  A standardized place of an individual player in the arrangement of play in a team sport. Examples would be pitcher in baseball or quarterback in American and Canadian football.

  .. include:: classes/SportsPosition.rst

aio:TvSeries
  An episodic TV broadcast structured as a connected set of program episodes running under the same title, possibly spanning many seasons.

  .. include:: classes/TvSeries.rst

aio:NbaPlayer
  A basketball player who plays in the NBA

  .. include:: classes/NbaPlayer.rst

aio:Percussionist
  A musician who plays a percussion instrument.

  .. include:: classes/Percussionist.rst

aio:Liquid
  A substance which is a non-compressible fluid, where molecules can move relative to each other but maintain an approximately constant volume.

  .. include:: classes/Liquid.rst

aio:SoccerMidfielder
  A soccer player who plays in the position of midfielder

  .. include:: classes/SoccerMidfielder.rst

aio:Swimmer
  An athlete who competes in swimming.

  .. include:: classes/Swimmer.rst

aio:PieceOfSoftware
  A creative work which is a collection of instructions controlling the behavior of a computer.

  .. include:: classes/PieceOfSoftware.rst

aio:TerroristOrganisation
  A political organisation which consists of people who use terror as a strategy of achieving their political goals.

  .. include:: classes/TerroristOrganisation.rst

aio:ArmedForce
  A governmental organisation which provides and can exert military power on behalf of a state.

  .. include:: classes/ArmedForce.rst

aio:UnitOfPungency
  unit of pungency

  .. include:: classes/UnitOfPungency.rst

aio:FashionModel
  A model who poses or performs to display fashion clothing.

  .. include:: classes/FashionModel.rst

aio:Instrumentalist
  A musician who plays a musical instrument.

  .. include:: classes/Instrumentalist.rst

aio:AdministrativeAreaWithinANation
  An area within a nation which is used as an administrative area.

  .. include:: classes/AdministrativeAreaWithinANation.rst

aio:MemberOfRoyalty
  A human being who is recognized as part of the family of a heritary monarch, by being in their immediate family or by signs of status which reflect belonging to their extended family.

  .. include:: classes/MemberOfRoyalty.rst

aio:SopranoSinger
  A singer who sings with a soprano range.

  .. include:: classes/SopranoSinger.rst

aio:AmericanFootballer
  An athlete who competes in American football.

  .. include:: classes/AmericanFootballer.rst

aio:FigureSkater
  An athlete who particulates in competitive figure skating

  .. include:: classes/FigureSkater.rst

aio:JazzGuitarist
  A guitarist whose primary genre is jazz.

  .. include:: classes/JazzGuitarist.rst

aio:BookAuthor
  A writer who authors books.

  .. include:: classes/BookAuthor.rst

aio:Area
  area

  .. include:: classes/Area.rst

aio:ContestAwardHonorOrPrize
  A general class for something that is winnable, such as a contest, award, honor or prize.

  .. include:: classes/ContestAwardHonorOrPrize.rst

aio:Pacifist
  A human being who opposes war, militarism or violence.

  .. include:: classes/Pacifist.rst

aio:UnitOfCalorificValuePerWeightOrVolume
  unit of calorific value per weight or volume

  .. include:: classes/UnitOfCalorificValuePerWeightOrVolume.rst

aio:Event
  Event, something that takes place at a given moment or period of time.

  .. include:: classes/Event.rst

aio:EntityThatIsCapableOfDeliberateAction
  Not documented

  .. include:: classes/EntityThatIsCapableOfDeliberateAction.rst

aio:Concentration
  concentration

  .. include:: classes/Concentration.rst

aio:AmericanFootballPosition
  A sports position pertaining to the sport of American football.

  .. include:: classes/AmericanFootballPosition.rst

aio:UnitOfArea
  unit of area

  .. include:: classes/UnitOfArea.rst

aio:DemocraticState
  A sovereign state whose rulers are selected by the people.

  .. include:: classes/DemocraticState.rst

aio:UnitOfTorque
  unit of speed

  .. include:: classes/UnitOfTorque.rst

aio:OlympicAthlete
  An athlete who competes in the Olympic Games.

  .. include:: classes/OlympicAthlete.rst

aio:HockeyPlayer
  An athlete who competes in hockey

  .. include:: classes/HockeyPlayer.rst

aio:String
  A sequence of characters, as per usage in computer science.

  .. include:: classes/String.rst

aio:AudioPublication
  An audio work which is an album, an EP, or a single.

  .. include:: classes/AudioPublication.rst

aio:ProfessionalSportsTeam
  A sports team which competes professionally.

  .. include:: classes/ProfessionalSportsTeam.rst

aio:AnimatedVisualMedium
  A creative visual work which is recorded for watching, such as a film or television series.

  .. include:: classes/AnimatedVisualMedium.rst

aio:PoloPlayer
  An athlete who competes in polo.

  .. include:: classes/PoloPlayer.rst

aio:UnitOfEnergy
  unit of energy

  .. include:: classes/UnitOfEnergy.rst

aio:Class
  A meaningful set containing individual entities as instances. Equivalent to rdfs:Class, and roughly equivalent to owl:Class

  .. include:: classes/Class.rst

aio:Energy
  energy

  .. include:: classes/Energy.rst

aio:Saxophonist
  A musician who plays the saxophone.

  .. include:: classes/Saxophonist.rst

aio:UnitOfMass
  unit of mass

  .. include:: classes/UnitOfMass.rst

aio:EnergyPerUnitOfTime
  energy per unit of time

  .. include:: classes/EnergyPerUnitOfTime.rst

aio:Mayor
  A politician who is mayor of a city.

  .. include:: classes/Mayor.rst

aio:MakeupArtist
  A human being who works applying makeup and prosthetics on others for performance or publications. 

  .. include:: classes/MakeupArtist.rst

aio:TypeOfAnimal
  A thing which is a type of animal. For example, dog is a type of animal; cat is a type of animal. (Not the same as :Animal.)

  .. include:: classes/TypeOfAnimal.rst

aio:UnitOfSpeed
  unit of speed

  .. include:: classes/UnitOfSpeed.rst

aio:Pianist
  A musician who plays the piano.

  .. include:: classes/Pianist.rst

aio:LegislativeChamber
  A governmental organisation which is a chamber within a legislative body. Many parliaments or other legislatures consist of deliberative assembly in two legislative chambers: an elected lower house and an upper house which may be appointed or elected by a different mechanism from the lower house.

  .. include:: classes/LegislativeChamber.rst

aio:UsGovernmentAgency
  A government agency within the United States.

  .. include:: classes/UsGovernmentAgency.rst

aio:Linebacker
  An American footballer who plays in the position of linebacker.

  .. include:: classes/Linebacker.rst

aio:CricketBatsman
  A cricketer who who is known as a batsman

  .. include:: classes/CricketBatsman.rst

aio:ArtsEntertainmentOrRecreationBusiness
  A business in the arts, entertainment, or recreation sector.

  .. include:: classes/ArtsEntertainmentOrRecreationBusiness.rst

aio:UnitOfLuminousIntensity
  unit of luminous intensity

  .. include:: classes/UnitOfLuminousIntensity.rst

aio:Violist
  A musician who plays the viola.

  .. include:: classes/Violist.rst

aio:PublishedWork
  A published piece of creative work, of which there may be different versions.

  .. include:: classes/PublishedWork.rst

aio:MilitaryBattle
  A military event which is a combat in war between two or more armed forces. Miltary battles can often be undestood as episodes within larger wars.

  .. include:: classes/MilitaryBattle.rst

aio:WinterOlympicGames
  A Olympic Games held in the winter. For example, the 2018 Winter Olympics is an instance of Winter Olympic Games.

  .. include:: classes/WinterOlympicGames.rst

aio:ProfessionalAthlete
  An athlete who competes in sport professionally.

  .. include:: classes/ProfessionalAthlete.rst

aio:UsPresident
  A politician who holds the office of President of the United States, us president

  .. include:: classes/UsPresident.rst

aio:BasketballTeam
  A sports team which plays basketball.

  .. include:: classes/BasketballTeam.rst

aio:BookEdition
  An edition of a book (first edition, second edition, etc.).

  .. include:: classes/BookEdition.rst

aio:Contralto
  A singer who sings with a contralto range.

  .. include:: classes/Contralto.rst

aio:SportsCoach
  A human being who is involved in the direction, instruction or training of a sports team or of individual sportspeople.

  .. include:: classes/SportsCoach.rst

aio:PieceOfMusic
  An audio work which is a musical composition.

  .. include:: classes/PieceOfMusic.rst

aio:AvmGenre
  A genre which is applicable to an animated visual medium.

  .. include:: classes/AvmGenre.rst

aio:FieldHockeyTeam
  A sports team which plays field hockey.

  .. include:: classes/FieldHockeyTeam.rst

aio:PopulatedPlace
  A geographical area which has a human population.

  .. include:: classes/PopulatedPlace.rst

aio:JazzMusician
  A musician whose primary genre is jazz.

  .. include:: classes/JazzMusician.rst

aio:RightFielder
  An outfielder who plays in the position of right fielder

  .. include:: classes/RightFielder.rst

aio:Gymnast
  An athlete who competes in gymnastics.

  .. include:: classes/Gymnast.rst

aio:RockMusician
  A musician whose primary genre is rock.

  .. include:: classes/RockMusician.rst

aio:AudioWork
  A published work which is an audio work.

  .. include:: classes/AudioWork.rst

aio:Song
  A track work which is a musical composition intended to be sung by the human voice.

  .. include:: classes/Song.rst

aio:Organist
  A musician who plays the organ.

  .. include:: classes/Organist.rst

aio:NationalCollegiateAthleticAssociationAthlete
  An athlete who competes in National Collegiate Athletic Association events.

  .. include:: classes/NationalCollegiateAthleticAssociationAthlete.rst

aio:Book
  A published written or printed work consisting of pages glued or sewn together along one side and bound in covers.

  .. include:: classes/Book.rst

aio:Skier
  An athlete who competes in skiing.

  .. include:: classes/Skier.rst

aio:AmericanFootballCoach
  A sports coach who coaches American football.

  .. include:: classes/AmericanFootballCoach.rst

aio:Accordionist
  A musician who plays the accordion.

  .. include:: classes/Accordionist.rst

aio:BiologicalTaxon
  a taxonomic unit in the biological taxonomy

  .. include:: classes/BiologicalTaxon.rst

aio:PresidentialResidence
  An official residence of a president.

  .. include:: classes/PresidentialResidence.rst

aio:PublishedThing
  Anything which is published, such as a book, newspaper, or music album.

  .. include:: classes/PublishedThing.rst

aio:VersionOfAPublishedWork
  A version of a published work, such as 'The Annotated Alice', or 'Master of Puppets (Remastered)'.

  .. include:: classes/VersionOfAPublishedWork.rst

aio:UnitOfForce
  unit of force

  .. include:: classes/UnitOfForce.rst

aio:Republic
  A sovereign state without a monarch.

  .. include:: classes/Republic.rst

aio:Screenwriter
  A writer of screenplays, on which a film, television program or video game is based. Also referred to as a screenplay writer, scriptwriter or scenarist.

  .. include:: classes/Screenwriter.rst

aio:BaseballTeam
  A sports team which plays baseball.

  .. include:: classes/BaseballTeam.rst

aio:Activist
  A human being who stands up or campaigns for a particular belief or ideology.

  .. include:: classes/Activist.rst

aio:MilitaryEvent
  An event which is part of a military confrontation. Examples include wars and battles.

  .. include:: classes/MilitaryEvent.rst

aio:IntergovernmentalOrganisation
  An organisation whose members are nation states.

  .. include:: classes/IntergovernmentalOrganisation.rst

aio:MolecularWeight
  molecular mass

  .. include:: classes/MolecularWeight.rst

aio:Teacher
  A human being who instructs students in an educational institution or through individual tuition.

  .. include:: classes/Teacher.rst

aio:Navy
  An armed force which fights primarily on the sea.

  .. include:: classes/Navy.rst

aio:BluesGuitarist
  A guitarist whose primary genre is the blues.

  .. include:: classes/BluesGuitarist.rst

aio:CompilationAlbum
  An album comprising tracks which may have been previosuly release or unreleased, usually from secveral separate recordings by either one or several audio acts.

  .. include:: classes/CompilationAlbum.rst

aio:EpisodeOfATvSeries
  An individual episode of a TV series

  .. include:: classes/EpisodeOfATvSeries.rst

aio:IauPlanet
  An astronomical object which is recognised by the International Astronomical Union as a planet in the solar system.

  .. include:: classes/IauPlanet.rst

aio:CricketBowler
  A cricketer who is known as a bowler

  .. include:: classes/CricketBowler.rst

aio:VisibleObject
  An object which reflects or emits light in the visible spectrum.

  .. include:: classes/VisibleObject.rst

aio:SportsLeague
  An organision comprising a group of sports teams which compete against each other in a sport.

  .. include:: classes/SportsLeague.rst

aio:AreaWithinANation
  A geographical area which is entirely contained within the boundaries of a nation state.

  .. include:: classes/AreaWithinANation.rst

aio:USState
  One of the constituent political entities of the United States. There are currently 50 US states.

  .. include:: classes/USState.rst

aio:Solid
  A substance which is solid in state, i.e. where atoms are closely packed and cannot move freely in relation to each other.

  .. include:: classes/Solid.rst

aio:HockeyTeam
  A sports team which plays some kind of hockey.

  .. include:: classes/HockeyTeam.rst

aio:Army
  An armed force which fights primarily on land.

  .. include:: classes/Army.rst

aio:Poet
  A writer who writes poetry.

  .. include:: classes/Poet.rst

aio:AcousticGuitarist
  A musician who plays the acoustic guitar.

  .. include:: classes/AcousticGuitarist.rst

aio:PlausiblyActedObject
  Examples of plausibly-acted objects include movies, TV series, theatrical productions and plays.

  .. include:: classes/PlausiblyActedObject.rst

aio:AmericanFootballVenue
  A sports venue at which American football is played.

  .. include:: classes/AmericanFootballVenue.rst

aio:UnitOfMeasurement
  unit of measurement

  .. include:: classes/UnitOfMeasurement.rst

aio:BiologicalOrder
  https://en.wikipedia.org/wiki/Order_(biology)

  .. include:: classes/BiologicalOrder.rst

aio:SupranationalUnion
  An intergovernmental organisation which is a multinational political union in which negotiated power is delegated to an authority by governments of member states.

  .. include:: classes/SupranationalUnion.rst

aio:UnitOfVolume
  unit of voltage

  .. include:: classes/UnitOfVolume.rst

aio:Drummer
  A musician who plays the drums (drum kit).

  .. include:: classes/Drummer.rst

aio:HumanBeing
  An animal which is a member of the species Homo Sapiens. This class includes people who are dead as well as those who are alive. It not does not contain fictional people.

  .. include:: classes/HumanBeing.rst

aio:UnitOfData
  unit of data

  .. include:: classes/UnitOfData.rst

aio:UnitOfLength
  unit of length

  .. include:: classes/UnitOfLength.rst

aio:Pungency
  pungency

  .. include:: classes/Pungency.rst

aio:OffensiveTackle
  An American footballer who plays in the position of offensive tackle

  .. include:: classes/OffensiveTackle.rst

aio:GeographicalArea
  An area of the surface of the Earth, usually considered to be an entity with a distinct identity. Examples include (but are not limited to): villages, towns, cities, counties, states, countries, blocs of countries, and continents.

  .. include:: classes/GeographicalArea.rst

aio:RunningBack
  An American footballer who plays in the position of running back.

  .. include:: classes/RunningBack.rst

aio:BookPublication
  A book, a version of a book, or edition of a book.

  .. include:: classes/BookPublication.rst

aio:Organism
  An object which is a living biological organism. Instances of this class are individual instances of organism, such as Methuselah, the oldest tree in the world, or Larry the Downing Street cat.

  .. include:: classes/Organism.rst

aio:UnitOfHardness
  unit of hardness

  .. include:: classes/UnitOfHardness.rst

aio:TvProducer
  A person who oversees video production on a television program.

  .. include:: classes/TvProducer.rst

aio:EpisodicTvBroadcast
  A television broadcast which is constituted as a sequence of distinct episodes. This includes TV series, seasons of a TV series and arbitrary sequences of TV episodes.

  .. include:: classes/EpisodicTvBroadcast.rst

aio:BiologicalKingdom
  https://en.wikipedia.org/wiki/Kingdom_(biology)

  .. include:: classes/BiologicalKingdom.rst

aio:Number
  An integer, rational number, real number, or complex number.

  .. include:: classes/Number.rst

aio:ObjectWhichCanHaveAGeographicalLocation
  Object which can have a geographical location. This is a general abstract class, including movable entities (e.g. human beings) as well as geographical areas.

  .. include:: classes/ObjectWhichCanHaveAGeographicalLocation.rst

aio:Vocalist
  A musician who produces music by manipulating their vocal cords.

  .. include:: classes/Vocalist.rst

aio:Voltage
  electric potential difference

  .. include:: classes/Voltage.rst

aio:TributeAlbum
  A concept album compiling a performer's work, covers of a performer's work, or tracks inspired by a performer.

  .. include:: classes/TributeAlbum.rst

aio:ThirdBaseman
  A baseball player who plays in the position of third baseman

  .. include:: classes/ThirdBaseman.rst

aio:Clarinetist
  A musician who plays the clarinet.

  .. include:: classes/Clarinetist.rst

aio:Goaltender
  An ice hockey player who plays in the position of goaltender

  .. include:: classes/Goaltender.rst

aio:FilmDirector
  A human being who directs the making of film.

  .. include:: classes/FilmDirector.rst

aio:Settlement
  A populated place which is a single human residential community. Examples of settlements are villages, towns and cities.

  .. include:: classes/Settlement.rst

aio:SingleVersion
  A version of a single track music publication.

  .. include:: classes/SingleVersion.rst

aio:UnitOfFrequency
  unit of frequency

  .. include:: classes/UnitOfFrequency.rst

aio:MembershipOrganisation
  An organisation which has members.

  .. include:: classes/MembershipOrganisation.rst

aio:MajorLeagueBaseballTeam
  A baseball team which plays in a Major League Baseball league.

  .. include:: classes/MajorLeagueBaseballTeam.rst

aio:BiologicalClass
  https://en.wikipedia.org/wiki/Class_(biology)

  .. include:: classes/BiologicalClass.rst

aio:President
  A politician who holds the role of president, a common title for the head of state in many republics.

  .. include:: classes/President.rst

aio:StringPlayer
  A musician who plays a string instrument.

  .. include:: classes/StringPlayer.rst

aio:TrackWork
  An audio work which is a track.

  .. include:: classes/TrackWork.rst

aio:Constituency
  A populated place which is represented by an elected person or body.

  .. include:: classes/Constituency.rst

aio:AlbumVersion
  Audio publication version of an album.

  .. include:: classes/AlbumVersion.rst

aio:DraftedAthlete
  A professional athlete selected by draft.

  .. include:: classes/DraftedAthlete.rst

aio:Businessperson
  A person who is engaged in the business sector.

  .. include:: classes/Businessperson.rst

aio:AudioAct
  An act which performs or records audio works, typically bands and other musical ensembles, but may also include spoken word poets and other non-musical audio acts.

  .. include:: classes/AudioAct.rst

aio:CompetingCyclist
  An athlete who competes in cycling

  .. include:: classes/CompetingCyclist.rst

aio:Inventor
  A human being who creates or discovers a new method, device or other useful means that becomes known as an invention.

  .. include:: classes/Inventor.rst

aio:ElectricalResistance
  electrical resistance

  .. include:: classes/ElectricalResistance.rst

aio:Scientist
  A human being who conducts scientific research to advance knowledge.

  .. include:: classes/Scientist.rst

aio:Bassist
  A musician who plays the bass guitar or double bass.

  .. include:: classes/Bassist.rst

aio:TightEnd
  An American football end who plays in the position of tight end.

  .. include:: classes/TightEnd.rst

aio:BasketballForward
  A basketball player who plays in a forward position.

  .. include:: classes/BasketballForward.rst

aio:Wrestler
  An athlete who competes in  wrestling.

  .. include:: classes/Wrestler.rst

aio:BiologicalPhylum
  https://en.wikipedia.org/wiki/Phylum

  .. include:: classes/BiologicalPhylum.rst

aio:Queen
  A monarch who is female.

  .. include:: classes/Queen.rst

aio:TimeZoneArea
  A geographical area that observes a uniform standard time for legal, commercial, and social purposes

  .. include:: classes/TimeZoneArea.rst

aio:Integer
  An number which can be written without a fractional component.

  .. include:: classes/Integer.rst

aio:Violinist
  A musician who plays the violin.

  .. include:: classes/Violinist.rst

aio:Catcher
  A baseball player who plays in the position of catcher.

  .. include:: classes/Catcher.rst

aio:Angle
  angle

  .. include:: classes/Angle.rst

aio:Speed
  speed

  .. include:: classes/Speed.rst

aio:MomentOfForce
  torque

  .. include:: classes/MomentOfForce.rst

aio:PhysicalObject
  A collection of physical matter within a defined contiguous boundary in three-dimensional space.

  .. include:: classes/PhysicalObject.rst

aio:Fullback
  An American footballer who plays in the position of fullback.

  .. include:: classes/Fullback.rst

aio:Golfer
  An athlete who competes in golf

  .. include:: classes/Golfer.rst

aio:Jockey
  An athlete who competes in horse-racing by riding the race-horse.

  .. include:: classes/Jockey.rst

aio:BasketballVenue
  A sports venue at which basketball is played.

  .. include:: classes/BasketballVenue.rst

aio:Frequency
  frequency

  .. include:: classes/Frequency.rst

aio:BasketballPosition
  A sports position pertaining to the sport of basketball.

  .. include:: classes/BasketballPosition.rst

aio:SoccerStriker
  A soccer player who plays in the position of striker

  .. include:: classes/SoccerStriker.rst

aio:Diplomat
  A human being whose role is to engage in diplomacy, conducting negotiations between state representative.

  .. include:: classes/Diplomat.rst

aio:SurfaceDensity
  surface density

  .. include:: classes/SurfaceDensity.rst

aio:Volume
  volume

  .. include:: classes/Volume.rst

aio:Musician
  A human being who plays, performs, composes, or arranges music.

  .. include:: classes/Musician.rst

aio:OperaComposer
  A composes who composes operas.

  .. include:: classes/OperaComposer.rst

aio:Temperature
  temperature

  .. include:: classes/Temperature.rst

aio:Baritone
  A singer who sings with a baritone range.

  .. include:: classes/Baritone.rst

aio:UsDemocrat
  A human being who supports the Democratic Party, one of the two main political parties in the United States.

  .. include:: classes/UsDemocrat.rst

aio:FilmScoreAlbum
  An album containing the score to a film.

  .. include:: classes/FilmScoreAlbum.rst

aio:AmountOfMoney
  amount of money

  .. include:: classes/AmountOfMoney.rst

aio:PowerForward
  A basketball forward who plays in the position of power forward

  .. include:: classes/PowerForward.rst

aio:Color
  A property possessed by an object of producing different sensations on the eye as a result of the way it reflects or emits light. Instances include green, white and fuschia pink.

  .. include:: classes/Color.rst

aio:Length
  length

  .. include:: classes/Length.rst

aio:Genre
  A conventionally agreed form or type of communication. Examples of genres include: comedy, drama, and horror.

  .. include:: classes/Genre.rst

aio:AustralianFootballPlayer
  An athlete who competes in Australian football

  .. include:: classes/AustralianFootballPlayer.rst

aio:VideoGame
  A piece of software which is a game involving interaction with a user interface generating visual feedback on a video display device.

  .. include:: classes/VideoGame.rst

aio:PoliticalConcept
  A concept that is used within politics.

  .. include:: classes/PoliticalConcept.rst

