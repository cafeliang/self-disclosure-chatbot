# AlexaPrizeCobotToolkit

This is a Python3 toolkit for building conversational bot on the Alexa Skills Kit.

## Development

See [DEVELOPMENT.md](./DEVELOPMENT.md)

All contents are copyright 2018 Amazon.com and are provided to Alexa Prize competitors as Program Materials, subject to
the Program Materials License Agreement ( https://developer.amazon.com/public/support/pml.html ). 


## Change Log

### v2.1.3 November 26, 2019
* Bugfix for cobot update: Modules will now be correctly removed from the beta stack when they are deleted from modules.yaml
* Bugfix for cobot transcribe: Output will now be sorted by timestamp when a conversation is retrieved using conversation_id

### v2.1.2 November 18, 2019
* Cobot SDK
    1. Fixes to State Manager
        * Removed returning user attributes as session attributes in the ASK response. Only conversationId is returned in sessionAttributes in the ASK response. 
        * Removed the assumption that attributes in ASK event implies user attributes. User Attributes will be decided and set by users of this SDK. User Attributes will be saved at the end of each turn if persist is enabled. Source of truth for conversationId is sessionAttributes in ASK response and attributes in ASK request. Source of truth for other User attributes is UserTable in Dynamodb.
    2. Fixes to Interactive mode
        * Added mocked conversationId in the launch request event to replicate prod event.
        * Overriding the session attributes in the event recieved from ask simulate by session attributes returned by local lambda in the previous turn.
        * Userid is a required field now.
    3. Fixes to AWS region specification in config.yaml. We now adhere to the region specified by the user in the config.yaml.
        * Removed us-east-1 as default region for Dynamo db.
        * AWS Region is read from user's aws configure settings. This is the case for dynamodb_manager.py.
    4. Minor changes to the deployment and delete scripts. Scripts now fail if any underlying command fails or a variable is undefined.
        
* Alexa Information Knowledge Graph
    1. Expansion of the Alexa Information Knowledge Graph to include Science & Tech ontologies
    2. Added a section describing quantities 

### v2.1.1 October 22, 2019
* Launch GPU support, see "doc/htm/bot_configuration.html" for details.
* Provide memory_reservation recommendations for baseline modules in modules.yaml to help reduce resource usage.
* Reduce default cpu_cluster_size and module desired_count parameters from 2 to 1. See "doc/htm/bot_configuration.html" for examples of how to configure these parameters.

### v2.1 October 2, 2019
* Expansion of the Alexa Information Knowledge Graph to include Politics and Cross-Domain ontologies
  1. This update includes representations for things such as politicians, political parties, government officials, and the countries and organizations to which they’re attached
  2. We also now support "cross-domain" ontology, allowing for easier graph traversal and seamless transitions between topics
* Bugfixes to interactive-mode to improve stability and provide mock 1-best ASR. Changes include:
  1. Fixed "This utterance did not resolve to any intent in your skill" intermittent crashes
  2. Mock utterance-level confidence and token offsets now available
  3. You are no longer required to start each interaction with "open <invocation name>"
  4. Your service module config is now a required parameter, see "doc/htm/testing_cobot_components.html#cobot-interactive-mode-cli" for updated examples
* Bugfix in the baseline bot selecting strategy to make the default an empty list of response generators

### v2.0 September 9, 2019

* Baseline socialbot built on top of Cobot implements learnings from past Alexa Prize competitions
  1. Demonstrates end-to-end intent classification and response generation, including sample response generators (News, Movies, ShareFacts, and ShowerThoughts)
  2. Includes noun phrase extraction, ASR correction, and coreference resolution remote modules
  3. Illustrates two example knowledge modules: a Reddit DynamoDB module and an Evi question answering module
* Integration with Alexa Information Knowledge Graph and entity resolution service, via the Alexa Information Query Language (AIQL)
* V2 DialogAct model uses conversation context to jointly predict dialog acts and topic, with improved performance
* Cobot CLI Improvements
  1. New -t option for cobot delete allows user to specify which target to delete (stack, lambda, repo, or all)
  2. Lambda allocated memory can now be configured in config.yaml
  3. New cobot add-remote-module CLI command creates a template directory for a new remote module
  4. Skill management is now handled in the ASK Developer Console, with the option to save a copy of the skill intent model in Cobot's CodeCommit repo for version control

### v1.8 August 22, 2018

* Add a new DialogActModel Service and python client endpoint hosted in AlexaPrizeToolkitService, see "doc/htm/model_release_material.html"
* Add a new ConversationEvaluatorModel Service and python client endpoint hosted in AlexaPrizeToolkitService, see "doc/htm/model_release_material.html"
* Upgrade Profanity service to V2, with improved model performance
* OffensiveSpeechClassifier now uses the 'overallclass' label from Profanity Service hosted in AlexaPrizeToolkitService
* Bugfix:
  1. Dynamodb manager stores a list of floats successfully, by converting them to a list of Decimal

### v1.7 July 9, 2018

* Add a new vaderSentiment service endpoint to return full analysis rather than sentiment label, see "doc/htm/nlp_pipeline.html"
* Add an optional user_id argument to interactive-mode, see "doc/htm/testing_cobot_components.html#interactive-mode-log"
* Make SNS topic as configurable parameter in config.yaml
* Code cleanup: remove unused classes

### v1.6 June 6, 2018

* Add tools to deploy and backfill dynamodb table to s3 and enable quick data analytics and visualization with AWS Athena and QuickSight, see "doc/htm/data_analytics_visualization.html"
* cobot interactive-mode CLI supports 1-best ASR
* Enable printing out local Docker modules' logs in the cobot local-test and interactive-mode CLI, see "doc/htm/testing_cobot_components.html#new-features-in-v1-6 and doc/htm/testing_cobot_components.html#local-test-log"
* Add alexa prize sentiment service in AlexaPrizeToolkitService, see "example/service_client/demo_client.py"
* Add a default timeout of 1s to all AlexaPrizeToolkitService: topic, sentiment, Evi Q/A, and profanity and the default timeout can be configured, see "doc/htm/accessing_toolkit_service_client.html"
* Add a default timeout of 1s to RemoteServiceModule and it can be configured through module config
* Enable customizing health-check grace period timeout for each ECS service, see "doc/htm/bot_configuration.html#module-configuration"
* Improve Documentation by adding more explanations and sample codes to each feature
  1. Add Cheat-sheet, an index page to all CoBot's supported features and dataset, to increase discoverability, see "doc/htm/cheat_sheet.html"
  2. Add FAQ, see "doc/htm/faq.html"
  3. Improve state manager by adding more explanation on state manager's life cycle, attributes, and read/write APIs, see "doc/htm/state_management_and_information_flow.html"
  4. Improve overrides by adding explanation on each component and its default behavior and a quick 3-step or 5-step to override each CoBot component, see "doc/htm/overriding_cobot_components.html"
  5. Add workflow recommendation in Cheat-sheet, see "doc/htm/workflow_recommendation.html"
  6. General improvements to all CoBot documentations: consistent with code and more diagrams, sample code, and explanations
* Bugfixes
  1. Sending a list of state information to remote service modules when history_turns is greater than 0
  2. If some specific state information is missing, send a default value of NONE to remote service modules

### v1.5 May 8, 2018

* Improve CoBot logging with meta data (conversation_id, session_id) to each log message and add instructions on log querying. See “doc/htm/debugging_and_log_access.html/Query CloudWatch Logs and doc/htm/debugging_and_log_access.html/Using CloudWatch for Log Access” for detail. 
* Add local turn-by-turn chat by interactive-mode, See “doc/htm/debugging_and_log_access.html/Using CoBot's Interactive-Mode” for detail
* Improve CoBot transcribe CLI by supporting transcribing a conversation by conversation id, See “doc/htm/debugging_and_log_access.html/Using the CoBot Transcriber” for detail. 
* Improve A/B test by persisting A/B test configs to DynamoDB. 
* Topic service in AlexaPrizeToolkitService generates both topic and keyword, see "example/service_client/demo_client.py" for detail. 
* Fetch the most recent 50 turns' session history information from DynamoDB. 
* Add new rank_advanced method in ranking_strategy.py to handle dict as input and add dialog_manager_advanced.py 
* Improve cobot_common/template Dockerfile by using the cobot_base base image (bundles ubuntu:16.04 and python 3.6.5)
* Improve Documentation, see "doc/htm/state_management_and_information_flow.html" for detail. 
* Bugfixes

### v1.4 April 10, 2018

* Improve CoBot logging with meta data (logging level, timestamp, request id) to each log message. See “doc/htm/debugging_and_log_access.html” for detail. 
* Enable multivariant A/B testing. See "doc/htm/experimentation_and_ab_testing.html" for detail. 
* Improve local-test logging and add preliminary input and output validations, See “doc/htm/testing_cobot_components.html” for detail. 
* Easy access to conversation_id and asr in State Manager when connected with AlexaPrizeSpeechlet (“Let's Chat” experience). 
* Change AMAZON.LITERAL to AMAZON.RAW_TEXT. 
* Enable RepromptBuilder with access to State Manager.  
* Add cobot transcribe CLI, to enable viewing state information (request, response, intent, etc) for one specific session id. see “doc/htm/debugging_and_log_access.html”. 
* Remove catch global exception logic in cobot_handler.py, to allow uptime system to monitor skill status and encourage teams to look for ERROR logs in cloudwatch and fix them ASAP.  https://code.amazon.com/reviews/CR-1758349/revisions/1#/details
* Bugfixes: 
    1. Catch Exception for each module creation. 
    2. Catch Exception when persistence to DynamoDB fails.
    3. Fix DynamoDB persistence bug when an AttributeValue contains an empty string. 


### Patch March 28, 2018

* Add request_type to state object, set LaunchRequest's intent as “LaunchRequestIntent”
* Catch offensive service call's exception in asr_processor and dialog_manager
* Write sample LaunchRequestGenerator to generate responses for LaunchRequestIntent, and add example mapping on MappingSelectingStrategy
* Fix local-test's text extraction logic, in sync with the logic in the state.py
* Add launch request handler doc. See "doc/htm/handling_launch_requests.html" for detail.

### v1.3 March 20, 2018

* Added beta and prod stages in code pipeline. See doc "doc/html/beta_stage_for_cobot.html" for detail. 
* Improved EC2 cluster utilization and exposed cluster's key-pair, instance type, and each docker module's desired count and memory reservation. See doc "doc/htm/bot_configuration.html" for detail. 
* Enabled auto-scaling ECS service and EC2 cluster
* Added A/B test functionality for different Dialog Managers, Feature Extractors, ASR Processors, Response Generators, and NLP Pipelines. See doc "doc/htm/experimentation_and_ab_testing.html" for detail.
* Added Enwiki and DbPedia data modules and entity linking. See "doc/htm/using_included_data_and_models.html" for detail.


### Patch March 5, 2018
* Support accessing and changing user attributes in local and remote Docker service modules
* Improve Documentation "State Management and Information Flow"

### v1.2 Feb 26, 2018

* Add a local testing script to test Lambda functions and local/remote Docker modules. See Documentation "Testing Cobot Components/End to End Local Lambda Function Tests with Remote/Local Service Modules". 
* Support dependent/hierarchical NLP Pipeline. See Documentation "Overriding Cobot Components/Adding a New Module to the NLP Pipeline"
* Release an improved topic classifier model in AlexaPrizeToolkitService
* Added type annotations to Cobot core classes
* Several improvements to documentation: how to override ASR Processor and Feature Extractor examples in "Overriding Cobot Components"

   
### v1.1 Feb 12, 2018
v1.1  with a few notable changes.

* Added new data Annotator API. 
* Added 2 new documentation sections: "State Management and Information Flow" and "Overriding Cobot Components"
* Several improvements to documentation
* Improve NLP module NER to handle caseless input. 
* Cobot deployment bug fixes: 
   1. envcheck script enforces ask-cli version is 1.0.0 
   2. prints out pipeline URL with error message
   3. validates default config values get changed

### v1.0 Feb 05, 2018
v1.0 with the basic Cobot Toolkit functionality. 