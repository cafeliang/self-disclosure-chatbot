import boto3
import csv
from datetime import *
# import time
import pprint
import logging
from retrying import retry
import argparse
import time
import botocore

import re
import os

rating_map = {}
ath_client = boto3.client('athena')
s3 = boto3.resource('s3')


@retry(stop_max_attempt_number=10,
       wait_exponential_multiplier=30 * 1000,
       wait_exponential_max=10 * 60 * 1000)
def poll_status(_id):
    '''
    poll query status
    '''
    print("Polling query status...")
    result = ath_client.get_query_execution(
        QueryExecutionId=_id
    )
    logging.info(pprint.pformat(result['QueryExecution']))
    state = result['QueryExecution']['Status']['State']
    if state == 'SUCCEEDED':
        return result
    elif state == 'FAILED':
        return result
    else:
        raise Exception


def write_new_file(output_file, start_date, end_date):
    with open('./ratings.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        sdt = time.mktime(datetime.strptime(
            start_date, '%Y-%m-%d').timetuple())
        edt = time.mktime(datetime.strptime(end_date, '%Y-%m-%d').timetuple())
        for row in reader:
            ti = row["Approximate Start Time"]
            cid = row['Conversation ID']
            rating = row['Rating']
            dt = time.mktime(datetime.strptime(
                ti, '%Y-%m-%d %H:%M:%S').timetuple())
            if int(dt) > int(edt) + 86400 * 2 or int(dt) < int(sdt) - 86400 * 2:
                continue
            rating_map[cid] = rating

    with open('./dialog.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        with open(output_file, 'w') as outfile:
            fieldnames = ["session_id", "creation_date_time", "candidate_response", "conversation_id", "user_id", "request_type", "intent", "text", "selected_modules",
                          "sentiment", "intent_classify", "npkknowledge", "knowledge", "noun_phrase", "topic", "response", "ner", "slots", "asr", "topic_class"]
            fieldnames.append('rating')
            writer = csv.DictWriter(outfile, fieldnames=fieldnames)
            writer.writeheader()
            for row in reader:
                if row['conversation_id'] in rating_map:
                    row['rating'] = rating_map[row['conversation_id']]
                    writer.writerow(row)


def get_dialog_file(ath_id, csv_output):
    ret = s3.meta.client.download_file(
        'gunrock-2018-dialog', str(ath_id) + '.csv', csv_output)
    return ret


def run_query(query, database, s3_output, csv_output):
    ath_client = boto3.client('athena')
    response = ath_client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )

    QueryExecutionId = response['QueryExecutionId']
    print("Get QueryExecutionId: {}\n".format(response['QueryExecutionId']))
        
    result = poll_status(QueryExecutionId)
    if result['QueryExecution']['Status']['State'] == 'SUCCEEDED':
        get_dialog_file(QueryExecutionId, csv_output)
        print("SUCCEEDED")
    else:
        if result['QueryExecution']['Status']['State'] == 'FAILED':
            if 'HIVE_BAD_DATA' in result['QueryExecution']['Status']['StateChangeReason']:
                    # fix particular log
                response = fix_hive_data(
                    result['QueryExecution']['Status']['StateChangeReason'])
                if not response:
                    print("Unable to fix hive data error .. exiting")
                else:
                    # if error was properly fixed try again
                    run_query(query, 'gunrock2018',
                              's3://gunrock-2018-dialog', args.output)
            else:
                print("FAILED. StateChangeReason: {}".format(result['QueryExecution']['Status']['StateChangeReason']))



def fix_hive_data(err):
    print(err)
    # find log
    stamp = r"\d{4}-\d{2}-\d{2}(T)\d{2}:\d{2}\:\d{2}"
    findstamp = re.search(stamp, err)
    if not findstamp:
        return False
    timestampstr = findstamp.group(0)
    parsestamp = timestampstr.replace('T', ' ')
    dtob = datetime.strptime(parsestamp, "%Y-%m-%d %H:%M:%S")
    t = dtob.strftime("%Y/%m/%d/%H")
    bname = 'cobot-stack-data-analytics-dynamodbstreambucket-4wfw1guhhvld'
    bucket = s3.Bucket(bname)
    dirlist = []
    stamplist = []
    idmap = {}
    for ob in bucket.objects.filter(Prefix='firehose/' + t):
        temp = re.search(r"\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}", ob.key)
        keystamp = temp.group(0)
        dirlist.append(keystamp)
        dt = datetime.strptime(keystamp, "%Y-%m-%d-%H-%M-%S")
        if dt > dtob:
            continue
        idmap[dt] = ob.key
        stamplist.append(dt)
    closest = min(stamplist, key=lambda x: abs(x - dtob))
    bad_log_path = idmap[closest]

    print(bad_log_path)
    parts_path = bad_log_path.split('/')
    if not parts_path:
        return False
    idlog = parts_path[-1]
    # temp = re.search(r"[a-z\d]{8}-[a-z\d]{4}-[a-z\d]{4}-[a-z\d]{4}-[a-z\d]{12}", bad_log_path)
    print(idlog)
    print(bad_log_path)
    try:
        bucket.download_file(bad_log_path, 'badlog')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise

    with open('./badlog', 'r+') as b_log_fp:
        line = b_log_fp.read()
        if timestampstr not in line:
            return False
        newline = line.replace(timestampstr, parsestamp)
        b_log_fp.seek(0)
        b_log_fp.write(newline)
        b_log_fp.close()

        bucket.upload_file('./badlog', bad_log_path)
        os.remove('./badlog')
    return True



if __name__ == "__main__":
    now = datetime.now()
    today = datetime.today() + timedelta(days=1)
    parser = argparse.ArgumentParser(description='Search range.')
    yesterday = datetime.today() - timedelta(days=1)
    yesterday = yesterday.replace(hour=9, minute=0, second=0)
    parser.add_argument('--start', '-s', default=yesterday.strftime("%Y-%m-%d %H:%M:%S"),
                        help='timerange from (YYYY-mm-dd HH:MM:SS)')
    parser.add_argument('--end', '-e', default=today.strftime("%Y-%m-%d %H:%M:%S"),
                        help='timerange to (YYYY-mm-dd HH:MM:SS)')
    parser.add_argument('--output', '-o', default='output.csv',
                        help='output file (default: output.csv)')
    args = parser.parse_args()
    # query = "SELECT * FROM Dialog WHERE date(creation_date_time) >= date('{}') and date(creation_date_time) <= date('{}');".format(args.start, args.end)
    print("Start: {}".format(args.start))
    print("End: {}".format(args.end))
    print("Output: {}\n".format(args.output))


    # query = """
    # WITH temp as
    # (SELECT DISTINCT conversation_id, rating
    #  FROM Dialog
    #  WHERE Dialog.creation_date_time between date_parse('{}', '%Y-%m-%d %T') and date_parse('{}', '%Y-%m-%d %T'))
    # SELECT Dialog.*, temp.rating as rating
    # FROM Dialog
    # JOIN temp
    # ON Dialog.conversation_id = temp.conversation_id
    # """.format(args.start, args.end)

    query = """
        SELECT creation_date_time, conversation_id, text, response, topic, topic_class, rating
        FROM dialog2018
        ORDER BY rating DESC;
    """

    # query = """
    # WITH temp as
    # (SELECT DISTINCT conversation_id, rating
    #  FROM Dialog, RATINGS
    #  WHERE RATINGS.cid = Dialog.conversation_id AND Dialog.creation_date_time between date_parse('{}', '%Y-%m-%d %T') and date_parse('{}', '%Y-%m-%d %T'))
    # SELECT Dialog.*, temp.rating as rating
    # FROM Dialog
    # JOIN temp
    # ON Dialog.conversation_id = temp.conversation_id
    # """.format(args.start, args.end)
    print("Query: {}".format(query))
    run_query(query, 'gunrock2018', 's3://gunrock-2018-dialog/output_queries/', args.output)
