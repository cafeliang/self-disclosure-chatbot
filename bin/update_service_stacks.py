import sys
import os
import yaml
import json
import re

bot_name = sys.argv[1]
modules = sys.argv[2]
stacks = sys.argv[3]
new_bot_modules = set()
old_bot_modules = set()
old_beta_bot_modules = set()
new_infra_modules = set()
old_infra_modules = set()
old_beta_infra_modules = set()
deleted_stacks = set()

try:
    y = yaml.load(open(modules))
except Exception as e:
    print("Unable to load yaml file: " + modules)
    print(e)
    sys.exit(1)

if "cobot_modules" in y and y['cobot_modules']:
    for module in y['cobot_modules']:
        if isinstance(module, dict):
            module_name = module.get('name')
            new_bot_modules.add(module_name)
        elif isinstance(module, str):
            new_bot_modules.add(module)
if "infrastructure_modules" in y and y['infrastructure_modules']:
    for module in y['infrastructure_modules']:
        if isinstance(module, dict):
            module_name = module.get('name')
            new_infra_modules.add(module_name)
        elif isinstance(module, str):
            new_infra_modules.add(module)

try:
    js = json.load(open(stacks))
except Exception as e:
    print("Unable to load json file: " + stacks)
    print(e)
    sys.exit(1)

if "StackSummaries" in js and js['StackSummaries']:
    for stack in js['StackSummaries']:
        if "StackName" in stack and stack['StackName']:
            # match the prod cobot modules
            pattern = bot_name + "-stack-BotService"
            match = re.match(pattern, stack['StackName'])
            if match:
                module = match.string[match.end():]
                old_bot_modules.add(module)
            # match the beta cobot modules
            pattern = bot_name + "-stack-BetaBotService"
            match = re.match(pattern, stack['StackName'])
            if match:
                module = match.string[match.end():]
                old_beta_bot_modules.add(module)
            # match the prod infra modules
            pattern = bot_name + "-stack-InfrastructureService"
            match = re.match(pattern, stack['StackName'])
            if match:
               module = match.string[match.end():]
               old_infra_modules.add(module)
            # match the beta infra modules
            pattern = bot_name + "-stack-BetaInfrastructureService"
            match = re.match(pattern, stack['StackName'])
            if match:
               module = match.string[match.end():]
               old_beta_infra_modules.add(module)


# Delete the removed modules from prod and beta stacks

for module in old_bot_modules:
    if not module in new_bot_modules:
        stack = bot_name + "-stack-BotService" + module
        cmd = "aws cloudformation delete-stack --stack-name " + stack
        if os.system(cmd) is not 0:
            sys.exit(1)
        deleted_stacks.add(stack)

for module in old_beta_bot_modules:
    if not module in new_bot_modules:
        stack = bot_name + "-stack-BetaBotService" + module
        cmd = "aws cloudformation delete-stack --stack-name " + stack
        if os.system(cmd) is not 0:
            sys.exit(1)
        deleted_stacks.add(stack)

for module in old_infra_modules:
    if not module in new_infra_modules:
        stack = bot_name + "-stack-InfrastructureService" + module
        cmd = "aws cloudformation delete-stack --stack-name " + stack
        if os.system(cmd) is not 0:
            sys.exit(1)
        deleted_stacks.add(stack)

for module in old_beta_infra_modules:
    if not module in new_infra_modules:
        stack = bot_name + "-stack-BetaInfrastructureService" + module
        cmd = "aws cloudformation delete-stack --stack-name " + stack
        if os.system(cmd) is not 0:
            sys.exit(1)
        deleted_stacks.add(stack)

if deleted_stacks:
    cmd = "echo Waiting for service stacks update to complete. This may take several minutes..."
    os.system(cmd)
    for stack in deleted_stacks:
        cmd = "aws cloudformation wait stack-delete-complete --stack-name " + stack
        if os.system(cmd) is not 0:
           sys.exit(1)