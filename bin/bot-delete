#!/bin/bash

set -euo pipefail


function exit_with_message {
    echo "$1" >&2   
    echo Exiting...
    cd "$orig_dir"
    exit 1
}

function delete_codecommit_repo {
    echo "Deleting CodeCommit repository..."
    if ! aws codecommit get-repository --repository-name "$repository" --region "$region" &> /dev/null; then
        echo "$repository CodeCommit repository doesn't exist."
    else
        aws codecommit delete-repository --repository-name "$repository" --region "$region" || {
            exit_with_message "Unable to delete CodeCommit repository $repository."
        }
    fi
}

function delete_lambda {
    echo "Deleting lambda..."
    if aws lambda get-function --function-name "$lambda_name" --region "$region" &> /dev/null ; then
        aws lambda delete-function --function-name "$lambda_name" --region "$region"|| {
            exit_with_message "Unable to delete lambda function $lambda_name."
        }
    fi
}

function delete_cloud_formation_stacks {
    echo "Deleting service stacks..."
    bot_modules=`python3 "$bin_directory"/get_bot_modules.py modules.yaml` || {
        exit_with_message "Unable to read bot modules from modules.yaml."
    }
    infrastructure_modules=`python3 "$bin_directory"/get_infrastructure_modules.py modules.yaml` || {
        exit_with_message "Unable to read infrastructure modules from modules.yaml."
    }
    for module in $bot_modules; do
        if aws cloudformation describe-stacks --stack-name "$botname"-stack-BotService"$module" --region "$region" &> /dev/null; then
            aws cloudformation delete-stack --stack-name "$botname"-stack-BotService"$module" --region "$region" || {
                exit_with_message "Unable to delete stack $botname-stack-BotService$module."
            }
        fi
        # Delete beta service
        if aws cloudformation describe-stacks --stack-name "$botname"-stack-BetaBotService"$module" --region "$region" &> /dev/null; then
            aws cloudformation delete-stack --stack-name "$botname"-stack-BetaBotService"$module" --region "$region"|| {
                exit_with_message "Unable to delete stack $botname-stack-BotService$module."
            }
        fi
    done
    for module in $infrastructure_modules; do
        if aws cloudformation describe-stacks --stack-name "$botname"-stack-InfrastructureService"$module" --region "$region" &> /dev/null; then
            aws cloudformation delete-stack --stack-name "$botname"-stack-InfrastructureService"$module" --region "$region"|| {
                exit_with_message "Unable to delete stack $botname-stack-InfrastructureService$module."
            }
        fi
        # Delete beta service
        if aws cloudformation describe-stacks --stack-name "$botname"-stack-BetaInfrastructureService"$module" --region "$region" &> /dev/null; then
            aws cloudformation delete-stack --stack-name "$botname"-stack-BetaInfrastructureService"$module" --region "$region"|| {
                exit_with_message "Unable to delete stack $botname-stack-BetaInfrastructureService$module."
            }
        fi
    done


    echo "Waiting for service stack deletion to complete. This may take several minutes..."
    for module in $bot_modules; do
        aws cloudformation wait stack-delete-complete --stack-name "$botname"-stack-BotService"$module" --region "$region"|| {
            exit_with_message "Stack $botname-stack-BotService$module did not reach DELETE_COMPLETE status."
        }

        aws cloudformation wait stack-delete-complete --stack-name "$botname"-stack-BetaBotService"$module" --region "$region"|| {
            exit_with_message "Stack $botname-stack-BetaBotService$module did not reach DELETE_COMPLETE status."
        }
    done
    for module in $infrastructure_modules; do
        aws cloudformation wait stack-delete-complete --stack-name "$botname"-stack-InfrastructureService"$module" --region "$region"|| {
            exit_with_message "Stack $botname-stack-InfrastructureService$module did not reach DELETE_COMPLETE status."
        }

        aws cloudformation wait stack-delete-complete --stack-name "$botname"-stack-BetaInfrastructureService"$module" --region "$region"|| {
            exit_with_message "Stack $botname-stack-BetaInfrastructureService$module did not reach DELETE_COMPLETE status."
        }
    done

    echo "Deleting root stack..."
    stack_name="$botname-stack"
    if aws cloudformation describe-stacks --stack-name "$stack_name" --region "$region" &> /dev/null ; then
        stack_cmd="aws cloudformation delete-stack --stack-name $stack_name --region $region" 
        python3 "$bin_directory"/get_stack_events.py "$stack_name" "$stack_cmd" || {
            exit_with_message "Stack deletion for stack $botname-stack failed."
        }
    fi
}


USAGE="Usage: ./bot-delete <cobot-name> -t <target>
Options:
<cobot-name> is the directory containing the cobot deployment details.
<target> can be 'stack', 'lambda', 'repo' or 'all'. "


if [ "$#" -ne 3 ]; then
    exit_with_message "$USAGE"
fi

if [ "$COBOT_HOME" = "" ]; then
    echo "Warning: COBOT_HOME environment variable not set. Quitting."
    exit 1
fi

cobot_directory=$1                         # Cobot Lambda source directory: use a relative path of the package AlexaPrizeCobotToolkit

cobot_path="$COBOT_HOME/$cobot_directory"
orig_dir=$PWD
cd "$COBOT_HOME/bin"

if [ ! -d "$cobot_path" ]; then
    # Control will enter here if $DIRECTORY doesn't exist.
    echo "Directory $cobot_directory doesn't exist in COBOT_HOME directory $COBOT_HOME"
    cd "$orig_dir"
    exit 1
fi

botname="$(cat ../${cobot_directory}/config.yaml | shyaml get-value botname)"
repository="$(cat ../${cobot_directory}/config.yaml | shyaml get-value repository)"
lambda_name="$(cat ../${cobot_directory}/config.yaml | shyaml get-value lambda_name)"
region="$(cat ../${cobot_directory}/config.yaml | shyaml get-value region)"

bin_directory=`pwd`
cd ..
cd "$cobot_directory" || {
    exit_with_message "Unable to cd into $cobot_directory."
}

target="$3"
if [ $target == "repo" ]; then
    echo -e "This step will delete your CodeCommit repository and you will lose access to your entire codebase.\n\
This action CANNOT BE UNDONE. Are you sure you want to delete your repo? "
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) delete_codecommit_repo; break;;
            No ) exit;;
        esac
    done
elif [ $target == "lambda" ]; then
    delete_lambda
elif [ $target == "stack" ]; then
    delete_cloud_formation_stacks
elif [ $target == "all" ]; then
    delete_lambda
    delete_cloud_formation_stacks
else
  echo "Invalid target. Valid targets: 'stack', 'lambda', 'repo' or 'all'."
  exit 1
fi

echo "Done!"