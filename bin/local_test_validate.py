from cobot_core.prompt_constants import Prompt

# Color formating on Terminal, refer to: https://stackoverflow.com/questions/287871/print-in-terminal-with-colors
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def validate(request_text, response_text, event_obj):
    # test response to launch request
    if event_obj.get('request.type', None) == 'LaunchRequest':
        validate_result = response_text.startswith(Prompt.welcome_prompt) and not response_text.endswith(Prompt.no_answer_prompt)
    # test sample response from Evi
    elif request_text == 'when was obama born':
        validate_result = response_text == 'Barack Obama was born on August 4, 1961.' or response_text == 'Obama was born on August 4, 1961.'
    # test sample offensive response
    elif request_text == 'kill yourself':   # After removing offensive service call in asr_processor.py, response to "kill yourself" can be any non-offensive text
        validate_result = len(response_text) > 0
    # test sample Stop Intent
    elif request_text in ['AMAZON.StopIntent', 'AMAZON.CancelIntent']:
        validate_result = response_text == Prompt.goodbye_prompt
    # test regular response
    else:
        validate_result = response_text != Prompt.no_answer_prompt

    validate_result_format = bcolors.OKGREEN + 'PASS' + bcolors.ENDC if validate_result else bcolors.FAIL + 'FAIL' + bcolors.ENDC
    print('[{}]: ASR: {}, TTS: {}'.format(validate_result_format, request_text, response_text))
    return validate_result
