#!/usr/bin/env python -
"""
Example usage:
$ python3 jsonlint.py input.json
Invalid JSON
    Expecting property name: line 4 column 3 (char 35)
        baz: -23
        ^-- Expecting property name
"""

import re
import sys
try:
    import json
except:
    import simplejson as json
from io import StringIO

def parse_error(err):
    """
    "Parse" error string (formats) raised by (simple)json:
    '%s: line %d column %d (char %d)'
    '%s: line %d column %d - line %d column %d (char %d - %d)'
    """
    return re.match("""^
            (?P<msg>[^:]+):\s+
            line\ (?P<lineno>\d+)\s+
            column\ (?P<colno>\d+)\s+
            (?:-\s+
                line\ (?P<endlineno>\d+)\s+
                column\ (?P<endcolno>\d+)\s+
            )?
            \(char\ (?P<pos>\d+)(?:\ -\ (?P<end>\d+))?\)
    $""", err, re.VERBOSE)

fname = sys.argv[1]

with open(fname, 'r') as myfile:
    src=myfile.read()

    try:
        json.loads(src)
        print("Valid JSON")
        exit(0)
    except ValueError as err:
        print("Invalid JSON")
        msg = str(err)
        err = parse_error(msg).groupdict()
    # cast int captures to int
        for k, v in err.items():
            if v and v.isdigit():
                err[k] = int(v)
        src = StringIO(src)
        for ii, line in enumerate(src.readlines()):
            if ii == err["lineno"] - 1:
                break
        print("""
    %s
    %s
    %s^-- %s
    """ % (msg, line.replace("\n", ""), " " * (err["colno"] - 1), err["msg"]))
        exit(1)
