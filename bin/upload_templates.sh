#!/usr/bin/env bash

set -euo pipefail

function jsonValue() {
KEY=$1
awk -F"[,:}]" '{for(i=1;i<=NF;i++){if($i~/'$KEY'\042/){print $(i+1)}}}' | tr -d '"' | tr -d '[:space:]' | sed -n p
}


bucket=$1
region=$2
cobot_dir=$3
aws_account_number="$(aws sts get-caller-identity | jsonValue Account)"


python3 template-expander.py "../${cobot_dir}" || {
	echo "Unable to expand templates."
    exit 1
}

cd ../cobot_deployment/rendered

rm -rf ../deploy/templates.zip || :
zip ../deploy/templates.zip master.yaml templates/*
cd ..

echo "Creating new bucket : $bucket $region"

aws s3api head-bucket --bucket "${bucket}" --region "$region"  2> /dev/null ||
aws s3 mb "s3://${bucket}" --region "$region" || {
	echo "Unable to create new bucket named $bucket."
	exit 1
}

echo "Creating policy for new bucket : $bucket $region"
aws s3api put-bucket-policy \
    --bucket "${bucket}" \
    --policy "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":\"arn:aws:iam::${aws_account_number}:root\"},\"Action\":[\"s3:GetObject\",\"s3:GetObjectVersion\"],\"Resource\":\"arn:aws:s3:::${bucket}/*\"},{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":\"arn:aws:iam::${aws_account_number}:root\"},\"Action\":[\"s3:ListBucket\",\"s3:GetBucketVersioning\"],\"Resource\":\"arn:aws:s3:::${bucket}\"}]}" \
    --region "$region"

echo "Configuring versioning for bucket"
aws s3api put-bucket-versioning \
    --bucket "${bucket}" \
    --versioning-configuration Status=Enabled \
    --region "$region"

echo "Uploading templates to bucket"
aws s3 cp deploy/templates.zip "s3://${bucket}" --source-region $region --region $region

cd rendered

echo "Uploading master.yaml to bucket"
aws s3 cp master.yaml "s3://${bucket}" --source-region $region --region "$region"

echo "copying templates again for some reason"
aws s3 cp --recursive templates/ "s3://${bucket}/templates" --source-region $region --region "$region"

