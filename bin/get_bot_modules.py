# Takes in the name of a modules yaml file and prints list of bot modules

import yaml
import sys

modules = sys.argv[1]

try:
    y=yaml.load(open(modules))
except Exception as e:
    print("Unable to load yaml file: " + modules)
    print(e)
    sys.exit(1)

if "cobot_modules" in y and y['cobot_modules']:
    for module in y['cobot_modules']:
        if isinstance(module, dict):
            module_name = module.get('name')
        elif isinstance(module, str):
            module_name = module
        print(module_name)