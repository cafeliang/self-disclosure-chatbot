import sys
import os
import json
import subprocess
import argparse
import logging
import shutil

# Root directory to find source codes and store log files.
COBOT_HOME = os.environ.get('COBOT_HOME', os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(COBOT_HOME)

# TODO: Clean up path
#from bin.shared import create_dir, ModuleManager, bcolors
from shared import create_dir, ModuleManager, bcolors
# Directory to store logs from local testing and local docker modules.
LOG_DIRECTORY = os.path.join(COBOT_HOME, 'local_test_logs')

# File to store logs from local testing.
LOCAL_TESTING_LOG_FILE_PATH = os.path.join(LOG_DIRECTORY, 'local_test.log')

create_dir(LOG_DIRECTORY)

# This is a global configuration. Can't move it to a function
logging.basicConfig(
    filename=LOCAL_TESTING_LOG_FILE_PATH,
    filemode='w',
    level=logging.INFO,
    format='[%(levelname)s] %(asctime)s %(filename)s [line %(lineno)d] %(message)s'
)


class LambdaManager(object):
    """ Manage Lambda function life cycle.
    1. Add remote module url to Lambda function environment variable.
    2. Dynamically load lambda handler function
    3. Test handler function
    """

    def __init__(self, module_configs, test_configs):
        self._module_configs = module_configs
        self._test_configs = test_configs
        self._config_environment_variable()
        self._lambda_handler = self._load_handler()

    def test(self, events):
        # Should add service module urls to environment variable at first and then import class from cobot_core
        # Otherwise, lambda handler function can't find service module urls.
        from cobot_python_sdk.event import Event
        from cobot_core.state import State
        # TODO: Clean up path
        #from bin.local_test_validate import validate
        from local_test_validate import validate

        with open(self._test_configs.output_file, 'w') as output_file:
            for event in events:
                event_obj = Event(event)
                request_text = State.extract_text_from_event(event_obj)
                if not request_text:
                    request_text = event_obj.get('request.intent.name', '')
                logging.info('Request text: %s', request_text)
                result = self._lambda_handler(event=event, context={})
                response_text = result['response']['outputSpeech']['ssml'].replace('<speak>', '').replace('</speak>', '').strip()
                logging.info('Response text: %s', response_text)
                output_file.write('ASR: {} \n'.format(request_text))
                output_file.write('TTS: {} \n'.format(response_text))

                if self._test_configs.validate_responses:
                    result = validate(request_text, response_text, event_obj)
                    if not result:
                        exit_code = 1

    def _config_environment_variable(self):
        for k, v in self._module_configs.items():
            if 'url' in v.keys():
                os.environ[k] = v['url']
            elif 'port' in v.keys():
                os.environ[k] = "http://localhost:" + v['port']
        if self._test_configs.api_key:
            os.environ['API_KEY'] = self._test_configs.api_key
        return self

    def _load_handler(self):
        #refer to https://stackoverflow.com/questions/547829/how-to-dynamically-load-a-python-class for more detail
        mod = __import__(self._test_configs.lambda_handler_path, fromlist=['lambda_handler'])
        lambda_handler_method = getattr(mod, 'lambda_handler')
        return lambda_handler_method


def main(cmd_args):
    """ Script entry function
    """
    with open(cmd_args.events_file, 'r') as events_file:
        events=json.load(events_file)
    with open(cmd_args.config_file, 'r') as config_file:
        test_config = json.load(config_file)

    module_manager = ModuleManager(test_config)
    module_manager.start(COBOT_HOME)
    LambdaManager(test_config, cmd_args).test(events)
    module_manager.log(LOG_DIRECTORY).stop()
    print("\nPlease find all log files at:", bcolors.success(LOG_DIRECTORY))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='argument parser')
    parser.add_argument('--lambda_handler_path', required='True',
                        help='Relative Path (relative to AlexaPrizeCobotToolkit package) to your lambda handler class, i.e. sample_cobot.non_trivial_bot')
    parser.add_argument('--events_file', required='True',
                        help='Path to ASK request events json file, i.e. test_event.json')
    parser.add_argument('--config_file', required='True',
                        help='Path to service module json file, i.e. test_service_module_config.json. '
                            'For local service module, it configures each module\'s unique host_port and Dockerfile\'s directory. '
                            'For remote service module, it configures each module\'s url, which can be found from Lambda function\'s environment variables. ')
    parser.add_argument('--output_file', required='True',
                        help='Output conversation txt file for each request in the events_file, it has 2 lines for each request, where one line is ASR text, another line is TTS text. ')

    parser.add_argument('--api_key', required=False, default=None,
                        help='API Key, required to call Alexa Prize Toolkit Service if it\'s not set in the cobot instance')
    parser.add_argument('--validate_responses', dest='validate_responses', required=False, action="store_true", help='Validate responses')
    parser.add_argument('--no_validate_responses', dest='validate_responses', required=False, action="store_false", help='Not validate responses')
    cmd_args = parser.parse_args()

    main(cmd_args)
