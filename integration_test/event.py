import os
import sys

COBOT_HOME = os.environ.get('COBOT_HOME', os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
gunrock_path = os.path.join(COBOT_HOME, 'gunrock_cobot')
sys.path.append(gunrock_path)

from dataclasses import *

app_id = "amzn1.ask.skill.a30b6334-6545-48ce-893c-2d3d7752afbd"


@dataclass
class Session:
    # Non-nullable pseudo field
    userId: InitVar[str]
    # normal fields
    sessionId: str
    new: bool = False
    application: dict = field(default_factory=lambda: {
        "applicationId": app_id
    })
    # Post init fields
    user: dict = field(init=False)

    def __post_init__(self, userId):
        self.user = {"userId": userId}

    def to_json(self):
        return json.dumps(asdict(self))


@dataclass
class Context:
    # Non-nullable pseudo field
    userId: InitVar[str]
    # Post init fields
    System: dict = field(init=False)

    def __post_init__(self, userId):
        user = {"userId": userId}
        application = {
            "applicationId": app_id
        }
        apiEndpoint = "https://api.amazonalexa.com"
        apiAccessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLmEzMGI2MzM0LTY1NDUtNDhjZS04OTNjLTJkM2Q3NzUyYWZiZCIsImV4cCI6MTU2OTAyODAxMSwiaWF0IjoxNTY5MDI3NzExLCJuYmYiOjE1NjkwMjc3MTEsInByaXZhdGVDbGFpbXMiOnsiY29udGV4dCI6IkFBQUFBQUFBQUFEY0tOK1MxZEVicFNVWDl6V3NCdlA3S2dFQUFBQUFBQUQzOElMRWxpb09USy95Vk1QeVZwUXI4RjFOVUVZRy94cmZqODI2eEJiR2w0d1F0NXFNa3dOWjJSTkdtTXVKbEg5MWs3V0lxZ1huTFV4VCswcHFuOEhOU3psbUM3cGtDL2hOS2tlZHF3WXMySGlNNHZiT2RxVEc1a0pSdmdIVE5MSE9GZW9yaFplZUJVL055MkdHSlN6YWRhQ3JBbFB0Qytwc2lTbVBLdkZnVkY5RHA2Z3FBdkZxMzdFTFIxMUU0SSswYS9rN1d2djdiRWVwWnptaTUzcUZ6NHVGRERIMFIxc3FMNlpQdjVjbmJ1MWhRbEtPdWNPbkZuU2RUMEZqUnFaT1c0ZWhkRWhleHl0YWRXUUxmazZubkk3eHo1TTB1V3FRZW1laXJmQkt3UkRLcEZxMzBqR3NiUzBIdzd4aE9tdUlEL2YycTZ2WXNERnVUV1pEckEwc0JROG80dXpLWDNHNFlTSkdSVU1hZktWTHhONGlXTzhSUWpDeTBNazRubmw4d0NxaXZWVFlqdmFmIiwiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUZMN1FBMjdLSTVVM0NQU1U1WkVaRERHRlBBVlVHWVRNSVRMWkpZUlo0WlNVRjNaTDI0QkpRUVJJNURaTFBIVFg3VVpOQlpPR1laT1U1VFBSR1dVUUtDTkhWUkg0NjVUQUtGS0NaQUdLNlhFMklGMlpHWVU3SjJDRzNRN1BKU1NEVFdRUUI2UTNUMktaSVhYUERVNjdQUEg3UjNCSFM3NFFOV1VSWEs0MkJYQlJRUTRPUDJFTSIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFIU1laN0FNWkJONjU0TkVDVkJURlU2VFhLVDVBREVZN0FKNUJBU1E3VEVFSllJREVUVU5MTE03M1VZNERPMktIUFVGRFVYSUU2UE9aTUxaWEdNV1JJUzZSUDQyS1k3U1VDU0tDR0hDNDRFTksyNEdMV1JUVFBEUFdWWURaSEpBM1NVNVg2QUNRSlROWUJTVlAzSUhLWkhVUVlSVTdBSFRVN1c3R0tNS1lOSDZUT0I2QlBIMk9TUFNMS0MzQk80WkhFSU1aV0EzUTJQN1JXWSJ9fQ.Vr3MawyrAx7BNfQa3Oy9lsjAWfKprxsuUNCqNHPTkwL7-VserocWkIYaxjHTVehT9krseWy4nw-QFYzRQUWCss--mVxjIv-d097LHgrn8Qk1vwUPGlke00ZNpI8kgYMs3Hlo1H4ZCD-Xz62mTvrgv17GVdUbf1xkeVwheRW0SQ5attBG_oRsjAxPnePOuSEngzw5TiC4T2oGmx_iSpzTBOVKQY3N9SuljG-XPTEtT1W9LSjFltPYCQoLCi88ARU5utsKy0kyeVIOu8jupTh2HTYeMabpST_lPBUUP_4I440aQzmtYsSd1sR2Gmgl4V1j49TgpGh1aneRXNbHSNrO6Q"

        self.System = {
            "application": application,
            "user": user,
            "apiEndpoint": apiEndpoint,
            "apiAccessToken": apiAccessToken
        }

    def to_json(self):
        return json.dumps(asdict(self))


class RequestType:
    launch_request = "LaunchRequest"
    intent_request = "IntentRequest"


@dataclass
class Request:
    timestamp: str
    text: InitVar[str] = ""
    requestId: str = "testRequestID"
    local: str = "en-US"

    # def __post_init__(self, timestamp, textvalue):
    #     a = 1

    def to_json(self):
        return json.dumps(asdict(self))


@dataclass
class LaunchRequest(Request):
    type: str = RequestType.launch_request
    shouldLinkResultBeReturned: bool = False


@dataclass
class IntentRequest(Request):
    # Non-nullable pseudo field
    text: InitVar[str] = ""

    # normal field
    type: str = RequestType.intent_request
    # post init field
    intent: dict = field(init=False)

    #
    def __post_init__(self, text):
        self.intent = {
            "name": "general",
            "confirmationStatus": "NONE",
            "slots": {
                "text": {
                    "name": "text",
                    "value": text,
                    "confirmationStatus": "NONE",
                    "source": "USER"
                }
            }
        }


@dataclass
class Event:
    # Non-nullable pseudo field
    userId: InitVar[str]
    sessionId: InitVar[str]
    # newSession: InitVar[bool]
    text: InitVar[str] = ""
    # Normal fields
    version: str = "1.0"
    # Post processed fields
    context: Context = field(init=False)

    def __post_init__(self, userId, sessionId, text):
        self.context = Context(userId=userId)

    def to_json(self):
        return json.dumps(asdict(self))


@dataclass
class LaunchEvent(Event):
    # Post processed fields
    session: Session = field(init=False)
    request: Request = field(init=False)

    def __post_init__(self, userId, sessionId, text):
        Event.__post_init__(self, userId, sessionId, text)
        self.session = Session(userId, sessionId, True)
        self.request = LaunchRequest(timestamp="2019-09-21T01:02:16Z")


@dataclass
class IntentEvent(Event):
    # Post processed fields
    session: Session = field(init=False)
    request: Request = field(init=False)

    def __post_init__(self, userId, sessionId, text):
        Event.__post_init__(self, userId, sessionId, text)
        self.session = Session(userId, sessionId, False)
        self.request = IntentRequest(timestamp="2019-09-21T01:02:16Z", text=text)

from dataclasses import *
import json

app_id = "amzn1.ask.skill.a30b6334-6545-48ce-893c-2d3d7752afbd"


@dataclass
class Session:
    # Non-nullable pseudo field
    userId: InitVar[str]
    # normal fields
    sessionId: str
    new: bool = False
    application: dict = field(default_factory=lambda: {
        "applicationId": app_id
    })
    # Post init fields
    user: dict = field(init=False)

    def __post_init__(self, userId):
        self.user = {"userId": userId}

    def to_json(self):
        return json.dumps(asdict(self))


@dataclass
class Context:
    # Non-nullable pseudo field
    userId: InitVar[str]
    # Post init fields
    System: dict = field(init=False)

    def __post_init__(self, userId):
        user = {"userId": userId}
        application = {
            "applicationId": app_id
        }
        apiEndpoint = "https://api.amazonalexa.com"
        apiAccessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLmEzMGI2MzM0LTY1NDUtNDhjZS04OTNjLTJkM2Q3NzUyYWZiZCIsImV4cCI6MTU2OTAyODAxMSwiaWF0IjoxNTY5MDI3NzExLCJuYmYiOjE1NjkwMjc3MTEsInByaXZhdGVDbGFpbXMiOnsiY29udGV4dCI6IkFBQUFBQUFBQUFEY0tOK1MxZEVicFNVWDl6V3NCdlA3S2dFQUFBQUFBQUQzOElMRWxpb09USy95Vk1QeVZwUXI4RjFOVUVZRy94cmZqODI2eEJiR2w0d1F0NXFNa3dOWjJSTkdtTXVKbEg5MWs3V0lxZ1huTFV4VCswcHFuOEhOU3psbUM3cGtDL2hOS2tlZHF3WXMySGlNNHZiT2RxVEc1a0pSdmdIVE5MSE9GZW9yaFplZUJVL055MkdHSlN6YWRhQ3JBbFB0Qytwc2lTbVBLdkZnVkY5RHA2Z3FBdkZxMzdFTFIxMUU0SSswYS9rN1d2djdiRWVwWnptaTUzcUZ6NHVGRERIMFIxc3FMNlpQdjVjbmJ1MWhRbEtPdWNPbkZuU2RUMEZqUnFaT1c0ZWhkRWhleHl0YWRXUUxmazZubkk3eHo1TTB1V3FRZW1laXJmQkt3UkRLcEZxMzBqR3NiUzBIdzd4aE9tdUlEL2YycTZ2WXNERnVUV1pEckEwc0JROG80dXpLWDNHNFlTSkdSVU1hZktWTHhONGlXTzhSUWpDeTBNazRubmw4d0NxaXZWVFlqdmFmIiwiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUZMN1FBMjdLSTVVM0NQU1U1WkVaRERHRlBBVlVHWVRNSVRMWkpZUlo0WlNVRjNaTDI0QkpRUVJJNURaTFBIVFg3VVpOQlpPR1laT1U1VFBSR1dVUUtDTkhWUkg0NjVUQUtGS0NaQUdLNlhFMklGMlpHWVU3SjJDRzNRN1BKU1NEVFdRUUI2UTNUMktaSVhYUERVNjdQUEg3UjNCSFM3NFFOV1VSWEs0MkJYQlJRUTRPUDJFTSIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFIU1laN0FNWkJONjU0TkVDVkJURlU2VFhLVDVBREVZN0FKNUJBU1E3VEVFSllJREVUVU5MTE03M1VZNERPMktIUFVGRFVYSUU2UE9aTUxaWEdNV1JJUzZSUDQyS1k3U1VDU0tDR0hDNDRFTksyNEdMV1JUVFBEUFdWWURaSEpBM1NVNVg2QUNRSlROWUJTVlAzSUhLWkhVUVlSVTdBSFRVN1c3R0tNS1lOSDZUT0I2QlBIMk9TUFNMS0MzQk80WkhFSU1aV0EzUTJQN1JXWSJ9fQ.Vr3MawyrAx7BNfQa3Oy9lsjAWfKprxsuUNCqNHPTkwL7-VserocWkIYaxjHTVehT9krseWy4nw-QFYzRQUWCss--mVxjIv-d097LHgrn8Qk1vwUPGlke00ZNpI8kgYMs3Hlo1H4ZCD-Xz62mTvrgv17GVdUbf1xkeVwheRW0SQ5attBG_oRsjAxPnePOuSEngzw5TiC4T2oGmx_iSpzTBOVKQY3N9SuljG-XPTEtT1W9LSjFltPYCQoLCi88ARU5utsKy0kyeVIOu8jupTh2HTYeMabpST_lPBUUP_4I440aQzmtYsSd1sR2Gmgl4V1j49TgpGh1aneRXNbHSNrO6Q"

        self.System = {
            "application": application,
            "user": user,
            "apiEndpoint": apiEndpoint,
            "apiAccessToken": apiAccessToken
        }

    def to_json(self):
        return json.dumps(asdict(self))


class RequestType:
    launch_request = "LaunchRequest"
    intent_request = "IntentRequest"


@dataclass
class Request:
    timestamp: str
    text: InitVar[str] = ""
    requestId: str = "testRequestID"
    local: str = "en-US"

    # def __post_init__(self, timestamp, textvalue):
    #     a = 1

    def to_json(self):
        return json.dumps(asdict(self))


@dataclass
class LaunchRequest(Request):
    type: str = RequestType.launch_request
    shouldLinkResultBeReturned: bool = False


@dataclass
class IntentRequest(Request):
    # Non-nullable pseudo field
    text: InitVar[str] = ""

    # normal field
    type: str = RequestType.intent_request
    # post init field
    intent: dict = field(init=False)

    #
    def __post_init__(self, text):
        self.intent = {
            "name": "general",
            "confirmationStatus": "NONE",
            "slots": {
                "text": {
                    "name": "text",
                    "value": text,
                    "confirmationStatus": "NONE",
                    "source": "USER"
                }
            }
        }


@dataclass
class Event:
    # Non-nullable pseudo field
    userId: InitVar[str]
    sessionId: InitVar[str]
    # newSession: InitVar[bool]
    text: InitVar[str] = ""
    # Normal fields
    version: str = "1.0"
    # Post processed fields
    context: Context = field(init=False)

    def __post_init__(self, userId, sessionId, text):
        self.context = Context(userId=userId)

    def to_json(self):
        return json.dumps(asdict(self))


@dataclass
class LaunchEvent(Event):
    # Post processed fields
    session: Session = field(init=False)
    request: Request = field(init=False)

    def __post_init__(self, userId, sessionId, text):
        Event.__post_init__(self, userId, sessionId, text)
        self.session = Session(userId, sessionId, True)
        self.request = LaunchRequest(timestamp="2019-09-21T01:02:16Z")


@dataclass
class IntentEvent(Event):
    # Post processed fields
    session: Session = field(init=False)
    request: Request = field(init=False)

    def __post_init__(self, userId, sessionId, text):
        Event.__post_init__(self, userId, sessionId, text)
        self.session = Session(userId, sessionId, False)
        self.request = IntentRequest(timestamp="2019-09-21T01:02:16Z", text=text)


if __name__ == '__main__':
    user_id = "testuserid-2"
    session_id = "testsessionid-2"
    launch_event = LaunchEvent(userId=user_id, sessionId=session_id)

    print(launch_event.to_json())
