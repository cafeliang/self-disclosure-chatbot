import os
from event_generator import EventGenerator
import argparse
import shutil
import logging
import subprocess
from tqdm import tqdm
import sys
from colorama import Fore, Style

COBOT_LOCAL_STRING = "cobot local-test gunrock_cobot.non_trivial_bot {0} bin/test_service_module_config.json {1}"

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler(sys.stdout))

# TODO: Update this later
KEY_Modules = {"Certificate": "certificate", "Selecting Strategy": "select_strategy",
               "Topic Modules": "topic", "Social Modules": "social", "Special Modules": "special"}

# Integration Test Path
INTEGRATION_ROOT = "integration_test"
COBOT_HOME = os.getenv("COBOT_HOME")
LOCAL_TEST_LOG = "local_test_logs"
# Integration Log Path
LOG_ROOT = "integration_log"


def store_local_test_log(test_name):
    test_folder_path = os.path.join(COBOT_HOME, LOG_ROOT, LOCAL_TEST_LOG, test_name)
    if os.path.exists(test_folder_path):
        shutil.rmtree(test_folder_path)
    src = os.path.join(COBOT_HOME, LOCAL_TEST_LOG)
    if os.path.exists(src):
        shutil.copytree(src, test_folder_path)
    else:
        logger.warning("Local test log for {0} is not saved".format(test_name))


def _add_argument(parser):

    parser.add_argument("--input_file_path", type=str, default=None,
                        help="Deprecated.  Use to be compatible with local-test")
    parser.add_argument("--filename", type=str, default=None, help="Name of input file.")
    parser.add_argument("--custom_folder", type=str, default=None, help="Custom Folder Path")

    parser.add_argument("--json_tmp_folder", type=str, default=os.path.join(LOG_ROOT, "tmp_json"),
                        help="Location of json temp folder.")

    parser.add_argument("--output_path", type=str, default=os.path.join(INTEGRATION_ROOT, "results"),
                        help="Store results folder.  Assume the folder is under COBOT_HOME")
    parser.add_argument("--run_section", type=str, default=None,
                        help="Pick one section to run" + "\n".join(KEY_Modules.values()))
    parser.add_argument("--save_json_tmp", type=str, default="True", help="if true, save intermediate json files.")

    parser.add_argument("--no_cobot", type=str, default="False")
    parser.add_argument("--run_all", type=str, default="True")
    parser.add_argument("--include_ignored", type=str, default="False")
    parser.add_argument("--run_local", type=str, default="False")
    parser.add_argument("--keyword", type=str, default=None)

    parser.add_argument("--display", type=str, default="True")
    parser.add_argument("--clear_log", type=str, default="True")
    parser.add_argument("--clear_docker", type=str, default="False")

    parser.add_argument("--verbose", type=str, default="True")
    return


def check_bool(s):
    return s.lower() == "true"


def file_walker(custom_folder, run_all=False, include_ignored=False, suffix=".txt", key_word=None):
    if custom_folder is not None:
        if not custom_folder.startswith(COBOT_HOME):
            root_folder = os.path.join(COBOT_HOME, custom_folder)
        else:
            root_folder = custom_folder
    else:
        root_folder = os.path.join(COBOT_HOME, INTEGRATION_ROOT, "inputs", "text")
    res = [os.path.join(w[0], f) for w in os.walk(root_folder) for f in w[2] if f.endswith(suffix) if (
        run_all or "[CRIT]" in f) and ("[IGNO]" not in f or include_ignored) and (key_word is None or key_word in f)]
    return res


def file_search(filename, custom_folder=None):
    if custom_folder is not None:
        if not custom_folder.startswith(COBOT_HOME):
            root_folder = os.path.join(COBOT_HOME, custom_folder)
        else:
            root_folder = custom_folder
    else:
        root_folder = os.path.join(COBOT_HOME, INTEGRATION_ROOT, "inputs", "text")
    for w in os.walk(root_folder):
        for f in w[2]:
            if f == filename:
                return os.path.join(w[0], f)
    logger.warning("Provided filename -- {0} does not exist in -- {1}".format(filename, custom_folder))
    return None


def run_generator(flags, logged_integration_testfile):
    # Fresh log
    lg = open(logged_integration_testfile, "a+")
    try:
        if check_bool(flags.clear_docker):
            docker_initial = subprocess.call("docker kill $(docker ps -a -q)", stderr=subprocess.DEVNULL, shell=True)

            logger.info("Cleared all docker containers.")
            print("Cleared all docker containers.")
    except subprocess.CalledProcessError:
        logger.info("No docker processes running")

    json_tmp_folder = flags.json_tmp_folder
    json_full_path = os.path.join(COBOT_HOME, json_tmp_folder)

    if not os.path.exists(json_full_path):
        os.mkdir(json_full_path)

    if check_bool(flags.run_local):
        # Get custom path
        output_path = flags.output_path

    else:
        # Get log path
        output_path = os.path.join(LOG_ROOT, "results")
        # docker_initial = subprocess.call("docker system prune -f", stderr=subprocess.DEVNULL, shell=True)

    full_output_path = os.path.join(COBOT_HOME, output_path)
    if not os.path.exists(full_output_path):
        os.mkdir(full_output_path)

    if flags.input_file_path is not None:
        input_file_path = flags.input_file_path
        input_file = input_file_path.split("/")[-1]
        print("Running test for {0}".format(input_file))
        event_generator = EventGenerator(input_file, json_tmp_folder)
        event_generator.generate_events_and_write_to_file(os.path.join(COBOT_HOME, INTEGRATION_ROOT, input_file_path))
        if not check_bool(flags.no_cobot):
            output_file_path = os.path.join(output_path, input_file)
            cmd = COBOT_LOCAL_STRING.format(event_generator.get_generated_file(True), output_file_path)

            cmd = cmd + " >> integration_json_log.txt" if not check_bool(flags.verbose) else cmd

            os.system(cmd)
            lg.write(os.path.join(COBOT_HOME, output_file_path))
            lg.write("\n")
            store_local_test_log(input_file.replace(".txt", ""))

        lg.close()
        return

    input_file = flags.filename
    save_json_tmp = check_bool(flags.save_json_tmp)
    custom_folder = flags.custom_folder
    section = flags.run_section

    if section is not None:
        logger.info("Running test for section {0}".format(section))
        print("Running test for section {0}".format(section))
        custom_folder = os.path.join(COBOT_HOME, INTEGRATION_ROOT, "inputs", "text", section)
        if not os.path.exists(custom_folder):
            os.mkdir(custom_folder)
        output_path = os.path.join(output_path, section)
        if not os.path.exists(output_path):
            os.mkdir(output_path)

    if input_file is not None:
        logger.info("Running test for {0}".format(input_file))
        print("Running test for {0}".format(input_file))
        file_path = file_search(input_file, custom_folder)
        if file_path is None:
            return
        event_generator = EventGenerator(input_file, json_tmp_folder)
        event_generator.generate_events_and_write_to_file(file_path)

        if not check_bool(flags.no_cobot):

            try:
                docker_initial = subprocess.call("docker kill $(docker ps -a -q)",
                                                 stderr=subprocess.DEVNULL, shell=True)
            except subprocess.CalledProcessError:
                logger.info("No docker processes running")
            output_file_path = os.path.join(output_path, input_file)
            cmd = COBOT_LOCAL_STRING.format(event_generator.get_generated_file(True), output_file_path)
            cmd = cmd + " >> integration_json_log.txt" if not check_bool(flags.verbose) else cmd
            os.system(cmd)
            lg.write(output_file_path)
            lg.write("\n")
            store_local_test_log(input_file.replace(".txt", ""))

    else:
        for file_path in tqdm(file_walker(custom_folder, run_all=check_bool(flags.run_all),
                                          include_ignored=check_bool(flags.include_ignored), key_word=flags.keyword)):
            files = file_path.split("/")[-1]
            event_generator = EventGenerator(files, json_tmp_folder)
            event_generator.generate_events_and_write_to_file(file_path)

            if not check_bool(flags.no_cobot):
                print("Running cobot test for {0}".format(files))
                output_file_path = os.path.join(output_path, files)

                try:
                    if check_bool(flags.clear_docker):
                        docker_initial = subprocess.call("docker kill $(docker ps -a -q)",
                                                         stderr=subprocess.DEVNULL, shell=True)
                except subprocess.CalledProcessError:
                    logger.info("No docker processes running")
                cmd = COBOT_LOCAL_STRING.format(event_generator.get_generated_file(True), output_file_path)
                cmd = cmd + " >> integration_json_log.txt" if not check_bool(flags.verbose) else cmd
                os.system(cmd)
                lg.write(output_file_path)
                lg.write("\n")
                store_local_test_log(files.replace(".txt", ""))

    if not save_json_tmp:
        shutil.rmtree(json_tmp_folder)
    lg.close()
    try:
        if check_bool(FLAGS.clear_docker):
            docker_initial = subprocess.call(  # noqa: F841
                "docker kill $(docker ps -a -q)", stderr=subprocess.DEVNULL, shell=True)
            # docker_prune = subprocess.check_output("sudo docker system prune -a --force",
            #                                        stderr=subprocess.STDOUT, shell=True)

        logger.info("Cleared all docker containers.")
    except subprocess.CalledProcessError:
        logger.info("No docker processes running")


def read_file_content(f):
    if not os.path.exists(f):
        print("Test Not Found:  " + f)
    else:
        print(f"=======================\n{f}\n=========================")
        with open(f, "r") as _f:
            for line in _f:
                line = line.strip()
                if line.startswith("ASR:"):
                    print(Fore.LIGHTCYAN_EX + line + Style.RESET_ALL)
                elif line.startswith("TTS:"):
                    print(Fore.LIGHTMAGENTA_EX + line + Style.RESET_ALL)
                else:
                    print(Fore.WHITE + line + Style.RESET_ALL)


def display_integration(log_file):
    if not os.path.exists(log_file):
        print("Integration Test didn't run!.")
        return
    with open(log_file, "r") as log:
        files = log.read().split("\n")
        if len(files) == 0:
            print("No additional files written ")
        for f in files:
            if len(f) > 0:
                read_file_content(os.path.join(COBOT_HOME, f))


if __name__ == '__main__':
    event_parser = argparse.ArgumentParser()

    # Check COBOT HOME
    assert os.getenv("COBOT_HOME") is not None
    assert os.path.exists(COBOT_HOME)

    log_root = os.path.join(COBOT_HOME, LOG_ROOT)
    # Check path
    if not os.path.exists(log_root):
        os.mkdir(log_root)

    local_test_root = os.path.join(log_root, LOCAL_TEST_LOG)
    if not os.path.exists(local_test_root):
        os.mkdir(local_test_root)

    _add_argument(event_parser)

    FLAGS, unparsed = event_parser.parse_known_args()
    LOG_FILE = os.path.join(COBOT_HOME, LOG_ROOT, "_integration_log.txt")
    if check_bool(FLAGS.clear_log):
        if os.path.exists(LOG_FILE):
            os.remove(LOG_FILE)
            open(LOG_FILE, "w+").close()
        print("cleared log")
        if os.path.exists(log_root):
            shutil.rmtree(log_root)
        os.mkdir(log_root)
        if os.path.exists(local_test_root):
            shutil.rmtree(local_test_root)
        os.mkdir(local_test_root)
        
    run_generator(FLAGS, LOG_FILE)
    if check_bool(FLAGS.display):
        display_integration(LOG_FILE)
