import logging
import string
from pathlib import Path


def _setup_logger():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    sh = logging.StreamHandler()
    logger.addHandler(sh)
    return logger


logger = _setup_logger()


games = [
    "assassins creed",
    "fortnite",
    "fortnight",
    "gta",
    # "g t a",
    "grand theft auto",
    "legend of zelda",
]

filename_template = "test_game_nlu_detect_game_batch_by_name_{}.txt"
trans_table = str.maketrans(' ', '_', string.punctuation)


def generate():
    directory = Path(__file__).resolve().parent
    for textfile in directory.glob('*.txt'):
        textfile.unlink()

    for game in games:
        filename = filename_template.format(game.translate(trans_table))
        test_file = directory / filename

        with open(test_file, 'w') as f:
            f.write('\n'.join([
                f"lets talk about the game {game}",
            ]))


if __name__ == '__main__':
    try:
        logger.info(f"Generating... games: {games}")
        generate()
        logger.info(f"Generation complete.")
    except Exception as e:
        logger.error(e)
