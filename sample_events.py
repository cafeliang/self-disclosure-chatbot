new_session_event= {
  "session": {
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.5017db5b-d522-4743-aefe-72dfa50c14fc"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    },
    "new": True
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.a1253d59-419b-4d6f-ba74-779d9775a31a",
    "locale": "en-US",
    "timestamp": "2017-06-08T20:44:15Z",
    "speechRecognition": {
      "hypotheses": [
        {
          "tokens": [
            {
              "confidence": 0.976,
              "value": "What is the weather today in Seattle?"
            }
          ]
        }
      ]
    },
    "intent": {
      "name": "QAIntent",
      "slots": {
        "question_text": {
          "name": "question_text",
          "value": "is the weather today in Seattle?"
        },
        "question_word": {
          "name": "question_word",
          "value": "What"
        }
      }
    }
  },
  "version": "1.0"
}

existing_session_event={
  "session": {
    "new": False,
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745",
      "lastState": "{\"user_id\": \"amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY\", \"conversation_id\": \"2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745\", \"session_id\": \"SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6\", \"intent\": \"RatingIntent\", \"slots\": {\"rating_fraction_number\": {\"name\": \"rating_fraction_number\"}, \"rating\": {\"name\": \"rating\"}, \"rating_fraction\": {\"name\": \"rating_fraction\", \"value\": \"a half\"}}, \"creation_date_time\": \"2017-12-21T20:13:16.451449\", \"nlp\": {}, \"topic\": \"BASELINE Topic\", \"asr\": [{\"confidence\": 0.976, \"value\": \"five\"}], \"response\": \"Local Baseline Response Generator\", \"mode\": null}"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.eb3ecee7-b35f-4df1-9a37-e38e93e5f394",
    "intent": {
      "name": "QAIntent",
      "slots": {
        "question_text": {
          "name": "question_text",
          "value": "is the capitol of france?"
        },
        "question_word": {
          "name": "question_word",
          "value": "What"
        }

      }
    },
    "locale": "en-US",
    "timestamp": "2017-12-10T01:46:55Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
      },
      "user": {
        "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

existing_session_event_name = {
  "session": {
    "new": False,
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745",
      "lastState": "{\"user_id\": \"amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY\", \"conversation_id\": \"2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745\", \"session_id\": \"SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6\", \"intent\": \"RatingIntent\", \"slots\": {\"rating_fraction_number\": {\"name\": \"rating_fraction_number\"}, \"rating\": {\"name\": \"rating\"}, \"rating_fraction\": {\"name\": \"rating_fraction\", \"value\": \"a half\"}}, \"creation_date_time\": \"2017-12-21T20:13:16.451449\", \"nlp\": {}, \"topic\": \"BASELINE Topic\", \"asr\": [{\"confidence\": 0.976, \"value\": \"five\"}], \"response\": \"Local Baseline Response Generator\", \"mode\": null}"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    }
  },
  "request": {
      "type": "IntentRequest",
      "requestId": "EdwRequestId.eb3ecee7-b35f-4df1-9a37-e38e93e5f394",
      "intent": {
        "name": "FirstNameIntent",
        "slots": {
            "name_text": {
                "name": "name_text",
                "value": "Arbit"
            }
        }
    },
    "locale": "en-US",
    "timestamp": "2017-12-10T01:45:55Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
      },
      "user": {
        "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

existing_session_event_health_prob = {
  "session": {
    "new": False,
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745",
      "lastState": "{\"user_id\": \"amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY\", \"conversation_id\": \"2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745\", \"session_id\": \"SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6\", \"intent\": \"RatingIntent\", \"slots\": {\"rating_fraction_number\": {\"name\": \"rating_fraction_number\"}, \"rating\": {\"name\": \"rating\"}, \"rating_fraction\": {\"name\": \"rating_fraction\", \"value\": \"a half\"}}, \"creation_date_time\": \"2017-12-21T20:13:16.451449\", \"nlp\": {}, \"topic\": \"BASELINE Topic\", \"asr\": [{\"confidence\": 0.976, \"value\": \"five\"}], \"response\": \"Local Baseline Response Generator\", \"mode\": null}"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    }
  },
  "request": {
      "type": "IntentRequest",
      "requestId": "EdwRequestId.eb3ecee7-b35f-4df1-9a37-e38e93e5f394",
      "intent": {
        "name": "health_problem",
        "slots": {
            "health_word": {
                "name": "health_word",
                "value": "I am"
            },
            "health_symptom": {
                "name": "health_symptom",
                "value": "anxiety"
            }
        }
    },
    "locale": "en-US",
    "timestamp": "2017-12-10T01:46:55Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
      },
      "user": {
        "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

existing_session_event_health = {
  "session": {
    "new": False,
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745",
      "lastState": "{\"user_id\": \"amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY\", \"conversation_id\": \"2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745\", \"session_id\": \"SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6\", \"intent\": \"RatingIntent\", \"slots\": {\"rating_fraction_number\": {\"name\": \"rating_fraction_number\"}, \"rating\": {\"name\": \"rating\"}, \"rating_fraction\": {\"name\": \"rating_fraction\", \"value\": \"a half\"}}, \"creation_date_time\": \"2017-12-21T20:13:16.451449\", \"nlp\": {}, \"topic\": \"BASELINE Topic\", \"asr\": [{\"confidence\": 0.976, \"value\": \"five\"}], \"response\": \"Local Baseline Response Generator\", \"mode\": null}"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    }
  },
  "request": {
      "type": "IntentRequest",
      "requestId": "EdwRequestId.eb3ecee7-b35f-4df1-9a37-e38e93e5f394",
      "intent": {
        "name": "health_request",
        "slots": {
            "all_text": {
                "name": "all_text",
                "value": "I am not felling well"
            }
        }
    },
    "locale": "en-US",
    "timestamp": "2017-12-10T01:46:55Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
      },
      "user": {
        "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

cannot_play_music_event= {
  "session": {
    "new": False,
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745",
      "lastState": "{\"user_id\": \"amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY\", \"conversation_id\": \"2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745\", \"session_id\": \"SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6\", \"intent\": \"RatingIntent\", \"slots\": {\"rating_fraction_number\": {\"name\": \"rating_fraction_number\"}, \"rating\": {\"name\": \"rating\"}, \"rating_fraction\": {\"name\": \"rating_fraction\", \"value\": \"a half\"}}, \"creation_date_time\": \"2017-12-21T20:13:16.451449\", \"nlp\": {}, \"topic\": \"BASELINE Topic\", \"asr\": [{\"confidence\": 0.976, \"value\": \"five\"}], \"response\": \"Local Baseline Response Generator\", \"mode\": null}"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    }
  },
  "request": {
      "type": "IntentRequest",
      "requestId": "EdwRequestId.eb3ecee7-b35f-4df1-9a37-e38e93e5f394",
      "intent": {
        "name": "cannot_play_music",
        "slots": {
            "all_text": {
                "name": "all_text",
                "value": "Play music"
            }
        }
    },
    "locale": "en-US",
    "timestamp": "2017-12-10T01:46:55Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
      },
      "user": {
        "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

retrieval_event={
  "session": {
    "new": False,
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745",
      "lastState": "{\"user_id\": \"amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY\", \"conversation_id\": \"2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745\", \"session_id\": \"SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6\", \"intent\": \"RatingIntent\", \"slots\": {\"rating_fraction_number\": {\"name\": \"rating_fraction_number\"}, \"rating\": {\"name\": \"rating\"}, \"rating_fraction\": {\"name\": \"rating_fraction\", \"value\": \"a half\"}}, \"creation_date_time\": \"2017-12-21T20:13:16.451449\", \"nlp\": {}, \"topic\": \"BASELINE Topic\", \"asr\": [{\"confidence\": 0.976, \"value\": \"five\"}], \"response\": \"Local Baseline Response Generator\", \"mode\": null}"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.eb3ecee7-b35f-4df1-9a37-e38e93e5f394",
    "intent": {
      "name": "retrieval",
      "slots": {
        "text": {
          "name": "text",
          "value": "what is your opinion for Roger Federer?"
        }
      }
    },
    "locale": "en-US",
    "timestamp": "2017-12-10T01:46:55Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
      },
      "user": {
        "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

backstory_event={
  "session": {
    "new": False,
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745",
      "lastState": "{\"user_id\": \"amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY\", \"conversation_id\": \"2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745\", \"session_id\": \"SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6\", \"intent\": \"RatingIntent\", \"slots\": {\"rating_fraction_number\": {\"name\": \"rating_fraction_number\"}, \"rating\": {\"name\": \"rating\"}, \"rating_fraction\": {\"name\": \"rating_fraction\", \"value\": \"a half\"}}, \"creation_date_time\": \"2017-12-21T20:13:16.451449\", \"nlp\": {}, \"topic\": \"BASELINE Topic\", \"asr\": [{\"confidence\": 0.976, \"value\": \"five\"}], \"response\": \"Local Baseline Response Generator\", \"mode\": null}"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.eb3ecee7-b35f-4df1-9a37-e38e93e5f394",
    "intent": {
      "name": "backstory",
      "slots": {
        "all_text": {
          "name": "all_text",
          "value": "what is your name?"
        }
      }
    },
    "locale": "en-US",
    "timestamp": "2017-12-10T01:46:55Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
      },
      "user": {
        "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

offensive_event={
  "session": {
    "new": False,
    "sessionId": "SessionId.c9b48519-60ce-4898-a224-9b4a2f7659d0",
    "application": {
      "applicationId": "amzn1.ask.skill.62691bcf-1858-43d0-a667-b3f89c584417"
    },
    "attributes": {
      "lastState": "{\"user_id\": \"amzn1.ask.account.AH3WVAEUEHZCPMQKXZRMLPWRH5LCWY4A332N4BM6MYGNLXKL7YC5SL75BSS6GSJFYNUXNZZYL5WUC7LVJ53VQ5T3FIXAFUOPFGNRDFVE4RDBUAFSBEROEPOYTNFHGJLOULAYZ2BEA24T5FCQA44JTTBSB2PAGGKSBWRAI33DRJFD6SNX5CUIXTFX4EBPXWPKDB3UQV2WLPBITGQ\", \"conversation_id\": null, \"session_id\": \"SessionId.c9b48519-60ce-4898-a224-9b4a2f7659d0\", \"intent\": \"topic_request\", \"slots\": {\"topic\": {\"name\": \"topic\", \"value\": \"sex\"}}, \"creation_date_time\": \"2018-02-09T01:40:19.303980\", \"topic\": \"Sex\", \"asr\": [], \"text\": \"sex\", \"response\": \"Sure, I know a few things about sex. Let's talk about that.\", \"mode\": null, \"input_offensive\": 0, \"ner\": [], \"sentiment\": \"neu\", \"features\": {\"intent\": \"topic_request\", \"topic\": \"Sex\", \"sentiment\": \"neu\", \"ner\": []}, \"candidate_responses\": {\"RULES\": {\"response\": \"Sure, I know a few things about sex. Let's talk about that.\", \"performance\": [0.006516456604003906], \"error\": false}}}"
    },
    "user": {
      "userId": "amzn1.ask.account.AH3WVAEUEHZCPMQKXZRMLPWRH5LCWY4A332N4BM6MYGNLXKL7YC5SL75BSS6GSJFYNUXNZZYL5WUC7LVJ53VQ5T3FIXAFUOPFGNRDFVE4RDBUAFSBEROEPOYTNFHGJLOULAYZ2BEA24T5FCQA44JTTBSB2PAGGKSBWRAI33DRJFD6SNX5CUIXTFX4EBPXWPKDB3UQV2WLPBITGQ"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.5527349a-259c-4b2e-81a9-b1a9c9e583f0",
    "intent": {
      "name": "topic_request",
      "slots": {
        "topic": {
          "name": "topic",
          "value": "kill yourself"
        }
      }
    },
    "locale": "en-US",
    "timestamp": "2018-02-09T01:41:08Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.62691bcf-1858-43d0-a667-b3f89c584417"
      },
      "user": {
        "userId": "amzn1.ask.account.AH3WVAEUEHZCPMQKXZRMLPWRH5LCWY4A332N4BM6MYGNLXKL7YC5SL75BSS6GSJFYNUXNZZYL5WUC7LVJ53VQ5T3FIXAFUOPFGNRDFVE4RDBUAFSBEROEPOYTNFHGJLOULAYZ2BEA24T5FCQA44JTTBSB2PAGGKSBWRAI33DRJFD6SNX5CUIXTFX4EBPXWPKDB3UQV2WLPBITGQ"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

offensive_event_custom={
  "session": {
    "new": False,
    "sessionId": "SessionId.c9b48519-60ce-4898-a224-9b4a2f7659d0",
    "application": {
      "applicationId": "amzn1.ask.skill.62691bcf-1858-43d0-a667-b3f89c584417"
    },
    "attributes": {
      "lastState": "{\"user_id\": \"amzn1.ask.account.AH3WVAEUEHZCPMQKXZRMLPWRH5LCWY4A332N4BM6MYGNLXKL7YC5SL75BSS6GSJFYNUXNZZYL5WUC7LVJ53VQ5T3FIXAFUOPFGNRDFVE4RDBUAFSBEROEPOYTNFHGJLOULAYZ2BEA24T5FCQA44JTTBSB2PAGGKSBWRAI33DRJFD6SNX5CUIXTFX4EBPXWPKDB3UQV2WLPBITGQ\", \"conversation_id\": null, \"session_id\": \"SessionId.c9b48519-60ce-4898-a224-9b4a2f7659d0\", \"intent\": \"topic_request\", \"slots\": {\"topic\": {\"name\": \"topic\", \"value\": \"sex\"}}, \"creation_date_time\": \"2018-02-09T01:40:19.303980\", \"topic\": \"Sex\", \"asr\": [], \"text\": \"sex\", \"response\": \"Sure, I know a few things about sex. Let's talk about that.\", \"mode\": null, \"input_offensive\": 0, \"ner\": [], \"sentiment\": \"neu\", \"features\": {\"intent\": \"topic_request\", \"topic\": \"Sex\", \"sentiment\": \"neu\", \"ner\": []}, \"candidate_responses\": {\"RULES\": {\"response\": \"Sure, I know a few things about sex. Let's talk about that.\", \"performance\": [0.006516456604003906], \"error\": false}}}"
    },
    "user": {
      "userId": "amzn1.ask.account.AH3WVAEUEHZCPMQKXZRMLPWRH5LCWY4A332N4BM6MYGNLXKL7YC5SL75BSS6GSJFYNUXNZZYL5WUC7LVJ53VQ5T3FIXAFUOPFGNRDFVE4RDBUAFSBEROEPOYTNFHGJLOULAYZ2BEA24T5FCQA44JTTBSB2PAGGKSBWRAI33DRJFD6SNX5CUIXTFX4EBPXWPKDB3UQV2WLPBITGQ"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.5527349a-259c-4b2e-81a9-b1a9c9e583f0",
    "intent": {
      "name": "topic_request",
      "slots": {
        "topic": {
          "name": "topic",
          "value": "How can I kill my wife"
        }
      }
    },
    "locale": "en-US",
    "timestamp": "2018-02-09T01:41:08Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.62691bcf-1858-43d0-a667-b3f89c584417"
      },
      "user": {
        "userId": "amzn1.ask.account.AH3WVAEUEHZCPMQKXZRMLPWRH5LCWY4A332N4BM6MYGNLXKL7YC5SL75BSS6GSJFYNUXNZZYL5WUC7LVJ53VQ5T3FIXAFUOPFGNRDFVE4RDBUAFSBEROEPOYTNFHGJLOULAYZ2BEA24T5FCQA44JTTBSB2PAGGKSBWRAI33DRJFD6SNX5CUIXTFX4EBPXWPKDB3UQV2WLPBITGQ"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

weather_session_event= {
  "session": {
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.5017db5b-d522-4743-aefe-72dfa50c14fc"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    },
    "new": True
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.a1253d59-419b-4d6f-ba74-779d9775a31a",
    "locale": "en-US",
    "timestamp": "2017-06-08T20:44:15Z",
    "intent": {
      "name": "weather_request",
      "slots": {
        "get_weather": {
          "name": "get_weather",
          "value": "weather"
        },
        "get_location": {
          "name": "get_location",
          "value": "Seattle"
        },
        "get_day": {
          "name": "get_day",
          "value": "today"
        }
      }
    }
  },
  "version": "1.0"
}


existing_session_event_news={
  "session": {
    "new": False,
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745",
      "lastState": "{\"user_id\": \"amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY\", \"conversation_id\": \"2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745\", \"session_id\": \"SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6\", \"intent\": \"RatingIntent\", \"slots\": {\"rating_fraction_number\": {\"name\": \"rating_fraction_number\"}, \"rating\": {\"name\": \"rating\"}, \"rating_fraction\": {\"name\": \"rating_fraction\", \"value\": \"a half\"}}, \"creation_date_time\": \"2017-12-21T20:13:16.451449\", \"nlp\": {}, \"topic\": \"BASELINE Topic\", \"asr\": [{\"confidence\": 0.976, \"value\": \"five\"}], \"response\": \"Local Baseline Response Generator\", \"mode\": null}"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.eb3ecee7-b35f-4df1-9a37-e38e93e5f394",
    "intent": {
      "name": "news",
      "slots": {
        "topic": {
          "name": "topic",
          "value": "apple"
        }
      }
    },
    "locale": "en-US",
    "timestamp": "2017-12-10T01:46:55Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
      },
      "user": {
        "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}
