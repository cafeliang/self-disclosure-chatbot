import logging
import os
import os.path
import shutil
from pathlib import Path
from pprint import pformat
from typing import List


logger = logging.getLogger(__name__)

# check script is run in the right dir
if os.path.basename(os.getcwd()) == 'gunrock_cobot':
    working_dir = Path('.')
elif os.path.basename(os.getcwd()) == 'cobot_base':
    working_dir = Path('gunrock_cobot')
else:
    raise IOError("template_manager preprocess only works under '<git_root>' or 'gunrock_cobot/'.")

FILES_TO_COPY = {
    working_dir / 'nlu/util_nlu.py',
    working_dir / 'nlu/utils',
    working_dir / 'nlu/intentmap_scheme.py',
    working_dir / 'nlu/profanity_regex.py',
    working_dir / 'nlu/dataclass',
    working_dir / 'nlu/nlu_metadata',
    working_dir / 'nlu/constants.py',
    working_dir / 'nlu/question_detector.py',
    working_dir / 'nlu/command_detector.py',
    working_dir / 'nlu/stopwords.py',
    working_dir / 'nlg/blender.py',
    working_dir / 'classproperty.py',
    working_dir / 'response_templates/template_data.py',
    working_dir / 'template_manager',
    working_dir / 'user_profiler',
    working_dir / 'user_attribute_adaptor.py',
    working_dir / 'selecting_strategy/module_selection.py',
    working_dir / 'selecting_strategy/topic_mapping.py',
    working_dir / 'util_redis.py',
    working_dir / 'error_handling_utils.py',
    working_dir / 'abtest_additional_config.py',
    working_dir / 'backstory_client.py',
    working_dir / 'classproperty.py',
    working_dir / 'constants_api_keys.py',
    working_dir / 'acknowledgement/acknowledgement_utils.py',
    working_dir / 'question_handling',
    working_dir / 'nlg_post_process/profanity_classifier.py',
    working_dir / 'nlg_post_process/profanity_list.txt'
}

DISABLED_MODULES = {'fooddrink', 'gender', 'greeter', 'news_deprecated', 'retrieval', 'social'}


# MARK: - Copying to Docker

def copy_sys_to_docker(only: List[str] = None, exclude: List[str] = None):

    # get list of remote modules to write to
    remote_modules = set(only) if only else set(m.name for m in (working_dir).glob('docker/*'))
    remote_modules -= set(['.DS_Store'])
    remote_modules -= DISABLED_MODULES
    if exclude:
        remote_modules -= set(exclude)

    logger.info(pformat(f"[BEGIN] Copying system files to:{remote_modules}"))

    # get items to copy
    logger.info(pformat(f"System files to copy:{FILES_TO_COPY}"))

    for module in remote_modules:
        docker_dir = working_dir / 'docker' / module / 'app'
        for files in FILES_TO_COPY:
            dest = (docker_dir / files)
            if dest.exists() and not files.is_file():
                shutil.rmtree(dest)
            if files.is_file():
                if not os.path.exists(dest) and not os.path.exists(dest.parent):
                    os.mkdir(dest.parent)
                shutil.copy(files.as_posix(), dest.as_posix())
            else:
                shutil.copytree(files.as_posix(), dest.as_posix())
        logger.info(f"Copied to: {module}")

    logger.info("[SUCCESS] System files copy complete")


def main(only: List[str], exclude: List[str]):

    logger.info('[BEGIN] running docker_hook')
    copy_sys_to_docker(only=only, exclude=exclude)
    logger.info('[SUCCESS] docker_hook complete')
