import logging
import re
import traceback
from transitions import Machine
from movietv_utils import *
from movie_module_regex import *
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from nlu.constants import DialogAct, Positivity


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('transitions').setLevel(logging.DEBUG)

SIZE_TRANSITION_HISTORY = 5

def set_movie_dialog_init_context(context, movie_info):
    context.current_subdialog_context = {
        "current_state": "s_init",
        "questions_asked": 0,
        "trivia_used": 0,
        "movie_info": movie_info,
        "curr_q_num": None,
        "q_key": "q",
        "transition_history": ["r_t0"] * SIZE_TRANSITION_HISTORY,
        "chitchat_mode": "qa_mode",
        "chitchat_context": [],
        "tags": [],
    }

def save_movie_dialog_current_context(context, movie_info):
    context.movies[str(movie_info["movie_id"])] = {
        "current_subdialog_context": context.current_subdialog_context,
    }

class MovieDialog(Machine):
    # High level specification of states, state transitions and transition
    # conditions
    def __init__(self, automaton):
        # Each state has prefix 's_'
        self.states = [
            's_init',  # main entry point
            's_chitchat',
            's_exit',
        ]
        # Each state has a corresponding transtion function that chooses the next state depending on certain conditions
        # There are different ways to define transitions. I'm defining arguments using a list.
        # Args ordering: transition_name, source, dest, conditions=None,
        # unless=None, before=None, after=None, prepare=None
        self.transitions = [
            # system
            ['transduce', '*', 's_exit',
                ['c_no_questions_left', 'c_no_trivia_left', 'r_t1']],
            # s_init
            ['transduce', 's_init', 's_chitchat', [
                'r_t2']],  # first time entry point
            # s_chitchat
            ['transduce', 's_chitchat', '=', ['r_t3']],
        ]

        self.automaton = automaton
        self.text = automaton.text
        self.utt = automaton.utt
        self.context = automaton.context
        self.movie_info = self.context.current_subdialog_context["movie_info"]
        self.tags = []
        # resume
        if str(self.movie_info["movie_id"]
               ) in self.context.movies and self.context.current_subdialog_context["current_state"] == "s_init" and "current_subdialog_context" in self.context.movies[str(self.movie_info["movie_id"])]:
            #print("self.context.movies", self.context.movies)
            self.tags.append("c_resuming_dialog")
            self.context.current_subdialog_context = self.context.movies[str(
                self.movie_info["movie_id"])]["current_subdialog_context"]
        self.current_context = self.context.current_subdialog_context

        # if self.current_context["chitchat_mode"] == "trivia_mode":
        self.facts = get_imdb_trivia_new(self.movie_info["movie_id"])
        if self.facts is None:
            self.facts = get_imdb_trivia(self.movie_info["movie_id"])

        # print ("IMDB trivia: {}".format(self.facts))

        # set context info at init
        if self.current_context["current_state"] == "s_init":
            if self.facts is not None:
                MAX_TRIVIA_COUNT = 2
                self.facts = self.facts[:MAX_TRIVIA_COUNT]
                self.current_context["num_total_trivia"] = len(self.facts)
            else:
                self.current_context["num_total_trivia"] = 0

        MAX_QUESTION_COUNT = 3
        if self.current_context["questions_asked"] >= MAX_QUESTION_COUNT:
            self.tags.append("c_no_questions_left")
        if self.current_context["trivia_used"] >= self.current_context["num_total_trivia"]:
            self.tags.append("c_no_trivia_left")

        Machine.__init__(
            self,
            states=self.states,
            transitions=self.transitions,
            initial=self.current_context["current_state"],
            prepare_event='prepare',
            finalize_event='finalize',
            send_event=True)

        #self.add_transition('init', 'solid', 'liquid')

    def get_response(self, input_data):
        print("current_context - beginning get_response:", self.current_context)
        self.input_data = input_data
        self.features = input_data["features"][0]
        self.returnnlp = ReturnNLP(input_data['returnnlp'][0])

        self.text = input_data["text"][0]
        self.dialog_act = input_data["features"][0]["dialog_act"]
        self.amz_dialog_act = input_data["features"][0].get("amz_dialog_act")
        self.ner = input_data["features"][0]["ner"]
        self.asr_correction = input_data["features"][0]["asrcorrection"]
        self.knowledge = input_data["features"][0]["knowledge"]
        self.topic_keywords = input_data["features"][0]["topic_keywords"]
        self.sentiment = input_data["features"][0]["sentiment"]
        self.cobot_intents = input_data["features"][0]["intent_classify"]["lexical"] + \
            input_data["features"][0]["intent_classify"]["topic"]
        self.old_state = self.state
        self.cache = {}  # for caching conditions results if necessary
        self.r_ack = ""
        self.r_body = ""
        self.r_question = ""
        self.transition_history = self.current_context["transition_history"]

        self.transduce()

        self.new_state = self.state
        self.current_context["current_state"] = self.state
        self.current_context["old_state"] = self.old_state
        self.current_context["transition_history"] = self.transition_history
        self.current_context["tags"] = self.tags
        save_movie_dialog_current_context(self.context, self.movie_info)
        self.final_response = self.r_ack + self.r_body + self.r_question
        # print("current_context - end get_response:", self.current_context)
        return self.final_response

    # on_enter states: Most of the logic for each state to generate the response is defined here
    # Every state has a corresponding callback here
    # =======================================================

    def on_enter_s_chitchat(self, event):
        # print("We've just entered state s_chitchat")

        self.set_ack()
        self.set_body()
        self.set_next_question()

    def on_enter_s_exit(self, event):
        # print("We've just entered state s_exit")
        # default response; should be overwritten upon returning
        self.r_question = self.utt(["got_distracted"])

    def set_chitchat_context(self):
        if "chitchat_context" not in self.current_context: # may not exist in old data format
            self.current_context["chitchat_context"] = []

        q_key = self.current_context.get("q_key", "q")
        if re.search(r"(didn't|did not|never|haven't|have not|yet to).* (\bwatch|\bsee(n|)\b|\bsaw\b).* (\bit\b|\bone\b|movie|yet)", self.text) and q_key != "q_not_seen":
            self.current_context["chitchat_context"].append("movie_not_watched")

        # if "qa_filter_when_where" in self.current_context.get("chitchat_context", {}):
        #     self.current_context["chitchat_context"].remove("qa_filter_when_where")         
        # elif "q2_question" in self.current_context["tags"] and "q" in self.current_context["q_key"]:
        #     self.current_context["chitchat_context"].append("qa_filter_when_where")

    def set_ack(self):
        # print("ack chitchat_mode:", self.current_context["chitchat_mode"])
        if self.current_context["chitchat_mode"] == "trivia_mode":
            self.r_ack = ""  # rely on system ack for now
        elif self.current_context["chitchat_mode"] == "qa_mode" and self.current_context["curr_q_num"] is not None:
            ack_bot = MovieAcknowledgeBot(self)
            self.tags.append(self.current_context["curr_q_num"] + "_ack")
            self.r_ack = ack_bot.get_ack(self.current_context["curr_q_num"])
        # overwrite if interrupted by question
        if "movie_user_qa_answer_only" in self.automaton.moviechat_user["tags"]:
            self.r_ack = self.utt(["so_about_movie"], {"movie_name":self.movie_info["movie_name"]})
        if "c_resuming_dialog" in self.tags:
            self.tags.append("resume_ack")
            self.r_ack = self.utt(["movie_dialog", "resume_movie_ack"], {
                                  "movie_title": self.movie_info["movie_name"]})
            #self.r_ack = "I think we were talking about {} before, but we didn't discuss this last time. ".format(self.movie_info["movie_name"])

    def set_body(self):
        '''
        if self.current_context["questions_asked"] % 2 == 0 and self.current_context["questions_asked"] != 0 and self.current_context["chitchat_mode"] == "qa_mode" and "c_no_trivia_left" not in self.tags:
            self.current_context["chitchat_mode"] = "trivia_mode"
        elif self.current_context["trivia_used"] % 2 == 0 and self.current_context["trivia_used"] != 0 and self.current_context["chitchat_mode"] == "trivia_mode" and "c_no_questions_left" not in self.tags:
            self.current_context["chitchat_mode"] = "qa_mode"
        '''

        self.current_context["chitchat_mode"] = self.get_body_chitchat_mode(self.current_context, self.tags)
        print("body chitchat_mode:", self.current_context["chitchat_mode"])

        if self.current_context["chitchat_mode"] == "trivia_mode":
            self.r_body = self.utt(["movie_dialog", "trivia_body"], {
                                   "trivia": self.facts[self.current_context["trivia_used"]]})
            self.current_context["trivia_used"] += 1

    def get_body_chitchat_mode(self, current_context, tags):
        """
        :return: qa_mode or trivia_mode
        """
        if current_context["questions_asked"] != 0 and current_context[
                "chitchat_mode"] == "qa_mode" and "c_no_trivia_left" not in tags:
            return "trivia_mode"
        if current_context["trivia_used"] != 0 and current_context["chitchat_mode"] == "trivia_mode" and "c_no_questions_left" not in self.tags:
            return "qa_mode"
        return current_context["chitchat_mode"]

    """
    Implement stopping question here if we detect user gets bored.
    """
    def set_next_question(self):
        if self.current_context["chitchat_mode"] == "qa_mode":
            if self.current_context["curr_q_num"] is None:
                self.current_context["curr_q_num"] = "q1" # here we ask the question
            else:
                self.current_context["curr_q_num"] = "q" + \
                    str(int(self.current_context["curr_q_num"][-1]) + 1)

            self.tags.append(self.current_context["curr_q_num"] + "_question")
            # overwrite in MovieAcknowledgeBot
            try:
                q_key = self.current_context.get("q_key", "q")
                self.tags.append("q_key_" + q_key)
                if self.current_context.get("qa_template_params") is not None:
                    self.r_question = self.utt(["movie_dialog", "movie_question_bank", self.current_context["curr_q_num"], q_key], self.current_context["qa_template_params"])
                    self.current_context["qa_template_params"] = None
                else:
                    self.r_question = self.utt(["movie_dialog", "movie_question_bank", self.current_context["curr_q_num"], q_key])
            except: # old data format incompat
                q_key = "q"
                self.current_context["q_key"] = q_key
                self.tags.append("q_key_" + q_key)      
                self.r_question = self.utt(["movie_dialog", "movie_question_bank", self.current_context["curr_q_num"], q_key])
                print("[MOVIECHAT_MODULE] Exception in set_next_question {}".format(traceback.format_exc()))   
            self.current_context["questions_asked"] += 1
        elif self.current_context["chitchat_mode"] == "trivia_mode":
            self.r_question = self.utt(["movie_dialog", "trivia_question"])

    # System level callbacks
    # =======================================================
    def prepare(self, event):
        return True

    def finalize(self, event):
        return True

    # Conditions
    # These callback functions cache their result before they may be checked multiple times
    # Depending on the function, they may have to cache multiple values, e.g. c_entity_detected
    # If conditions callbacks interact with user_attributes, remember to reset fields where necessary
    # ==========================================================
    def c_resuming_dialog(self, event):
        if "c_resuming_dialog" in self.tags:
            return True

        return False

    def c_no_questions_left(self, event):
        if "c_no_questions_left" in self.tags:
            return True

        return False

    def c_no_trivia_left(self, event):
        # temp
        if "c_no_trivia_left" in self.tags:
            return True

        return False

    def r_t1(self, event):
        return self.update_transition_history("r_t1")

    def r_t2(self, event):
        return self.update_transition_history("r_t2")

    def r_t3(self, event):
        return self.update_transition_history("r_t3")

    def update_transition_history(self, transition_tag):
        self.transition_history.append(transition_tag)
        self.transition_history.pop(0)
        return True

class MovieAcknowledgeBot:
    def __init__(self, fsm):
        self.fsm = fsm
        self.utt = fsm.utt
        self.text = fsm.text
        self.cobot_intents = fsm.cobot_intents
        self.movie_info = self.fsm.movie_info
        self.sentiment = fsm.sentiment
        self.context = self.fsm.context
        self.current_context = self.context.current_subdialog_context
        self.questions_mapping = {
            "q1": self.get_q1_ack,
            "q2": self.get_q2_ack,
            "q3": self.get_q3_ack,
            "q4": self.get_q4_ack,
        }

    def get_ack(self, q_num):
        return self.questions_mapping[q_num]()

    def get_q1_ack(self):
        if self.not_watched():
            ack = self.utt(["movie_dialog", "movie_question_bank", "q1", "ack_not_seen"])
            self.current_context["q_key"] = "q_not_seen"
        elif self.is_sad_answer():
            ack = self.utt(["movie_dialog", "movie_question_bank", "q1", "ack_sad"])
            self.current_context["q_key"] = "q_7-8_rating"  # todo: use separaet flow?
        elif self.is_highly_positive_answer():
            ack = self.utt(["movie_dialog", "movie_question_bank", "q1", "ack_10_rating"])
            self.current_context["q_key"] = "q_9-10_rating"
        elif self.is_positive_answer():
            ack = self.utt(["movie_dialog", "movie_question_bank", "q1", "ack_7-9_rating"])
            self.current_context["q_key"] = "q_7-8_rating"
        elif self.is_negative_answer():
            ack = self.utt(["movie_dialog", "movie_question_bank", "q1", "ack_1-3_rating"])
            self.current_context["q_key"] = "q_1-3_rating"
        elif self.is_neutral_answer():
            ack = self.utt(["movie_dialog", "movie_question_bank", "q1", "ack_4-6_rating"])
            self.current_context["q_key"] = "q_4-6_rating"
        elif self.is_unknown_answer():
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])
            self.current_context["q_key"] = "q"
        else:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q1", "ack_no_rating"])
            self.current_context["q_key"] = "q"

        return ack

    def not_watched(self):
        return "movie_not_watched" in self.current_context.get("chitchat_context", {})

    def is_highly_positive_answer(self):
        regex_highly_positive = r"^(?!.*(not|never|doesn't|don't)).*(super|great|fantastic|fascinating|amazing|marvelous|wonderful|awesome|exceptional|outstanding|exciting|favorite|best|masterpiece|lov(e|ed|ing)|enjoy)(.*)"

        return re.search(regex_highly_positive, self.text)

    def is_positive_answer(self):
        regex_positive = r"^(?!.*(not|never|doesn't|don't|didn't)).*(like|nice|good|like|amusing|hilarious|entertaining|fun)(.*)"
        regex_positive_blacklist = r"^.*\bok(ay)?\b"
        return not re.search(regex_positive_blacklist, self.text) and \
               self.fsm.returnnlp.answer_positivity == Positivity.pos or \
               self.fsm.returnnlp.has_intent("opinion_positive") or \
               re.search(regex_positive, self.text)


    def is_neutral_answer(self):
        regex_neutral = r"^.*\bok(ay)?\b"
        return self.sentiment == "neu" or re.search(regex_neutral, self.text)

    def is_sad_answer(self):
        return re.search(sad, self.text)

    def is_negative_answer(self):
        regex_negative = r"^(?!.*(not|never|doesn't|don't)).*(poor|hate)(.*)"

        return self.fsm.returnnlp.answer_positivity == Positivity.neg or \
               self.fsm.returnnlp.has_intent("opinion_negative") or \
               re.search(regex_negative, self.text)

    def is_unknown_answer(self):
        return "ans_unknown" in self.cobot_intents

    def get_q2_ack(self):
        user_utt_length = len(self.text.strip().split())

        q_key = self.current_context["q_key"]
        if "movie_not_watched" in self.current_context.get("chitchat_context", {}):
            ack = self.utt(["movie_dialog", "movie_question_bank", "q2", "ack_not_seen"])
            self.current_context["q_key"] = "q_not_seen"
        elif "ans_unknown" in self.cobot_intents:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])
            # if "user_hesitate_prompt" not in self.current_context["chitchat_context"]:
            #     self.current_context["chitchat_context"].append("user_hesitate_prompt")            
            #     ack += self.utt(["movie_dialog", "movie_question_bank", "q_general", "user_hesitate_prompt"])

        elif re.search(likely_mentioning_an_element_of_the_movie, self.text):
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general_positive_ack"])

        elif user_utt_length <= 4:
            # if "user_hesitate_prompt" not in self.current_context["chitchat_context"]:
            #     self.current_context["chitchat_context"].append("user_hesitate_prompt")            
            #     ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "q_2-3_ack_short_ans"]) + self.utt(["movie_dialog", "movie_question_bank", "q_general", "user_hesitate_prompt"])
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "q_2-3_ack_short_ans"])
        elif user_utt_length <= 15:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "q_2-3_ack_med_ans"])
        else:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "q_2-3_ack_long_ans"])   

        return ack

    def get_q3_ack(self):
        user_utt_length = len(self.text.strip().split())

        q_key = self.current_context["q_key"]
        if "movie_not_watched" in self.current_context.get("chitchat_context", {}):
            ack = self.utt(["movie_dialog", "movie_question_bank", "q3", "ack_not_seen"])
            self.current_context["q_key"] = "q_not_seen"
        elif "ans_unknown" in self.cobot_intents:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])
            # if "user_hesitate_prompt" not in self.current_context["chitchat_context"]:
            #     self.current_context["chitchat_context"].append("user_hesitate_prompt")            
            #     ack += self.utt(["movie_dialog", "movie_question_bank", "q_general", "user_hesitate_prompt"])
            self.current_context["q_key"] = "q"
        elif user_utt_length <= 4:
            # if "user_hesitate_prompt" not in self.current_context["chitchat_context"]:
            #     self.current_context["chitchat_context"].append("user_hesitate_prompt")            
            #     ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "q_2-3_ack_short_ans"]) + self.utt(["movie_dialog", "movie_question_bank", "q_general", "user_hesitate_prompt"])
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "q_2-3_ack_med_ans"])
            self.current_context["q_key"] = "q"
        elif user_utt_length <= 15:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "q_2-3_ack_med_ans"])
            self.current_context["q_key"] = "q"
        else:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "q_2-3_ack_long_ans"]) 
            self.current_context["q_key"] = "q"

        return ack

    def get_q4_ack(self):
        q_key = self.current_context["q_key"]
        if "movie_not_watched" in self.current_context.get("chitchat_context", {}):
            ack = self.utt(["movie_dialog", "movie_question_bank", "q4", "ack_not_seen"])
            self.current_context["q_key"] = "q_not_seen"
        elif "ans_unknown" in self.cobot_intents:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])
            # if "user_hesitate_prompt" not in self.current_context["chitchat_context"]:
            #     self.current_context["chitchat_context"].append("user_hesitate_prompt")            
            #     ack += self.utt(["movie_dialog", "movie_question_bank", "q_general", "user_hesitate_prompt"])
            self.current_context["q_key"] = "q"
        else:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q4", "q_ack"])
            self.current_context["q_key"] = "q" 

        return ack
