import pytest
from movietv_command_detector import MovieCommandDetector
from movietv_command_detector import MovieCommand

@pytest.mark.parametrize("text, expected_result", [
    ("what's the latest coming out", MovieCommand.ASK_CURRENT_PLAYING.value),
    ("what movies are coming out", MovieCommand.ASK_CURRENT_PLAYING.value),
    ("what's latest", None),

])
def test_detect_intent(text, expected_result):
    results = MovieCommandDetector(text, [{}]).detect_intents()
    if results:
        assert expected_result in results
    else:
        assert expected_result == results

