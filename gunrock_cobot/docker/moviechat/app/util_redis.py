import redis
import logging
import warnings

import random
from datetime import datetime, timedelta
from fuzzywuzzy import fuzz
import hashlib
import json
from user_profiler.user_profile import Gender
from typing import List


def try_catch(func):
    def inner_func(*args, **kwargs):
        try:
            output = func(*args, **kwargs)  # Call the original function with its arguments.
            return output

        except Exception as e:
            logging.error(f"[Redis] Fail to execute function: {func.__name__}, message: {e}")

    return inner_func


########################################################################################

#  IMPORTANT: DO NOT MODIFY THIS FILE IF IT IS IN REMOTE MODULE (ie. under docker folder)

#  IF you want to change this file, please modify the one directly under gunrock_cobot folder
#  and run "python3 _runme.py" to copy the file to all remote modules

########################################################################################

class RedisHelper:
    SERVER_IP = 'language.cs.ucdavis.edu'

    # Name
    COMMON_NAME_KEY = "gunrock:name:set"
    NAME_KEY_PREFIX = "gunrock:name"
    NAME_FIELD_GENDER = "gender"

    # Profanity
    PREFIX_PROFANITY = "gunrock:profanity"

    # NLU
    PREFIX_ASR = "gunrock:asrcorrection:data"
    PREFIX_NPKNOWLEDGE_BLANK_DESCRIPTION = "gunrock:googlekg:data:blank_description_20200505"
    PREFIX_NPKNOWLEDGE_NEW_STRUCT_SIMILARITY_V1 = "gunrock:googlekg:data:20200516"
    PREFIX_NPKNOWLEDGE_NEW_STRUCT_SIMILARITY_V2 = "gunrock:googlekg:data:20200528"

    PREFIX_AMAZONKG_DIRECTMAP = "gunrock:amazonkg:phrase2entity:20200523"
    PREFIX_AMAZONKG_NAME = "gunrock:amazonkg:name:20200523"
    PREFIX_AMAZONKG_TYPE = "gunrock:amazonkg:type:20200523"

    # Retrieval
    PREFIX_RETRIEVAL = "gunrock:retrieval:data"
    RETRIEVAL_LIST_KEY = "gunrock:ic:retrieval:apikeys"

    # Blender
    PREFIX_BLENDER_QUESTION_HANDLING = "gunrock:blender:question_handling"
    PREFIX_BLENDER_RETRIEVAL = "gunrock:blender:retrieval"

    # Movie
    MOVIE_USER_PREFIX = "gunrock:module:moviechat:user_id"  # User conversation history
    MOVIE_ID_BRIEF_INFO_REFIX = "gunrock:module:moviechat:movie_id:brief_info"  # Movie info from tmdb
    MOVIE_ID_DETAILED_PREFIX = "gunrock:module:moviechat:movie_id"  # Movie detailed info, including trivia and plot summary
    MOVIE_ID_LEADING_ACTOR = "gunrock:module:moviechat:movie_id:leading_actor"
    MOVIE_ACTOR_PREFIX = "gunrock:module:moviechat:actor_name"  # Actor trivia
    MOVIE_TMDB_MOVIE_SEARCH_PREFIX = "gunrock:module:moviechat:tmdb_movie_search"  # search by movie name
    MOVIE_TMDB_PERSON_SEARCH_PREFIX = "gunrock:module:moviechat:tmdb_person_search"
    MOVIE_TMDB_MOVIE_COLLECTION_DETAIL_PREFFIX = "gunrock:module:moviechat:tmdb_movie_collection_detail"

    # Book
    BOOK_USER_PREFIX = "gunrock:module:bookchat:user_id"

    # Music
    PREFIX_MUSIC_ARTIST = 'module:music:artist:'
    MUSIC_GENRE_KEY = 'gunrock:module:music:genre'
    PREFIX_MUSIC_USER = 'gunrock:module:music:user:'

    # Games
    PERFIX_GAME_IGDB_GAME = 'gunrock:module:game:igdb:game'
    PERFIX_GAME_IGDB_GAME_SLUG = 'gunrock:module:game:igdb:game_slug'
    PREFIX_GAME_CANONICAL_GAME_MAP = 'gunrock:module:game:canonical:game_map'

    # Holiday
    HOLIDAY_KEY = "gunrock:ic:holiday:ts"
    HOLIDAY_PREFIX_KEY = "gunrock:ic:holiday:"

    # Travel
    TRAVEL_LOCATION_NAME = "gunrock:module:travel:place_name"

    # Deprecated?
    RANDOM_THOUGHT_KEY = "gunrock:randomthoughts"
    MOVIE_NAME_PREFIX = "gunrock:module:moviechat:movie_name_2"
    MOVIE_NAME_PREFIX_GROUNDING = "gunrock:module:moviechat:movie_name_grounding"
    ACTOR_ID_REDDIT_PREFIX = "gunrock:module:moviechat:actor_id_reddit"
    DIRECTOR_ID_PREFIX = "gunrock:module:moviechat:director_id:"
    GENRE_ID_PREFIX = "gunrock:module:moviechat:genre_id:"


    def __init__(self, dev_mode=False):
        self.r = redis.StrictRedis(
            host=self.SERVER_IP,
            socket_connect_timeout=1,
            socket_timeout=1,
            port=5012,
            db=0,
            password="alexaprize",
            encoding=u'utf-8',
            decode_responses=True)

        self.dev_mode = dev_mode

    def get_client(self):
        return self.r

    def _preprocess_prefix(self, prefix):
        if self.dev_mode:
            prefix = "dev:" + prefix
        return prefix

    @try_catch
    def get(self, prefix, key, require_hashed_key=True):
        prefix = self._preprocess_prefix(prefix)

        if require_hashed_key:
            key = hashlib.md5(key.encode()).hexdigest()

        results = self.r.get(prefix + ':' + key)
        logging.info(f"[REDIS] get redis input: key: {key}, value {results}")
        if results:
            return json.loads(results)
        return None

    @try_catch
    def set(self, prefix, key, value, expire_in_sec=None, require_hashed_key=True):
        logging.info('[REDIS] set redis: input: {} output: {}, expire {}'.format(key, value, expire_in_sec))
        prefix = self._preprocess_prefix(prefix)

        if require_hashed_key:
            key = hashlib.md5(key.encode()).hexdigest()
        if expire_in_sec:
            return self.r.set(prefix + ':' + key, json.dumps(value), nx=True, ex=expire_in_sec)
        else:
            return self.r.set(prefix + ':' + key, json.dumps(value))

    @try_catch
    def set_with_expire(self, prefix, key, value, expire=None):
        warnings.warn("deprecated", DeprecationWarning)
        return self.set(prefix, key, value, expire)

    def hgetall(self, prefix, key, require_to_hash_key=True):
        if require_to_hash_key:
            key = hashlib.md5(key.encode()).hexdigest()

        search_term = prefix + ":" + key
        if self.r.exists(search_term):
            response = self.r.hgetall(search_term)
            return response
        return None

    @try_catch
    def sadd(self, key, value):
        """
        Add item to set
        """
        self.r.sadd(key, value)

    @try_catch
    def smembers(self, key):
        """
        :return: all members in the set
        """
        return self.r.smembers(key)

    @try_catch
    def delete_by_prefix(self, prefix):
        """
        DANGEROUS: THIS WILL CLEAN DATA STORED IN REDIS!!!
        """
        x = self.r.keys(prefix + ':*')
        for key in x:
            self.r.delete(key)

    @try_catch
    def delete(self, prefix, key, require_hashed_key=True):
        """
        DANGEROUS: THIS WILL CLEAN DATA STORED IN REDIS!!!
        """
        print(self.get(prefix, key, require_hashed_key))

        if require_hashed_key:
            key = hashlib.md5(key.encode()).hexdigest()

        complete_key = prefix + ":" + key
        self.r.delete(complete_key)
        print( self.get(prefix, key, require_hashed_key))


    # [start] movies
    @try_catch
    def delete_user_history(self, user_id):
        self.delete(RedisHelper.MOVIE_USER_PREFIX, user_id, True)
        self.delete(RedisHelper.BOOK_USER_PREFIX, user_id, True)

    # [start] user profile
    @try_catch
    def is_common_name(self, name):
        return self.is_common_english_name(name) or self.is_common_english_indian_european_name(name)

    @try_catch
    def is_common_english_name(self, name):
        return self.r.sismember(self.COMMON_NAME_KEY, name.lower())

    @try_catch
    def is_common_english_indian_european_name(self, name):
        return self.r.exists(self.NAME_KEY_PREFIX + ":" + name.lower())

    @try_catch
    def get_predicted_gender(self, name):
        gender = self.r.hget(self.NAME_KEY_PREFIX + ":" + name.lower(), self.NAME_FIELD_GENDER)

        if not gender:
            gender = '3'

        gender_dict = {'0': Gender.FEMALE, '1': Gender.MALE, '3': Gender.UNKNOWN}

        return gender_dict[gender]

    @try_catch
    def set_name_gender(self, name: str, gender: str = "3"):
        """
        :param name:
        :param gender: "0": female, "1": male, "3": unknown
        """
        if gender != "0" and gender != "1":
            gender = "3"

        self.r.hset(self.NAME_KEY_PREFIX + ":" + name, "gender", gender)

    @try_catch
    def remove_name(self, *names):
        for name in names:
            assert isinstance(name, str)
            name = name.lower()
            key = f'{self.NAME_KEY_PREFIX}:{name}'
            if self.r.exists(key):
                print(f'deleting {name} in NAME_KEY_PREFIX...')
                self.r.delete(key)

            if self.r.sismember(self.COMMON_NAME_KEY, name):
                print(f'deleting {name} in COMMON_NAME_KEY...')
                self.r.srem(self.COMMON_NAME_KEY, name)

    # [end] user profile


    # [start] reddit key
    @try_catch
    def list_reddit_keys(self):
        return self.r.smembers(self.RETRIEVAL_LIST_KEY)

    @try_catch
    def get_reddit_keys_length(self):
        return self.r.llen(self.RETRIEVAL_LIST_KEY)

    @try_catch
    def get_reddit_key(self):
        """get the first one from a list of reddit keys and round robin"""
        return self.r.brpoplpush(self.RETRIEVAL_LIST_KEY, self.RETRIEVAL_LIST_KEY)
    # [end] reddit key


    # [start] holiday
    @try_catch
    def scan_related_holiday(self, keyword, threshold=70):
        key = None
        for holiday in self.r.zscan_iter(self.HOLIDAY_KEY):
            if fuzz.token_sort_ratio(holiday[0], keyword) > threshold:
                key = holiday[0]
                break
        if key is None:
            return None
        return self.r.hgetall(self.HOLIDAY_PREFIX_KEY + key)

    @try_catch
    def get_future_holiday(self, category=None, limit=5):
        # FIXME: safe way to make sure it include today
        second_before_today = int(
            (datetime.utcnow() - timedelta(hours=24)).strftime("%s")) - 1
        keys = self.r.zrangebyscore(
            self.HOLIDAY_KEY, second_before_today, 'inf', num=limit, start=0)
        ret = []
        for key in keys:
            holiday_data = self.r.hgetall(self.HOLIDAY_PREFIX_KEY + key)
            if category is None or (
                    "Category" in holiday_data and category == holiday_data["Category"]):
                ret.append(holiday_data)
        return ret

    @try_catch
    def get_all_holiday(self, category=None, limit=5):
        # FIXME: safe way to make sure it include today
        keys = self.r.zrangebyscore(self.HOLIDAY_KEY, 0, 'inf', num=limit, start=0)
        ret = []
        for key in keys:
            holiday_data = self.r.hgetall(self.HOLIDAY_PREFIX_KEY + key)

            if category is None or (
                    "Category" in holiday_data and category == holiday_data["Category"]):
                ret.append(holiday_data)
        return ret
    # [end] holiday

    # [start] nlu
    @try_catch
    def get_asr(self, phonetics_type):
        return self.get(self.PREFIX_ASR, phonetics_type)

    # [end] nlu

    # [start] response generator
    @try_catch
    def get_retrieval(self, input_str):
        return self.get(self.PREFIX_RETRIEVAL, input_str)

    @try_catch
    def get_random_thoughts(self, keyword):
        logging.info(
            "[Utils] Get random thoughts with input word: {} and redis key {}".format(
                keyword, self.RANDOM_THOUGHT_KEY))
        resp = self.r.hget(self.RANDOM_THOUGHT_KEY, keyword.lower().strip())
        if resp is None or resp == '':
            return None
        return resp

    # [end] response generator


    # [start] music
    @try_catch
    def is_known_artist(self, artist):
        return self.r.exists(self.PREFIX_MUSIC_ARTIST + artist)

    @try_catch
    def is_known_genre(self, genre):
        return self.r.sismember(self.MUSIC_GENRE_KEY, genre)

    @try_catch
    def is_returning_user_in_music_module(self, user_id):
        return self.r.exists(self.PREFIX_MUSIC_USER + user_id)

    @try_catch
    def get_user_music_profile(self, user_id):
        return self.r.get(self.PREFIX_MUSIC_USER + user_id)

    @try_catch
    def set_user_music_profile(self, user_id, user):
        return self.r.set(self.PREFIX_MUSIC_USER + user_id, json.dumps(user))
    # [end] music




