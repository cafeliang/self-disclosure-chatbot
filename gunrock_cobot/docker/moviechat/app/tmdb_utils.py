# tmdb utils

import json
import hashlib
import redis
import random
import re
import urllib
import requests
from fuzzywuzzy import fuzz
from tmdbv3api import TMDb, Movie, Person, TV
from util_redis import RedisHelper
from types import SimpleNamespace
from typing import List, Optional
from operator import itemgetter
from utils import trace_latency
import logging



TMDB_API_KEYS = ['cfd14412e3951047cd49fc48c56369b2',
                 '2b08f9a99dd827c4ba952e4202162f72',
                 'acc78bfb93c7ef930ac7f06699286aca']
tmdb_api = random.choice(TMDB_API_KEYS)

tmdb = TMDb()
TMDb.api_key = tmdb_api
movie = Movie()
tv = TV()
person = Person()

r = redis.StrictRedis(host='language.cs.ucdavis.edu', port=5012,
                      socket_timeout=3,
                      socket_connect_timeout=1,
                      retry_on_timeout=True,
                      charset='utf-8',
                      db=0, password="alexaprize", decode_responses=True)

def tmdb_movie_get_popular_movie_in_genre(genre):
    results = []
    return results

@trace_latency
def get_now_playing_movies():
    try:
        now_playing = Movie().now_playing()[0:3]
    except Exception as e:
        logging.info("[MOVIECHAT_MODULE] Exception in search_with_movie_name {}".format(repr(e)))
        now_playing = []
    return now_playing



@trace_latency
def tmdb_movie_search(name, page_ind=1):
    print ("tmdb_movie_search:")

    name = remove_definite_article_in_the_beginning(name)

    key = name + "_" + str(page_ind)
    cache = RedisHelper().get(RedisHelper.MOVIE_TMDB_MOVIE_SEARCH_PREFIX, key)
    if cache is not None:
        logging.debug("[tmdb_movie_search] cache hit", cache)
        return cache
    logging.debug("[tmdb_movie_search] cache miss")

    # results = movie.search(name, page_ind) # it is broken since 19/11/27
    results = search_movie(name, page_ind)

    value = parse_tmdb_movie_search_results(results)

    RedisHelper().set(RedisHelper.MOVIE_TMDB_MOVIE_SEARCH_PREFIX, key, value, 14 * 24 * 60 * 60)
    return value

@trace_latency
def search_movie(name, page_ind):
    base_url = 'https://api.themoviedb.org/3/search/movie?'
    query = {
        'api_key': tmdb_api,
        'query': name,
        'page': page_ind
    }
    encoded_query = urllib.parse.urlencode(query)

    results = requests.get(base_url + encoded_query).json()
    return results['results']


def parse_tmdb_movie_search_results(results):
    output = []
    for res in results:
        if isinstance(res, dict):
            res = SimpleNamespace(**res)

        features = dir(res)
        # some ugly tricks to solve the inconsistent between movie and tv
        data = {
            "adult": res.adult if 'adult' in features else None,
            "backdrop_path": res.backdrop_path,
            "genre_ids": res.genre_ids,
            "id": res.id,
            "original_language": res.original_language if 'original_language' in features else None,
            "original_title": res.original_title if 'original_title' in features else res.name,
            "overview": res.overview,
            "popularity": res.popularity,
            "poster_path": res.poster_path if 'poster_path' in features else None,
            "release_date": res.release_date if 'release_date' in features else None,
            "title": res.title if 'title' in features else res.name,
            "video": res.video if 'video' in features else None,
            "vote_average": res.vote_average,
            "vote_count": res.vote_count,
        }
        output.append(data)
    return output



@trace_latency
def retrieve_movie_info(movie_id):
    result = None

    base_url = 'https://api.themoviedb.org/3/movie/{}?'.format(movie_id)
    query = {
        'api_key': tmdb_api,
    }

    encoded_query = urllib.parse.urlencode(query)
    try:
        result = requests.get(base_url + encoded_query).json()
        logging.info(f"retrieve_movie_info result: {result}")
        if hasattr(result, 'status_code'):  # fails to find movie
            result = None
    except Exception as e:
        logging.error(f"Failed to retrieve movie info: {e}")
    return result


@trace_latency
def tmdb_get_all_movies_in_collection(collection_id) -> List[dict]:
    key = str(collection_id)
    cache = RedisHelper().get(RedisHelper.MOVIE_TMDB_MOVIE_COLLECTION_DETAIL_PREFFIX, key)

    if cache is not None:
        logging.debug("[tmdb_movie_collection_detail] cache hit")
        movies = cache["parts"]
        return movies
    logging.debug("[tmdb_movie_collection_detail] cache miss")

    value = get_collection(collection_id)
    RedisHelper().set(RedisHelper.MOVIE_TMDB_MOVIE_COLLECTION_DETAIL_PREFFIX, key, value, 14 * 24 * 60 * 60)

    movies = value["parts"]
    return movies

@trace_latency
def get_collection(collection_id):
    base_url = 'https://api.themoviedb.org/3/collection/{}?'.format(collection_id)
    query = {
        'api_key': tmdb_api,
    }

    encoded_query = urllib.parse.urlencode(query)
    result = requests.get(base_url + encoded_query).json()

    if hasattr(result, 'status_code'):  # fails to find result
        return None

    return result


def sort_movies_by_popularity(movies: List[dict]):
    return sorted(movies, key=itemgetter('popularity'), reverse=True)


@trace_latency
def get_popular_movies():
    movies = movie.popular()
    return movies


def is_popular_movie(movie_id):
    movies = movie.popular()
    for item in movies:
        if movie_id == item.id:
            return True
    return False


def remove_definite_article_in_the_beginning(name):
    return re.sub(r"^\b(the )\b", "", name)

def tmdb_tv_search(name, page_ind=1):
    TMDB_TV_SEARCH_PREFIX = "gunrock:module:moviechat:tmdb_tv_search"
    key = name + "_" + str(page_ind)

    # todo: add the following back after finishing cleaning up the cache
    # cache = RedisHelper().get(TMDB_TV_SEARCH_PREFIX, key)
    #
    # if cache is not None:
    #     print("[tmdb_tv_search] cache hit")
    #     return cache
    # print("[tmdb_tv_search] cache miss")

    results = tv.search(name, page_ind)
    value = parse_tmdb_movie_search_results(results)
    # currently use expire period of three months; don't expire until after
    # finals
    RedisHelper().set_with_expire(TMDB_TV_SEARCH_PREFIX, key, value, 14 * 24 * 60 * 60)

    return value


# cached version of tmdb person.search() method
@trace_latency
def tmdb_person_search(name, page_ind=1):
    TMDB_PERSON_SEARCH_PREFIX = "gunrock:module:moviechat:tmdb_person_search"
    key = name + "_" + str(page_ind)

    cache = RedisHelper().get(TMDB_PERSON_SEARCH_PREFIX, key)

    if cache is not None:
        print("[tmdb_person_search] cache hit")
        return cache
    print("[tmdb_person_search] cache miss")

    # results = person.search(name, page_ind) # this is broken since 19/11/27
    results = search_person(name, page_ind)
    value = parse_tmdb_person_search_results(results)
    # currently use expire period of three months; don't expire until after
    # finals
    RedisHelper().set_with_expire(TMDB_PERSON_SEARCH_PREFIX, key, value, 14 * 24 * 60 * 60)

    return value


def discover_movie(cast_id="", genre_id=""):
    base_url = 'https://api.themoviedb.org/3/discover/movie?'
    query = {
        'api_key': tmdb_api,
        "language": "en-US",
        'sort_by': "popularity.desc",
        'include_adult': False,
        'with_cast': str(cast_id) if cast_id else "",
        'with_genres': str(genre_id) if genre_id else "",
        'page': 1
    }

    encoded_query = urllib.parse.urlencode(query)
    results = requests.get(base_url + encoded_query)

    return results.json()['results']


def recommend_movie(movie_id):
    base_url = f'https://api.themoviedb.org/3/movie/{movie_id}/recommendations?'
    query = {
        'api_key': tmdb_api,
        "language": "en-US",
        'page': 1
    }
    encoded_query = urllib.parse.urlencode(query)
    results_raw = requests.get(base_url + encoded_query)
    results = results_raw.json()['results']
    sorted_results = sort_movies_by_popularity(results)
    return sorted_results


@trace_latency
def get_credits(movie_id) -> List[dict]:
    base_url = f'https://api.themoviedb.org/3/movie/{movie_id}/credits?'
    query = {
        'api_key': tmdb_api,
    }

    encoded_query = urllib.parse.urlencode(query)
    results_raw = requests.get(base_url + encoded_query)
    results = results_raw.json()
    return results["cast"]

def get_plot_summary(movie_id):
    from bs4 import BeautifulSoup

    MAX_LEN = 300  # no of characters
    MAX_TRIVIA = 5
    imdb_id = get_imdb_id(movie_id)
    PlotSummary_list=[]
    url = "https://www.imdb.com/title/"+imdb_id+"/plotsummary"
    response = requests.get(url)
    html_soup = BeautifulSoup(response.text, 'html.parser')
    plotsummary = html_soup.find('ul', class_ = 'ipl-zebra-list',id = 'plot-summaries-content')
    new_plotsummary = plotsummary.find_all('p')
    for item in new_plotsummary:
        if len(item.get_text())<MAX_LEN:
            PlotSummary_list.append(item.get_text().strip())
        if len(PlotSummary_list)==MAX_TRIVIA:
            break
    return PlotSummary_list

def get_imdb_id(tmdb_id):
    return retrieve_movie_info(tmdb_id)["imdb_id"]

# results returned by tmdb_api

@trace_latency
def search_person(name, page_ind):
    base_url = 'https://api.themoviedb.org/3/search/person?'
    query = {
        'api_key': tmdb_api,
        'query': name,
        'page': page_ind
    }
    encoded_query = urllib.parse.urlencode(query)

    results = requests.get(base_url + encoded_query)
    return results.json()['results']



def parse_tmdb_person_search_results(results):
    output = []
    for res in results:
        if isinstance(res, dict):
            res = SimpleNamespace(**res)

        data = {
            "adult": res.adult,
            "id": res.id,
            "known_for": res.known_for,
            "name": res.name,
            "popularity": res.popularity,
            "profile_path": res.profile_path,
        }
        output.append(data)
    return output

# for caching api calls

@trace_latency
def tmdb_get_redis(prefix, key):
    hinput = hashlib.md5(key.encode()).hexdigest()
    value = r.get(prefix + ':' + hinput)
    #print('[REDIS] tmdb_get_redis - key: {}, value: {}'.format(key, value))
    if value is None or value == '[]':
        return None
    return json.loads(value)

# for caching api calls

@trace_latency
def tmdb_set_redis(prefix, key, value, expire=90 * 24 * 60 * 60):
    #print('[REDIS] tmdb_set_redis - key: {}, value: {}, expire {}'.format(key, value, expire))
    hinput = hashlib.md5(key.encode()).hexdigest()
    return r.set(prefix + ':' + hinput, json.dumps(value))


