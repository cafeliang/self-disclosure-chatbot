BACKSTORY_URL_NO_PARAPHRASE = 'http://ec2-54-175-156-102.compute-1.amazonaws.com:8085/ask/default'
BACKSTORY_URL_PARAPHRASE = 'http://language.cs.ucdavis.edu:5021/ask/paraphrase'

BACKSTORY_URL_KEY = 'backstory_url'


class ABTestConfig:
    a = {
        BACKSTORY_URL_KEY: BACKSTORY_URL_PARAPHRASE
    }


config = {
    'A': ABTestConfig.a,
    'B': ABTestConfig.a,
    'C': ABTestConfig.a,
    'D': ABTestConfig.a,
    'E': ABTestConfig.a,
}