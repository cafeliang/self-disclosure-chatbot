from movietv_utils import *

trivia_limit = 3

movie_info_avenger = {'movie_name': 'the avengers', 'movie_id': 24428,
                      'movie_overview': 'When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!',
                      'movie_genre_ids': [878, 28, 12],
                      'movie_leading_actor': {'name': 'Robert Downey Jr.', 'role': 'Tony Stark / Iron Man'},
                      'movie_release_year': '2012'}



def test_get_imdb_trivia_new_invalid_id():
    invalid_id = 123

    posts = get_imdb_trivia_new(invalid_id, trivia_limit)

    assert posts is None


def test_get_imdb_trivia_new_valid_id():
    valid_id = 522369

    posts = get_imdb_trivia_new(valid_id, trivia_limit)

    assert type(posts) is list
    assert len(posts) > 0

def test_get_leading_actor_role():
    result = get_leading_actor_role(522369)
    assert result["name"] == "Kris Hitchen"
    assert result["role"] == "Ricky Turner"
