import logging
import re
import traceback
from transitions import Machine
from movietv_utils import *
from context_manager import ContextManager
from nlu.constants import DialogAct

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('transitions').setLevel(logging.DEBUG)

SIZE_TRANSITION_HISTORY = 5

def set_actor_dialog_init_context(context: ContextManager, actor_info):
    context.current_subdialog_context = {
        "current_state": "s_init",
        "questions_asked": 0,
        "trivia_used": 0,
        "actor_info": actor_info,
        "curr_q_num": None,
        "q_key": "q",
        "transition_history": ["r_t0"] * SIZE_TRANSITION_HISTORY,
        "chitchat_mode": "qa_mode",
        "chitchat_context": [],
        "tags": [],
    }


def save_actor_dialog_current_context(context: ContextManager, actor_info):
    context.actors[str(actor_info["id"])] = {
        "current_subdialog_context": context.current_subdialog_context,
    }


class ActorDialog(Machine):
    # High level specification of states, state transitions and transition conditions
    def __init__(self, automaton):
        # Each state has prefix 's_'
        self.states = [
            's_init',  # main entry point
            's_chitchat',
            's_exit',
        ]
        # Each state has a corresponding transtion function that chooses the next state depending on certain conditions
        # Each transition function has prefix 't_' and following name matches the state name e.g. 's_init' and 't_init'
        # There are different ways to define transitions. I'm defining arguments using a list.
        # Args ordering: transition_name, source, dest, conditions=None, unless=None, before=None, after=None, prepare=None
        self.transitions = [
            # s_init
            ['transduce', '*', 's_exit',
                ['c_no_questions_left', 'c_no_trivia_left', 'r_t1']],
            ['transduce', 's_init', 's_chitchat', [
                'r_t2']],  # first time entry point
            ['transduce', 's_chitchat', '=', ['r_t3']],
        ]

        self.automaton = automaton
        self.text = automaton.text
        self.utt = automaton.utt
        self.context = automaton.context
        self.actor_info = self.context.current_subdialog_context["actor_info"]
        self.tags = []

        if str(self.actor_info["id"]) in self.context.actors and self.context.current_subdialog_context["current_state"] == "s_init" and "current_subdialog_context" in self.context.actors[str(self.actor_info["id"])]:
            self.tags.append("c_resuming_dialog")
            self.context.current_subdialog_context = self.context.actors[str(
                self.actor_info["id"])]["current_subdialog_context"]
        self.current_context = self.context.current_subdialog_context

        self.facts = get_interesting_reddit_posts([self.actor_info["name"]], 2)

        if self.current_context["current_state"] == "s_init":
            if self.facts is not None:
                self.current_context["num_total_trivia"] = len(self.facts)
            else:
                self.current_context["num_total_trivia"] = 0 

        if self.current_context["questions_asked"] >= 3:
            self.tags.append("c_no_questions_left")

        if "trivia_used" not in self.current_context:
            print("[MOVIECHAT_MODULE] Exception with trivia_used - current_context {}".format(self.current_context))
            self.current_context["trivia_used"] = 0
            
        if self.current_context["trivia_used"] >= self.current_context["num_total_trivia"]:
            self.tags.append("c_no_trivia_left")           

        Machine.__init__(self, states=self.states, transitions=self.transitions, initial=self.context.current_subdialog_context["current_state"],
                         prepare_event='prepare', finalize_event='finalize', send_event=True)

    def get_response(self, input_data):
        print("current_context - beginning get_response:", self.current_context)
        self.text = input_data["text"][0]
        self.dialog_act = input_data["features"][0]["dialog_act"]
        self.amz_dialog_act = input_data["features"][0].get("amz_dialog_act")
        self.ner = input_data["features"][0]["ner"]
        self.asr_correction = input_data["features"][0]["asrcorrection"]
        self.knowledge = input_data["features"][0]["knowledge"]
        self.topic_keywords = input_data["features"][0]["topic_keywords"]
        self.sentiment = input_data["features"][0]["sentiment"]
        self.cobot_intents = input_data["features"][0]["intent_classify"]["lexical"] + \
            input_data["features"][0]["intent_classify"]["topic"]
        self.features = input_data["features"][0]
        self.input_data = input_data
        self.old_state = self.state
        self.cache = {}  # for caching conditions results if necessary
        self.r_ack = ""
        self.r_body = ""
        self.r_question = ""
        self.transition_history = self.context.current_subdialog_context["transition_history"]

        self.transduce()

        self.new_state = self.state
        self.context.current_subdialog_context["current_state"] = self.state
        self.context.current_subdialog_context["old_state"] = self.old_state
        self.context.current_subdialog_context["transition_history"] = self.transition_history
        self.context.current_subdialog_context["tags"] = self.tags
        save_actor_dialog_current_context(self.context, self.actor_info)
        self.final_response = self.r_ack + self.r_body + self.r_question
        print("current_context - end get_response:", self.current_context)
        return self.final_response

    # on_enter states: Most of the logic for each state to generate the response is defined here
    # Every state has a corresponding callback here
    # =======================================================
    def on_enter_s_chitchat(self, event):
        print("We've just entered state s_chitchat")

        self.set_ack()
        self.set_body()
        self.set_next_question()

    def on_enter_s_exit(self, event):
        print("We've just entered state s_exit")
        # default response; should be overwritten upon returning
        self.r_question = "Sorry, I was thinking about what movie to watch and got distracted. Can you remind me what we were talking about? "

    def set_chitchat_context(self):
        if "chitchat_context" not in self.current_context: # may not exist in old data format
            self.current_context["chitchat_context"] = []

    def set_ack(self):
        print("ack chitchat_mode:", self.current_context["chitchat_mode"])
        if self.current_context["chitchat_mode"] == "trivia_mode":
            self.r_ack = ""  # rely on system ack for now
        elif self.current_context["chitchat_mode"] == "qa_mode" and self.current_context["curr_q_num"] is not None:
            ack_bot = ActorAcknowledgeBot(self)
            self.tags.append(self.current_context["curr_q_num"] + "_ack")
            self.r_ack = ack_bot.get_ack(
                self.current_context["curr_q_num"])
        # overwrite if interrupted by question
        if "movie_user_qa_answer_only" in self.automaton.moviechat_user["tags"]:
            self.r_ack = "So. About {}. <break time='0.15s'/>. ".format(self.actor_info["name"])
        if "c_resuming_dialog" in self.tags:
            self.tags.append("resume_ack")
            self.r_ack = self.utt(["actor_dialog", "resume_actor_ack"], {
                                  "actor_name": self.actor_info["name"]})
        elif self.old_state == "s_init":
            self.r_ack = self.utt(["actor_dialog", "init_actor_ack"], {
                                  "actor_name": self.actor_info["name"]})

    def set_body(self):
        if self.current_context["questions_asked"] != 0 and self.current_context[
                "chitchat_mode"] == "qa_mode" and "c_no_trivia_left" not in self.tags:
            self.current_context["chitchat_mode"] = "trivia_mode"
        elif self.current_context["trivia_used"] != 0 and self.current_context["chitchat_mode"] == "trivia_mode" and "c_no_questions_left" not in self.tags:
            self.current_context["chitchat_mode"] = "qa_mode"

        print("body chitchat_mode:", self.current_context["chitchat_mode"])

        if self.current_context["chitchat_mode"] == "trivia_mode":
            self.r_body = self.utt(["actor_dialog", "trivia_body"], {
                                  "actor_name": self.actor_info["name"], "trivia": self.facts[self.current_context["trivia_used"]]})
            self.current_context["trivia_used"] += 1

    def set_next_question(self):
        if self.current_context["chitchat_mode"] == "qa_mode":
            if self.current_context["curr_q_num"] is None:
                self.context.current_subdialog_context["curr_q_num"] = "q1"
            else:
                self.context.current_subdialog_context["curr_q_num"] = "q" + \
                    str(int(self.context.current_subdialog_context["curr_q_num"][-1]) + 1)

            self.tags.append(
                self.context.current_subdialog_context["curr_q_num"] + "_question")
            # overwrite in ActorAcknowledgeBot
            try:
                q_key = self.current_context.get("q_key", "q")
                self.tags.append("q_key_" + q_key)
                if self.current_context.get("qa_template_params") is not None:
                    self.r_question = self.utt(["actor_dialog", "actor_question_bank", self.current_context["curr_q_num"], q_key], self.current_context["qa_template_params"])
                    self.current_context["qa_template_params"] = None
                else:
                    self.r_question = self.utt(["actor_dialog", "actor_question_bank", self.current_context["curr_q_num"], q_key], {"actor_name": self.actor_info["name"]})
            except: # old data format incompat
                q_key = "q"
                self.current_context["q_key"] = q_key
                self.tags.append("q_key_" + q_key)      
                self.r_question = self.utt(["actor_dialog", "actor_question_bank", self.current_context["curr_q_num"], q_key], {"actor_name": self.actor_info["name"]})
                print("[MOVIECHAT_MODULE] Exception in set_next_question {}".format(traceback.format_exc()))   
            self.current_context["questions_asked"] += 1            
        elif self.current_context["chitchat_mode"] == "trivia_mode":
            self.r_question = self.utt(["movie_dialog", "trivia_question"])

    # System level callbacks
    # =======================================================
    def prepare(self, event):
        return True

    def finalize(self, event):
        return True

    # Conditions
    # These callback functions cache their result before they may be checked multiple times
    # Depending on the function, they may have to cache multiple values, e.g. c_entity_detected
    # If conditions callbacks interact with user_attributes, remember to reset fields where necessary
    # ==========================================================
    # detection strength varies depending
    def c_resuming_dialog(self, event):
        if "c_resuming_dialog" in self.tags:
            return True

        return False    

    def c_no_questions_left(self, event):
        if "c_no_questions_left" in self.tags:
            return True

        return False

    def c_no_trivia_left(self, event):
        # temp
        if "c_no_trivia_left" in self.tags:
            return True

        return False

    def r_t1(self, event):
        return self.update_transition_history("r_t1")

    def r_t2(self, event):
        return self.update_transition_history("r_t2")

    def r_t3(self, event):
        return self.update_transition_history("r_t3")

    def update_transition_history(self, transition_tag):
        self.transition_history.append(transition_tag)
        self.transition_history.pop(0)
        return True

class ActorAcknowledgeBot:
    def __init__(self, fsm):
        self.fsm = fsm
        self.utt = fsm.utt
        self.text = fsm.text
        self.cobot_intents = fsm.cobot_intents
        self.actor_info = self.fsm.actor_info
        self.context = self.fsm.context
        self.current_context = self.context.current_subdialog_context
        self.questions_mapping = {
            "q1": self.get_q1_ack,
            "q2": self.get_q2_ack,
            "q3": self.get_q3_ack,
        }

    def is_dont_know(self):
        return self.fsm.automaton.returnnlp.has_intent("ans_unknown") or self.fsm.automaton.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS)

    def get_ack(self, q_num):
        return self.questions_mapping[q_num]()

    def get_q1_ack(self):
        ans_good_regex = r"(^(?!.*(not|never|doesn't|don't)).*(excellent|nice|friend|admir(e|able)|good|great|lov(e|ing)|beautiful|wonderful|outstanding|exceptional|awesome|amazing|gets into character|incredible|marvelous|enjoy|like))|(^(?=.*(not|never|doesn'|don't)).*(horrible|bad|awful|dreadful|terrible|hate|\bmean\b|hostile|rude|arrogant))"
        ans_bad_regex = r"(^(?=.*(not|never|doesn't|don't)).*(excellent|nice|friend|admir(e|able)|good|great|lov(e|ing)|beautiful|wonderful|outstanding|exceptional|awesome|amazing|gets into character|incredible|marvelous|enjoy|like))|(^(?!.*(not|never|doesn'|don't)).*(horrible|bad|awful|dreadful|terrible|hate|\bmean\b|hostile|rude|arrogant))"
        r_ans_mixed = False
        r_ans_good = False
        r_ans_bad = False
        if re.search(ans_good_regex, self.text):
            r_ans_good = True
        if re.search(ans_bad_regex, self.text):
            r_ans_bad = True
        if r_ans_good and r_ans_bad:
            r_ans_mixed = True

        if self.is_dont_know():
            ack = self.utt(["actor_dialog", "actor_question_bank", "q1", "ack_dont_know"], {"actor_name": self.actor_info["name"]})
        elif r_ans_mixed:
            print("[actor_ack] r_ans_mixed")
            ack = self.utt(["actor_dialog", "actor_question_bank", "q1", "ack_mixed"], {"actor_name": self.actor_info["name"]})
        elif r_ans_good:
            print("[actor_ack] r_ans_good")
            ack = self.utt(["actor_dialog", "actor_question_bank", "q1", "ack_good"], {"actor_name": self.actor_info["name"]})
        elif r_ans_bad:
            print("[actor_ack] r_ans_bad")
            ack = self.utt(["actor_dialog", "actor_question_bank", "q1", "ack_bad"], {"actor_name": self.actor_info["name"]})
        else:
            print("[actor_ack] default")
            ack = self.utt(["actor_dialog", "actor_question_bank", "q1", "ack_default"], {"actor_name": self.actor_info["name"]})

        return ack

    def get_q2_ack(self):
        ans_good_regex = r"(^(?!.*(not|never|doesn't|don't)).*(excellent|nice|friend|admir(e|able)|good|great|lov(e|ing)|beautiful|wonderful|outstanding|exceptional|awesome|amazing|gets into character|incredible|marvelous|enjoy|like))|(^(?=.*(not|never|doesn'|don't)).*(horrible|bad|awful|dreadful|terrible|hate|\bmean\b|hostile|rude|arrogant))"
        ans_bad_regex = r"(^(?=.*(not|never|doesn't|don't)).*(excellent|nice|friend|admir(e|able)|good|great|lov(e|ing)|beautiful|wonderful|outstanding|exceptional|awesome|amazing|gets into character|incredible|marvelous|enjoy|like))|(^(?!.*(not|never|doesn'|don't)).*(horrible|bad|awful|dreadful|terrible|hate|\bmean\b|hostile|rude|arrogant))"
        r_ans_mixed = False
        r_ans_good = False
        r_ans_bad = False
        if re.search(ans_good_regex, self.text):
            r_ans_good = True
        if re.search(ans_bad_regex, self.text):
            r_ans_bad = True
        if (r_ans_good and r_ans_bad):
            r_ans_mixed = True

        if self.is_dont_know():
            ack = self.utt(["actor_dialog", "actor_question_bank", "q2", "ack_dont_know"], {"actor_name": self.actor_info["name"]})
        elif r_ans_mixed:
            print("[actor_ack] r_ans_mixed")
            ack = self.utt(["actor_dialog", "actor_question_bank", "q2", "ack_mixed"], {"actor_name": self.actor_info["name"]})
        elif r_ans_good:
            print("[actor_ack] r_ans_good")
            ack = self.utt(["actor_dialog", "actor_question_bank", "q2", "ack_good"], {"actor_name": self.actor_info["name"]})
        elif r_ans_bad:
            print("[actor_ack] r_ans_bad")
            ack = self.utt(["actor_dialog", "actor_question_bank", "q2", "ack_bad"], {"actor_name": self.actor_info["name"]})
        else:
            print("[actor_ack] default")
            ack = self.utt(["actor_dialog", "actor_question_bank", "q2", "ack_default"], {"actor_name": self.actor_info["name"]})

        return ack

    def get_q3_ack(self):
        # No ack, should go to movie dialog
        ack = ""

        return ack
