#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import logging
import sys
import traceback
from timeit import default_timer as timer

from flask import Flask, request, Response
from flask_restful import reqparse, Api, Resource

import self_disclosure_study.self_disclosure_response_generator as response_generator
from utils import setup_logging_config

app = Flask("Movie")
api = Api(app)

logger = logging.getLogger("Movie")
setup_logging_config(logger)

class RemoteResponseGenerator(Resource):

    def get(self):
        return 200

    def post(self):
        t0 = time.time()
        args = request.get_json(force=True)
        # print(args)
        validation = self.__validate_input(args)
        if validation:
            return validation, 500

        ret = {}

        ret.update(
            self.__get_response(args)
        )

        ret['performance'] = time.time() - t0,
        ret['error'] = False

        return ret, 200

    @staticmethod
    def __validate_input(args):
        # message = ""
        # for ctx in response_generator.get_required_context():
        #     if not args.get(ctx):
        #         message = "Context missing: " + ctx
        # if message:
        #     return {
        #         'message': message,
        #         'error': True
        #     }
        return None

    @staticmethod
    def __get_response(msg):
        response = response_generator.handle_message(msg)
        return response


api.add_resource(RemoteResponseGenerator, '/')

if __name__ == '__main__':
    # print ("Inside main of app.py") works
    app.run(host="0.0.0.0", port=os.environ.get('RG_PORT') or 5001)
