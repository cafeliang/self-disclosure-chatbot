# -*- coding: utf-8 -*-


import re
import random
import signal
import hashlib
import json
import traceback
import praw
import redis
import ast
import yaml

from fuzzywuzzy import fuzz
from num2words import num2words
from attrdict import AttrDict
from movietv_templates_data import data as mdata

from cobot_common.service_client import get_client
from typing import List
from tmdbv3api import TMDb, Movie
from movie_module_regex import ask_recommend
from enum import Enum
from tmdb_utils import *
from utils import trace_latency
import logging


# Redis python client
r = redis.StrictRedis(host='language.cs.ucdavis.edu', port=5012,
                      socket_timeout=3,
                      socket_connect_timeout=1,
                      retry_on_timeout=True,
                      charset='utf-8',
                      db=0, password="alexaprize", decode_responses=True)


# API keys
COBOT_API_KEY = 'xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT'
REDDIT_CLIENT_ID = "lKmLeMmSSLns6A"
REDDIT_CLIENT_SECRET = "Fi6V_pCzKqE-4TRVqcmpMJdET2s"
RETRIEVAL_LIST_KEY = "gunrock:ic:retrieval:apikeys"
TMDB_API_KEYS = ['cfd14412e3951047cd49fc48c56369b2',
                 '2b08f9a99dd827c4ba952e4202162f72']
TMDb.api_key = random.choice(TMDB_API_KEYS)

TRUNCATE_LIMIT = 50

serialized_fields = {"trivia", "genre_id", "similar", "actors", "directors"}

title_filter_list = {"yes", "no", "okay", "well", "what"}

blacklist = [r"\bass", r"\basshole", r"\banal\b", r"\banus", r"\barsehole", r"\barse\b", r"\bbitch", r"\bbangbros", r"\bbastard"
                 r"\btit(s|ties|)\b", r"\bbutt(s|)\b", r"\bblow( |)job\b", r"\bboob", r"\bbra(s|)\b", r"\bcock(s|)\b", r"\bcum\b", r"\bcunt(s|)\b", r"\bdick(s|)\b", r"\bshit(s|)\b", r"\bcocaine\b",
                 r"\bsex", "erotic", "fuck", "fxxk", r"f\*\*k", r"f\*\*c", r"f\*ck", r"fcuk", r"f\*\*\*", "fcking", "fck", "fucken", "fucked", "fucks", "gang bang", "gangbang",
                 "genital", "damn", "damnit", "horny", "jerk off", "jerkoff", r"\bkill", "loser", "masterbate", "masterbates", "masterbated", "naked",
                 "nasty", "negro", "nigger", "nigga", "orgy", "pussy", "penis", "perve", r"\brape", "fucked",
                 "fucking", "trump", "racist", "sexy", "strip club", "vagina", "faggot", "fag", "drug", "weed", "marijuana", "cannabis", "lsd",
                 "murder", "incel", "prostitute", "slut", "whore", "hooker", "cuck", "cuckold", "shooting", "nazis",
                 "nazism", "nazi", "sgw", r"\bballs", "thot", "virgin", "virginity", "cumshot", "cumming", "dildo", "vibrator", "sexual",
                 "douche", "douche", "bukkake", "douchebag", r"\bporn", "porngraph", "porngraphy", "died", "killed",
                 "killing", "murdered", "murdering", "murders", r"\bclit", "clitoris", "masterbate tatoo", "cheat",
                 "cheated", "cheating", "dammit", "toxicity", "toxic", "wtf", "breasts", "boobies", r"\bdead", "sperm",
                 "death", "pornhub", "intercourse", "pornography", "porno", "boobs", "penisis", "my husband", "my boyfriend",
                 "my girlfriend", "my wife", "end my life", "reddit", "reddit,", "reddit:", "subreddit", "tifu", "post raw", "show discussion thread",
                 "penises", "dildos", "vaginas", "school shooting", "breasts", "showerthoughts", "todayilearned", "mushroom tatoo", r"commit.* suicide"]  # add "\r" after testing

ask_intents = {
    "ask_yesno", "ask_preference", "ask_opinion", "ask_advice", "ask_ability", "ask_hobby",
    "ask_recommend", "ask_reason", "ask_self", "ask_fact", "ask_freq", "ask_dist", "ask_loc",
    "ask_count", "ask_degree", "ask_time", "ask_person", "ask_name", "topic_qa", "topic_backstory"
}

ans_intents = {
    "ans_unknown", "ans_neg", "ans_pos", "ans_like", "ans_wish", "ans_factopinion",
}

def get_movie_recommendation(genre):
    return tmdb_movie_get_popular_movie_in_genre(genre)

def get_retrieval_keys():
    return r.brpoplpush(RETRIEVAL_LIST_KEY, RETRIEVAL_LIST_KEY)

class Timeout():
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()


def unclear_movie_name(movie_name):
    filter_text = {"sure", "i don't", "none", "man",
                   "i love", "next", "no way", "let's chat", "that's"}
    if len(movie_name.strip().split()) <= 3 and all(word in title_filter_list for word in movie_name.strip().split()):
        return True
    elif movie_name in filter_text:
        return True

def is_potential_tv(knowledge):
    try:
        print("is_potential_tv", knowledge)
        if type(knowledge) == list:
            for entry in knowledge:
                if type(entry) != list:
                    continue
                if re.search(r"television|drama series|sitcom", entry[1].lower()) and float(entry[2]) > 100:
                    return True
        return False
    except:
        print("[MOVIECHAT_MODULE] Exception in is_potential_tv {}".format(traceback.format_exc()))
        return False 

def get_potential_tv_keywords(knowledge):
    try:
        keywords = "hmm"
        if type(knowledge) == list:
            max_score = -1
            for entry in knowledge:
                if type(entry) != list:
                    continue
                if re.search(r"television|drama series|sitcom", entry[1].lower()) and float(entry[2]) > 100:
                    if float(entry[2]) > max_score:
                        max_score = float(entry[2])
                        keywords = entry[0]
        return keywords
    except:
        print("[MOVIECHAT_MODULE] Exception in get_potential_tv_keywords {}".format(traceback.format_exc()))
        return "hmm"

# def detect_movietv_and_get_facts(text, ner, knowledge, strength, limit):
#     try:
#         with Timeout(1):
#             movie_infos, _ = MovieExtractor().extract_movies(
#                 text, ner, knowledge, strength=strength)
#             movie_info, posts = None, None
#
#             if movie_infos:
#                 movie_info = movie_infos[0]
#                 posts = get_imdb_trivia_new(movie_info["movie_id"], limit)
#                 if posts:
#                     posts = get_imdb_trivia(movie_info["movie_id"], limit)
#
#             return movie_info, posts
#     except Timeout.Timeout:
#         print("[MOVIECHAT_MODULE] Timeout in detect_movietv_and_get_facts")
#     return None, None

@trace_latency
def retrieve_reddit(utterance, subreddits, limit):
    REDDIT_SEARCH_PREFIX = "gunrock:module:moviechat:reddit_search_prefix"
    utterance = utterance.lower()
    subreddits.sort()
    subreddits = '+'.join(subreddits)
    key = utterance + "_" + subreddits + "_" + str(limit)
    print("[retrieve_reddit] key", key)
    cache = tmdb_get_redis(REDDIT_SEARCH_PREFIX, key)

    if cache is not None:
        print("[retrieve_reddit] cache hit")
        return cache
    print("[retrieve_reddit] cache miss")

    keys = get_retrieval_keys()
    c_id, c_secret, u_agent = keys.split("::")

    reddit = praw.Reddit(client_id=c_id,
                         client_secret=c_secret,
                         user_agent=u_agent,
                         timeout=2
                         #username='gunrock_cobot', # not necessary for read-only
                         #password='ucdavis123'
                        )
    subreddit = reddit.subreddit(subreddits)
    results = list(subreddit.search(utterance, limit=limit)) # higher limit is slower; delete limit arg for top 100 results
    value = parse_retrieve_reddit_results(results)
    if len(value)==0:
        value = None
    # currently use expire period of three months; don't expire until after
    # finals
    tmdb_set_redis(REDDIT_SEARCH_PREFIX, key, value)
    return value

def parse_retrieve_reddit_results(results):
    output = []
    for res in results:
        data = {
            "title": res.title,
            "subreddit": res.subreddit.display_name,
            "score": res.score,
            "num_comments": res.num_comments,
            "created_utc": res.created_utc,
        }
        output.append(data)
    return output

# note: order of keywords when searching does affect results
# each keywords(s) in keywords must appear in the result, not necessarily in the same order
def get_interesting_reddit_posts(keywords, limit):
    try:
        with Timeout(1):
            num_query = 5
            text = " ".join(keywords)
            submissions = retrieve_reddit(text, ['todayilearned'], num_query)
            if submissions is not None:
                results = []
                print("[get_interesting_reddit_posts] number of facts retrieved - initial", len(submissions))
                # highest score posts first
                submissions.sort(reverse=True, key=lambda x: x["score"])                
                for submission in submissions:
                    # title post processing
                    title = submission["title"].replace("TIL", "").strip()
                    if title[-1]!='.' and title[-1]!='!' and title[-1]!='?':
                        title += "."
                    # filters
                    title_lower = title.lower()
                    if submission["score"] < 100:
                        print("[get_interesting_reddit_posts] filtered post by score")
                    elif len(title) > 300:
                        print("[get_interesting_reddit_posts] filtered post by length")                      
                    elif any(not re.search(r"\b"+keyword.lower()+r"\b", title_lower) for keyword in keywords):
                        print("[get_interesting_reddit_posts] filtered post by keywords")
                    elif any(re.search(x, title_lower) for x in blacklist):
                        print("[get_interesting_reddit_posts] filtered post by blacklist")   
                    elif any(fuzz.token_set_ratio(title_lower, res.lower()) > 70 for res in results):
                        print("[get_interesting_reddit_posts] filtered post by duplicate")
                    else:
                        results.append(title)
                #Sensitive Words Pattern
                # sort based on score
                print("[get_interesting_reddit_posts] number of facts retrieved - final", len(results))
                if len(results) > 0:
                    return results[:limit]
            else:
                print("[get_interesting_reddit_posts] retrieve_reddit returned None")
                return None
    except Timeout.Timeout:
        print("[MOVIECHAT_MODULE] Timeout in get_interesting_reddit_posts {}".format(traceback.format_exc()))
        return None   
    except Exception as e:
        print("[MOVIECHAT_MODULE] Exception in get_interesting_reddit_posts {}".format(traceback.format_exc()))
        return None

def get_imdb_trivia(movie_id, limit=10):
    try:
        movie_id = str(movie_id)
        data = RedisHelper().hgetall(RedisHelper.MOVIE_ID_DETAILED_PREFIX, movie_id, False)
        if data:
            trivia = ast.literal_eval(data["trivia"])
            final_results = []
            for result in trivia:
                if not any(re.search(x, result.lower()) for x in blacklist):
                    final_results.append(result)
            final_results = final_results[:limit]
            if len(final_results) > 0:
                return final_results
        return None
    except Exception as e:
        return None


@trace_latency
def get_actor_trivia_from_reddit(actor_name):
    data = RedisHelper().get(RedisHelper.MOVIE_ACTOR_PREFIX, actor_name)

    if data:
        RedisHelper().set(RedisHelper.MOVIE_ACTOR_PREFIX, actor_name, data, 14 * 24 * 60 * 60)
    else:
        try:
            data = get_interesting_reddit_posts([actor_name], 3)
        except Exception as e:
            logging.error(f"[get_actor_trivia_from_reddit] fails: {e}")

    return data


@trace_latency
def get_imdb_trivia_new(movie_id, limit=10) -> List[str]:
    try:
        movie_id = str(movie_id)
        data = RedisHelper().get(RedisHelper.MOVIE_ID_DETAILED_PREFIX, movie_id)

        if data:
            trivia = data["trivia"]
            final_results = []
            for result in trivia:
                if not any(re.search(x, result.lower()) for x in blacklist):
                    final_results.append(result)
            final_results = final_results[:limit]
            if len(final_results) > 0:
                return final_results
        return []
    except Exception as e:
        return []


def get_movie_info(movie_id):
    cache = RedisHelper().get(RedisHelper.MOVIE_ID_BRIEF_INFO_REFIX, str(movie_id))

    if cache:
        return cache

    movie = retrieve_movie_info(movie_id)

    movie_info = {
        "movie_name": movie.get("title", "").replace(": ", " "),
        "movie_id": movie_id,
        "movie_overview": movie.get("overview", ""),
        "movie_genre_ids": [genre.get("id") for genre in movie.get("genres", [])],
        "movie_leading_actor": get_leading_actor_role(movie_id),
        "movie_release_year": movie.get("release_date", "").split("-")[0],
        "movie_popularity": movie.get("popularity", 0),
        "movie_vote_average": movie.get("vote_average", None)
    }

    RedisHelper().set(RedisHelper.MOVIE_ID_BRIEF_INFO_REFIX, movie_info)

    return movie_info

@trace_latency
def get_movie_plot_summary(movie_id):
    try:
        movie_id = str(movie_id)

        data = RedisHelper().get(RedisHelper.MOVIE_ID_DETAILED_PREFIX, movie_id)
        logging.debug("[get_movie_plot_summary] data: {}".format(data))

        plot_summaries = None

        if data:
            plot_summaries = data.get("plotsummary", None)

        if not plot_summaries:
            plot_summaries = get_plot_summary(movie_id)
            RedisHelper().set(RedisHelper.MOVIE_ID_DETAILED_PREFIX, movie_id, {"plotsummary": plot_summaries})

        return plot_summaries[0].replace(": ", " ")
    except Exception as e:
        logging.warn("fail to get movie plot".format(e))
        return None


class Genre(Enum):
    ACTION = 28
    ADVENTURE = 12
    ANIMATION = 16
    COMEDY = 35
    CRIME = 80
    DOCUMENTARY = 99
    DRAMA = 18
    FAMILY = 10751
    FANTASY = 14
    HISTORY = 36
    HORROR = 27
    MUSIC = 10402
    MYSTERY = 9648
    ROMANCE = 10749
    SCIENCE_FICTION = 878
    TV_MOVIE = 10770
    THRILLER = 53
    WAR = 10752
    WESTERN = 37
    NA = -1

    @classmethod
    def from_int(cls, id: int):
        try:
            return cls(id)
        except Exception as e:
            return None

    def get_name(self):
        return re.sub("_", " ", self.name.lower())


@trace_latency
def get_movie_genres(movie_id) -> List[Genre]:
    movie_id = str(movie_id)
    data = RedisHelper().get(RedisHelper.MOVIE_ID_DETAILED_PREFIX, movie_id)
    if data:
        logging.info(f"data: {data}")
        genres = data.get("genre_id", [])
        genre_ids = [Genre.from_int(item) for item in genres]
        return genre_ids
    else:
        return []


def sort_genres(genres: List[Genre], prioritized_genres: List[Genre]) -> List[Genre]:
    if not genres:
        return []

    results = []
    for genre in prioritized_genres:
        if genre in genres:
            results.append(genre)

    for genre in genres:
        if genre not in results:
            results.append(genre)
    return results

@trace_latency
def get_leading_actor_role(movie_id):
    try:
        movie_id = str(movie_id)
        actor_info = RedisHelper().get(RedisHelper.MOVIE_ID_LEADING_ACTOR, movie_id)

        if actor_info:
            return actor_info

        data = RedisHelper().get(RedisHelper.MOVIE_ID_DETAILED_PREFIX, movie_id)

        if data:
            actors = data["actors"]
        else:
            actors = get_credits(movie_id)

        result = {
            "name": actors[0]["name"],
            "role": actors[0]["character"],
        }

        logging.info(f"get_leading_actor_role : {result}")

        RedisHelper().set(RedisHelper.MOVIE_ID_LEADING_ACTOR, movie_id, result)

        return result

    except Exception as e:
        logging.error("[Movie Extractor] Exception in get_leading_actor_role {}".format(e))
        return None


def constructDynamicResponse(candidateResponses):
    if type(candidateResponses) == list:
        selectedResponse = random.choice(candidateResponses)
    else:
        selectedResponse = candidateResponses
    finalString = ""
    choicesBuffer = ""
    flag = False
    for char in selectedResponse:
        if char == "(":
            flag = True
        elif char == ")":
            flag = False
            choices = choicesBuffer.split("|")
            finalString += random.choice(choices)
            choicesBuffer = ""
        if flag == True and char != "(" and char != ")":
            choicesBuffer += char
        elif flag == False and char != "(" and char != ")":
            finalString += char
    finalString = " ".join(finalString.split())
    return finalString

def utterance(key: List[str], get_all: bool = False):
    """
    :param key: a list of str that represents the nested dict keys.
                e.g. {key1: {key2: {key3: text}}}, you do ['key1', 'key2', 'key3']
    :param get_all:
    :return: either a list, or an instance of, the utterance, and the hash of the utterance
    terry - not using hash right now because my template format has some qa pairs that don't work with hash
    """
    # return "what what", "hash"
    try:
        # print(key)
        #print("data", json.dumps(data, indent=4))
        data = mdata[key[0]]
        for i in key[1:]:
            data = data[i]

        #print("data1", json.dumps(data, indent=4))
        data = [d['text'] for d in data]
        #print("data2", json.dumps(data, indent=4))

        if get_all:
            return data  # type: List[Tuple[str, str]]
        else:
            return random.choice(data)  # type: Tuple[str, str]
            # print(key)

    except Exception as e:
        # To keep cobot running, catch everything!
        return "Oh no, I think my creators misspelled something in my code. Would you mind asking something else?"

def _truncate_response(text, limit):
    print("[MOVIECHAT] truncate result with  text: {}, limit: {}", text, limit)
    sentences = re.split("\?|\.|\,|\!|\n", text)
    num_words = len(text.split())

    if num_words <= limit:
        return text
    else:
        word_in_sent = 0
        for i in range(len(sentences)):
            word_in_sent += len(sentences[i].split())
            if word_in_sent > limit:
                if i == 0:
                    return "well i have a lot of thoughts on this, but it's not gonna be easy to talk about it now unless you are free until tomorrow night. maybe we should talk about something else"
                else:
                    return ". ".join(word for word in sentences[:i+1])

def parse_configs():
    import os
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    config = os.path.join(THIS_FOLDER, 'config.yml')
    with open(config, "r") as f:
        try:
            cfgs = AttrDict(yaml.load(f))
            print("Loaded configs: {}".format(json.dumps(cfgs, indent=4)))
            return cfgs
        except yaml.YAMLError as exc:
            print("[MOVIECHAT_MODULE] Exception in parse_configs {}".format(
                traceback.format_exc()))
