import re
from tmdbv3api import TMDb, Movie
import random
from fuzzywuzzy import fuzz
import traceback
from num2words import num2words
import hashlib
from tmdb_utils import *
from util_redis import RedisHelper
import logging
from typing import List, Optional
from enum import Enum
from utils import *
from movietv_utils import get_leading_actor_role, Genre

class MoviePreferenceExtractor:
    pass


class MovieNER:

    @staticmethod
    def fetch(user_utterance, last_bot_response):
        try:
            MOVIE_NER_URL = "http://169.237.4.21:5013/movie_NER"
            TIMEOUT = (0.2, 0.5)
            headers = {'content-type': 'application/json'}

            request_url = MOVIE_NER_URL
            data = {"user_utterance": user_utterance, "last_bot_response": last_bot_response}
            data = json.dumps(data)
            result = requests.post(url=request_url, data=data, headers=headers, timeout=TIMEOUT).json()

            logging.info("[fetch_movie_ner] results: {}".format(result))
        except Exception as e:
            logging.info("[fetch_movie_ner] error: {}".format(e))
            result = {}
        return result


class MovieExtractor:
    TMDB_API_KEYS = ['cfd14412e3951047cd49fc48c56369b2',
                     '2b08f9a99dd827c4ba952e4202162f72']

    MOVIE_ID_PREFIX = "gunrock:module:moviechat:movie_id"

    movie_re = [
        r"have you.* (seen|watched) the movie ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
        r"have you.* (seen|watched) ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
        r"my.* favorite.* movie is ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
        r"you.* know the movie ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
        r"(\bsaw\b|about|\bwatched|\bsee(n|)\b) the movie ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
        r"talk(ing|) about ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
        r"tell me about ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
        r"like the movie ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
        r"((a|the|one|1) movie )?(i|we) (saw|watched)( was)? ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
        # r"(yes|yeah)( (the|one|a) movie)?( (i|we) (saw|watched))?( was)? ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
        r"(probably|maybe| might be|would be) ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))"
    ]

    def extract_movies_v2(self, user_utterance, last_bot_response) -> List[dict]:
        fetch_result = MovieNER.fetch(user_utterance, last_bot_response)

        processed_results = self.post_process_movie_ner(fetch_result)

        return processed_results

    def post_process_movie_ner(self, fetched_results) -> List[dict]:
        if not fetched_results:
            return []
        movies = fetched_results.get("movie", [])
        movie_names = fetched_results.get("movie_raw", [])

        movie_results = []
        if movies:
            for entry in movies:
                if entry.get("type", "") == "movie":
                    movie_info = MovieExtractor.convert_ner_server_format_to_fsm_format(entry)
                    movie_results.append(movie_info)

                if entry.get("type", "") == "movieSeries":
                    series_id = entry["tmdbId"]
                    if series_id:
                        movies = tmdb_get_all_movies_in_collection(series_id)
                        sorted_movies = sort_movies_by_popularity(movies)

                        # TODO: Currently assume user is talking about the most popular one. Need to change once we have a movie series flow
                        most_popular_movie = sorted_movies[0]
                        movie_info = MovieExtractor.convert_tmdb_movie_format_to_fsm_format(most_popular_movie)

                        return [movie_info]

        if movie_names:
            for name in movie_names:
                movies = self._search_with_movie_name(name, None)
                movie_results.extend(movies)

        return movie_results

    @staticmethod
    def convert_ner_server_format_to_fsm_format(movie_from_ner):
        movie_title = movie_from_ner["title"].replace(":", "")

        movie_info = {
            "movie_name": movie_title,
            "movie_id": movie_from_ner["tmdbId"],
            "movie_leading_actor": get_leading_actor_role(movie_from_ner["tmdbId"]),
            "movie_release_year": str(movie_from_ner["year"]),
            "movie_popularity": movie_from_ner["popularity"]
        }

        return movie_info

    @staticmethod
    def convert_tmdb_movie_format_to_fsm_format(tmdb_movie):
        movie_title = tmdb_movie["title"].replace(":", "")
        movie_info = {
            "movie_name": movie_title,
            "movie_id": tmdb_movie["id"],
            "movie_leading_actor": get_leading_actor_role(tmdb_movie["id"]),
            "movie_release_year": tmdb_movie["release_date"].split("-")[0],
            "movie_popularity": tmdb_movie["popularity"]
        }
        return movie_info

    # class MovieSeries(Enum):
    #     AVENGERS = 86311
    #     STAR_WAR = 10
    #     HOBBIT = 121938
    #     HARRY_POTTER = 1241
    #     SPIDER_MAN = 556
    #     X_MAN = 748
    #     LION_KING = 94032
    #
    # MOVIE_SERIES_REGEX = {
    #     r"\b(Avengers)\b": MovieSeries.AVENGERS,
    #     r"\b(Star Wars)\b": MovieSeries.STAR_WAR,
    #     r"\b(Hobbit)\b": MovieSeries.HOBBIT,
    #     r"\b(Harry Potter)\b": MovieSeries.HARRY_POTTER,
    #     r"\b(Spider-Man)\b": MovieSeries.SPIDER_MAN,
    #     r"\b(X-Man)\b": MovieSeries.X_MAN,
    #     r"\b(The Lion King)\b": MovieSeries.LION_KING,
    # }
    #
    # @staticmethod
    # def get_series_id_by_name(series_name):
    #     for series_regex in MovieExtractor.MOVIE_SERIES_REGEX:
    #         match = re.search(series_regex, series_name)
    #         if match:
    #             movie_series = MovieExtractor.MOVIE_SERIES_REGEX[series_regex]
    #             return movie_series.value
    #     return None


    #####################################
    # Old way of movie name extraction
    #####################################

    def extract_movies(self, text, ner=None, knowledge=None, asr_correction=None, strength=0) -> List[dict]:
        """

        :param text:
        :param ner:
        :param knowledge:
        :param asr_correction:
        :param strength:
        :param get_list:

        :return: movieinfo, movie name
        """

        logging.info("[Movie Extractor] try extracting movie name from text : {}".format(text))

        if self.no_movie_name_included(text):
            return []

        results = self._search_movie_with_regex_matched_movie_name(text, asr_correction)
        if results:
            return results

        if strength >= 1:
            results = self._search_movie_with_google_knowledge_graph(text, knowledge, asr_correction)
            if results:
                return results

        if strength == 2:
            logging.info("[Movie Extractor] search movie with whole text: {}".format(text))
            results = self._search_with_movie_name(text, asr_correction)
            if results:
                return results

        return []


    def no_movie_name_included(self, text):
        regex_list = [
            r"^(i agree)$",
            r"^nothing$",
            r"^(\w+ ){0,3}((that's|it's)( \w+){0,4})$",  # opinion: i think that's very hilarious
            r"^(\w+ ){0,3}(like(|d)|love(|d)|enjoy(|ed)) it$"  # likeness: yes i liked it
        ]

        for regex in regex_list:
            match = re.search(regex, text)
            if match:
                return True
        return False


    def _search_movie_with_regex_matched_movie_name(self, text, asr_correction):
        potential_movie_names = self._find_matched_movie_name_with_regex(text)

        for movie_name in potential_movie_names:
            results = self._search_with_movie_name(movie_name, asr_correction)
            if results:
                return results
        return []

    def _find_matched_movie_name_with_regex(self, text):
        text = re.sub("yes|yeah", "", text)
        matched_movie_names = []
        for expression in self.movie_re:
            if re.search(expression, text):
                regexresult = list(
                    filter(None, re.search(expression, text).groups()))
                if regexresult:
                    potential_movie_name = regexresult[-1]
                    if potential_movie_name in {"movie", "movies", "stuff"} or \
                            potential_movie_name in matched_movie_names:
                        continue
                    else:
                        matched_movie_names.append(potential_movie_name)

        logging.info("_find_matched_movie_name_with_regex: {}".format(matched_movie_names))

        return matched_movie_names

    def _search_movie_with_google_knowledge_graph(self, text, knowledge_graph, asr_correction) -> List[dict]:
        movie_name = None
        try:  # sometimes the knowledge format is erroneous
            max_score = -1
            for entry in knowledge_graph:
                if type(entry) != list:
                    continue
                knowledge_filter = {"a movie", "last night",
                                    "the movie", "about movie", "a child"}
                if entry[0].lower() in knowledge_filter or fuzz.partial_ratio(entry[0].lower(), text) < 70:
                    print("filtered knowledge entry:", entry[0])
                    continue
                if entry[3] == "movie" and float(entry[2]) > max_score:
                    max_score = float(entry[2])
                    movie_name = entry[0]

            if movie_name:
                results = self._search_with_movie_name(movie_name, asr_correction)
                return results

        except Exception as e:
            logging.error("[Movie Extractor] Exception in searching movie name with google kg: {}".format(e))

        return []

    def _search_with_movie_name(self, movie_name_query, asr_correction) -> List[dict]:

        TMDb.api_key = random.choice(self.TMDB_API_KEYS)

        try:
            movie_name_query = self._preprocess_movie_name(movie_name_query, asr_correction)
            processed_result = self.search_tmdb_movie_and_process_result(movie_name_query)

            if len(processed_result) == 0:
                movie_alt_name = self._get_alternative_movie_name(movie_name_query)
                if movie_alt_name:
                    processed_result = self.search_tmdb_movie_and_process_result(movie_alt_name)

            if len(processed_result) == 0:
                return []

            if len(processed_result) > 3:
                processed_result = processed_result[:3]

            logging.debug("[Movie Extractor] [_search_with_movie_name] movie found: {}".format(
                movie_name_query))
            return processed_result

        except Exception as e:
            logging.error("[Movie Extractor] Exception in search_with_movie_name {}".format(e))

        return []

    def _preprocess_movie_name(self, movie_name, asr_correction):
        normalized_movie_name = self._normalize_number_in_movie_name(movie_name)
        corrected_movie_name = self._correct_movie_name(normalized_movie_name, asr_correction)
        return corrected_movie_name

    def _normalize_number_in_movie_name(self, movie_name):
        # for roman numerals; don't replace i, v, or x because they are ambiguous, e.g. V for Vendetta, X Men
        replace_words = {
            "ii": "two", "iii": "three", "iv": "four", "vi": "six",
            "vii": "seven", "viii": "eight", "ix": "nine", "xi": "eleven",
            "xii": "twelve", "xiii": "thirteen",
        }
        res = []
        # remove non alphanumeric chars except for '; convert to lower case
        movie_name = ''.join(ch if (ch.isalnum() or ch == "'")
                             else " " for ch in movie_name.lower())
        for word in movie_name.split():
            try:
                # convert numbers
                word = int(word)
                word = num2words(word)
            except:
                # convert roman numerals
                if word in replace_words:
                    word = replace_words[word]
            res.append(word)
        return " ".join(res)

    def _correct_movie_name(self, movie_name_query, asr_correction):
        movie_name_query = movie_name_query.lower()

        corrected_movie_name = self._replace_movie_with_currently_playing_movie(movie_name_query)
        if corrected_movie_name is not None:
            return corrected_movie_name

        corrected_movie_name = self._correct_movie_name_with_regex(movie_name_query)
        if corrected_movie_name is not None:
            return corrected_movie_name

        corrected_movie_name = self._correct_movie_name_with_asr_correction(movie_name_query, asr_correction)
        if corrected_movie_name is not None:
            return corrected_movie_name

        return movie_name_query

    def _replace_movie_with_currently_playing_movie(self, movie_name_query):
        corrected_movie_name = None

        if re.search(
                r"ralph (rex|wreck|break).* the internet|new (rex|wreck).* it ralph|(rex|wreck).* it ralph (2|two|to)",
                movie_name_query):
            corrected_movie_name = "Ralph Breaks the Internet"
        elif re.search(r"(c|g)reed (2|two|to)|new (c|g)reed", movie_name_query):
            corrected_movie_name = "Creed II"
        elif re.search(r"robin(| )hood", movie_name_query):
            corrected_movie_name = "Robin Hood"
        elif re.search(r"green (book|blue)", movie_name_query):
            corrected_movie_name = "Green Book"
        elif re.search(r"fantastic.* beast(s|).* crime|new fantasic.* beast|fantastic.* beast(s|) (2|two|to)",
                       movie_name_query):
            corrected_movie_name = "Fantastic Beasts: The Crimes of Grindelwald"
        elif re.search(r"grinch|^the great$", movie_name_query):
            corrected_movie_name = "The Grinch"
        elif re.search(r"\bbohemian rhapsody\b", movie_name_query):
            corrected_movie_name = "bohemian rhapsody"
        elif re.search(r"\binstant famil(y|ies)\b", movie_name_query):
            corrected_movie_name = "Instant Family"
        elif re.search(r"widow(s|)|wittel(s|)|twiddle(s|)|^the little$|^where do$", movie_name_query):
            corrected_movie_name = "Widows"
        elif re.search(r"\bnut(| )cracker.* four realm", movie_name_query):
            corrected_movie_name = "The Nutcracker and the Four Realms"
        elif re.search(r"\b(a|the) star is born", movie_name_query):
            corrected_movie_name = "A Star Is Born"
        elif re.search(r"over(| )lord", movie_name_query):
            corrected_movie_name = "Overlord"
        elif re.search(r"\bgirl.* spider.* web", movie_name_query):
            corrected_movie_name = "The Girl in the Spider's Web"

        logging.info(
            "[Movie Extractor][_replace_movie_with_currently_playing_movie] regex corrected currently playing movie name: {}".format(
                corrected_movie_name))

        return corrected_movie_name

    def _correct_movie_name_with_regex(self, movie_name_query):
        corrected_movie_name = None

        if re.search(r"infinity war|new avenger", movie_name_query):
            corrected_movie_name = "avengers infinity war"
        elif re.search(r"avenger.* age of", movie_name_query):
            corrected_movie_name = "avengers age of ultron"
        elif re.search(r"avenger", movie_name_query) and not re.search("infinity|war|captain|america|toxic",
                                                                       movie_name_query):
            corrected_movie_name = "the avengers"
        elif re.search(r"mama mia.* here we go|mama mia (2|two|to)", movie_name_query):
            corrected_movie_name = "mamma mia here we go again"
        elif re.search(r"mama mia", movie_name_query):
            corrected_movie_name = "mamma mia"
        elif re.search(r"blade runner (twenty|two thousand) forty nine|new blade runner", movie_name_query):
            corrected_movie_name = "blade runner 2049"
        elif re.search(r"new mission impossible|mission impossible fall out", movie_name_query):
            corrected_movie_name = "mission impossible fallout"
        elif re.search(r"wally", movie_name_query):
            corrected_movie_name = "WALL·E"
        elif re.search(r"ground hog day", movie_name_query):
            corrected_movie_name = "groundhog day"
        elif re.search(r"la land", movie_name_query):
            corrected_movie_name = "la la land"
        elif re.search(r"beast.* twenty thousand fathoms", movie_name_query):
            corrected_movie_name = "the beast from 20,000 fathoms"
        elif re.search(r"movie it\b", movie_name_query) or (
                re.search(r"\bit\b", movie_name_query) and re.search(r"2017|seventeen|clown",
                                                                     movie_name_query) and not re.search(r"follow",
                                                                                                         movie_name_query)):
            corrected_movie_name = "it 2017"
        elif re.search(r"movie saw\b", movie_name_query):
            corrected_movie_name = "jigsaw"
        elif re.search(r"\balpha\b", movie_name_query) and not re.search("dog|house|partridge|omega|toxic",
                                                                         movie_name_query):
            corrected_movie_name = "alpha"
        elif re.search(r"\binterstellar\b", movie_name_query):
            corrected_movie_name = "interstellar"
        elif re.search(r"\binception\b", movie_name_query):
            corrected_movie_name = "inception"
        elif re.search(r"\bhalloween\b", movie_name_query) and not re.search(r"goosebumps", movie_name_query):
            corrected_movie_name = "halloween"
        elif re.search(r"\bvenom\b", movie_name_query):
            corrected_movie_name = "venom"
        elif re.search(r"\b(first|1st) man\b", movie_name_query):
            corrected_movie_name = "first man"
        elif re.search(r"\bsmall(| )foot\b", movie_name_query):
            corrected_movie_name = "smallfoot"
        elif re.search(r"\bnight school\b", movie_name_query):
            corrected_movie_name = "night school"
        elif re.search(r"\bmid (nineties|90s)\b", movie_name_query):
            corrected_movie_name = "mid90s"
        elif re.search(r"\bgoosebumps (2|two)\b", movie_name_query):
            corrected_movie_name = "goosebumps 2 haunted halloween"
        elif re.search(r"\bhunter killer\b", movie_name_query):
            corrected_movie_name = "hunter killer"
        elif re.search(r"\bthe hate you give\b", movie_name_query):
            corrected_movie_name = "the hate u give"
        elif re.search(r"\bold man.* gun\b", movie_name_query):
            corrected_movie_name = "the old man and the gun"
        elif re.search(r"\bjohnny english (tries|strikes) again\b", movie_name_query):
            corrected_movie_name = "johnny english strikes again"
        elif re.search(r"\bindivisible\b", movie_name_query):
            corrected_movie_name = "indivisible"
        elif re.search(r"\btimes.* el royale\b", movie_name_query):
            corrected_movie_name = "bad times at the el royale"
        elif re.search(r"\bnobody('s|) fool\b", movie_name_query):
            corrected_movie_name = "nobody's fool"
        elif re.search(r"\bboy (you |)race\b", movie_name_query):
            corrected_movie_name = "boy erased"

        logging.info("[Movie Extractor][movie_name_replace] regex corrected movie name: {}".format(corrected_movie_name))

        return corrected_movie_name

    def _correct_movie_name_with_asr_correction(self, movie_name_query, asr_correction):
        # sometimes random error where asr_correction is a list
        # asr corrected movie names
        no_need_to_be_corrected_movies = ["frozen 2"]
        if movie_name_query in no_need_to_be_corrected_movies:
            return None

        corrected_movie_name = None
        try:
            if asr_correction:
                for key, corrected in asr_correction.items():
                    if movie_name_query == key:
                        for candidate in corrected:
                            if fuzz.ratio(movie_name_query.lower(), candidate.lower()) > 75:
                                corrected_movie_name = candidate
                            elif fuzz.partial_ratio(movie_name_query.lower(), candidate.lower()) > 75 and max(
                                    len(movie_name_query.split()), len(candidate.split())) >= 2:
                                corrected_movie_name = candidate
                if corrected_movie_name is not None:
                    logging.info("[movie extractor][movie_name_replace] asr corrected movie name: {} {}".format(
                        corrected_movie_name))
                    return corrected_movie_name
        except Exception as e:
            logging.error("[Movie Extractor] Exception with asr_correction {}".format(e))
        return None

    def _get_alternative_movie_name(self, movie_name_query):
        replace_words = {"one": "1", "two": "2", "three": "3", "four": "4", "five": "5",
                         "six": "6", "seven": "7", "eight": "8", "nine": "9", "ten": "10",
                         "eleven": "11", "twelve": "12", "thirteen": "13", "fourteen": "14", "fifteen": "15",
                         "1st": "first", "2nd": "second", "3rd": "third", "4th": "fourth", "5th": "fifth",
                         "6th": "sixth", "7th": "seventh", "8th": "eighth", "9th": "ninth", "10th": "tenth",
                         "11th": "eleventh", "12th": "twelfth", "13th": "thirteenth", "14th": "fourteenth",
                         "15th": "fifteenth"}
        title = []
        replacement_done = False
        for word in movie_name_query.split():
            if word in replace_words:
                word = replace_words[word]
                replacement_done = True
            title.append(word)

        if replacement_done:
            alternate_movie_name = " ".join(title)
            logging.info("[Movie Extractor][_get_alternative_movie_name] movie name: {}".format(alternate_movie_name))
            return alternate_movie_name

        return None

    def search_tmdb_movie_and_process_result(self, movie_name_query) -> List[dict]:
        results = tmdb_movie_search(movie_name_query)
        results = sorted(results, key=lambda t: t['popularity'], reverse=True)
        processed_result = self._process_tmdb_movie_search_results(movie_name_query, results)
        return processed_result

    def _process_tmdb_movie_search_results(self, movie_name_query, results, max_results=5) -> List[dict]:
        logging.debug("[movie extractor][process_tmdb_movie_search_results] number of init results: {}".format(len(results)))
        processed_result = []
        movie_name_query = self._normalize_number_in_movie_name(movie_name_query)
        for entry in results[:max_results]:
            try:
                entry["title"] = self._normalize_number_in_movie_name(entry["title"])
                movie_name = entry["title"]
                if self._is_good_movie_entry(movie_name_query, entry):
                    movie_info = {
                        "movie_name": movie_name,
                        "movie_id": entry["id"],
                        "movie_overview": entry["overview"],
                        "movie_genre_ids": entry["genre_ids"],
                        "movie_leading_actor": get_leading_actor_role(entry["id"]),
                        "movie_release_year": entry["release_date"].split("-")[0],
                        "movie_popularity": entry["popularity"]
                    }

                    if MovieExtractor.are_almost_the_same_movie_name(movie_name_query, movie_name, 95):
                        logging.info(
                            "[Movie Extractor][process_tmdb_movie_search_results] found good movie match - movie_name_query: {}, movie_name: {}".format(
                                movie_name_query, movie_name))
                        return [movie_info]
                    elif MovieExtractor.is_movie_series(movie_name):
                        # todo: temporary returning the most popular movie in the series to avoid weird grounding
                        return [movie_info]
                    else :
                        processed_result.append(movie_info)

            except Exception as e:
                logging.error("[Movie Extractor] Exception in process_tmdb_movie_search_results {}".format(e))

        logging.info("[Movie Extractor][process_tmdb_movie_search_results] number of final results:{}".format(len(processed_result)))

        return processed_result


    def _is_good_movie_entry(self, movie_name_query, entry):
        movie_id = str(entry["id"])
        movie_name = entry["title"]
        regex_filter = r"^((the | this |that |about |a |)(movie(s|)|serie(s|)|trilogy)|the|this|that('s|)|about|a|new(s|)|yes|no|nope|okay|sure|(i |)love|i think|well|what('s|)|it('s|)|tv|i|(i |)read|read|(the movie |)before|something|anymore|anything|thank you|a lot|(a |)name|talk|chat|him)$"
        movie_id_keyword_mapping = {
            "317442": "naruto", "378236": "emoji", "102899": "ant", "15789": "goofy",
        }

        if not MovieExtractor.are_potentially_same_movie(movie_name_query, movie_name, 75):
            logging.debug(
                "[Movie Extractor][is_good_movie_entry] fuzz partial ratio too low - movie_name_query: {}, movie_name: {}".format(
                    movie_name_query, movie_name))
            return False
        elif not self._is_popular_movie(entry):
            logging.debug("[Movie Extractor][is_good_movie_entry] movie popularity too low: {}".format(movie_name))
            return False
        elif re.search(regex_filter, movie_name_query):
            logging.debug("[Movie Extractor][is_good_movie_entry] failed regex filter: {}".format(movie_name))
            return False
        elif movie_id in movie_id_keyword_mapping and movie_id_keyword_mapping[movie_id] not in movie_name_query:
            logging.debug("[Movie Extractor][is_good_movie_entry] missing keyword in movie name: {}".format(movie_name))
            return False

        return True

    @staticmethod
    def are_potentially_same_movie(movie_name_1, movie_name_2, confidence_threshold=95):
        partial_ratio = fuzz.partial_ratio(
            MovieExtractor.remove_definite_article_in_the_beginning(movie_name_1),
            MovieExtractor.remove_definite_article_in_the_beginning(movie_name_2))
        ratio = fuzz.token_sort_ratio(
                   MovieExtractor.remove_definite_article_in_the_beginning(movie_name_1),
                   MovieExtractor.remove_definite_article_in_the_beginning(movie_name_2))
        return partial_ratio >= confidence_threshold and \
               ratio >= 40

    @staticmethod
    def are_almost_the_same_movie_name(movie_name_1, movie_name_2, confidence_threshold=95):
        ratio = fuzz.ratio(
            MovieExtractor.remove_definite_article_in_the_beginning(movie_name_1),
            MovieExtractor.remove_definite_article_in_the_beginning(movie_name_2))
        return ratio >= confidence_threshold

    @staticmethod
    def remove_definite_article_in_the_beginning(name):
        return re.sub(r"^\b(the )\b", "", name)

    @staticmethod
    def is_movie_series(movie_name):
        MOVIE_SERIES = [
            "harry potter",
            "hobbit",
            "the lord of the rings",
            "The Chronicles of Narnia",
            "Star wars",
            "Pirates of the Caribbean",
            "X man",
            "Transformer"
        ]

        for series_name in MOVIE_SERIES:
            if series_name in movie_name:
                return True
        return False

    def _is_popular_movie(self, entry):
        if entry["popularity"] > 6.5 or entry["vote_count"] > 5000:
            return True
        try:
            release_year = float(entry["release_date"].split("-")[0])
            if release_year < 1970 and entry["popularity"] > 4:
                return True
            elif release_year < 1980 and entry["popularity"] > 4.5:
                return True
            elif release_year < 1990 and entry["popularity"] > 5:
                return True
            elif release_year < 2000 and entry["popularity"] > 5.5:
                return True
            elif release_year < 2010 and entry["popularity"] > 6:
                return True
        except:
            return False

        return False


class PersonExtractor:
    class PersonType:
        ACTOR = "actor"
        DIRECTOR = "director"

    regex_map = {
        PersonType.ACTOR: r"\bactor|\bactress",
        PersonType.DIRECTOR: "director|filmmaker"
    }

    def extract_person_names(self, person_type: str, name_entities, knowledge_entities, user_utterance="", last_response="") -> List[str]:
        person_names = []

        if user_utterance.split()[-1] in ["leo", "leonardo"]:
            return ["Leonardo DiCaprio"]

        if name_entities:
            for name_entity in name_entities:
                actor_ner_labels = {"PERSON", "ORG"}  # ORG because some actors are detected as ORG, e.g. ryan gosling
                if name_entity["label"] in actor_ner_labels:
                    try:
                        for knowledge_entity in knowledge_entities:
                            entity_name = knowledge_entity[0].lower()
                            entry_text = name_entity["text"].lower()
                            if re.search(self.regex_map[person_type], knowledge_entity[1].lower()) and \
                                    fuzz.partial_ratio(entity_name, entry_text) > 50:
                                person_names.append(name_entity["text"])
                    except Exception as e:
                        logging.info("[Person Extractor] Exception in extract_person_names {}".format(repr(e)))

        if not person_names:
            result = MovieNER.fetch(user_utterance, last_response)
            people = PersonExtractor.post_process_movie_ner_result(result, person_type)
            person_names.extend(people)
        return person_names



    @staticmethod
    def post_process_movie_ner_result(fetched_results, person_type) -> List[str]:

        peoples = fetched_results.get("people", [])

        peoples_result = []
        if peoples:
            for entry in peoples:
                if re.search(PersonExtractor.regex_map[person_type], entry.get("description", "")):
                    peoples_result.append(entry.get("name"))

        return peoples_result


class ActorExtractor(PersonExtractor):
    strong_intents = [
        r"you.* (know|like|heard of)( the (actor|actress)|) ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+))",
        r"^(?!.*(not|never|don't)).*(about the (actor|actress) ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)))",
        r"^(?!.*(not|never|don't)).*(talk(ing|) about ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)))",
        r"^(?!.*(not|never|don't)).*(tell me about ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)))",
    ]

    def extract_actor_with_info(self, name_entities, knowledge):
        if name_entities is None:
            return None
        actor_names = self.extract_actor_names(name_entities, knowledge)
        person_infos = self._get_actor_infos(actor_names)

        return person_infos if person_infos else None

    def extract_actor_names(self, name_entities, knowledge_entities, user_utterance="", last_response="") -> List[str]:
        return super().extract_person_names(PersonExtractor.PersonType.ACTOR, name_entities, knowledge_entities, user_utterance, last_response)

    def _get_actor_infos(self, actor_names) -> List[dict]:
        person_infos = []
        for actor_name in actor_names:
            try:
                results = tmdb_person_search(actor_name)
                result = self._get_best_actor_match(actor_name, results)
                if result is not None:
                    result["ner_term"] = actor_name
                    person_infos.append(result)
            except Exception as e:
                logging.error("[Actor Extractor] Exception in _get_actor_infos {}".format(repr(e)))

        logging.info("[Actor Extractor] extracted actor: {}".format(person_infos))
        return person_infos

    def _get_best_actor_match(self, query_term, results):
        query_term = query_term.lower()
        for entry in results:
            name = entry["name"].lower()
            if fuzz.token_set_ratio(query_term, name) > 95 and fuzz.partial_ratio(query_term, name) > 50:
                return entry
        return None

    def extract_potential_actor_name_with_strong_regex_intent(self, text):
        potential_names = []
        for expression in self.strong_intents:
            if re.search(expression, text):
                regexresult = list(filter(None, re.search(expression, text).groups()))
                if regexresult:
                    potential_name = regexresult[-1].lower()
                    potential_names.append(potential_name)
        return potential_names


class DirectorExtractor(PersonExtractor):
    def extract_director_names(self, name_entities, knowledge_entities):
        return super().extract_person_names(PersonExtractor.PersonType.DIRECTOR, name_entities, knowledge_entities)


class GenreExtractor:
    # list of genres from tmdb and its tmdb id
    class Genre(Enum):
        ACTION = 28
        ADVENTURE = 12
        ANIMATION = 16
        COMEDY = 35
        CRIME = 80
        DOCUMENTARY = 99
        DRAMA = 18
        FAMILY = 10751
        FANTASY = 14
        HISTORY = 36
        HORROR = 27
        MUSIC = 10402
        MYSTERY = 9648
        ROMANCE = 10749
        SCIENCE_FICTION = 878
        TV_MOVIE = 10770
        THRILLER = 53
        WAR = 10752
        WESTERN = 37

        def __str__(self):
            return re.sub("_", " ", self.name.lower())

        @classmethod
        def favorite_genre(cls):
            fav_genre = [cls.COMEDY, cls.ACTION, cls.SCIENCE_FICTION]
            return [str(genre) for genre in fav_genre]

    ALL_GENRE_REGEX = {
        r"\b(action|fight|stunt(s|)|explosion|car chase)\b": Genre.ACTION,
        r"\b(adventure)": Genre.ADVENTURE,
        r"\b(animation|anime|animated|car(| )toon|kids)\b": Genre.ANIMATION,
        r"\b(comed(ies|y))|((funny|hilarious) (movie|film))\b": Genre.COMEDY,
        r"\b(crime|heist)\b": Genre.CRIME,
        r"\b(documentary|non(| )fiction)\b": Genre.DOCUMENTARY,
        r"\b(drama(|s))\b": Genre.DRAMA,
        r"\b(family|\bkid(s|))\b": Genre.FAMILY,
        r"\b(fantas(y|ies)|magic|super(| )natural|monster|dragon)\b": Genre.FANTASY,
        r"\b(histor(ical|y)|period piece)\b": Genre.HISTORY,
        r"\b(horror|scar(e|y)|scream|frighten)\b": Genre.HORROR,
        r"\b(music(|al)|song|sing)\b": Genre.MUSIC,
        r"\b(mystery|detective)\b": Genre.MYSTERY,
        r"\b(romance|romantic|wedding|about love)\b": Genre.ROMANCE,
        r"\b(science fiction|sci(-| )fi)|science|fiction\b": Genre.SCIENCE_FICTION,
        r"\b(tv movie)\b": Genre.TV_MOVIE,
        r"\b(thriller(|s)|suspense(|s))\b": Genre.THRILLER,
        r"\b(western|cow(| )boy)\b": Genre.WESTERN,
    }

    MIXED_GENRE_REGEX = {
        r"\b(romcom)\b": [Genre.COMEDY, Genre.ROMANCE]
    }

    def extract_genre(self, text) -> List[int]:
        logging.info("[extract_genre]")
        results = None

        try:
            genre_results = []
            for genre_regex in GenreExtractor.ALL_GENRE_REGEX:
                match = re.search(genre_regex, text)
                if match:
                    genre = GenreExtractor.ALL_GENRE_REGEX[genre_regex]
                    genre_results.append(self.construct_genre_result(genre.name.lower(), genre.value, match.start()))

            for genre_regex in GenreExtractor.MIXED_GENRE_REGEX:
                match = re.search(genre_regex, text)
                if match:
                    mixed_genre_list = GenreExtractor.MIXED_GENRE_REGEX[genre_regex]
                    for genre in mixed_genre_list:
                        genre_results.append(self.construct_genre_result(genre.name.lower(), genre.value, match.start()))

            # Sort genre by putting the ones that appear earlier to the front
            genre_results = sorted(
                genre_results, key=lambda item: item['pos']
            )

            results = [item["genre_id"] for item in genre_results]
            results = unique(results)
        except Exception as e:
            print("[Genre Extractor] Exception in extract_genre {}".format(e))
        return results

    def construct_genre_result(self, genre_name: str, genre_id: int, position: int) -> dict:
        return {
            "genre_name": genre_name,
            "genre_id": genre_id,
            "pos": position
        }
