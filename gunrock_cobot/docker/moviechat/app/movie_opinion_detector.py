import logging
import re
from typing import Optional, List
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment

from nlu.constants import Positivity
from enum import Enum
from nlu.intentmap_scheme import MIN_2_WORDS


class MoviePreferenceLevel(Enum):
    SAD = "sad"
    HIGHLY_POSITIVE = "highly_positive"
    POSITIVE = "positive"
    NEUTRAL = "neutral"
    NEGATIVE = "negative"
    NOT_SURE = "not_sure"
    NA = "not_available"


class MovieLikeReason(Enum):
    MUSIC = "music"
    ACTION = "action"
    STORY = "story"
    SPECIFIC_SCENE = "specific_scene"
    ACTOR = "story"
    CHARACTER = "chracter"
    FUNNY = "funny"
    NA = "not_available"


class MovieDisikeReason(Enum):
    DEATH = "death"
    SCARY = "scary"
    GENERAL = "general"
    NA = "not_available"
    # FLAT = "flat"
    # TOO_LONG = "too_long"
    # ACTING = "acting"
    # UNORIGINAL = "unoriginal"
    # DUMB = "dumb"


class MovieOpinionDetector:
    reason_general =r".*because.*"

    def __init__(self, complete_user_utterance: str, returnnlp: Optional[List[dict]]):
        self.text = complete_user_utterance
        self.returnnlp = ReturnNLP.from_list(returnnlp)

    #############################
    #  Detect preference level
    #############################
    def detect_preference_level(self) -> MoviePreferenceLevel:
        if self.is_sad():
            return MoviePreferenceLevel.SAD
        if self.is_highly_positive():
            return MoviePreferenceLevel.HIGHLY_POSITIVE
        if self.is_positive_answer():
            return MoviePreferenceLevel.POSITIVE
        if self.is_negative():
            return MoviePreferenceLevel.NEGATIVE
        if self.is_neutral_answer():
            return MoviePreferenceLevel.NEUTRAL
        if self.is_not_sure():
            return MoviePreferenceLevel.NOT_SURE
        else:
            return MoviePreferenceLevel.NA

    def is_sad(self):
        sad = r".*\b(sad|cry|crying|cried|depressing)\b.*"

        return re.search(sad, self.text)

    def is_highly_positive(self):
        regex_highly_positive = r"^(?!.*(not|never|doesn't|don't)).*(super|great|fantastic|fascinating|amazing|marvelous|wonderful|awesome|exceptional|outstanding|exciting|favorite|best|masterpiece|lov(e|ed|ing)|enjoy)(.*)"
        return re.search(regex_highly_positive, self.text)

    def is_positive_answer(self):
        regex_positive = r"^(?!.*(not|never|doesn't|don't|didn't)).*(like|nice|good|like|amusing|hilarious|entertaining|fun)(.*)"
        regex_positive_blacklist = r"^.*\bok(ay)?\b"
        return not re.search(regex_positive_blacklist, self.text) and \
               self.returnnlp.answer_positivity == Positivity.pos or \
               self.returnnlp.has_intent("opinion_positive") or \
               re.search(regex_positive, self.text)

    def is_negative(self):
        regex_negative = r"^(?!.*(not|never|doesn't|don't)).*(poor|hate)(.*)"

        return self.returnnlp.answer_positivity == Positivity.neg or \
            self.returnnlp.has_intent("opinion_negative") or \
               re.search(regex_negative, self.text)

    def is_neutral_answer(self):
        regex_neutral = r"^.*\bok(ay)?\b"
        return self.returnnlp.sentiment == "neu" or re.search(regex_neutral, self.text)

    def is_not_sure(self):
        return self.returnnlp.has_intent("ans_unknown")


    ##########################
    # Detect why like movie
    ##########################
    like_music = r".*\b(music|soundtrack|song(|s))\b"
    like_action = r".*\b(action|fight(|ing))\b"
    like_story = r".*\b(story|plot)\b"
    like_specific_scene = rf".*(the scene|(when{MIN_2_WORDS}|the end))"
    like_actor = r".*\b('s acting|actor(|s)|actress|cast)\b"
    like_character = r".*\b(character)\b"
    like_funny = r".*\b(funny|fun|interesting|hilarious|comedy|laugh)\b"

    def detect_like_movie_reason(self) -> MovieLikeReason:
        if re.match(self.like_music, self.text):
            return MovieLikeReason.MUSIC
        elif re.match(self.like_action, self.text):
            return MovieLikeReason.ACTION
        elif re.match(self.like_story, self.text):
            return MovieLikeReason.STORY
        elif re.match(self.like_specific_scene, self.text):
            return MovieLikeReason.SPECIFIC_SCENE
        elif re.match(self.like_actor, self.text):
            return MovieLikeReason.ACTOR
        elif re.match(self.like_character, self.text):
            return MovieLikeReason.CHARACTER
        elif re.match(self.like_funny, self.text):
            return MovieLikeReason.FUNNY
        else:
            return MovieLikeReason.NA

    ##########################
    # Detect why dislike movie
    ##########################
    dislike_death = r".*\b(die|died|dying)"
    dislike_scary = r".*\b(scary)"

    def detect_dislike_movie_reason(self) -> MovieDisikeReason:
        if re.match(self.dislike_death, self.text):
            return MovieDisikeReason.DEATH
        elif re.match(self.dislike_scary, self.text):
            return MovieDisikeReason.SCARY
        elif re.match(self.reason_general, self.text) or \
                self.returnnlp.has_intent("ans_dislike") or \
                self.returnnlp.has_intent("opinion_negative"):
            return MovieDisikeReason.GENERAL
        else:
            return MovieDisikeReason.NA


    ##########################
    # Detect character
    ##########################
    regex_concept_character = r".*\bcharacter\b$"

    def detect_character(self):
        detected_character_from_concept = self.detect_character_in_concept()
        if detected_character_from_concept:
            return detected_character_from_concept
        else:
            return None

    def detect_character_in_concept(self):
        concepts_in_seg: List[ReturnNLPSegment.Concept]
        for concepts_in_seg in self.returnnlp.concept:
            for concept in concepts_in_seg:
                if concept.noun in ["character"]:
                    continue
                for data in concept.data:
                    if re.match(self.regex_concept_character, data.description):
                        return concept.noun
        return None

    # def detect_character_in_google_kg(self):
    #     google_kg_items_in_seg: List[ReturnNLPSegment.GoogleKG]
    #     for google_kg_items_in_seg in self.returnnlp.googlekg:
    #