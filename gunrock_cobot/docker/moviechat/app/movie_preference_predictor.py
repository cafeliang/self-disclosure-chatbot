import logging
from enum import Enum, auto

import requests
from tmdb_utils import get_popular_movies
from movietv_utils import get_movie_info
from typing import Optional, List
from random import choice
from utils import trace_latency

logger = logging.getLogger(__name__)

POPULAR_MOVIE_IDS = [
    299534,  # avenger endgame - action / sci-fi
    496243,  # parasite - thriller/comedy
    475557,  # joker - drama / crime
    466272,  # once upon a time in hollywood - Comedy/Drama
    546554,  # knives out - comedy/mystery
]

class MoviePreferencePredictor:
    URL_FIND_SIMILAR_MOVIE = 'http://ec2-54-175-156-102.compute-1.amazonaws.com:5000/find_similar_movie/'

    class Connection(Enum):
        SIMILAR_MOVIE = auto()
        POPULAR_MOVIE = auto()

    def __init__(self,
                 current_liked_movie_info: dict = None,
                 liked_genre_ids: List[int] = None,
                 discussed_movie_ids: list = None,
                 a_b_test='A'):
        logging.debug("[MoviePreferencePredictor]")
        self.current_movie_info = current_liked_movie_info

        self.current_movie_id = ""
        if current_liked_movie_info:
            movie_id = current_liked_movie_info.get("movie_id", "")
            self.current_movie_id = int(movie_id) if movie_id else ""

        self.liked_genre_ids = liked_genre_ids

        self.discussed_movie_ids = discussed_movie_ids
        self.a_b_test = a_b_test


    def pick_next_movie_with_info(self) -> (dict, str):
        movie_id, connection = self.pick_next_movie()
        return self.get_movie_info(movie_id), connection

    @trace_latency
    def pick_next_movie(self) -> (int, str):
        logging.debug("[pick_next_movie] current movie id: {}, info: {}".format(self.current_movie_id, self.current_movie_info))
        # todo: exclude discussed movie
        strategy = self.select_strategy_to_pick_next_movie()
        if strategy is MoviePreferencePredictor.Connection.SIMILAR_MOVIE:
            if self.current_movie_info:
                # try find similar movie
                similar_movie_id = self.find_a_similar_movie()
                if similar_movie_id:
                    # todo: filter out movie with same title and release date
                    #  (sometimes same movie has multiple ids on tmdb)
                    logging.debug("[pick_next_movie] found similar movie: {}".format(similar_movie_id))

                    return similar_movie_id, MoviePreferencePredictor.Connection.SIMILAR_MOVIE.name

        # try find popular movie
        popular_movie_id = self.find_a_popular_movie()
        logging.debug("[pick_next_movie] found popular movie: {}".format(popular_movie_id))

        return popular_movie_id, MoviePreferencePredictor.Connection.POPULAR_MOVIE.name

    def select_strategy_to_pick_next_movie(self) -> Connection:
        # Todo: improve the logic, like interleaving or user adaptation instead of randomly pick one
        return choice(list(MoviePreferencePredictor.Connection))

    @trace_latency
    def find_a_similar_movie(self) -> int:
        # return 330457  # hard code frozen two for now
        logging.debug("[find_similar_movies]")

        try:
            response = requests.get(url=MoviePreferencePredictor.URL_FIND_SIMILAR_MOVIE + str(self.current_movie_id),
                                    timeout=(0.1, 0.5))

            if response.status_code < 300:
                results = response.json()
                logging.debug(
                    "[find_similar_movies] current movie id: {}, results: {}".format(self.current_movie_id, results))

                if results:
                    for result in results:
                        similar_movie_id = result["movie_id"]
                        logging.debug(
                            "[find_similar_movies] similar_movie_id: {}".format(similar_movie_id))

                        if self.current_movie_info:
                            if self.is_same_movie(similar_movie_id, self.current_movie_id):
                                continue
                        if self.has_been_discussed(similar_movie_id):
                            continue
                        else:
                            return similar_movie_id
        except Exception as e:
            logging.error(f"[find_a_similar_movie] error: {e}")

        return None
        # todo: filter out discussed movie


    def is_same_movie(self, movie_id_1, movie_id_2):
        logging.debug("[is_same_movie] {}, {}".format(movie_id_1, movie_id_2))
        is_same_movie =  (movie_id_1 == movie_id_2)
        logging.debug("[is_same_movie] {}".format(is_same_movie))
        return is_same_movie

        # movie_1_info = self.get_movie_info(movie_id_1)
        # movie_2_info = self.get_movie_info(movie_id_2)
        # is_same_movie = movie_1_info["movie_name"] == movie_2_info["movie_name"] and \
        #     movie_1_info["movie_release_year"] == movie_2_info["movie_release_year"]
        #
        # logging.debug("[is_same_movie] {}".format(is_same_movie))

        # return is_same_movie

    def get_movie_info(self, movie_id: int) -> dict:
        logging.debug("[get_movie_info]: {}".format(movie_id))
        if movie_id == self.current_movie_id:
            return self.current_movie_info

        movie_info = get_movie_info(movie_id)

        return movie_info

    def find_a_popular_movie(self):
        movie_id = self.try_find_popular_movie_from_manually_created_list()
        if movie_id:
            return movie_id
        return self.find_popular_movie_from_tmdb()

    def try_find_popular_movie_from_manually_created_list(self) -> Optional[int]:
        for movie_id in POPULAR_MOVIE_IDS:
            if self.current_movie_info:
                if self.is_same_movie(movie_id, self.current_movie_id):
                    continue
            if self.has_been_discussed(movie_id):
                continue
            else:
                return movie_id

        return None

    def find_popular_movie_from_tmdb(self):
        movies = get_popular_movies()
        vote_avaerage_threshold = 6.5

        default_id = 330457  # frozen two

        # these movie are not popular in our users
        EXCLUDE_MOVIE_IDs = [
            419704,  # ad astra
            475303,  # a rainy day in new york
            454626,  # sonic the hedgehog
        ]

        # todo: find popular movie under liked genre
        logging.debug("[find a popular movie]: current movie: {}".format(self.current_movie_info))

        if movies:
            for movie in movies:
                result_movie_id = movie.id
                logging.debug("popular movie id: {}".format(result_movie_id))
                if movie.vote_average < 6.5:
                    continue
                if self.current_movie_info:
                    if self.is_same_movie(result_movie_id, self.current_movie_id):
                        continue
                if self.has_been_discussed(result_movie_id):
                    continue
                if result_movie_id in EXCLUDE_MOVIE_IDs:
                    continue
                else:
                    return result_movie_id

        return default_id

    def has_been_discussed(self, movie_id):
        if self.discussed_movie_ids:
            for discussed_movie_id in self.discussed_movie_ids:
                if self.is_same_movie(discussed_movie_id, movie_id):
                    logging.debug("movie {} has been discussed")
                    return True
        return False

