from tmdb_utils import *
import json
import pytest
import unittest


def test_plot_summary():
    results = get_plot_summary(605116)
    print(results)

def test_tmdb_recommend_movie():
    results = recommend_movie(movie_id=299534)
    print(results)


def test_tmdb_discover_movie():
    results = discover_movie(genre_id=35)
    print(results)

def test_retrieve_movie_info():
    movie_id = "299534"
    result = retrieve_movie_info(movie_id)
    print(result)


def test_tmdb_movie_search_harriet():
    movie_name = "harriet"
    test_tmdb_movie_search(movie_name)


def test_tmdb_movie_search_joker():
    movie_name = "joker"
    test_tmdb_movie_search(movie_name)


def test_tmdb_movie_search_matrix():
    movie_name = "matrix"
    test_tmdb_movie_search(movie_name)




@pytest.mark.parametrize("movie_name",
                         ["the avenger", "joker", "Jurassic Park", "Star Wars", "the lord of the ring", "frozen 2",
                          "how to train a dragon"])
def test_tmdb_movie_search(movie_name):
    movie_infos = tmdb_movie_search(movie_name)
    movie_infos = sorted(movie_infos, key=lambda t: t['popularity'], reverse=True)
    print(json.dumps(movie_infos, indent=4))

    assert len(movie_infos) > 0

    for movie_info in movie_infos:
        check_searched_movie_info(movie_info)


def test_tmdb_movie_collection():
    results = tmdb_get_all_movies_in_collection(10)
    print(json.dumps(results, indent=4))

@pytest.mark.parametrize("movie_id, movie_name, movie_release_year",
                         [(200, "Star Trek: Insurrection", "1998")])
def test_tmdb_movie_get_movie(movie_id, movie_name, movie_release_year):
    movie = get_movie(movie_id)
    assert movie["title"] == movie_name
    assert movie["release_date"] == "1998-12-10"

@pytest.mark.parametrize("collection_id, collection_name", [(10, "Star Wars Collection")])
def test_tmdb_movie_get_collection(collection_id, collection_name):
    collection = get_collection(collection_id)
    print(collection)

def test_tmdb_get_popular_movies():
    movies = get_popular_movies()
    for result in movies:
        print(str(result.id) + " " + result.title)
        assert result.title is not None
        assert result.id is not None
        # assert result.keywords is not None

def test_is_popular_movie():
    assert is_popular_movie(419704) is True
    assert is_popular_movie(222) is False


def check_searched_movie_info(movie_info):
    assert type(movie_info) is dict

    assert 'adult' in movie_info
    assert 'backdrop_path' in movie_info
    assert 'genre_ids' in movie_info
    assert 'id' in movie_info
    assert 'original_language' in movie_info
    assert 'original_title' in movie_info
    assert 'overview' in movie_info
    assert 'popularity' in movie_info
    assert 'poster_path' in movie_info
    assert 'release_date' in movie_info
    assert 'title' in movie_info
    assert 'video' in movie_info
    assert 'vote_average' in movie_info
    assert 'vote_count' in movie_info
