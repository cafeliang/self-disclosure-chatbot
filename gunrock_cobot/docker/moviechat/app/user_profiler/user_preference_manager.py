import logging
import re
from collections import defaultdict
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

from .user_profile import UserProfile

# from .predictor import GenderPredictor, PreferencePredictor


# Mark: - Logger Setup

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter('[UserProfile] %(message)s'))
logger.addHandler(ch)


# class PreferenceRelation:
#     LIKE = "<like>"
#     DISLIKE = "<dislike>"
#     SEEN = "<seen>"


pref_regex = re.compile(r"<(?P<relation>(\w+|\*))> *<(?P<entity_type>([\w\s]+|\*)):(?P<entity>([\w\s]+|\*))>")


class UserPreferenceManager:

    RELATIONS = {'like', 'dislike', 'seen'}
    ENTITY_TYPE = {'movie', 'actor', 'genre'}

    def __init__(self, user_attributes_ref):
        self.ua = UserProfile(user_attributes_ref)

    def add_preference(self, entry: Union[str, List[str]]):
        try:
            if isinstance(entry, str):
                entry = [entry]

            if not (isinstance(entry, list) and all(isinstance(i, str) for i in entry)):
                raise ValueError(
                    f"Adding invalid entry to UserPreferenceManager. Must be of type Union[str, List[str]]. "
                    f"Type: {type(entry)}, Query: {entry}")

            extraction = self._extract_query(entry, predicate=lambda _1, _2, entity: entity != '*')
            self.ua.preferences.extend(extraction)

        except ValueError as e:
            logger.warning(e, exc_info=True)

    def query_preference(self, query: Union[str, List[str]], format=True):
        try:
            if isinstance(query, str):
                query = [query]

            if not (isinstance(query, list) and all(isinstance(i, str) for i in query)):
                raise ValueError(
                    f"UserPreferenceManager Query must be of type Union[str, List[str]]. "
                    f"Type: {type(query)}, Query: {query}")

            extraction = self._extract_query(query, predicate=None)

            dbs = set()
            for e in extraction:
                print(e)
                db = set(self._filter_db(e))
                dbs |= db

            print(f"dbs: {dbs}")
            return self._db_to_dict(dbs)

        except ValueError as e:
            logger.warning(e, exc_info=True)

    def _extract_query(self,
                       query: List[str],
                       predicate: Optional[Callable[[str, str, str], bool]]
                       ) -> List[Tuple[str, str, str]]:

        extracted = []

        for item in query:
            match = pref_regex.search(item)
            try:
                if not match:
                    raise ValueError

                relation = match.group('relation')
                entity_type = match.group('entity_type')
                entity = match.group('entity')

                if relation not in UserPreferenceManager.RELATIONS | {'*'} or \
                        entity_type not in UserPreferenceManager.ENTITY_TYPE | {'*'}:
                    raise ValueError

                if predicate and not predicate(relation, entity_type, entity):
                    raise ValueError

                extracted.append((relation, entity_type, entity))

            except (ValueError, IndexError) as e:
                logger.warning(f"invalid user_preference: {item}. Error: {e}")
                continue

        return extracted

    def _filter_db(self, query: Tuple[str, str, str]) -> List[Tuple[str, str, str]]:
        relation, entity_type, entity = query

        db = self.ua.preferences
        if relation != '*':
            db = filter(lambda e: e[0] == relation, db)
        if entity_type != '*':
            db = filter(lambda e: e[1] == entity_type, db)
        if entity != '*':
            db = filter(lambda e: e[2] == entity, db)

        return db

    def _db_to_dict(self, db: List[Tuple[str, str, str]]) -> Dict[str, Any]:
        result = defaultdict(lambda: defaultdict(list))
        for relation, entity_type, entity in db:
            result[relation][entity_type].append(entity)
        return {k: dict(v) for k, v in result.items()}
