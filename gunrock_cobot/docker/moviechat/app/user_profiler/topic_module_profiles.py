from typing import Callable, Dict, List, Union


class TopicModuleProfiles:

    def __init__(self, topic_module_profiles: dict):
        """Add your module here"""
        self.movie = MovieTopicModuleProfile(topic_module_profiles)
        self.music = MusicTopicModuleProfile(topic_module_profiles)
        self.game = GameTopicModuleProfile(topic_module_profiles)
        self.book = BookTopicModuleProfile(topic_module_profiles)
        self.sport = SportTopicModuleProfile(topic_module_profiles)
        self.food = FoodTopicModuleProfile(topic_module_profiles)
        self.techscience = TechScienceTopicModuleProfile(topic_module_profiles)
        self.animal = AnimalTopicModuleProfile(topic_module_profiles)
        self.news = NewsTopicModuleProfile(topic_module_profiles)
        self.travel = TravelTopicModuleProfile(topic_module_profiles)
        self.fashion = FashionTopicModuleProfile(topic_module_profiles)


class TopicModuleProfile:

    def __init__(self, module: str, topic_module_profiles: dict):
        if module not in topic_module_profiles:
            topic_module_profiles[module] = {}
        self._storage = topic_module_profiles[module]

    def _safe_get(self, attribute: str, default_factory: Callable[[], Union[Dict, List, int, float, str]]):
        if attribute not in self._storage:
            self._storage[attribute] = default_factory()
        return self._storage[attribute]


"""Create the class for your specific module here"""


class MovieTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('movie', topic_module_profiles)

    @property
    def liked_movie_ids(self) -> List[str]:
        return self._safe_get('liked_movie_ids', list)

    @property
    def liked_genre_ids(self) -> List[int]:
        return self._safe_get('liked_genre_ids', list)

    @property
    def liked_actors(self) -> List[str]:
        return self._safe_get('liked_actors', list)


class MusicTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('music', topic_module_profiles)

    @property
    def fav_artists(self) -> List[str]:
        return self._safe_get('fav_artists', list)

    @property
    def fav_genres(self) -> List[str]:
        return self._safe_get('fav_genres', list)

    @property
    def played_instruments(self) -> List[str]:
        return self._safe_get('played_instruments', list)


class GameTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('game', topic_module_profiles)

    @property
    def mentioned_games(self) -> List[str]:
        return self._safe_get('mentioned_games', list)


class BookTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('book', topic_module_profiles)

    @property
    def fav_books(self) -> List[str]:
        return self._safe_get('fav_books', list)

    @property
    def fav_authors(self) -> List[str]:
        return self._safe_get('fav_authors', list)

    @property
    def fav_genre(self) -> List[str]:
        return self._safe_get('fav_genre', list)


class SportTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('sport', topic_module_profiles)

    @property
    def fav_sport_type(self) -> str:
        return self._safe_get('fav_sport_type', str)

    @fav_sport_type.setter
    def fav_sport_type(self, value: str):
        self._storage['fav_sport_type'] = value

    @property
    def talked_sport_types(self) -> List[str]:
        return self._safe_get('talked_sport_types', list)


class FoodTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('food', topic_module_profiles)

    @property
    def fav_cuisine(self) -> str:
        return self._safe_get('fav_cuisine', str)

    @fav_cuisine.setter
    def fav_cuisine(self, value: str):
        self._storage['fav_cuisine'] = value

    @property
    def talked_cuisine(self) -> List[str]:
        return self._safe_get('talked_cuisine', list)

    @property
    def fav_food(self) -> str:
        return self._safe_get('fav_food', str)

    @fav_food.setter
    def fav_food(self, value: str):
        self._storage['fav_food'] = value

    @property
    def talked_food(self) -> List[str]:
        return self._safe_get('talked_food', list)


class TechScienceTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('techscience', topic_module_profiles)

    @property
    def fav_topics(self) -> Dict[str, str]:
        return self._safe_get('fav_topics', dict)

    @property
    def last_topic(self) -> Dict[str, str]:
        return self._safe_get('last_topic', dict)


class AnimalTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('animal', topic_module_profiles)

    @property
    def pet(self) -> Dict[str, str]:
        return self._safe_get('pet', dict)

    @property
    def liked_animals(self) -> List[str]:
        return self._safe_get('liked_animals', list)


class NewsTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('news', topic_module_profiles)

    @property
    def previous_topic(self) -> List[str]:
        return self._safe_get('previous_topic', list)


class TravelTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('travel', topic_module_profiles)

    @property
    def last_city_visited(self) -> str:
        return self._safe_get('last_city_visited', str)

    @last_city_visited.setter
    def last_city_visited(self, value: str):
        self._storage['last_city_visited'] = value


class FashionTopicModuleProfile(TopicModuleProfile):

    def __init__(self, topic_module_profiles):
        super().__init__('fashion', topic_module_profiles)

    @property
    def clothes_preference(self) -> List[str]:
        return self._safe_get('clothes_preference', list)

    @property
    def makeup_preference(self) -> List[str]:
        return self._safe_get('makeup_preference', list)

    @property
    def recommended_fragrance(self) -> str:
        return self._safe_get('recommended_fragrance', str)

    @recommended_fragrance.setter
    def recommended_fragrance(self, value: str):
        self._storage['recommended_fragrance'] = value
