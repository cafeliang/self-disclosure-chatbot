import logging
import re

from dataclasses import dataclass, asdict, field
from typing import List, Optional
import random

from dispatcher import Dispatcher
from movie_module_context import WatchedMoviePreferenceLevel, WatchedMovieDiscussionMode, States, \
    CurrentWatchedMovieSubdialogContext
from movie_user_profile import MovieUserProfile
from selecting_strategy.module_selection import ProposeContinue
from nlu.constants import Positivity, DialogAct
from movietv_command_detector import MovieCommand
from movie_module_regex import \
    that_is_the_one, clarify_movie_name, like_all, rewatch_positive, watch_alone, watch_with_others, normally_watch, new_experience
from movie_preference_predictor import MoviePreferencePredictor, POPULAR_MOVIE_IDS
from movie_preference_extractor import MovieExtractor, ActorExtractor, GenreExtractor, Genre
from movie_opinion_detector import MoviePreferenceLevel, MovieLikeReason, MovieDisikeReason
from actor_opinion_detector import ActorOpinion
from actor_subdialog import ActorQuestionType, ActorSubdialogManager
from movietv_question_handler import MovieQuestionHandler
from movietv_utils import get_imdb_trivia_new, get_movie_genres, get_movie_plot_summary
from tmdb_utils import get_now_playing_movies
import template_manager
from tracker import Tracker
from nlu.constants import TopicModule
from popular_movie_trivia import popular_movie_detail

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('Movie').setLevel(logging.DEBUG)

BOT_FAVORITE_GENRE = [
    Genre.COMEDY.value,
    Genre.ACTION.value,
    Genre.SCIENCE_FICTION.value,
]

BOT_FAVORITE_ACTOR = {
    "actor_name": "Leonardo DiCaprio",
    "pronoun": "him",
    "actor_id": 6193,
    "opinion": "He consistently gives great performances, and he uses his fame to help the environment too. "
}

max_grounding_count = 1


class SpecificMovieQuestionType:
    # Better for liked movie
    ASK_IF_LIKE_MOVIE = "ask_if_like_movie"

    ASK_WHY_VERY_LIKE_MOVIE = "ask_why_very_like_movie"

    ASK_FAVORITE_PART = "ask_favorite_part"
    ASK_ANY_LIKED_PART = "ask_any_liked_part"
    # ASK_FAVORITE_SCENE = "ask_favorite_scene"

    ASK_FAV_CHARACTER = "ask_fav_character"

    # Better for disliked movie
    ASK_WHY_DISLIKE_MOVIE = "ask_why_dislike_movie"
    ASK_WHAT_TO_IMPROVE = "ask_what_to_improve"

    # General question
    ASK_FEELING = "ask_feeling"
    ASK_IF_REWATCH = "ask_if_rewatch"
    ASK_IF_WATCH_ALONE = "ask_if_watch_alone"
    ASK_IS_LIKED_TYPE = "ask_is_liked_type"


class WatchedMovieSubdialogManager:

    watched_movie_question_bank_very_like = [
        [
            SpecificMovieQuestionType.ASK_WHY_VERY_LIKE_MOVIE,
            SpecificMovieQuestionType.ASK_FAVORITE_PART],
        [
            SpecificMovieQuestionType.ASK_FAV_CHARACTER
        ],
        [
            SpecificMovieQuestionType.ASK_IS_LIKED_TYPE,
            SpecificMovieQuestionType.ASK_IF_REWATCH,
            SpecificMovieQuestionType.ASK_IF_WATCH_ALONE,
        ]
    ]

    watched_movie_question_bank_like = [
        [
            SpecificMovieQuestionType.ASK_FAVORITE_PART,
            # SpecificMovieQuestionType.ASK_FEELING
         ],
        [SpecificMovieQuestionType.ASK_FAV_CHARACTER],
        [
            SpecificMovieQuestionType.ASK_IS_LIKED_TYPE,
            SpecificMovieQuestionType.ASK_IF_REWATCH,
            SpecificMovieQuestionType.ASK_IF_WATCH_ALONE,
        ]
    ]

    watched_movie_question_bank_neutral = [
        [SpecificMovieQuestionType.ASK_ANY_LIKED_PART],
        [SpecificMovieQuestionType.ASK_FAV_CHARACTER],
        [
            SpecificMovieQuestionType.ASK_IS_LIKED_TYPE,
            SpecificMovieQuestionType.ASK_IF_REWATCH,
            SpecificMovieQuestionType.ASK_IF_WATCH_ALONE,
        ]
    ]

    watched_movie_question_bank_dislike = [
        [SpecificMovieQuestionType.ASK_WHY_DISLIKE_MOVIE],
        [SpecificMovieQuestionType.ASK_IF_WATCH_ALONE]
    ]

    watched_movie_question_bank_not_sure = [
        [SpecificMovieQuestionType.ASK_ANY_LIKED_PART],
        [
            SpecificMovieQuestionType.ASK_IS_LIKED_TYPE,
            SpecificMovieQuestionType.ASK_IF_WATCH_ALONE
        ]
    ]

    def __init__(self, movie_id,
                 user_profile: MovieUserProfile,
                 current_watched_movie_subdialog: CurrentWatchedMovieSubdialogContext,
                 previous_asked_questions: List[str]):
        self.movie_id = movie_id

        self.trivia = []
        self.user_profile = user_profile
        self.current_watched_movie_subdialog = current_watched_movie_subdialog
        self.previous_asked_question = previous_asked_questions

    def set_movie_preference_level(self, level: WatchedMoviePreferenceLevel):
        self.current_watched_movie_subdialog.movie_preference_level = level.value

    def get_trivia(self):
        if self.trivia:
            return self.trivia

        if self.movie_id and int(self.movie_id) in POPULAR_MOVIE_IDS:
            logging.info("movie in popular movie ids")
            self.trivia = popular_movie_detail.get(str(self.movie_id), {}).get("trivia", list())
        else:
            self.trivia = get_imdb_trivia_new(self.movie_id) if self.movie_id else list()
        return self.trivia

    def get_next_state(self):
        # todo: consider engagement here to see if we should stop earlier
        result = None
        logging.info(f"[movie: get_next_state] current mode: {self.current_watched_movie_subdialog.mode}")
        if self.current_watched_movie_subdialog.mode == WatchedMovieDiscussionMode.ASK_OPINION:
            if self.has_available_trivia() and self.used_trivial_fewer_than_default():
                self.current_watched_movie_subdialog.mode = WatchedMovieDiscussionMode.PROVIDE_TRIVIA
                result = "provide_watched_movie_trivia"
            elif self.has_available_opinion_question() and self.used_question_fewer_than_default():
                self.current_watched_movie_subdialog.mode = WatchedMovieDiscussionMode.ASK_OPINION
                result = "ask_watched_movie_question"
        elif self.current_watched_movie_subdialog.mode == WatchedMovieDiscussionMode.PROVIDE_TRIVIA:
            if self.has_available_opinion_question() and self.used_question_fewer_than_default():
                self.current_watched_movie_subdialog.mode = WatchedMovieDiscussionMode.ASK_OPINION
                result = "ask_watched_movie_question"
            elif self.has_available_trivia() and self.used_trivial_fewer_than_default():
                self.current_watched_movie_subdialog.mode = WatchedMovieDiscussionMode.PROVIDE_TRIVIA
                result = "provide_watched_movie_trivia"
        return result

    def has_available_trivia(self):

        used_trivia = self.current_watched_movie_subdialog.trivial_used_count
        result = used_trivia < len(self.get_trivia())
        logging.info(f"[has_available_trivia] {result}")
        return result

    def used_trivial_fewer_than_default(self):
        result = self.current_watched_movie_subdialog.trivial_used_count < self.get_max_question_and_trivia_count()
        logging.info(f"[used_trivial_fewer_than_default] {result}")
        return result

    def used_question_fewer_than_default(self):
        result = self.current_watched_movie_subdialog.question_used_count < self.get_max_question_and_trivia_count()
        logging.info(f"[used_question_fewer_than_default] {result}")

        return result
    def get_new_trivia_and_update_count(self):
        result = self.get_trivia()[self.current_watched_movie_subdialog.trivial_used_count] + " "
        self.current_watched_movie_subdialog.trivial_used_count += 1
        return result

    def has_available_opinion_question(self):
        next_question = self.get_next_opinion_question()
        result =  next_question is not None
        logging.info(f"[has_available_opinion_question] {result}")
        return result

    def get_next_opinion_question(self):
        reusable_questions = [
            SpecificMovieQuestionType.ASK_WHY_VERY_LIKE_MOVIE,
            SpecificMovieQuestionType.ASK_FAVORITE_PART,
            SpecificMovieQuestionType.ASK_WHY_DISLIKE_MOVIE
        ]
        used_questions = self.current_watched_movie_subdialog.question_asked

        logging.info(f"[get_next_opinion_question] used_questions: {used_questions}")
        for candidates in self.get_question_candidates():
            logging.info(f"[get_next_opinion_question] candidate: {candidates}")
            if not set(candidates).intersection(set(used_questions)):

                modified_candidate = self.get_modified_question_candidates(candidates)

                for candidate in modified_candidate:
                    if candidate not in self.previous_asked_question or candidate in reusable_questions:
                        logging.info(f"[get_next_opinion_question] selected candidate: {candidate}")

                        return candidate
        return None

    @staticmethod
    def get_modified_question_candidates(candidates):
        modified_candidate = []
        # special case for ask_is_liked_type, as we want to ask it first
        if SpecificMovieQuestionType.ASK_IS_LIKED_TYPE in candidates:
            copied_candidates = candidates.copy()
            copied_candidates.remove(SpecificMovieQuestionType.ASK_IS_LIKED_TYPE)
            shuffled_candidates = random.sample(candidates, len(candidates))

            modified_candidate.append(SpecificMovieQuestionType.ASK_IS_LIKED_TYPE)
            modified_candidate.extend(shuffled_candidates)

        else:
            modified_candidate = candidates
        return modified_candidate

    def get_next_opinion_question_and_update_context(self) -> str:
        """
        :return: question template key
        """
        next_opinion_question = self.get_next_opinion_question()
        logging.info(f"next_opinion_question: {next_opinion_question}")

        if next_opinion_question:
            self.current_watched_movie_subdialog.question_asked.append(next_opinion_question)
            self.current_watched_movie_subdialog.question_used_count += 1
            self.previous_asked_question.append(next_opinion_question)
            return "specific_movie_watched/{}/question".format(next_opinion_question)

        else:
            return ""

    def get_current_question(self):
        if self.current_watched_movie_subdialog.question_asked:
            return self.current_watched_movie_subdialog.question_asked[-1]
        else:
            return None

    def get_movie_preference_level(self) -> Optional[WatchedMoviePreferenceLevel]:
        if self.current_watched_movie_subdialog.movie_preference_level:
            return WatchedMoviePreferenceLevel(self.current_watched_movie_subdialog.movie_preference_level)
        else:
            return None

    def get_max_question_and_trivia_count(self):
        if self.get_movie_preference_level() == WatchedMoviePreferenceLevel.VERY_LIKE:
            result = 3
        elif self.get_movie_preference_level() == WatchedMoviePreferenceLevel.LIKE:
            result = 3
        elif self.get_movie_preference_level() == WatchedMoviePreferenceLevel.NEUTRAL:
            result = 2
        elif self.get_movie_preference_level() == WatchedMoviePreferenceLevel.DISLIKE:
            result = 1
        elif self.get_movie_preference_level() == WatchedMoviePreferenceLevel.UNKNOWN:
            result = 2
        else:
            result = 2

        logging.info(f"[get_max_question_and_trivia_count] result: {result}")
        return result

    def get_question_candidates(self) -> List[List[str]]:
        movie_preference_level = self.get_movie_preference_level()
        if movie_preference_level == WatchedMoviePreferenceLevel.VERY_LIKE:
            return self.watched_movie_question_bank_very_like
        elif movie_preference_level == WatchedMoviePreferenceLevel.LIKE:
            return self.watched_movie_question_bank_like
        elif movie_preference_level == WatchedMoviePreferenceLevel.NEUTRAL:
            return self.watched_movie_question_bank_neutral
        elif movie_preference_level == WatchedMoviePreferenceLevel.DISLIKE:
            return self.watched_movie_question_bank_dislike
        elif movie_preference_level == WatchedMoviePreferenceLevel.UNKNOWN:
            return self.watched_movie_question_bank_not_sure

@dataclass
class RedisMovieModuleContext:
    user_id: str = ""
    movies: dict = field(default_factory=dict)
    actors: dict = field(default_factory=dict)


class MovieTVAutomaton:
    def __init__(self, input_data, user_attributes):
        self.tracker = Tracker(input_data, user_attributes)
        self.dispatcher = Dispatcher(user_attributes)
        self.movie_question_handler = MovieQuestionHandler(self.tracker.text,
                                                           input_data['returnnlp'][0],
                                                           self.tracker,
                                                           self.dispatcher)
        self.current_watched_movie_subdialog_manager = WatchedMovieSubdialogManager(
            self.tracker.current_movie_id,
            self.tracker.module_context.movie_user_profile,
            self.tracker.module_context.current_watched_movie_subdialog,
            self.tracker.module_context.previous_asked_questions
        )

        self.actor_subdialog_manager = ActorSubdialogManager(
            self.tracker.module_context.actor_subdialog
        )
        self.template = template_manager.Templates.movies

    def transduce(self):
        if self.tracker.enter_from_other_module:
            self.tracker.propose_continue = ProposeContinue.CONTINUE

        if self.tracker.is_resume_mode:
            self.t_resume()

        else:
            current_context = self.tracker.module_context
            logging.info(f"[transduce] start: current_context: {current_context}")

            topic_switch_intents = self.tracker.command_detector.detect_intents()
            if topic_switch_intents:
                self.handles_topic_switch(topic_switch_intents[0])
            else:
                state_handler = self.get_function_from_name("t_", current_context.current_state)
                logging.info(f"state_handler: {state_handler}")
                state_handler()

        responses = [
                self.dispatcher.response_text.ack,
                self.dispatcher.response_text.transition,
                self.dispatcher.response_text.main_content,
        ]

        final_response = " ".join(filter(None, responses))

        result = {
            "response": final_response,
            "moviechat_user": asdict(self.tracker.module_context)
        }

        logging.info(f"[transduce] result: {result}")

        return result

    def handles_topic_switch(self, command_intent: str):
        if command_intent == MovieCommand.EXIT.value:
            self.s_jump_out_by_user()
        elif command_intent == MovieCommand.ASK_FAV_MOVIE.value:
            self.s_answer_fav_movie()
        elif command_intent == MovieCommand.REQ_CHANGE_MOVIE.value:
            self.s_ans_change_movie()
        elif command_intent == MovieCommand.REQ_GUESS_MOVIE.value:
            self.s_ans_guess_movie()
        elif command_intent == MovieCommand.REQ_TV.value:
            self.s_ans_req_tv()
        elif command_intent == MovieCommand.ASK_RECOMMEND.value:
            self.s_propose_movie()
        elif command_intent == MovieCommand.ASK_SEEN_MOVIE.value:
            self.s_propose_movie()
        elif command_intent == MovieCommand.ASK_CURRENT_PLAYING.value:
            self.s_answer_current_playing_movie()

    def try_handles_question(self):
        """
        :return: False if replace prefix only and move on. True if insert one more turn or propose other topic
        """
        answer = self.movie_question_handler.handle_question()
        logging.info(f"[try_handles_question] {answer}")
        if answer and answer.bot_propose_module and answer.bot_propose_module != TopicModule.MOVIE:
            propose_topic = answer.bot_propose_module.value

            self.dispatcher.ack = answer.response
            self.dispatcher.main_content = self.dispatcher.propose_topic(
                selector=[f"propose_topic_short/{propose_topic}"])

            self.tracker.propose_continue = ProposeContinue.UNCLEAR
            self.tracker.module_selector.propose_topic = propose_topic
            return True

        elif answer:
            self.dispatcher.ack = answer.response
            self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
            return False
        return False

    def s_answer_fav_movie(self):
        self.dispatcher.ack = self.dispatcher.generate_response("topic_switch/ans_fav_movie")
        self.tracker.current_state = States.ASK_MOVIE_TO_DISCUSS
        self.tracker.module_context.asking_liked_movie = True

    def s_ans_change_movie(self):
        self.dispatcher.ack = self.dispatcher.generate_response(
            "topic_switch/ans_change_movie"
        )

        self.tracker.current_state = States.ASK_MOVIE_TO_DISCUSS
        self.tracker.module_context.asking_liked_movie = True

    def s_ans_guess_movie(self):
        self.dispatcher.ack = self.dispatcher.generate_response(
            "topic_switch/ans_guess_movie")

        self.tracker.current_state = States.ASK_MOVIE_TO_DISCUSS
        self.tracker.module_context.asking_liked_movie = True

    def s_ans_req_tv(self):
        if self.tracker.enter_from_other_module:
            self.s_try_switch_from_tv_to_movie()
        else:
            self.dispatcher.ack = self.dispatcher.generate_response(
                "topic_switch/ans_req_tv")

            self.tracker.current_state = States.ASK_MOVIE_TO_DISCUSS
            self.tracker.module_context.asking_liked_movie = True

    def s_try_switch_from_tv_to_movie(self):
        self.dispatcher.main_content = "Cool, watching TV must be fun! I haven't watched many tv shows yet, " \
                                       "but I do watch a lot of movies. Do you like movies? "
        self.tracker.current_state = States.TRY_SWITCH_FROM_TV_TO_MOVIE

    def t_try_switch_from_tv_to_movie(self):
        if self.tracker.returnnlp.answer_positivity == Positivity.neg:
            return self.s_jump_out_from_tv()
        else:
            self.s_ask_recent_seen_movie()

    def s_answer_current_playing_movie(self):
        now_playing = get_now_playing_movies()
        logging.info(f"[now playing] {now_playing}")
        if now_playing:
            if len(now_playing) == 1:
                self.dispatcher.ack = self.dispatcher.generate_response(
                    "topic_switch/ans_current_playing_movie/one_movie",
                    {"title1": now_playing[0].title})
            elif len(now_playing) == 2:
                self.dispatcher.ack = self.dispatcher.generate_response(
                    "topic_switch/ans_current_playing_movie/two_movies",
                    {
                        "title1": now_playing[0].title,
                        "title2": now_playing[1].title,
                    }
                )
            elif len(now_playing) == 3:
                self.dispatcher.ack = self.dispatcher.generate_response(
                    "topic_switch/ans_current_playing_movie/three_movies",
                    {
                        "title1": now_playing[0].title,
                        "title2": now_playing[1].title,
                        "title3": now_playing[2].title
                    }
                )
        else:
            self.dispatcher.ack = self.dispatcher.generate_response(
                "topic_switch/ans_current_playing_movie/no_result"
            )
            self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
            self.s_propose_movie()



    def get_function_from_name(self, prefix, function_name):
        return getattr(self, "{}{}".format(prefix, function_name))

    def get_next_subtopic(self):
        next_chitchat_state = self.get_next_chitchat_state()
        if next_chitchat_state:
            return next_chitchat_state
        else:
            mentioned_movie_count = len(self.tracker.module_context.mentioned_movie_ids)
            bot_proposed_movie_count = len(self.tracker.module_context.proposed_movie_ids)
            user_proposed_movie_count = mentioned_movie_count - bot_proposed_movie_count
            if bot_proposed_movie_count == 0:
                return self.s_propose_movie
            elif bot_proposed_movie_count >= 2 and user_proposed_movie_count >= 2:
                # todo: increase threshold for returning mode
                return self.s_jump_out_by_bot
            elif self.tracker.current_state in [
                States.ASK_MOVIE_AGAIN, States.ASK_MOVIE_TO_DISCUSS, States.ASK_MOVIE_TO_DISCUCSS_FOLLOW_UP]:
                # User fails to come up with a movie, let's propose movie
                return self.s_propose_movie
            elif user_proposed_movie_count < bot_proposed_movie_count:
                return self.s_ask_movie_to_discuss
            elif bot_proposed_movie_count < user_proposed_movie_count:
                return self.s_propose_movie
            else:
                if self.tracker.module_context.last_movie_proposed_by_bot:
                    return self.s_ask_movie_to_discuss
                else:
                    return self.s_propose_movie

    def get_next_chitchat_state(self):
        user_profile = self.tracker.module_context.movie_user_profile
        if not user_profile.liked_genre_ids and \
                GeneralChitchatQuestion.ASK_LIKED_GENRE not in self.tracker.module_context.general_chitchat_history:
            return self.s_ask_liked_movie_genre

        if not user_profile.liked_actor_names and \
                GeneralChitchatQuestion.ASK_LIKED_ACTOR not in self.tracker.module_context.general_chitchat_history:
            return self.s_ask_liked_actor

        general_chitchat_question_candidates = [GeneralChitchatQuestion.ASK_WATCH_REVIEW_OR_TRAILER]

        for chitchat_question in general_chitchat_question_candidates:
            if chitchat_question not in self.tracker.module_context.general_chitchat_history:
                function_name = self.get_function_from_name("s_", chitchat_question)
                return function_name

        return None

    def t_init(self):
        executed = self.try_actor_genre_movie_flow()
        if executed:
            return

        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        self.s_ask_recent_seen_movie()

    def t_resume(self):
        executed = self.try_actor_genre_movie_flow()
        if executed:
            return

        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        self.s_resume()

    def s_resume(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("resume")
        self.tracker.current_state = States.ASK_MOVIE_TO_DISCUSS

    def s_ask_recent_seen_movie(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("ask_recent_seen_movie/question")

        self.tracker.current_state = States.ASK_RECENT_SEEN_MOVIE

        self.tracker.propose_continue = ProposeContinue.CONTINUE

    def t_ask_recent_seen_movie(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.returnnlp.answer_positivity == Positivity.neg or self.tracker.ans_not_sure_only():
            if "ask_back" in self.tracker.returnnlp.flattened_lexical_intent:
                self.s_propose_movie()
                return
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("maybe_you_been_busy")
                self.execute_next_subtopic()
                return

        if self.tracker.yes_only():
            return self.s_ask_recent_seen_movie_follow_up()

        executed = self.try_actor_genre_movie_flow()
        if executed:
            return

        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        self.try_generate_general_acknowledgement()
        self.execute_next_subtopic()


    def s_ask_movie_to_discuss(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("ask_movie_to_discuss/question")
        self.tracker.current_state = States.ASK_MOVIE_TO_DISCUSS

    def t_ask_movie_to_discuss(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.returnnlp.answer_positivity == Positivity.neg or \
                self.tracker.ans_not_sure_only():
            self.dispatcher.ack = "No problem. Let's talk about something else. "
            self.tracker.propose_continue = ProposeContinue.STOP


        if self.tracker.yes_only():
            return self.s_ask_movie_to_discuss_follow_up()

        else:
            detected = self.try_actor_genre_movie_flow()
            if detected:
                return

            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

            else:
                self.s_ask_movie_again()

    def s_ask_movie_to_discuss_follow_up(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("ask_movie_name_follow_up")
        self.tracker.current_state = States.ASK_MOVIE_TO_DISCUCSS_FOLLOW_UP

    def t_ask_movie_to_discuss_follow_up(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        self.t_ask_movie_to_discuss()


    def s_ask_movie_again(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("repeat_movie")
        self.tracker.current_state = States.ASK_MOVIE_AGAIN

    def t_ask_movie_again(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.returnnlp.answer_positivity == Positivity.neg or \
                self.tracker.ans_not_sure_only():
            self.dispatcher.ack = "No problem.  "
            self.tracker.propose_continue = ProposeContinue.STOP

        else:
            detected = self.try_actor_genre_movie_flow()
            if detected:
                return

            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

            else:
                if not self.dispatcher.ack:
                    self.dispatcher.ack = self.dispatcher.generate_response("do_not_know_movies_by_that_name")

                self.try_generate_general_acknowledgement()
                self.execute_next_subtopic()


    def s_jump_out_by_bot(self):
        self.dispatcher.ack = self.dispatcher.generate_response("jump_out/bot_initiated")
        self.dispatcher.transition = ""
        self.tracker.propose_continue = ProposeContinue.STOP

    def s_jump_out_by_user(self):
        self.dispatcher.ack = self.dispatcher.generate_response("jump_out/user_initiated")
        self.dispatcher.transition = ""
        self.tracker.propose_continue = ProposeContinue.STOP

    def s_jump_out_from_tv(self):
        self.dispatcher.ack = self.dispatcher.generate_response("jump_out/from_tv")
        self.dispatcher.transition = ""
        self.tracker.propose_continue = ProposeContinue.STOP


    def try_specific_movie_flow(self, text: str = None):
        if not text:
            text = self.tracker.text

        if self.tracker.back_channeling_only():
            return False

        movie_infos = MovieExtractor().extract_movies_v2(text, self.tracker.last_response)
        if movie_infos:
            self.tracker.module_context.last_movie_proposed_by_bot = False

            # don't discuss movies mentioned before
            matched_movie = self.match_mentioned_movie(movie_infos)
            if matched_movie:
                if self.tracker.current_state == States.ASK_LIKED_MOVIE_IN_GENRE:
                    self.dispatcher.ack = "I see! Yeah, it's really a good movie. "
                    return

            if len(movie_infos) > 1:  # need grounding
                self.tracker.movie_name_grounding_count = 0
                self.tracker.detected_movies_info = movie_infos
                logging.info(f"self.tracker.detected_movies_info: {movie_infos}")
                self.dispatcher.ack = self.dispatcher.generate_response("ground_movie_ack")
                self.s_movie_name_grounding()
            else:

                detected_movie_info = movie_infos[0]
                movie_name = detected_movie_info["movie_name"]

                self.dispatcher.ack = self.dispatcher.generate_response(
                    "know_movie", slots={"movie_title": movie_name})

                self.handles_detected_movie(detected_movie_info)
            return True
        else:
            return False

    def match_mentioned_movie(self, detected_movie_infos):
        all_detected_movie_ids = [movie_info.get("movie_id") for movie_info in detected_movie_infos]
        return set(all_detected_movie_ids).intersection(self.tracker.mentioned_movie_ids)

    def try_movie_genre_flow(self, text=None):
        logging.info("[try_detect_movie_genre_flow]")

        if not text:
            text = self.tracker.text

        if self.tracker.back_channeling_only():
            return False

        extracted_genres = GenreExtractor().extract_genre(text)
        # assume user mention the genre they like at last segment
        # eg. no i don't like action movie i like romance movie
        # todo: handles if the genre is already in the list
        if extracted_genres:
            if extracted_genres[0] in self.tracker.liked_genre_ids:
                # Don't follow up on that genre if it's already in liked genre
                return False

            logging.info(f"[try_detect_movie_genre_flow] extracted_genres: {extracted_genres}")
            all_favorite_genre_ids = extracted_genres + self.tracker.liked_genre_ids
            self.tracker.liked_genre_ids = all_favorite_genre_ids
            self.dispatcher.ack = "I see, i like your taste! "
            self.s_ask_liked_movie_in_genre()
            return True
        return False

    def handles_detected_movie(self, detected_movie_info: dict):

        if self.tracker.module_context.asking_liked_movie:
            logging.info("[handles_detected_movie]: asking_liked_movie = True")
            self.tracker.module_context.asking_liked_movie = False  # reset
            self.handles_liked_movie(detected_movie_info)

        else:
            self.init_context_for_current_movie(detected_movie_info)

            self.try_use_system_ack()
            self.s_ask_like_watched_movie()

    def init_context_for_current_movie(self, movie_info):
        movie_id = movie_info["movie_id"]

        self.tracker.module_context.current_watched_movie_subdialog.reset()
        self.tracker.current_movie_info = movie_info
        self.tracker.current_movie_id = movie_id
        if not movie_id in self.tracker.mentioned_movie_ids:
            self.tracker.mentioned_movie_ids.append(movie_id)

    def handles_liked_movie(self, liked_movie_info):
        movie_id = liked_movie_info["movie_id"]
        self.tracker.module_context.movie_user_profile.liked_movie_ids.append(movie_id)
        self.init_context_for_current_movie(liked_movie_info)
        self.current_watched_movie_subdialog_manager.set_movie_preference_level(
            WatchedMoviePreferenceLevel.VERY_LIKE)

        self.try_use_system_ack()
        self.execute_next_state_from_watched_movie_subdialog()


    def s_ask_recent_seen_movie_follow_up(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("what_did_you_see")
        self.tracker.current_state = States.ASK_RECENT_SEEN_MOVIE_FOLLOW_UP

    def t_ask_recent_seen_movie_follow_up(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.back_channeling_only():  # special case for "yeah", as it sometimes can be considered pos_answer
            self.dispatcher.ack = self.dispatcher.generate_response("maybe_you_been_busy")
            self.execute_next_subtopic()
            return
        else:
            self.t_ask_recent_seen_movie()

    def s_movie_name_grounding(self):
        grounding_count = self.tracker.movie_name_grounding_count
        detected_movies_info = self.tracker.detected_movies_info
        logging.debug("detected movie infos: {}".format(detected_movies_info))
        logging.debug("grounding_count: {}".format(grounding_count))

        current_ground_movie_info = detected_movies_info[grounding_count]
        logging.debug("current_ground_movie_info: {}".format(current_ground_movie_info))

        leading_actor = current_ground_movie_info.get("movie_leading_actor", {})

        if leading_actor:
            actor_name = leading_actor.get("name", "")
            actor_role = leading_actor.get("role", "")
            if actor_name and actor_role:
                template_args = {
                    "movie_title": current_ground_movie_info["movie_name"],
                    "release_year": current_ground_movie_info["movie_release_year"],
                    "actor_name": actor_name,
                    "actor_role": actor_role,
                }
                self.dispatcher.main_content = self.dispatcher.generate_response("ground_movie", template_args)
        else:
            template_args = {
                "movie_title": current_ground_movie_info["movie_name"],
                "release_year": current_ground_movie_info["movie_release_year"],
            }
        self.dispatcher.main_content = self.dispatcher.generate_response("ground_movie_no_actor", template_args)

        self.tracker.current_state = States.MOVIE_NAME_GROUNDING

    def t_movie_name_grounding(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.ans_yes() or re.search(that_is_the_one, self.tracker.text):

            confirmed_movie_info = self.tracker.detected_movies_info[self.tracker.movie_name_grounding_count]

            if self.tracker.returnnlp.has_intent(["ans_like", "opinion_positive"]):
                self.handles_liked_movie(confirmed_movie_info)
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("ground_movie_guess_right")
                self.handles_detected_movie(confirmed_movie_info)
            return

        if self.tracker.no_only():
            self.tracker.movie_name_grounding_count += 1
            if self.tracker.movie_name_grounding_count <= max_grounding_count:
                self.s_movie_name_grounding()
                return
        else:
            if self.tracker.returnnlp.answer_positivity == Positivity.neg:
                self.tracker.text = re.sub(r"^(no i'm thinking of )|^(no )", "", self.tracker.text)

            executed = self.try_actor_genre_movie_flow()
            if executed:
                return

            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

        if not self.dispatcher.ack:
            self.dispatcher.ack = self.dispatcher.generate_response("do_not_know_movies_by_that_name")

        self.execute_next_subtopic()

        return

    def s_propose_movie(self):
        current_movie_info = self.tracker.current_movie_info

        movie_info, connection = MoviePreferencePredictor(
            current_movie_info,
            self.tracker.liked_genre_ids,
            self.tracker.mentioned_movie_ids,
            self.tracker.a_b_test) \
            .pick_next_movie_with_info()

        movie_id = movie_info["movie_id"]
        self.tracker.module_context.last_movie_proposed_by_bot = True
        self.tracker.module_context.proposed_movie_ids.append(movie_id)
        self.tracker.mentioned_movie_ids.append(movie_id)
        self.tracker.module_context.current_movie_id = movie_id
        self.init_context_for_current_movie(movie_info)

        movie_name = movie_info["movie_name"]
        if connection == MoviePreferencePredictor.Connection.SIMILAR_MOVIE.name:
                self.dispatcher.main_content = self.dispatcher.generate_response(
                    "propose_movie/ask_if_seen_movie/question/similar", slots={"movie_name": movie_name})
        else:
            self.dispatcher.main_content = self.dispatcher.generate_response(
                "propose_movie/ask_if_seen_movie/question/default", slots={"movie_name": movie_name})

        self.tracker.current_state = States.PROPOSE_MOVIE

    def t_propose_movie(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        plot_summary = self.get_plot_summary(self.tracker.current_movie_id)
        if self.tracker.returnnlp.has_intent("req_more"):
            if plot_summary:
                self.s_provide_movie_plot_summary()
            else:
                self.dispatcher.ack = "Well, i actually don't remember the detail, but i did enjoy it. "
                self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
                next_state = self.get_next_subtopic()
                next_state()
            return
        if self.tracker.returnnlp.has_intent(["ans_dislike", "opinion_negative"]):
            # if last segment is dislike, then we assume user didn't mention what they like in other segment
            if self.tracker.returnnlp[-1].has_intent(["ans_dislike", "opinion_negative"]):
                # talk about something else, as user don't like the proposed movie
                # user utterance example: "yes but i'm not a fan" , "no i don't like star wars"
                self.dispatcher.ack = self.dispatcher.generate_response("thats_okay")

                self.execute_next_subtopic()
                return

        if self.tracker.returnnlp.answer_positivity is Positivity.pos:
            if self.tracker.returnnlp.has_intent(["ans_like", "opinion_positive"]):
                # handles "yes + positive opinion" - eg. "yeah that's one of my favorite"
                self.try_use_system_ack()

                self.handles_liked_movie(self.tracker.current_movie_info)
                return
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("propose_movie/ask_if_seen_movie/ack/seen")
                self.s_ask_like_watched_movie()
                return

        elif self.tracker.returnnlp.answer_positivity is Positivity.neg or self.tracker.ans_not_sure():
            self.dispatcher.ack = self.dispatcher.generate_response("propose_movie/ask_if_seen_movie/ack/not_seen")
            if plot_summary:
                self.s_ask_if_interested_in_proposed_movie()
                return
             # else: todo: handles follow up question (eg. "no is it an action movie")

        executed = self.try_actor_genre_movie_flow()
        if executed:
            return

        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        else:
            # default: assume user hasn't seen it
            self.try_generate_general_acknowledgement()

            if plot_summary:
                self.s_ask_if_interested_in_proposed_movie()
            else:
                self.execute_next_subtopic()

    @staticmethod
    def get_plot_summary(movie_id):
        if movie_id in POPULAR_MOVIE_IDS:
            return MovieTVAutomaton.get_plot_summary_for_popular_movies(str(movie_id))
        else:
            return get_movie_plot_summary(movie_id)

    @staticmethod
    def get_plot_summary_for_popular_movies(movie_id):
        return popular_movie_detail.get(str(movie_id), {}).get("plotsummary")

    def s_ask_if_interested_in_proposed_movie(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("propose_movie/ask_if_interested/question")
        self.tracker.current_state = States.ASK_IF_INTERESTED_IN_PROPOSED_MOVIE

    def t_ask_if_interested_in_proposed_movie(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.returnnlp.answer_positivity is Positivity.neg and not self.tracker.returnnlp.has_intent("req_more"):
            self.dispatcher.ack = self.dispatcher.generate_response("propose_movie/ask_if_interested/ack/negative")
            self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
            next_state = self.get_next_subtopic()
            next_state()
            return

        executed = self.try_actor_genre_movie_flow()
        if executed:
            return

        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        self.s_provide_movie_plot_summary()
        # todo: handles other cases

    def s_provide_movie_plot_summary(self):
        plot_summary = self.get_plot_summary(self.tracker.current_movie_id)

        self.dispatcher.main_content = self.dispatcher.generate_response("propose_movie/provide_plot_summary/content",
                                                                         slots={'plot': plot_summary})
        self.tracker.current_state = States.PROVIDE_MOVIE_PLOT_SUMMARY

    def t_provide_movie_plot_summary(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.returnnlp.answer_positivity is Positivity.pos:
            # todo: handles the case if user say "i like action movie", which is not about the current movie
            self.dispatcher.ack = self.dispatcher.generate_response("propose_movie/provide_plot_summary/ack_positive")

            self.execute_next_subtopic()
            return
        else:
            executed = self.try_actor_genre_movie_flow()
            if executed:
                return
            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return
            else:
                self.try_generate_general_acknowledgement()
                self.execute_next_subtopic()

    def try_actor_genre_movie_flow(self):
        logging.info("[try_detect_genre_and_movie_flow] ")
        text = self.tracker.returnnlp.text_excluding_dislike_segments

        if self.tracker.back_channeling_only():
            return False

        if not text:
            return False

        actor_flow_executed = self.try_actor_flow(text)
        if actor_flow_executed:
            logging.info("actor_flow_executed")
            return True

        if not self.tracker.is_follow_up_question():
            # avoid detecting genre if user ask "is it a scary movie"
            genre_flow_executed = self.try_movie_genre_flow(text)
            if genre_flow_executed:
                logging.info("genre_detected")
                return True

        specific_movie_flow_exectued = self.try_specific_movie_flow(text)
        logging.info("movie_name_detected")
        if specific_movie_flow_exectued:
            return True
        return False

    def s_repeat_movie_name(self, current_state):
        movie_name = self.tracker.current_movie_info["movie_name"]
        self.dispatcher.main_content = movie_name
        self.tracker.current_state = current_state


####### [begin] specific movie subdialog #######


    def s_ask_like_watched_movie(self):
        self.dispatcher.main_content = self.dispatcher.generate_response(
            "specific_movie_watched/ask_if_like_movie/question")
        self.tracker.current_state = States.ASK_LIKE_SPECIFIC_MOVIE
        self.tracker.current_watched_movie_subdialog_context.mode = WatchedMovieDiscussionMode.ASK_OPINION

    def t_ask_like_watched_movie(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.not_watched():
            self.handles_not_watched()
            return

        # Set preference level and Acknowledge
        self.set_preference_level_and_ack()
        self.execute_next_state_from_watched_movie_subdialog()

    def handles_not_watched(self):
        self.dispatcher.ack = self.dispatcher.generate_response(
            "specific_movie_watched/ask_if_like_movie/ack/not_seen")
        # reset current movie to avoid proposing similar movie later
        self.tracker.current_movie_info = {}
        self.tracker.current_movie_id = None
        # todo: maybe ask another movie?
        self.execute_next_subtopic()

    def set_preference_level_and_ack(self):
        movie_opinion_level = self.tracker.movie_opinion_detector.detect_preference_level()
        if movie_opinion_level is MoviePreferenceLevel.SAD:
            self.current_watched_movie_subdialog_manager.set_movie_preference_level(
                WatchedMoviePreferenceLevel.LIKE)
            self.tracker.liked_movie_ids.append(self.tracker.current_movie_id)
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_like_movie/ack/sad")

        elif movie_opinion_level is MoviePreferenceLevel.HIGHLY_POSITIVE:
            self.current_watched_movie_subdialog_manager.set_movie_preference_level(
                WatchedMoviePreferenceLevel.VERY_LIKE)
            self.tracker.liked_movie_ids.append(self.tracker.current_movie_id)
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_like_movie/ack/very_like")

        elif movie_opinion_level is MoviePreferenceLevel.POSITIVE:
            self.current_watched_movie_subdialog_manager.set_movie_preference_level(
                WatchedMoviePreferenceLevel.LIKE)
            self.tracker.liked_movie_ids.append(self.tracker.current_movie_id)
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_like_movie/ack/like")

        elif movie_opinion_level is MoviePreferenceLevel.NEUTRAL:
            self.current_watched_movie_subdialog_manager.set_movie_preference_level(
                WatchedMoviePreferenceLevel.NEUTRAL)
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_like_movie/ack/neutral")

        elif movie_opinion_level is MoviePreferenceLevel.NEGATIVE:
            self.current_watched_movie_subdialog_manager.set_movie_preference_level(
                WatchedMoviePreferenceLevel.DISLIKE)
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_like_movie/ack/dislike")

        elif movie_opinion_level is MoviePreferenceLevel.NOT_SURE:
            self.current_watched_movie_subdialog_manager.set_movie_preference_level(
                WatchedMoviePreferenceLevel.UNKNOWN)
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_like_movie/ack/not_sure")

        else:
            self.current_watched_movie_subdialog_manager.set_movie_preference_level(
                WatchedMoviePreferenceLevel.NEUTRAL)
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_like_movie/ack/neutral")

    def s_provide_watched_movie_trivia(self):
        trivia = self.current_watched_movie_subdialog_manager.get_new_trivia_and_update_count()
        self.dispatcher.main_content = \
            self.dispatcher.generate_response("specific_movie_watched_provide_trivia/intro") + \
            trivia + \
            self.dispatcher.generate_response("specific_movie_watched_provide_trivia/question")

        self.tracker.current_state = States.PROVIDE_WATCHED_MOVIE_TRIVIA
        self.tracker.current_watched_movie_subdialog_context.mode = WatchedMovieDiscussionMode.PROVIDE_TRIVIA


    def t_provide_watched_movie_trivia(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.not_watched():
            self.handles_not_watched()
            return

        if self.tracker.returnnlp.has_intent("req_more"):
            if self.current_watched_movie_subdialog_manager.has_available_trivia():
                self.s_provide_watched_movie_trivia()
                return
            else:
                self.dispatcher.ack = "Well, that's all i know for now. "
        else:
            executed = self.try_actor_genre_movie_flow()
            if executed:
                return
            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

        if self.tracker.ans_not_sure() and self.tracker.system_ack_tag == "not_sure":
            self.dispatcher.ack = " "  # hacky way to avoid system adding acknowledgement when user say "i didn't know that"

        self.try_generate_general_acknowledgement()
        self.execute_next_state_from_watched_movie_subdialog() 

    def try_generate_general_acknowledgement(self):
        if self.dispatcher.ack:
            return False

        use_system_ack = self.try_use_system_ack()
        if use_system_ack:
            return True

        if self.tracker.user_engagement_detector.is_engaged_answer() or \
            self.tracker.returnnlp.has_dialog_act(DialogAct.STATEMENT):
            if len(self.tracker.text) >= 5:
                self.dispatcher.ack = self.dispatcher.generate_response("ack/general/medium")
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("ack/general/short")

    def try_use_system_ack(self):
        if self.dispatcher.ack:
            return False
        if self.tracker.system_ack_tag in \
                ["ack_opinion", "positive_opinion", "appreciation", "thanking", "opinion_crazy", "opinion_surprising", "not_sure"]:
            self.dispatcher.ack = self.tracker.system_ack_text

            return True
        return False


    def s_ask_watched_movie_question(self):
        question_template_key = \
            self.current_watched_movie_subdialog_manager.get_next_opinion_question_and_update_context()
        self.dispatcher.main_content = self.dispatcher.generate_response(question_template_key)

        self.tracker.current_state = States.ASK_WATCHED_MOVIE_QUESTION
        self.tracker.current_watched_movie_subdialog_context.mode = WatchedMovieDiscussionMode.ASK_OPINION


    def t_ask_watched_movie_question(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.not_watched():
            self.handles_not_watched()
            return

        current_question = self.current_watched_movie_subdialog_manager.get_current_question()
        logging.info(f"current_question: {current_question}")
        if current_question in [SpecificMovieQuestionType.ASK_WHY_VERY_LIKE_MOVIE,
                                SpecificMovieQuestionType.ASK_FAVORITE_PART,
                                SpecificMovieQuestionType.ASK_ANY_LIKED_PART]:
            self.t_ask_why_like_movie()
            return

        elif current_question == SpecificMovieQuestionType.ASK_WHY_DISLIKE_MOVIE:
            self.t_ask_why_dislike_movie()
            return

        elif current_question == SpecificMovieQuestionType.ASK_FAV_CHARACTER:
            self.t_ask_fav_character()
            return

        elif current_question == SpecificMovieQuestionType.ASK_FEELING:
            # self.t_ask_feeling()
            # todo
            return
        elif current_question == SpecificMovieQuestionType.ASK_IF_REWATCH:
            self.t_ask_rewatch()
            return
        elif current_question == SpecificMovieQuestionType.ASK_IF_WATCH_ALONE:
            self.t_ask_if_watch_alone()
            return
        elif current_question == SpecificMovieQuestionType.ASK_IS_LIKED_TYPE:
            self.t_ask_if_is_liked_type()
            return
        else:
            self.execute_next_state_from_watched_movie_subdialog()

    def t_ask_why_like_movie(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        reason = self.tracker.movie_opinion_detector.detect_like_movie_reason()

        # Generate acknowledgement
        template_key_prefix = "specific_movie_watched/ack_why_like_movie/"
        if reason == MovieLikeReason.MUSIC:
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "music")
            # todo: propose music after finishing movie?
        elif reason == MovieLikeReason.ACTION:
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "action")
            # todo: add action movie to user profile?
        elif reason == MovieLikeReason.STORY:
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "story")
        elif reason == MovieLikeReason.SPECIFIC_SCENE:
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "specific_scene")
        elif reason == MovieLikeReason.ACTOR:
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "actor")
            # todo: talk about that actor?
        elif reason == MovieLikeReason.CHARACTER:
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "character")
        elif reason == MovieLikeReason.FUNNY:
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "funny")
            # todo: add comedy to user profile?
        else:
            executed = self.try_actor_genre_movie_flow()
            if executed:
                return
            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

        self.try_generate_general_acknowledgement()
        self.execute_next_state_from_watched_movie_subdialog()

    def t_ask_why_dislike_movie(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        reason = self.tracker.movie_opinion_detector.detect_dislike_movie_reason()
        logging.info(f"[t_ask_why_dislike_movie] dislike movie reason: {reason}")
        template_key_prefix = "specific_movie_watched/ask_why_dislike_movie/ack/"
        if reason == MovieDisikeReason.SCARY:
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "scary")
        elif reason == MovieDisikeReason.DEATH:
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "death")
        elif self.tracker.returnnlp.has_intent("ans_same"):
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "agree")
        elif reason == MovieDisikeReason.GENERAL:
            self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "general")
        else:
            executed = self.try_actor_genre_movie_flow()
            if executed:
                return
            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

        # since user doesn't like the movie, we switch to next subtopic
        self.try_generate_general_acknowledgement()
        self.execute_next_subtopic()

    def t_ask_fav_character(self):
        logging.info("[t_ask_fav_character]")
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        detected_character = self.tracker.movie_opinion_detector.detect_character()
        logging.info(f"detected character: {detected_character}")

        if detected_character:
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_fav_character/ack/character_detected",
                slots={"character": detected_character})
        elif self.tracker.returnnlp.answer_positivity == Positivity.neg or self.tracker.ans_not_sure():
            self.dispatcher.ack = self.dispatcher.generate_response("thats_okay")
        else:
            executed = self.try_actor_genre_movie_flow()
            if executed:
                return
            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

        # Select next state
        self.execute_next_state_from_watched_movie_subdialog()

    def t_ask_rewatch(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.returnnlp.answer_positivity == Positivity.pos or \
            self.tracker.returnnlp.has_intent("ans_same") or \
            re.match(rewatch_positive, self.tracker.text):
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_rewatch/ack/positive")
        elif self.tracker.returnnlp.answer_positivity == Positivity.neg:
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_rewatch/ack/negative")
        else:
            executed = self.try_actor_genre_movie_flow()
            if executed:
                return
            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

        # Select next state
        self.try_use_system_ack()
        self.execute_next_state_from_watched_movie_subdialog()

    def t_ask_if_watch_alone(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        match_watch_alone = re.match(watch_alone, self.tracker.text)

        match_watch_with_others = re.match(watch_with_others, self.tracker.text)
        logging.info(f"match_watch_alone: {match_watch_alone}")
        logging.info(f"match_watch_with_others: {match_watch_with_others}")

        if match_watch_alone and match_watch_with_others:
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_watch_alone/ack/both")
        elif match_watch_alone:
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_watch_alone/ack/alone")
        elif match_watch_with_others:
            self.dispatcher.ack = self.dispatcher.generate_response(
                "specific_movie_watched/ask_if_watch_alone/ack/with_others")
        else:
            executed = self.try_actor_genre_movie_flow()
            if executed:
                return
            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

        self.try_generate_general_acknowledgement()
        self.execute_next_state_from_watched_movie_subdialog()

    def t_ask_if_is_liked_type(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if re.match(normally_watch, self.tracker.text):
            movie_genres = get_movie_genres(self.tracker.current_movie_id)

            if movie_genres:
                self.dispatcher.ack = self.dispatcher.generate_response("specific_movie_watched/ask_is_liked_type/ack/normal")
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("specific_movie_watched/ask_is_liked_type/ack/normal")
            self.s_confirm_liked_genre(movie_genres)
            return
        elif re.match(new_experience, self.tracker.text):
            self.dispatcher.ack = self.dispatcher.generate_response("specific_movie_watched/ask_is_liked_type/ack/new_experience")
        else:
            executed = self.try_actor_genre_movie_flow()
            if executed:
                return
            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

        self.try_generate_general_acknowledgement()

        self.execute_next_subtopic()

    def s_confirm_liked_genre(self, movie_genres: List[Genre]):
        genres_description = ""
        for id, genre in enumerate(movie_genres):
            genre_name = genre.name.lower()
            if id == 0:
                genres_description += f"{genre_name}"  # first genre
            elif id == len(movie_genres) - 1:  # last genre
                genres_description += f", and {genre_name}"
            else:
                genres_description += f", {genre_name}"

        self.dispatcher.main_content = f"So you like {genres_description} movies? "
        self.tracker.current_state = States.CONFIRM_LIKED_MOVIE_GENRE
        self.tracker.current_watched_movie_subdialog_context.potential_liked_movie_genres = \
            [item.value for item in movie_genres]

    def t_confirm_liked_movie_genre(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.ans_yes():
            extracted_genres = GenreExtractor().extract_genre(self.tracker.text)
            if extracted_genres:
                self.tracker.liked_genre_ids = extracted_genres
            else:
                self.tracker.liked_genre_ids = self.tracker.current_watched_movie_subdialog_context.potential_liked_movie_genres

            self.dispatcher.ack = "Awesome, you have a good taste! "
            self.s_ask_liked_movie_in_genre(is_another=True)
            return
        else:
            # todo: handle more cases?
            executed = self.try_actor_genre_movie_flow()
            if executed:
                return
            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return

        self.execute_next_subtopic()


    ####### [end] specific movie subdialog #######


    ####### [begin] genre chitchat #######

    def s_ask_liked_movie_genre(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        self.tracker.module_context.general_chitchat_history.append(GeneralChitchatQuestion.ASK_LIKED_GENRE)
        self.dispatcher.main_content = self.dispatcher.generate_response(
            "ask_genre/ask_favourite_movie_genre",
            slots={"A": "comedy", "B": "action", "C": "science fiction"})
        self.tracker.current_state = States.ASK_FAVORITE_MOVIE_GENRE

    def t_ask_liked_movie_genre(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if "ans_same" in self.tracker.returnnlp.flattened_lexical_intent:
            self.tracker.liked_genre_ids.extend(BOT_FAVORITE_GENRE)

            self.dispatcher.ack = self.dispatcher.generate_response("ask_genre/ack/same")

            self.s_ask_liked_movie_in_genre()
            return

        executed = self.try_actor_genre_movie_flow()
        if executed:
            return

        if self.tracker.ans_not_sure_only() or self.tracker.no_only():
            self.dispatcher.ack = self.dispatcher.generate_response("thats_okay")
        elif re.search(like_all, self.tracker.text):
            self.dispatcher.ack = self.dispatcher.generate_response("ask_genre/like_all")  # todo: ask liked movie?
        elif self.tracker.back_channeling_only():
            self.dispatcher.ack = ""
        elif self.tracker.yes_only():
            self.dispatcher.ack = ""   # todo: should follow up
        else:
            question_handled_and_should_not_move_on = self.try_handles_question()
            if question_handled_and_should_not_move_on:
                return
            if not self.dispatcher.ack:
                self.dispatcher.ack = self.dispatcher.generate_response("ask_genre/unknown_genre")

        self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
        self.s_propose_movie()

    def s_ask_liked_movie_in_genre(self, is_another: bool = False):
        self.tracker.module_context.asking_liked_movie = True

        user_fav_genre_ids = self.tracker.liked_genre_ids
        first_favorite_genre_id = user_fav_genre_ids[0]
        genre_name = GenreExtractor.Genre(first_favorite_genre_id)

        if is_another:
            self.dispatcher.main_content = self.dispatcher.generate_response("ask_movie_in_genre/question/another",
                                                                             slots={"genre": genre_name})
        else:
            self.dispatcher.main_content = self.dispatcher.generate_response("ask_movie_in_genre/question/first",
                                                                             slots={"genre": genre_name})
        self.tracker.current_state = States.ASK_LIKED_MOVIE_IN_GENRE

    def t_ask_liked_movie_in_genre(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.no_only() or self.tracker.ans_not_sure_only():
            self.dispatcher.ack = self.dispatcher.generate_response("thats_okay")
            self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
            self.s_propose_movie()
            return

        if self.tracker.yes_only():
            return self.s_ask_liked_movie_in_genre_follow_up()

        executed = self.try_actor_genre_movie_flow()
        if executed:
            return
        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
        self.s_propose_movie()

    def s_ask_liked_movie_in_genre_follow_up(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("ask_movie_in_genre/question_follow_up")
        self.tracker.current_state = States.ASK_LIKED_MOVIE_IN_GENRE

    ####### [end] genre chitchat #######

    ####### [begin] actor chitchat #######
    def s_ask_liked_actor(self):
        self.tracker.module_context.general_chitchat_history.append(GeneralChitchatQuestion.ASK_LIKED_ACTOR)
        self.dispatcher.main_content = self.dispatcher.generate_response(
            "ask_liked_actor/question",
            slots={"actor": BOT_FAVORITE_ACTOR.get("actor_name"),
                   "opinion": BOT_FAVORITE_ACTOR.get("opinion")}
        )
        self.tracker.current_state = States.ASK_FAVORITE_ACTOR

    def t_ask_liked_actor(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        liked_actor = None
        if "ans_same" in self.tracker.returnnlp.flattened_lexical_intent:
            liked_actor = BOT_FAVORITE_ACTOR.get("actor_name")
            self.dispatcher.ack = self.dispatcher.generate_response(
                "ask_liked_actor/ack/same", slots={"pronoun": BOT_FAVORITE_ACTOR.get("pronoun")})
        else:
            detected_actors = ActorExtractor().extract_actor_names(None, None, self.tracker.text,
                                                                   self.tracker.last_response)
            if detected_actors:
                liked_actor = detected_actors[0]

        executed = self.try_start_actor_flow(liked_actor)
        if executed:
            return

        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        if self.tracker.returnnlp.answer_positivity == Positivity.neg:
            if not self.dispatcher.ack:
                self.dispatcher.ack = self.dispatcher.generate_response("thats_okay")
        else:
            self.try_generate_general_acknowledgement()

        self.execute_next_subtopic()

    def try_actor_flow(self, text: str = None):
        if not text:
            text = self.tracker.text
        detected_actors = ActorExtractor().extract_actor_names(None, None, text,
                                                               self.tracker.last_response)
        if detected_actors:
            executed = self.try_start_actor_flow(detected_actors[0])
            if executed:
                return True

        return False


    def try_execute_next_actor_subdialog_state(self):
        next_state_name = self.actor_subdialog_manager.get_next_state()
        if next_state_name:
            function = self.get_function_from_name("s_", next_state_name)
            function()
            return True
        return False

    def try_start_actor_flow(self, actor_name):
        if not actor_name:
            return False

        if actor_name in self.tracker.module_context.movie_user_profile.liked_actor_names:
            # if user already talked about this actor before, we don't do it again
            return False

        if not self.dispatcher.ack:
            self.dispatcher.ack = self.dispatcher.generate_response(
                "ask_liked_actor/ack/detected", slots={"actor": actor_name})

        self.init_context_for_actor_subdialog(actor_name)

        executed = self.try_execute_next_actor_subdialog_state()
        if executed:
            return True

        return False

    def init_context_for_actor_subdialog(self, actor_name: str):
        self.tracker.liked_actor_names.insert(0, actor_name)


        self.tracker.module_context.current_actor_name = actor_name
        self.actor_subdialog_manager.set_actor_name_and_retrieve_trivia(actor_name)

    def s_provide_actor_trivia(self):
        trivia = self.actor_subdialog_manager.get_new_trivia_and_update_context()
        self.dispatcher.main_content = \
            self.dispatcher.generate_response("actor_subdialog/provide_trivia/intro") + \
            trivia + \
            self.dispatcher.generate_response("actor_subdialog/provide_trivia/question")

        self.tracker.current_state = States.PROVIDE_ACTOR_TRIVIA

    def t_provide_actor_trivia(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.returnnlp.has_intent("req_more"):
            if self.actor_subdialog_manager.has_available_trivia():
                self.s_provide_actor_trivia()
                return

        executed = self.try_actor_genre_movie_flow()
        if executed:
            return

        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        if self.tracker.ans_not_sure() and self.tracker.system_ack_tag == "not_sure":
            self.dispatcher.ack = " "  # hacky way to avoid system adding acknowledgement when user say "i didn't know that"

        self.try_generate_general_acknowledgement()
        self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
        self.execute_next_state_from_actor_subdialog()

    def s_ask_actor_question(self):
        question_template_key = \
            self.actor_subdialog_manager.get_next_opinion_question_and_update_context()
        self.dispatcher.main_content = self.dispatcher.generate_response(
            question_template_key, {"actor_name": self.tracker.current_actor_name})

        self.tracker.current_state = States.ASK_ACTOR_QUESTION
        setattr(self.tracker.user_attributes, "expect_opinion", True)

    def t_ask_actor_question(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        current_question = self.actor_subdialog_manager.get_current_question()
        if current_question == ActorQuestionType.ASK_OPINION_ACTING:
            self.t_ask_opinion_acting()
        elif current_question == ActorQuestionType.ASK_FAV_MOVIE_WITH_ACTOR:
            self.t_ask_fav_movie_with_actor()
        else:
            self.execute_next_state_from_actor_subdialog()

    def execute_next_state_from_watched_movie_subdialog(self):
        executed_next_state = self.try_execute_next_watched_movie_subdialog_state()
        if executed_next_state:
            return
        else:
            self.execute_next_subtopic()

    def try_execute_next_watched_movie_subdialog_state(self):
        next_state_name = self.current_watched_movie_subdialog_manager.get_next_state()
        if next_state_name:
            function = self.get_function_from_name("s_", next_state_name)
            function()
            return True
        return False

    def execute_next_state_from_actor_subdialog(self):
        executed_next_state = self.try_execute_next_actor_subdialog_state()
        if executed_next_state:
            return
        else:
            self.execute_next_subtopic()

    def execute_next_subtopic(self):
        self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
        next_subtopic = self.get_next_subtopic()
        next_subtopic()

    def t_ask_opinion_acting(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        self.try_use_system_ack()

        if not self.dispatcher.ack:
            opinion = self.tracker.actor_opinion_detector.detect_opinion()
            if opinion == ActorOpinion.POSITIVE:
                self.dispatcher.ack = self.dispatcher.generate_response(
                    "actor_subdialog/ask_opinion_on_acting/ack/positive")
            elif opinion == ActorOpinion.NEGATIVE:
                self.dispatcher.ack = self.dispatcher.generate_response(
                    "actor_subdialog/ask_opinion_on_acting/ack/negative")
            elif opinion == ActorOpinion.MIXED:
                self.dispatcher.ack = self.dispatcher.generate_response(
                    "actor_subdialog/ask_opinion_on_acting/ack/mixed")
            elif opinion == ActorOpinion.OTHER:
                self.dispatcher.ack = self.dispatcher.generate_response("ack/general/short")
                self.dispatcher.ack += self.dispatcher.generate_response(
                    "actor_subdialog/ask_opinion_on_acting/ack/other")

        executed = self.try_actor_genre_movie_flow()
        if executed:
            return

        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        self.execute_next_state_from_actor_subdialog()

    def t_ask_fav_movie_with_actor(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.no_only() or self.tracker.ans_not_sure_only():
            self.dispatcher.ack = self.dispatcher.generate_response("actor_subdialog/ask_fav_movie_with_actor/ack/negative")
            # todo

        self.tracker.module_context.asking_liked_movie = True  # todo: reset it if interrupted afterwards

        executed = self.try_actor_genre_movie_flow()
        if executed:
            return

        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        # undetected
        self.try_generate_general_acknowledgement()
        if not self.dispatcher.ack:
            self.dispatcher.ack = self.dispatcher.generate_response(
                "actor_subdialog/ask_fav_movie_with_actor/ack/not_detected")

        self.execute_next_state_from_actor_subdialog()


    ####### [end] actor chitchat #######


    ####### [end] general chitchat #######
    def s_ask_watch_review_or_trailer(self):
        self.tracker.module_context.general_chitchat_history.append(GeneralChitchatQuestion.ASK_WATCH_REVIEW_OR_TRAILER)
        self.dispatcher.main_content = self.dispatcher.generate_response(
            "ask_watch_review_or_trailer/question")
        self.tracker.current_state = States.ASK_WATCH_REVIEW_OR_TRAILER
        print("todo")
        # todo

    def t_ask_watch_review_or_trailer(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if not self.dispatcher.ack:
            if self.tracker.returnnlp.answer_positivity == Positivity.neg:
                self.dispatcher.ack = self.dispatcher.generate_response("ask_watch_review_or_trailer/ack/negative")
            elif self.tracker.returnnlp.answer_positivity == Positivity.pos:
                self.dispatcher.ack = self.dispatcher.generate_response("ask_watch_review_or_trailer/ack/positive")
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("ask_watch_review_or_trailer/ack/default")

        executed = self.try_actor_genre_movie_flow()
        if executed:
            return

        question_handled_and_should_not_move_on = self.try_handles_question()
        if question_handled_and_should_not_move_on:
            return

        self.try_generate_general_acknowledgement()
        self.execute_next_subtopic()


class GeneralChitchatQuestion:
    ASK_LIKED_GENRE = "ask_liked_genre"
    ASK_LIKED_ACTOR = "ask_liked_actor"
    ASK_WATCH_REVIEW_OR_TRAILER = "ask_watch_review_or_trailer"