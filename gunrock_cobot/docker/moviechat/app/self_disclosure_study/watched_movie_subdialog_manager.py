import logging
import re

from dataclasses import dataclass, asdict, field
from typing import List, Optional
import random

from dispatcher import Dispatcher
from movie_module_context import WatchedMoviePreferenceLevel, WatchedMovieDiscussionMode, States, \
    CurrentWatchedMovieSubdialogContext
from movie_user_profile import MovieUserProfile
from selecting_strategy.module_selection import ProposeContinue
from nlu.constants import Positivity, DialogAct
from movietv_command_detector import MovieCommand
from movie_module_regex import \
    that_is_the_one, clarify_movie_name, like_all, rewatch_positive, watch_alone, watch_with_others, normally_watch, new_experience
from movie_preference_predictor import MoviePreferencePredictor, POPULAR_MOVIE_IDS
from movie_preference_extractor import MovieExtractor, ActorExtractor, GenreExtractor, Genre
from movie_opinion_detector import MoviePreferenceLevel, MovieLikeReason, MovieDisikeReason
from actor_opinion_detector import ActorOpinion
from actor_subdialog import ActorQuestionType, ActorSubdialogManager
from movietv_question_handler import MovieQuestionHandler
from movietv_utils import get_imdb_trivia_new, get_movie_genres, get_movie_plot_summary
from tmdb_utils import get_now_playing_movies
import template_manager
from tracker import Tracker
from nlu.constants import TopicModule
from popular_movie_trivia import popular_movie_detail


class SpecificMovieQuestionType:
    # Better for liked movie

    ASK_WHY_VERY_LIKE_MOVIE = "ask_why_very_like_movie"

    ASK_FAVORITE_PART = "ask_favorite_part"
    # ASK_FAVORITE_SCENE = "ask_favorite_scene"

    ASK_FAV_CHARACTER = "ask_fav_character"



class WatchedMovieSubdialogManager:

    def __init__(self, movie_id,
                 user_profile: MovieUserProfile,
                 current_watched_movie_subdialog: CurrentWatchedMovieSubdialogContext,
                 previous_asked_questions: List[str]):
        self.movie_id = movie_id

        self.trivia = []
        self.user_profile = user_profile
        self.current_watched_movie_subdialog = current_watched_movie_subdialog
        self.previous_asked_question = previous_asked_questions

    def set_movie_preference_level(self, level: WatchedMoviePreferenceLevel):
        self.current_watched_movie_subdialog.movie_preference_level = level.value

    def get_trivia(self):
        if self.trivia:
            return self.trivia

        self.trivia = get_imdb_trivia_new(self.movie_id) if self.movie_id else list()
        return self.trivia

    def get_next_state(self):
        # todo: consider engagement here to see if we should stop earlier
        result = None
        logging.info(f"[movie: get_next_state] current mode: {self.current_watched_movie_subdialog.mode}")
        if self.current_watched_movie_subdialog.mode == WatchedMovieDiscussionMode.ASK_OPINION:
            if self.has_available_trivia() and self.used_trivial_fewer_than_default():
                self.current_watched_movie_subdialog.mode = WatchedMovieDiscussionMode.PROVIDE_TRIVIA
                result = "provide_watched_movie_trivia"
            elif self.has_available_opinion_question() and self.used_question_fewer_than_default():
                self.current_watched_movie_subdialog.mode = WatchedMovieDiscussionMode.ASK_OPINION
                result = "ask_watched_movie_question"
        elif self.current_watched_movie_subdialog.mode == WatchedMovieDiscussionMode.PROVIDE_TRIVIA:
            if self.has_available_opinion_question() and self.used_question_fewer_than_default():
                self.current_watched_movie_subdialog.mode = WatchedMovieDiscussionMode.ASK_OPINION
                result = "ask_watched_movie_question"
            elif self.has_available_trivia() and self.used_trivial_fewer_than_default():
                self.current_watched_movie_subdialog.mode = WatchedMovieDiscussionMode.PROVIDE_TRIVIA
                result = "provide_watched_movie_trivia"
        return result

    def has_available_trivia(self):

        used_trivia = self.current_watched_movie_subdialog.trivial_used_count
        result = used_trivia < len(self.get_trivia())
        logging.info(f"[has_available_trivia] {result}")
        return result

    def used_trivial_fewer_than_default(self):
        result = self.current_watched_movie_subdialog.trivial_used_count < self.get_max_trivia_count()
        logging.info(f"[used_trivial_fewer_than_default] {result}")
        return result

    def used_question_fewer_than_default(self):
        result = self.current_watched_movie_subdialog.question_used_count < self.get_max_question_count()
        logging.info(f"[used_question_fewer_than_default] {result}")

        return result
    def get_new_trivia_and_update_count(self):
        result = self.get_trivia()[self.current_watched_movie_subdialog.trivial_used_count] + " "
        self.current_watched_movie_subdialog.trivial_used_count += 1
        return result

    def has_available_opinion_question(self):
        next_question = self.get_next_opinion_question()
        result = next_question is not None
        logging.info(f"[has_available_opinion_question] {result}")
        return result

    def get_next_opinion_question(self):
        used_questions = self.current_watched_movie_subdialog.question_asked

        logging.info(f"[get_next_opinion_question] used_questions: {used_questions}")
        for candidates in self.get_question_candidates():
            logging.info(f"[get_next_opinion_question] candidate: {candidates}")
            if not set(candidates).intersection(set(used_questions)):

                for candidate in candidates:
                    if candidate not in self.previous_asked_question:
                        logging.info(f"[get_next_opinion_question] selected candidate: {candidate}")

                        return candidate
        return None


    def get_next_opinion_question_and_update_context(self) -> str:
        """
        :return: question template key
        """
        next_opinion_question = self.get_next_opinion_question()
        logging.info(f"next_opinion_question: {next_opinion_question}")

        if next_opinion_question:
            self.current_watched_movie_subdialog.question_asked.append(next_opinion_question)
            self.current_watched_movie_subdialog.question_used_count += 1
            self.previous_asked_question.append(next_opinion_question)
            return "specific_movie_watched/{}/question".format(next_opinion_question)

        else:
            return ""

    def get_current_question(self):
        if self.current_watched_movie_subdialog.question_asked:
            return self.current_watched_movie_subdialog.question_asked[-1]
        else:
            return None

    def get_movie_preference_level(self) -> Optional[WatchedMoviePreferenceLevel]:
        if self.current_watched_movie_subdialog.movie_preference_level:
            return WatchedMoviePreferenceLevel(self.current_watched_movie_subdialog.movie_preference_level)
        else:
            return None

    def get_max_question_count(self):
        return 2

    def get_max_trivia_count(self):
        return 1

    def get_question_candidates(self) -> List[List[str]]:
        return [
            [
                SpecificMovieQuestionType.ASK_WHY_VERY_LIKE_MOVIE
             ],
            [
                SpecificMovieQuestionType.ASK_FAVORITE_PART
            ]
        ]
