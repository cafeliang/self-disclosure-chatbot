import logging
import re

from dataclasses import dataclass, asdict, field
from typing import List, Optional
import random

from self_disclosure_study.dispatcher import Dispatcher
from self_disclosure_study.movie_module_context import WatchedMoviePreferenceLevel, WatchedMovieDiscussionMode, States, \
    CurrentWatchedMovieSubdialogContext
from movie_user_profile import MovieUserProfile
from selecting_strategy.module_selection import ProposeContinue
from nlu.constants import Positivity, DialogAct
from movietv_command_detector import MovieCommand
from movie_module_regex import \
    that_is_the_one, clarify_movie_name, like_all, rewatch_positive, watch_alone, watch_with_others, normally_watch, new_experience
from movie_preference_extractor import MovieExtractor, ActorExtractor, GenreExtractor, Genre
from movie_opinion_detector import MoviePreferenceLevel, MovieLikeReason, MovieDisikeReason
from actor_opinion_detector import ActorOpinion
from actor_subdialog import ActorQuestionType, ActorSubdialogManager
from self_disclosure_study.movie_question_handler import MovieQuestionHandler
from movietv_utils import get_imdb_trivia_new, get_movie_genres, get_movie_plot_summary
from self_disclosure_study.watched_movie_subdialog_manager import WatchedMovieSubdialogManager, \
    SpecificMovieQuestionType
from tmdb_utils import get_now_playing_movies
import template_manager
from self_disclosure_study.tracker import Tracker
from nlu.constants import TopicModule
from popular_movie_trivia import popular_movie_detail
from movietv_utils import get_actor_trivia_from_reddit, Genre
from self_disclosure_study.movie_preference_predictor import MoviePreferencePredictor
from tmdb_utils import tmdb_person_search

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('Movie').setLevel(logging.DEBUG)

BOT_FAVORITE_GENRE = [
    Genre.COMEDY.value,
]

class MovieTVAutomaton:

    def __init__(self, input_data, user_attributes):
        self.tracker = Tracker(input_data, user_attributes)
        self.dispatcher = Dispatcher(user_attributes)
        self.movie_question_handler = MovieQuestionHandler(self.tracker.text,
                                                           input_data['returnnlp'][0],
                                                           self.tracker,
                                                           self.dispatcher)
        self.current_watched_movie_subdialog_manager = WatchedMovieSubdialogManager(
            self.tracker.current_movie_id,
            self.tracker.module_context.movie_user_profile,
            self.tracker.module_context.current_watched_movie_subdialog,
            self.tracker.module_context.previous_asked_questions
        )

        self.actor_subdialog_manager = ActorSubdialogManager(
            self.tracker.module_context.actor_subdialog
        )
        self.template = template_manager.Templates.self_disclosure_movie_factual

    def transduce(self):
        logging.info(f"[transduce]: input retunnlp: {self.tracker.returnnlp}")

        if self.tracker.enter_from_other_module:
            self.tracker.propose_continue = ProposeContinue.CONTINUE

        current_context = self.tracker.module_context

        # todo: handles command and drive the flow back
        # topic_switch_intents = self.tracker.command_detector.detect_intents()
        # if topic_switch_intents:
        #     self.handles_topic_switch(topic_switch_intents[0])
        # else:

        state_handler = self.get_function_from_name("t_", current_context.current_state)
        logging.info(f"state_handler: {state_handler}")
        state_handler()

        responses = [
            self.dispatcher.response_text.ack,
            self.dispatcher.response_text.transition,
            self.dispatcher.response_text.main_content,
        ]

        final_response = " ".join(filter(None, responses))

        result = {
            "response": final_response,
            "moviechat_user": asdict(self.tracker.module_context)
        }

        logging.info(f"[transduce] result: {result}")

        return result

    def get_function_from_name(self, prefix, function_name):
        return getattr(self, "{}{}".format(prefix, function_name))


    ##########################
    # States and transitions
    ##########################
    def t_init(self):
        self.s_ask_if_enjoy_watching_movie()

    def s_ask_if_enjoy_watching_movie(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("ask_if_enjoy_watching_movie/question")
        self.tracker.current_state = States.ASK_IF_ENJOY_WATCHING_MOVIE

    def t_ask_if_enjoy_watching_movie(self):
        # todo: add acknowledgement
        self.dispatcher.ack = self.dispatcher.generate_response("ack/positive")
        self.s_ask_liked_movie()

    def s_ask_liked_movie(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("ask_liked_movie/question")

        self.tracker.current_state = States.ASK_LIKED_MOVIE

    def t_ask_liked_movie(self):
        specific_movie_flow_executed = self.try_specific_movie_flow(self.tracker.text)
        if specific_movie_flow_executed:
            logging.info("specific_movie_flow_executed")
            return
        else:
            self.try_handles_question()
            self.try_generate_general_acknowledgement()
            self.s_ask_movie_again()
            return
        # todo

    def s_ask_movie_again(self):
        logging.info("[s_ask_movie_again]")
        if not self.dispatcher.ack:
            self.dispatcher.ack = self.dispatcher.generate_response("movie_detection_fail/ack")
        self.dispatcher.main_content = self.dispatcher.generate_response("movie_detection_fail/ask_another_movie")

        self.tracker.current_state = States.ASK_LIKED_MOVIE

    def s_ask_why_like_movie(self):
        self.dispatcher.main_content = self.dispatcher.generate_response(
            "specific_movie_watched/ask_why_like_movie/question")

        self.tracker.current_state = States.ASK_LIKED_MOVIE

    def s_ask_watched_movie_question(self):
        question_template_key = \
            self.current_watched_movie_subdialog_manager.get_next_opinion_question_and_update_context()
        self.dispatcher.main_content = self.dispatcher.generate_response(question_template_key)

        self.tracker.current_state = States.ASK_WATCHED_MOVIE_QUESTION
        self.tracker.current_watched_movie_subdialog_context.mode = WatchedMovieDiscussionMode.ASK_OPINION

    def t_ask_watched_movie_question(self):
        should_not_move_on = self.movie_question_handler.handles_topic_specific_question()
        if should_not_move_on:
            return

        if self.tracker.not_watched():
            self.handles_not_watched()
            return

        current_question = self.current_watched_movie_subdialog_manager.get_current_question()
        logging.info(f"current_question: {current_question}")
        if current_question in [SpecificMovieQuestionType.ASK_WHY_VERY_LIKE_MOVIE]:
            self.t_ask_why_like_movie()
            return

        else:
            self.t_ask_favorite_part()

    def handles_not_watched(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("wrong_movie")
        self.tracker.current_state = States.ASK_LIKED_MOVIE

    def s_provide_watched_movie_trivia(self):
        trivia = self.current_watched_movie_subdialog_manager.get_new_trivia_and_update_count()
        self.dispatcher.main_content = \
            self.dispatcher.generate_response("specific_movie_watched/provide_trivia/intro") + \
            trivia + \
            self.dispatcher.generate_response("specific_movie_watched/provide_trivia/question")

        self.tracker.current_state = States.PROVIDE_WATCHED_MOVIE_TRIVIA
        self.tracker.current_watched_movie_subdialog_context.mode = WatchedMovieDiscussionMode.PROVIDE_TRIVIA


    def t_provide_watched_movie_trivia(self):
        if self.tracker.returnnlp.has_intent("req_more"):
            self.dispatcher.ack = "Well, that's all i know for now. "

        self.try_generate_general_acknowledgement()
        self.execute_next_state_from_watched_movie_subdialog()

    def t_ask_why_like_movie(self):
        # reason = self.tracker.movie_opinion_detector.detect_like_movie_reason()

        # Generate acknowledgement
        # Todo: add this part for cognitive/emotional condition
        # template_key_prefix = "specific_movie_watched/ack_why_like_movie/"
        # if reason == MovieLikeReason.MUSIC:
        #     self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "music")
        # elif reason == MovieLikeReason.ACTION:
        #     self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "action")
        # elif reason == MovieLikeReason.STORY:
        #     self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "story")
        # elif reason == MovieLikeReason.SPECIFIC_SCENE:
        #     self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "specific_scene")
        # elif reason == MovieLikeReason.ACTOR:
        #     self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "actor")
        # elif reason == MovieLikeReason.CHARACTER:
        #     self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "character")
        # elif reason == MovieLikeReason.FUNNY:
        #     self.dispatcher.ack = self.dispatcher.generate_response(template_key_prefix + "funny")
        # else:
        #     executed = self.try_actor_genre_movie_flow()
        #     if executed:
        #         return
        #     question_handled_and_should_not_move_on = self.try_handles_question()
        #     if question_handled_and_should_not_move_on:
        #         return

        self.try_handles_question()
        self.try_generate_general_acknowledgement()
        self.execute_next_state_from_watched_movie_subdialog()

    def t_ask_favorite_part(self):
        self.try_handles_question()
        self.try_generate_general_acknowledgement()
        self.s_ask_liked_movie_genre()
        # todo

    def s_ask_liked_movie_genre(self):
        self.dispatcher.main_content = self.dispatcher.generate_response(
            "liked_movie_genre/question", slots={"bot_favorite": "comedy"})

        self.tracker.current_state = States.ASK_FAVORITE_MOVIE_GENRE

    def t_ask_liked_movie_genre(self):
        if "ans_same" in self.tracker.returnnlp.flattened_lexical_intent:
            self.tracker.liked_genre_ids.extend(BOT_FAVORITE_GENRE)
            self.dispatcher.ack = self.dispatcher.generate_response("liked_movie_genre/ack/detected/same_preference")
            self.s_ask_disliked_movie_genre()
            return

        text = self.tracker.returnnlp.text_excluding_dislike_segments
        genre_flow_executed = self.try_movie_genre_flow(text)
        if genre_flow_executed:
            logging.info("genre_detected")
            return
        else:
            self.try_handles_question()
            self.try_generate_general_acknowledgement()

            if not self.dispatcher.ack:
                self.dispatcher.ack = "I see. "

            self.s_ask_disliked_movie_genre()

    def s_ask_disliked_movie_genre(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("disliked_movie_genre/question")
        self.tracker.current_state = States.ASK_DISLIKE_MOVIE_GENRE

    def t_ask_disliked_movie_genre(self):
        extracted_genres = GenreExtractor().extract_genre(self.tracker.text)
        if extracted_genres:
            if Genre.HORROR.value in extracted_genres:
                self.dispatcher.ack = self.dispatcher.generate_response("disliked_movie_genre/ack/detected/same_preference", {"genre": Genre(extracted_genres[0]).get_name()})
            elif Genre.COMEDY.value in extracted_genres:
                self.dispatcher.ack = self.dispatcher.generate_response("disliked_movie_genre/ack/detected/different_preference", {"genre": Genre(extracted_genres[0]).get_name()})
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("disliked_movie_genre/ack/detected/general", {"genre": Genre(extracted_genres[0]).get_name()})  # todo
        else:
            self.try_handles_question()
            self.try_generate_general_acknowledgement()

            if not self.dispatcher.ack:
                self.dispatcher.ack = "Okay. "

        self.s_ask_liked_actor()

    def s_ask_liked_actor(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("liked_actor/question")
        self.tracker.current_state = States.ASK_LIKED_ACTOR

    def t_ask_liked_actor(self):
        text = self.tracker.returnnlp.text_excluding_dislike_segments

        actor_flow_executed = self.try_actor_flow(text)
        if actor_flow_executed:
            logging.info("actor_flow_executed")
            return
        else:
            if self.tracker.returnnlp.answer_positivity is Positivity.neg or self.tracker.ans_not_sure():
                self.dispatcher.ack = self.dispatcher.generate_response("ack/negative")
            else:
                self.try_handles_question()
                self.try_generate_general_acknowledgement()
                if not self.dispatcher.ack:
                    self.dispatcher.ack = "okay cool. "

            self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
            self.s_recommend_movie()


    def s_provide_actor_trivia(self):
        actor_name = self.tracker.module_context.current_actor_name
        if actor_name:
            retrieved_trivia = get_actor_trivia_from_reddit(actor_name)
            top_trivia = retrieved_trivia[0] + " "
            logging.info(f"retrieved_trivia: {retrieved_trivia}")

            self.dispatcher.main_content = \
                self.dispatcher.generate_response("actor_trivia/intro") + \
                top_trivia + \
                self.dispatcher.generate_response("actor_trivia/question")
            logging.info(f"retrieved_trivia: {top_trivia}")
            self.tracker.current_state = States.PROVIDE_ACTOR_TRIVIA
            return True
        else:
            # todo
            return False

    def t_provide_actor_trivia(self):
        self.try_handles_question()
        self.try_generate_general_acknowledgement()
        if not self.dispatcher.ack:
            self.dispatcher.ack = "Yeah. "

        self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
        self.s_recommend_movie()

    def s_recommend_movie(self):
        actor_name, genre_name, recommended_movie_name = self.recommend_movie()

        self.dispatcher.main_content = self.dispatcher.generate_response(
            "recommend_movie/introduce_movie_name", {"movie_name": recommended_movie_name,
                                  "actor": actor_name,
                                  "genre": genre_name
                                  })

        self.tracker.current_state = States.RECOMMEND_MOVIE

    def s_recommend_another_movie(self):
        actor_name, genre_name, recommended_movie_name = self.recommend_movie()

        self.dispatcher.main_content = self.dispatcher.generate_response(
            "recommend_movie/introduce_another_movie_name", {"movie_name": recommended_movie_name,
                                  "actor": actor_name,
                                  "genre": genre_name
                                  })

        self.tracker.current_state = States.RECOMMEND_MOVIE

    def recommend_movie(self):
        current_movie_info = self.tracker.current_movie_info
        movie_preference_predictor = MoviePreferencePredictor(
            current_movie_info,
            self.tracker.module_context.movie_user_profile,
            self.tracker.mentioned_movie_ids,
            self.tracker.a_b_test
        )
        recommended_movie_info, connection = movie_preference_predictor.pick_next_movie_with_info()
        recommended_movie_name = recommended_movie_info["movie_name"]
        recommended_movie_id = recommended_movie_info["movie_id"]

        self.tracker.module_context.proposed_movie_ids.append(recommended_movie_id)
        self.tracker.mentioned_movie_ids.append(recommended_movie_id)

        if connection == MoviePreferencePredictor.Connection.ACTOR_AND_GENRE:
            actor_name = self.tracker.liked_actor_names[0]
            genre_id = self.tracker.liked_genre_ids[0]
            genre_name = Genre.from_int(genre_id).get_name()
        elif connection == MoviePreferencePredictor.Connection.ACTOR:
            actor_name = self.tracker.liked_actor_names[0]
            genre_id = recommended_movie_info["movie_genre_ids"][0]
            genre_name = Genre.from_int(genre_id).get_name()
        elif connection == MoviePreferencePredictor.Connection.GENRE:
            actor_name = recommended_movie_info["movie_leading_actor"]["name"]
            genre_id = self.tracker.liked_genre_ids[0]
            genre_name = Genre.from_int(genre_id).get_name()
        else:
            actor_name = recommended_movie_info["movie_leading_actor"]["name"]
            genre_id = recommended_movie_info["movie_genre_ids"][0]
            genre_name = Genre.from_int(genre_id).get_name()
        return actor_name, genre_name, recommended_movie_name

    def t_recommend_movie(self):
        if self.tracker.returnnlp.answer_positivity is Positivity.neg or self.tracker.ans_not_sure():
            self.try_handles_question()
            self.try_generate_general_acknowledgement()
            self.s_ask_if_interested_in_recommended_movie()
        else:  # assume it's a positive answer here
            # todo: improve detection and acknowledgement
            self.try_handles_question()
            self.try_generate_general_acknowledgement()

            if not self.dispatcher.ack:
                self.dispatcher.ack = "Nice. "

            self.s_recommend_another_movie()

    def s_ask_if_interested_in_recommended_movie(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("recommend_movie/ask_if_interested/question")
        self.tracker.current_state = States.ASK_IF_INTERESTED_IN_RECOMMENDED_MOVIE

    def t_ask_if_interested_in_recommended_movie(self):
        if self.tracker.returnnlp.answer_positivity is Positivity.neg and not self.tracker.returnnlp.has_intent("req_more"):
            self.dispatcher.ack = self.dispatcher.generate_response("recommend_movie/ask_if_interested/ack/negative")
            self.s_ending()
        else:
            self.try_handles_question()
            self.try_generate_general_acknowledgement()
            self.s_provide_movie_plot()

    def s_provide_movie_plot(self):
        movie_id = self.tracker.module_context.proposed_movie_ids[-1]
        plot = get_movie_plot_summary(movie_id)

        self.dispatcher.main_content = self.dispatcher.generate_response("recommend_movie/provide_movie_plot/content",
                                                                         slots={'plot': plot})
        self.tracker.current_state = States.PROVIDE_MOVIE_PLOT

    def t_provide_movie_plot(self):
        if self.tracker.returnnlp.answer_positivity is Positivity.neg:
            self.dispatcher.ack = self.dispatcher.generate_response("recommend_movie/provide_movie_plot/ack/negative")
        else:
            self.try_handles_question()
            self.try_generate_general_acknowledgement()
            self.dispatcher.ack = self.dispatcher.generate_response("recommend_movie/provide_movie_plot/ack/positive")

        self.s_ending()

    def s_ending(self):
        if self.tracker.is_first_conversation:
            self.dispatcher.main_content = self.dispatcher.generate_response("ending_first_conv")
        else:
            self.dispatcher.main_content = self.dispatcher.generate_response("ending_last_conv")

        self.tracker.propose_continue = ProposeContinue.STOP

    def s_movie_name_grounding(self):
        grounding_count = self.tracker.movie_name_grounding_count
        detected_movies_info = self.tracker.detected_movies_info
        logging.debug("detected movie infos: {}".format(detected_movies_info))
        logging.debug("grounding_count: {}".format(grounding_count))

        current_ground_movie_info = detected_movies_info[grounding_count]
        logging.debug("current_ground_movie_info: {}".format(current_ground_movie_info))

        leading_actor = current_ground_movie_info.get("movie_leading_actor", {})

        if leading_actor:
            actor_name = leading_actor.get("name", "")
            actor_role = leading_actor.get("role", "")
            if actor_name and actor_role:
                template_args = {
                    "movie_title": current_ground_movie_info["movie_name"],
                    "release_year": current_ground_movie_info["movie_release_year"],
                    "actor_name": actor_name,
                    "actor_role": actor_role,
                }
                self.dispatcher.main_content = self.dispatcher.generate_response("ground_movie", template_args)
        else:
            template_args = {
                "movie_title": current_ground_movie_info["movie_name"],
                "release_year": current_ground_movie_info["movie_release_year"],
            }
            self.dispatcher.main_content = self.dispatcher.generate_response("ground_movie_no_actor", template_args)

        self.tracker.current_state = States.MOVIE_NAME_GROUNDING

    def t_movie_name_grounding(self):
        if self.tracker.ans_yes() or re.search(that_is_the_one, self.tracker.text):
            confirmed_movie_info = self.tracker.detected_movies_info[self.tracker.movie_name_grounding_count]

            self.dispatcher.ack = self.dispatcher.generate_response("ground_movie_guess_right")
            self.handles_detected_movie(confirmed_movie_info)
            return

        if self.tracker.no_only():
            self.tracker.movie_name_grounding_count += 1
            max_grounding_count = 1
            if self.tracker.movie_name_grounding_count <= max_grounding_count:
                self.s_movie_name_grounding()
                return
            else:
                self.s_ask_movie_again()
        else:
            if self.tracker.returnnlp.answer_positivity == Positivity.neg:
                self.tracker.text = re.sub(r"^(no i'm thinking of )|^(no )", "", self.tracker.text)

            specific_movie_flow_exectued = self.try_specific_movie_flow(self.tracker.text)
            if specific_movie_flow_exectued:
                return

            self.s_ask_movie_again()
            return


    ##########################
    # Reusable functions
    ##########################
    def try_generate_general_acknowledgement(self):
        if self.dispatcher.ack:
            return False

        use_system_ack = self.try_use_system_ack()
        if use_system_ack:
            return True

        if self.tracker.returnnlp.answer_positivity is Positivity.neg or self.tracker.ans_not_sure():
            logging.info("self.tracker.returnnlp.answer_positivity is Positivity.neg or self.tracker.ans_not_sure()")
            self.dispatcher.ack = self.dispatcher.generate_response("ack/negative")

        if self.tracker.user_engagement_detector.is_engaged_answer() or \
            self.tracker.returnnlp.has_dialog_act(DialogAct.STATEMENT):
            if len(self.tracker.text.split()) >= 5:
                self.dispatcher.ack = self.dispatcher.generate_response("ack/general/medium")
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("ack/general/short")


    def try_handles_question(self):
        """
        :return: False if replace prefix only and move on. True if insert one more turn or propose other topic
        """
        answer = self.movie_question_handler.handle_question()
        logging.info(f"[try_handles_question] {answer}")

        if answer:
            self.dispatcher.ack = answer.response
            self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")


    def try_actor_genre_movie_flow(self):
        logging.info("[try_detect_genre_and_movie_flow] ")
        text = self.tracker.returnnlp.text_excluding_dislike_segments

        if self.tracker.back_channeling_only():
            return False

        if not text:
            return False

        actor_flow_executed = self.try_actor_flow(text)
        if actor_flow_executed:
            logging.info("actor_flow_executed")
            return True

        if not self.tracker.is_follow_up_question():
            # avoid detecting genre if user ask "is it a scary movie"
            genre_flow_executed = self.try_movie_genre_flow(text)
            if genre_flow_executed:
                logging.info("genre_detected")
                return True

        specific_movie_flow_exectued = self.try_specific_movie_flow(text)
        logging.info("movie_name_detected")
        if specific_movie_flow_exectued:
            return True
        return False

    def try_actor_flow(self, text: str = None):
        if not text:
            text = self.tracker.text
        detected_actors = ActorExtractor().extract_actor_names(None, None, text,
                                                               self.tracker.last_response)

        logging.info(f"detected actors: {detected_actors}")
        if detected_actors:
            liked_actor = detected_actors[0]
            self.init_context_for_actor_subdialog(liked_actor)
            # self.dispatcher.ack = "Nice. "  # todo
            self.dispatcher.ack = self.dispatcher.generate_response("liked_actor/ack",
                                                                    {"actor": liked_actor})

            trivia_provided = self.s_provide_actor_trivia()
            if trivia_provided:
                return True

        return False


    def init_context_for_actor_subdialog(self, actor_name: str):
        self.tracker.liked_actor_names.insert(0, actor_name)

        actor_info = tmdb_person_search(actor_name)
        logging.info(f"actor_info: {actor_info}")
        if actor_info:
            actor_id = actor_info[0].get("id", "")

            self.tracker.liked_actor_ids.insert(0, actor_id)

        logging.info(f"tracker.movie.liked_actor_names: {self.tracker.liked_actor_names}")
        logging.info(f"tracker.movie.liked_actor_ids: {self.tracker.liked_actor_ids}")

        self.tracker.module_context.current_actor_name = actor_name
        self.actor_subdialog_manager.set_actor_name_and_retrieve_trivia(actor_name)


    def try_movie_genre_flow(self, text=None):
        logging.info("[try_detect_movie_genre_flow]")

        if not text:
            text = self.tracker.text

        if self.tracker.back_channeling_only():
            return False

        extracted_genres = GenreExtractor().extract_genre(text)
        # assume user mention the genre they like at last segment
        # eg. no i don't like action movie i like romance movie
        # todo: handles if the genre is already in the list
        if extracted_genres:
            if extracted_genres[0] in self.tracker.liked_genre_ids:
                # Don't follow up on that genre if it's already in liked genre
                return False

            logging.info(f"[try_detect_movie_genre_flow] extracted_genres: {extracted_genres}")
            all_favorite_genre_ids = extracted_genres + self.tracker.liked_genre_ids
            self.tracker.liked_genre_ids = all_favorite_genre_ids

            if Genre.COMEDY.value in self.tracker.liked_genre_ids:
                self.dispatcher.ack = self.dispatcher.generate_response("liked_movie_genre/ack/detected/same_preference", {"genre": Genre.COMEDY.get_name()}) # todo
            elif Genre.HORROR.value in self.tracker.liked_genre_ids:
                self.dispatcher.ack = self.dispatcher.generate_response("liked_movie_genre/ack/detected/different_taste") # todo
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("liked_movie_genre/ack/detected/general", {"genre": Genre(self.tracker.liked_genre_ids[0]).get_name()}) # todo

            self.s_ask_disliked_movie_genre()
            return True
        return False


    def try_specific_movie_flow(self, text: str = None):
        logging.info("try_specific_movie_flow")
        if not text:
            text = self.tracker.text

        movie_infos = MovieExtractor().extract_movies_v2(text, self.tracker.last_response)
        logging.info(f"[try_specific_movie_flow] movie_infos: {movie_infos}")
        if movie_infos:
            self.tracker.module_context.last_movie_proposed_by_bot = False

            if len(movie_infos) > 1:  # need grounding
                self.tracker.movie_name_grounding_count = 0
                self.tracker.detected_movies_info = movie_infos
                logging.info(f"self.tracker.detected_movies_info: {movie_infos}")
                self.dispatcher.ack = self.dispatcher.generate_response("ground_movie_ack")
                self.s_movie_name_grounding()
            else:

                detected_movie_info = movie_infos[0]
                movie_name = detected_movie_info["movie_name"]

                self.dispatcher.ack = self.dispatcher.generate_response(
                    "know_movie", slots={"movie_title": movie_name})

                self.tracker.liked_movie_ids.insert(0, detected_movie_info["movie_id"])
                self.handles_detected_movie(detected_movie_info)
            return True
        else:
            return False

    def handles_detected_movie(self, detected_movie_info: dict):
        self.init_context_for_current_movie(detected_movie_info)

        self.try_use_system_ack()

        self.execute_next_state_from_watched_movie_subdialog()

    def execute_next_state_from_watched_movie_subdialog(self):
        executed_next_state = self.try_execute_next_watched_movie_subdialog_state()
        if executed_next_state:
            return
        else:
            self.execute_next_subtopic()

    def try_execute_next_watched_movie_subdialog_state(self):
        next_state_name = self.current_watched_movie_subdialog_manager.get_next_state()
        if next_state_name:
            function = self.get_function_from_name("s_", next_state_name)
            function()
            return True
        return False

    def execute_next_subtopic(self):
        self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")
        next_subtopic = self.get_next_subtopic()
        next_subtopic()

    def get_next_subtopic(self):
        if self.tracker.current_state == States.ASK_WATCHED_MOVIE_QUESTION:
            return self.s_ask_liked_movie_genre
        #todo
        return

    def try_use_system_ack(self):
        if self.dispatcher.ack:
            return False
        if self.tracker.system_ack_tag in \
                ["ack_opinion", "positive_opinion", "appreciation", "thanking", "opinion_crazy",
                 "opinion_surprising", "not_sure"]:
            self.dispatcher.ack = self.tracker.system_ack_text

            return True
        return False

    def init_context_for_current_movie(self, movie_info):
        movie_id = movie_info["movie_id"]

        self.tracker.module_context.current_watched_movie_subdialog.reset()
        self.tracker.current_movie_info = movie_info
        self.tracker.current_movie_id = movie_id
        if not movie_id in self.tracker.mentioned_movie_ids:
            self.tracker.mentioned_movie_ids.append(movie_id)


