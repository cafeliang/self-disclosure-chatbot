from typing import TYPE_CHECKING, List, Dict, Optional

from template_manager.utils import hash_utterance
from user_attribute_adaptor import UserAttributeAdaptor

if TYPE_CHECKING:
    from template_manager.template import Template


class TemplateUserAttributes(UserAttributeAdaptor):

    def __init__(self, user_attributes_ref):
        super().__init__('template_manager', user_attributes_ref, auto_add_missing_keys=True)

    @property
    def prev_hash(self) -> Dict[str, List[str]]:
        """
        Wrapper to the prev_hash key under template manager store
        """
        if 'prev_hash' not in self._storage:
            self['prev_hash'] = {}
        return self['prev_hash']

    def append_hash(self, template: 'Template', selector: str, utt_id: str) -> bool:
        """
        Used to append used utterance hash into the has store table
        """

        selector_id = hash_utterance(template, selector, None)

        if template.count_utterances(selector) == 1:
            self.prev_hash[selector_id] = []
            return True

        if selector_id not in self.prev_hash:
            self.prev_hash[selector_id] = []

        if utt_id in self.prev_hash[selector_id]:
            return False

        else:
            self.prev_hash[selector_id].append(utt_id)
            return True

    def has_hash(self,
                 utt_id: str,
                 template: Optional['Template'] = None,
                 selector: Optional[str] = None) -> bool:

        if template and selector:
            selector_id = hash_utterance(template, selector, None)
            return utt_id in self.prev_hash[selector_id]

        else:
            for utt_ids in self.prev_hash.values():
                return utt_id in utt_ids

            return False

    def append_selector_history(self,
                                template: 'Template',
                                selector: str):

        if "reprompt" in selector:  # Exclude reprompt as it occurs every turn
            return

        if 'selector_history' not in self._storage:
            self['selector_history'] = []
        selector = f"{template.name}::{selector}"
        self['selector_history'].append(selector)
