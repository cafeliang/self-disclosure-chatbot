from template_manager.template import Template  # noqa: F401
from template_manager.tua import TemplateUserAttributes  # noqa: F401

from template_manager.manager import TemplateManager  # noqa: F401
from template_manager.error import TemplateError  # noqa: F401
from template_manager.error import TemplateSelectorError  # noqa: F401
from template_manager.error import TemplateNoEmbeddedInfo  # noqa: F401

# Backward compatibility

from template_manager.migration import Templates, SelectorAPI  # noqa: F401