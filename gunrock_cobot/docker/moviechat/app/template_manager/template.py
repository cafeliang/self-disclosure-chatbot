import logging
import random
import warnings
from enum import Enum
from typing import List, Dict, Tuple, Union

from template_manager.tua import TemplateUserAttributes
from template_manager.formatter import TemplateFormatter

from template_manager.utils import hash_utterance, traverse
from response_templates import template_data  # noqa: F401

# MARK: - Logger setup
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter('[TM] %(message)s'))
logger.addHandler(ch)


TEMPLATES_MAP = {

    # Global / System templates
    'shared': "shared_template",
    'sys_rg': "intent_general",
    'error': "error_template",
    'topics': "intent_topics",

    'transition': "intent_transition",
    'dm': "dm_template",
    'greeting': "launchgreeting_template",

    # Module templates
    # Add your template yml file to the 'gunrock_cobot/response_templates/'
    # then add your module name - filename mapping here
    'social': 'social_template',
    'comfort': 'comfort_template',
    'jokes': 'jokes_template',
    'selfdisclosure': 'selfdisclosure_template',

    'techscience': 'techscience_template',
    'games': 'games_template',

    'news': 'news_template_2019',
    'weather': 'weather_template',
    'animal': 'animal_template',
    'movies': 'movies_template',
    'books': 'books_template',
    'sports': 'sports_template',
    'food': 'food_template',
    'fashion': 'fashion_template',
    'music': 'music_template',
    'outdoor': 'outdoor_template',
    'retrieval': 'retrieval_template',

    'acknowledgement': 'acknowledgement_template',
    'acknowledgement_emotional': 'acknowledgement_template_emotional',

    # self-disclosure study
    'self_disclosure_movie_factual': 'self_disclosure_movie_factual',
    'self_disclosure_movie_cognitive': 'self_disclosure_movie_cognitive',
    'self_disclosure_movie_emotional': 'self_disclosure_movie_emotional',

    'self_disclosure_covid_factual': 'self_disclosure_covid_factual',
    'self_disclosure_covid_cognitive': 'self_disclosure_covid_cognitive',
    'self_disclosure_covid_emotional': 'self_disclosure_covid_emotional'

}

TemplateItem = Union[str, dict]


# MARK: - Template Class

class _Template_v3(Enum):

    """
    Enum class representing a particular template in the system.
    Provide access and abstraction for accessing the templates.
    For modules use TemplateManager instead.
    """

    @property
    def _data(self) -> dict:
        try:
            return getattr(globals()['template_data'], 'data')[self.name]
        except Exception as e:
            logger.critical(e, exc_info=True)
            raise e

    # MARK: - API

    def speak(self,
              selector: str,
              slots: Dict[str, str],
              user_attributes: 'TemplateUserAttributes',
              embedded_info: dict,
              remove_duplicate='none',
              *,
              _append_selector_history=True) -> str:

        logger.debug("utterance with selector: {}, slots: {}".format(selector, slots))

        try:
            # access template with selector
            entries = self._get_utterance_list(selector)
            if not entries:
                logger.error("not utterance with selector: {}".format(selector))
                raise KeyError

            # choose an untterance from selector group
            entry, utt_id = self._select_utterance(entries, selector, user_attributes)

            utterance = self._str_dict_selector(entry, embedded_info)

            if utterance is not None and isinstance(utterance, str):
                # string interpolation
                formatter = TemplateFormatter(self)
                utterance = formatter.format_string(utterance, selector, slots, user_attributes)
            else:
                logger.warning(
                    f"Utterance in speak is not a string. Return value will be malformed. selector={selector}")
                utterance = entry

            # append to history when everything is successful
            user_attributes.append_hash(self, selector, utt_id)
            if _append_selector_history:
                user_attributes.append_selector_history(self, selector)

            return utterance

        except (KeyError, Exception) as e:
            logger.warning(f"{e}, selector = {selector}", exc_info=True)
            # TODO: Graceful error handling here plz
            if user_attributes.prev_hash.get('sys_env') == 'local':
                raise e  # assert error only during lcoal test
            else:
                # TODO:
                return " "

    def has_utterance(self, unformatted_utterance: str, selector: str) -> bool:
        warnings.warn("Unsupported, may return invalid results", DeprecationWarning)
        try:
            utterances = self._get_utterance_list(selector)
            return any(utt == unformatted_utterance for utt, _ in utterances)
        except Exception as e:
            logger.warning(e, exc_info=True)
            return False

    # def has_utterance(self, uid: str, selector: Optional[str] = None) -> bool:
    #     """
    #     This function checks if the template has a specified utterance.
    #     ### IMPORTANT ###
    #     Use sparingly. O(size of prev_hash.values)
    #     """
    #     warnings.warn("Unsupported, may return invalid results", DeprecationWarning)

    #     try:
    #         utterances = self._get_utterance_list(selector)
    #         return any(uid == hash_id_t for _, hash_id_t in utterances)
    #     except Exception as e:
    #         logger.warning(e, exc_info=True)
    #         return False

    def get_child_keys(self, selector: str) -> List[str]:
        # warnings.warn("Unsupported, may return invalid results", DeprecationWarning)
        try:
            data = traverse(self._data, selector)
            return list(data.keys())
        except (KeyError, AttributeError) as e:
            logger.warning(e, exc_info=True)
            return []

    def has_selector(self, selector: str) -> bool:
        try:
            return traverse(self._data, selector) is not None
        except (AttributeError, KeyError, TypeError):
            return False

    def count_utterances(self, selector: str) -> int:
        """
        Return the number of utterances under a selector
        """

        entries = self._get_utterance_list(selector)
        if not entries:
            logger.warning("Entries are empty")
            return 0

        return len(entries)

    def count_used_utterances(self, selector: str, user_attributes: 'TemplateUserAttributes') -> int:
        """
        Count the length of the stored prev_hash[selector_uid], thus returning how many of the templates are used
        """
        selector_uid = hash_utterance(self, selector, None)
        uids = user_attributes.prev_hash.get(selector_uid) or []
        return len(uids)

    # MARK: - Internal Methods

    def _get_utterance_list(self, selector: str) -> List[Tuple[TemplateItem, str]]:
        """
        Internal method for getting the template group under the specified selector
        """

        logger.debug("get utt list with selector: {}".format(selector))
        data = traverse(self._data, selector)
        if isinstance(data, list):
            return [(d['entry'], d['uid']) for d in data]
        else:
            logger.warning("_get_utterance_list is selecting unsupported type: {}, ({})".format(
                type(data), selector), exc_info=True)
            return None

    def _select_utterance(self,
                          utterances: List[Tuple[TemplateItem, str]],
                          selector: str,
                          user_attributes: 'TemplateUserAttributes') -> Tuple[TemplateItem, str]:
        """
        Internal method for choosing a template under a template group.
        """
        # grab a copy of the selector as hash
        selector_uid = hash_utterance(self, selector, None)

        if len(utterances) == 1:  # if there is only one utterance
            return utterances[0]

        if selector_uid not in user_attributes.prev_hash:  # if template has not been used
            return random.choice(utterances)

        filtered = [
            (entry, uid)
            for entry, uid in utterances
            if uid not in user_attributes.prev_hash[selector_uid]
        ]

        if len(filtered) == 0:  # all templates used
            user_attributes.prev_hash[selector_uid] = [
                user_attributes.prev_hash[selector_uid][-1]
            ]  # keep last hash, so not to choose template just used

            filtered = [
                (utt, hash_id)
                for utt, hash_id in utterances
                if hash_id not in user_attributes.prev_hash[selector_uid]
            ]

        return random.choice(filtered)

    def _str_dict_selector(self, entry: Union[str, dict], embedded_info: dict = None) -> str:
        if isinstance(entry, str):
            utterance = entry
        elif isinstance(entry, dict):
            if 'utt' in entry:
                utterance = entry['utt']
                if embedded_info is not None:
                    args = entry['args']
                    embedded_info.update(args)
            else:
                logger.warning("selected utterance is dict and without 'utt' key, skipping formatting")
                utterance = None
        else:
            raise ValueError("Entry type invalid")
        return utterance


# Export Template class with imported enum values
Template = _Template_v3('Template', TEMPLATES_MAP)
