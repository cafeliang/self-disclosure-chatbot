import logging
import re
import traceback
from transitions import Machine
from movietv_utils import *
from movie_module_regex import *
from movie_preference_extractor import ActorExtractor, DirectorExtractor, GenreExtractor


logging.basicConfig(level=logging.DEBUG)
logging.getLogger('transitions').setLevel(logging.DEBUG)

SIZE_TRANSITION_HISTORY = 5

def set_chitchat_dialog_init_context(context):
    context.current_subdialog_context = {
        "current_state": "s_init",
        "old_state": None, 
        "q_key": None,
        "transition_history": ["r_t0"] * SIZE_TRANSITION_HISTORY,
        "chitchat_context": [], # persistent context info
        "tags": [], # temporary context info
        "interrupted": False
    }

def save_chitchat_dialog_current_context(context):
    context.chitchat = {
        "current_subdialog_context": context.current_subdialog_context,
    }

class ChitchatDialog(Machine):
    # High level specification of states, state transitions and transition
    # conditions
    def __init__(self, automaton):
        # Each state has prefix 's_'
        self.states = [
            's_init',  # main entry point
            's_chitchat',
            's_exit',
        ]
        # Each state has a corresponding transtion function that chooses the next state depending on certain conditions
        # There are different ways to define transitions. I'm defining arguments using a list.
        # Args ordering: transition_name, source, dest, conditions=None,
        # unless=None, before=None, after=None, prepare=None
        self.transitions = [
            # system
            ['transduce', '*', 's_exit',
                ['c_no_questions_left', 'r_t1']],
            ['transduce', 's_init', 's_chitchat', [
                'r_t2']],  # first time entry point
            ['transduce', 's_chitchat', '=', ['r_t3']],
        ]

        self.automaton = automaton
        self.text = automaton.text
        self.utt = automaton.utt
        self.context = automaton.context
        self.tags = []
        # resume
        if "current_subdialog_context" in self.context.chitchat and self.context.current_subdialog_context["current_state"] == "s_init":
            self.tags.append("c_resuming_dialog")
            self.context.current_subdialog_context = self.context.chitchat["current_subdialog_context"]
        self.current_context = self.context.current_subdialog_context

        if "chitchat_context" not in self.current_context:
            print("[MOVIECHAT_MODULE] Exception with chitchat_context - current_context {}".format(self.current_context))
            self.current_context["chitchat_context"] = []

        if "c_no_questions_left" in self.current_context["chitchat_context"]:
            self.tags.append("c_no_questions_left")

        Machine.__init__(
            self,
            states=self.states,
            transitions=self.transitions,
            initial=self.current_context["current_state"],
            prepare_event='prepare',
            finalize_event='finalize',
            send_event=True)

    def get_response(self, input_data):
        print("current_context:", self.current_context)
        self.output = {
            "response": self.utt(["got_distracted"]),
            "next_t_transition": "t_chitchat",
            "exit_code": None,
        }
        self.text = input_data["text"][0]
        self.last_response = input_data['last_response'][0]
        self.dialog_act = input_data["features"][0]["dialog_act"]
        self.amz_dialog_act = input_data["features"][0].get("amz_dialog_act")
        self.ner = input_data["features"][0]["ner"]
        self.asr_correction = input_data["features"][0]["asrcorrection"]
        self.knowledge = input_data["features"][0]["knowledge"]
        self.topic_keywords = input_data["features"][0]["topic_keywords"]
        self.sentiment = input_data["features"][0]["sentiment"]
        self.cobot_intents = input_data["features"][0]["intent_classify"]["lexical"] + \
            input_data["features"][0]["intent_classify"]["topic"]
        self.features = input_data["features"][0]
        self.input_data = input_data
        self.old_state = self.state
        self.cache = {}  # for caching conditions results if necessary
        self.r_ack = ""
        self.r_body = ""
        self.r_question = ""
        self.r_filler_1 = ""
        self.r_filler_2 = ""
        self.r_filler_3 = ""
        self.transition_history = self.current_context["transition_history"]

        self.transduce()

        self.new_state = self.state
        self.current_context["current_state"] = self.state
        self.current_context["old_state"] = self.old_state
        self.current_context["transition_history"] = self.transition_history
        self.current_context["tags"] = self.tags
        save_chitchat_dialog_current_context(self.context)
        self.final_response = self.r_filler_1 + self.r_ack + self.r_filler_2 + self.r_body + self.r_filler_3 + self.r_question
        print("current_context:", self.current_context)
        self.output["response"] = self.final_response
        return self.output

    # on_enter states: Most of the logic for each state to generate the response is defined here
    # Every state has a corresponding callback here
    # =======================================================
    def on_enter_s_chitchat(self, event):
        # print("We've just entered state s_chitchat")

        self.set_ack()
        if self.output["exit_code"] != "s_exit":
            self.set_body()
            self.set_next_question()

    def on_enter_s_exit(self, event):
        # print("We've just entered state s_exit")
        # default response; should be overwritten upon returning
        self.r_question = self.utt(["got_distracted"])

    def set_chitchat_context(self):
        pass

    def set_ack(self):
        # not init for first time or resuming ; except for first time, this should always run as it sets the next q_key
        if self.current_context["q_key"] is not None and "c_resuming_dialog" not in self.tags and not self.current_context["interrupted"]:
            ack_bot = ChitchatAcknowledgeBot(self)
            self.r_ack = ack_bot.get_ack(self.current_context["q_key"])


    def set_body(self):
        pass

    def set_next_question(self):
        if self.current_context["q_key"] is None:
            self.current_context["q_key"] = "q1"
        if self.current_context["interrupted"] == True:
            self.current_context["interrupted"] = False
            self.current_context["q_key"] = "q{}".format(int(self.current_context["q_key"][1])+1)

        q_key = self.current_context.get("q_key", "q1")
        if q_key == "q7":
            self.current_context["chitchat_context"].append("c_no_questions_left")
            self.output["exit_code"] = "s_exit"
        else:
            self.tags.append(q_key + "_question")

            if "qa_template_params" in self.current_context:
                self.r_question = self.utt(["chitchat_dialog", "chitchat_question_bank", q_key, "q"], self.current_context["qa_template_params"])
                self.current_context["qa_template_params"] = None
            else:
                self.r_question = self.utt(["chitchat_dialog", "chitchat_question_bank", q_key, "q"])


    # System level callbacks
    # =======================================================
    def prepare(self, event):
        return True

    def finalize(self, event):
        return True

    # Conditions
    # These callback functions cache their result before they may be checked multiple times
    # Depending on the function, they may have to cache multiple values, e.g. c_entity_detected
    # If conditions callbacks interact with user_attributes, remember to reset fields where necessary
    # ==========================================================
    def c_resuming_dialog(self, event):
        if "c_resuming_dialog" in self.tags:
            return True

        return False

    def c_no_questions_left(self, event):
        if "c_no_questions_left" in self.tags:
            return True

        return False

    def r_t1(self, event):
        return self.update_transition_history("r_t1")

    def r_t2(self, event):
        return self.update_transition_history("r_t2")

    def r_t3(self, event):
        return self.update_transition_history("r_t3")

    def update_transition_history(self, transition_tag):
        self.transition_history.append(transition_tag)
        self.transition_history.pop(0)
        return True

class ChitchatAcknowledgeBot:
    def __init__(self, fsm):
        self.fsm = fsm
        self.utt = fsm.utt
        self.text = fsm.text
        self.cobot_intents = fsm.cobot_intents
        self.ner = fsm.ner
        self.knowledge = fsm.knowledge
        self.sentiment = fsm.sentiment
        self.tags = fsm.tags
        self.output = fsm.output
        self.context = self.fsm.context
        self.current_context = self.context.current_subdialog_context
        self.questions_mapping = {
            "q1": self.get_q1_ack,
            "q2": self.get_q2_ack,
            "q3": self.get_q3_ack,
            "q4": self.get_q4_ack,
            "q4_1": self.get_q4_1_ack,
            "q5": self.get_q5_ack,
            "q6": self.get_q6_ack,
        }

    def get_ack(self, q_key):
        return self.questions_mapping[q_key]()

    def get_q1_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_default"])

        # custom asr correction
        if re.search(likes_leo_too, self.text):
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_same_actor"])

        actor_names = ActorExtractor().extract_actor_names(self.ner, self.knowledge, self.fsm.text, self.fsm.last_response)

        if actor_names:
            our_entity = "leonardo dicaprio"
            our_entity_2 = "leonardo di caprio"
            if len(actor_names) == 1 and not ("ans_neg" in self.cobot_intents or self.sentiment == "neg"):
                entity = actor_names[0]
                # favorite matches ours
                if fuzz.token_set_ratio(entity.lower(), our_entity) > 90:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_same_actor"])
                elif fuzz.token_set_ratio(entity.lower(), our_entity_2) > 90:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_same_actor"]) 
                else:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_diff_actor"], {"actor": entity})
            elif len(actor_names) > 1:
                for entity in actor_names:
                    if fuzz.token_set_ratio(entity.lower(), our_entity) > 90:
                        continue
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_diff_actor"], {"actor": entity})
                    break
        elif "ans_unknown" in self.cobot_intents:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])

        self.current_context["q_key"] = "q2"

        return ack

    """
    I return q5 instead of q3 to skip some of the chitchat questions.
    """
    def get_q2_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_default"])

        director_names = DirectorExtractor().extract_director_names(self.ner, self.knowledge)

        if director_names:
            our_entity = "christopher nolan"
            if len(director_names) == 1 and not ("ans_neg" in self.cobot_intents or self.sentiment == "neg"):
                entity = director_names[0]
                # favorite matches ours
                if fuzz.token_set_ratio(entity.lower(), our_entity) > 90:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_same_director"])
                else:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_diff_director"], {"director": entity})
            elif len(director_names) > 1:
                for entity in director_names:
                    if fuzz.token_set_ratio(entity.lower(), our_entity) > 90:
                        continue
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_diff_director"], {"director": entity})
                    break
        elif "ans_unknown" in self.cobot_intents:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])

        self.current_context["q_key"] = "q5"
        self.output["exit_code"] = "s_exit"

        return ack

    def get_q3_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q3", "ack_default"])

        actor_names = ActorExtractor().extract_actor_names(self.ner, self.knowledge, self.fsm.text, self.fsm.last_response)

        if actor_names:
            our_entity = "heath ledger"
            if len(actor_names) == 1 and not ("ans_neg" in self.cobot_intents or self.sentiment == "neg"):
                entity = actor_names[0]
                # favorite matches ours
                if fuzz.token_set_ratio(entity.lower(), our_entity) > 90:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q3", "ack_same_actor"])
                else:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q3", "ack_diff_actor"], {"actor": entity})
            elif len(actor_names) > 1:
                for entity in actor_names:
                    if fuzz.token_set_ratio(entity.lower(), our_entity) > 90:
                        continue
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q3", "ack_diff_actor"], {"actor": entity})
                    break
        elif "ans_unknown" in self.cobot_intents:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])

        self.current_context["q_key"] = "q4"

        return ack

    def get_q4_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q4", "ack_incorrect"])

        entity_names = GenreExtractor().extract_genre(self.text)

        # respond with last entity
        if entity_names:
            our_entity = "science fiction"
            for entity in entity_names:
                if entity == our_entity:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q4", "ack_correct"])

        if re.search(r"my favorite|i (like|enjoy|watch)", self.text):
            self.current_context["q_key"] = "q5"
            self.output["exit_code"] = "s_exit"
        else:
            self.current_context["q_key"] = "q4_1"

        return ack

    def get_q4_1_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q4_1", "ack_default"])

        entity_names = GenreExtractor().extract_genre(self.text)

        # respond with last entity
        if entity_names:
            our_entity = "science fiction"
            for entity in entity_names:
                if entity != our_entity:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q4_1", "ack_diff_genre"], {"genre": entity})
                    break
                if entity == our_entity:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q4_1", "ack_same_genre"])
        elif "ans_unknown" in self.cobot_intents:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])
                    
        self.current_context["q_key"] = "q5"
        self.output["exit_code"] = "s_exit"

        return ack

    def get_q5_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q5", "ack_default"])

        if re.search("theater|cinema", self.text) and not re.search("home|house", self.text):
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q5", "ack_theater"])
        elif re.search("home|house", self.text) and not re.search("theater|cinema", self.text):
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q5", "ack_home"])
        elif "ans_unknown" in self.cobot_intents:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])
                    
        self.current_context["q_key"] = "q6"

        return ack

    def get_q6_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q6", "ack_default"])

        if "ans_pos" in self.cobot_intents:
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q6", "ack_yes"])
        elif "ans_neg" in self.cobot_intents:
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q6", "ack_no"])
        elif "ans_unknown" in self.cobot_intents:
            ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])

        self.output["exit_code"] = "s_exit"
        self.current_context["chitchat_context"].append("c_no_questions_left")

        return ack