import logging
import re
from typing import Optional, List
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment

from nlu.constants import Positivity
from enum import Enum
from nlu.intentmap_scheme import MIN_2_WORDS


class ActorOpinion(Enum):
    POSITIVE = "positive"
    NEGATIVE = "negative"
    MIXED = "mixed"
    OTHER = "other"


class ActorOpinionDetector:

    def __init__(self, complete_user_utterance: str, returnnlp: Optional[List[dict]]):
        self.text = complete_user_utterance
        self.returnnlp = ReturnNLP.from_list(returnnlp)

    def detect_opinion(self) -> ActorOpinion:
        if self.is_positive() and self.is_negative():
            return ActorOpinion.MIXED
        elif self.is_positive():
            return ActorOpinion.POSITIVE
        elif self.is_negative():
            return ActorOpinion.NEGATIVE
        else:
            return ActorOpinion.OTHER

    def is_positive(self):
        regex_positive = r"(^(?!.*(not|never|doesn't|don't)).*(excellent|nice|friend|admir(e|able)|good|great|lov(e|ing)|beautiful|wonderful|outstanding|exceptional|awesome|amazing|gets into character|incredible|marvelous|enjoy|like))|(^(?=.*(not|never|doesn'|don't)).*(horrible|bad|awful|dreadful|terrible|hate|\bmean\b|hostile|rude|arrogant))"
        return self.returnnlp.has_intent("opinion_positive") or re.search(regex_positive, self.text)

    def is_negative(self):
        regex_negative = r"(^(?=.*(not|never|doesn't|don't)).*(excellent|nice|friend|admir(e|able)|good|great|lov(e|ing)|beautiful|wonderful|outstanding|exceptional|awesome|amazing|gets into character|incredible|marvelous|enjoy|like))|(^(?!.*(not|never|doesn'|don't)).*(horrible|bad|awful|dreadful|terrible|hate|\bmean\b|hostile|rude|arrogant))"
        return self.returnnlp.has_intent("opinion_negative") or re.search(regex_negative, self.text)

