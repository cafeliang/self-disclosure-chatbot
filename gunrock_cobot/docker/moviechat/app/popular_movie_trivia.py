popular_movie_detail = {
    "299534": {
        "trivia": [
            "Robert Downey Jr., who plays Iron Man, was the only actor who was allowed to read the entire script during filming. Other actors were left in the dark about the plot, and some filmed multiple endings and had fake script pages to prevent accidental spoilers. This movie was really a top-secret project! ",
            "The phrase 'I love you 3000' was actually originally said to Robert Downey Jr. by one of his own kids. He told the Avengers Endgame directors, and they liked it so much that they included it in the film. I wonder if they gave the kids any writing credits. ",
            "The character Korg is voiced by Taika Waititi, who also directed Thor Ragnarok. In Avengers Endgame, Korg wears a Hawaiian shirt that resembles the shirt Waititi actually wore in interviews promoting Thor Ragnarok. I wish I could wear a Hawaiian shirt too. ",
            "The autographs for the original Avengers actors are shown during the ending credits of Avengers Endgame. This idea was inspired by the finale Star Trek movie, Star Trek 6 The Undiscovered country, which did the same for its original cast. It might be because I'm a bot, but I do love me some Star Trek. ",
            "When Avengers Endgame surpassed Titanic's record box office total, James Cameron sent a congratulatory message to Marvel Studios. He included a photo of the Titanic being sunk by the Avengers 'A' logo instead of an iceberg. "
        ],
        "opinion": [
            "fav_scene: My favorite scene is definitely the last battle scene. I don't want to spoil it too much, but it brings so many characters together in such a satisfying way, and the action is just top-notch. ",
            "fav_character: My favorite Avenger is probably either Bruce Banner or Tony Stark. I appreciate heroes who can fight for justice, and also invent cool things through science! "
        ],
        "plotsummary": "The movie focuses on a group of superheroes, called The Avengers, as they cope with their biggest loss yet. After Avengers Infinity War, Thanos has wreaked havoc across the entire universe. The Avengers must assemble once more to stop Thanos once and for all, and try to bring back everyone they've lost. It's full of action and emotion. "

    },
    "466272": {
        "trivia": [
            "During some scenes Margot Robbie actually wears some jewelry that was previously owned by Sharon Tate. The pieces were loaned to her by Tate's sister Debra. ",
            "The Maltese Falcon statue that Sharon Tate sees in the bookstore was not only the real Maltese Falcon, but it's also actually owned by Leornardo DiCaprio. He paid around 305,000 US dollars for it during an auction in 2010.",
            "The actor who plays Charles Manson, Damon Herriman, also plays Charles Manson in the Netflix series Mindhunter. Talk about consistency!",
            "Quentin Tarantino actually spent five years trying to write Once Upon a Time in Hollywood as a book before realizing it was better suited as a movie. ",
            "While Rick Dalton having a giant poster of his own face on his driveway may seem strange, Tarantino actually got the idea when he visited actor Lee Van Cleef's home and saw that there was a giant poster of Van Cleef's face in the garage. He thought it was both funny and strange. "
        ],
        "opinion": {
            "fav_scene": "I liked all the scenes between Rick and Cliff. They're just so loyal to each other! I hope my friends will stick with me like that through all my problems. ",
            "fav_character": "Cliff Booth is just outrageously funny. I also really like Leonardo DiCaprio as Rick, but Brad Pitt really knocked it out of the park. "
        },
        "plotsummary": "The movie is a comedy-drama written and directed by Quentin Tarantino. It focuses on Rick Dalton and Cliff Booth, two previously successful Hollywood stars who are now struggling to deal with the changing film industry of 1969. In contrast, Sharon Tate and Roman Polanski are two rising stars on the scene. However, dramatic events occur that alter all of Hollywood. It's very dramatic and funny! "
    },
    "546554": {
        "trivia": [
            "K Callan plays Great-nana Wanetta, who is the mom of Chrisopher Plummer's character Harlan. Funnily enough, Callan is actually six years younger than Plummer in real life. I wonder if Plummer was flattered by the casting. ",
            "The movie title, Knives Out, and the movie's in-production working title, Morning Bell, are both tracks from the album Amnesiac by Radiohead. The creators must be fans! ",
            "The character Harlan Thrombey dies on his 85th birthday. This was done in reference to famous mystery-writer Agatha Christie, who died at 85 years old.",
            "Writer and director Rian Johnson named the main characters after musical artists from the 1970's. Walt and Donna are named after Walt and Donald from Steely Dan. Joni is Joni Mitchell and her dead husband was named after Neil Young. Lastly, Linda and Richard are named for Linda and Richard Thompson. ",
            "Director and writer Rian Johnson collaborated with some of his cousins to create this movie. His cousin Mark Johnson did the title credit sequence and his cousin Zack Johnson painted the cast paintings that are in the end credits. Must be a talented family! "
        ],
        "opinion": {
            "fav_scene": "Without mentioning any spoilers, Detective Benoit Blanc has an amazing rant about donuts. That's definitely my favorite part of the movie. ",
            "fav_character": "Detective Benoit Blanc is my favorite character. He's really funny, observant, patient, and who wouldn't like Daniel Craig with a Southern accent. "
        },
        "plotsummary": "The movie is a mystery film that focuses on solving the murder of renowned crime novelist Harlan Thrombey. Inquisitive and clever Detective Benoir Blanc must sift through an eccentric and selfish family, and their web of lies, to uncover the truth behind Harlan's untimely death. The movie is filled with surprising twists and turns. "
    },
    "496243": {
        "trivia": [
            "The Parasite director Bong Joon Ho mentioned in an interview that filming rich houses gave him anxiety because of how expensive the furniture was. One prop trash can alone was worth around 2500 US dollars. I'd be nervous too! ",
            "The Parks' house in the movie was not actually designed by architect Namgoong Hyeonja, like they say in the movie. In fact, the house was actually a set built entirely from scratch for filming. ",
            "The end credits song called A Glass of Soju, was actually written by the Parasite director, and sung by lead actor Woo-sik Choi, who plays Ki Woo in the movie. The song was originally titled 564 years, which is how many years Ki-Woo would have had to work to earn enough to buy the house in the movie. I wish I could live that long! ",
            "The Kim family's house, alleyway, and entire neighborhood, were all built on a massive set that doubled as a water tank, so that the whole thing could be flooded for the film. My engineers and I appreciate that level of planning and design! ",
            "Director Bong Joon Ho, originally started Parasite as a play, but the first line got him thinking about different camera positions and angles. He realized that he had to do the story as a film to do it justice. "
        ],
        "opinion": {
            "fav_scene": "My favorite part is the montage leading up to the Kim's convincing the Park's mother that her housekeeper has tuberculosis. It's such an intricate set of events, and the editing makes it so satisfying! ",
            "fav_character": "I think the Kim's eldest son, Woo-sik Choi, is my favorite just because his character is so funny! Also, his actor starred in Train to Busan, which I also enjoyed. "
        },
        "plotsummary": "The movie is a dark comedy thriller about the poor Kim family and the rich Park family. The Kim family trick the Park family into hiring them for a generous salary. However, the Park household harbors dark secrets. You'll see how the Kim family struggles to keep up the scam as their greed grows, and they become more entangled with the Park family. So intriguing!"
    },
    "475557": {
        "trivia": [
            "Joaquin Phoenix based the Joker's laugh on videos of people who experience pathological laughter, which is an emotional expression disorder that causes people to randomly uncontrollably laugh. He really studies hard for his roles. ",
            "In the movie, Arthur Fleck writes right-handed for his general journal entries. But he switches to his left-hand temporarily when he has mental breaks or stress. I wonder if I would be right-handed or left-handed you know? If I had hands. ",
            "Joaquin Phoenix lost a lot of weight for his role as the Joker. It was so serious that filming could only be done once, with no opportunity for reshoots. Talk about a high-pressure situation! ",
            "Originally, Warner Brothers wanted Martin Scorsese to make this film with Leonardo DiCaprio as Arthur. However, DiCaprio had already signed on to do Once Upon a Time in Hollywood for Quentin Tarantino. Scorsese had also committed himself to another project, The Irishman. I wonder how different the movie would have been if the cast was different. ",
            "Although he is not referred to by name anywhere in the films dialogue, English subtitles reevealed the Englishman who confronts Arthur at the gates of Wayne Manor is Alfred Pennyworth, the butler of Bruce Wayne."
        ],
        "opinion": {
            "fav_scene": "It might be a predictable answer, but I liked Arthur's stairway dance. From the sunlight to the slow-motion, it was just filmed in such a cool way. ",
            "fav_character": "I don't support Arthur Fleck's actions, but his backstory and character growth is really interesting to witness. "
        },
        "plotsummary": "The movie is a psychological thriller about a mentally-troubled comedian named Arthur Fleck. The citizens of Gotham City disregard and criticize Fleck, which leads him to commit violent crimes. These criminal activities lead to the creation of his villainous alter ego, The Joker. It's a dark movie that's very thrilling. "
    }
}
