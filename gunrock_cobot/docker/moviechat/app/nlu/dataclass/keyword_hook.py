import logging
from pathlib import Path
from pprint import pformat
import pandas as pd


logger = logging.getLogger(__name__)


def read_nlu_metadata(metadata_path: Path):
    return {
        f.stem: {
            'keyword_detector': pd.read_csv(f).to_dict('records')
        }
        for f in metadata_path.glob('*.csv')
    }


def write_nlu_metadata(metadata_path: Path, data: dict):
    with open(metadata_path / 'keywords.py', 'w') as w:
        w.write("# <--- dict object for [nlu_dataclass] ---> #\n")
        w.write("# <--- Do not directly edit this file. Edit the csv file and run the "
                "_runme.py script instead ---> #\n\n")

        w.write("data = \\\n")
        w.write(pformat(data, width=120))
        w.write('\n')


def main(cobot_home: Path):
    metadata_path = cobot_home / 'nlu/nlu_metadata'

    logger.info("[BEGIN] nlu_metadata transform")
    data = read_nlu_metadata(metadata_path)
    logger.info("[SUCCESS] nlu_metadata transform")

    logger.info("[BEGIN] writing nlu_metadata")
    write_nlu_metadata(metadata_path, data)
    logger.info("[SUCCESS] written nlu_metadata as keywords.py")
