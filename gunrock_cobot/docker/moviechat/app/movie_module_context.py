from enum import Enum
from typing import List, Dict, Any
from dataclasses import dataclass, asdict, field
import logging

from actor_subdialog import ActorSubdialogContext
from movie_user_profile import MovieUserProfile

class WatchedMoviePreferenceLevel(Enum):
    VERY_LIKE = "very_like"
    LIKE = "like"
    NEUTRAL = "neutral"
    DISLIKE = "dislike"
    UNKNOWN = "unknown"


class WatchedMovieDiscussionMode:
    ASK_OPINION = "ask_opinion"
    PROVIDE_TRIVIA = "provide_trivia"
    PROVIDE_OPINION = "provide_opinion"


@dataclass
class CurrentWatchedMovieSubdialogContext:
    movie_preference_level: str = WatchedMoviePreferenceLevel.UNKNOWN.value
    mode: str = WatchedMovieDiscussionMode.PROVIDE_TRIVIA
    # is_liked_movie_type: bool = False  # if it's the type of movie user usually like
    potential_liked_movie_genres: List[int] = field(default_factory=list)
    trivia_total_count: int = 0
    trivial_used_count: int = 0
    question_used_count: int = 0
    question_asked: List[str] = field(default_factory=list)
    short_answer_count: int = 0
    opinion_comment_answer_count: int = 0

    @classmethod
    def from_dict(cls, ref: Dict[str, Any]):
        result = cls(movie_preference_level=ref.get("movie_preference_level", WatchedMoviePreferenceLevel.UNKNOWN.value),
                     mode=ref.get("mode", WatchedMovieDiscussionMode.PROVIDE_TRIVIA),
                     trivia_total_count=ref.get("trivia_total_count", 0),
                     trivial_used_count=ref.get("trivial_used_count", 0),
                     question_used_count=ref.get("question_used_count", 0),
                     question_asked=ref.get("question_asked", []),
                     potential_liked_movie_genres=ref.get("potential_liked_movie_genres", [])
        )
        return result

    def reset(self):
        self.movie_preference_level = WatchedMoviePreferenceLevel.UNKNOWN.value
        self.mode = WatchedMovieDiscussionMode.PROVIDE_TRIVIA
        self.potential_liked_movie_genres = []
        self.trivia_total_count: int = 0
        self.trivial_used_count: int = 0
        self.question_used_count: int = 0
        self.question_asked: List[str] = []
        self.short_answer_count: int = 0
        self.opinion_comment_answer_count: int = 0

@dataclass
class MovieModuleContext:
    propose_continue: str
    current_state: str
    asking_liked_movie: bool
    last_movie_proposed_by_bot: bool

    detected_movie_infos: List[dict]  # for grounding
    movie_name_grounding_count: int

    mentioned_movie_ids: List[int]  # mentioned both by user and bot
    proposed_movie_ids: List[int]  # proposed by bot
    current_movie_id: int
    current_movie_info: dict

    current_actor_name: str
    general_chitchat_history: List[str]
    previous_asked_questions: List[str]
    movie_user_profile: MovieUserProfile
    current_watched_movie_subdialog: CurrentWatchedMovieSubdialogContext
    actor_subdialog: ActorSubdialogContext

    @classmethod
    def from_dict(cls, ref: Dict[str, Any]):

        movie_user_profile_raw = ref.pop("movie_user_profile", None)
        logging.info(f"[movie_user_profile_raw] {movie_user_profile_raw}")
        movie_user_profile = MovieUserProfile.from_dict(movie_user_profile_raw) if movie_user_profile_raw else MovieUserProfile()
        logging.info(f"[movie_user_profile] {movie_user_profile}")

        current_watched_movie_subdialog_raw = ref.pop("current_watched_movie_subdialog", None)
        if current_watched_movie_subdialog_raw:
            current_watched_movie_subdialog = CurrentWatchedMovieSubdialogContext.from_dict(
                current_watched_movie_subdialog_raw)
        else:
            current_watched_movie_subdialog = CurrentWatchedMovieSubdialogContext()

        actor_subdialog_raw = ref.pop("actor_subdialog", None)
        if actor_subdialog_raw:
            actor_subdialog = ActorSubdialogContext.from_dict(actor_subdialog_raw)
        else:
            actor_subdialog = ActorSubdialogContext()

        result = cls(propose_continue=ref.get("propose_continue", "CONTINUE"),
                     current_state=ref.get("current_state",  States.INIT),
                     last_movie_proposed_by_bot=ref.get("last_movie_proposed_by_bot", False),
                     asking_liked_movie=ref.get("asking_liked_movie", False),
                     detected_movie_infos=ref.get("detected_movie_infos", []),
                     movie_name_grounding_count=ref.get("movie_name_grounding_count", 0),
                     mentioned_movie_ids=ref.get("mentioned_movie_ids", []),
                     proposed_movie_ids=ref.get("proposed_movie_ids", []),
                     current_movie_id=ref.get("current_movie_id", None),
                     current_movie_info=ref.get("current_movie_info", {}),
                     current_actor_name=ref.get("current_actor_name"),
                     general_chitchat_history=ref.get("general_chitchat_history", []),
                     previous_asked_questions=ref.get("previous_asked_questions", []),
                     movie_user_profile=movie_user_profile,
                     current_watched_movie_subdialog=current_watched_movie_subdialog,
                     actor_subdialog=actor_subdialog)

        return result



class States:
    INIT = "init"

    # ask a movie to discuss
    ASK_RECENT_SEEN_MOVIE = "ask_recent_seen_movie"
    ASK_MOVIE_TO_DISCUSS = "ask_movie_to_discuss"
    ASK_MOVIE_AGAIN = "ask_movie_again"

    ASK_RECENT_SEEN_MOVIE_FOLLOW_UP = "ask_recent_seen_movie_follow_up"
    ASK_MOVIE_TO_DISCUCSS_FOLLOW_UP = "ask_movie_to_discuss_follow_up"
    ASK_LIKED_MOVIE_IN_GENRE = "ask_liked_movie_in_genre"
    MOVIE_NAME_GROUNDING = "movie_name_grounding"

    # watched movie subdialog
    ASK_LIKE_SPECIFIC_MOVIE = "ask_like_watched_movie"
    PROVIDE_WATCHED_MOVIE_TRIVIA = "provide_watched_movie_trivia"
    ASK_WATCHED_MOVIE_QUESTION = "ask_watched_movie_question"

    # propose movie
    PROPOSE_MOVIE = "propose_movie"
    ASK_IF_INTERESTED_IN_PROPOSED_MOVIE = "ask_if_interested_in_proposed_movie"
    PROVIDE_MOVIE_PLOT_SUMMARY = "provide_movie_plot_summary"

    # general chitchat
    ASK_FAVORITE_MOVIE_GENRE = "ask_liked_movie_genre"
    ASK_FAVORITE_ACTOR = "ask_liked_actor"
    ASK_WATCH_REVIEW_OR_TRAILER = "ask_watch_review_or_trailer"
    CONFIRM_LIKED_MOVIE_GENRE = "confirm_liked_movie_genre"

    # actor subdialog
    PROVIDE_ACTOR_TRIVIA = "provide_actor_trivia"
    ASK_ACTOR_QUESTION = "ask_actor_question"

    # switch from tv
    TRY_SWITCH_FROM_TV_TO_MOVIE = "try_switch_from_tv_to_movie"

