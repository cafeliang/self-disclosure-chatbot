import re

from movie_module_regex import ask_recommend

modules_mapping = {
    "ANIMALCHAT": "animals", "BOOKCHAT": "books", "GAMECHAT": "games", "FOODCHAT": "food", "MUSICCHAT": "music",
    "SPORT": "sports", "TECHSCIENCECHAT": "science and technology", "TRAVELCHAT": "traveling",
}


class TopicSwitchHandler:
    def __init__(self, automaton):
        self.automaton = automaton
        self.logger = automaton.logger
        self.text = automaton.coref_text
        self.dialog_act = automaton.dialog_act
        self.amz_dialog_act = automaton.amz_dialog_act
        self.ner = automaton.ner
        self.asr_correction = automaton.asr_correction
        self.noun_phrase = automaton.noun_phrase
        self.knowledge = automaton.knowledge
        self.topic_keywords = automaton.topic_keywords
        self.sentiment = automaton.sentiment
        self.cobot_intents = automaton.cobot_intents
        self.central_elem = automaton.central_elem
        self.features = automaton.features

    def handle_potential_topic_switch(self, resume_state):
        self.resume_state = resume_state

        detected_topic_module = self.detect_topic_module()

        if detected_topic_module and detected_topic_module != "MOVIECHAT":
            self.automaton.moviechat_user["tags"].append("movie_user_command")

            candidate_module = self.automaton.user_attributes.get("candidate_module")

            if candidate_module is not None and candidate_module == detected_topic_module:
                self.automaton.t_context[
                    "propose_topic"] = "Hmm. Do you want to talk about {} instead of movies? ".format(
                    modules_mapping[detected_topic_module])
                self.automaton.t_context["propose_continue"] = "UNCLEAR"
                self.automaton.moviechat_user["propose_topic"] = detected_topic_module
                return "s_ask_exit"

            self.automaton.moviechat_user["candidate_module"] = detected_topic_module
            return None

        return None

    def detect_topic_module(self):
        if self.automaton.current_t_transition != "t_ask_movietv":
            movie_module = "MOVIECHAT"
            candidate_module = None
            for module in self.central_elem["module"]:
                if movie_module == module:
                    return "MOVIECHAT"
                if module in modules_mapping:
                    candidate_module = module

            if candidate_module is not None:
                self.logger.debug(": propose_module_switch - {}".format(candidate_module))
                return candidate_module
