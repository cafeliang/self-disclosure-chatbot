import re
from cobot_core.state_manager import StateManager  # noqa
from cobot_core.log.logger import LoggerFactory  # noqa
from nlu.constants import TopicModule


class ResponseProsodyProcessor:
    def __init__(self, response, a_b_test_condition: str = "", selected_module: str = ""):
        self.response = response
        self.a_b_test_condition = a_b_test_condition.lower()
        self.selected_module = selected_module
        self.logger = LoggerFactory.setup(self)

    def process(self):
        self.logger.info("[ResponseProsodyProcessor] start processing response: {}".format(self.response))
        if not self.response:
            return self.response

        self.replace_unallowed_character()
        self.avoid_dot_being_pronounced()
        # self.add_pauses()

        # self.remove_pitch()

        # self.remove_domain()
        # self.remove_emphasis()
        # self.remove_emotion()
        # self.adjust_punctuation_around_interjection()
        # self.wrap_emotion_with_ab_test()

        self.logger.info("[ResponseProsodyProcessor] response after process: {}".format(self.response))
        # self.slow_down_long_response()

        return self.response

    def slow_down_long_response(self):
        if len(self.response.split()) > 30:
            self.response = "<prosody rate='95%'>" + self.response + "</prosody>"

    def add_pauses(self):

        def replace(match):
            word_before_break_punctuation = match.group(1)
            if len(word_before_break_punctuation) > 1 and word_before_break_punctuation.lower() not in ["mr"]:
                return f"{match.group(1)}{match.group(2)} <break time='150ms'/> {match.group(3)}"
            else:
                return match.group(0)

        result = re.sub(r"([a-zA-Z0-9]+)(\.|\!|\?|\:)\s+([A-Z0-9])", replace, self.response)

        self.response = result

    def replace_unallowed_character(self):
        self.response = re.sub(r"(?<! )&", " &", self.response)  # ensure a space before &
        self.response = re.sub(r"&(?!\s)", "& ", self.response)  # ensure a space after &
        self.response = re.sub(r"&", "and", self.response)

    def avoid_dot_being_pronounced(self):
        self.response = re.sub(r"\.\s\<\/", ".</", self.response)

    def remove_emotion(self):
        self.response = re.sub(r"\<amazon:emotion[^\<]+\>", "", self.response)
        self.response = re.sub(r"\<\/amazon\:emotion\>", "", self.response)

    def remove_emphasis(self):
        self.response = re.sub(r"\<emphasis[^\<]+\>", "", self.response)
        self.response = re.sub(r"\<\/emphasis\>", "", self.response)

    def remove_domain(self):
        self.response = re.sub(r"\<amazon\:domain[^\<]+\>", "", self.response)
        self.response = re.sub(r"\<\/amazon\:domain\>", "", self.response)

    def remove_pitch(self):
        regex_pitch = r"\<prosody pitch[^<]+\>(.*?)\<\/prosody\>"
        matches = re.findall(regex_pitch, self.response)
        number_matches = len(matches)

        if number_matches >= 3:
            # don't remove pitch if more then 3 matches
            return self.response

        self.response = re.sub(regex_pitch, r"\g<1>", self.response)

    def adjust_punctuation_around_interjection(self):
        # remove comma after interjection
        regex_say_as_end_and_next_comma = r"<\/say\-as\>[\s]*(\<.+\>)*[\s]*,"
        pattern = re.compile(regex_say_as_end_and_next_comma)
        m = pattern.search(self.response)
        if m:
            replace = m.group(0)[:-1]
            self.response = pattern.sub(replace, self.response)

        # remove break after interjection
        regex_break_pattern_1 = r"<\/say\-as\>[\s]*(\<.+\>)*(\<break[^<]*</break>)"
        self.remove_break(regex_break_pattern_1)

        regex_break_pattern_2 = r"<\/say\-as\>[\s]*(\<.+\>)*[\s]*(\<break.*/>)"
        self.remove_break(regex_break_pattern_2)

        # add period before interjection if needed
        regex_interjection = r"\<say\-as interpret-as[^<]+\>(.*?)\<\/say\-as\>"
        m = re.search(regex_interjection, self.response)
        if m:
            inner_content = m.group(1)
            if inner_content and inner_content.strip()[-1] not in [".", "?", "!"]:
                self.response = re.sub(regex_interjection,
                                   f"<say-as interpret-as='interjection'>{m.group(1)}. </say-as>",
                                   self.response)

    def remove_break(self, regex):
        pattern = re.compile(regex)
        match = pattern.search(self.response)
        if match:
            break_part = match.group(2)
            self.response = self.response.replace(break_part, "")

    def remove_interjection(self):
        regex_interjection = r"\<say\-as interpret-as[^<]+\>(.*?)\<\/say\-as\>"
        self.response = re.sub(regex_interjection, r"\g<1>", self.response)

    def wrap_emotion_with_ab_test(self):
        if self.a_b_test_condition == 'a':
            return

        if self.selected_module != TopicModule.NEWS.value:
            if self.a_b_test_condition in ['b']:
                self.wrap_emotion("low")

    def wrap_emotion(self, excited_intensity: str):
        self.response = f"<amazon:emotion name='excited' intensity='{excited_intensity}'>" + self.response + "</amazon:emotion>"
