from nlu.util_nlu import *
from nlu.intent_classifier import *  # noqa
from cobot_core.state_manager import StateManager  # noqa
from cobot_core.log.logger import LoggerFactory  # noqa
from selecting_strategy.topic_module_proposal import *
from cobot_core.prompt_constants import Prompt  # noqa
import utils
import template_manager  # noqa
import re
from nlu.constants import TopicModule
from acknowledgement.acknowledgement_generator import LocalOutputTag
from cobot_core.prompt_constants import Prompt  # noqa

from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier

error_template = template_manager.Templates.error



class ResponseContentPostProcessor:
    PROFANITY_BLACK_LIST_MODULE = [
        TopicModule.RETRIEVAL.value,
        TopicModule.MOVIE.value,
        TopicModule.TECHSCI.value,
        TopicModule.SPORTS.value,
        TopicModule.FOOD.value
    ]

    def __init__(self, state_manager: StateManager, current_module, response_text):
        self.state_manager = state_manager
        self.current_module = current_module
        self.response_text = response_text
        self.custom_intents = get_feature_by_key(self.state_manager.current_state, 'intent_classify')
        self.logger = LoggerFactory.setup(self)

    def process(self):
        self.logger.info("[ResponseContentPostProcessor] start processing response: {}".format(self.response_text))
        if self.is_invalid_response():
            setattr(self.state_manager.current_state, "resp_type", "empty_response")

            self.logger.warning(
                "[RCPP] The selected response generator return an invalid response: {}".format(self.response_text))
            if self.state_manager.current_state.request_type == 'LaunchRequest':
                response = Prompt.welcome_prompt_followup
            elif self.two_crash_in_a_roll():
                response = error_template.utterance(selector="two_crash_in_a_roll", slots={},
                                         user_attributes_ref=self.state_manager.user_attributes)

                reset_propose_continue(self.state_manager.user_attributes)
                #response = self.concatenate_proposed_topic_if_applicable(self.current_module, response, True)

            else:
                response = self.generate_error_handling_response()
                if utils.get_working_environment() == "local":
                    response = "CRASH - TOPIC MODULE EMPTY RESPONSE"

        # elif self.contains_profanity():
        #     setattr(self.state_manager.current_state, "resp_type", "profanity_response")
        #     if utils.get_working_environment() == "local":
        #         response = "ERROR - TOPIC MODULE CONTAINS PROFANITY RESPONSE"
        #     else:
        #         response = self.generate_error_handling_response()

        else:
            response = self.add_acknowledgement(self.response_text, self.current_module)
            # response = self.concatenate_proposed_topic_if_applicable(self.current_module, response)

        self.update_last_module_and_previous_modules_to_user_attributes()

        self.logger.info("[ResponseContentPostProcessor]response after process: {}".format(self.response_text))
        return response

    def is_invalid_response(self):
        return not self.response_text or self.response_text in ["\{\}", "{}"]

    def contains_profanity(self):
        if self.current_module not in self.PROFANITY_BLACK_LIST_MODULE:
            self.logger.info(
                '[RCPP] response from profanity-free module: {}'.format(self.current_module))
            return False

        try:
            if BotResponseProfanityClassifier.is_profane(self.response_text, self.current_module):
                self.logger.info('[DM] response contains profanity')
                return True
            return False
        except Exception as e:
            self.logger.error('[DM] output profanity check error, {}'.format(e), exc_info=True)
            return False

    def two_crash_in_a_roll(self):
        return self.contains_crash_template(self.get_last_turn_template_keys())


    def generate_error_handling_response(self):
        self.logger.info('[RCPP] execute error handling with invalid response')

        custom_intents = get_feature_by_key(self.state_manager.current_state, 'intent_classify')

        selector = 'ranking_strategy/error/'
        # TODO: design more granular error handling based on the lexical patterns in a template format
        keyword = 'that'
        if 'ask_opinion' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ask_opinion',
                                                      slots={
                                                          'keyword': keyword if keyword != '' else ' that'},
                                                      user_attributes_ref=self.state_manager.user_attributes)
        elif 'ask_ability' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ask_ability', slots={},
                                                      user_attributes_ref=self.state_manager.user_attributes)

        elif 'ask_advice' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ask_advice',
                                                      slots={
                                                          'keyword': keyword if keyword != '' else ' that'},
                                                      user_attributes_ref=self.state_manager.user_attributes)
        elif 'ask_preference' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ask_preference',
                                                      slots={
                                                          'keyword': keyword if keyword != '' else 'that'},
                                                      user_attributes_ref=self.state_manager.user_attributes)

        elif 'ask_hobby' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ask_hobby',
                                                      slots={
                                                          'keyword': keyword if keyword != '' else 'that'},
                                                      user_attributes_ref=self.state_manager.user_attributes)
        elif 'ask_recommend' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ask_recommend',
                                                      slots={
                                                          'keyword': keyword if keyword != '' else ''},
                                                      user_attributes_ref=self.state_manager.user_attributes)
        elif 'ask_reason' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ask_reason',
                                                      slots={
                                                          'keyword': keyword if keyword != '' else 'it'},
                                                      user_attributes_ref=self.state_manager.user_attributes)
        elif 'ask_loc' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ask_loc', slots={},
                                                      user_attributes_ref=self.state_manager.user_attributes)

        elif 'ans_like' in custom_intents['lexical']:
            # TODO: what is curr_utt
            # entity =  re.sub(r".*like |.*love ", "", curr_utt) \
            #     if re.search(r"i like \w+$|i love \w+$", curr_utt) else ''

            final_response = error_template.utterance(selector=selector + 'ans_like',
                                                      slots={'keyword': keyword if keyword != '' else 'it'},
                                                      user_attributes_ref=self.state_manager.user_attributes)
        elif 'ans_factopinion' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ans_factopinion', slots={},
                                                      user_attributes_ref=self.state_manager.user_attributes)

        elif 'ans_unknown' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ans_unknown', slots={},
                                                      user_attributes_ref=self.state_manager.user_attributes)

        elif 'ans_neg' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ans_neg', slots={},
                                                      user_attributes_ref=self.state_manager.user_attributes)

        elif 'ans_pos' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ans_pos', slots={},
                                                      user_attributes_ref=self.state_manager.user_attributes)

        elif 'ans_wish' in custom_intents['lexical']:
            final_response = error_template.utterance(selector=selector + 'ans_wish', slots={},
                                                      user_attributes_ref=self.state_manager.user_attributes)

        else:
            final_response = error_template.utterance(selector=selector + 'default', slots={},
                                                      user_attributes_ref=self.state_manager.user_attributes)
        return final_response

    def add_acknowledgement(self, final_response, slct_module):

        SHOULD_NOT_FORCE_ADD_ACKNOWLEDGEMENT_MODULES = [
            "LAUNCHGREETING",
            TopicModule.MOVIE.value,
            TopicModule.FASHION.value,
            TopicModule.BOOK.value,
            "RETRIEVAL",
            "TECHSCIENCECHAT",
            "TRAVELCHAT",
            "NEWS",
            "FASHIONCHAT",
            "CLARIFICATION",
            "FOODCHAT",
            "SPORT",
            TopicModule.SAY_COMFORT.value,
            "MUSICCHAT",
            TopicModule.GAME.value,
            TopicModule.SOCIAL.value,
            TopicModule.COVIDCHAT.value
        ]

        if slct_module not in SHOULD_NOT_FORCE_ADD_ACKNOWLEDGEMENT_MODULES:
            acknowledgement = getattr(self.state_manager.current_state, "system_acknowledgement",
                                      {"output_tag": "", "ack": ""})
            self.logger.info("acknowledgement: {}".format(acknowledgement))
            if acknowledgement:
                if acknowledgement.get("output_tag") == "not_sure":
                    MODULES_THAT_DONT_HANDLE_NOT_SURE = ["TECHSCIENCECHAT", "SOCIAL", "NEWS"]
                    if slct_module in MODULES_THAT_DONT_HANDLE_NOT_SURE:
                        final_response = acknowledgement.get("ack", "") + final_response
                elif acknowledgement.get("output_tag") in [LocalOutputTag.ACK_QUESTION_IDK,
                                                           LocalOutputTag.ACK_OPINION,
                                                           LocalOutputTag.ACK_GENERAL]:
                    # don't force add this, let module handle it themselves
                    final_response = final_response
                elif acknowledgement.get("output_tag") in [LocalOutputTag.ACK_HOBBIES, LocalOutputTag.ACK_WHAT_TO_TALK]:
                    MODULES_THAT_NEED_SYSTEM_ACK = [
                        # TopicModule.FASHION.value,
                        TopicModule.ANIMAL.value,
                        TopicModule.NEWS.value,
                        TopicModule.TRAVEL.value,
                        # TopicModule.FOOD.value
                    ]
                    if slct_module in MODULES_THAT_NEED_SYSTEM_ACK:
                        final_response = acknowledgement.get("ack", "") + final_response
                else:
                    final_response = acknowledgement.get("ack", "") + final_response

                self.logger.info("final response: {}".format(final_response))

        return final_response

    def concatenate_proposed_topic_if_applicable(self, current_module, final_response, force=False):
        ab_setting = getattr(
            self.state_manager.current_state, "a_b_test", None)

        ab_setting = ab_setting.lower() if ab_setting is not None else None

        propose_topic_response = get_propose_topic_response(self.state_manager.user_attributes,
                                                            self.state_manager,
                                                            current_module, ab_setting, force)

        final_response += " " + propose_topic_response

        return final_response

    def update_last_module_and_previous_modules_to_user_attributes(self):
        setattr(self.state_manager.user_attributes, 'last_module', self.current_module)
        # update previous three modules
        if self.state_manager.user_attributes.previous_modules is None:
            setattr(self.state_manager.user_attributes, 'previous_modules', [
                '__EMPTY__', '__EMPTY__', '__EMPTY__'])
        self.state_manager.user_attributes.previous_modules.pop()
        self.state_manager.user_attributes.previous_modules.insert(0, self.current_module)

    def get_last_turn_template_keys(self):
        return self.state_manager.last_state.get("template_keys", [])

    @staticmethod
    def contains_crash_template(template_keys):
        crash_template_regex = "|".join([
            r"error::empty_response",
            r"error::sys_crash",
            r"error::ranking_strategy/error.*"
        ])

        for key in template_keys:
            if re.search(crash_template_regex, key):
                return True

        return False


