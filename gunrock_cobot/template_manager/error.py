class TemplateError(Exception):
    pass


class TemplateSelectorError(TemplateError):

    def __init__(self, selector: str, message: str = None):
        self.selector = selector
        self.message = message

    def __str__(self):
        return f"[{self.__class__.__name__}] selector: {self.selector}; message: {self.message}"


class TemplateNoEmbeddedInfo(Exception):

    def __init__(self, selector: str, embedded_info: dict, message: str = None):
        self.selector = selector
        self.embedded_info = embedded_info
        self.message = message

    def __str__(self):
        return (f"[{self.__class__.__name__}] selector: {self.selector}; "
                f"embedded_info: {self.embedded_info}; message: {self.message}")
