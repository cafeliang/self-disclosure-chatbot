import logging
import template_manager
from utils import EVI_PROFANITY_RESPONSE, template_transition, template_social
import utils
from typing import List
import random

from user_profiler.user_profile import UserProfile
from selecting_strategy.module_selection import ModuleSelector, module_and_user_attributes_key_map, global_state_map, discussed_module_global_state_map, GlobalState
from nlu.constants import TopicModule


OPEN_QUESTION_KEYS = ['open_question/past_events', 'open_question/future_events']

LIST_MODULE_ACCEPT_PREFERRED_ENTITY = [
    'GAMECHAT',
    'MUSICCHAT',
    # 'SPORT',
    # 'BOOKCHAT',
    # 'MOVIECHAT',
    # 'TRAVELCHAT',
    # 'TECHSCIENCECHAT',
    # 'ANIMALCHAT',
    # 'FOODCHAT',
    # 'FASHIONCHAT',
]

TOPICS_THAT_NEED_SYSTEM_PROPOSE_NEXT_MODULE_WHEN_STOP = [
        'SPORT',
        'BOOKCHAT',
        'MOVIECHAT',
        'GAMECHAT',
        'TRAVELCHAT',
        'TECHSCIENCECHAT',
        'ANIMALCHAT',
        'MUSICCHAT',
        'NEWS',
        'FOODCHAT',
        'HOLIDAYCHAT',
        'REQ_TIME',
        'WEATHER',
        'FASHIONCHAT',
        'OUTDOORCHAT',
        'SELFDISCLOSURE',
        TopicModule.SAY_FUNNY.value
    ]


def get_propose_topic_response(user_attributes, state_manager, current_module, ab_setting, force=False):
    topic_propose_response = ""

    if force or (module_propose_stop(user_attributes, current_module) and
                 current_module in TOPICS_THAT_NEED_SYSTEM_PROPOSE_NEXT_MODULE_WHEN_STOP):

        if not get_propose_topic(user_attributes):
            if should_propose_open_question(user_attributes, ab_setting):
                topic_propose_response = propose_open_question(user_attributes)
            else:
                topic_propose_response = propose_topic_module(current_module, user_attributes, state_manager)

    # special case for retrieval due to legacy issue
    elif current_module == 'RETRIEVAL' and user_attributes.retrieval_propose is True:
        topic_propose_response = propose_topic_after_retrieval(user_attributes)

    return topic_propose_response


def propose_open_question(user_attributes):
    template_key = get_next_available_open_question_type(user_attributes)
    logging.info(f"get_next_available_open_question_type: {template_key}")
    add_asked_open_question_type(user_attributes, template_key)
    topic_propose_response = template_social.utterance(selector=template_key,
                                                       slots={},
                                                       user_attributes_ref=user_attributes)
    return topic_propose_response


def propose_topic_module(current_module, user_attributes, state_manager):
    logging.info("propose_topic_module")

    module_selector = ModuleSelector(user_attributes)
    next_module, tag = module_selector.get_next_modules()[0]
    if next_module is not None:

        transition_template_between_modules, args = get_transition_template_between_modules(current_module, next_module, user_attributes)
        if transition_template_between_modules:
            topic_propose_response = transition_template_between_modules
            user_attributes.prev_hash['transition_args'] = args
        else:
            #get the proposed topic in last turn, and users dislike it
            topic_propose_response = generate_topic_proposal_response(state_manager, next_module)

        # add module to used module and set propose_topic
        module_selector.add_used_topic_module(next_module)
        setattr(user_attributes, "propose_topic", next_module)

    else:
        topic_propose_response = template_transition.utterance(selector='propose_topic_short/SOCIAL', slots={},
                                                               user_attributes_ref=user_attributes)
    return topic_propose_response


def generate_topic_proposal_response(state_manager, module_to_propose, ack=True):
    user_attributes = state_manager.user_attributes

    module_selector = ModuleSelector(user_attributes)

    acknowledge = ""
    topic_proposal = ""

    last_state = state_manager.last_state if state_manager.last_state else None
    proposed_topic_in_last_turn = last_state.get("proposed_topic", None)
    all_modules_discussed = getattr(user_attributes, "all_modules_discussed", None)

    # acknowledge to tell the user it's ok
    if proposed_topic_in_last_turn:
        acknowledge = template_social.utterance(selector='neg_transition', slots={},
                                                user_attributes_ref=user_attributes)
    # when all modules have been discussed
    if all_modules_discussed:
        propose = EntityProposer(user_attributes)
        mention = propose.mention_saved_entity_in_topic_module(module_to_propose)
        logging.info("mention entity results: {}".format(mention))
        if mention:
            module_subtopic_proposal_state = discussed_module_global_state_map.get(module_to_propose, None)
            logging.info(f"module_subtopic_proposal_state: {module_subtopic_proposal_state}")
            if module_subtopic_proposal_state:
                global_state = random.choice(module_subtopic_proposal_state)
                template_key = "reenter_topic_subtopic/{}/{}".format(module_to_propose, global_state.value)
                topic_proposal = template_social.utterance(template_key, slots={}, user_attributes_ref=user_attributes)
                module_selector.global_state = global_state
            else:
                topic_proposal = template_transition.utterance(selector='propose_topic_short/{}'.format(module_to_propose), slots={},
                                                               user_attributes_ref=user_attributes)
            topic_proposal = mention + topic_proposal
        else:
            topic_specific_key = 'topic_specific'
            topic_proposal = template_social.utterance(selector='{}/{}'.format(
                topic_specific_key, module_to_propose), slots={}, user_attributes_ref=user_attributes)
    elif module_selector.has_available_preferred_topics():
        preferred_entities = module_selector.get_available_preferred_entity_topics()
        propose_entity = select_propose_entity(preferred_entities, module_to_propose)
        # if the entity has entity_detected, mention that we remember it and propose this module
        if propose_entity.entity_detected and module_to_propose in LIST_MODULE_ACCEPT_PREFERRED_ENTITY:
            template_key = "user_preferred_entity/{}".format(module_to_propose)
            topic_proposal = template_social.utterance(template_key, slots={"entity": propose_entity.entity_detected}, user_attributes_ref=user_attributes)
            # todo set propose entity in module selection, mapping
            module_selector.add_propose_keyword(propose_entity)
        else:
            module_subtopic_proposal_state = global_state_map.get(module_to_propose, None)
            logging.info(f"module_subtopic_proposal_state: {module_subtopic_proposal_state}")
            if module_subtopic_proposal_state:
                global_state = random.choice(module_subtopic_proposal_state)
                template_key = "user_preferred_topic_subtopic/{}/{}".format(module_to_propose, global_state.value)
                topic_proposal = template_social.utterance(template_key, slots={}, user_attributes_ref=user_attributes)

                module_selector.global_state = global_state
            else:
                topic_specific_key = 'user_preferred_topic'
                topic_proposal = template_social.utterance(selector='{}/{}'.format(
                    topic_specific_key, module_to_propose), slots={}, user_attributes_ref=user_attributes)  
    else:
        topic_specific_key = 'topic_specific'
        topic_proposal = template_social.utterance(selector='{}/{}'.format(
            topic_specific_key, module_to_propose), slots={}, user_attributes_ref=user_attributes)
    if ack:
        topic_propose_response = acknowledge + topic_proposal
    else:
        topic_propose_response = topic_proposal
    return topic_propose_response


def get_transition_template_between_modules(current_module, next_module, user_attributes):
    selector = 'transition_56/{}/{}'.format(current_module, next_module)
    if template_transition.has_selector(selector):
        transition = template_transition.utterance(selector='transition_56/{}/{}'.format(
            current_module, next_module), slots={}, user_attributes_ref=user_attributes)
        # only if transition returns a dict
        if transition and isinstance(transition, dict):
            utt, args = transition['utt'], transition['args']
            if utt:
                return utt, args
    return None, None

def propose_topic_after_retrieval(user_attributes):
    module_selector = ModuleSelector(user_attributes)
    module, tag = module_selector.get_next_modules()[0]
    module_selector.add_used_topic_module(module)
    setattr(user_attributes, "propose_topic", module)
    if module is not None:
        topic_propose_response = template_social.utterance(
            selector='topic_specific/{}'.format(module),
            slots={},
            user_attributes_ref=user_attributes)
    else:  # error handling
        topic_propose_response = template_transition.utterance(selector='propose_topic_short/SOCIAL', slots={},
                                                               user_attributes_ref=user_attributes)
    return topic_propose_response


def reset_propose_continue(user_attributes):
    module_selector = ModuleSelector(user_attributes)

    for module in module_and_user_attributes_key_map:
        module_selector.set_propose_continue(module, 'STOP')

    user_attributes.socialchat['curr_state'] = 's_init'


def module_propose_stop(user_attributes, module):
    module_selector = ModuleSelector(user_attributes)
    return module_selector.get_propose_continue(module) == "STOP"


def check_propose_continue(user_attributes, selected_module):
    module_selector = ModuleSelector(user_attributes)
    if selected_module in module_and_user_attributes_key_map:
        for module in module_and_user_attributes_key_map:
            if module != selected_module:
                module_selector.set_propose_continue(module, 'STOP')


def disable_propose(user_attributes, module):
    if user_attributes.disable_propose is None:
        user_attributes.disable_propose = [module]
    else:
        user_attributes.disable_propose.append(module)


def last_module_propose_unclear(user_attributes, last_module):
    module_selector = ModuleSelector(user_attributes)
    return module_selector.get_propose_continue(last_module) == "UNCLEAR"


def get_propose_topic(user_attributes):
    module_selector = ModuleSelector(user_attributes)
    return module_selector.propose_topic

####### Open question ####

def should_propose_open_question(user_attributes, a_b_test):
    module_selector = ModuleSelector(user_attributes)
    return not user_attributes.social_open_question_proposing and \
           not module_selector.has_available_preferred_topics() and \
           get_available_open_question_type(user_attributes) and \
           prefer_open_question(a_b_test, user_attributes)

def should_propose_open_question_after_jump_out_from_topic(user_attributes, a_b_test):
    module_selector = ModuleSelector(user_attributes)
    return user_attributes.previous_modules[0] not in ['SOCIAL', 'LAUNCHGREETING', 'DAILYLIFE'] and \
           not module_selector.has_available_preferred_topics() and \
           not user_attributes.last_turn_propose_open_question and \
           get_available_open_question_type(user_attributes) and \
           prefer_open_question(a_b_test, user_attributes)


def prefer_open_question(a_b_test: str, user_attributes):
    return True


def get_asked_open_question_types(user_attributes) -> List[str]:
    if not user_attributes.socialchat:
        user_attributes.socialchat = {}  # todo: should do initialization in other place
        return []

    return user_attributes.socialchat.get('asked_open_question_type', [])


def add_asked_open_question_type(user_attributes, question_type):
    asked_open_question_types = get_asked_open_question_types(user_attributes)
    asked_open_question_types.append(question_type)
    user_attributes.socialchat["asked_open_question_type"] = asked_open_question_types


def get_available_open_question_type(user_attributes) -> List[str]:
    asked_open_question_types = get_asked_open_question_types(user_attributes)
    available_open_question = [open_question_type for open_question_type in OPEN_QUESTION_KEYS if open_question_type not in asked_open_question_types]
    logging.info(f"get_available_open_question_type: {available_open_question}")

    return available_open_question


def get_next_available_open_question_type(user_attributes):
    available_open_questions = get_available_open_question_type(user_attributes)
    if available_open_questions:
        return available_open_questions[0]
    return ""

def select_propose_entity(preferred_entities, module_to_propose):
    propose_entity = UserProfile.PreferrdEntityTopic()
    for entity in preferred_entities:
        if entity.module and entity.module.value == module_to_propose:
            if entity.entity_detected:
                return entity
            else:
                propose_entity = entity
    return propose_entity


class EntityProposer:
    def __init__(self, user_attributes):
        self.user_profile = UserProfile(user_attributes)
        self.entities_extracted = 0
        self.max_entites = 3

    def propose_entity_in_topic_profile(self) -> str:

        try_to_extract = []

        # Animal
        try:
            if self.entities_extracted != self.max_entites:
                pet_name = self.user_profile.topic_module_profiles.animal.pet['name']
                pet_type = self.user_profile.topic_module_profiles.animal.pet['type']
                try_to_extract.append(f"you have a {pet_type} named {pet_name}")
                self.entities_extracted += 1
        except:
            try:
                if self.entities_extracted != self.max_entites:
                    pet_name = self.user_profile.topic_module_profiles.animal.pet['name']
                    try_to_extract.append(f"you have a pet named {pet_name}")
                    self.entities_extracted += 1
            except:
                pass
            try:
                if self.entities_extracted != self.max_entites:
                    pet_type = self.user_profile.topic_module_profiles.animal.pet['type']
                    try_to_extract.append(f"you have a {pet_type}")
                    self.entities_extracted += 1
            except:
                pass

        # Music
        try:
            if self.entities_extracted != self.max_entites:
                fav_genere = self.user_profile.topic_module_profiles.music.fav_genres[0]
                try_to_extract.append(f"you like {fav_genere} music")
                self.entities_extracted += 1
        except:pass

        # Food
        try:
            if self.entities_extracted != self.max_entites:
                fav_food = self.user_profile.topic_module_profiles.food.fav_food
                if fav_food != "":
                    try_to_extract.append(f"your favourite food is {fav_food}")
                    self.entities_extracted += 1
        except:pass

        # Sport
        try:
            if self.entities_extracted != self.max_entites:
                fav_sport = self.user_profile.topic_module_profiles.sport.fav_sport_type
                logging.info("fav_sportjosh")
                logging.info(fav_sport)
                logging.info("fav_sportjosh")
                if fav_sport != "":
                    try_to_extract.append(f"you like {fav_sport}")
                    self.entities_extracted += 1
        except:pass

        self.entities_extracted = 0

        if try_to_extract != []:
            return "I remember " + " and ".join(try_to_extract) + "!"

        return ""

    def mention_saved_entity_in_topic_module(self, module) -> str:
        try_to_extract = []
        if module == TopicModule.ANIMAL.value:
            try:
                pet_name = self.user_profile.topic_module_profiles.animal.pet['name']
                pet_type = self.user_profile.topic_module_profiles.animal.pet['type']
                try_to_extract.append(f"you have a {pet_type} named {pet_name}")
            except:
                try:
                    pet_name = self.user_profile.topic_module_profiles.animal.pet['name']
                    try_to_extract.append(f"you have a pet named {pet_name}")
                except:
                    pass
                try:
                    pet_type = self.user_profile.topic_module_profiles.animal.pet['type']
                    try_to_extract.append(f"you have a {pet_type}")
                except:
                    pass
        elif module == TopicModule.MUSIC.value:
            try:
                fav_genere = self.user_profile.topic_module_profiles.music.fav_genres[0]
                try_to_extract.append(f"you like {fav_genere} music")
            except:pass
        elif module == TopicModule.FOOD.value:
            try:
                fav_food = self.user_profile.topic_module_profiles.food.fav_food
                if fav_food != "":
                    try_to_extract.append(f"your favourite food is {fav_food}")
            except:pass
        elif module == TopicModule.SPORTS.value:
            try:
                fav_sport = self.user_profile.topic_module_profiles.sport.fav_sport_type
                if fav_sport != "":
                    try_to_extract.append(f"you like {fav_sport}")
            except:pass
        if try_to_extract != []:
            return "I remember " + " and ".join(try_to_extract) + "! "

        return ""
