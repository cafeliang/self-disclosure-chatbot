'''
Map custom topic intent to topic response generator
'''

custom_intent_topic_map = {
    # Modular Level Intent
    # topic_controversial
    'topic_phatic': 'PHATIC',
    'topic_controversial': 'CONTROVERSIALOPION',
    'topic_profanity': 'CONTROVERSIALOPION',
    #'topic_backstory': 'SOCIAL',
    'topic_movietv': 'MOVIECHAT',
    'topic_newspolitics': 'NEWS',
    'topic_health': 'SAY_COMFORT',
    'topic_weather': 'WEATHER',
    'topic_qa': 'SOCIAL',
    # Note that this is just for temporary categorization setting to
    # rely on a single retrieval model;
    # Later, more customization is needed to design the dialog flow
    'topic_music': 'MUSICCHAT',
    'topic_book': 'BOOKCHAT',
    'topic_sport': 'SPORT',
    'topic_outdoor': 'OUTDOORCHAT',
    'topic_fashion': 'FASHIONCHAT',
    'topic_entertainment': 'RETRIEVAL',  # Potentially can be MOVIECHAT
    'topic_holiday': 'HOLIDAYCHAT',
    'topic_food': 'FOODCHAT',
    'topic_techscience': 'TECHSCIENCECHAT',  # disable it until version ready
    'topic_travel': 'TRAVELCHAT',
    'topic_finance': 'CONTROVERSIALOPION',
    'topic_history': 'RETRIEVAL',
    'topic_animal': 'ANIMALCHAT',
    'topic_game': 'GAMECHAT',
    'topic_storymeme': 'STORYFUNFACT',
    'topic_dailylife': 'DAILYLIFE',
    'topic_others': 'RETRIEVAL',
    'req_topic': 'SOCIAL',
    'topic_saycomfort': 'SAY_COMFORT',
    'say_funny': 'SAY_FUNNY'
}

amazon_topic_map_2019 = {
    "Entertainment_Movies": "MOVIECHAT",
    "Entertainment_Music" : "MUSICCHAT",
    "Entertainment_Books" : "BOOKCHAT",
    "Entertainment_General" : "",
    "Sports": "SPORT",
    "Politics": "",
    "Science_and_Technology" : "TECHSCIENCECHAT",
    "Phatic" : "",
    "Interactive" : "",
    "Inappropriate_Content" : "",
    "Other" : "",
}

AMAZON_TOPIC_MAP_2018 = {
    'Art_Event': '',
    'Business': 'NEWS',
    'Celebrities': '',
    'Drug': '',
    'Education': 'NEWS',
    'Entertainment': '',
    'Fashion': 'FASHIONCHAT',
    'Food_Drink': 'FOODCHAT',
    'Games': '',
    'Health': '',
    'History': '',
    'Joke': 'SAY_FUNNY',
    'Literature': '',
    'Math': 'SOCIAL',
    'Movies_TV': '',
    'Music': '',
    'News': 'NEWS',
    'Other': '',
    'Pets_Animals': '',
    'Phatic': '',
    'Politics': '',
    'Psychology': '',
    'Relationship': '',
    'Religion': '',  # TODO: add a religion module
    'SciTech': '',
    'Sex_Profanity': '',
    'Shopping': '',
    'Sports': 'SPORT',
    'Travel_Geo': '',
    'Weather_Time': ''
}


concept_map = {
    #"movie": "MOVIECHAT",
    #"movies": "MOVIECHAT",
    #"film": "MOVIECHAT",
    "superhero": "MOVIECHAT",
    "science fiction movie": "MOVIECHAT",
    "actor": "MOVIECHAT",
    #"book": "BOOKCHAT",
    "singer": "MUSICCHAT",
    "performer": "MUSICCHAT",
    "musician": "MUSICCHAT",
    "music": "MUSICCHAT",
    "musical instrument": "MUSICCHAT",
    #"star": "MUSICCHAT",
    "hit": "MUSICCHAT",
    "band": "MUSICCHAT",
    "animals": "ANIMALCHAT",
    "animal": "ANIMALCHAT",
    "dog": "ANIMALCHAT",
    "bird": "ANIMALCHAT",
    "wildlife": "ANIMALCHAT",
    "cat": "ANIMALCHAT",
    "insect": "ANIMALCHAT",
    "small dog": "ANIMALCHAT",
    "sport": "SPORT",
    "player": "SPORT",
    "athlete": "SPORT",
    "game": "GAMECHAT",
    "games": "GAMECHAT",
    "cartoon game": "GAMECHAT",
    "technology": "TECHSCIENCECHAT",
    "science": "TECHSCIENCECHAT",
    "gemstone": "TECHSCIENCECHAT",
    "country": "TRAVELCHAT",
    #"city": "TRAVELCHAT",
    "nation": "TRAVELCHAT",
    "region": "TRAVELCHAT",
    "state": "TRAVELCHAT",
    #"place": "TRAVELCHAT",
    "food": "FOODCHAT",
    "meat": "FOODCHAT",
    "baked goods": "FOODCHAT",
    "cosmetic": "FASHIONCHAT",
    #"outdoor activity": "OUTDOORCHAT",
}


def preprocess_concept_key(concept_item):
    if "country" in concept_item:
        return "country"
    if "city" in concept_item:
        return "city"
    # if "movie" in concept_item:
    #     return "movie"
    if "hit" in concept_item:
        return "hit"
    if "wildlife" in concept_item:
        return "wildlife"
    return concept_item



google_kg_entity_type_topic_map = {
    'SportsTeam': 'sports',
    'TouristAttraction': 'travel',
    'AmusementPark': 'travel',
}

google_kg_entity_name_topic_map = {
    'Pet': 'animals',

    'Board game': 'game',
    'Game': 'game',
    'Xbox': 'game',

    'Actor': 'movie',
    'Hulu': 'movie',
    'Netflix': 'movie',

    'Cosmetics': 'fashion',
    'Perfume': 'fashion',

    'Hiking': 'outdoor',

    'Running': 'sports',
    'Yoga': 'sports',
    'Baseball': 'sports',
    'Cue sports': 'sports',
    'Martial arts': 'sports',
    'Racing': 'sports',
    'Softball': 'sports',

    'Popcorn': 'food',
    'Chipotle': 'food',
    'Pepperoni': 'food',
}

google_kg_description_topic_map = {
    #===========================
    # sports
    'baseball': 'sports',
    'basketball': 'sports',
    'tennis': 'sports',
    'soccer': 'sports',
    'football': 'sports',
    'footballer': 'sports',
    'hockey': 'sports',
    'badminton': 'sports',
    'swim': 'sports',
    'bowling': 'sports',
    'sport': 'sports',
    'equestrian': 'sports',
    'golf': 'sports',
    'sailing': 'sports',
    'fishing': 'sports',
    'racquetball': 'sports',
    'squash': 'sports',
    'surfing': 'sports',
    'water polo': 'sports',
    'swimmer': 'sports',
    'golfer': 'sports',
    'volleyball': 'sports',
    'cyclist': 'sports',
    'runner': 'sports',
    'athlete': 'sports',
    'archery': 'sports',
    'gymnastics': 'sports',
    'racing': 'sports',
    'rowing': 'sports',
    'skating': 'sports',
    'athletics': 'sports',
    'diving': 'sports',
    'league': 'sports',
    #===========================
    # movie
    # 'movie': "movie",
    'actor': "movie",
    'actress': "movie",
    # 'film': "movie",
    # 'film series': "movie",
    'filmmaker': "movie",
    'director': "movie",
    'disney princess': "movie",
    # 'sitcom': "movie",
    # 'drama': "movie",
    # 'television': "movie",
    # 'animated': "movie",
    'superhero': 'movie',
    # 'fictional': 'movie',
    'television': 'movie',
    #===========================
    # book
    # 'novel': 'book',
    # 'novelist': 'book',
    'author': 'book',
    'writer': 'book',
    'sacred text': 'book',
    'comic genre': 'book',
    # 'book': 'book',
    #===========================
    # music
    # 'song': 'music',
    'musical instrument': 'music',
    'musical genre': 'music',
    'singer': 'music',
    'songwriter': 'music',
    'american singer': 'music',
    # 'album': 'music',
    # 'band': 'music',
    'singer-songwriter': 'music',
    'rapper': 'music',
    #===========================
    # travel
    'country': 'travel',
    'capital': 'travel',
    #'city': 'travel',
    'province': 'travel',
    'state': 'travel',
    'town': 'travel',
    #===========================
    # news
    'prime minister': 'news',
    'chancellor': 'news',
    'president': 'news',
    'united states': 'news',
    'minister': 'news',
    'governor': 'news',
    'senator': 'news',
    'white house': 'news',
    #===========================
    # game
    'game': 'game',
    #===========================
    # animals
    'fish': 'animals',
    'animal': 'animals',
    'bird': 'animals',
    'birds': 'animals',
    'dog': 'animals',
    'rodent': 'animals',
    'insect': 'animals',
    'breed': 'animals',
    'reptiles': 'animals',
    #===========================
    # food
    'food': 'food',
    'drink': 'food',
    'pasta': 'food',
    'meat': 'food',
    'poultry': 'food',
    'bean': 'food',
    'cereal': 'food',
    'dairy': 'food',
    'vegetable': 'food',
    'plants': 'food',
    'confection': 'food',
    'sauce': 'food',
    'cooking': 'food',
    'cuisine': 'food',
    'soup': 'food',
    'fruit': 'food',
    'cake': 'food',
    'cookie': 'food',
    'chocolate bar': 'food',
    'condiment': 'food',
    'restaurant': 'food',
    'type of dish': 'food',
    'dish': 'food',
}

google_knowledge_map = {
    "movie": "MOVIECHAT",
    "book": "BOOKCHAT",
    "game": "GAMECHAT",
    "sports": "SPORT",
    "music": "MUSICCHAT",
    "travel": "TRAVELCHAT",
    "news": "NEWS",
    "animals": "ANIMALCHAT",
    "food": "FOODCHAT",
    "outdoor": "OUTDOORCHAT",
    "fashion": "FASHIONCHAT",
}

custom_intent_sys_map = {
    # System level intents
    'adjust_speak': 'ADJUSTSPEAK',
    'terminate': 'TERMINATE',
    'change_topic': 'SOCIAL',
    'clarify': 'CLARIFICATION',
    'say_thanks': 'SAY_THANKS',
    'say_bio': 'SELFDISCLOSURE',
    'req_more': 'REQ_MORE',
    'req_time': 'REQ_TIME',
    'req_playgame': 'PLAY_GAME',
    'req_playmusic': 'MUSICCHAT',
    'req_task': 'REQ_TASK',
    'req_loc': 'REQ_LOC'
}


