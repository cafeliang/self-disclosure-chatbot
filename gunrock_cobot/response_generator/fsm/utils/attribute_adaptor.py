import logging

from typing import List
from user_attribute_adaptor import UserAttributeAdaptor
from selecting_strategy.module_selection import ModuleSelector


class FSMAttributeAdaptor(UserAttributeAdaptor):

    @property
    def module_name(self) -> str:
        return NotImplementedError

    def __init__(self, user_attributes_ref):
        super().__init__(self.module_name, user_attributes_ref)
        self.module_selection = ModuleSelector(user_attributes_ref)

    @property
    def propose_continue(self) -> str:
        return self._storage['propose_continue']

    @propose_continue.setter
    def propose_continue(self, value: str):
        self._storage['propose_continue'] = value

    @property
    def propose_topic(self) -> str:
        try:
            return self.module_selection.propose_topic
        except Exception:
            return self._get_raw('propose_topic')

    @propose_topic.setter
    def propose_topic(self, value: str):
        if isinstance(value, str):
            self.module_selection.propose_topic = value
        else:
            logging.warning(f"[FSM] wrong propose_topic value: {value}")

    @property
    def state_history(self) -> List[List[str]]:
        if not self._storage.get("state_history"):
            self._storage["state_history"] = []
        return self._storage["state_history"]

    @state_history.setter
    def state_history(self, value: List[List[str]]):
        self._storage["state_history"] = value

    @property
    def curr_state(self):
        return self.state_history[-1]
