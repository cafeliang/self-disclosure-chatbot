import logging
import random
import re
from typing import Optional, Tuple

import boto3
from botocore.exceptions import ClientError


logger = logging.getLogger('launchgreeting.dynamodb')

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table_acks = dynamodb.Table('launchgreeting_how_are_you_ack')


def get_acks():
    try:
        response = table_acks.scan()
        ret = response.get('Items', {})

        while 'LastEvaluatedKey' in response:
            response = table_acks.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
            ret.extend(response.get('Items', {}))
        return ret
    except ClientError as e:
        logger.warning(f"[GAME] <funfacts> query db error: {e}", exc_info=True)


def match_ack(text: str) -> Tuple[Optional[str], Optional[str]]:

    table = get_acks()
    print(table)

    if not table:
        return None, None

    for item in table:
        regex, utts, short_utts = tuple(item.get(i) for i in ['regex', 'utts', 'short_utts'])

        if not (regex and utts and short_utts):
            return None, None

        if re.search(regex, text, re.I):
            return random.choice(utts), random.choice(short_utts)
    return None, None
