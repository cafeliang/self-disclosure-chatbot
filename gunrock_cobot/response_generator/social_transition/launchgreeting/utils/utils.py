import random
import re
from typing import List, Tuple, TYPE_CHECKING

import utils as sys_utils
from nlu.constants import DialogAct, Positivity
from nlu.dataclass import ReturnNLP, ReturnNLPSegment
from response_generator.fsm2 import FSMLogger, Dispatcher, Tracker
from response_generator.fsm2.utils.state_tracker import StateTracker
from selecting_strategy.module_selection import ModuleSelector
from user_profiler.user_profile import UserProfile

from response_generator.social_transition.launchgreeting.utils import constants


logger = FSMLogger('LG', 'utils', 'launchgreeting.utils')


@property
def my_name(self):
    if self.regexes['alexa_name'].search(self.input_text):
        return "You can call me Alexa Prize Socialbot."
    else:
        return ""


def is_new_user(tracker: Tracker):
    user_profiler = UserProfile(tracker.user_attributes.ua_ref)
    return user_profiler.visit <= 1


def is_intent_correcting_name(tracker: Tracker, state_tracker: StateTracker, user_model: UserProfile):

    check_input_for_name(tracker)

    logger.debug(
        f"intent_correcting_name: "
        f"regex = {constants.regexes['not_my_name'].search(tracker.input_text)}, "
        f"regex w/ = {constants.regexes['not_my_name_with_correction'].search(tracker.input_text)}, "
        f"check_input_for_name = {tracker.volatile_store.get('check_input_for_name')}, "
        f"username = {user_model.username}"
    )
    if (
        state_tracker.curr_turn().curr_state() != 'not_my_name' and
        (
            constants.regexes['not_my_name'].search(tracker.input_text) or
            constants.regexes['not_my_name_with_correction'].search(tracker.input_text)
        )
    ):
        matches = constants.regexes['not_my_name_with_correction'].search(tracker.input_text)
        logger.debug(f"intent_correcting_name: with_correction matches = {matches}")
        if matches:
            logger.debug(f"intent_correcting_name: with_correction match group = {matches.groupdict().items()}")
        new_names = [v for k, v in matches.groupdict().items() if k.startswith('nn') and v] if matches else []
        new_names_common = list(filter(
            lambda i: _search_common_name(tracker, [i], skip_ua=True), new_names
        ))
        old_names = [v for k, v in matches.groupdict().items() if k.startswith('on') and v] if matches else []
        old_names_common = list(filter(
            lambda i: _search_common_name(tracker, [i], skip_ua=True), old_names
        ))
        logger.debug(f"intent_correcting_name: old_names_common = {old_names_common}")

        if (
            (
                constants.regexes['not_my_name_with_correction'].search(tracker.input_text) and
                (
                    # check if the captured old names is common
                    len(old_names_common) > 0 or
                    # or, if no old names captured, check if the captured new name is common
                    # TODO: better than this. rn if no old name, new name must be common, which may not be true
                    len(old_names) <= 0 and len(new_names_common) > 0
                )
            ) or
            (
                tracker.volatile_store.get('check_input_for_name') and
                tracker.volatile_store.get('is_name_common', False) and
                tracker.volatile_store.get('check_input_for_name') != user_model.username
            )
        ):
            logger.debug(f"return with_correction")
            return 'with_correction'
        elif (
            constants.regexes['not_my_name'].search(tracker.input_text) and
            not user_model.username
        ):
            logger.debug('match only not_my_name but username is None')
            return 'with_correction'
        elif (
            constants.regexes['not_my_name'].search(tracker.input_text)
        ):
            logger.debug(f"return no_correction")
            return 'no_correction'

    logger.debug(f"return None")
    return None


def _search_common_name(tracker: Tracker, candidates: List[str], skip_ua=False):
    ret = []
    for candidate in candidates:
        # check whether full match is a name
        if sys_utils.is_common_name(candidate):
            ret.append(candidate)
            continue

        # check whether part of match is a name
        # in case user provided full name and that combination (first + last) is not in database
        for token in candidate.split():
            if sys_utils.is_common_name(token):
                logger.debug(f'token "{token}" is matched with redis name list')
                if not skip_ua:
                    tracker.volatile_store['check_input_for_name'] = token
                ret.append(token)
                break

        # if this point is reach, no *common* name was detected
    logger.debug(f"_search_common_name: candidates: {candidates}, ret: {ret}")
    return ret


def check_input_for_name(tracker: Tracker):
    profanity = tracker.input_data.get('features', {}).get('profanity_check')
    logger.debug(
        f"check_input_for_name: input = {tracker.input_text}, profanity = {profanity}"
        f"segment = {tracker.returnnlp.text}")

    if profanity and profanity[0] != 0:
        return

    def _search(pattern, name_loc, segments):
        matches = [pattern.search(seg) for seg in segments]
        return [m.group(name_loc) for m in matches if m]

    tracker.volatile_store['check_input_for_name'] = None
    tracker.volatile_store['is_name_common'] = False

    def _save_name(candidates):  # return True on success and False on failure
        names = _search_common_name(tracker, candidates)
        if len(names) > 0:
            tracker.volatile_store['check_input_for_name'] = names[-1]
            tracker.volatile_store['is_name_common'] = True
            return True
        if len(candidates) > 0:  # save the uncommon name but continue searching
            tracker.volatile_store['check_input_for_name'] = candidates[-1]
        return False

    candidates = _search(constants.regexes['call_me'], 1, tracker.returnnlp.text)
    logger.debug(f"check_input_for_name: call_me: {candidates}")
    if _save_name(candidates):
        return

    candidates = _search(constants.regexes['check_name'], 4, tracker.returnnlp.text)
    logger.debug(f"check_input_for_name: check_name: {candidates}")
    if _save_name(candidates):
        return

    candidates = _search(constants.regexes['yes_my_name_is'], 2, tracker.returnnlp.text)
    logger.debug(f"check_input_for_name: yes_my_name_is: {candidates}")
    if _save_name(candidates):
        return

    candidates = [seg for seg in tracker.returnnlp.text if len(seg.split()) <= 2]
    logger.debug(f"check_input_for_name: segmented: {candidates}")
    names = _search_common_name(tracker, candidates)
    if len(names) > 0:
        tracker.volatile_store['check_input_for_name'] = names[-1]
        tracker.volatile_store['is_name_common'] = True
        return

    keyphrase = tracker.returnnlp.key_phrase
    if len(keyphrase) == 1 and len(keyphrase[0]) == 1 and 'name' in tracker.returnnlp.text[0]:
        # only 1 segment with 1 name in segment
        if _save_name(keyphrase[0]):
            return

    # e.g. when user say "nakamura" which isn't in database but
    # there is only 1 segment so it's likely to be a name
    # also exclue 'yes' and 'no'
    if (
        len(candidates) == 1 and
        not (
            constants.regexes['yes_only'].search(next(iter(candidates), "")) or
            constants.regexes['no_only'].search(next(iter(candidates), "")) or
            constants.regexes['yes_only'].search(tracker.input_text) or
            constants.regexes['no_only'].search(tracker.input_text)
        )

    ):
        tracker.volatile_store['check_input_for_name'] = candidates[0]

    logger.info(
        f"check_input_for_name complete. "
        f"name = {tracker.volatile_store.get('check_input_for_name')}, "
        f"is_name_common = {tracker.volatile_store.get('is_name_common')}")


def holiday_greet(get_any_holiday=False) -> Tuple[str, str]:
    """
    Get holiday from sys_utils, return a ("<formatted greeting>", "holiday name")
    """
    """disable this"""
    return "", None

    # holidays = sys_utils.get_future_holiday(limit=1)
    # logger.debug(f"getting holiday from db. holidays = {holidays}")
    # if not holidays:
    #     return "", None

    # holidays_filtered = []
    # for holiday in holidays:
    #     cat = holiday['Category']
    #     if isinstance(cat, str):
    #         cat = [cat]
    #     if any(i in cat for i in ['Federal', 'Government']):
    #         holidays.append(holiday)

    # holiday = next(iter(holidays_filtered), None)
    # if holiday:
    #     holiday = holiday.get('Holiday')
    # if not holiday:
    #     return "", None

    # message = random.choice(['Happy {}!', "It's {}!", "It's {} today!"])
    # return message.format(holiday), holiday


def how_are_you_sentiment(tracker: Tracker) -> Positivity:
    def decider(segment: ReturnNLPSegment):
        # calculate net positivity by summing up all sentiment indicators

        da_score = (
            1 if segment.has_dialog_act(DialogAct.POS_ANSWER) and not segment.has_dialog_act(DialogAct.NEG_ANSWER) else
            -1 if segment.has_dialog_act(DialogAct.NEG_ANSWER) and not segment.has_dialog_act(DialogAct.POS_ANSWER) else
            0
        )
        intent_score = (
            1 if segment.has_intent('opinion_positive') and not segment.has_intent('opinion_negative') else
            -1 if segment.has_intent('opinion_negative') and not segment.has_intent('opinion_positive') else
            0
        )
        senti_score = segment.sentiment.positivity().as_int()
        custom_regex_score = (
            1 if constants.regexes['sentiment_pos'].search(segment.text) else
            -1 if constants.regexes['sentiment_neg'].search(segment.text) else
            0
        )

        combined_score = sum([
            da_score * 1,
            intent_score * 1,
            senti_score * (
                1 if segment.has_dialog_act(DialogAct.OPINION) and not segment.has_dialog_act(DialogAct.STATEMENT) else
                0.5
            ),
            custom_regex_score * 1,
        ])

        logger.debug(
            f"how_are_you_sentiment, decider for text = {segment.text}: "
            f"combined = {combined_score}, ({da_score, intent_score, senti_score})"
        )
        return combined_score

    # regex override
    regex_override = {
        Positivity.pos: (
            r"^(good)$"
        ),
        Positivity.neu: (
            r"^(okay)$"
        ),
        Positivity.neg: (
            r"^(bad)$"
        ),
    }

    for key, regex in regex_override.items():
        if re.search(regex, tracker.input_text):
            return key

    # ignore the ask_back segment's sentiment
    positivities = list(map(decider, filter(
        lambda segment: not any([
            segment.has_intent({
                'ask_back', 'ask_reason', 'ask_follow_up', 'ask_recommend', 'ask_opinion',
                'ask_preference', 'ask_hobby', 'ask_advice'}),
            segment.has_dialog_act(
                {DialogAct.OPEN_QUESTION, DialogAct.OPEN_QUESTION_FACTUAL, DialogAct.OPEN_QUESTION_OPINION},
                threshold=(0.3, 0.1))
        ]),
        tracker.returnnlp
    )))
    final_positivity = sum(positivities)
    logger.debug(f"how_are_you_sentiment, final: {positivities} -> {final_positivity}")
    return Positivity.from_int(final_positivity)


def get_social_template(dispatcher: Dispatcher, tracker: Tracker):

    module_selector = ModuleSelector(tracker.user_attributes.ua_ref)
    module, utt = module_selector.get_next_modules()[0]
    module_selector.add_used_topic_module(module)

    # todo: restore it when we update holiday data on redis
    # _, holiday = self.holiday_greet(get_any_holiday=False)

    if not dispatcher.tm.has_selector(f"social_template/{module}"):
        logger.warn(
            "Launch Greeting get_social_template cannot get a valid social_template, using MOVIE as fallback")
        module = "MOVIECHAT"

    utt = dispatcher.tm.speak(f"social_template/{module}", {'holiday': "not a holiday"})

    return module, utt


def proposing_topics(tracker: Tracker) -> Tuple[List[str], str]:
    module_selector = ModuleSelector(tracker.user_attributes.ua_ref)
    proposing_topics = module_selector.get_next_modules(amount=3)
    modules, utts = list(zip(*proposing_topics))
    return modules, ', '.join(utts[:-1]) + ' and ' + utts[-1]


def has_command_or_question(tracker: Tracker):
    # select the segment containing the question
    if tracker.volatile_store.get('from_question_handler'):
        return False
    # elif self.state == 'how_are_you':
    #     return (
    #         self.question_detector.has_question() and
    #         not self.question_detector.is_ask_back_question()
    #     )
    # else:
    #     return (
    #         # self.command_detector.has_command() or
    #         self.question_detector.has_question()
    #     )
    return False
