from typing import Callable, List, Optional

from nlu.command_detector import CommandDetector
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse
from response_generator.fsm2 import Dispatcher, Tracker, FSMLogger
from response_generator.fsm2.events import Event, NextState

from response_generator.social_transition.launchgreeting import states
from response_generator.social_transition.launchgreeting.utils import constants


logger = FSMLogger('launchgreeting', 'handler', 'launchgreeting.handler')


BACKSTORY_THRESHOLD = 0.82


class Handler:

    def __init__(self, tracker: Tracker):

        self.question_detector = QuestionDetector(
            tracker.input_text, tracker.input_data['returnnlp'])
        self.command_detector = CommandDetector(
            tracker.input_text, tracker.input_data['returnnlp'])
        self.question_handler = QuestionHandler(
            tracker.input_text, tracker.input_data['returnnlp'],
            backstory_threshold=BACKSTORY_THRESHOLD,
            system_ack=tracker.input_data['system_acknowledgement'],
            user_attributes_ref=tracker.user_attributes.ua_ref)

    """
    Detection
    """

    def has_command(self, tracker: Tracker):
        flag = tracker.volatile_store.get('__handler_has_command_handled__') or False
        logger.debug(f"has_question: flag = {flag}, question = {self.command_detector.has_command()}")
        if not flag:
            return self.command_detector.has_command()
        else:
            return False

    def has_question(self, tracker: Tracker):
        flag = tracker.volatile_store.get('__handler_has_question_handled__') or False
        logger.debug(f"has_question: flag = {flag}, question = {self.question_detector.has_question()}")
        if not flag:
            return self.question_detector.has_question()
        else:
            return False

    """
    Handling
    """

    def handle_command(
        self, dispatcher: Dispatcher, tracker: Tracker,
    ):
        tracker.volatile_store['__handler_has_command_handled__'] = True

        # if self.command_detector.is_command_by_dialog_act():
        #     pass
        # elif self.command_detector.is_request_jumpout():
        #     pass
        # elif self.command_detector.is_request_tellmore():
        #     pass
        if False:
            pass
        else:
            dispatcher.respond_template('command/fallback', {})

    def handle_question(
        self, dispatcher: Dispatcher, tracker: Tracker,
        *,
        # required handler
        ask_back_handler: Callable[[QuestionResponse], Optional[List[Event]]],
        follow_up_handler: Callable[[QuestionResponse], Optional[List[Event]]],
        # optional handler
        override_handler: Callable[[QuestionResponse], Optional[List[Event]]] = None,
        elif_handler: Callable[[QuestionResponse], Optional[List[Event]]] = None,
        # optional detector
        is_ask_back_question: Callable[[], bool] = None,
        is_follow_up_question: Callable[[], bool] = None
    ) -> Optional[List[Event]]:
        tracker.volatile_store['__handler_has_question_handled__'] = True

        question_response = self.question_handler.handle_question()
        logger.debug(
            f"question_resposne = {question_response}, "
            f"ask_back = {self.question_detector.is_ask_back_question()}, "
            f"follow_up = {self.question_detector.is_follow_up_question()}, "
        )

        if override_handler:
            logger.debug(f"override_handler")
            return override_handler(question_response)

        elif (
            is_ask_back_question() if is_ask_back_question is not None else
            self.question_detector.is_ask_back_question()
        ):
            logger.debug(f"ask_back_handler, custom = {bool(is_ask_back_question)}")
            return ask_back_handler(question_response)

        elif (
            is_follow_up_question() if is_follow_up_question is not None else
            self.question_detector.is_follow_up_question()
        ):
            logger.debug(f"follow_up_qustion, custom = {bool(is_follow_up_question)}")
            return follow_up_handler(question_response)

        elif (
            constants.regexes['whats_my_name'].search(tracker.input_text)
        ):
            """
            when user asks us to recall their username
            """
            logger.debug(f"what's my name")
            return [NextState(states.WhatsMyNameRespond.name, jump=True)]

        elif elif_handler:
            logger.debug(f"elif_handler")
            return elif_handler(question_response)

        else:
            logger.debug(f"default")
            dispatcher.respond(question_response.response)
            return None
