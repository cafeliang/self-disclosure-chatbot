import re
from typing import Dict, Pattern

from nlu import intentmap_scheme


regexes: Dict[str, Pattern] = {k: re.compile(v, re.I) for k, v in {
    'check_name': r"(.*my name is|.*my name's|.*i'm|.*i am|.*it's|.*this is) (?!(not|great|good|nice|ok(ay)?|confused))(.*)$",
    'call_me': r"call me (.*)",
    'yes_my_name_is': r"(yes|yeah|yup|yea|sure) (.*)",
    'yes_only': r"^(yes|yeah|yup|yea|sure|ok|okay|why not|alright|(yes ?)i do|definitely|certainly|yes my name is|yes i do|if you like|if you want( (me )?to))$",
    'no_only': r"(^(no|nope|na|nah)( thank(s| you))?$|not telling you|why|do(n't| not) (want to|wanna) (tell you|say))",
    'one_word_name_blacklist': r"\b(other)\b",
    'not_my_name': intentmap_scheme.lexical_re_patterns['info_name'],
    'not_my_name_with_correction': (
        # r"(no )?not (?P<on1>(?!.*\b(my)\b.*)\b[\w ]+\b) (i('m| am) |it('s| is) )?(?P<nn1>\b[\w ]+\b)$|(?!my name( is not|'s not| isn't))(i('m| am) |my name('s| is) )?(?P<nn2>(?!.*\b(m|s|name|is)\b.*)\b[\w ]+\b) not (?P<on2>(?!.*\b(it('s| is)|my name)\b.*)\b[\w ]+\b)$|(no (my name('s| is) )?(?P<nn3>\b[\w ]+\b)$)|(?!my name( is not|'s not| isn't))(my name( is|'s)) (?P<nn4>(?!.*what('s| is) your name)\b[\w ]+\b)$"  # noqa: E501
        # limit nn or on to only two words
        # r"(no )?not (?P<on1>(?!.*\b(my)\b.*)\b\w+( \w+)?\b) (i('m| am) |it('s| is) )?(?P<nn1>\b\w+( \w+)?\b)$|(?!my name( is not|'s not| isn't))(i('m| am) |my name('s| is) )?(?P<nn2>(?!.*\b(m|s|name|is|no)\b.*)\b\w+( \w+)?\b) not (?P<on2>(?!.*\b(it('s| is)|my name)\b.*)\b\w+( \w+)?\b)$|(no (my name('s| is) )?(?P<nn3>(?!.*\b(m|s|name|is|no|do|i|you))\b.*\b\w+( \w+)?\b)$)|(?!my name( is not|'s not| isn't))(my name( is|'s)) (?P<nn4>(?!.*what('s| is) your name)\b\w+( \w+)?\b)$"
        r"(no )?not (?P<on1>(?!.*\b(my|really)\b.*)\b\w+( \w+)?\b) (i('m| am) |it('s| is) )?(?P<nn1>\b\w+( \w+)?\b)$|(?!my name( is not|'s not| isn't))(i('m| am) |my name('s| is) )?(?P<nn2>(?!.*\b(m|s|name|is|no)\b.*)\b\w+( \w+)?\b) not (?P<on2>(?!.*\b(it('s| is)|my name)\b.*)\b\w+( \w+)?\b)$|(no (my name('s| is) )?(?P<nn3>(?!.*\b(m|s|name|is|no|do|i|you))\b.*\b\w+( \w+)?\b)$)|(?!my name( is not|'s not| isn't))(my name( is|'s)|it('s| is)) (?P<nn4>(?!.*what('s| is) your name|\w+ (good|bad|fine)|(\w+ )?birthday)\b\w+( \w+)?\b)$"
    ),
    'alexa_name': r"what('s| is) your(s| name)|(how|what) (should|can|could|will|would|shall) I call you",
    'whats_my_name': intentmap_scheme.lexical_re_patterns['ask_user_name'],
    'how_are_you': (
        r"(((how (are|about))|and) you)|((i am|i'm) ?\w* (fine|good|alright|\w+)( what about|^(?!thank)) you)"
    ),
    'coronavirus': (r"\b(corona ?virus)\b"),

    # used for how_are_you_sentiment
    'sentiment_pos': (
        r"\b(i('m| am) (doing|feeling)? ?(pretty|very)? ?(good|well|fine|great))\b"
    ),
    'sentiment_neg': (
        r"\b((i('m| am))? ?not (doing|feeling)? ?(pretty|so|very)? ?(good|fine|well|great|ok(ay)?))\b"
    )
}.items()}
