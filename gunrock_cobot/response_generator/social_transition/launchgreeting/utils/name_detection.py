from typing import List

import utils as sys_utils
from response_generator.fsm2 import FSMLogger, Tracker

from response_generator.social_transition.launchgreeting.utils.constants import regexes


logger = FSMLogger('LG', 'name_detection', 'launchgreeting.name_detection')


def _search_common_name(tracker: Tracker, candidates: List[str], skip_ua=False):
    ret = []
    for candidate in candidates:
        # check whether full match is a name
        if sys_utils.is_common_name(candidate):
            ret.append(candidate)
            continue

        # check whether part of match is a name
        # in case user provided full name and that combination (first + last) is not in database
        for token in candidate.split():
            if sys_utils.is_common_name(token):
                logger.debug(f'token "{token}" is matched with redis name list')
                if not skip_ua:
                    tracker.volatile_store['check_input_for_name'] = token
                ret.append(token)
                break

        # if this point is reach, no *common* name was detected
    logger.debug(f"_search_common_name: candidates: {candidates}, ret: {ret}")
    return ret


def check_input_for_name(tracker: Tracker):
    profanity = tracker.input_data.get('features', {}).get('profanity_check')
    logger.debug(
        f"check_input_for_name: input = {tracker.input_text}, "
        f"profanity = {profanity}, "
        f"segment = {tracker.returnnlp.text}"
    )

    if profanity and profanity[0] != 0:
        return

    def _search(pattern, name_loc, segments):
        matches = [pattern.search(seg) for seg in segments]
        return [m.group(name_loc) for m in matches if m]

    tracker.volatile_store['check_input_for_name'] = None
    tracker.volatile_store['is_name_common'] = False

    def _save_name(candidates):  # return True on success and False on failure
        names = _search_common_name(tracker, candidates)
        if len(names) > 0:
            tracker.volatile_store['check_input_for_name'] = names[-1]
            tracker.volatile_store['is_name_common'] = True
            return True
        if len(candidates) > 0:  # save the uncommon name but continue searching
            tracker.volatile_store['check_input_for_name'] = candidates[-1]
        return False

    candidates = _search(regexes['call_me'], 1, tracker.returnnlp.text)
    logger.debug(f"check_input_for_name: call_me: {candidates}")
    if _save_name(candidates):
        return

    candidates = _search(regexes['check_name'], 4, tracker.returnnlp.text)
    logger.debug(f"check_input_for_name: check_name: {candidates}")
    if _save_name(candidates):
        return

    candidates = _search(regexes['yes_my_name_is'], 2, tracker.returnnlp.text)
    logger.debug(f"check_input_for_name: yes_my_name_is: {candidates}")
    if _save_name(candidates):
        return

    candidates = [seg for seg in tracker.returnnlp.text if len(seg.split()) <= 2]
    logger.debug(f"check_input_for_name: segmented: {candidates}")
    names = _search_common_name(candidates)
    if len(names) > 0:
        tracker.volatile_store['check_input_for_name'] = names[-1]
        tracker.volatile_store['is_name_common'] = True
        return
    # e.g. when user say "nakamura" which isn't in database but
    # there is only 1 segment so it's likely to be a name
    # also exclue 'yes' and 'no'
    if (
        len(candidates) == 1 and
        not (
            regexes['yes_only'].search(next(iter(candidates), "")) or
            regexes['no_only'].search(next(iter(candidates), "")) or
            regexes['yes_only'].search(tracker.input_text) or
            regexes['no_only'].search(tracker.input_text)
        )

    ):
        tracker.volatile_store['check_input_for_name'] = candidates[0]

    logger.info(
        f"check_input_for_name complete. "
        f"name = {tracker.volatile_store.get('check_input_for_name')}, "
        f"is_name_common={tracker.volatile_store.get('is_name_common')}"
    )
