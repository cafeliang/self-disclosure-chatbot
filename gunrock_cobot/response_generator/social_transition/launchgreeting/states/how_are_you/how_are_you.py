from nlu.constants import DialogAct, Positivity
from question_handling.quetion_handler import QuestionResponse
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.social_transition.launchgreeting import states
from response_generator.social_transition.launchgreeting.states.base import LGState
from response_generator.social_transition.launchgreeting.utils import constants, utils, dynamodb


class HowAreYouPropose(LGState):
    """
    say "how are you doing today"
    """

    name = "how_are_you_propose"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if (
            tracker.persistent_store.get('is_returning_user', False) and
            # bypass this in case no_name_again already said it,
            # happens when returnning user correct their names with an uncommon name
            not any(i in {states.DetectNameRespondNoNameAgain.name} for i in self.module.state_tracker.curr_turn())
        ):
            dispatcher.respond_template('chitchat/returning_ack', {})
        dispatcher.respond_template('chitchat/how_are_you', {})
        return [NextState(states.HowAreYouRespond.name)]


class HowAreYouRespond(LGState):
    """
    respond to user's response to "how are you doing"
    """

    name = "how_are_you_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        positivity = utils.how_are_you_sentiment(tracker)

        self.logger.debug(
            f"positivity = {positivity}, "
            f"question = {self.handler.has_question(tracker)}, "
            f"is_ask_back_question = {self.handler.question_detector.is_ask_back_question()}, "
        )

        """
        check if there are segments that are not questions.
        if the utt is only question, makes no sense to provide positivity ack.
        """
        if next(filter(
            lambda segment: not any([
                segment.has_intent({
                    'ask_back', 'ask_reason', 'ask_follow_up', 'ask_recommend', 'ask_opinion',
                    'ask_preference', 'ask_hobby', 'ask_advice'}),
                segment.has_dialog_act(
                    {DialogAct.OPEN_QUESTION, DialogAct.OPEN_QUESTION_FACTUAL, DialogAct.OPEN_QUESTION_OPINION},
                    threshold=(0.3, 0.1))
            ]),
            tracker.returnnlp
        ), None):
            self.logger.debug(f"positivity_ack = {positivity}")
            try:
                utt, short_utt = dynamodb.match_ack(tracker.input_text)
                self.logger.info(f"<LG MATCH ACK> = {tracker.input_text, utt, short_utt}")
            except Exception as e:
                self.logger.warning(f"e = {e}, text = {tracker.input_text}", exc_info=True)
                utt, short_utt = None, None

            if (
                self.handler.has_question(tracker) or
                constants.regexes['how_are_you'].search(tracker.input_text)
            ):
                """override to shorten normal response if there's a question"""
                if short_utt:
                    dispatcher.respond(short_utt)
                else:
                    dispatcher.respond_template(f'chitchat/followup/{positivity.name}_short', {})
            else:
                if utt:
                    dispatcher.respond(utt)
                else:
                    dispatcher.respond_template(f'chitchat/followup/{positivity.name}', {})

        """
        question handling
        """
        if (
            self.handler.has_question(tracker) or
            constants.regexes['how_are_you'].search(tracker.input_text)
        ):

            def ask_back_handler(qr: QuestionResponse):
                dispatcher.respond_template('chitchat/followup/how_are_you', {})

            events = self.handler.handle_question(
                dispatcher, tracker,
                ask_back_handler=ask_back_handler,
                follow_up_handler=lambda qr: None,
                is_ask_back_question=lambda: (
                    (
                        self.handler.question_detector.is_ask_back_question() or
                        constants.regexes['how_are_you'].search(tracker.input_text)
                    ) and
                    positivity is not Positivity.neg
                )
            )
            if events:
                return events

        """
        Open question propose
        """

        dispatcher.respond("I'm curious,")

        if positivity in {Positivity.neg}:
            dispatcher.respond_template('open_questions/neg', {})
        elif tracker.persistent_store.get('is_returning_user') is True:
            dispatcher.respond_template('open_questions/return_user', {})
        else:
            dispatcher.respond_template('open_questions/general', {})

        tracker.persistent_store['is_first_module'] = False
        dispatcher.propose_continue('STOP')

        return [NextState(states.Initial.name)]


class OpenQuestionFollowup(LGState):

    name = "open_question_followup"

    def get_open_question_pos_ans(self, tracker: Tracker):
        return getattr(tracker.user_attributes.ua_ref, 'open_question_has_pos_ans', None)
        # return self.state_manager.user_attributes.open_question_has_pos_ans

    def set_open_question_pos_ans(self, tracker: Tracker, pos_ans):
        setattr(tracker.user_attributes.ua_ref, 'open_question_has_pos_ans', pos_ans)
        # self.state_manager.user_attributes.open_question_has_pos_ans = pos_ans

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        self.logger.debug(f"openquestion followup: {self.central_element}, {self.returnnlp}")

        tracker.respond_template('open_questions/followup', {})
        if self.get_open_question_pos_ans(tracker) is True:
            self.set_open_question_pos_ans(tracker, False)
        tracker.persistent_store['is_first_module'] = False
        dispatcher.propose_continue('STOP')


class SocialTemplateExit(LGState):

    name = "social_template_exit"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if 'not_my_name_flag' in tracker.persistent_store:
            del tracker.persistent_store['not_my_name_flag']
        social_module, social_utt = utils.get_social_template(dispatcher, tracker)
        # utt += social_utt
        dispatcher.respond(social_utt)
        dispatcher.propose_continue('STOP')
        tracker.user_attributes._set_raw('propose_topic', social_module)
