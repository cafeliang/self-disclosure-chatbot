from nlu.constants import Positivity, TopicModule
from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from template_manager import Template

from response_generator.social_transition.launchgreeting import states
from response_generator.social_transition.launchgreeting.states.base import LGState
from response_generator.social_transition.launchgreeting.utils import constants


class WhatsMyNameRespond(LGState):

    name = 'whats_my_name_respond'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):
        if (
            not tracker.persistent_store.get('is_first_module', False) and
            constants.regexes['whats_my_name'].search(tracker.input_text)
        ):
            return self.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if self.user_model.username:
            key = 'default'
            username = self.user_model.username

        elif self.user_model._username_uncommon:
            if BotResponseProfanityClassifier.is_profane(self.user_model._username_uncommon):
                self.logger.warning(f"username_uncommon \"{self.user_model._username_uncommon}\" is profane")
                key = 'weird_name'  # "this is embarrassing", just assume we forgot even if they didn't tell us
                username = ""
            else:
                key = 'default'
                username = self.user_model._username_uncommon

        else:
            key = 'weird_name'  # "this is embarrassing", just assume we forgot even if they didn't tell us
            username = ""

        tracker.persistent_store['whats_my_name_key'] = key

        dispatcher.respond_template(f'whats_my_name/{key}', {'name': username})

        if key == 'default':
            return [NextState(states.WhatsMyNameCorrectionRespond.name)]
        else:
            return [NextState(states.DetectNameRespond.name)]


class WhatsMyNameCorrectionRespond(LGState):

    name = 'whats_my_name_correction_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if tracker.returnnlp.answer_positivity in {Positivity.pos, Positivity.neu}:
            dispatcher.respond_template('correction/correction_pos', {})
            if tracker.persistent_store.get('is_first_module', True):
                dispatcher.respond("I'm curious,")
                dispatcher.respond_template('open_questions/general', {})
            else:
                dispatcher.respond_template('propose_topic_short/SOCIAL', {}, template=Template.transition)

            tracker.persistent_store['is_first_module'] = False
            dispatcher.propose_continue('STOP')
            return [NextState(states.Initial.name)]

        else:
            dispatcher.respond_template('ask_name/correction', {})
            return [NextState(states.DetectNameRespond.name)]
