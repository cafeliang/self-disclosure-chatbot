import re
from types import MethodType
from typing import TYPE_CHECKING

from nlu.constants import Positivity, TopicModule
from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier

import utils as sys_utils
from user_initializer import UserInitializer
from user_profiler.predictor import GenderPredictor

from response_generator.social_transition.launchgreeting.transitions import conditions

if TYPE_CHECKING:
    from response_generator.social_transition.launchgreeting.fsm import LaunchGreetingFSM


states = [
    'initial', 'debug', 'social_template_exit',
    'greet_new_user', 'greet_returning_user_with_username', 'greet_returning_user_without_username',
    'returning_user_same_user', 'returning_user_different_user',
    'got_name_common_name', 'got_name_uncommon_name',
    'got_name_no_name',
    'got_name_decline',
    'got_name_no_name_again',
    'got_name_uncommon_verify_pos', 'got_name_uncommon_verify_neg',
    'how_are_you', 'how_are_you_followup', 'open_question_followup',
    'followup_ack',
    'not_my_name', 'not_my_name_with_correction',
    'whats_my_name',
    'correction_default_pos', 'correction_default_neg',
    'correction_no_name',
    'question_handler',
    'coronavirus',
    'fallthrough',
    # 'backstory', 'evichat',
]


def get_states(fsm: 'LaunchGreetingFSM'):
    return [
        dict(name=state, on_enter=MethodType(globals().get(f'on_enter_{state}', lambda: None), fsm))
        for state in states
    ]


# MARK: - First round of utterance

def on_enter_greet_new_user(self: 'LaunchGreetingFSM'):
    if self.ua.persistent.get('is_first_module', True):
        self.ua.persistent['is_first_module'] = True
    holiday_utt, holiday = self.holiday_greet(get_any_holiday=False)

    self.respond_template('init/new', {'holiday_greet': holiday_utt})
    self.user_model._username = '<no_name>'


# MARK: - greet_returning_user

def on_enter_greet_returning_user_with_username(self: 'LaunchGreetingFSM'):
    if self.ua.persistent.get('is_first_module', True):
        self.ua.persistent['is_first_module'] = True
    username = self.user_model.username
    holiday_utt, holiday = self.holiday_greet(get_any_holiday=False)
    self.ua.persistent['is_returning_user'] = False

    self.respond_template("init/returning_with_check/with_name",
                          {'name': username, 'holiday_greet': holiday_utt})


def on_enter_greet_returning_user_without_username(self: 'LaunchGreetingFSM'):
    if self.ua.persistent.get('is_first_module', True):
        self.ua.persistent['is_first_module'] = True
    holiday_utt, holiday = self.holiday_greet(get_any_holiday=False)
    self.ua.persistent['returning_user_without_username_flag'] = True
    self.ua.persistent['is_returning_user'] = False
    self.respond_template("init/returning_with_check/without_name", {'holiday_greet': holiday_utt})


def on_enter_returning_user_same_user(self: 'LaunchGreetingFSM'):
    if self.ua.persistent.get('is_first_module', True):
        self.ua.persistent['is_first_module'] = True
    username = self.user_model.username

    self.ua.persistent['is_returning_user'] = True
    self.respond_template("ask_name/got_name", {'name': username})
    # self.ua.volatile['utt'] = utt
    self.next_state()


def on_enter_returning_user_different_user(self: 'LaunchGreetingFSM'):
    if self.ua.persistent.get('is_first_module', True):
        self.ua.persistent['is_first_module'] = True
    user_initializer = UserInitializer(self.state_manager)
    self.logger.info(f"user_attribute being cleared for returning user: "
                     f"{self.state_manager.user_attributes.map_attributes}")
    user_initializer.init_for_returning_user_different_name()
    # self.state_manager.user_attributes.launchgreeting['propose_continue'] = 'CONTINUE'
    self.logger.info(f"user_attribute cleared for returning user: "
                     f"{self.state_manager.user_attributes.map_attributes}")
    self.respond_template(f"init/returning_different_user", {})
    # self.next_state()


# MARK: - second round, after asking for user's name

def on_enter_got_name_common_name(self: 'LaunchGreetingFSM'):
    username = self.ua.volatile['check_input_for_name']
    self.user_model._username = username

    predicted_gender = GenderPredictor().get_gender_prediction(username)
    self.user_model.gender = predicted_gender

    # patch in such that if returning user but without username, we'll count as returning user based on their answer
    if self.ua.persistent.get('returning_user_without_username_flag', False):
        del self.ua.persistent['returning_user_without_username_flag']
        self.ua.persistent['is_returning_user'] = True

    self.respond_template('ask_name/got_name', {'name': username})
    from_question_handler: str = self.ua.volatile.get('from_question_handler')
    if from_question_handler:
        self.respond(from_question_handler)
    # self.ua.volatile['utt'] = utt
    self.next_state()


def on_enter_got_name_uncommon_name(self: 'LaunchGreetingFSM'):
    self.user_model._username = '<no_name>'
    self.user_model._username_uncommon = self.ua.volatile['check_input_for_name']
    # self.ua.persistent['username_uncommon'] = self.ua.volatile['check_input_for_name']
    self.next_state()


def on_enter_got_name_no_name(self: 'LaunchGreetingFSM'):
    self.respond_template('ask_name/yes_only', {})


def on_enter_got_name_decline(self: 'LaunchGreetingFSM'):
    self.user_model._username = '<no_name>'
    self.respond(f"That's ok.")
    if self.is_new_user():
        self.respond_template('exit/no_name', {'my_name': self.my_name})
    # self.ua.volatile['utt'] = utt
    self.next_state()


def on_enter_got_name_no_name_again(self: 'LaunchGreetingFSM'):
    # if self.is_new_user():
    if self.last_fsm_state != 'greet_returning_user_without_username':
        # only if new user, otherwise how_are_you is going to have an ack already
        self.respond_template('exit/no_name', {'my_name': self.my_name})
    from_question_handler: str = self.ua.volatile.get('from_question_handler')
    if from_question_handler:
        self.respond(from_question_handler)
    # self.ua.volatile['utt'] = utt
    self.next_state()


# MARK: - chitchat

def on_enter_how_are_you(self: 'LaunchGreetingFSM'):
    self.logger.debug(f"entering")
    # if not self.is_new_user():
    if self.ua.persistent.get('is_returning_user', False):
        # self.ua.persistent['is_returning_user'] = True
        self.respond_template('chitchat/returning_ack', {})
    self.respond_template('chitchat/how_are_you', {})
    # utt = self.ua.volatile.get('utt', '') + ' '
    # utt += self.tm_greet.speak('chitchat/how_are_you', {})
    # self.response = utt, 'CONTINUE'


def on_enter_how_are_you_followup(self: 'LaunchGreetingFSM'):
    # lex = 'pos' if self.lexicals == 'pos' else 'neg' if self.lexicals == 'neg' else 'neu'

    self.logger.debug(f"how_are_you_followup")

    if sys_utils.get_working_environment() == 'local':
        self.logger.debug("pytest\n{}".format({
            self.input_text: {
                'input': {
                    'central_elem': self.input_data['central_elem'],
                    'returnnlp': self.input_data['returnnlp']
                },
                'output': {
                    'how_are_you_sentiment': self.how_are_you_sentiment().name
                }
            }
        }))

    # if re.search(r"^good$", self.input_text.lower()):
    #     evi_utt = None
    # else:
    #     evi_utt = evi.EVI_bot(self.input_text.lower(), handle_empty_response=False)

    # self.logger.debug(f"how_are_you_followup received evichat: {evi_utt}")

    custom_positivity = self.how_are_you_sentiment()
    self.logger.debug(f"how_are_you_followup custom positivitiy: {custom_positivity}")

    def positivity_ack():
        if custom_positivity in {Positivity.pos}:
            self.logger.debug(f"how_are_you_followup: answer_positivity pos")
            self.respond_template('chitchat/followup/pos', {})

        elif custom_positivity in {Positivity.neg}:
            self.logger.debug(f"how_are_you_followup: answer_positivity neg")
            self.respond_template('chitchat/followup/neg', {})

        else:
            self.logger.debug(f"how_are_you_followup: default")
            self.respond_template('chitchat/followup/neu', {})

    if (
        custom_positivity in {Positivity.pos, Positivity.neu} and
        self.regexes['how_are_you'].search(self.input_text)
    ):
        self.logger.debug(f"how_are_you_followup: matches followup with how are you")

        if (
            custom_positivity in {Positivity.pos} and
            len(re.sub(self.regexes['how_are_you'], '', self.input_text, re.I)) > 3
        ):
            positivity_ack()
        self.respond_template('chitchat/followup/how_are_you', {})

    elif self.question_detector.has_question() and self.question_detector.is_ask_back_question():
        # handle ask back question, aka. how are we doing
        self.respond_template('chitchat/followup/how_are_you', {})

    elif re.search(r"(not (so |that )?(good|great))|bad day|so and so", self.input_text, re.I):
        self.logger.debug(f"how_are_you_followup: matches bad day so and so chitchat")
        self.respond_template('chitchat/followup/neg', {})

    # elif evi_utt is not None:
    #     self.logger.debug(f"how_are_you_followup: adding evi_utt")
    #     self.respond(evi_utt)

    else:
        positivity_ack()

    self.respond("I'm curious,")

    if custom_positivity in {Positivity.neg}:
        self.respond_template('open_questions/neg', {})
    elif self.ua.persistent.get('is_returning_user') is True:
        self.respond_template('open_questions/return_user', {})
    else:
        self.respond_template('open_questions/general', {})


    self.ua.persistent['is_first_module'] = False
    self.propose_continue('STOP')


def on_enter_open_question_followup(self: 'LaunchGreetingFSM'):
    self.logger.debug(f"openquestion followup: {self.central_element}, {self.returnnlp}")

    self.respond_template('open_questions/followup', {})
    if self.open_question_pos_ans is True:
        self.open_question_pos_ans = False
    self.ua.persistent['is_first_module'] = False
    self.propose_continue('STOP')


def on_enter_social_template_exit(self: 'LaunchGreetingFSM'):
    # utt = self.ua.volatile.get('utt', '') + ' '
    if 'not_my_name_flag' in self.ua.persistent:
        del self.ua.persistent['not_my_name_flag']
    social_module, social_utt = self.get_social_template()
    # utt += social_utt
    self.respond(social_utt)
    self.propose_continue('STOP')
    setattr(self.state_manager.user_attributes, 'propose_topic', social_module)


# MARK: - not my name
def on_enter_not_my_name(self: 'LaunchGreetingFSM'):
    self.respond_template('ask_name/correction', {})
    self.ua.persistent['not_my_name_flag'] = True


def on_enter_not_my_name_with_correction(self: 'LaunchGreetingFSM'):
    # first, identify if a new name is uttered
    matches = self.regexes['not_my_name_with_correction'].search(self.input_text)
    matches = [v for k, v in matches.groupdict().items() if k.startswith('nn') and v] if matches else []
    if matches:
        # get match group from regex
        new_name = matches[0]
        self.logger.debug(f"not_my_name_with_correction (match): "
                          f"{matches}, "
                          f"{new_name}")

    elif (
        self.ua.volatile.get('check_input_for_name') and
        self.ua.volatile.get('is_common_name', False) and
        self.ua.volatile.get('check_input_for_name') != self.user_model._username
    ):
        new_name = self.ua.volatile.get('check_input_for_name', '')
        self.logger.debug(f"not_my_name_with_correction (check_input): "
                          f"{self.ua.volatile.get('check_input_for_name')}")
    else:
        self.to_not_my_name()
        return

    # set the new name into user_model
    self.user_model._username = new_name

    # generate response to the new name
    if self.ua.persistent.get('is_first_module', False):
        self.respond_template('correction/lets_start_over', {'new_name': new_name})
        # self.ua.volatile['utt'] = utt
        self.to_how_are_you()
    else:
        social_module, social_utt = self.get_social_template()
        # utt += social_utt
        self.respond(social_utt)
        self.propose_continue('STOP')
        # self.response = utt, 'STOP'
        setattr(self.state_manager.user_attributes, 'propose_topic', social_module)


def on_enter_whats_my_name(self: 'LaunchGreetingFSM'):
    if self.user_model.username:
        key = 'default'
        username = self.user_model.username

    elif self.user_model._username_uncommon:
        if BotResponseProfanityClassifier.is_profane(self.user_model._username_uncommon):
            self.logger.warning(f"username_uncommon \"{self.user_model._username_uncommon}\" is profane")
            key = 'weird_name'  # "this is embarrassing", just assume we forgot even if they didn't tell us
            username = ""
        else:
            key = 'default'
            username = self.user_model._username_uncommon

    else:
        key = 'weird_name'  # "this is embarrassing", just assume we forgot even if they didn't tell us
        username = ""

    self.ua.persistent['whats_my_name_key'] = key

    self.respond_template(f'whats_my_name/{key}', {'name': username})


def on_enter_correction_default_pos(self: 'LaunchGreetingFSM'):
    self.respond_template('correction/correction_pos', {})
    if self.ua.persistent.get('is_first_module', True):
        self.respond("I'm curious,")
        self.respond_template('open_questions/general', {})
    else:
        self.respond_transition_template('propose_topic_short/SOCIAL', {})

    self.ua.persistent['is_first_module'] = False
    self.propose_continue('STOP')


def on_enter_correction_default_neg(self: 'LaunchGreetingFSM'):
    self.respond_template('ask_name/correction', {})


def on_enter_correction_no_name(self: 'LaunchGreetingFSM'):
    self.next_state()


def on_enter_got_name_uncommon_verify_pos(self: 'LaunchGreetingFSM'):
    self.ua.volatile['check_input_for_name'] = self.ua.persistent['username']
    self.next_state()

# MARK: - Question Handler


def on_enter_question_handler(self: 'LaunchGreetingFSM'):

    # MARK: - Question Handler funcs

    def is_unanswerable_question():
        return bool(
            self.question_detector.is_ask_back_question() or
            self.question_detector.is_follow_up_question()
        )

    def handle_topic_specific_question():

        self.logger.debug(
            f"handling specific question. "
            f"last_state={self.last_fsm_state}, "
            "'whats your name' = {}".format(re.search(f"what('s| is) your(s| name)", self.input_text))
        )
        # if "my name is <name> what's your name"
        if (
            self.last_fsm_state == 'greet_new_user' and
            re.search(f"what('s| is) your(s| name)", self.input_text)
        ):
            self.logger.debug(
                f"last_state = {self.last_fsm_state} and match r'whats your name'. "
                f"conditions: {conditions.got_name_common_name(self, True)}, "
                f"{conditions.got_name_uncommon_name(self, True)}"
            )
            self.ua.volatile['from_question_handler'] = self.tm_greet.speak('exception/whats_your_name', {})
            if conditions.got_name_common_name(self, skip_command_question_check=True):
                self.to_got_name_common_name()
                return True
            elif conditions.got_name_uncommon_name(self, skip_command_question_check=True):
                self.to_got_name_uncommon_name()
                return True

    def handle_general_question():
        answer = self.question_handler.handle_question()
        return answer

    # MARK: - Command Detector Logic

    # if self.command_detector.has_command():
    #     self.logger.warning(f"has command, but lg currently don't have fine-grained command detection")
    #     pass

    # MARK: - Question Handler Logic

    self.logger.debug(f"entering question_handler")

    topic_specific = handle_topic_specific_question()
    if topic_specific:
        return

    if is_unanswerable_question():
        self.logger.debug(f"question handler: is unanswerable")
        utt = self.question_handler.generate_i_dont_know_response()
        self.respond(utt)
        self.propose_continue('STOP')
        return

    general_answer = handle_general_question()
    self.logger.debug(f"general answer = {general_answer}")
    if (
        general_answer and general_answer.bot_propose_module and
        general_answer.bot_propose_module is not TopicModule.LAUNCHGREETING
    ):
        self.logger.debug(f"proposed module {general_answer.bot_propose_module}")
        self.respond(general_answer.response)
        self.respond_transition_template(f"propose_topic_short/{general_answer.bot_propose_module.value}", {})
        self.propose_continue('UNCLEAR', general_answer.bot_propose_module)
        return

    elif general_answer and general_answer.response:
        self.logger.debug(f"general question {general_answer.response}")
        self.respond(general_answer.response)
        self.propose_continue('STOP')
        return

    self.logger.debug(f"default")
    utt = self.question_handler.generate_i_dont_know_response()
    self.respond(utt)
    self.propose_continue('STOP')
    return


# coronavirus

def on_enter_coronavirus(self: 'LaunchGreetingFSM'):
    self.logger.debug(f"entering")
    self.respond_template("special/coronavirus/ack/neu", {})
    self.respond_template('open_questions/general', {})

    self.ua.persistent['is_first_module'] = False


def on_enter_fallthrough(self: 'LaunchGreetingFSM'):
    self.logger.warning(f"fsm fellthrough, using how_are_you_followup")
    self.logger.warning(f"last_state: {self.last_state}")
    self.to_how_are_you_followup()


# def on_enter_backstory(self: 'LaunchGreetingFSM'):
#     # utt = utils.get_backstory_response_with_score(
#     #         self.input_data['text'].lower(),
#     #         self.state_manager.user_attributes
#     #     )
#     self.ua.persistent['is_first_module'] = False
#     utt = self.central_elem.backstory.text
#     utt += ' ' + self.tm_greet.speak('open_questions/general', {})
#     p_c = 'STOP'
#     self.response = (utt, p_c)


# def on_enter_evichat(self: 'LaunchGreetingFSM'):
#     self.ua.persistent['is_first_module'] = False
#     utt = evi.EVI_bot(self.input_text.lower(), handle_empty_response=True)
#     self.logger.debug(f"Launchgreeting received evichat: {utt}")
#     p_c = 'STOP'
#     self.response = (utt, p_c)
