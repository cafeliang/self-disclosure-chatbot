from response_generator.fsm2 import State, Dispatcher, Tracker
from user_profiler.user_profile import UserProfile

from response_generator.social_transition.launchgreeting.utils.handler import Handler

# MARK: - Setup


class LGState(State):

    def setup(self, dispatcher: Dispatcher, tracker: Tracker):
        self.user_model = UserProfile(tracker.user_attributes.ua_ref)
        self.handler = Handler(tracker)
