import logging
import os
import sys
import re
import json
import requests

import nlu.util_nlu
from nlu.dataclass import ReturnNLP, CentralElement
from nlu.constants import DialogAct
from nlu.constants import TopicModule
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
import utils
from enum import auto
from typing import List, Dict, Tuple, Optional, Union, Type

import template_manager

from cobot_core.service_module import LocalServiceModule, ToolkitServiceModule
from cobot_common.service_client import get_client
from cobot_common.service_client.exceptions import ParamValidationError, UnknownAPIError
from constants_api_keys import COBOT_API_KEY

from user_profiler.user_profile import UserProfile
from selecting_strategy.topic_module_proposal import add_asked_open_question_type, \
    get_next_available_open_question_type, should_propose_open_question_after_jump_out_from_topic, \
    generate_topic_proposal_response
from selecting_strategy.module_selection import ModuleSelector

logger = logging.getLogger(__name__)

template_social = template_manager.Templates.social
template_topics = template_manager.Templates.topics
template_sys = template_manager.Templates.sys_rg
template_transition = template_manager.Templates.transition

BACKSTORY_THRESHOLD = 0.82

pick_map = {
    'animals': 'ANIMALCHAT',
    'animal': 'ANIMALCHAT',
    'books': 'BOOKCHAT',
    'movies': 'MOVIECHAT',
    'movie': 'MOVIECHAT',
    'games': 'GAMECHAT',
    'game': 'GAMECHAT',
    'sports': 'SPORT',
    'sport': 'SPORT',
    'tech science': 'TECHSCIENCECHAT',
    'music': 'MUSICCHAT',
    'news': 'NEWS',
    'fashion': 'FASHIONCHAT',
    'travel': 'TRAVELCHAT',
}


module_utt_map = {
    'ANIMALCHAT': 'animals',
    'BOOKCHAT': 'books',
    'MOVIECHAT': 'movies',
    'GAMECHAT': 'games',
    'SPORT': 'sports',
    'TECHSCIENCECHAT': 'technology',
    'MUSICCHAT': 'music',
    'NEWS': 'news',
    'TRAVELCHAT': 'travel',
    'FASHIONCHAT': 'fashion',
}

def EVI_bot(utterance):  # TODO: get EVI response
    try:
        client = get_client(api_key=COBOT_API_KEY)
        r = client.get_answer(question=utterance, timeout_in_millis=1000)
        if r["response"] == "" or r["response"].startswith('skill://'):
            return None
        if re.search(r"an opinion on that", r["response"]):
            return "Oh, I haven't thought about that before. "
        return r["response"] + " "
    except Exception as e:
        logger.warning(
            '[SOCIAL] call evi error: {}'.format(e))
        return None



class Social_ResponseGenerator(ToolkitServiceModule):
    """
    Deal with incoming users. Provide a greeting to user.
    Different greeting for first comers and returning users.

    """

    # MARK: - Some properties w/ regarding to user_attributes, since u_a is storing all states

    regex_map = {
        'my_name_is': r".*my name is |.*you can call me |.*call me|.*my name's |.*i'm |.*i am |.*it's|.*this is",
        'yes_only': r"yes|sure|ok|okay|why not|alright|i do",
        'alexa_name': r"what('s| is) your(s| name)|(how|what) (should|can|could|will|would|shall) I call you"
    }

    @property
    def regex(self):
        return Social_ResponseGenerator.regex_map

    @property
    def socialchat(self):
        if not self.state_manager.user_attributes.socialchat:
            self.state_manager.user_attributes.socialchat = {}
        return self.state_manager.user_attributes.socialchat

    @property
    def text(self):
        coreference = nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, "coreference")
        if coreference is not None:
            text = coreference.get("text")
            if text and text != ' ':
                return text.lower()
        return self.input_data['text'].lower()

    @property
    def keyword(self):
        coreference = nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, "coreference")
        if coreference is not None:
            replace = coreference.get("replace")
            if replace is not None:
                for key in replace:
                    if replace[key]:
                        return replace[key][-1]
        return None

    @property
    def asr(self):
        # return utils.get_feature_by_key(self.state_manager.current_state, "asrcorrection")
        return None

    @property
    def lexicals(self):
        custom_intents = nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, "central_elem")["regex"]
        return custom_intents['lexical'] + custom_intents['sys'] + custom_intents['topic']

    @property
    def topic_module(self):
        return nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, 'topic_module')

    @property
    def senti(self):
        sentiment = nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, "central_elem")["senti"]
        return nlu.util_nlu.get_sentiment_key(sentiment, self.lexicals)

    @property
    def DA(self):
        return nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, "central_elem")["DA"]
    
    @property
    def modules(self):
        return nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, "central_elem")["module"]

    @property
    def central_element(self):
        central_e_feat = self.state_manager.current_state.features.get('central_elem', {})
        central_e = CentralElement.from_dict(central_e_feat)
        return central_e

    @property
    def username(self):
        return self.state_manager.user_attributes.usr_name

    @username.setter
    def username(self, username):
        self.state_manager.user_attributes.usr_name = username

    @property
    def state(self):
        state = self.socialchat.get('curr_state')
        if not state:
            state = 's_init'
            self.state = state
        return state

    @state.setter
    def state(self, state: str):
        self.socialchat['curr_state'] = state

    @property
    def backstory(self):
        return self.socialchat.get('backstory')

    @backstory.setter
    def backstory(self, backstory: dict):
        self.socialchat['backstory'] = backstory

    @property
    def asrinfo(self):
        return self.socialchat.get('asrinfo')

    @asrinfo.setter
    def asrinfo(self, asrinfo: dict):
        self.socialchat['asrinfo'] = asrinfo

    @property
    def acknowledgement(self):
        return self.input_data['system_acknowledgement']

    @property
    def a_b_test(self):
        return self.input_data['a_b_test']

    @property
    def returnnlp(self):
        return self.input_data['returnnlp']

    @property
    def module_selector(self):
        return ModuleSelector(self.state_manager.user_attributes)

    @property
    def question_detector(self):
        return QuestionDetector(self.input_data["text"], self.input_data["returnnlp"])

    @property
    def question_handler(self):
        return QuestionHandler(self.input_data["text"],
            self.input_data["returnnlp"],
            BACKSTORY_THRESHOLD,
            self.input_data["system_acknowledgement"],
            self.state_manager.user_attributes)

    @property
    def open_question_pos_ans(self):
        return self.state_manager.user_attributes.open_question_has_pos_ans

    @open_question_pos_ans.setter
    def open_question_pos_ans(self, pos_ans):
        self.state_manager.user_attributes.open_question_has_pos_ans = pos_ans

    @property
    def social_open_question_proposing(self):
        return self.state_manager.user_attributes.social_open_question_proposing

    @social_open_question_proposing.setter
    def social_open_question_proposing(self, proposing):
        self.state_manager.user_attributes.social_open_question_proposing = proposing
    
    @property
    def last_state_open_question(self):
        return self.state_manager.last_state.get("bot_ask_open_question", False)

    @property
    def dominant_turn_ratio(self):
        user_profile = UserProfile(self.state_manager.user_attributes)
        dominant_turn_ratio = user_profile.dominant_turn_ratio
        return dominant_turn_ratio
    
    @property
    def propose_block_module(self):
        propose_block_module = self.socialchat.get('propose_block_module')
        if not propose_block_module:
            propose_block_module = None
        return propose_block_module

    @propose_block_module.setter
    def propose_block_module(self, module: str):
        self.socialchat['propose_block_module'] = module

    def generate_social_template_utt(self, selector, slots=None):
        if slots is None:
            slots = {}
        utt = template_social.utterance(selector=selector, slots=slots,
                                        user_attributes_ref=self.state_manager.user_attributes)
        return utt


    # MARK: - Main RG function

    def execute(self):
        """
        main function that cobot calls
        :return: utterance: str
        """
        logger.info("input_data: {}".format(self.input_data))
        # check for current state:
        if self.state == 's_init':  # init
            utt, next_state, propose_continue = self.s_init()
        elif self.state == 's_question_handling':  # social question handling
            utt, next_state, propose_continue = self.s_question_handling()
        elif self.state == 's_backstory':  # backstory
            utt, next_state, propose_continue = self.s_backstory()
        elif self.state == 's_backstory_reason':  # backstory_reason
            utt, next_state, propose_continue = self.s_backstory_reason()
        elif self.state == 's_propose_topic':  # propose
            utt, next_state, propose_continue = self.s_propose_topic()
        elif self.state == 's_open_question_followup':  # open_question_followup
            utt, next_state, propose_continue = self.s_open_question_followup()
        elif self.state == 's_grounding_topic':  # grounding
            utt, next_state, propose_continue = self.s_grounding_topic()
        else:
            logger.warning('[SOCIAL] getting states that is unknown, state: {}'.format(self.state))
            utt = 'I think my creator wrote something wrong in my code. Could you pick another topic?'
            next_state = 's_init'
            propose_continue = 'STOP'

        # everything's done. update propose_continue and return response
        self.socialchat['propose_continue'] = propose_continue
        # set to init since it shouldn't come back
        self.state = next_state
        return utt

    # init
    # Detect any states?
    def s_init(self):
        ret_nlp_feat = self.state_manager.current_state.features.get('returnnlp', [])
        ret_nlp = ReturnNLP.from_list(ret_nlp_feat)
        if ret_nlp.has_intent(["change_topic", "req_topic"]) or \
                (self.modules and (self.central_element.has_dialog_act(DialogAct.COMMANDS) or
                                   ret_nlp.has_intent("req_topic_jump"))):
            return self.s_propose_topic()
        # open question, user:"how about you"
        elif self.last_state_open_question and "ask_back" in self.lexicals:
            return self.s_propose_topic()
        elif self.open_question_pos_ans is True:
            return self.s_open_question_followup()
        elif "select_wrong" in self.lexicals:
            utt = template_social.utterance(selector='select_wrong', slots={},
                                            user_attributes_ref=self.state_manager.user_attributes)
            next_state = 's_init'
            propose_continue = 'STOP'
            return utt, next_state, propose_continue
        elif 'say_bio' in self.lexicals:
            utt = template_social.utterance(selector='self_intro', slots={},
                                            user_attributes_ref=self.state_manager.user_attributes)
            next_state = 's_init'
            propose_continue = 'STOP'
            return utt, next_state, propose_continue
        elif self.question_detector.has_question():
            return self.s_question_handling()
        #opinion or statement format utterance triggers backstory
        elif self.central_element.backstory and self.central_element.backstory.confidence > BACKSTORY_THRESHOLD:
            return self.s_backstory()
        else:
            return self.s_propose_topic()

    # question_handling
    def s_question_handling(self):
        utt = ""
        answer = self.question_handler.handle_question()
        next_state = 's_init'
        propose_continue = 'UNCLEAR'
        if answer and answer.response:
            logger.info("[SOCIAL] question handling")
            utt = answer.response
            if answer.tag and answer.tag == QuestionResponseTag.IDK.value:
                #propose a module
                module = self.get_propose_module()
                proposal = self.generate_topic_proposal_question(module, ack=False)
                self.module_selector.add_used_topic_module(module)
                self.module_selector.propose_topic = module

                utt = utt + proposal
                next_state = 's_init'
                propose_continue = 'STOP'
            if answer.bot_propose_module and answer.bot_propose_module != TopicModule.SOCIAL:
                propose_module = answer.bot_propose_module.value
                topic_proposal = template_transition.utterance(
                    selector='propose_topic_short/{}'.format(propose_module), slots={},
                    user_attributes_ref=self.state_manager.user_attributes)
                setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
                utt = utt + " " + topic_proposal
                next_state = 's_init'
                propose_continue = 'STOP'
            if answer.backstory_reason:
                self.backstory = {"reason": answer.backstory_reason}
                next_state = 's_backstory_reason'
                propose_continue = 'UNCLEAR'
        else:
            logger.info("[SOCIAL] social triggers state backstory but backstory is none")
            idk = self.question_handler.generate_i_dont_know_response()
            #propose a module
            module = self.get_propose_module()
            proposal = self.generate_topic_proposal_question(module, ack=False)
            self.module_selector.add_used_topic_module(module)
            self.module_selector.propose_topic = module
            utt = idk + proposal
        return utt, next_state, propose_continue

    # state backstory
    def s_backstory(self):
        utt = ""
        backstory = self.central_element.backstory
        next_state = 's_init'
        propose_continue = 'UNCLEAR'
        if backstory and backstory.text:
            logger.info("[SOCIAL] opinion or statement format utterance triggers backstory")
            utt = backstory.text
            if backstory.bot_propose_module and backstory.bot_propose_module != TopicModule.SOCIAL:
                propose_module = backstory.bot_propose_module.value
                topic_proposal = template_transition.utterance(
                    selector='propose_topic_short/{}'.format(propose_module), slots={},
                    user_attributes_ref=self.state_manager.user_attributes)
                setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
                utt = utt + " " + topic_proposal
                next_state = 's_init'
                propose_continue = 'STOP'
            if backstory.reason:
                self.backstory = {"reason": backstory.reason}
                next_state = 's_backstory_reason'
                propose_continue = 'UNCLEAR'
        else:
            logger.info("[SOCIAL] social triggers state backstory but backstory is none")
            return self.s_propose_topic()
        return utt, next_state, propose_continue

    # backstory reason
    def s_backstory_reason(self):
        if utils.check_intersect(['ask_reason'], self.lexicals):
            if self.backstory.get('reason') is not None:
                utt = self.backstory.get('reason')
            else:
                logger.warning("[SOCIAL] no reason found in state backstory reason")
                utt = template_social.utterance(selector='backstory/no_reason', slots={},
                                                user_attributes_ref=self.state_manager.user_attributes)
            propose_continue = 'STOP'
            next_state = 's_init'
            return utt, next_state, propose_continue
        else:
            return self.s_init()

    # propose
    def s_propose_topic(self):
        #add acknowledgement
        sys_acknowledge = self.acknowledgement.get("ack", "")
        module_selector = ModuleSelector(self.state_manager.user_attributes)

        #proposed open question last turn, topic proposal in this turn
        if (self.last_state_open_question or self.social_open_question_proposing):
            module = self.get_propose_module()
            module_selector.add_used_topic_module(module)
            setattr(self.state_manager.user_attributes, "propose_topic", module)
            utt = template_social.utterance(selector='open_question_transition/{}'.format(module),
                                            slots={},
                                            user_attributes_ref=self.state_manager.user_attributes)
            next_state = 's_init'
        #propose an open question
        elif self.should_propose_open_question_after_jump_out_from_topic():
            open_question_template_key = get_next_available_open_question_type(self.state_manager.user_attributes)
            add_asked_open_question_type(self.state_manager.user_attributes, open_question_template_key)
            utt = self.generate_social_template_utt(open_question_template_key)
            next_state = 's_open_question_followup'
        #propose a topic
        else:
            module = None
            if self.state_manager.user_attributes.previous_modules == ['SOCIAL', 'SOCIAL', 'SOCIAL']:
                module = None
            else:
                module = self.get_propose_module()
            if module is not None:
                utt = self.generate_topic_proposal_question(module, ack=True)
                module_selector.add_used_topic_module(module)
                module_selector.propose_topic = module
                sys_acknowledge = ""
            else:
                utt = self.generate_general_open_question()
            next_state = 's_init'
        if 'req_topic' not in self.lexicals:
            utt = sys_acknowledge + utt
        propose_continue = 'STOP'
        return utt, next_state, propose_continue

    def generate_general_open_question(self):
        return template_social.utterance(selector='open_question/backup',
                                      slots={},
                                      user_attributes_ref=self.state_manager.user_attributes)

    def generate_topic_proposal_question(self, module, ack=True):
        return generate_topic_proposal_response(self.state_manager, module, ack=ack)

    def should_propose_open_question_after_jump_out_from_topic(self):
        return should_propose_open_question_after_jump_out_from_topic(self.state_manager.user_attributes, self.a_b_test)

    def get_propose_module(self):
        module_selector = ModuleSelector(self.state_manager.user_attributes)
        if self.propose_block_module:
            module = self.propose_block_module
            self.propose_block_module = None
        else:
            module, _ = module_selector.get_next_modules()[0]

        return module


    #open question followup
    def s_open_question_followup(self):
        utt = ""
        if self.open_question_pos_ans is True:
            utt = template_social.utterance(selector='open_question_follow_up',
                                            slots={},
                                            user_attributes_ref=self.state_manager.user_attributes)
            next_state = 's_init'
            propose_continue = 'STOP'
            self.open_question_pos_ans = False
            return utt, next_state, propose_continue
        else:
            return self.s_init()

    def s_grounding_topic(self):
        last_dialogflow_module = self.state_manager.user_attributes.previous_modules[-1]
        setattr(self.state_manager.user_attributes, 'propose_topic', last_dialogflow_module)
        if module_utt_map.get(self.state_manager.user_attributes.previous_modules[-1]):
            utt = "Do you want to continue our chat on %s?" % (module_utt_map.get(last_dialogflow_module))
        else:
            utt = "Do you want to continue our previous chat?"
        next_state = 's_init'
        propose_continue = 'UNCLEAR'
        return utt, next_state, propose_continue
