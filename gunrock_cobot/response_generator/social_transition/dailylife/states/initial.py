from response_generator.fsm2 import Dispatcher, Tracker, NextState
from .base import DailyLifeState

# social_template.yaml
# test_random.txt
# propose.py

class Initial(DailyLifeState):

    name = 'initial'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        # Clear our persistent store
        tracker.persistent_store["propose_topic"] = None
        return [NextState('ack', jump=True)]
