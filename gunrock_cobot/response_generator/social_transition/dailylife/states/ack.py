from typing import Dict, Pattern
import re
from .base import DailyLifeState
from response_generator.fsm2 import Dispatcher, Tracker, NextState
import logging
from .utils import  *

logger = logging.getLogger("fashion.states")

regexes: Dict[str, Pattern] = {k: re.compile(v, re.I) for k, v in {
            "topic_birth": r"\b(birth|born)\b",
            "topic_social_media": r"\b(social media|social-media|socialmedia)\b",
            "topic_pod_cast": r"\b(podcast|pod cast|podcasts|pod casts)\b",
            "topic_school": r"\bschool\b",
            "topic_sleep": r"\bsleep\b|\bsleeping\b|\bnaps\b",
            "topic_hangout": r"\b(hang|hanging|hang out|hangout) with (my )?friend(s)?\b",
            "topic_play_with_friends": r"\bfriend\b|\bfriends\b|^play$|^playing$",
            "topic_work": r"(\bwork\b|\bworking\b)(?! (on|with))",
            "topic_draw": r"\bdraw\b|\bdrawing\b|\bpaint\b|\bpainting\b|\bart(s)?\b|\bsketch\b|\bsketching\b|\bphotography\b",
            "topic_dance": r"\bdance(s)?\b|\bdancing\b",
            "topic_chores_clean":r"\b(clean|cleaning).*(house|yard|room)\b|\bdo cleaning\b|^cleaning",
            "topic_family_kids":r"\b(with my kids|son)\b",
            "topic_family_parent":r"\b(play with|\btalk to) my (father|dad|mother|mom|daddy|mummy)\b",
            "topic_family_general":r"\bfamily\b",
            "topic_youtube":r"\byoutube\b",
            "topic_tablet_phone":r"\bphone\b|\btablet\b",
            "topic_toys":r"\btoy(s)?\b",
            "topic_my_birthday":r"\bmy birthday\b",
            "topic_talk_to_bot":r"^(talk|talking) (to|with) (you|chatbot)\b",
            "topic_relationship":r"\bgirlfriend\b|\bboyfriend\b|^i (like|love) a (girl|boy)|\brelationship",
            "topic_stuck_home":r"\b(stuck|stucking|stay|staying) (at |in )?(my |the )?(home|house)\b|\bquarantine\b",
            "topic_gardening":r"\bgardening\b|\bgarden\b",
            "topic_climate_change":r"\bclimate change\b",
        }.items()}

class Ack(DailyLifeState):

    name = 'ack'

    def jump_to_friendship(self, dispatcher: Dispatcher, tracker: Tracker):
        if re.search(r"\b(friend|friends|friendship|mate)\b", tracker.input_text.lower()):
            return [NextState('friendship_q1', jump=True)]

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        logger.info("Reached ack state")

        # Check for friendship flow
        friend = self.jump_to_friendship(dispatcher, tracker)

        if friend:
            dispatcher.respond("You guys must have a great time hanging out together!")
            return friend

        # Try to acknowledge from the regex up above
        did_ack = False
        for topic, regex in regexes.items():
            if re.search(regex, tracker.input_text):
                dispatcher.respond_template(topic.replace("topic_", "one_turn_service_acknowledge/"), {})
                did_ack = True
                break

        # Maybe replace this in the future?
        if not did_ack:
            simple_ack(dispatcher, tracker)

        # Propose the next state
        return [NextState('propose', jump=True)]