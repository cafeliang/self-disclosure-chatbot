from typing import Dict, Pattern
import re
from .base import DailyLifeState
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from .utils import  *
from enum import Enum
from nlu.friendship_acitvities_classifier import *
from selecting_strategy.module_selection import ModuleSelector, GlobalState

class CommonSimilarity(Enum):
    unknown=0
    birthday=1
    color=2
    age=3
    humor=4 # we have the same sense of humor
    personality=5 # similar / same personality
    funny=6 # we are both funny
    both_like=7

class FriendshipQ1(DailyLifeState):

    name = 'friendship_q1'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        # Respond with the template
        dispatcher.respond_template("friendship/open_question", {})

        # Set global state to friendship_open_question
        ModuleSelector(tracker.user_attributes.ua_ref).global_state = GlobalState.FRIENDSHIP_OPEN_QUESTION

        # Propose stop
        dispatcher.propose_continue('STOP')

        # Return next state
        return [NextState('friendship_a1', jump=False)]

class FriendshipA1(DailyLifeState):

    name = 'friendship_a1'

    def detect_common_activities(self, dispatcher: Dispatcher, tracker: Tracker):
        return FriendshipActivityClassifier.classify(tracker.input_text.lower())

    def ack_activity(self, activity: CommonActivity):
        if activity == CommonActivity.movie: return "Watching movies sounds like a lot of fun!"
        if activity == CommonActivity.tv: return "Watching tv sounds like a lot of fun!"
        if activity == CommonActivity.walk: return "Going for walks sounds very relaxing."
        if activity == CommonActivity.games: return "Playing games sounds like a lot of fun!"
        if activity == CommonActivity.mall: return "Going to the mall sounds like a lot of fun!"
        if activity == CommonActivity.play: return "Playing with your friends sounds like a lot of fun!"
        if activity == CommonActivity.talk: return "Yeah, talking with your friends is the best."
        if activity == CommonActivity.hangout: return "Just hanging out sounds like a good time."
        if activity == CommonActivity.netflix: return "Watching Netflix can be a really nice time."
        if activity == CommonActivity.sport: return "Playing sports is always a lot of fun."
        if activity == CommonActivity.shopping: return "Shopping sounds like a lot of fun."
        if activity == CommonActivity.photography: return "Taking photos is always a lot of fun."
        if activity == CommonActivity.singing: return "Cool! I've always been a bit shy to sing in front of others."
        if activity == CommonActivity.facetime: return "Facetime is a great way to keep in touch with friends!"
        if activity == CommonActivity.dancing: return "Dancing sounds like a lot of fun! You must be very talented to be able to dance."
        return "Nice!"

    def get_next_state(self, activity,  dispatcher, tracker):
        if activity == CommonActivity.mall or activity == CommonActivity.shopping: tracker.persistent_store["propose_topic"] = "fashion_style"
        if activity == CommonActivity.sport: tracker.persistent_store["propose_topic"] = "sport"
        if activity == CommonActivity.games: tracker.persistent_store["propose_topic"] = "game"
        if activity == CommonActivity.movie: tracker.persistent_store["propose_topic"] = "movie"
        if "propose_topic" in tracker.persistent_store: return [NextState('propose', jump=True)]

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        act = self.detect_common_activities(dispatcher, tracker)
        if act != CommonActivity.unknown:
            utt = self.ack_activity(act)
            dispatcher.respond(utt)
            next = self.get_next_state(act, dispatcher, tracker)
            if next: return next
        else:
            simple_ack(dispatcher, tracker)
        return [NextState('friendship_q2', jump=True)]

class FriendshipQ2(DailyLifeState):

    name = 'friendship_q2'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond("I heard that friends usually have something in common. Do you share any similarities with your friends?")
        return [NextState('friendship_a2', jump=False)]

class FriendshipA2(DailyLifeState):

    name = 'friendship_a2'

    def detect_similarity(self, dispatcher: Dispatcher, tracker: Tracker):
        if re.search(r"\b(birthday|born)\b", tracker.input_text.lower()): return CommonSimilarity.birthday
        if re.search(r"\b(color|colour)\b", tracker.input_text.lower()): return CommonSimilarity.color
        if re.search(r"\b(age)\b", tracker.input_text.lower()): return CommonSimilarity.age
        if re.search(r"\b(humor)\b", tracker.input_text.lower()): return CommonSimilarity.humor
        if re.search(r"\b(personality|like the same things)\b", tracker.input_text.lower()): return CommonSimilarity.personality
        if re.search(r"\b(funny)\b", tracker.input_text.lower()): return CommonSimilarity.funny
        if re.search(r"(.*)(we|both)(.*)(like|enjoy|love)(.*)", tracker.input_text.lower()): return CommonSimilarity.both_like
        return CommonSimilarity.unknown

    def ack_similarity(self, activity: CommonActivity):
        if activity == CommonSimilarity.birthday: return "Wow, what a coincidence!"
        if activity == CommonSimilarity.color: return "That's cool that you guys both like the same color!"
        if activity == CommonSimilarity.age: return "Wow, what a coincidence!"
        if activity == CommonSimilarity.humor: return "You must get along really well since you guys share the same sense of humor!"
        if activity == CommonSimilarity.personality: return "It sounds like you guys were meant to be best friends since you have similar personalities!"
        if activity == CommonSimilarity.funny: return "You're both funny? That's nice!"
        if activity == CommonSimilarity.both_like: return "That's nice that you guys both like the same thing!"
        return "Nice!"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        act = self.detect_similarity(dispatcher, tracker)
        if act != CommonSimilarity.unknown:
            utt = self.ack_similarity(act)
            dispatcher.respond(utt)
        else:
            simple_ack(dispatcher, tracker)

        if gender(tracker) == 'female': return [NextState('friendship_q3_female', jump=True)]
        return [NextState('friendship_q3_general', jump=True)]

class FriendshipQ3Female(DailyLifeState):

    name = 'friendship_q3_female'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond("I'm wondering, are there any qualities you admire about your friend?")
        return [NextState('friendship_a3_female', jump=False)]

class FriendshipA3Female(DailyLifeState):

    name = 'friendship_a3_female'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if "funny" in tracker.input_text.lower():
            dispatcher.respond("Being funny is a great quality in a friend! By the way, ")
        else:
            simple_ack(dispatcher, tracker)
        tracker.persistent_store["propose_topic"] = "fashion"
        return [NextState('propose', jump=True)]

class FriendshipQ3General(DailyLifeState):

    name = 'friendship_q3_general'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond("I've learned people become good friends through shared experience. Do you have any memorable moments you've shared with your friend?")
        return [NextState('friendship_a3_general', jump=False)]

class FriendshipA3General(DailyLifeState):

    name = 'friendship_a3_general'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        simple_ack(dispatcher, tracker)
        tracker.persistent_store["propose_topic"] = "movie"
        return [NextState('propose', jump=True)]