from cobot_core.service_module import LocalServiceModule

from response_generator.fsm2 import FSMModule, FSMAttributeAdaptor
from template_manager import Template

from .states.base import DailyLifeState


class DailyLifeResponseGenerator(LocalServiceModule):

    def execute(self):
        module = FSMModule(
            FSMAttributeAdaptor('dailylife', self.state_manager.user_attributes),
            self.input_data,
            Template.social,
            DailyLifeState,
            first_state='initial',
            state_manager_last_state=self.state_manager.last_state
        )
        return module.generate_response()
