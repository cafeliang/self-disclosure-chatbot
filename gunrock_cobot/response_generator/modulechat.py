import logging
from enum import Enum, auto
from typing import List, Union

import json
import hashlib
import requests

import nlu.util_nlu
import redis

from transitions import Machine
from cobot_common.service_client import get_client

import template_manager
import utils


logger = logging.getLogger(__name__)

from constants_api_keys import COBOT_API_KEY

class DA(Enum):
    def _generate_next_value_(name, start, count, last_values):
        if name == 'back_channeling':
            return 'back-channeling'
        return name

    open_question_factual = auto()
    open_question_opinion = auto()
    yes_no_question = auto()
    pos_answer = auto()
    neg_answer = auto()
    other_answers = auto()
    statement = auto()
    opinion = auto()
    comment = auto()
    command = auto()
    back_channeling = auto()
    appreciation = auto()
    abandon = auto()
    opening = auto()
    closing = auto()
    hold = auto()
    complaint = auto()
    thanking = auto()
    apology = auto()
    respond_to_apology = auto()
    nonsense = auto()
    other = auto()
    open_question = auto()

    questions_any = [open_question, open_question_factual,
                     open_question_opinion, yes_no_question]


class RGMachine(Machine):

    @property
    def modulechat(self):
        if not getattr(self.user_attributes, self.modulechat_name):
            setattr(self.user_attributes, self.modulechat_name, {})
        return getattr(self.user_attributes, self.modulechat_name)

    @property
    def persistent(self):
        if not self.modulechat.get('persistent_store'):
            self.modulechat['persistent_store'] = {}
        return self.modulechat['persistent_store']

    @property
    def input_text(self):
        return self.input_data['text'].lower()

    @property
    def username(self):
        return self.user_attributes.usr_name

    # @property
    def lexicals(self):
        custom_intents = nlu.util_nlu.get_feature_by_key(
            self.current_state, "intent_classify")
        return custom_intents['lexical']

    @property
    def dialog_act(self):
        return nlu.util_nlu.get_feature_by_key(self.current_state, 'dialog_act')

    @property
    def knowledge(self):
        return nlu.util_nlu.get_feature_by_key(self.current_state, 'knowledge')

    def dialog_act_has(self, da: Union[DA, List[DA]]):
        # print('[MODULECHAT]', da)
        if isinstance(da, DA):
            da = [da]
        r = any(any(d.get('DA') == a.value for a in da)
                for d in self.dialog_act)
        # print('[MODULECHAT]', r)
        return r

    def __init__(self, user_attributes, current_state, input_data,
                 template, states, modulechat_name):
        self.tm = template_manager.TemplateManager(template, user_attributes)
        self.qa = QABot(modulechat_name)

        self.user_attributes = user_attributes
        self.current_state = current_state
        self.input_data = input_data

        self.modulechat_name = modulechat_name

        self._response = (None, None)
        self.ua.volatile = {}

        Machine.__init__(self, states=states, initial=self.modulechat.get('states', {}).get('state') or 'initial',
                         after_state_change='machine_clean_up')

        # MARK: - Add transitions

    def response(self, utt: str, p_c: str, reset=False):
        self._response = (self._response[0] +
                          ' ' if self._response and
                          self._response[0] and
                          not reset else '') + utt, p_c

    def respond(self):
        try:
            self.next_state()
            logger.info("[{}] Entered state: {}".format(
                self.modulechat_name, self.state))
            if not all(self._response):
                raise ValueError('response not set. state: {}, resp: {}, locals: {}, {}, {}'.format(
                    self.state, self._response, self.modulechat, self.ua.volatile, self.persistent))

            self.modulechat['propose_continue'] = self._response[1]
            self.persistent['prev_utt'] = self._response[0]
            return self._response[0]
        except Exception as e:
            logger.error('[MODULECHAT] ERROR [{}]: {}'.format(
                self.modulechat_name.upper(), e), exc_info=True)
            self.modulechat.clear()

    def machine_clean_up(self):
        if 'states' not in self.modulechat:
            self.modulechat['states'] = {}
        self.modulechat['states'].update({'state': self.state})


class QABot:
    def __init__(self, module: str):
        self.module = module

        self.COBOT_API_KEY = COBOT_API_KEY
        self.EVI_KEY = "gunrock:{}:evi".format(self.module)

        self.rds = redis.StrictRedis(host='language.cs.ucdavis.edu', port=5012,
                                     socket_timeout=3,
                                     socket_connect_timeout=1,
                                     retry_on_timeout=True,
                                     db=0, password="alexaprize", decode_responses=True)

        self.evi_filter_list = {
            "Sorry, I can’t find the answer to the question I heard.",
            "I don't have an opinion on that.",
            "That's tough to explain.",
            "You could ask me about music or geography.",
            "You can ask me anything you like.",
            "I can answer questions about people, places and more.",
        }

    def get_answer(self, utterance: str):
        response = None
        back_response, confidence = self.get_backstory_response(utterance)
        if confidence < 0.9:
            response = self.get_evi_response(utterance)
            if not response and confidence > 0.65:
                response = back_response
        if not response:
            response = "Hmm, I've not thought about that before."
        return response

    def get_backstory_response(self, utterance):
        try:
            headers = {
                'Content-Type': 'application/json',
            }
            data = {
                'text': utterance
            }
            resp = requests.post(
                'http://ec2-54-175-156-102.compute-1.amazonaws.com:8085/module',
                headers=headers,
                data=json.dumps(data),
                timeout=.5)
            ret = resp.json()
            if resp.status_code < 500:
                if ret['text']:
                    return ret['text'], ret['confidence']
                else:
                    return None
        except Exception as e:
            print(
                '[{}_qa.get_backstory_response] warning: \
                post sentence embedding backstory with data err: {}'
                .format(self.module, e))
        return None

    def get_evi_response(self, utterance):
        evi_cache_res = self.get_redis(self.EVI_KEY, utterance.lower())
        if evi_cache_res is not None:
            return evi_cache_res
        try:
            client = get_client(api_key=self.COBOT_API_KEY)
            r = client.get_answer(question=utterance, timeout_in_millis=1000)
            if (r["response"] == "" or
                    r["response"].startswith('skill://') or
                    r["response"] in self.evi_filter_list):
                return None
            res = r["response"] + " "
            self.set_redis_with_expire(self.EVI_KEY, utterance.lower(), res,
                                       expire=3 * 30 * 86400)
            return res
        except Exception as e:
            print("[{}_qa.get_evi_response] Exception: {}".format(
                repr(self.module, e)))
            return None

    def set_redis_with_expire(self, prefix, input_str, output, expire=24 * 60 * 60):
        logging.info(
            '[REDIS] set redis with expire input: {} output: {}, expire {}'.format(
                input_str, output, expire))
        hinput = hashlib.md5(input_str.encode()).hexdigest()
        return self.rds.set(prefix + ':' + hinput, json.dumps(output),
                            nx=True, ex=expire)

    def get_redis(self, prefix, input_str):
        logging.info('[REDIS] get redis  input: {}'.format(input_str))
        hinput = hashlib.md5(input_str.encode()).hexdigest()
        results = self.rds.get(prefix + ':' + hinput)
        if results is None:
            return None
        return json.loads(results)
