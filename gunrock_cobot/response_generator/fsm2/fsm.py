import re
from typing import Any, Dict, List, Optional, Set, Type, Union

from template_manager import Template
from user_profiler.user_profile import UserProfile

from . import exceptions
from .events import NextState, PreviousState
from .state import State
from .utils.attribute_adaptor import FSMAttributeAdaptor
from .utils.dispatcher import Dispatcher
from .utils.logger import FSMLogger
from .utils.state_tracker import StateTracker
from .utils.tracker import Tracker


logger = FSMLogger('FSM2', 'FSMModule', 'fsm.fsm')
pytest_logger = FSMLogger('FSM2', '__pytest__', 'fsm.pytest')


class FSMModule:

    def __init__(
        self,
        user_attribute_adaptor: Type[FSMAttributeAdaptor],
        input_data: Dict[str, Any],
        template: 'Template',
        state_class: Type['State'],
        *,
        first_state: str = 'init',
        state_manager_last_state: Dict[str, Any] = None
    ):
        self.ua = user_attribute_adaptor
        self.input_data = input_data
        self.state_manager_last_state = state_manager_last_state

        self.template = template
        self.state_class = state_class
        self.state_tracker = StateTracker(self.ua.state_history or [[first_state]])

        pytest_logger.debug("\n{}".format(dict(
            input_data=input_data,
            user_attributes=user_attribute_adaptor,
            last_state=state_manager_last_state,
        )))

    def generate_response(self):
        dispatcher = Dispatcher(self.ua, self.template)
        tracker = Tracker(self.input_data, self.ua, self.state_manager_last_state)

        logger.info(
            f"transition begin. Current Turn: {self.state_tracker.curr_turn()}, {self.state_tracker.data}")

        # first check global states
        self._check_global_states_and_run(dispatcher, tracker, special_type={'global_state'})

        # running recursive state transitions
        self._transition(dispatcher, tracker)

        # setting first turn flag after first turn is complete. All remaining turn should have first turn now set.
        user_profile = UserProfile(self.ua.ua_ref)
        first_turn = tracker.persistent_store.get(f"__first__")
        if not first_turn or first_turn != user_profile.visit:
            tracker.persistent_store['__first__'] = user_profile.visit or 1

        # commiting tracker
        tracker.commit()
        self.commit()
        self.state_tracker.transition()

        # post-process
        self._post_process_hook(dispatcher, tracker)

        # finalize
        logger.debug(f"Cycle complete.")
        dispatcher._commit_propose_continue()
        response = " ".join(dispatcher.responses) or " "

        return re.sub(r" \. ", " ", response)

    def commit(self):
        self.ua.state_history = self.state_tracker.json()

    def _proposed_states_from_enter_condition(
            self,
            dispatcher: Dispatcher, tracker: Tracker,
            special_type: Set[str]) -> Optional['State']:

        global_states = self.state_class.subclasses(special_type=special_type)  # type: List[State]
        for global_state in global_states:
            logger.debug(f"checking global state: {global_state.name}")
            global_state = global_state(self)  # type: State
            global_state.setup(dispatcher, tracker)
            proposed_state = global_state.enter_condition(dispatcher, tracker)
            logger.debug(f"global state '{global_state.name}' proposed proposed_state: {proposed_state}")

            if proposed_state:
                yield proposed_state

    def _check_global_states_and_run(
            self,
            dispatcher: Dispatcher, tracker: Tracker, special_type: Set[str]):

        logger.debug(f"transition checking special states.")
        proposed_state = next(self._proposed_states_from_enter_condition(dispatcher, tracker, special_type), None)
        if proposed_state:
            logger.debug(f"global state selected: '{proposed_state}'")
            self.state_tracker.curr_turn().append(proposed_state)

    def _post_process_hook(
        self,
        dispatcher: Dispatcher, tracker: Tracker
    ):
        logger.debug(f"checking post_process_hooks")

        states = self.state_class.subclasses(special_type={'post_process'})  # List[State]
        for state in states:
            logger.debug(f"post_processing state = {state.name}")
            state = state(self)
            state.setup(dispatcher, tracker)
            state.post_process(dispatcher, tracker)

        logger.info(
            f"post_process run complete.\n"
            "Tracker:\t{}".format(dict(
                volatile=tracker.volatile_store, one_turn=tracker.one_turn_store,
                last_utt=tracker.last_utt_store, persistent=tracker.persistent_store)))

    def _transition(self, dispatcher: Dispatcher, tracker: Tracker):

        logger.debug(f"states = {self.state_tracker.data}")

        # begin state running
        curr_state = self.state_class.from_name(self.state_tracker.curr_turn().curr_state())
        if not curr_state:
            raise exceptions.FSMStateInitError(self.state_tracker.curr_turn().curr_state(), self.state_class)
        curr_state = curr_state(self)
        curr_state.setup(dispatcher, tracker)

        logger.debug(
            f"Running state... \n"
            f"curr_state:\t{curr_state.name}\n"
            "tracker:\t{}".format(dict(
                volatile=tracker.volatile_store, last_utt=tracker.last_utt_store,
                persistent=tracker.persistent_store)))

        events = curr_state.run(dispatcher, tracker)
        if not events:
            raise exceptions.FSMTransitionEventError(curr_state.name, events)

        logger.info(
            f"State run complete.\n"
            f"State:\t{curr_state.name}\n"
            f"Response:\t{dispatcher.responses}\n"
            f"Propose_continue:\t{dispatcher._propose_continue}\n"
            f"History:\t{dispatcher.tm.user_attributes.get('selector_history')}\n"
            f"Returned events: {events}\n"
            "Tracker:\t{}".format(dict(
                volatile=tracker.volatile_store, one_turn=tracker.one_turn_store,
                last_utt=tracker.last_utt_store, persistent=tracker.persistent_store)))

        # obtaining next state
        next_state: Union[NextState, PreviousState] = next(
            (event for event in events if isinstance(event, (NextState, PreviousState))), None)
        if not next_state:
            raise exceptions.FSMTransitionEventError(curr_state.name, events)
        logger.debug(f"next_state: {next_state}")

        # recursively call next state or save to next turn
        if isinstance(next_state, NextState) and next_state.jump is True:
            logger.debug(f"Jumping to next state: {next_state.state}")
            self.state_tracker.curr_turn().append(next_state.state)
            self._transition(dispatcher, tracker)

        elif isinstance(next_state, PreviousState):
            prev_state = self.state_tracker.curr_turn().prev_state()
            logger.debug(f"Jumping to previous state: {prev_state}")
            self.state_tracker.curr_turn().append(prev_state)
            if next_state.jump is True:
                self._transition(dispatcher, tracker)

        else:
            logger.debug(f"Appending next state: {next_state.state}")
            self.state_tracker.next_turn().append(next_state.state)
