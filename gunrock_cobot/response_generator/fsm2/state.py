from typing import Iterator, List, Optional, Set, Type, TYPE_CHECKING

from .utils.logger import FSMLogger

if TYPE_CHECKING:
    from .fsm import FSMModule
    from .utils import Dispatcher, Tracker


class State:
    @classmethod
    def subclass(cls, name: str) -> 'State':
        return cls.from_name(name)

    @classmethod
    def from_name(cls, name: str) -> Optional[Type['State']]:
        """
        Get a State class from its name.
        """

        state = next(cls.subclasses(name=name), None)
        return state

    @classmethod
    def subclasses(cls, *, name: str = None, special_type: Set[str] = None) -> Iterator[Type['State']]:
        states = (subclass for subclass in cls.__subclasses__())
        if name:
            states = filter(lambda s: s.name == name, states)
        elif special_type and all(isinstance(i, str) for i in special_type):

            def filter_condition(state: Type['State']):
                lhs = state.special_type
                # FSMLogger('FSM2', 'states', 'fsm').debug(f"filter condition: {state.name}, {lhs}, {special_type}")
                if isinstance(lhs, set):
                    pass
                elif isinstance(lhs, str):
                    lhs = {lhs}
                elif isinstance(lhs, list):
                    lhs = set(lhs)
                else:
                    return False
                # FSMLogger('FSM2', 'states', 'fsm').debug(f"filter condition, result: {lhs & special_type}")
                return lhs & special_type

            states = filter(filter_condition, states)

        return states

    @classmethod
    def has_subclass(cls, name: str) -> bool:
        return cls.from_name(name) is not None

    def __init__(self, module: 'FSMModule'):
        self.module = module
        self.logger = FSMLogger(self.module.template.name.upper(), self.name, 'fsm.state')

    name: str = NotImplementedError
    special_type: Set[str] = None

    # MARK: - Main driver methods

    @classmethod
    def require_entity(cls, *args, **kwargs):
        return

    @classmethod
    def accept_transition(cls, *args, **kwargs):
        return

    def setup(self, dispatcher: 'Dispatcher', tracker: 'Tracker'):
        return

    def enter_condition(self, dispatcher: 'Dispatcher', tracker: 'Tracker') -> Optional[str]:
        return

    def post_process(self, dispatcher: 'Dispatcher', tracker: 'Tracker'):
        return

    def run(self, dispatcher: 'Dispatcher', tracker: 'Tracker') -> List[Type['Event']]:
        return NotImplementedError
