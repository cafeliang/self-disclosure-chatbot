import logging


class FSMLogger(logging.LoggerAdapter):
    def __init__(self, prefix: str, subprefix: str, logger_module):
        logger = logging.getLogger(logger_module)
        super(FSMLogger, self).__init__(logger, {})
        self.prefix = prefix
        self.subprefix = subprefix

    def process(self, msg, kwargs):
        return f"[{self.prefix}] <{self.subprefix}>: {msg}", kwargs
