from typing import Dict, Optional

from nlu.constants import TopicModule
from template_manager import Template, TemplateManager

from .logger import FSMLogger
from .attribute_adaptor import FSMAttributeAdaptor


logger = FSMLogger('FSM2', 'DISPATCHER', 'fsm.dispatcher')


class Dispatcher:

    def __init__(
        self,
        ua_adaptor: FSMAttributeAdaptor,
        template: Template
    ):
        self.ua = ua_adaptor
        if not self.ua.has_required_key(['template_manager', 'propose_topic']):
            logger.error(f"missing key in user_attributes_adaptor. Running in volatile mode as fallback.")
            self._volatile_mode = True
        else:
            self._volatile_mode = False

        self.tm = TemplateManager(
            template, dict(template_manager={}) if self._volatile_mode else self.ua.ua_ref)

        self.responses = []
        self._propose_continue = 'CONTINUE'
        self._propose_topic: Optional[TopicModule] = None

    def respond(self, text: str):
        self.responses.append(text)

    def respond_template(
        self, selector: str, slots: Dict[str, str], embedded_info: dict = None, *,
        template: Template = None,
        postfix: str = None
    ):
        logger.debug(f"responding: selector = {selector}")
        if template:
            tm = TemplateManager(
                template, dict(template_manager={}) if self._volatile_mode else self.ua.ua_ref)

        else:
            tm = self.tm
        try:
            utt = tm.speak(selector, slots, embedded_info)
        except (KeyError, ValueError) as e:
            logger.warning(f"respond_template failed: {e}")
        else:
            self.respond(utt)
            if postfix and isinstance(postfix, str):
                self.respond(postfix)

    def respond_error(self, selector: str, slots: Dict[str, str] = {}, embedded_info: dict = None):
        self.respond_template(f"error/{selector}", slots, embedded_info)

    def respond_remove(self, last: int):
        if len(self.responses) >= last:
            del self.responses[-last:]

    def propose_continue(self, state: str, topic: TopicModule = None):
        logger.debug(f"propose_continue called: state = {state}, topic = {topic}", exc_info=True)
        if state not in {'CONTINUE', 'STOP', 'UNCLEAR'}:
            raise ValueError(f"Dispatcher received invalid propose_continue ({state})")
        self._propose_continue = state
        self._propose_topic = topic

    def _commit_propose_continue(self):
        self.ua.propose_continue = self._propose_continue
        if self._propose_topic:
            if not self._volatile_mode:
                self.ua.propose_topic = self._propose_topic.value

    def expect_opinion(self, expectation=False):
        self.ua.expect_opinion = expectation
