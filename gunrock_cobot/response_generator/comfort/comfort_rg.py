from typing import TYPE_CHECKING

from template_manager import Template
from response_generator.fsm.utils import FSMAttributeAdaptor, FSMModule

from .states import ComfortState

if TYPE_CHECKING:
    from cobot_core.service_module import LocalServiceModule


class ComfortUserAttributes(FSMAttributeAdaptor):

    @property
    def module_name(self) -> str:
        return "comfortchat"


class ComfortModule(FSMModule):

    def __init__(self, response_generator_ref: 'LocalServiceModule'):
        super().__init__(response_generator_ref,
                         Template.comfort,
                         ComfortUserAttributes,
                         ComfortState)
