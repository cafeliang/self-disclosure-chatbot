from cobot_core.service_module import LocalServiceModule
from response_generator.comfort import comfort_rg
import logging

logger = logging.getLogger(__name__)


class ComfortResponseGeneratorLocal(LocalServiceModule):

    def execute(self):
        module = comfort_rg.ComfortModule(self)
        return module.generate_response()
