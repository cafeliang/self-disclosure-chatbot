import logging
import random
import re
from typing import List, TYPE_CHECKING, Optional

import nlu.utils.topic_module
from nlu.intent_classifier import IntentClassifier
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse
from response_generator.fsm.events import Event, NextState, PreviousState
from response_generator.fsm.utils import Dispatcher, Tracker
from template_manager import Template
from template_manager import error as tm_error
from utils import get_working_environment
from nlu.constants import Positivity, TopicModule
from nlu.constants import DialogAct as DialogActEnum
from nlu.intentmap_scheme import lexical_re_patterns

from .. import utils as cutils

from .base import ComfortState

logger = logging.getLogger("comfort.states")

def get_question_for_bot(tracker):
    regexAskBotSad = r"\byou\b.*(\bsad\b|\bupset\b|\bcry\b)"
    regexAskBotMad = r"\byou\b.*(\bmad\b|\bangry\b|\bpissed off\b|\bfrustrated\b)"
    regexAskBotLonely = r"\byou\b.*(\blonely\b|\bfeel alone\b|\bleft out\b)"
    regexAskBotStress = r"\byou\b.*(\bstressed\b|\boverwhelmed\b)"

    regexAskSuicide = r"\bi\b((?!(\bdon't\b|\bdo not\b)).)*(\bdie\b|\bkill myself\b|\bsuicide\b)"

    regexCheckAskBotSad = re.compile(regexAskBotSad)
    regexCheckAskBotMad = re.compile(regexAskBotMad)
    regexCheckAskBotLonely = re.compile(regexAskBotLonely)
    regexCheckAskBotStress = re.compile(regexAskBotStress)

    regexCheckAskSuicide = re.compile(regexAskSuicide)

    if regexCheckAskBotSad.search(tracker.input_text) is not None:
        return "bot_sad"
    elif regexCheckAskBotMad.search(tracker.input_text) is not None:
        return "bot_mad"
    elif regexCheckAskBotLonely.search(tracker.input_text) is not None:
        return "bot_lonely"
    elif regexCheckAskBotStress.search(tracker.input_text) is not None:
        return "bot_stress"
    else:
        None


class QuestionBot(ComfortState):
    name = 'question_bot'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:

        talk_limit = tracker.last_utt_store.get('talk_limit')
        if talk_limit is None:
            talk_limit = 1

        getTemplate = "question/" + get_question_for_bot(tracker)
        dispatcher.respond_template(getTemplate, {})

        if talk_limit % 2 == 0:
            tracker.one_turn_store['talk_limit'] = talk_limit + 1
        else:
            tracker.one_turn_store['talk_limit'] = talk_limit

        return [NextState('question_to_talk')]

#TODO: add in detection for negative for both classes below
class ListenMore(ComfortState):
    name = 'listen_more'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:

        dispatcher.respond("If you want to keep talking you can! I always want to hear what you have to say!")
        tracker.one_turn_store['talk_limit'] = 1

        return [NextState('talking_about_feelings')]

class BotShareOpinion(ComfortState):
    name = 'bot_share_opinion'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:

        dispatcher.respond("Personally I don't have much to share since I'm just a bot, but thank you for asking me! I'm always here to listen!")
        tracker.one_turn_store['talk_limit'] = 1

        return [NextState('talking_about_feelings')]


class BotShouldCheerUp(ComfortState):
    name = 'bot_should_cheer_up'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:

        dispatcher.respond("How can I cheer you up? Would you like to hear a corny joke?")
        tracker.one_turn_store['joke_limit'] = 0
        return [NextState('joke')]


#TODO: Need to implement
class QuestionSuicide(ComfortState):
    name = 'question_suicide'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:
        dispatcher.respond_template("question/suicide", {})
        dispatcher.respond_template("exit/here_for_you_transition", {})
        dispatcher.propose_continue("STOP")
        return [NextState('init')]


class QuestionHandlerRespond(ComfortState):
    name = 'question_handler'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        self.question_detector = QuestionDetector(
            tracker.input_text, tracker.module.response_generator_ref.input_data['returnnlp'])

        #TODO: Might need to implement this part later down the line
        regexForTopic = r"sure why not|^((?!(\bshould\b|\bwhy\b)).)*\bi\b((?!(\bshould\b|\bwhy\b)).)*(\bfeel\b|\bfeeling\b)|can.*keep talking"
        regexCheckTopic = re.compile(regexForTopic)

        logger.debug(f"[COMFORT] Entering question handler check")

        if regexCheckTopic.search(tracker.input_text) is not None:
            logger.debug(f"[COMFORT] going back into states")
            return None
        if self.question_detector.has_question():
            logger.debug(f"[COMFORT] question_handler is entering")
            return self.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        self.question_detector = QuestionDetector(
            tracker.input_text, tracker.module.response_generator_ref.input_data['returnnlp'])
        self.question_handler = QuestionHandler(
            tracker.input_text, tracker.module.response_generator_ref.input_data['returnnlp'],
            backstory_threshold=0.82,
            system_ack=tracker.module.response_generator_ref.input_data['system_acknowledgement'],
            user_attributes_ref=tracker.user_attributes.ua_ref
        )

        #This is how to get previous state
        logger.debug("[COMFORT] state tracker: " + str(tracker.state_tracker.curr_turn()[0]))
        logger.debug(f"[COMFORT] blender: {self.question_handler.get_response_from_blender()}")

        # # Step 2: Check if it's unanswerable
        # if self.is_unanswerable_question(dispatcher, tracker):
        #     logger.debug(f"[COMFORT] question_handler is unanswerable question")
        #     dispatcher.respond(self.question_handler.generate_i_dont_know_response())
        #     return [PreviousState()]

        # step 3:
        next_state = self.handle_topic_specific_question(dispatcher, tracker)
        if next_state:
            logger.debug(f"[COMFORT] question_handler is topic specific question: {next_state}")
            return next_state

        # Step 4
        next_state = self.handle_general_question(dispatcher, tracker)
        if next_state:
            logger.debug(f"[COMFORT] question_handler is general question: {next_state}")
            return next_state

    def is_unanswerable_question(self, dispatcher: Dispatcher, tracker: Tracker) -> bool:
        # todo: you should decide what's answerable in you topic module
        if self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
            return True
        return False

    def handle_topic_specific_question(self, dispatcher: Dispatcher, tracker: Tracker):
        # eg. for movie, it should handle user's requests like "what movie do you recommend to me"
        enterTopicBot = get_question_for_bot(tracker)

        wantToHear = r"\bhear more\b|\blisten\b|\blistening\b"
        checkListenMore = re.compile(wantToHear)
        listenMore = False
        if checkListenMore.search(tracker.input_text) is not None:
            listenMore = True

        botShareProblem = r"\byou.*share\b"
        checkBotShareProblem = re.compile(botShareProblem)
        botShouldShare = False
        if checkBotShareProblem.search(tracker.input_text) is not None:
            botShouldShare = True

        botCheerUp = "cheer me up|me feel better"
        checkBotCheerUp = re.compile(botCheerUp)
        botShouldCheerUp = False
        if checkBotCheerUp.search(tracker.input_text) is not None:
            botShouldCheerUp = True

        lexicalYesNo = lexical_re_patterns["ask_yesno"]
        checkYesNo = re.compile(lexicalYesNo)
        botYesNo = False
        logger.debug("[COMFORT] testing yes no:" + str(lexicalYesNo))
        if checkYesNo.search(tracker.input_text) is not None:
            logger.debug("[COMFORT] got yes no to be true")
            botYesNo = True

        questionForBot = False
        yesNoQuestionForBot = False
        if tracker.returnnlp.has_dialog_act({DialogActEnum.OPEN_QUESTION_OPINION, DialogActEnum.OPEN_QUESTION_FACTUAL, DialogActEnum.OPEN_QUESTION}):
            questionForBot = True
        if tracker.returnnlp.has_dialog_act(DialogActEnum.YES_NO_QUESTION):
            yesNoQuestionForBot = True

        if enterTopicBot is not None:
            return [NextState('question_bot', jump=True)]
        elif (questionForBot is True or yesNoQuestionForBot is True or botYesNo is True) and listenMore is True:
            return [NextState('listen_more', jump=True)]
        elif (questionForBot is True or yesNoQuestionForBot is True or botYesNo is True) and botShouldShare is True:
            return [NextState('bot_share_opinion', jump=True)]
        elif botShouldCheerUp:
            return [NextState('bot_should_cheer_up', jump=True)]

        else:
            return None


    def handle_general_question(self, dispatcher: Dispatcher, tracker: Tracker) -> QuestionResponse:
        answer = self.question_handler.handle_question()
        logger.debug(f"[COMFORT] question_handler general_question: {answer}")

        # if answer.tag is "ack_question_idk":
        #     dispatcher.respond("Sorry,")
        dispatcher.respond(answer.response)

        if answer and answer.bot_propose_module and answer.bot_propose_module is not TopicModule.SAY_COMFORT:
            logger.debug(f"[COMFORT] question_handler has bot_propose_module: {answer.bot_propose_module}")
            # backstory wants to propose a topic
            dispatcher.respond_with_alt_template(
                Template.transition, f"propose_topic_short/{answer.bot_propose_module.value}", {})
            dispatcher.propose_continue('STOP', answer.bot_propose_module)
        else:
            logger.debug(f"[COMFORT] question_handler straight to SOCIAL")
            dispatcher.respond_with_alt_template(
                Template.transition, f"propose_topic_short/SOCIAL", {})
            dispatcher.propose_continue('STOP')

        return [PreviousState()]
