import logging
import random
import re
from typing import List, TYPE_CHECKING, Optional

import nlu.utils.topic_module
from nlu.intent_classifier import IntentClassifier
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse
from response_generator.fsm.events import Event, NextState, PreviousState
from response_generator.fsm.utils import Dispatcher, Tracker
from template_manager import Template
from template_manager import error as tm_error
from utils import get_working_environment
from nlu.constants import Positivity, TopicModule
from nlu.constants import DialogAct as DialogActEnum
from .states import regexCheckWantMoreTalk, regexCheckExceptions, regexCheckNegative
from nlu.intentmap_scheme import sys_re_patterns, lexical_re_patterns

from .. import utils as cutils

from .base import ComfortState

logger = logging.getLogger("comfort.joke")
regexDislike = lexical_re_patterns["ans_dislike"]
lexicalNegative = lexical_re_patterns["ans_negative"]
regexPositive = lexical_re_patterns["ans_positive"]
regexOpinionPositive = lexical_re_patterns["opinion_positive"]
regexContinue = lexical_re_patterns["ans_continue"]
regexLike = lexical_re_patterns["ans_like"]
regexCheckDislike = re.compile(regexDislike)
lexicalCheckNegative = re.compile(lexicalNegative)
regexCheckPositive = re.compile(regexPositive)
regexChecKOpinionPositive = re.compile(regexOpinionPositive)
regexCheckContinue = re.compile(regexContinue)
regexCheckLike = re.compile(regexLike)


class Joke(ComfortState):
    name = 'joke'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:
        joke_limit = tracker.last_utt_store.get('joke_limit')
        logger.debug("[Comfort]: " + str(tracker.returnnlp))
        if regexCheckWantMoreTalk.search(tracker.input_text) is not None:
            return [NextState('return_to_talking', jump=True)]
        if regexCheckExceptions.search(tracker.input_text) is not None or tracker.returnnlp.has_dialog_act(DialogActEnum.POS_ANSWER) or regexCheckPositive.search(tracker.input_text) is not None or regexChecKOpinionPositive.search(tracker.input_text) is not None or regexCheckContinue.search(tracker.input_text) is not None or regexCheckLike.search(tracker.input_text) is not None:
            system_ack = tracker.module.response_generator_ref.input_data['system_acknowledgement']
            acknowledgement_text = system_ack['ack']

            if acknowledgement_text is '':
                dispatcher.respond("Great!")
            else:
                dispatcher.respond(acknowledgement_text)
            dispatcher.respond_template("joke/give", {})
            dispatcher.respond("What did you think of that joke?")
            tracker.one_turn_store['joke_limit'] = joke_limit + 1
            return [NextState('joke_input')]
        else:
            if joke_limit == 0:
                dispatcher.respond_template("exit/here_for_you_transition_segment", {})
            else:
                dispatcher.respond_template("exit/here_for_you_transition_segment", {})
            dispatcher.propose_continue("STOP")
            return [NextState('init')]


class JokeInput(ComfortState):
    name = 'joke_input'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:

        joke_limit = tracker.last_utt_store.get('joke_limit')
        pos = tracker.returnnlp[0].sentiment.pos
        neg = tracker.returnnlp[0].sentiment.neg
        logger.debug("[Comfort] This is dialog act: " + str(tracker.returnnlp))
        if regexCheckWantMoreTalk.search(tracker.input_text) is not None:
            return [NextState('return_to_talking', jump=True)]
        #TODO: possibly switch checking for positive instead
        if joke_limit == 3:
            dispatcher.respond_template("joke/enough", {})
            dispatcher.propose_continue("STOP")
            return [NextState('init')]
        elif regexCheckExceptions.search(tracker.input_text) is not None or tracker.returnnlp.has_dialog_act(DialogActEnum.POS_ANSWER) or regexCheckPositive.search(tracker.input_text) is not None or regexChecKOpinionPositive.search(tracker.input_text) is not None or regexCheckContinue.search(tracker.input_text) is not None or regexCheckLike.search(tracker.input_text) is not None:
            system_ack = tracker.module.response_generator_ref.input_data['system_acknowledgement']
            acknowledgement_text = system_ack['ack']
            if acknowledgement_text is '':
                #logger.debug("THIS IS NOT EMPTY")
                dispatcher.respond_template("joke/another/need_utterance", {})
                tracker.one_turn_store['joke_limit'] = joke_limit
                return [NextState('joke')]
            else:
                dispatcher.respond(acknowledgement_text)
                dispatcher.respond_template("joke/another/no_utterance", {})
                tracker.one_turn_store['joke_limit'] = joke_limit
                return [NextState('joke')]
        else:
            dispatcher.respond_template("exit/here_for_you_transition_segment", {})
            dispatcher.propose_continue("STOP")
            return [NextState('init')]