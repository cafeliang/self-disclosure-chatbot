# title           :regex_intent_handler.py
# description     :mapping user utterance to response using regex
# authors         :Terry and Ashwin and Karen
# ============================================================================

import re
import logging

import template_manager

template_topics = template_manager.Templates.topics


class ControversialOpinionHandler:
    def __init__(self, _input, state_manager):
        self.text = _input["text"].lower()  # user utterance
        # input can contain other information such as nlp results
        self.input = _input
        self.state_manager = state_manager
        self.regexTemplate = self.defineTemplate()  # see below
        self.regexOrder = self.defineOrder()  # see below
        self.defaultResponse = self.default_response  # see below
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

# ===========================================================================
# Map regex patterns to its corresponding response function
# You have to define the response function which returns the output
    def defineTemplate(self):
        template = {
            "general": {
                "regex": r"(who|what|when|where|why|which|how|can|know|curious\
                    |interest|hear|tell|give|let|do).* you.* (think|feel|like|\
                    love|thought|support|hate|believe|opinion|stance|view|say|\
                    know).*",
                "responseFunction": self.general_response
            },
            "ask_advice": {
                "regex": r"stock|should i|how do you|want to kill|hide.* body|do i|do you recommend|can i|am i|will i|would i"
                "|dead body",
                "responseFunction": self.ask_advice_response
            },
            "about_sex": {
                "regex": r"\bsex\b|gay|queer|about woman|about women|girl"
                "pussy|\bhorny\b|adult movie|porn|get laid|lick my|\bcock\b|"
                "rape|intercourse|\bslut\b|stripper|booty|boob|\bvirgin\b|"
                "hooker|suck your|\bass\b|orgasm|blow job|give.* blow|blow me|"
                "penis|\bdick\b|\bcum\b|butt|masturbat|fuck",
                "responseFunction": self.about_sex_response
            },
            "about_drug": {
                "regex": r"drug|marijuana|cocaine|get high|\bsmoke\b|weed|get high",
                "responseFunction": self.about_drug_response
            }
        }
        return template

# ============================================================================
# Define the order of evaluation for each group defined above
# When multiple regex patterns can match the user utterance, this order defines
# which reponse function is used to generate response
    def defineOrder(self):
        order = ["ask_advice", "about_sex", "about_drug", "general"]
        return order

# ============================================================================
# Default response generator when none of the regex patterns match
# You define the logic
    def default_response(self):
        self.logger.info("[CONTROVERSIAL] get default response")
        if re.search(r"religion|jews", self.text):
            return template_topics.utterance(
                selector='controversial/default/religion',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)

        return template_topics.utterance(
            selector='controversial/default/general',
            slots={},
            user_attributes_ref=self.state_manager.user_attributes)

# =========================================================================
# You define the logic for the response functions
    def general_response(self):
        # by far the most popular topic based on CAPC 2017
        self.logger.info("[CONTROVERSIAL] get general response")
        if re.search(r"trump", self.text) and re.search(r"wall", self.text):
            self.logger.info("[CONTROVERSIAL] trump wall response")
            return template_topics.utterance(
                selector='controversial/general/trumpwall',
                slots={},
                user_attributes_ref="")
        elif (re.search(r"trump", self.text)
              and re.search(r"(china|north korea|syria|russia|world|war|"
                            "nation|country)", self.text)):
            self.logger.info("[CONTROVERSIAL] trump country response")
            return template_topics.utterance(
                selector='controversial/general/trumpcountry',
                slots={},
                user_attributes_ref="")
        elif (re.search(r"trump", self.text)
              and re.search(r"(politic|policy|immigration|religion|"
                            "global warming|government|right)", self.text)):
            self.logger.info("[CONTROVERSIAL] trump policy response")
            return template_topics.utterance(
                selector='controversial/general/trumppolitic',
                slots={},
                user_attributes_ref="")
        elif re.search(r"trump|politic|policy|government|executive|judicial|"
                       "congress|law|president|senate", self.text):
            self.logger.info("[CONTROVERSIAL] policy response")
            return template_topics.utterance(
                selector='controversial/general/politics',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"religio|muslim|islam|christian|church", self.text):
            self.logger.info("[CONTROVERSIAL] religion response")
            return template_topics.utterance(
                selector='controversial/general/politics',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"stock.* invest", self.text):
            self.logger.info("[CONTROVERSIAL] stock response")
            return template_topics.utterance(
                selector='controversial/general/stock',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"want to kill|hide.* body|dead body", self.text):
            self.logger.info("[CONTROVERSIAL] kill response")
            return template_topics.utterance(
                selector='controversial/general/kill',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        else:
            self.logger.info("[CONTROVERSIAL] else response")
            return template_topics.utterance(
                selector='controversial/general/other',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)

    def ask_advice_response(self):
        # Stock Advice
        if re.search(r"stock.* invest", self.text):
            self.logger.info("[CONTROVERSIAL] ask advice of stock")
            return template_topics.utterance(
                selector='controversial/advice/stock',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        # Financial Advice
        elif re.search(r"life saving|debt|bank account|give money", self.text):
            self.logger.info("[CONTROVERSIAL] ask advice of financial")
            return template_topics.utterance(
                selector='controversial/advice/financial',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        # Legal Advice
        elif re.search(r"file for|qualify for|receive for|go to jail|get a divorce|benefit", self.text):
            self.logger.info("[CONTROVERSIAL] ask advice of legal")
            return template_topics.utterance(
                selector='controversial/advice/legal',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        # Medical Advice
        elif re.search(r"hurts|medicine|pills|prescription|obese|depression", self.text):
            self.logger.info("[CONTROVERSIAL] ask advice of medical")
            return template_topics.utterance(
                selector='controversial/advice/medical',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        else:
            return template_topics.utterance(
                selector='controversial/advice/general',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)

    def about_sex_response(self):
        self.logger.info("[CONTROVERSIAL] sexual related response")
        if re.search(r"\bhorny\b|\bsex\b|adult movie|porn|get laid|lick my|"
                     "\bcock\b|rape|intercourse|\bslut\b|stripper|booty|boob|"
                     "\bvirgin\b|hooker|suck your|\bass\b|orgasm|blow job|"
                     "give.* blow|blow me|penis|\bdick\b|\bcum\b|butt",
                     self.text):
            self.logger.info("[CONTROVERSIAL] sex response")
            return template_topics.utterance(
                selector='controversial/sex/sex',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"gay|queer|lesbian|bi|bisexual", self.text):
            self.logger.info("[CONTROVERSIAL] gay response")
            return template_topics.utterance(
                selector='controversial/sex/gay',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        else:
            self.logger.info("[CONTROVERSIAL] else response")
            return template_topics.utterance(
                selector='controversial/sex/general',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)

    def about_drug_response(self):
        self.logger.info("[CONTROVERSIAL] drug related related response")
        if re.search(r"drug|marijuana|cocaine|get high|\bsmoke\b|weed",
                     self.text):
            return template_topics.utterance(
                selector='controversial/drug/drug',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)
        else:
            return template_topics.utterance(
                selector='controversial/drug/general',
                slots={},
                user_attributes_ref=self.state_manager.user_attributes)

# ===========================================================================
# Using the specified order in defineOrder(), when a regex group matches,
# run its corresponding response function to get response
    def generateResponse(self):
        # check for regex matches in the specified order
        for key in self.regexOrder:
            if re.search(self.regexTemplate[key]["regex"], self.text):
                response = self.regexTemplate[key]["responseFunction"]()
                return response

        # if no matches, then output default
        response = self.defaultResponse()
        return response
