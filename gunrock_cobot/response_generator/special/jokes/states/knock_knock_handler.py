import logging
import random
import re
from typing import List, TYPE_CHECKING, Optional

from response_generator.fsm2 import Dispatcher, Tracker, NextState
from response_generator.fsm2.events import Event

from .base import JokesState

logger = logging.getLogger("jokes.states")


class KnockKnockHandlerRespond(JokesState):
    name = 'knock_knock_handler'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:

        knockKnockRegex = r"knock knock"
        checkKnockKnock = re.compile(knockKnockRegex)

        if checkKnockKnock.search(tracker.input_text):
            logger.debug("[JOKES] entering knock knock handler")
            return self.name
        else:
            return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        joke_limit = tracker.last_utt_store.get('joke_limit')
        logger.debug("[JOKES] entered knock knock")
        dispatcher.respond_template('jokes/knock_knock', {})
        tracker.one_turn_store['joke_limit'] = joke_limit
        return [NextState('want_joke')]