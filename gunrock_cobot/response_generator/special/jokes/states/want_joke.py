from response_generator.fsm2 import Dispatcher, Tracker, NextState, State
from .base import JokesState
from nlu.constants import TopicModule
from nlu.constants import DialogAct as DialogActEnum
from nlu.intent_classifier import IntentClassifier
from template_manager import Template
from nlu.intentmap_scheme import sys_re_patterns, lexical_re_patterns
import logging
import re

logger = logging.getLogger("jokes.want_joke")

class WantJoke(JokesState):

    name = 'want_joke'


    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        logger.debug("[JOKES] entered the want joke state")
        joke_limit = tracker.last_utt_store.get('joke_limit')
        if joke_limit is None:
            joke_limit = 1

        logger.debug("[JOKES] This is nlp: " + str(tracker.returnnlp))
        regexMore = sys_re_patterns["req_more"]
        regexDislike = lexical_re_patterns["ans_dislike"]
        regexNegative = lexical_re_patterns["ans_negative"]
        regexOpinionNegative = lexical_re_patterns["opinion_negative"]
        regexPositive = lexical_re_patterns["ans_positive"]
        regexOpinionPositive = lexical_re_patterns["opinion_positive"]
        regexContinue = lexical_re_patterns["ans_continue"]
        regexLike = lexical_re_patterns["ans_like"]
        regexNotUnderstand = r"i (don't|do not) (get|understand)"
        regexLeave = lexical_re_patterns["ask_leave"]
        regexAnother = r"(tell|give) me.*(joke|one)|don't stop"
        regexCheckMore = re.compile(regexMore)
        regexDislike = re.compile(regexDislike)
        regexNegative = re.compile(regexNegative)
        regexOpinionNegative = re.compile(regexOpinionNegative)
        regexCheckNotUnderstand = re.compile(regexNotUnderstand)
        regexCheckLeave = re.compile(regexLeave)
        regexCheckAnother = re.compile(regexAnother)
        regexCheckPositive = re.compile(regexPositive)
        regexChecKOpinionPositive = re.compile(regexOpinionPositive)
        regexCheckContinue = re.compile(regexContinue)
        regexCheckLike = re.compile(regexLike)


        system_ack = self.module.input_data['system_acknowledgement']
        acknowledgement_text = system_ack['ack']

        number_of_jokes = dispatcher.tm.count_utterances("jokes/general")
        #out of jokes
        if joke_limit == number_of_jokes:
            dispatcher.respond_template("out_of_jokes", {})
            dispatcher.propose_continue('STOP')
            return [NextState('initial')]
        if regexCheckLeave.search(tracker.input_text) is not None:
            #dispatcher.respond_template("understand", {})
            dispatcher.propose_continue('STOP')
            return [NextState('initial')]
        #did not like joke
        logger.debug(f"[JOKES] testing dislike: {tracker.input_text} has this {regexDislike.search(tracker.input_text)}")
        if tracker.returnnlp.has_dialog_act(DialogActEnum.NEG_ANSWER) or regexDislike.search(tracker.input_text) is not None or regexNegative.search(tracker.input_text) is not None or regexCheckNotUnderstand.search(tracker.input_text) is not None or regexOpinionNegative.search(tracker.input_text) is not None:
            if joke_limit == 3:
                dispatcher.respond_template("bad_jokes", {})
                dispatcher.propose_continue('STOP')
                return [NextState('initial')]
            else:
                tracker.one_turn_store['joke_limit'] = joke_limit + 1
                dispatcher.respond_template('jokes/no_like', {})
                dispatcher.respond_template('jokes/general', {})
                return [NextState('want_joke')]
        #want another joke
        elif regexCheckMore.search(tracker.input_text) is not None or regexCheckAnother.search(tracker.input_text) is not None:
            tracker.one_turn_store['joke_limit'] = joke_limit + 1
            if acknowledgement_text is '':
                dispatcher.respond_template('positive', {})
            dispatcher.respond_template('jokes/general', {})
            return [NextState('want_joke')]
        elif tracker.returnnlp.has_dialog_act(DialogActEnum.POS_ANSWER) or regexCheckPositive.search(tracker.input_text) is not None or regexChecKOpinionPositive.search(tracker.input_text) is not None or regexCheckContinue.search(tracker.input_text) is not None or regexCheckLike.search(tracker.input_text) is not None:
            if acknowledgement_text is '':
                dispatcher.respond_template("jokes/liked", {})
            dispatcher.respond_template("jokes/another", {})
            tracker.one_turn_store['joke_limit'] = joke_limit
            return [NextState('like_joke')]
        else:
            #dispatcher.respond_template("understand", {})
            dispatcher.propose_continue('STOP')
            return [NextState('initial')]
