from response_generator.fsm2 import Dispatcher, Tracker, NextState
from .base import JokesState
from nlu.constants import TopicModule
from nlu.constants import DialogAct as DialogActEnum
from nlu.intent_classifier import IntentClassifier
from nlu.intentmap_scheme import sys_re_patterns, lexical_re_patterns
import logging
import re

logger = logging.getLogger("special.jokes.like_joke")

class LikeJoke(JokesState):

    name = 'like_joke'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        logger.debug("[JOKES] entered like_joke state")
        pos = tracker.returnnlp[0].sentiment.pos
        neg = tracker.returnnlp[0].sentiment.neg
        joke_limit = tracker.last_utt_store.get('joke_limit')
        if joke_limit is None:
            joke_limit = 1
        regexDislike = lexical_re_patterns["ans_dislike"]
        regexLeave = lexical_re_patterns["ask_leave"]
        regexNegative = lexical_re_patterns["ans_negative"]
        regexOpinionNegative = lexical_re_patterns["opinion_negative"]
        regexDislike = re.compile(regexDislike)
        regexCheckLeave = re.compile(regexLeave)
        regexNegative = re.compile(regexNegative)
        regexOpinionNegative = re.compile(regexOpinionNegative)

        system_ack = self.module.input_data['system_acknowledgement']
        acknowledgement_text = system_ack['ack']

        if regexCheckLeave.search(tracker.input_text) is not None:
            #dispatcher.respond_template("understand", {})
            dispatcher.propose_continue('STOP')
            return [NextState('initial')]
        #did not like joke
        if tracker.returnnlp.has_dialog_act(DialogActEnum.NEG_ANSWER) or regexDislike.search(tracker.input_text) is not None or regexNegative.search(tracker.input_text) is not None or regexOpinionNegative.search(tracker.input_text) is not None:
            dispatcher.respond_template('understand', {})
            dispatcher.respond_template('jokes/transition', {})
            dispatcher.propose_continue('STOP')
            return [NextState('initial')]
        #they liked the joke
        else:
            logger.debug("[JOKES] entered positive feedback")
            tracker.one_turn_store['joke_limit'] = joke_limit + 1
            if acknowledgement_text is '':
                dispatcher.respond_template('positive', {})
            dispatcher.respond_template('jokes/general', {})
            return [NextState('want_joke')]
