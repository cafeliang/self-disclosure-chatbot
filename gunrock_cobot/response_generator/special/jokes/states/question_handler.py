import logging
import random
import re
from typing import List, TYPE_CHECKING, Optional

import nlu.utils.topic_module
from nlu.intent_classifier import IntentClassifier
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse
from response_generator.fsm2 import Dispatcher, Tracker, NextState, PreviousState
from response_generator.fsm2.events import Event
from template_manager import Template
from template_manager import error as tm_error
from utils import get_working_environment
from nlu.constants import Positivity, TopicModule
from nlu.constants import DialogAct as DialogActEnum
from nlu.intentmap_scheme import lexical_re_patterns

from .base import JokesState

logger = logging.getLogger("jokes.question_handler")

class AnotherJoke(JokesState):
    name = 'another_joke'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return None

class QuestionHandlerRespond(JokesState):
    name = 'question_handler'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        self.question_detector = QuestionDetector(
            tracker.input_text, self.module.input_data['returnnlp'])

        logger.debug("[JOKES] ENTERING")

        if self.question_detector.has_question():
            logger.debug("[JOKES] question_handler is entering")
            return self.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        self.question_detector = QuestionDetector(
            tracker.input_text, self.module.input_data['returnnlp'])
        self.question_handler = QuestionHandler(
            tracker.input_text, self.module.input_data['returnnlp'],
            backstory_threshold=0.82,
            system_ack=self.module.input_data['system_acknowledgement'],
            user_attributes_ref=tracker.user_attributes.ua_ref
        )

        joke_limit = tracker.last_utt_store.get('joke_limit')
        if joke_limit is None:
            joke_limit = 1
        tracker.one_turn_store['joke_limit'] = joke_limit

        # # Step 2: Check if it's unanswerable
        # if self.is_unanswerable_question(dispatcher, tracker):
        #     logger.debug(f"[COMFORT] question_handler is unanswerable question")
        #     dispatcher.respond(self.question_handler.generate_i_dont_know_response())
        #     return [PreviousState()]

        # step 3:
        next_state = self.handle_topic_specific_question(dispatcher, tracker)
        if next_state:
            logger.debug(f"[JOKES] question_handler is topic specific question: {next_state}")
            return next_state

        # Step 4
        next_state = self.handle_general_question(dispatcher, tracker)
        if next_state:
            logger.debug(f"[JOKES] question_handler is general question: {next_state}")
            return next_state

    def is_unanswerable_question(self, dispatcher: Dispatcher, tracker: Tracker) -> bool:
        # todo: you should decide what's answerable in you topic module
        if self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
            return True
        return False

    def handle_topic_specific_question(self, dispatcher: Dispatcher, tracker: Tracker):
        # eg. for movie, it should handle user's requests like "what movie do you recommend to me"

        joke_limit = tracker.last_utt_store.get('joke_limit')
        if joke_limit is not None:
            tracker.one_turn_store['joke_limit'] = joke_limit
        regexAnotherJoke = r"\bjoke\b|\bjokes\b"
        checkAnotherJoke = re.compile(regexAnotherJoke)
        anotherJoke = False
        if checkAnotherJoke.search(tracker.input_text) is not None:
            anotherJoke = True

        questionForBot = False
        yesNoQuestionForBot = False
        if tracker.returnnlp.has_dialog_act({DialogActEnum.OPEN_QUESTION_OPINION, DialogActEnum.OPEN_QUESTION_FACTUAL, DialogActEnum.OPEN_QUESTION}):
            questionForBot = True
        if tracker.returnnlp.has_dialog_act(DialogActEnum.YES_NO_QUESTION):
            yesNoQuestionForBot = True

        if anotherJoke is True:
            if joke_limit is not None:
                return [NextState('want_joke', jump=True)]
            else:
                return [NextState('initial', jump=True)]
        else:
            return None


    def handle_general_question(self, dispatcher: Dispatcher, tracker: Tracker) -> QuestionResponse:
        answer = self.question_handler.handle_question()
        logger.debug(f"[JOKES] question_handler general_question: {answer}")

        dispatcher.respond(answer.response)

        if answer and answer.bot_propose_module and answer.bot_propose_module is not TopicModule.SAY_FUNNY:
            self.logger.debug(f"[JOKES] question_handler has bot_propose_module: {answer.bot_propose_module}")
            # backstory wants to propose a topic
            dispatcher.respond_template(
                f"propose_topic_short/{answer.bot_propose_module.value}", {}, template=Template.transition
            )
            dispatcher.propose_continue('UNCLEAR', answer.bot_propose_module)
        elif answer.tag is "ack_question_idk":
            joke_limit = tracker.last_utt_store.get('joke_limit')
            if joke_limit is not None:
                tracker.one_turn_store['joke_limit'] = joke_limit
            else:
                tracker.one_turn_store['joke_limit'] = 1
            dispatcher.respond("Would you like to hear another joke?")
            return [NextState("like_joke", jump=False)]

        prev_state = self.module.state_tracker.curr_turn()[0]
        # temp fix PreviousState not working
        return [NextState(prev_state)]
