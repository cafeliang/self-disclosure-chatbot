import re
import random


class tellStoryFactMemeIntentHandler:
    def __init__(self, input, last_content_key, sys_intents):
        self.text = input["text"]
        self.last_content_key = last_content_key
        self.sys_intents = sys_intents
        self.input = input
        self.regexTemplate = self.defineTemplate()
        self.regexOrder = self.defineOrder()
        self.defaultResponse = self.default_response

    def defineTemplate(self):
        template = {
                        "quote": {
                            "regex": r"quote|motivation|inspiration|emotion|meaning|strength|moral",
                            "responseFunction": self.quote_response,
                        },
                        "fun_fact": {
                            "regex": r"fact|something interesting|funny.* true",
                            "responseFunction": self.fun_fact_response,
                        },
                        "life_hack": {
                            "regex": r"life hack|save.* life|practical|easy|low_cost|hack",
                            "responseFunction": self.life_hack_response,
                        },
                        "story": {
                            "regex": r"story|fable|plot|scenario|narrative|tales|storyline",
                            "responseFunction": self.story_response,
                        },
                        "meme": {
                            "regex": r"meme",
                            "responseFunction": self.meme_response,
                        }

        }
        return template

    def defineOrder(self):
        order = ["quote", "fun_fact", "life_hack", "story", "meme"]
        return order

    def default_response(self):
        return random.choice(["Hmm, I can't think of one right now. Do you mind telling me your favorite one? ",
                              "Well, I don't have a good one for now. why don't you tell me one."])

    def quote_response(self):
        responses = [
            "<say-as interpret-as=\"interjection\">okey dokey!</say-as> What do you think about this quote: Marcus Tullius Cicero once wrote: A room without books is like a body without a soul.",
            "Okay! I think you might like this one. J.K. Rowling wrote: If you want to know what a man's like, take a good look at how he treats his inferiors, not his equals.",
            "Einstein once said: There are only two ways to live your life. One is as though nothing is a miracle. The other is as though everything is a miracle.",
            "Maybe you will like this quote: The man who does not read has no advantage over the man who cannot read.",
            "Sure. Dr. Seuss once said: I like nonsense, it wakes up the brain cells. Fantasy is a necessary ingredient in living.",
            "What do you think about this quote: Mark Twain once said, 'Whenever you find yourself on the side of the majority, it is time to reform.'",
            "I like this quote! Mark Twain once wrote: If you tell the truth, you don't have to remember anything."
        ]
        return random.choice(responses)


    def fun_fact_response(self):
        responses = [
            "It is impossible to sneeze with your eyes open.",
            "Cherophobia is the fear of having fun.",
            "The Chesapeake Bay Bridge Tunnel is both a bridge and a tunnel, which spans over 23 miles long and is one of the wonders of modern engineering.",
            "Kleenex was originally invented to create paper filters for gas masks during the first World War.",
            "Pteronophobia is the fear of being tickled by feathers.",
            "In 1939 Hewlett-Packard, also known as HP, decided their name in a coin toss.",
            "The name of the Twitter bird is Larry.",
            "New Zealand has an official wizard.",
            "Brontophobia is the fear of lightning.",
        ]

        return random.choice(responses)



    def life_hack_response(self):


        responses = [
            "Tie a small piece of a coloured fabric to your luggage. Saves a lot of your time to check if it is your bag or not.",
            "Wrap a wet paper towel around your beverage and put it in the freezer. The beverage will get ice-cold within 15 minutes.",
            "Put a wooden spoon across a pot of boiling hot water so as to keep it from boiling over.",
        ]
        return random.choice(responses)


    def meme_response(self):


        responses = [
            "Well, I can't tell a meme without an image. But I like memes though.",
			"Ah ha I love memes but sorry I can't tell one without context"
        ]
        return random.choice(responses)



    def story_response(self):

        responses = [

#            "Here is my ten words story: It's easier to love myself when you love me too.",

#            "Here is my six words story: Strangers. Friends. Best Friends. Lovers. Strangers.",

            "Sure, I'd love to tell you a story. On his 50th birthday, Phil saddled a giraffe and rode through town. His wife waited patiently as he reclaimed the dreams of his youth.",

            "Of course, I love telling stories. David liked ants, the only pet his parents had approved of. He designed his house like an ant colony but still searched for his queen.",

            "Okay, here is a story for you. After months of looking, the only job I found was working as a mannequin. It paid very little, but was better than standing around for free.",

            #"Sure, I'm happy to tell you a story. Gerald cared for his mom's neglected plants. Grateful, a fern felt compelled to speak, Thank you. Terrified, he got rid of the plants.",

            "Sure, I'd love to tell you a story. With a shortage of human teachers, robots were brought in to teach. It went well, until the kids built their own robots and the battle began.",

            "Okay, here is a story for you. One day a father wants to play a video game his kids keep on talking about. They usually brag about how many levels they have completed. On his first day of playing the game he was able to reach stage 11 and bragged about it to his kids. That was when one of his kids pointed out that the number 11 wasn't actually 11, rather it was the pause sign.",

            #"Of course, I love telling stories. Tell me the truth. Is there anything that you have hidden from me <break time=\"0.5s\"/> she asked. Yes, <break time=\"0.5s\"/> he replied, <break time=\"0.5s\"/> <emphasis level=\"strong\">your birthday plan.</emphasis>",

            #"Okay, here a story for you. Ten years from now, the decision to do so would make perfect sense. Ten years ago, the possibility was utterly unimaginable",

            #"Sure, I'm happy to tell you a story. 16 hours at the office and he is the best employee of the month. 1 hour daily at the playground and he is the world's best dad",

            #"Sure, I love telling stories. Even after 7000 miles and 24 hours, she was not jetlagged. For the last three years, her heart had been in this time zone. ",

#            "Her birthday today. She is surrounded by balloons. But no one buys from her",

#            "Tiffin, homework, assignments, vows. <emphasis level=\"strong\">They shared everything.</emphasis>",

            #"Of course, I love stories. You are going to be the only one <break time=\"0.5s\"/> she whispered with tears in her eyes. And then she betrayed the trust by ordering another pizza.",

            "Sure, this is one of my favorite stories. A man walked to the top of a hill to talk to God. \
The man asked, <emphasis level=\"reduced\"><break time=\"0.5s\"/> God, what's a million years to you? </emphasis>and God said, <emphasis level=\"strong\"><break time=\"0.5s\"/> A minute. </emphasis>\
Then the man asked, <emphasis level=\"reduced\"><break time=\"0.5s\"/> 'Well, what's a million dollars to you?' </emphasis>and God said, <emphasis level=\"strong\"><break time=\"0.5s\"/> A penny. </emphasis>\
Then the man asked, <emphasis level=\"reduced\"><break time=\"0.5s\"/> God...can I have a penny? </emphasis>and God said, <emphasis level=\"strong\"><break time=\"0.5s\"/> Sure...in a minute. </emphasis>"
             ]

        return random.choice(responses)


    def generateResponse(self, debug=False):
        # check for regex matches in the specified order
        if 'req_more' in self.sys_intents:
            if self.last_content_key is not None:
                response = self.regexTemplate[self.last_content_key]["responseFunction"]()
                return response, self.last_content_key
            else:
                for key in self.regexOrder:
                    if re.search(self.regexTemplate[key]["regex"], self.text.lower()):
                        response = self.regexTemplate[key]["responseFunction"]()
                        if debug == True:
                            response += " " + str(self.regexTemplate[key]["responseFunction"])
                        return response, key
        else:
            for key in self.regexOrder:
                if re.search(self.regexTemplate[key]["regex"], self.text.lower()):
                    response = self.regexTemplate[key]["responseFunction"]()
                    if debug == True:
                        response += " " + str(self.regexTemplate[key]["responseFunction"])
                    return response, key

        # if no matches, then output default
        response = self.defaultResponse()
        key = None
        return response, key
