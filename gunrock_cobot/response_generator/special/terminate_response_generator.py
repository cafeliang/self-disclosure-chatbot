import re

from cobot_core.service_module import LocalServiceModule
from response_generator.special.special_response_generator import logger, template_sys


class Terminate_ResponseGenerator(LocalServiceModule):
    """ Special Intents to pass Amazon's test """

    def execute(self):
        user_utterance = self.input_data['text'].lower()
        logger.info(
            '[TerminateRG] execute terminate with utterance {}'.format(user_utterance))

        key = ['terminate']
        if re.search(
                r"sleep|good night|\bi\b.* to bed|\bi\b.* to sleep|go to bed|ready for bed",
                user_utterance
        ):
            key.append('good_night')

        elif re.search(
                r"see you (later|tomorrow|soon|again)|have to go|i must go|gonna go|talk later|i.* take a nap|be quiet",
                user_utterance
        ):
            key.append('see_you')
        else:
            key.append('default')
        setattr(self.state_manager.current_state, "resp_type", "terminate")
        return template_sys.utterance(selector=key, slots={},
                                      user_attributes_ref=self.state_manager.user_attributes)