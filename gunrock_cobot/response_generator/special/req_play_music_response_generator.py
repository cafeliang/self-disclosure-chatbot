import re
import logging
import warnings
from cobot_core.service_module import LocalServiceModule
from response_generator.special.special_response_generator import template_sys
from nlu.util_nlu import get_feature_by_key

# Deprecated class. logic is handled in music module instead.
class ReqPlayMusic_ResponseGenerator(LocalServiceModule):

    def execute(self):
        logging.info("[REQGAME] enter play music")
        usr_utterance = self.input_data['text'].lower()
        topic_intent = get_feature_by_key(
            self.state_manager.current_state, 'intent_classify')['topic']
        logging.info("[REQMUSIC] input utterance is {}".format(usr_utterance))
        setattr(self.state_manager.current_state, "resp_type", "req_task")

        key = ['play_music']
        if re.search(r"podcast|radio", usr_utterance):
            key.append('podcast')
            setattr(self.state_manager.user_attributes,
                    "propose_topic", "RECOMMENDATIONTCHAT")
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"\bgame\b", usr_utterance) or 'topic_game' in topic_intent:
            key.append('game')
            setattr(self.state_manager.user_attributes,
                    "propose_topic", "GAMECHAT")
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"ted talk", usr_utterance):
            key.append('ted_talk')
            setattr(self.state_manager.user_attributes,
                    "propose_topic", "RECOMMENDATIONTCHAT")
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"sport", usr_utterance) or 'topic_sport' in topic_intent:
            key.append('sport')
            setattr(self.state_manager.user_attributes,
                    "propose_topic", "SPORT")
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        else:
            key.append('default')
            setattr(self.state_manager.user_attributes,
                    "propose_topic", "MUSICCHAT")
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)