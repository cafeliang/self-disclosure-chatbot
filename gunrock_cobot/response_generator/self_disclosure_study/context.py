import logging
from dataclasses import dataclass, asdict, field
from typing import List, Dict, Any


class State:
    INIT = "init"
    GREETING = "greeting"
    WHAT_DO_YOU_DO_TO_KEEP_BUSY = "what_do_you_do_to_keep_busy"
    ONLINE_SHOPPING = "online_shopping"
    DIET_CHANGE = "diet_change"
    FEEL_DOWN = "feel_down"
    PHYSICAL_DISTANCE = "physical_distance"
    SELF_CARE_RECOMMENDATION_PROPOSE = "self_care_recommendation_propose"
    SELF_CARE_RECOMMENDATION_CONTENT = "self_care_recommendation_content"




@dataclass
class CovidContext:
    current_state: str
    propose_continue: str

    @classmethod
    def from_dict(cls, ref: Dict[str, Any]):
        logging.info(f"CovidContext: ref: {ref}")
        result = cls(current_state=ref.get("current_state", State.INIT),
                     propose_continue=ref.get("propose_continue", "CONTINUE"))

        return result

