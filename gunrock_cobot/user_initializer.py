from cobot_core.state_manager import StateManager  # noqa
from util_redis import RedisHelper
import selecting_strategy.topic_module_proposal
from selecting_strategy.module_selection import ModuleSelector
from nlu.constants import TopicModule
from user_profiler.user_profile import UserProfile
from selecting_strategy.module_selection import module_and_user_attributes_key_map
import logging
import random

class UserInitializer:
    def __init__(self, state_manager: StateManager):
        self.state_manager = state_manager
        self.user_profile = UserProfile(self.state_manager.user_attributes)
        self.module_selector = ModuleSelector(self.state_manager.user_attributes)

    def is_new_user(self):
        return self.user_profile.visit == 0

    def init_for_new_user(self):
        logging.info("[User Initializer] init_for_new_user")
        self.__reset_user_attributes()
        self.reset_user_history_in_redis()
        self.__add_visit()
        self.module_selector.init_module_user_attributes()
        self.module_selector.init_module_rank()
        self.__init_previous_modules()
        self.__init_propose_continue()

        # self-disclosure-study
        self.init_abtest_for_self_disclosure_study()

    def init_for_returning_user(self):
        logging.info("[User Initializer] init_for_returning_user")
        if self.user_profile.visit >= 3:  # If user already talked to the bot 3 times, reset everything
            self.user_profile.visit = 0
            self.init_for_new_user()
        else:
            self.__reset_user_attributes(UserInitializer.__get_persistent_keys_for_returning_user())
            self.__add_visit()
            self.module_selector.init_module_rank()
            self.__init_propose_continue()

    def init_for_returning_user_different_name(self, except_keys: list = ['launchgreeting', 'last_session_id']):
        self.__reset_user_attributes(except_keys)
        self.reset_user_history_in_redis()
        self.__add_visit()
        self.module_selector.init_module_user_attributes()
        self.module_selector.init_module_rank()
        self.__init_previous_modules()
        self.__init_propose_continue()

    def init_for_open_day(self):
        self.__reset_user_attributes()
        self.reset_user_history_in_redis()
        self.__add_visit()
        self.module_selector.init_module_user_attributes()
        self.module_selector.init_module_rank()
        self.__init_previous_modules()
        self.__init_propose_continue()

    @staticmethod
    def __get_persistent_keys_for_returning_user():
        persistent_session_attributes = ["last_session_id", "visit", "self-disclosure-study-abtest", "self-disclosure-study-current-level"]
        persistent_nlu_attributes = ["asr_error_count"]
        persistent_user_profile_attributes = ["user_profile"]
        persistent_module_selection_attributes = [
            "previous_modules", "module_selection"]


        persistent_topic_module_names = [
            # TopicModule.MOVIE.value,
            TopicModule.BOOK.value,
            TopicModule.MUSIC.value,
            TopicModule.GAME.value,
            TopicModule.TECHSCI.value,
            TopicModule.FOOD.value,
            TopicModule.FASHION.value,
            TopicModule.TRAVEL.value,
            TopicModule.ANIMAL.value,
            TopicModule.SPORTS.value,
            TopicModule.FOOD.value,
            TopicModule.NEWS.value
        ]

        persistent_topic_module_keys = [module_and_user_attributes_key_map.get(module)
                                        for module in persistent_topic_module_names]

        persistent_template_attributes = ["template_manager"]

        persistent_keys = persistent_session_attributes + persistent_nlu_attributes + \
            persistent_user_profile_attributes + \
            persistent_module_selection_attributes + persistent_topic_module_keys + \
            persistent_template_attributes

        return persistent_keys

    def __reset_user_attributes(self, except_key: list = None):
        logging.info(f"[User Initializer] persistent_keys: {except_key}")

        if except_key is None:
            except_key = []

        for key in self.state_manager.user_attributes.map_attributes:
            if key in except_key:
                continue
            else:
                setattr(self.state_manager.user_attributes, key, None)

        setattr(self.state_manager.user_attributes, "template_manager", {})

    def reset_user_history_in_redis(self):
        redis_helper = RedisHelper()
        user_id = self.state_manager.user_attributes.user_id
        redis_helper.delete_user_history(user_id)

    def __add_visit(self):
        self.user_profile.add_visit()

    def __init_previous_modules(self):
        setattr(self.state_manager.user_attributes, 'previous_modules', [
            '__EMPTY__', '__EMPTY__', '__EMPTY__'])

    def __init_module_rank(self):
        module_selector = ModuleSelector(self.state_manager.user_attributes)
        module_selector.init_module_rank()

    def __init_propose_continue(self):
        selecting_strategy.topic_module_proposal.reset_propose_continue(
            self.state_manager.user_attributes)

    def init_abtest_for_self_disclosure_study(self):
        logging.info("[init_abtest_for_self_disclosure_study]")
        # conditions = ["A", "B", "C", "D"]  # todo
        conditions = ["C"]  # Always use the highest disclosure level

        picked_condition = random.choice(conditions)
        print("self-disclosure-study-abtest condition: ", picked_condition)
        setattr(self.state_manager.user_attributes, "self-disclosure-study-abtest", picked_condition)
        setattr(self.state_manager.user_attributes, "self-disclosure-study-current-level", "factual")

        logging.info("Condition: {}".format(getattr(self.state_manager.user_attributes, "self-disclosure-study-abtest", "")))

