import logging

def trace_latency(func):
    import time

    def inner_func(*args, **kwargs):
        try:
            start = time.time()
            output = func(*args, **kwargs)  # Call the original function with its arguments.
            end = time.time()

            logging.info(f"[Trace latency] function: {func.__qualname__}, latency: {(end - start) * 1000}")

            return output

        except Exception as e:
            logging.error(f"[Trace latency] Fail to execute function: {func.__qualname__}, message: {e}")

    return inner_func