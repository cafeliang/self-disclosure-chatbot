import json
import logging
import re
import socket
import requests
import os
import random
import praw
import datetime as dt

from nlg_post_process.response_prosody_post_processor import ResponseProsodyProcessor
from util_redis import RedisHelper

import template_manager

template = template_manager.Templates.sys_rg
template_social = template_manager.Templates.social
template_transition = template_manager.Templates.transition

logger = logging.getLogger(__name__)

# Reddit API KEY
REDDIT_CLIENT_ID = "lKmLeMmSSLns6A"
REDDIT_CLIENT_SECRET = "Fi6V_pCzKqE-4TRVqcmpMJdET2s"

WORKING_ENV = ""

EVI_PROFANITY_RESPONSE = 'rather not answer that'

HOST_NAME = socket.gethostname()

TEDTALK_TOPIC_PREFIX = "gunrock:ic:tedtalk:topic:"

r = RedisHelper().get_client()



def get_working_environment():
    if os.environ.get('STAGE') == 'PROD' or os.environ.get('STAGE') == 'BETA':
        return os.environ.get('STAGE').lower()
    else:
        return "local"


def set_logger_level(working_environment):
    if working_environment == "local":
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)


def print_log_for_local_env(log_text):
    if get_working_environment() == "local":
        print("[DEBUG] {}".format(log_text))

# [begin] general utilities
def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]


def check_intersect(a, b):
    a_set = set(a)
    b_set = set(b)
    if (a_set & b_set):
        return True
    else:
        return False


# [end] general utilities

# [begin] datetime helper
def parse_creation_date_time(creation_date_time: str) -> dt.datetime:
    return dt.datetime.strptime(creation_date_time, '%Y-%m-%dT%H:%M:%S.%f')


def time_diff_from_now(datetime: dt.datetime):
    return dt.datetime.utcnow() - datetime


def more_than_mins(time_delta: dt.timedelta, seconds: float):
    return time_delta.seconds >= seconds

# [end] datetime helper


# [begin] data helper
def get_reddit_key():
    return RedisHelper().get_reddit_key()


def get_reddit_client():
    key = get_reddit_key()
    c_id, c_secret, u_agent = key.split("::")
    return praw.Reddit(client_id=c_id,
                       client_secret=c_secret,
                       user_agent=u_agent,
                       timeout=2)


def retrieve_reddit(utterance, subreddits, limit):
    # cuurently using gt's client info

    reddit = get_reddit_client()
    subreddits = '+'.join(subreddits)
    subreddit = reddit.subreddit(subreddits)
    # higher limit is slower; delete limit arg for top 100 results
    submissions = list(subreddit.search(utterance, limit=limit))
    if len(submissions) > 0:
        return submissions
    return None


def is_common_name(name):
    return RedisHelper().is_common_name(name)


def scan_related_holiday(keyword, threshold=70):
    return RedisHelper().scan_related_holiday(keyword, threshold=70)


def get_future_holiday(category=None, limit=5):
    return RedisHelper().get_future_holiday(category=None, limit=5)


def get_all_holiday(category=None, limit=5):
    return RedisHelper().get_all_holiday(category, limit)

def get_randomthoughts_by_key(keyword):
    return RedisHelper().get_random_thoughts(keyword)


def get_interesting_reddit_posts(text, limit):
    SENSITIVE_WORDS_LIST = ['kill', 'murder', 'sex', 'porn',
                            'bitch', 'dead', 'torture', 'die', 'sucks', 'damn', 'death', 'fuck', 'masturbate',
                            'masturbating']
    submissions = retrieve_reddit(text, ['Showerthoughts'], limit)
    if submissions is not None:
        results = [submissions[i].title.replace(
            "TIL", "").lstrip() for i in range(len(submissions))]
        results = [
            results[i] + '.' if results[i][-1] != '.' and results[i][-1] != '!' and results[i][-1] != '?' else results[
                i] for i in range(len(results))]
        # Sensitive Words Pattern
        final_results = []
        for result in results:
            if not any(re.search(x, result.lower()) for x in SENSITIVE_WORDS_LIST):
                final_results.append(result)
        return final_results
    return None


# [end] data helper


def random_pick(elements, weights):
    x = random.uniform(0, 1)
    cumulative_probability = 0.0
    for item, item_probability in zip(elements, weights):
        cumulative_probability += item_probability
        if x < cumulative_probability:
            break
    return item

# state manager
def set_ask_open_question_tag(state_manager):
    setattr(state_manager.current_state, "bot_ask_open_question", True)


# system level utils
def _previous_repeat_requests_count(session_history):
    types = [response.get('resp_type', '') for response in session_history]
    count = 0
    for t in types:
        if t in {'incomplete', 'asr_error'}:
            count += 1
        else:
            break
    return count


def save_template_keys_to_current_state_and_reset(state_manager):
    template_keys = get_template_keys(state_manager)
    logger.debug("save_template_keys_to_current_state_and_reset: {}".format(template_keys))
    setattr(state_manager.current_state, "template_keys", template_keys)
    state_manager.user_attributes.map_attributes[
        'template_manager']['selector_history'] = []


def save_qa_response_to_current_state_and_reset(state_manager):
    qa_response = getattr(state_manager.user_attributes, 'qa_response', None)
    logging.info(f"qa_response: {qa_response}")
    if qa_response:
        setattr(state_manager.current_state, "qa_response", qa_response)
        setattr(state_manager.user_attributes, "qa_response", None)


def save_proposed_topic_to_current_state(state_manager):
    proposed_topic = get_proposed_topic(state_manager)
    logger.debug("save_proposed_topic_to_current_state: {}".format(proposed_topic))
    setattr(state_manager.current_state, "proposed_topic", proposed_topic)


def process_prosody(response, state_manager):
    processor = ResponseProsodyProcessor(response, getattr(state_manager.current_state, "a_b_test", ""))
    processor.process()
    return processor.response


def get_template_keys(state_manager):
    template_manager = getattr(state_manager.user_attributes, 'template_manager', {})
    return template_manager.get('selector_history', [])

def get_proposed_topic(state_manager):
    proposed_topic = getattr(state_manager.user_attributes, 'propose_topic', None)
    return proposed_topic


def trace_latency(func):
    import time

    def inner_func(*args, **kwargs):
        try:
            start = time.time()
            output = func(*args, **kwargs)  # Call the original function with its arguments.
            end = time.time()

            logging.info(f"[Trace latency] function: {func.__qualname__}, latency: {(end - start) * 1000}")

            return output

        except Exception as e:
            logging.error(f"[Trace latency] Fail to execute function: {func.__qualname__}, message: {e}")

    return inner_func