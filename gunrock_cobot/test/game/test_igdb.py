import pytest

from response_generator.game.utils.igdb import igdb
from response_generator.game.utils.igdb.igdb import Game, Genre


@pytest.mark.parametrize("query, expected_result", [
    ('witcher', Game(
        id=1942, name='The Witcher 3: Wild Hunt', slug='the-witcher-3-wild-hunt',
        summary=(
            "The Witcher: Wild Hunt is a story-driven, next-generation open world role-playing game set in a "
            "visually stunning fantasy universe full of meaningful choices and impactful consequences. "
            "In The Witcher you play as the professional monster hunter, Geralt of Rivia, "
            "tasked with finding a child of prophecy in a vast open world rich with merchant cities, "
            "viking pirate islands, dangerous mountain passes, and forgotten caverns to explore."
        ),
        genres=[
            dict(id=12, name='Role-playing(RPG)', slug='role-playing-rpg'),
            dict(id=31, name='Adventure', slug='adventure')
        ]
    )),
])
def test_search_game(query: str, expected_result):
    db = igdb.IGDB()
    result = db.search_game(query)
    print('result:', result)
    assert (
        result and
        result.id == expected_result.id and
        result.name == expected_result.name and
        result.slug == expected_result.slug and
        result.summary == expected_result.summary and
        len(result.genres) == len(expected_result.genres)
    )