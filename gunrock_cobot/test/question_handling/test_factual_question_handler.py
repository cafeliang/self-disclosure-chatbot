from nlu.constants import DialogAct as DialogActEnum
import pytest
from question_handling.quetion_handler import FactualQuestionHandler

@pytest.mark.parametrize("text", [
    "'that' is usually defined as",
    "'That' is usually defined as",
    "'his last name' is spelled:",
    "hi I am",
    "This might answer your question: Cat is a species of living things. Its scientific name is Felis catus.",
    "I'm Alexa",
    "A socialbot is an Alexa skill that converses about popular topics, such as Entertainment, Politics, and Sports",
    "The Special Activities Center is a division of the United States Central Intelligence Agency",
    "'This way' is usually defined as as follows, thuswise. ",
    "the number is used in calling a particular telephone",
    "The speaker of the United States House of Representatives is the presiding officer of the United States House of Representative",
    "I pronounce that 'wrong'",
    "Lopez Brothers is a YouTuber with 1,200,000 subscribers",
    "the audio publications label founded in 2013",
    "you can help improve my voice recognition by completing Voice Training",
    "are the citizens of the United Kingdom of Great Britain and Northern Ireland",
    "Bath is the largest city in the county",
    "Playing by Crystal Kaswell",
    "real number is any number that can be expressed as a decimal",
    "I don't know Harry Potter and the Deathly Hallows",
    "In Andorra, summer begins on Saturday",
    "Real is located in Quezon",
    "SpongeBob SquarePants is played by Tom Kenny as SpongeBob",
    "Ashanti, 35, does not have any children",
    "Taryn Blake Miller who performs as Your Friend",
    "Han shot first in the original version of Star Wars Episode IV",
    "Hu's on first. Watt's on second.",
    "person can typically get an identification card through the Department of Motor Vehicles",
    "The average MSRP of Ram vehicles is $30,204",
    "The noun 'doing' is usually defined as action",
    "noun, rating, can have a few meanings",
    "The pronoun 'it' is usually defined as used to represent an inanimate thing",
    "Sony traded at 6,737.00 Japanese yen on the Tokyo Stock Exchange",
    "Ask Me's performer is Elvis Presley",
    "I always enjoy telling a good story. To hear one, just say",
    "AirPlay is located in United States",
    "On Saturday, February 1 in International Exhibitions, United States beat Costa Rica 1 nil at StubHub Center in Carson, California.",
    "A company, abbreviated as co., is a legal entity representing an association of people",
    "Minecraft is Stephen's 5th Let's Play",
    "Jagermeister German imported liqueur has about 103 calories in every one ounce shot",
    "So is a noun meaning sol, the syllable used for the fifth tone of a diatonic scale",
    "Dog is a subspecies of living things",
    "Website, operating system, API, video game, ERP system and others",
    "You can tell me to turn up the volume, play music, create a to-do list",
    "is: third‐person singular personal pronoun",
    "Else's definitions are other; in addition to previously mentioned items and otherwise, if not",
    "play music from your Amazon library, just ask for the track",
    "The Beatles was released on October 6, 1969",
    "Something in My Soul (feat. Rob Jones)'s performer is Lovestreet",
    "'Toys' is a form of the noun 'toy', which means an object, often a small representation of something familiar",
    "Anything But Love?'s duration is 40 minutes",
    "New York, New York is a city in Kings County, New York",
    "census geographic units in New Brunswick",
    "Canada is a country in Americas",
    "traditionally venerated as the protomartyr or first martyr of Christianity",
    "Sleeping, the post-hardcore band from Long Island",
    "You can find support on Amazon.com and in the Help and Feedback section of the Alexa App.",
    "skill://amzn1.alexa-speechlet-client.DOMAIN:A2S/EnableApp?happyPrompt=ok%2C%20the%20skill%20save%20the%20",
    "Someone Else is a 2006 British Comedy-drama independent film.",
    "Apple Inc. is an American multinational technology company headquartered in Cupertino",
    "The noun 'movie' is usually defined as motion picture.",
    "The noun 'three' is usually defined as a cardinal number",
    "Competition arises whenever at least two parties strive for a goal which cannot be shared",
    "G or g is the seventh letter of the ISO basic Latin alphabet. Its name in English is gee, plural gees.",
    "Lary died on December 13, 2017 in Northport, Alabama of pneumonia at the age of 87.",
    "Anheuser-Busch Companies, Inc. operates the largest brewing company in the United States in volume with a 48.8% share of beer sales.",
    "Reisigl was a Major League Baseball pitcher",
    "the thick white fluid containing spermatozoa that is ejaculated by the male genital tract.",
    "I can sing dozens of songs! You can ask me to sing: a spooky song",
    "NHRA News Today, better known as NHRA News Now, delivers comprehensive information on the National Hot Rod Association",
    "Alexa, can you change your wake word",
    "noun 'name' is usually defined as a word or a combination of words",
    "In the Alexa app, select the Devices icon and select your device",
    "Those People is a 2015 drama film",
    "I don't know the population of australasian snappers, however their conservation status is least concern",
    "The most obvious way an author can use honesty in their work is simply by writing an honest character",
    "Sorry, I don't know The Saga of You, Confused Destroyer of Planets' theme.",
    "A W-2 paperless employee is someone employed by a company",
    "Michigan is a state located in the Midwestern United States.",
    "original video animation, made for TV movie and Feature film",
    "nipple is a raised region of tissue on the surface of the breast",
    "When the United States Social Security program was created, persons older than 65 numbered only",
    "HIDING IN PLAIN SIGHT is a book by Sandra Kemayou published in 2017",
    "pronoun 'yours' is usually defined as a form of the possessive case",
    "I know about twenty-four House Lives including"
])

def test_should_filter_out_response(text):
    result = FactualQuestionHandler.should_filter_out_response(text)
    assert result is True


@pytest.mark.parametrize("text, expected_result", [
    ("Film, also called movie or motion picture, is a visual art-form used to simulate experiences that communicate ideas, stories, perceptions, feelings, beauty or atmosphere, by the means of recorded or programmed moving images, along with sound other sensory stimulations. <break time='150ms'/> The word \"cinema\", short for cinematography, is often used to refer to filmmaking and the film industry, and to the art form that is the result of it.",
     "Film, also called movie or motion picture, is a visual art-form used to simulate experiences that communicate ideas, stories, perceptions, feelings, beauty or atmosphere, by the means of recorded or programmed moving images, along with sound other sensory stimulations."),
    ("According to Reuters as of June 1, 2020, more than 6.2 million cases of COVID-19 have been confirmed worldwide. The countries most affected by the outbreak include the United States, Brazil, and Russia. The virus has caused over 371,000 deaths worldwide, with over 2.5 million recoveries. For more information, try asking, “Is the coronavirus in Spain?” Or, “How many cases of coronavirus are in Iran?",
     "According to Reuters as of June 1, 2020, more than 6.2 million cases of COVID-19 have been confirmed worldwide. The countries most affected by the outbreak include the United States, Brazil, and Russia.")
])
def test_process_evi_response(text, expected_result):
    result = FactualQuestionHandler.process_evi_response(text)
    assert result == expected_result


@pytest.mark.parametrize("text", [
    "The noun 'scandal' is usually defined as a disgraceful or discreditable action",
    "The Tooth Fairy is a fantasy figure of early childhood in Western and Western-influenced cultures.",
    "Interstellar is a 2014 adventure film starring Matthew McConaughey",
    "Jogging is a form of trotting or running at a slow or leisurely pace",
    "2 grams of table salt is equivalent to about 1/3 teaspoons of table salt",
    "Harry Edward Styles is an English singer, songwriter, and actor"
])
def test_should_filter_out_response_false(text):
    result = FactualQuestionHandler.should_filter_out_response(text)
    assert result is False
