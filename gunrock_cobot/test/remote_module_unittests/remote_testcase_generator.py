"""Generate Unittest Test Cases for Each Module

# When writing unittest, check for three things. 
1. Does your newly changed code break our system?
2. Does your code crash?

"""
import os
import logging
from utils import get_working_environment, set_logger_level
import pandas as pd
import json
from test.remote_module_unittests.remote_test_utils import create_if_not_exists, clean_data, custom_returnnlp_clean

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

DEFAULT_UNITTEST_CASES = os.path.join(os.environ.get("COBOT_HOME", ""), "unittest_cases")
MODULE_NAMES = {}

def add_module_fields(module, d):
    if module is not None:
        d[module] = {}

    else:
        for name in MODULE_NAMES:
            d[module] = {}


class _TestCaseGenerator:
    def __init__(self, csv_root=None, module=None, req_fields=None):

        if not csv_root:
            csv_root = DEFAULT_UNITTEST_CASES
        create_if_not_exists(csv_root)

        # Get all the modules if module is None
        if not module:
            module_dirs = [os.path.join(csv_root, fpath) for fpath in os.listdir(csv_root) if os.path.isdir(os.path.join(csv_root, fpath))]
        else:
            module_dir = os.path.join(csv_root, module)
            if os.path.exists(module_dir):
                module_dirs = [module_dir]
            else:
                module_dirs = []

        self.test_case_dirs = module_dirs
        self.req_fields = req_fields
        self.module = module
        self._num_cases = None
        

    def _process_raw_row(self, row):
        """Turn a row in dataframes into a dictionary of req_fields element
        """
        if self.req_fields is None:
            req_fields = [k for k in list(row.keys()) if not k.startswith("Unnamed")]
        else:
            req_fields = set(self.req_fields).intersection(row.keys())
        ret = {field: clean_data(row[field]) for field in req_fields}
        custom_returnnlp_clean(ret["returnnlp"])
        if isinstance(ret["system_acknowledgement"], dict):
            ret["system_acknowledgement"] = [ret["system_acknowledgement"]]
        ret["template_manager"] = self.create_mock_template_manager
        add_module_fields(self.module, ret)
        return ret


    @property
    def generator(self):
        
        for test_dir in self.test_case_dirs:
            if not os.path.exists(test_dir):
                # TODO: Write Error - test dir doesn't exist
                continue
            test_csvs = os.listdir(test_dir)
            for test_csv in test_csvs:
                test_csv = os.path.join(test_dir, test_csv)
                if not os.path.exists(test_csv) or not test_csv.endswith(".csv"):
                    continue
                df = pd.read_csv(test_csv)
                for i, r in df.iterrows():
                    yield r["session_id"], self._process_raw_row(r)

    @property
    def num_cases(self):
        if self._num_cases is not None:
            return self._num_cases

        self._num_cases = 0
        for test_dir in self.test_case_dirs:
            if not os.path.exists(test_dir):
                continue
            test_csvs = os.listdir(test_dir)
            for test_csv in test_csvs:
                test_csv = os.path.join(test_dir, test_csv)
                if not os.path.exists(test_csv) or not test_csv.endswith(".csv"):
                    continue
                df = pd.read_csv(test_csv)
                self._num_cases += df.shape[0]
        return self._num_cases

    @property
    def create_mock_template_manager(self):
        return {"prev_hash":{}, "selector_history":[]}

if __name__ == '__main__':
    # Used to Debug, Deprecated
    test_generator = TestCaseGenerator()
    print(next(test_generator.iterator))
                
