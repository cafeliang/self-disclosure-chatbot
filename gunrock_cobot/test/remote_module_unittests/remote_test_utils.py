import os
import logging
from utils import get_working_environment, set_logger_level
import json
import math

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

REMOTE_HOME = os.path.join(os.environ.get("COBOT_HOME", ""), "gunrock_cobot", "docker")


TEST_PATHS = {"TECHSCIENCECHAT": os.path.join(REMOTE_HOME, "{}/app".format("techsciencechat"))}

def create_if_not_exists(path):
    if not os.path.exists(path):
        LOGGER.info("[SYSTEM UNITTEST] {} not found, creating now...".format(path))
        os.mkdir(path)

def clean_data(field_data):
    try:
        ret = remove_extra_field(json.loads(field_data))
        return ret
    except Exception as e:
        if isinstance(field_data, float):
            return None
        return field_data

def custom_returnnlp_clean(returnnlp_data):
    def retrieve_dialog_act(da):
        if "DA2" in da:
            return [da.pop("DA2"), da.pop("confidence2")]
        return ["", ""]
    if isinstance(returnnlp_data, list):
        for r in returnnlp_data:
            r["intent"] = r.pop("intent_classify2")
            r["sentiment"] = r.pop("sentiment2")
            r["topic"] = r.pop("topic2")
            dialog_act2 = r["dialog_act"]
            r["dialog_act2"] = retrieve_dialog_act(r["dialog_act"])
            r["dialog_act"] = [r["dialog_act"].pop("DA"), r["dialog_act"].pop("confidence")]
            

def remove_extra_field(d):
    """Remove M, L, S, BOOL, N from dictionary value
    """
    if isinstance(d, dict):
        if "M" in d:
            return remove_extra_field(d["M"])
        elif "L" in d:
            return [remove_extra_field(l) for l in d["L"]]
        elif "S" in d:
            return str(d["S"])
        elif "BOOL" in d:
            return bool(d["BOOL"])
        elif "N" in d:
            return float(d["N"])
        else:
            return {k: remove_extra_field(d[k]) for k in d}
    if isinstance(d, list):
        return [remove_extra_field(i) for i in d]
    if isinstance(d, float) and math.isnan(d):
        return None
    return d