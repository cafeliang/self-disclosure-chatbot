"""UniTest to ensure the questions are all detected.
"""

from test.remote_module_unittests.remote_test_utils import TEST_PATHS
import sys
import os
import unittest
from test.remote_module_unittests.remote_testcase_generator import _TestCaseGenerator
import logging
import json
from tqdm import tqdm
LIMIT = 1000
def add_to_test_path():
    test_path = TEST_PATHS.get("TECHSCIENCECHAT")
    if os.path.exists(test_path):
        sys.path.insert(0, test_path)
        # print(sys.path)

        # sys.path = [test_path]
        return True
    return False

import random

class TestTechscienceQuestionDetectors(unittest.TestCase):
    """ Test TopicUtils will focus on testing Topic Handler
        The goal of topic handler is to take in a text and
        1. Extract the NLU features that identify the topic user wants to talk about
        2. Map the user input to the corresponding resonse templates
     """

    def test_yes_no_questions(self):
        return

    def test_factual_questions(self):
        return

    def test_opinion_questions(self):
        return