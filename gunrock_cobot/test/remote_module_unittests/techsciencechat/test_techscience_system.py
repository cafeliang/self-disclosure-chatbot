"""Run Unittest on Techscience to remove all the system Bugs
"""
from test.remote_module_unittests.remote_test_utils import TEST_PATHS
import sys
import os
import unittest
from test.remote_module_unittests.remote_testcase_generator import _TestCaseGenerator
import logging
import json
from tqdm import tqdm
LIMIT = 1000
def add_to_test_path():
    test_path = TEST_PATHS.get("TECHSCIENCECHAT")
    if os.path.exists(test_path):
        sys.path.insert(0, test_path)
        # print(sys.path)

        # sys.path = [test_path]
        return True
    return False

import random
BREAK_SESSION = "amzn1.echo-api.session.52d9bd51-8364-4dad-b2ab-89f692a54308"
ignore_tests = set(["amzn1.echo-api.session.54c45c0e-16fd-4a3e-a36c-2193825c37e7", "amzn1.echo-api.session.d7912a63-7882-4a10-afdb-8210fd093e2f"])
class TestDailyUnitTest(unittest.TestCase):
    """ Test TopicUtils will focus on testing Topic Handler
        The goal of topic handler is to take in a text and
        1. Extract the NLU features that identify the topic user wants to talk about
        2. Map the user input to the corresponding resonse templates
     """

    @unittest.skip
    def test_all(self):
        visited_states = set()
        visited_transitions = set()
        if add_to_test_path():
            from techscience_automaton import TechScienceAutomaton
            logging.disable(logging.DEBUG)
            logging.disable(logging.INFO)
            logging.disable(logging.CRITICAL)
            logging.disable(logging.WARNING)

            test_cases = _TestCaseGenerator(module="techsciencechat", req_fields=["a_b_test", "system_acknowledgement", "text", "returnnlp", "central_elem", "last_module"])
            all_states = set(TechScienceAutomaton().states_mapping.keys())
            all_transitions = set(TechScienceAutomaton().transitions_mapping.keys())


            prev_session_id = ""
            prev_transition = ""
            result = {}
            i = 0
            run = False
            for session_id, input_data in tqdm(test_cases.generator, total=test_cases.num_cases):
                i += 1
                if session_id == BREAK_SESSION:
                    run = True
                if run and session_id not in ignore_tests:
                    if prev_session_id != session_id:

                        automaton = TechScienceAutomaton()
                        prev_session_id = session_id
                    else:
                        new_user_attribute_state = result.get("techsciencechat", {})
                        input_data["techsciencechat"] = new_user_attribute_state
                        
                    enter_transition = input_data.get("current_transition", "")
                    visited_transitions.add(enter_transition)
                    result = automaton.transduce(input_data)
                    error_state = result.get("techsciencechat", {}).get("prev_state", None)
                    error_template = result.get("techsciencechat", {}).get("error_response", None)
                    visited_states.add(error_state)
                    
                    text = result.get("response", "")
                    self.assertNotEqual(error_state, "s_error_handling",  msg="Text:{0} \n Response: {1}, SessionID: {2}".format(input_data, result, session_id))
                    self.assertFalse(error_template,  msg="Result: {0}, previous_transition {1}, SessionID {2}".format(result, prev_transition, session_id))
                    self.assertNotEqual(text, "", msg="Text:{0} \n Response: {1}, SessionID: {2}".format(input_data, result, session_id))
                    prev_transition = result.get("techsciencechat", {}).get("current_transition", None)
                    if i >= LIMIT and LIMIT > 0:
                        break

            # Disable this test until finding all the cases
            print("Unvisited states {}".format(" ".join(all_states - visited_states)))
            # self.assertEquals(len(all_states), len(visited_states), msg="Not all states are visited- {0}".format(all_states - visited_states))
            # self.assertEquals(len(all_transitions), len(visited_transitions), msg="Not all transitions are visited- {0}".format(all_transitions - visited_transitions))
        else:
            self.assertFalse(True, msg="Unittest Techscience Not Run")

    # def test_intro_to_initresume(self):
    #     if add_to_test_path():
    #         from techscience_automaton import TechScienceAutomaton
    #         logging.disable(logging.DEBUG)
    #         logging.disable(logging.INFO)
    #         logging.disable(logging.CRITICAL)
    #         logging.disable(logging.WARNING)

    #         test_cases = _TestCaseGenerator(module="techsciencechat", req_fields=["a_b_test", "system_acknowledgement", "text", "returnnlp", "central_elem", "last_module"])
    #         all_states = set(TechScienceAutomaton().states_mapping.keys())
    #         all_transitions = set(TechScienceAutomaton().transitions_mapping.keys())


    #         visited_session = set()
    #         visited_transitions = set()
    #         result = {}
    #         i = 0
    #         for session_id, input_data in tqdm(test_cases.generator, total=test_cases.num_cases):
    #             i += 1
    #             if i >= LIMIT:
    #                 break
    #             if session_id in visited_session:
    #                 continue

    #             automaton = TechScienceAutomaton()
    #             result = automaton.transduce(input_data)
    #             current_transition = result.get("techsciencechat",{}).get("prev_transition", None)
    #             visited_transitions.add(current_transition)
    #             visited_session.add(session_id)    

    #         self.assertTrue(len(visited_transitions) <= 2, msg="Only Two Visited States are allowed, t_init and t_init_resume.  Actual Transitions {}".format(visited_transitions))
    #         for tran in visited_transitions:
    #             if tran != "t_init" and tran != "t_init_resume":
    #                 self.assertFalse(True, msg="{} found in init transition".format(tran))

    @unittest.skip("temporarily skip")
    def test_specific_id(self):
        logging.disable(logging.DEBUG)
        logging.disable(logging.INFO)
        logging.disable(logging.CRITICAL)
        logging.disable(logging.WARNING)        # logging.disable(logging.WARNING)
        if add_to_test_path():
            from techscience_automaton import TechScienceAutomaton
            test_cases = _TestCaseGenerator(module="techsciencechat", req_fields=["a_b_test", "system_acknowledgement", "text", "returnnlp", "central_elem", "last_module"])
            
            custom_session_id = BREAK_SESSION
            result = {}
            automaton = TechScienceAutomaton()

            for session_id, input_data in test_cases.generator:
                if custom_session_id == session_id:
                    print(input_data.get("text", [""]) + " \n \n ")
                    
                    input_data["techsciencechat"] = result.get("techsciencechat", {})
                    result = automaton.transduce(input_data)
                    error_state = result.get("techsciencechat", {}).get("prev_state", None)
                    print(error_state + "\n \n")
                    text = result.get("response", "")
                    print(text)
                    error_template = result.get("techsciencechat", {}).get("error_response", None)
                    prev_transition = result.get("techsciencechat", {}).get("current_transition", None)
                    self.assertNotEqual(error_state, "s_error_handling", msg=result)
                    self.assertNotEqual(text, "", msg="Text:{0} \n Response: {1}".format(input_data, result))

                    self.assertFalse(error_template,  msg="Result: {0}, previous_transition {1}, SessionID {2}".format(result, prev_transition, session_id))

if __name__ == '__main__':

        unittest.main()