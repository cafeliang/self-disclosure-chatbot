from response_generator.special.jokes_response_generator import SayFunnyHandler

utterances = [
    "tell me a secret",
    "do you fart",
    "give me money",
    "who built your software",
    "he he he",
    "can i name you something",
    "tell me about myself",
    "play with my baby",
    "how stupid are you",
    "can you kill me",
    "tell alexa something for me",
    "tell me a lie",
    "what state do i live in",
    "can you be my friend",
    "hi siri",
    "do you know cortana",
    "are you spying on me",
    "i love you",
    "i like you",
    "can you speak in an accent",
    "how smart are you",
    "i want to unplug you",
    "tell me a joke",
    "how much are you worth",
    "what's inside you",
    "roast me please",
    "what is the extent of your knowledge",
    "what did trump tweet recently",
    "are you better than Cortana"
]

input = {}
for utterance in utterances:
    # Usage: pass in input_data with text 
    input["text"] = utterance
    intentHandler = SayFunnyHandler(input)
    response = intentHandler.generateResponse()
    #================================================
    print(input["text"])
    print(response+"\n")
