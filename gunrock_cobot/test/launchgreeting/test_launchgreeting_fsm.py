import itertools
import pytest
from unittest.mock import patch
from types import SimpleNamespace

from response_generator.fsm2 import FSMAttributeAdaptor
from template_manager import Template

from response_generator.social_transition.launchgreeting.response_generator import LaunchGreetingModule
from response_generator.social_transition.launchgreeting.states import LGState

from test.launchgreeting.data import fsm_states


@pytest.mark.skip(reason="new fsm2 changed mocking procedure. Haven't updated this func to it")
@pytest.mark.parametrize('case_key, inputs, outputs', [
    (key, input_dict['inputs'], output_dict['outputs'])
    for key, (input_dict, output_dict) in fsm_states.cases.items()
])
@patch.object(Template.greeting, 'speak', side_effect=Template.greeting.speak)
def test_state_transition_and_template(tm_speak, case_key, inputs, outputs, caplog):

    caplog.set_level('ERROR', 'nlu.dataclass.returnnlp')
    caplog.set_level('ERROR', 'nlu.dataclass.central_element')
    caplog.set_level('ERROR', 'nlu.typecheck')
    caplog.set_level('ERROR', 'pytest.input')
    caplog.set_level('WARNING', 'transitions.core')
    caplog.set_level('WARNING', 'template_manager.template')

    ua_ref, input_data = inputs['ua_ref'], inputs['input_data']

    ua_ref = SimpleNamespace(**ua_ref['map_attributes'])
    state_manager = SimpleNamespace(current_state=SimpleNamespace(bot_ask_open_question=None))

    fsm = LaunchGreetingModule(
        FSMAttributeAdaptor('launchgreeting', ua_ref),
        input_data,
        Template.greeting,
        LGState,
        first_state='initial',
        state_manager_last_state=None
    )
    fsm.__post_init__(state_manager)

    response = fsm.generate_response()
    context = f"case_key = {case_key}; text = {inputs['input_data']['text']}; response = {response}"
    assert fsm.state == outputs['state'], context
    tm_speak.assert_has_calls(outputs['assert_has_calls'])
    assert response, context
