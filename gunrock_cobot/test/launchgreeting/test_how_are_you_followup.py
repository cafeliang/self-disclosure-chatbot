import pytest
from types import SimpleNamespace

from nlu.constants import Positivity
from template_manager import Template
from response_generator.fsm2 import FSMAttributeAdaptor, Tracker

from response_generator.social_transition.launchgreeting.response_generator import LaunchGreetingModule
from response_generator.social_transition.launchgreeting.states import LGState
from response_generator.social_transition.launchgreeting.utils import utils

from test.launchgreeting.data import how_are_you_followup


@pytest.mark.parametrize('inputs, outputs', [
    (input_dict['inputs'], output_dict['outputs'])
    for input_dict, output_dict in how_are_you_followup.cases
])
def test_name_correction(inputs, outputs, caplog):

    caplog.set_level('ERROR', 'nlu.dataclass.returnnlp')
    caplog.set_level('ERROR', 'nlu.dataclass.central_element')
    caplog.set_level('ERROR', 'nlu.utils.type_checking')
    caplog.set_level('ERROR', 'pytest.input')
    caplog.set_level('WARNING', 'transitions.core')
    caplog.set_level('WARNING', 'template_manager.template')

    ua_ref_raw, input_data = inputs['ua_ref'], inputs['input_data']

    ua_ref = SimpleNamespace(**ua_ref_raw['map_attributes'])
    state_manager = SimpleNamespace(current_state=SimpleNamespace(bot_ask_open_question=None))

    ua = FSMAttributeAdaptor('launchgreeting', ua_ref)
    fsm = LaunchGreetingModule(
        ua,
        input_data,
        Template.greeting,
        LGState,
        first_state='initial',
        state_manager_last_state=None
    )
    fsm.__post_init__(state_manager)

    tracker = Tracker(input_data, ua, None)

    positivity = utils.how_are_you_sentiment(tracker)

    context = f"text = {tracker.input_text}"
    assert positivity == Positivity[outputs['sentiment']], context
