import pytest
from types import SimpleNamespace

from response_generator.fsm2 import FSMAttributeAdaptor, Tracker
from template_manager import Template
from user_profiler.user_profile import UserProfile

from response_generator.social_transition.launchgreeting.response_generator import LaunchGreetingModule
from response_generator.social_transition.launchgreeting.states import LGState
from response_generator.social_transition.launchgreeting.transitions import conditions
from response_generator.social_transition.launchgreeting.utils import utils

from test.launchgreeting.data import got_name_cases


@pytest.mark.parametrize('inputs, outputs', [
    (input_dict['inputs'], output_dict['outputs'])
    for input_dict, output_dict in got_name_cases.cases
])
def test_got_name(inputs, outputs, caplog):

    caplog.set_level('DEBUG')
    caplog.set_level('ERROR', 'nlu.dataclass.returnnlp')
    caplog.set_level('ERROR', 'nlu.dataclass.central_element')
    caplog.set_level('ERROR', 'nlu.typecheck')
    caplog.set_level('ERROR', 'pytest.input')
    caplog.set_level('WARNING', 'transitions.core')
    caplog.set_level('WARNING', 'template_manager.template')

    ua_ref, input_data = inputs['ua_ref'], inputs['input_data']

    ua_ref = SimpleNamespace(**ua_ref['map_attributes'])
    state_manager = SimpleNamespace(current_state=SimpleNamespace(bot_ask_open_question=None))

    ua = FSMAttributeAdaptor('launchgreeting', ua_ref)
    fsm = LaunchGreetingModule(
        ua,
        input_data,
        Template.greeting,
        LGState,
        first_state='initial',
        state_manager_last_state=None
    )
    fsm.__post_init__(state_manager)

    tracker = Tracker(input_data, ua, None)
    # print("DJFOIJDSFOIDJSOFIDs:", tracker)

    # utils.check_input_for_name(tracker)

    result = tuple(map(bool, (
        conditions.got_name_common_name(tracker),
        conditions.got_name_uncommon_name(tracker),
        conditions.got_name_no_name(tracker),
        conditions.got_name_decline(tracker),
    )))
    expected = tuple(map(bool, outputs['result']))

    context = f"text = {tracker.input_text}"
    assert result == expected, context

    # if expected == tuple(map(bool, [1, 0, 0, 0])):
    #     fsm.generate_response()
    #     user_model = UserProfile(tracker.user_attributes.ua_ref)
    #     print("DJNFOIDJFOIDJSOFJ", user_model)
    #     assert user_model.username == outputs['name'], context + f"user_model = {user_model}"

    # elif expected == tuple(map(bool, [0, 1, 0, 0])):
    #     fsm.generate_response()
    #     user_model = UserProfile(tracker.user_attributes.ua_ref)
    #     assert user_model._username_uncommon == outputs['name_uncommon'], context + f"user_model = {user_model}"

    # elif expected == tuple(map(bool, [0, 0, 0, 0])):
    #     fsm.generate_response()
    #     user_model = UserProfile(tracker.user_attributes.ua_ref)
    #     if 'name' in outputs:
    #         assert user_model.username == outputs['name'], context
    #     elif 'name_uncommon' in outputs:
    #         assert user_model._username_uncommon == outputs['name_uncommon'], context

