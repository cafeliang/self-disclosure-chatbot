import pytest

from template_manager import Template

from response_generator import fsm2


@pytest.mark.parametrize('module_name, user_attributes_ref, template', [
    (
        'moviechat_user',
        {
            "moviechat_user": {
                "current_loop": {}, "subdialog_context": {}, "ask_movietv_repeat": True,
                "current_transition": "t_ask_movietv", "current_state": "s_echo", "flow_interruption": False,
                "not_first_time": True, "propose_continue": "CONTINUE", "tags": ["movie_user_qa_answer_only"],
                "current_subdialog": None, "propose_topic": None, "candidate_module": None
            },
            "propose_topic": None,
            "template_manager": {
                "prev_hash": {
                    "MUoGKQ": ["Vc4kFA"], "jSoKmw": ["C6kjgg"], "ocgSRA": ["c4IZmw", "VS0VHQ"], "KxoF0w": [],
                    "/M0N7w": ["t3gtIQ", "5SE7MQ"], "sicL6A": []
                },
                "selector_history": [
                    "error::s_fragments/reprompt/thanks_for_sharing", "error::s_fragments/reprompt/feedback",
                    "error::reprompt", "movies::what_movie_do_you_like"
                ]
            }
        },
        Template.movies
    )
])
def test_init(module_name: str, user_attributes_ref, template: Template, caplog):

    class MovieState(fsm2.fsm.State):
        pass

    class InitState(MovieState):
        name = 'init'

        def run(self, dispatcher: fsm2.utils.dispatcher.Dispatcher, tracker: fsm2.utils.tracker.Tracker):
            dispatcher.respond('running init')
            return [fsm2.events.NextState('state2')]

    class State2State(MovieState):
        name = 'state2'

        def run(self, dispatcher: fsm2.utils.dispatcher.Dispatcher, tracker: fsm2.utils.tracker.Tracker):
            dispatcher.respond('running state2')
            return [fsm2.events.NextState('state3', jump=True)]

    class State3State(MovieState):
        name = 'state3'

        def run(self, dispatcher: fsm2.utils.dispatcher.Dispatcher, tracker: fsm2.utils.tracker.Tracker):
            dispatcher.respond('running state3')
            return [fsm2.events.NextState('init')]

    caplog.set_level('DEBUG')

    ua_adaptor = fsm2.utils.attribute_adaptor.FSMAttributeAdaptor(module_name, user_attributes_ref)

    for turn_response in ['running init', 'running state2 running state3', 'running init']:

        fsm = fsm2.fsm.FSMModule(
            user_attribute_adaptor=ua_adaptor,
            input_data={},
            template=template,
            state_class=MovieState
        )
        assert fsm

        response = fsm.generate_response()
        assert response == turn_response
    # assert False
