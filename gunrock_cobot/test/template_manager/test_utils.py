import pytest
from types import SimpleNamespace

from template_manager import Template, TemplateManager


def test_get_child_keys():
    ua_ref = SimpleNamespace()

    tm_greet = TemplateManager(Template.greeting, ua_ref)

    keys = tm_greet.get_child_keys('social_template')
    expected = ['ANIMALCHAT', 'BOOKCHAT', 'FOODCHAT', 'GAMECHAT', 'HOLIDAYCHAT',
                'MOVIECHAT', 'MUSICCHAT', 'NEWS', 'PSYPHICHAT', 'SPORT', 'TECHSCIENCECHAT', 'TRAVELCHAT']

    assert expected == keys


@pytest.mark.parametrize('total, selector', [
    (1, 'whats_my_name/no_name'),
    (2, 'social_template/MOVIECHAT'),
    (3, 'social_template/ANIMALCHAT'),
])
def test_count_used_utterances(total, selector):

    ua_ref = SimpleNamespace()
    tm_music = TemplateManager(Template.greeting, ua_ref)

    counts = [tm_music.count_used_utterances(selector)]
    for i in range(6):
        tm_music.speak(selector, {})
        # context = str(dict(
        #     count=tm_music.count_used_utterances(selector),
        #     selector_history=getattr(ua_ref, 'template_manager', SimpleNamespace())
        # ))
        counts += [tm_music.count_used_utterances(selector)]
        # assert tm_music.count_used_utterances(selector) == i, context
    
    print(counts)
    assert counts, str(counts)
