import unittest
from pprint import pprint
from unittest import mock

from template_manager import preprocess


@unittest.skip('read_yml removed')
def test_read_yaml():

    test_file = './response_templates/games_template.yml'

    import yaml 
    truth = yaml.load(open(test_file))

    template = preprocess.read_yml(test_file)

    # compare
    import json
    assert json.dumps(truth, sort_keys=True) == json.dumps(template, sort_keys=True)


def test_gen_template_data():
    template_data = preprocess.transform_templates()
    pprint(template_data, width=360)

    # assert False
