import logging
import pytest
import random
import sys
import unittest.mock as mock
import yaml
from types import SimpleNamespace
from typing import Dict, Tuple
from unittest import skip
import os

from template_manager import Template, TemplateManager, TemplateUserAttributes
from template_manager.formatter import TemplateFormatter
from template_manager.migration import Templates as Template_C
from template_manager.template import traverse
from template_manager.utils import hash_utterance

TEMPLATE_ROOT = os.path.join(os.getenv("COBOT_HOME"), "gunrock_cobot", "response_templates")


logging.basicConfig(level=logging.WARNING, stream=sys.stdout)


class YMLDirectAccessor:

    def __init__(self, template: Template, ua_ref):
        self.template = template
        self.tua = TemplateUserAttributes(ua_ref)

    @property
    def _data(self) -> dict:
        return yaml.load(
                open(os.path.join(TEMPLATE_ROOT, '{}.yml'.format(self.template.value))),
                Loader=yaml.FullLoader
            )

    def selectors(self):
        selectors = []

        def dfs(obj: dict, **kwargs):
            for k, v in obj.items():
                path = (kwargs.get('path') or []) + [k]
                if isinstance(v, dict):
                    dfs(v, **{**kwargs, **{
                        'path': path,
                    }})
                elif isinstance(v, list):
                    selectors.append('/'.join(path))

        dfs(self._data)
        return selectors

    def slots(self, selector: str):
        tua_mock = TemplateUserAttributes(SimpleNamespace())
        slots = {}
        utt_list = traverse(self._data, selector)
        formatter = TemplateFormatter(self.template)
        for utt in utt_list:
            if isinstance(utt, dict):
                continue
            while True:
                try:
                    formatter.format_string(
                            utt, selector, slots, tua_mock
                        )
                    break
                except KeyError as e:
                    key = e.args[0]
                    slots[key] = '{{{}}}'.format(key)
        return slots

    def speak(self, selector: str, slots: Dict[str, str], embedded_info: dict = None) -> str:

        utt_list = traverse(self._data, selector)
        utt_list[:] = [(utt, hash_utterance(self.template, selector, utt)) for utt in utt_list]

        entry, utt_id = self.template._select_utterance(utt_list, selector, self.tua)
        utterance = self.template._str_dict_selector(entry, embedded_info)

        if utterance is not None and isinstance(utterance, str):
            # string interpolation
            def _speak_mock(selector_m, slots_m, ua_m, embedded_info_m=None):
                print(f"selector_m: {selector_m}; slots_m: {slots_m}; embedded_info_m: {embedded_info_m}")
                return self.speak(selector_m, slots_m, embedded_info_m)

            template_mock = Template[self.template.name]
            template_mock.speak = _speak_mock
            formatter = TemplateFormatter(template_mock)
            utterance = formatter.format_string(
                    utterance,
                    selector,
                    slots,
                    self.tua
                )
        else:
            utterance = entry

        # append to history when everything is successful
        self.tua.append_hash(self.template, selector, utt_id)

        return utterance

    def utterance(self, selector: str, slots: Dict[str, str]) -> Tuple[str, dict]:
        """
        Mimic compatibility mode
        """

        utt_list = traverse(self._data, selector)

        utt = random.choice(utt_list)

        if isinstance(utt, str):
            formatter = TemplateFormatter(self.template)
            utt = formatter.format_string(
                    utt, selector, slots, self.tua
                )

        return utt


@skip('this is only for testing the test script is working')
def test_hello_world():
    ua_ref = SimpleNamespace()

    tm = TemplateManager(Template.sys_rg, ua_ref)
    utt = tm.speak('say_comfort/normal', {})
    # print(utt)
    # assert False


def hashing_append(mode):
    templates_to_test = [t for t in Template]

    for template_to_test in templates_to_test:

        expected = YMLDirectAccessor(template_to_test, SimpleNamespace())

        selectors = expected.selectors()
        selectors = random.choices(selectors, k=len(selectors) // 10)

        for selector in selectors:
            # print(selector)

            ua_ref_tm = SimpleNamespace()

            slots = expected.slots(selector)

            if mode == 'tm':
                tm = TemplateManager(template_to_test, ua_ref_tm)
            elif mode == 'templates':
                tm = Template_C[template_to_test.name]
            elif mode == 'tm_c':
                tm = TemplateManager(Template_C[template_to_test.name], ua_ref_tm)

            selector_id = hash_utterance(template_to_test, selector, None)

            template_count = tm.count_utterances(selector)

            count_list = set([1, 2, template_count - 1, template_count, template_count + 1])

            for count in range(1, max(count_list) + 1):
                if mode == 'tm':
                    utt = tm.speak(selector, slots)
                elif mode == 'templates':
                    utt = tm.utterance(selector, slots, ua_ref_tm)
                elif mode == 'tm_c':
                    utt = tm.utterance(selector, slots)

                if count in count_list:
                    # print(f"template: {template_to_test.value}, selector: {selector}; utt: {utt}")
                    # print(f"template_count: {template_count}, count: {count}")

                    # print(ua_ref_tm)
                    ua_hash = ua_ref_tm.template_manager['prev_hash'][selector_id]

                    if template_count == 1:
                        assert len(ua_hash) == 0

                    elif count <= template_count:
                        assert len(ua_hash) == count

                    else:
                        assert len(ua_hash) == count % template_count + 1

#TODO
@skip('resolve this skip when code is finalized')
def test_hashing_append():
    hashing_append(mode='tm')

#TODO
@skip('resolve this skip when code is finalized')
@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_hashing_append_compatibility():
    hashing_append(mode='templates')

#TODO
@skip('resolve this skip when code is finalized')
@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_hashing_append_compatibility_tm():
    hashing_append(mode='tm_c')


def mock_random_choice(l: list):
    # print(f"possible choices: {l}")
    if not l or len(l) < 1:
        return ""
    # choice = sorted(l)[0]
    choice = l[0]
    # print(f"choosing: {choice}")
    return choice


@mock.patch('random.choice', mock_random_choice)
def test_template_speak():

    templates_to_test = [t for t in Template]
    templates_to_test = [Template.sys_rg]  # for debugging test
    templates_to_test = [Template.games]  # for debugging test

    asserts_map = {}

    for template_to_test in templates_to_test:
        ua_ref_truth = SimpleNamespace()
        ua_ref_tm = SimpleNamespace()

        truths = YMLDirectAccessor(Template[template_to_test.name], ua_ref_truth)
        # pprint(truths.selectors())

        templates = TemplateManager(template_to_test, ua_ref_tm)

        asserts = []

        for selector in truths.selectors():
            if selector != 'intro/what_game':
                continue
            slots = truths.slots(selector)
            # TODO: Somehow they are sharing hashes

            # print("--- TRUTH SPEAK ---")
            # truth = truths.speak(selector, slots)
            # print("--- UTT SPEAK ---")
            utt = templates.speak(selector, slots)

            # asserts.append((truth == utt, truth, utt))

        asserts.append((vars(ua_ref_truth) == vars(ua_ref_tm), ua_ref_truth, ua_ref_tm)),

        # truths.tua.prev_hash.clear()
        # templates.user_attributes.prev_hash.clear()

        for match, truth, utt in asserts:
            if not match:
                print("TRUTH:\t{}\nUTT:\t{}".format(truth, utt))

        asserts_map[template_to_test] = asserts
    # print(asserts_map)
    # assert all(i[0] for asserts in asserts_map.values() for i in asserts)
    # TODO:  This test is disabled since there is no input for i.  Futher inqury is needed.


@pytest.mark.filterwarnings('ignore::DeprecationWarning')
@mock.patch('random.choice', lambda l: l[0])
def test_compatibility_speak():

    templates_to_test = [t for t in Template]
    templates_to_test = [Template.transition]  # for debugging test

    asserts_map = {}

    for template_to_test in templates_to_test:
        ua_ref_truth = SimpleNamespace()
        ua_ref_tm = SimpleNamespace()

        truths = YMLDirectAccessor(template_to_test, ua_ref_truth)
        # pprint(truths.selectors())

        templates = Template_C[template_to_test.name]

        asserts = []

        for selector in truths.selectors():
            slots = truths.slots(selector)
            truth = truths.utterance(selector, slots)
            utt = templates.utterance(selector, slots, ua_ref_tm)

            asserts.append((truth == utt, truth, utt))

        asserts.append((vars(ua_ref_truth) == vars(ua_ref_tm), ua_ref_truth, ua_ref_tm)), 

        for match, truth, utt in asserts:
            if not match:
                print("TRUTH:\t{}\nUTT:\t{}".format(truth, utt))

        asserts_map[template_to_test] = asserts

        # print('ua_ref', ua_ref)
    #TODO
    # assert all(i[0] for asserts in asserts_map.values() for i in asserts)


@pytest.mark.filterwarnings('ignore::DeprecationWarning')
@mock.patch('random.choice', lambda l: l[0])
def test_compatibility_speak_tm():

    templates_to_test = [t for t in Template]
    templates_to_test = [Template.sports]  # for debugging test

    asserts_map = {}

    for template_to_test in templates_to_test:
        ua_ref_truth = SimpleNamespace()
        ua_ref_tm = SimpleNamespace()

        truths = YMLDirectAccessor(template_to_test, ua_ref_truth)
        # pprint(truths.selectors())

        templates = TemplateManager(Template_C[template_to_test.name], ua_ref_tm)

        asserts = []

        for selector in truths.selectors():
            slots = truths.slots(selector)
            truth = truths.utterance(selector, slots)
            utt = templates.utterance(selector, slots)

            asserts.append((truth == utt, truth, utt))

        asserts.append((vars(ua_ref_truth) == vars(ua_ref_tm), ua_ref_truth, ua_ref_tm)), 

        for match, truth, utt in asserts:
            if not match:
                print("TRUTH:\t{}\nUTT:\t{}".format(truth, utt))

        asserts_map[template_to_test] = asserts

        # print('ua_ref', ua_ref)

    # assert all(i[0] for asserts in asserts_map.values() for i in asserts)


@skip("This is not needed")
def test_dict_utt():

    ua_ref = SimpleNamespace()

    truths = YMLDirectAccessor(Template.transition, ua_ref)
    truth = truths.speak('transition_56/MOVIECHAT/BOOKCHAT', {})

    templates = TemplateManager(Template.transition, ua_ref)
    args = {}
    utt = templates.speak('transition_56/MOVIECHAT/BOOKCHAT', {}, embedded_info=args)

    print(truth)
    print(utt)
    print(args)

    # assert False


def test_format_string():
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)

    ua_ref = SimpleNamespace()

    expected_template = YMLDirectAccessor(Template.sys_rg, ua_ref)
    tm = TemplateManager(Template.sys_rg, ua_ref)

    utt = tm.speak('easter_egg/spell_name/user', {})

    # print(utt)

    # assert False

