
import unittest
import pytest
from types import SimpleNamespace

from template_manager.manager import Template, TemplateManager


class TestSelectorHistory:

    @pytest.mark.xfail(reason="waiting for Austin to fix it")
    def test_append(self):
        ua_ref = SimpleNamespace()
        template = Template.games
        tm = TemplateManager(template, ua_ref)

        selectors = [
            "chitchat/postfix/another_game",
            "qa/favorite_genre/default",
            "qa/tell_me_more_about_game/decline",
            "intro/what_game",
        ]

        target_selector_history = [
            'games::chitchat/postfix/another_game',
            'shared::t_fragments/ack_i_see',
            'games::qa/favorite_genre/default',
            'shared::t_fragments/ack_i_see',
            'games::qa/tell_me_more_about_game/decline',
            'shared::t_fragments/affirm',
            'games::intro/what_game'
        ]

        for selector in selectors:
            tm.speak(selector, {})

        print(ua_ref.__dict__)
        assert ua_ref.template_manager['selector_history'] == target_selector_history
