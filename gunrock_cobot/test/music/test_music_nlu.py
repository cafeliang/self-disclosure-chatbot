import pprint
import pytest

from response_generator.music.utils import dynamodb


pp = pprint.PrettyPrinter(width=180)


@pytest.mark.parametrize('name, expected', [
    ('billie eilish', True)
])
def test_dynamodb_get_artist_details(name: str, expected):
    response = dynamodb.get_artist_details(name)
    pp.pprint(response)
    assert bool(response) is expected


@pytest.mark.parametrize('name, expected', [
    # ('billie eilish', False),
    ('billie holiday', True),
])
def test_dynamodb_get_songs_by_artist(name: str, expected):
    response = dynamodb.get_songs_by_artist(name)
    pp.pprint(response)
    assert bool(response) is expected
