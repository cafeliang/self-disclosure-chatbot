import itertools
import pytest
from types import SimpleNamespace

from response_generator.fsm2 import FSMAttributeAdaptor, FSMModule, Tracker
from template_manager import Template

from response_generator.music.states import MusicState
from response_generator.music.utils import regex_match
from response_generator.music.constants import regex_lists


from test.music.data import sing_cases


@pytest.mark.parametrize('inputs, outputs', sing_cases.cases)
def test_sing_a_song(inputs, outputs, caplog):

    caplog.set_level('DEBUG')
    caplog.set_level('ERROR', 'nlu.dataclass.returnnlp')
    caplog.set_level('ERROR', 'nlu.dataclass.central_element')
    caplog.set_level('ERROR', 'nlu.typecheck')
    caplog.set_level('ERROR', 'pytest.input')
    caplog.set_level('WARNING', 'transitions.core')
    caplog.set_level('WARNING', 'template_manager.template')

    ua_ref = SimpleNamespace(**inputs['user_attributes']['map_attributes'])
    ua = FSMAttributeAdaptor('musicchat', ua_ref)
    input_data = inputs['input_data']
    state_manager = SimpleNamespace(last_state={})

    # fsm = FSMModule(
    #     ua,
    #     input_data,
    #     Template.music,
    #     MusicState,
    #     first_state='initial',
    #     state_manager_last_state=state_manager.last_state
    # )

    tracker = Tracker(input_data, ua, state_manager.last_state)

    result = regex_match.is_command_sing_a_song(tracker)
    expected = outputs['sing_a_song']
    intents = [list(itertools.chain(seg.intent.topic, seg.intent.sys, seg.intent.lexical)) for seg in tracker.returnnlp]
    match = regex_match.match(tracker, **regex_lists['command_sing_a_song'])
    context = f"text = {tracker.input_text}, intents = {intents}, match = {match}"
    assert bool(result) == expected, context
