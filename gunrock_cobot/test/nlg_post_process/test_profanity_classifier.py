import pytest

from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier
from nlu.constants import TopicModule


@pytest.mark.parametrize("text", [
    "I'm no expert on faggots, but I thought of something related!",
    "I learned that: People are so afraid of the F word when it's the thing that brought them to this world.",
    "moby dick fuck",
    "shooting",
    "i do like to have sex with people"
])
def test_profanity_classifier(text):
    result = BotResponseProfanityClassifier.is_profane(text)
    assert result is True


@pytest.mark.parametrize("text", [
    "shooting"
])
def test_profanity_classifier_whitelist_one_word(text):
    result = BotResponseProfanityClassifier.is_profane(text, TopicModule.SPORTS.value)
    assert result is False


@pytest.mark.parametrize("text", [
    "shooting fuck"
])
def test_profanity_classifier_whitelist_one_word_followed_by_profanity(text):
    result = BotResponseProfanityClassifier.is_profane(text, TopicModule.SPORTS.value)
    assert result is True


@pytest.mark.parametrize("text, module", [
    ("moby dick", TopicModule.BOOK.value),
    ("i read moby dick",  TopicModule.BOOK.value),
    ("here's something i found from the article 'bowling' on wikipedia: different kinds of balls are available for different styles of bowling.", TopicModule.SPORTS.value),
])
def test_profanity_classifier_whitelist_multiple_words(text, module):
    result = BotResponseProfanityClassifier.is_profane(text, module)
    assert result is False


@pytest.mark.parametrize("text", [
    "moby dick fuck"
])
def test_profanity_classifier_whitelist_followed_by_profanity(text):
    result = BotResponseProfanityClassifier.is_profane(text, TopicModule.BOOK.value)
    assert result is True


@pytest.mark.parametrize("text", [
    "great",
])
def test_profanity_classifier_false(text):
    result = BotResponseProfanityClassifier.is_profane(text)
    assert result is False
