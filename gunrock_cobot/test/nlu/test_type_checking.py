from logging import LogRecord
import pytest

from nlu.dataclass import ReturnNLP, CentralElement


@pytest.mark.parametrize('inputs', [
    dict(returnnlp=[{
        'dialog_act': ['pos_answer', '0.6304426789283752'],
        'dialog_act2': ['commands', '0.3689870536327362'],
    }]),
    dict(central_elem={
        'senti': 'neu',
        'regex': {'sys': [], 'topic': [], 'lexical': []},
        # 'DA': 'statement',
        'DA_score': '1.0',
        'DA2': 'commands',
        # 'DA2_score': '0.5',
        'module': [],
        'np': ['john'],
        'text': 'john',
        'backstory': {
            'confidence': 0.4724680185317993,
            'text': "Sorry, I'm not supposed to tell you. I'm part of a competition, remember?"
        }
    })
])
def test_warnings(inputs, caplog):
    if 'returnnlp' in inputs:
        rnlp = ReturnNLP.from_list(inputs['returnnlp'])
        assert rnlp
    elif 'central_elem' in inputs:
        ce = CentralElement.from_dict(inputs['central_elem'])
        assert ce

    assert len([r for r in caplog.records if r.name == 'nlu.typecheck']) > 0

    for record in caplog.records:
        record: LogRecord
        if record.funcName in {'is_type', 'ensure_type'}:
            assert "[TYPECHECK] <ensure_type> failed:" in record.message
        elif record.funcName in {'safe_cast'}:
            assert "[TYPECHECK] <safe_cast> failed:" in record.message


@pytest.mark.parametrize('inputs', [
    dict(returnnlp=[
        {
            'text': "i'm fine", 'dialog_act': ['opinion', '0.45750177'], 'dialog_act2': ['NA', '0.0'],
            'intent': {'sys': [], 'topic': ['topic_phatic'], 'lexical': []},
            'sentiment': {'compound': '0.5', 'neg': '0.05', 'neu': '0.9', 'pos': '0.05'},
            'amazonkg': [], 'googlekg': [], 'noun_phrase': [], 'keyphrase': [],
            'topic': {'text': "i'm fine", 'topicClass': 'Phatic', 'confidence': 999.0, 'topicKeywords': []},
            'concept': [], 'amazon_topic': 'Phatic',
            'dependency_parsing': {'head': [0, 1], 'label': ['amod', 'advmod']}, 'pos_tagging': ['ADV', 'ADJ'], 'generic_ner': [],
        }, {
            'text': 'okay', 'dialog_act': ['back-channeling', '0.3990792'], 'dialog_act2': ['NA', '0.0'],
            'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']},
            'sentiment': {'compound': '0.5', 'neg': '0.05', 'neu': '0.9', 'pos': '0.05'},
            'amazonkg': [], 'googlekg': [], 'noun_phrase': [], 'keyphrase': [],
            'topic': {'text': 'okay', 'topicClass': 'Phatic', 'confidence': 999.0, 'topicKeywords': []},
            'concept': [], 'amazon_topic': 'Phatic', 'dependency_parsing': {'head': [0], 'label': ['amod']},
            'pos_tagging': ['ADJ'], 'generic_ner': [],
        }, {
            'text': 'a little sad', 'dialog_act': ['opinion', '0.78006744'], 'dialog_act2': ['NA', '0.0'],
            'intent': {'sys': [], 'topic': [], 'lexical': ['ans_neg']},
            'sentiment': {'compound': '0.5', 'neg': '0.9', 'neu': '0.0', 'pos': '0.1'},
            'amazonkg': [], 'googlekg': [], 'noun_phrase': [], 'keyphrase': [],
            'topic': {'text': 'a little sad', 'topicClass': 'Phatic', 'confidence': 999.0, 'topicKeywords': []},
            'concept': [], 'amazon_topic': 'Phatic',
            'dependency_parsing': {'head': [3, 3, 0], 'label': ['det', 'amod', 'amod']},
            'pos_tagging': ['DET', 'ADJ', 'ADJ'], 'generic_ner': [],
        },
    ]),
    # dict(central_elem={
    #     'senti': 'neg', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_neg']},
    #     'DA': 'opinion', 'DA_score': '0.78006744', 'DA2': 'NA', 'DA2_score': '0.0', 'module': [], 'np': [],
    #     'text': 'a little sad',
    #     'backstory': {
    #         'bot_propose_module': '', 'confidence': 0.7001732587814331, 'followup': '', 'module': '', 'neg': '',
    #         'pos': '', 'postfix': '', 'question': 'are you just kidding me', 'reason': '', 'tag': '',
    #         'text': "I'm kidding if you thought it was funny!                                                                                                                                                                                                                                                                                                                                                                                                          "
    #     },
    # })
])
def test_init_without_warnings(inputs, caplog):
    if 'returnnlp' in inputs:
        rnlp = ReturnNLP.from_list(inputs['returnnlp'])
        assert rnlp
    elif 'central_elem' in inputs:
        ce = CentralElement.from_dict(inputs['central_elem'])
        assert ce

    assert len([r for r in caplog.records if r.name == 'nlu.typecheck']) <= 0
