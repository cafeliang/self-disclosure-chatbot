import pytest
from custom_asr_processor import CustomASRProcessor


@pytest.mark.parametrize("text, expected_result", [
    ("carova nineteen", "covid nineteen")
])
def test_asr_processor(text, expected_result):
    result = CustomASRProcessor._correct_covid_mispronunciation(text)
    assert result == expected_result

