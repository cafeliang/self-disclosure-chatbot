import pytest

from nlu.dataclass import ReturnNLP


@pytest.mark.parametrize('index, returnnlp, condition_intent, expected_value', [
    (idx, returnnlp, intents, expected)
    for idx, (returnnlp, conditions) in enumerate([
        ([
            {'intent': {"sys": [], "topic": ["topic_newspolitics", "topic_election"], "lexical": ["ask_yesno"]}}
        ], [
            ('topic_newspolitics', True),
            (['topic_newspolitics', 'ask_yesno'], True),
            (['topic_newspolitics', 'topic_election'], True),
            (['ask_yesno', 'ask_ability'], True),
            ('req_play_music', False)
        ]),
        ([
            {'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos', 'ans_factopinion']}, },
            {'intent': {'sys': [], 'topic': ['topic_backstory'], 'lexical': ['ans_pos']}, }
        ], [
            ('ans_pos', True),
            (['topic_backstory', 'ans_factopinion'], True),
            ('ans_neg', False),
            (['ans_neg', 'topic_music'], False)
        ])
    ]) for (intents, expected) in conditions
])
def test_has_intent(index, returnnlp, condition_intent, expected_value, caplog):

    caplog.set_level('DEBUG')
    caplog.set_level('ERROR', 'nlu.typecheck')

    rnlp = ReturnNLP.from_list(returnnlp)
    context = str(dict(index=index, text=rnlp.text, condition_intent=condition_intent, expected_value=expected_value))
    assert rnlp.has_intent(condition_intent) == expected_value, context
