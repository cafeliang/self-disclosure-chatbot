import re
import pytest


regex_answer_unknown = r"(i don't know)+|(do not|don't).* (know|have|remember|recall)|not (too |)sure|unsure|uncertain|no idea|no opinion|i forgot|no comment|hard to say|hard for me to say|hard question|hard one|tough one|tough question|depends"


@pytest.mark.parametrize("text", [
    "i don't know",
    # "i'm not sure"
])
def test_answer_unknown_true(text):
    assert match_regex(text, regex_answer_unknown) is True


@pytest.mark.parametrize("text", [
    "know"
])
def test_answer_unknown_false(text):
    assert match_regex(text, regex_answer_unknown) is False


def match_regex(text, regex_pattern):
    match = re.match(regex_pattern, text)
    return match is not None
