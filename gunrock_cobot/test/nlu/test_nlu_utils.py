from nlu.util_nlu import ReturnNLP
from nlu.constants import Positivity
import unittest

returnnlp = [
    {
        "text": "did you ever watch full house?",
        "dialog_act": ["yes_no_question", "1.0"],
        "dialog_act2": ["commands", "0.01"],
        "intent": {
            "sys": [],
            "topic": [
                "topic_newspolitics",
                "topic_election"
            ],
            "lexical": [
                "ask_yesno"
            ]
        },
        "sentiment": {
            "neg": "0.0",
            "neu": "1.0",
            "pos": "0.0",
            "compound": "0.0"
        },
        "googlekg": [
            [
                "Full House",
                "American sitcom",
                "651.743408",
                "movie"
            ]
        ],
        "noun_phrase": [
            "full house"
        ],
        "topic": {
            "topicClass": "Movies_TV",
            "confidence": 0.656,
            "text": "did you ever watch full house?",
            "topicKeywords": [
                {
                    "confidence": 0.486,
                    "keyword": "house"
                }
            ]
        },
        "concept": [
            {
                "noun": "full house",
                "data": {
                    "show": "0.5789",
                    "hand": "0.2368",
                    "sitcom": "0.1842"
                }
            }
        ],
        "dependency_parsing": None
    }
]

KEYWORD_MAPPING = {"artificial_intelligence": set(["artificial intelligence", "ai", "a.i.", "a.i.", "a. i.", "intelligence", "a i", "a. i."]),
                   "robot": set(["robotics", "robots", "robot"]),
                   "astronomy": set(["astro", "astronomy"]),
                   "bitcoin": set(["bitcoins", "bitcoin"]),
                   "biology": set(["bio", "biology"])}


class ReturnNLPTest(unittest.TestCase):

    def test_instantiate(self):
        returnnlp_obj = ReturnNLP(returnnlp)
        returnnlp_obj.text
        returnnlp_obj.noun_phrase
        returnnlp_obj.googlekg
        returnnlp_obj.sentiment
        returnnlp_obj.concept
        returnnlp_obj.topic
        returnnlp_obj.topic_intent
        returnnlp_obj.sys_intent
        returnnlp_obj.lexical_intent

    def test_keyword_detector(self):
        returnnlp_obj = ReturnNLP(returnnlp)
        result = returnnlp_obj.detect_module_keywords("techsciencechat")
        assert result is not None

    def test_yes_no(self):
        returnnlp_obj = ReturnNLP(returnnlp)
        result = returnnlp_obj.answer_positivity
        assert result in Positivity

    def test_predefined_keyword_detector(self):
        returnnlp_obj = ReturnNLP(returnnlp)
        returnnlp_obj.detect_keyword_from_mapping(KEYWORD_MAPPING)
