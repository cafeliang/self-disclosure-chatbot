import pytest

from nlu.intent_classifier import IntentClassifier


@pytest.mark.parametrize("text", [
    "can i tell you something",
    "i need to tell you something",
    "can i tell you something",
    "okay now can i tell you my favorite singer",
    "okay now can i tell you what singer i most like",
    "i'd like to tell you something",
    "i wanna tell you sth interesting",
    "can i tell you a joke",
    "yes would you like to know more",
    "no do you wanna hear what i really like",
    "you let me talk",
    "will you stop interjecting",
    "listen i wanna talk about what i like",
    "how come you're not listening",
    "you wanna hear something interesting",
    "how about i tell you something",
])
def test_req_allow_tell_sth(text: str):
    __has_intent(text, "req_allow_tell_sth", True)


@pytest.mark.parametrize("text", [
    "do you like guess throughout the day usually i don't wanna tell you my personal information but",
    "uh i can't really tell you that right now",
    "i'll tell you again"
])
def test_not_req_allow_tell_sth(text: str):
    __has_intent(text, "req_allow_tell_sth", False)


@pytest.mark.parametrize("text", [
    "tell me more",
    "tell me more about it",
    "tell me another",
    "give me another one",
    "what's it about",
    "what is it about",
    "i like to know more please",
    "yeah listen to more",
    "i wanna hear a lot more",
    "what else do you have to tell me",
    "i like it that's interesting what else do you know about it",
    "that's interesting can you tell me more about joker",
    "one more",
    "keep going",
    "continue",
])
def test_is_req_tell_more_true(text: str):
    __has_intent(text, "req_more", True)


@pytest.mark.parametrize("text", [
    "tell me about cupcakes",
    "i don't want to talk about this anymore",
    "give me another a b c d"
])
def test_is_req_tell_more_false(text: str):
    __has_intent(text, "req_more", False)


@pytest.mark.parametrize("text", [
    "i don't wanna talk about movie anymore",
    "i don't wanna talk about movie",
    "i'm not into fashion",
    "i'm not interested in fashion"
])
def test_get_off_topic(text):
    __has_intent(text, "get_off_topic", True)


@pytest.mark.parametrize("text", [
    "do you wanna talk about family or school",
    "no do you wanna talk about talk about other things",
    "yeah do you wanna talk about my life",
    "can we talk about baseball",
    "hey can you show me like screenshots of the p.s. five",
    "can we chat about boys",
    "can we change the subject",
    "can we talk about other topics",
    "can we talk about something different",
    "can we talk about something else",
    "can you repeat that",
    "can we talk about cats",
    "can we please change the subjects",
    "no can we talk about something else",
    "can i talk to you about my girlfriend",
    "can we talk something about something else can we talk about our marriage",
    "can you talk in a british accent",
    "can you repeat",
    "can you tell me the name of my cat",
    "can you stop"
])
def test_is_command(text: str):
    __has_intent(text, "command", True)


@pytest.mark.parametrize("text", [
    "do you have any suggestions",
    "no can you tell me when you were born",
    "could you tell me who is obama",
    "do you know anything about computer programming in c. h.",
    "can you just talk about what's going on with nasa",
])
def test_is_not_command(text: str):
    __has_intent(text, "command", False)


@pytest.mark.parametrize("text", [
    "can you poop",
    "can we kiss",
    "can you be my friend",
    "can we talk in spanish",
    "can we be more than friends",
])
def test_is_command_in_question_form_no_req_topic(text):
    __has_intent(text, "command_in_question_form_no_req_topic", True)


@pytest.mark.parametrize("text", [
    "can you talk about",
    "can we chat about",
    "can you tell me how to cook"
    "do you wanna talk about your friends"
])
def test_is_command_in_question_form_no_req_topic_false(text):
    __has_intent(text, "command_in_question_form_no_req_topic", False)


@pytest.mark.parametrize("text", [
    "can you talk about sports",
    "can we chat about harry potter",
    "can we talk about"
])
def test_is_command_in_question_form_req_topic(text):
    __has_intent(text, "command_in_question_form_req_topic", True)


@pytest.mark.parametrize("text", [
    "Do you like zebra",
])
def test_ask_preference_true(text):
    __has_intent(text, "ask_preference", True)


@pytest.mark.parametrize("text", [
    "i would probably be a zebra or giraffe",
])
def test_ask_preference_false(text):
    __has_intent(text, "ask_preference", False)


@pytest.mark.parametrize("text", [
    "how do i summon you back",
])
def test_ask_preference_true(text):
    __has_intent(text, "ask_advice", True)


@pytest.mark.parametrize("text", [
    "i feel helpless",
])
def test_ask_preference_false(text):
    __has_intent(text, "ask_advice", False)


@pytest.mark.parametrize("text", [
    "do you know anything about zebra",
    "do you know about zebra",
])
def test_ask_info_true(text):
    __has_intent(text, "ask_info", True)


@pytest.mark.parametrize("text", [
    "do you know",
])
def test_ask_info_false(text):
    __has_intent(text, "ask_info", False)


@pytest.mark.parametrize("text", [
    "do you know anything about zebra",
    "do you know about zebra",
])
def test_ask_yesno_true(text):
    __has_intent(text, "ask_yesno", True)


@pytest.mark.parametrize("text", [
    "am doing well",
    "was sleeping all day",
    "i don't wanna talk about that"
])
def test_ask_yesno_false(text):
    __has_intent(text, "ask_yesno", False)


@pytest.mark.parametrize("text, is_ask_back", [
    ("what is yours", True),
    ("what about you", True),
    ("who's yours", True),
    ("what is your favorite one", True),
    ("who is your favorite one", True),
    ("a perfectly fine give me a second", False),
    ("what is a cat", False),
])
def test_is_ask_back(text: str, is_ask_back: bool):
    __has_intent(text, "ask_back", is_ask_back)


@pytest.mark.parametrize("text", [
    "why is that",
    "why"
    "is that why you are sad",
])
def test_ask_reason_true(text):
    __has_intent(text, "ask_reason", True)


@pytest.mark.parametrize("text", [
    "that's why i'm sad",
    "that is why it is difficult",
    "why not share my feelings with the social bot"
])
def test_ask_reason_false(text):
    __has_intent(text, "ask_reason", False)

@pytest.mark.parametrize("text", [
    "let's not talk about books",
    "i don't want to talk about books",
    "i don't love to chat books"
])
def test_req_not_jump_true(text):
    __has_intent(text, "req_not_jump", True)

@pytest.mark.parametrize("text", [
    "let's talk about books",
    "i want to talk about books",
    "i love to chat books"
])
def test_req_not_jump_false(text):
    __has_intent(text, "req_not_jump", False)

@pytest.mark.parametrize("text", [
    "let's talk about books",
    "i want to talk about books",
    "i'd like to talk about book",
    "i want to talk about book",
    "why don't we talk about book",
    "do you want to talk about book",
    "can we talk about book",
    "how about we talk about book",
    "can you tell me anything about book",
    "tell me something about it",
    "i'm into book"
])
def test_req_topic_jump(text):
    __has_intent(text, "req_topic_jump", True)

@pytest.mark.parametrize("text", [
    "i want to talk about",
    "tell me more about it",
    "i like book do you want to talk about it",
    "tell me more",
    "can you tell me more"
])
def test_req_topic_jump_false(text):
    __has_intent(text, "req_topic_jump", False)

def __has_intent(text: str, intent: str, boolean: bool):
    result = IntentClassifier(text).has_intent(intent)
    assert result is boolean
