import redis
import unittest
import praw
import random

import pytest
from util_redis import RedisHelper
from user_profiler.user_profile import Gender



@pytest.yield_fixture
def redis_client():
    redis_client = RedisHelper()
    yield redis_client


def test_common_name(redis_client):
    try:
        assert redis_client.is_common_name("John") is True
        assert redis_client.is_common_name("Ishan") is True
        assert redis_client.is_common_name("qq") is False
    except redis.exceptions.ConnectionError as e:
        print("redis test is not working due to connection error")


def test_set_name_gender():
    # given
    redis_dev_client = RedisHelper(True)
    redis_dev_client.delete(RedisHelper.NAME_KEY_PREFIX, "abc", False)
    redis_dev_client.delete(RedisHelper.NAME_KEY_PREFIX, "def", False)

    assert redis_dev_client.is_common_english_indian_european_name("abc") is False
    assert redis_dev_client.is_common_english_indian_european_name("def") is False

    # when
    redis_dev_client.set_name_gender("abc")
    redis_dev_client.set_name_gender("def", "1")

    # then
    assert redis_dev_client.get_predicted_gender("abc") is Gender.UNKNOWN
    assert redis_dev_client.get_predicted_gender("def") is Gender.MALE



def test_get_predicted_gender(redis_client):
    assert redis_client.get_predicted_gender("alice") == "female"
    assert redis_client.get_predicted_gender("bob") == "male"
    assert redis_client.get_predicted_gender("abc") is "unknown"


def test_get_reddit_key_length(redis_client):
    try:
        assert redis_client.get_reddit_keys_length() >= 5
    except redis.exceptions.ConnectionError as e:
        print("redis test is not working due to connection error")


def test_get_retrieval_key(redis_client):
    try:
        assert "::" in redis_client.get_reddit_key()
    except redis.exceptions.ConnectionError as e:
        print("redis test is not working due to connection error")


def test_get_all_holiday(redis_client):
    try:
        assert len(redis_client.get_all_holiday(limit=5)) is 5
    # todo: test category
    except redis.exceptions.ConnectionError as e:
        print("redis test is not working due to connection error")


def test_get_asr(redis_client):
    try:
        assert redis_client.get_asr("movies_phonetics") is not None
        assert redis_client.get_asr("games_phonetics") is not None
        assert redis_client.get_asr("artist_phonetics") is not None
        assert redis_client.get_asr("sports_phonetics") is not None
    except redis.exceptions.ConnectionError as e:
        print("redis test is not working due to connection error")


def test_get_random_thoughts(redis_client):
    try:
        result = redis_client.get_random_thoughts('santa')
        assert result == "in 1986 students at UC Santa Cruz voted to make the school's official mascot the Banana Slug. " \
                         "The chancellor refused to honour the result, instead polling just student-athletes on the " \
                         "question... who also voted overwhelmingly in favour of being the Banana Slugs."
    except redis.exceptions.ConnectionError as e:
        print("redis test is not working due to connection error")


def test_artist_exists(redis_client):
    try:
        assert redis_client.is_known_artist("justin bieber") is True
        assert redis_client.is_known_artist("nobody") is False
    except redis.exceptions.ConnectionError as e:
        print("redis test is not working due to connection error")


def test_genre_exists(redis_client):
    try:
        assert redis_client.is_known_genre("pop") is True
        assert redis_client.is_known_genre("random") is False
    except redis.exceptions.ConnectionError as e:
        print("redis test is not working due to connection error")


@unittest.skip('test skipped')  # TODO
def test_returning_user_in_music_module(redis_client):
    try:
        assert redis_client.is_returning_user_in_music_module("lesley-08290000") is True
        assert redis_client.is_returning_user_in_music_module("0000") is False
    except redis.exceptions.ConnectionError as e:
        print("redis test is not working due to connection error")


def test_get(redis_client):
    try:
        assert redis_client.get(RedisHelper.MOVIE_ID_DETAILED_PREFIX, str(383498)) is not None

        USER_ID_KAIHUI = "amzn1.ask.account.AHSYZ7AMZBN654NECVBTFU6TXKT5ADEY7AJ5BASQ7TEEJYIDETUNLLM73UY4DO2KHPUFDUXIE6POZMLZXGMWRIS6RP42KY7SUCSKCGHC44ENK24GLWRTTPDPWVYDZHJA3SU5X6ACQJTNYBSVP3IHKZHUQYRU7AHTU7W7GKMKYNH6TOB6BPH2OSPSLKC3BO4ZHEIMZWA3Q2P7RWY"
        print(redis_client.get(RedisHelper.MOVIE_USER_PREFIX, USER_ID_KAIHUI))
    except redis.exceptions.ConnectionError as e:
        print("redis test is not working due to connection error")


def test_get_blender(redis_client):
    import time
    for i in range(3):
        try:
            result = redis_client.get("gunrock:blender:question_handling", "00001")
            print(result)
        except redis.exceptions.ConnectionError as e:
            print("redis test is not working due to connection error")

        time.sleep(1)

def test_hgetall(redis_client):
    movie_id = str(383498)
    result = redis_client.hgetall(RedisHelper.MOVIE_ID_DETAILED_PREFIX, movie_id, False)
    assert result["title"] == "Deadpool 2"
    assert result["release_date"] == "2018-05-15"
    assert result["trivia"] is not None
    assert result[
               "overview"] == "Wisecracking mercenary Deadpool battles the evil and powerful Cable and other bad guys to save a boy's life."


@unittest.skip('test skipped')
def test_delete_user(redis_client):
    USER_ID_KAIHUI = "amzn1.ask.account.AHSYZ7AMZBN654NECVBTFU6TXKT5ADEY7AJ5BASQ7TEEJYIDETUNLLM73UY4DO2KHPUFDUXIE6POZMLZXGMWRIS6RP42KY7SUCSKCGHC44ENK24GLWRTTPDPWVYDZHJA3SU5X6ACQJTNYBSVP3IHKZHUQYRU7AHTU7W7GKMKYNH6TOB6BPH2OSPSLKC3BO4ZHEIMZWA3Q2P7RWY"
    redis_client.delete_user_history(USER_ID_KAIHUI)

def test_sadd():
    redis_client = RedisHelper(True)

    # clean up values in key
    redis_client.r.delete("test_set_key")
    result = redis_client.smembers("test_set_key")
    assert len(result) == 0

    # add key
    redis_client.sadd("test_set_key", "test_value")
    redis_client.sadd("test_set_key", "test_value2")

    # check values are added
    result = redis_client.smembers("test_set_key")
    assert "test_value" in result
    assert "test_value2" in result
