import os
import unittest
import subprocess
import yaml

from gunrock_cobot.response_generator.special.controversial_opinion.controversial_opinion_handler import ControversialOpinionHandler as ControHandler

from gunrock_cobot.template_manager import preprocess
from .utils import MockStateManager

YAML_PATH = os.path.join(os.getenv("COBOT_HOME"), "gunrock_cobot/response_templates/intent_topics.yml")

"""
After we pass the task to controversial handler, make sure the series of sentences goes to appropriate submodule.
"""
class AdviceResponseTest(unittest.TestCase):

    def setUp(self):
        self.mock_state_manager = MockStateManager()
        return

    def get_yaml_response(self, topics):
        with open(YAML_PATH, "r") as yaml_file:
            data = yaml.load(yaml_file)
            for t in topics:
                if t in data:
                    data = data[t]
                else:
                    return None
        return data

    def test_basic_stock_advice(self):
        # The input text is geared to the specific regex in advice function.

        text = "What stock should I invest?"
        _inputs = {"text": text}
        handler = ControHandler(_inputs, self.mock_state_manager)
        response = handler.generateResponse()
        truth = self.get_yaml_response(["controversial", "advice", "stock"])
        self.assertIsNotNone(truth, msg="ResponseTemplate controversial/advice/stock is returning None.")
        self.assertTrue(response in truth, msg="location:  controversial/advice/stock.")



    def test_general_advice(self):
        text = "Should I play with something controversial?"
        _inputs = {"text": text}
        handler = ControHandler(_inputs, self.mock_state_manager)
        response = handler.generateResponse()
        truth = self.get_yaml_response(["controversial", "advice", "general"])
        self.assertIsNotNone(truth, msg="ResponseTemplate controversial/advice/general is returning None.")
        self.assertTrue(response in truth, msg="location:  controversial/advice/general." + response)


    def test_medical_advice(self):
        text = "My shoulder hurts, what medicine should I take?"
        _inputs = {"text": text}
        handler = ControHandler(_inputs, self.mock_state_manager)
        response = handler.generateResponse()
        truth = self.get_yaml_response(["controversial", "advice", "medical"])
        self.assertIsNotNone(truth, msg="ResponseTemplate controversial/advice/medical is returning None.")
        self.assertTrue(response in truth, msg="location:  controversial/advice/medical.")

    def test_financial_advice(self):
        text = "Where should I spend my life saving?"
        _inputs = {"text": text}
        handler = ControHandler(_inputs, self.mock_state_manager)
        response = handler.generateResponse()
        truth = self.get_yaml_response(["controversial", "advice", "financial"])
        self.assertIsNotNone(truth, msg="ResponseTemplate controversial/advice/financial is returning None.")
        self.assertTrue(response in truth, msg="location:  controversial/advice/financial.")

    def test_legal_advice(self):
        text = "Should I file for bankruptcy?"
        _inputs = {"text": text}
        handler = ControHandler(_inputs, self.mock_state_manager)
        response = handler.generateResponse()
        truth = self.get_yaml_response(["controversial", "advice", "legal"])
        self.assertIsNotNone(truth, msg="ResponseTemplate controversial/advice/legal is returning None.")
        self.assertTrue(response in truth, msg="location:  controversial/advice/legal.")

