import os
import unittest
import subprocess
import yaml

from gunrock_cobot.response_generator.special.controversial_opinion.controversial_opinion_handler import ControversialOpinionHandler as ControHandler

from gunrock_cobot.template_manager import preprocess
from .utils import MockStateManager

YAML_PATH = os.path.join(os.getenv("COBOT_HOME"), "gunrock_cobot/response_templates/intent_topics.yml")

"""
After we pass the task to controversial handler, make sure the series of sentences goes to appropriate submodule.
"""
class DefaultResponseTest(unittest.TestCase):

    def setUp(self):
        self.mock_state_manager = MockStateManager()
        return

    def get_yaml_response(self, topics):
        with open(YAML_PATH, "r") as yaml_file:
            data = yaml.load(yaml_file)
            for t in topics:
                if t in data:
                    data = data[t]
                else:
                    return None
        return data

    def test_religion_response(self):
        # The input text is geared to the specific regex in advice function.

        text = "I want to know about religion?"
        _inputs = {"text": text}
        handler = ControHandler(_inputs, self.mock_state_manager)
        response = handler.generateResponse()
        truth = self.get_yaml_response(["controversial", "default", "religion"])
        self.assertIsNotNone(truth, msg="ResponseTemplate controversial/default/religion is returning None.")
        self.assertTrue(response in truth, msg="location:  controversial/default/religion.")



    def test_general_advice(self):
        text = "General controversial opinion."
        _inputs = {"text": text}
        handler = ControHandler(_inputs, self.mock_state_manager)
        response = handler.generateResponse()
        truth = self.get_yaml_response(["controversial", "default", "general"])
        self.assertIsNotNone(truth, msg="ResponseTemplate controversial/default/general is returning None.")
        self.assertTrue(response in truth, msg="location:  controversial/default/general.")
