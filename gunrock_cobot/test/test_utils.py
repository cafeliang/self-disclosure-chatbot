from utils import is_common_name, get_reddit_key
from redis.exceptions import ConnectionError

def test_get_retrieval_key():
    try:
        assert (get_reddit_key()) is not None
    except ConnectionError as e:
        print("test get retrieval key is not run due to connection error")

def test_is_common_name():
    try:
        assert type(is_common_name("John")) is bool
    except ConnectionError as e:
        print("test common name is not run due to connection error")


