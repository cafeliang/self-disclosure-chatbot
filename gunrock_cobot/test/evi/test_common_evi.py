import pytest
from typing import Any, Callable, Dict


@pytest.mark.parametrize("query, assertions", [
    ('how long can a dog live', {
        'type': lambda r: isinstance(r, str),
        'len': lambda r: len(r.split()) > 3 if isinstance(r, str) else False,
    })
])
def test_evi(query: str, assertions: Dict[str, Callable[[Any], bool]]):
    from response_generator.common import evi
    result = evi.query(query, handle_empty_response=False)
    if result:
        for k, v in assertions.items():
            print(k, result)
            assert v(result)
