import pytest
from unittest.mock import PropertyMock
from types import SimpleNamespace
from nlu.constants import TopicModule
from selecting_strategy.module_selection import ModuleSelector, GlobalState
from unittest.mock import patch
from user_profiler.user_profile import UserProfile


@pytest.fixture
def module_selector():
    ua_ref = SimpleNamespace()
    return ModuleSelector(ua_ref)

@patch('selecting_strategy.module_selection.ModuleSelector.get_personal_topic_proposal_order')
def test_get_available_topics(personal_topic_proposal_order, module_selector):
    # Given
    personal_topic_proposal_order.return_value = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.SPORTS.value,
        TopicModule.FASHION.value,
        TopicModule.FOOD.value,
        TopicModule.GAME.value,
        TopicModule.TRAVEL.value,
        TopicModule.BOOK.value,
        TopicModule.MUSIC.value,
        TopicModule.TECHSCI.value,
        TopicModule.NEWS.value,
    ]

    module_selector.used_topic_modules = [
        TopicModule.FOOD.value,
        TopicModule.MUSIC.value
    ]

    # When
    available_topics = module_selector.get_available_topics()

    # Then
    assert available_topics == [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.SPORTS.value,
        TopicModule.FASHION.value,
        TopicModule.GAME.value,
        TopicModule.TRAVEL.value,
        TopicModule.BOOK.value,
        TopicModule.TECHSCI.value,
        TopicModule.NEWS.value,
    ]

@patch('selecting_strategy.module_selection.ModuleSelector.preferred_entity_topics', new_callable=PropertyMock)
@patch('selecting_strategy.module_selection.ModuleSelector.get_personal_topic_proposal_order')
def test_preferred_topics_has_available_preferred_topic(personal_topic_proposal_order, preferred_entity_topics, module_selector):
    # Given
    personal_topic_proposal_order.return_value = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
        TopicModule.FOOD.value,
        TopicModule.GAME.value,
        TopicModule.TRAVEL.value,
        TopicModule.SPORTS.value,
        TopicModule.MUSIC.value,
        TopicModule.TECHSCI.value,
        TopicModule.NEWS.value,
    ]

    module_selector.used_topic_modules = [
        TopicModule.FOOD.value,
        TopicModule.MUSIC.value
    ]
    preferred_entity_topics.return_value = [
        UserProfile.PreferrdEntityTopic.from_dict({"module": TopicModule.SPORTS.value}),
        UserProfile.PreferrdEntityTopic.from_dict({"module": TopicModule.FOOD.value}),
        UserProfile.PreferrdEntityTopic.from_dict({"module": TopicModule.BOOK.value}),
    ]

    module_selector.init_module_rank()
    module_selector.module_rank[TopicModule.GAME.value] += 1

    # Then
    available_preferred_topics = module_selector.get_available_preferred_topics()
    assert set(available_preferred_topics) == {
        TopicModule.SPORTS.value,
        TopicModule.BOOK.value,
    }

    has_available_preferred_topics = module_selector.has_available_preferred_topics()
    assert has_available_preferred_topics is True

    next_available_preferred_topics = module_selector.get_sorted_available_preferred_topics()
    assert next_available_preferred_topics == [
        TopicModule.BOOK.value,
        TopicModule.SPORTS.value
    ]

    next_modules = [module for module, utt in module_selector.get_next_modules(11)]
    assert next_modules == [
        # preferred topics go first
        TopicModule.BOOK.value,
        TopicModule.SPORTS.value,
        # highest ranked module next
        TopicModule.GAME.value,
        # available module
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.FASHION.value,
        TopicModule.TRAVEL.value,
        TopicModule.TECHSCI.value,
        TopicModule.NEWS.value,
        # used module
        TopicModule.FOOD.value,
        TopicModule.MUSIC.value,
    ]

@patch('selecting_strategy.module_selection.ModuleSelector.preferred_entity_topics', new_callable=PropertyMock)
@patch('selecting_strategy.module_selection.ModuleSelector.get_personal_topic_proposal_order')
def test_preferred_topics_has_no_available_preferred_topic(personal_topic_proposal_order, preferred_entity_topics, module_selector):
    # Given
    personal_topic_proposal_order.return_value = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
        TopicModule.FOOD.value,
        TopicModule.GAME.value,
        TopicModule.TRAVEL.value,
        TopicModule.SPORTS.value,
        TopicModule.MUSIC.value,
        TopicModule.TECHSCI.value,
        TopicModule.NEWS.value,
    ]

    module_selector.used_topic_modules = [
        TopicModule.FOOD.value,
        TopicModule.MUSIC.value
    ]

    preferred_entity_topics.return_value = [
        UserProfile.PreferrdEntityTopic.from_dict({"module": TopicModule.MUSIC.value}),
        UserProfile.PreferrdEntityTopic.from_dict({"module": TopicModule.FOOD.value}),
    ]

    # When
    available_preferred_topics = module_selector.get_available_preferred_topics()
    has_available_preferred_topics = module_selector.has_available_preferred_topics()
    next_available_preferred_topics = module_selector.get_sorted_available_preferred_topics()

    # Then
    assert set(available_preferred_topics) == set()

    assert has_available_preferred_topics is False

    assert next_available_preferred_topics == []


@patch('selecting_strategy.module_selection.ModuleSelector.get_personal_topic_proposal_order')
def test_get_next_module_highest_module_rank_in_used_topic(personal_topic_proposal_order, module_selector):
    # Given
    personal_topic_proposal_order.return_value = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
        TopicModule.FOOD.value,
        TopicModule.GAME.value,
    ]

    module_selector.used_topic_modules = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
    ]

    module_selector.preferred_topics = []

    module_selector.init_module_rank()
    module_selector.module_rank[TopicModule.ANIMAL.value] += 1

    # When
    next_selected_module = [module for module, utt in module_selector.get_next_modules()]
    next_selected_2_module = [module for module, utt in module_selector.get_next_modules(2)]
    next_selected_10_module = [module for module, utt in module_selector.get_next_modules(10)]

    # Then
    assert next_selected_module == [TopicModule.FOOD.value]

    assert next_selected_2_module == [
        TopicModule.FOOD.value,
        TopicModule.GAME.value,
    ]

    assert next_selected_10_module == [
        TopicModule.ANIMAL.value,
        TopicModule.FOOD.value,
        TopicModule.GAME.value,
        TopicModule.MOVIE.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
    ]


@patch('selecting_strategy.module_selection.ModuleSelector.get_personal_topic_proposal_order')
def test_preferred_topics_has_no_preferred_topic(personal_topic_proposal_order, module_selector):
    # Given
    personal_topic_proposal_order.return_value = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
        TopicModule.FOOD.value,
        TopicModule.GAME.value,
        TopicModule.TRAVEL.value,
        TopicModule.SPORTS.value,
        TopicModule.MUSIC.value,
        TopicModule.TECHSCI.value,
        TopicModule.NEWS.value,
    ]

    module_selector.used_topic_modules = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value
    ]

    module_selector.preferred_topics = []

    module_selector.init_module_rank()

    # When
    available_preferred_topics = module_selector.get_available_preferred_topics()
    has_available_preferred_topics = module_selector.has_available_preferred_topics()
    next_available_preferred_topics = module_selector.get_sorted_available_preferred_topics()
    next_selected_module = [module for module, _ in module_selector.get_next_modules(1)]
    next_selected_5_modules = [module for module, _ in module_selector.get_next_modules(5)]
    next_selected_10_modules = [module for module, _ in module_selector.get_next_modules(11)]

    # Then
    assert available_preferred_topics == []

    assert has_available_preferred_topics is False

    assert next_available_preferred_topics == []

    assert next_selected_module == [TopicModule.BOOK.value]

    assert next_selected_5_modules == [TopicModule.BOOK.value,
                                     TopicModule.FASHION.value,
                                     TopicModule.FOOD.value,
                                     TopicModule.GAME.value,
                                     TopicModule.TRAVEL.value]

    assert next_selected_10_modules == [
        # unused first
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
        TopicModule.FOOD.value,
        TopicModule.GAME.value,
        TopicModule.TRAVEL.value,
        TopicModule.SPORTS.value,
        TopicModule.MUSIC.value,
        TopicModule.TECHSCI.value,
        TopicModule.NEWS.value,
        # used last
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value
    ]


@patch('selecting_strategy.module_selection.ModuleSelector.get_personal_topic_proposal_order')
def test_add_used_topic_modules_not_reset(personal_topic_proposal_order, module_selector):

    # Given
    personal_topic_proposal_order.return_value = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
        TopicModule.FOOD.value,
        TopicModule.GAME.value,
    ]

    module_selector.used_topic_modules = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
    ]

    # When
    module_selector.add_used_topic_module(TopicModule.GAME.value)

    # Then
    assert module_selector.used_topic_modules == [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
        TopicModule.GAME.value,
    ]

    next_module, _ = module_selector.get_next_modules()[0]
    assert next_module == TopicModule.FOOD.value

@patch('selecting_strategy.module_selection.ModuleSelector.get_personal_topic_proposal_order')
def test_add_used_topic_modules_reset(personal_topic_proposal_order, module_selector):

    # Given
    personal_topic_proposal_order.return_value = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
        TopicModule.FOOD.value,
        TopicModule.GAME.value,
    ]

    module_selector.used_topic_modules = [
        TopicModule.MOVIE.value,
        TopicModule.ANIMAL.value,
        TopicModule.BOOK.value,
        TopicModule.FASHION.value,
        TopicModule.FOOD.value,
    ]

    # When
    module_selector.add_used_topic_module(TopicModule.GAME.value)
    # Then
    assert module_selector.used_topic_modules == []
    next_module, _ = module_selector.get_next_modules()[0]
    assert next_module == TopicModule.MOVIE.value

    # When
    module_selector.add_used_topic_module(TopicModule.FOOD.value)
    # Then
    assert module_selector.used_topic_modules == [TopicModule.FOOD.value]
    next_module, _ = module_selector.get_next_modules()[0]
    assert next_module == TopicModule.MOVIE.value

