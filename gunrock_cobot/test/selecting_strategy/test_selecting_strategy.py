from nlu.dataclass.central_element import CentralElementFeatures
from unittest.mock import patch
from selecting_strategy.util_selecting_strategy import predict_custom_sys_intent
from selecting_strategy.util_selecting_strategy import check_if_profanity_topic
from selecting_strategy.util_selecting_strategy import check_if_terminate_chat
from selecting_strategy.util_selecting_strategy import check_if_change_topic
from selecting_strategy.util_selecting_strategy import check_if_select_wrong
from selecting_strategy.util_selecting_strategy import check_if_opening
from selecting_strategy.util_selecting_strategy import check_if_controversial_topic
from selecting_strategy.util_selecting_strategy import check_if_finance_topic
from selecting_strategy.util_selecting_strategy import check_if_last_topic_module_unclear_and_propose_module
from selecting_strategy.util_selecting_strategy import check_if_jump_topic_command
from selecting_strategy.util_selecting_strategy import check_if_jump_topic_by_single_word
from selecting_strategy.util_selecting_strategy import check_if_append_last_module
from selecting_strategy.util_selecting_strategy import check_if_append_backstory
from selecting_strategy.util_selecting_strategy import check_if_jump_to_weather
from selecting_strategy.util_selecting_strategy import check_if_command_without_topic
from selecting_strategy.util_selecting_strategy import check_if_special_last_modules_cases
from selecting_strategy.util_selecting_strategy import check_if_pos_ans_following_propose_topic
from selecting_strategy.util_selecting_strategy import check_if_not_negative_sentiment_to_topic
from selecting_strategy.util_selecting_strategy import check_if_in_phatic_topic
from selecting_strategy.util_selecting_strategy import check_if_in_saycomfort_topic
from selecting_strategy.util_selecting_strategy import check_if_append_backstory_and_set_state
from selecting_strategy.util_selecting_strategy import check_if_has_question
from selecting_strategy.util_selecting_strategy import check_if_pos_sent_to_propose_module
from selecting_strategy.util_selecting_strategy import check_if_req_for_a_topic
from selecting_strategy.util_selecting_strategy import check_if_unclear_proposed_module
from selecting_strategy.util_selecting_strategy import check_if_pos_sent_to_unhandled_topic
from selecting_strategy.util_selecting_strategy import check_if_pos_sent_to_proposed_module
from selecting_strategy.util_selecting_strategy import check_if_unclear_last_module
from selecting_strategy.topic_module_proposal import last_module_propose_unclear
from nlu.question_detector import QuestionDetector

class UserAttributes:
    def __init__(self):
        self.pchat = {}
        self.prev_hash = {}
        self.last_module = ""
        self.previous_modules = []
        self.moviechat_user = {"propose_continue": "STOP"}
        self.bookchat_user = {"propose_continue": "STOP"}
        self.travelchat = {"propose_continue": "STOP"}
        self.gamechat = {"propose_continue": "STOP"}
        self.news_user = {"propose_continue": "STOP"}
        self.animalchat = {"propose_continue": "STOP"}
        self.musicchat = {"propose_continue": "STOP"}
        self.socialchat = {"propose_continue": "STOP"}
        self.weatherchat = {"propose_continue": "STOP"}
        self.timechat = {"propose_continue": "STOP"}
        self.holidaychat = {"propose_continue": "STOP"}
        self.comfortchat = {"propose_continue": "STOP"}
        self.sportchat = {"propose_continue": "STOP"}
        self.techsciencechat = {"propose_continue": "STOP"}
        self.tedtalkchat = {"propose_continue": "STOP"}
        self.foodchat = {"propose_continue": "STOP"}
        self.fashionchat = {"propose_continue": "STOP"}
        self.launchgreeting = {"propose_continue": "STOP"}
        self.dailylife = {"propose_continue": "STOP"}
        self.outdoorchat = {"propose_continue": "STOP"}
        self.sayfunny = {"propose_continue": "STOP"}



class TestPredictCustomSysIntent:
    #<Condition testing casews>#
    def test_check_if_profanity_topic(self, central_features = None, checking = False):
        #given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"topic": ["topic_profanity"]})
        # when
        return_value = check_if_profanity_topic(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False


    def test_check_if_terminate_chat(self, central_features = None, checking = False):
        #given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": ["terminate"],
                                                                  "topic": []})
        # when
        return_value = check_if_terminate_chat(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_profanity_topic(central_features=central_features, checking=True)

    def test_check_if_change_topic(self, central_features = None, checking = False):
        #given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": ["change_topic"],
                                                                  "topic": []})
        # when
        return_value = check_if_change_topic(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_terminate_chat(central_features=central_features, checking=True)

    def test_check_if_select_wrong(self, central_features = None, checking = False):
        #given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": ["select_wrong"],
                                                                  "topic": []})
        # when
        return_value = check_if_select_wrong(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_change_topic(central_features=central_features, checking=True)

    def test_check_if_opening(self, central_features = None, checking = False):
        #given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": [],
                                                                  "topic": [],
                                                                  "lexical": ["info_name", "ask_user_name"]})
        # when
        return_value = check_if_opening(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_select_wrong(central_features=central_features, checking=True)

    @patch('selecting_strategy.util_selecting_strategy.is_short_utterance')
    def testReqPlayGame_matchAll_shouldIncludeReqPlayGame(self, is_short_utterance, central_features = None, checking = False, user_attributes = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": ["req_play_game"],
                                                                      "topic": [],
                                                                      "lexical": []},
                                                  topic_modules=["GAMECHAT"])
        is_short_utterance.return_value = True

        # when
        if False == checking:
            custom_sys_intents = 'req_playgame'
        else:
            custom_sys_intents = None

        # then
        if False == checking:
            assert custom_sys_intents == "req_playgame"
        else:
            assert custom_sys_intents != "req_playgame"
        self.test_check_if_opening(central_features=central_features, checking=True)

    @patch('selecting_strategy.util_selecting_strategy.is_short_utterance')
    def testReqPlayMusic_matchAll_shouldReturnReqPlayMusic(self, is_short_utterance, central_features = None, checking = False, user_attributes = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": ["req_play_music"],
                                                                      "topic": [],
                                                                      "lexical": []},
                                                  topic_modules=["MUSICCHAT"])

        is_short_utterance.return_value = True

        # when
        if False == checking:
            predicted_custom_sys_intent = 'req_playmusic'
        else:
            predicted_custom_sys_intent = None

        # then
        if False == checking:
            assert predicted_custom_sys_intent == "req_playmusic"
        else:
            assert predicted_custom_sys_intent != "req_playmusic"
        self.test_check_if_opening(central_features=central_features, checking=True)

    def test_check_if_controversial_topic(self, central_features=None, checking=False, user_attributes = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": [],
                                                                      "topic": ["topic_controversial"],
                                                                      "lexical": ["ask_yesno"]},)
        # when
        return_value = check_if_controversial_topic(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.testReqPlayGame_matchAll_shouldIncludeReqPlayGame(central_features=central_features, checking=True, user_attributes = user_attributes)
        self.testReqPlayMusic_matchAll_shouldReturnReqPlayMusic(central_features=central_features, checking=True)

    def test_check_if_finance_topic(self, central_features=None, checking=False, user_attributes = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": [],
                                                                      "topic": ["topic_finance"],
                                                                      "lexical": ["ask_yesno"]},)
        # when
        return_value = check_if_finance_topic(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_controversial_topic(central_features=central_features, checking=True, user_attributes = user_attributes)

    def test_check_if_jump_topic_command(self, central_features=None, checking=False, last_module = None,
                                         user_attributes = None, propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": ["req_topic_jump"],
                                                                      "topic": [],
                                                                      "lexical": []},
                                                      dialog_act = "commands",
                                                      topic_modules = ["NEWS"])
        if None == last_module:
            last_module = "MOVIECHAT"
        if None == user_attributes:
            user_attributes = UserAttributes()
            user_attributes.moviechat_user = {"propose_continue": "STOP"}
            user_attributes.retrieval_propose = False
        # when
        return_value = check_if_jump_topic_command(central_features, last_module, propose_module, neg_ans_in_nlu)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_finance_topic(central_features=central_features, checking=True, user_attributes = user_attributes)

    def test_check_if_jump_topic_by_single_word(self, central_features=None, checking=False,
                                                return_nlp=None, last_module=None,
                                         user_attributes = None, propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": ["req_topic_jump_single"],
                                                                      "topic": [],
                                                                      "lexical": []},
                                                      topic_modules = ["NEWS"])
        if None == return_nlp:
            return_nlp =[{}]
        if None == last_module:
            last_module = "MOVIECHAT"

        # when
        return_value = check_if_jump_topic_by_single_word(central_features, return_nlp, last_module)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_jump_topic_command(central_features=central_features, checking=True, last_module = last_module,
                                         user_attributes = user_attributes, propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_append_last_module(self, central_features = None, checking = False, return_nlp=None, last_module = None,
                                         user_attributes = None, last_response = None, propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        #given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": ["req_more"],
                                                                  "topic": [],
                                                                  "lexical": []},
                                                      key_phrase = ["bitcoin"],)
        if None == last_module:
            last_module = "MOVIECHAT"
        if None == user_attributes:
            user_attributes = UserAttributes()
            user_attributes.moviechat_user = {"propose_continue": "CONTINUE"}
            user_attributes.retrieval_propose = False
        if None == last_response:
            last_response = "bitcoin"
        # when
        return_value = check_if_append_last_module(central_features, last_module, user_attributes, last_response, propose_module)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_jump_topic_by_single_word(central_features=central_features, checking=True,
                                                return_nlp=return_nlp, last_module=last_module,
                                         user_attributes = user_attributes, propose_module = propose_module,
                                                     pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)




    def test_check_if_append_backstory(self, central_features=None, checking=False, return_nlp=None, last_module=None,
                                         user_attributes = None, last_response = None, propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(backstory = {"confidence": 0.83, "module": "NEWS"})
        # when
        return_value = check_if_append_backstory(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_append_last_module(central_features=central_features, checking=True,
                                                     return_nlp = return_nlp, last_module = last_module,
                                                    user_attributes = user_attributes, last_response = last_response,
                                              propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu,
                                              neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_jump_to_weather(self, central_features=None, checking=False, return_nlp = None, last_module = None,
                                         user_attributes = None, last_response = None, propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(dialog_act = "open_question",
                                                      topic_modules = ["WEATHER"],
                                                      backstory = {"confidence": 0, "module": "NEWS"})
        # when
        return_value = check_if_jump_to_weather(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_append_backstory(central_features=central_features, checking=True,
                                            return_nlp = return_nlp, last_module = last_module,
                                         user_attributes = user_attributes, last_response = last_response,
                                            propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu,
                                            neg_ans_in_nlu = neg_ans_in_nlu)

    @patch('selecting_strategy.util_selecting_strategy.pronoun_in_text')
    def test_check_if_command_without_topic(self, pronoun_in_text, central_features=None,
                                            checking=False, return_nlp = None, last_module = None,
                                         user_attributes = None, last_response = None, propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(dialog_act="commands",
                                                      custom_intents={"sys": ["req_topic_jump"],
                                                                      "topic": [],
                                                                      "lexical": []},
                                                      topic_modules=[],
                                                      key_phrase = ["ethereum"],
                                                      backstory={"confidence": 0, "module": "NEWS"})
        pronoun_in_text.return_value = None
        if None == last_module:
            last_module = "MOVIECHAT"
        # when
        return_value = check_if_command_without_topic(central_features, last_module)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_jump_to_weather(central_features=central_features, checking=True,
                                           return_nlp = return_nlp, last_module = last_module,
                                            user_attributes = user_attributes, last_response = last_response,
                                           propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu,
                                           neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_special_last_modules_cases(self, central_features=None, checking=False,
                                                 return_nlp = None, last_module = None,
                                                 user_attributes = None, last_response = None, propose_module = None,
                                                 pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(backstory={"confidence": 0, "module": "NEWS"})
        if None == last_module:
            last_module = "STORYFUNFACT"
        if None == user_attributes:
            user_attributes = UserAttributes()
            user_attributes.previous_modules = ["MOVIECHAT", "BOOKCHAT"]
        # when
        return_value = check_if_special_last_modules_cases(user_attributes, last_module)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_command_without_topic(central_features=central_features, checking=True,
                                                 return_nlp = return_nlp, last_module = last_module,
                                                user_attributes = user_attributes, last_response = last_response,
                                                 propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu,
                                                 neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_current_topic_module_continue_and_propose_module(self, central_features=None, checking=False,
                                                                       return_nlp = None, last_module = None,
                                                                        user_attributes = None, last_response = None, propose_module = None,
                                                                       pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents={"sys": [],
                                                                      "topic": [],
                                                                      "lexical": []},
                                                      backstory = {"confidence": 0, "module": "NEWS"})
        if None == propose_module and False == checking:
            propose_module = "MUSICCHAT"
        if None == last_module:
            last_module = "MOVIECHAT"
        if None == user_attributes:
            user_attributes = UserAttributes()
            user_attributes.moviechat_user = {"propose_continue": "UNCLEAR"}
            user_attributes.previous_modules = ["MOVIECHAT", "BOOKCHAT"]
        if None == pos_ans_in_nlu:
            pos_ans_in_nlu = True
        if None == neg_ans_in_nlu:
            neg_ans_in_nlu = False
        # when
        return_value = check_if_last_topic_module_unclear_and_propose_module(user_attributes, last_module, propose_module, pos_ans_in_nlu, neg_ans_in_nlu)
        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_special_last_modules_cases(central_features=central_features, checking=True,
                                                      return_nlp=return_nlp, last_module=last_module,
                                                      user_attributes=user_attributes, last_response=last_response,
                                                      propose_module=propose_module, pos_ans_in_nlu=pos_ans_in_nlu,
                                                      neg_ans_in_nlu=neg_ans_in_nlu)

    def test_check_if_pos_ans_following_propose_topic(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None,
                                                propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == propose_module and False == checking:
            propose_module = "MOVIECHAT"
        if None == user_attributes:
            user_attributes = UserAttributes()
            user_attributes.moviechat_user = {"propose_continue": "STOP"}
            user_attributes.previous_modules = ["MOVIECHAT", "BOOKCHAT"]
        if None == pos_ans_in_nlu:
            pos_ans_in_nlu = True
        if None == neg_ans_in_nlu:
            neg_ans_in_nlu = False
        if None == last_module:
            last_module = "NEWS"
        # when
        return_value = check_if_pos_ans_following_propose_topic(propose_module, pos_ans_in_nlu)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_current_topic_module_continue_and_propose_module(central_features=central_features, checking=True,
                                                      return_nlp=return_nlp, last_module=last_module,
                                                      user_attributes=user_attributes, last_response=last_response,
                                                      propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu,
                                                      neg_ans_in_nlu = neg_ans_in_nlu)


    def test_check_if_positive_sentiment_to_topic(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None,
                                                propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            list_module_detection_with_amazon = ["NEWS"]
            central_features = CentralElementFeatures(topic_modules = ["NEWS"],
                                                      sentiment = "pos",
                                                      backstory={"confidence": 0, "module": "NEWS"})
        else:
            list_module_detection_with_amazon = []
        if None == pos_ans_in_nlu:
            pos_ans_in_nlu = False
        if None == neg_ans_in_nlu:
            neg_ans_in_nlu = False
        minor_features = None
        if None == user_attributes:
            user_attributes = UserAttributes()
            user_attributes.moviechat_user = {"propose_continue": "STOP"}
            user_attributes.previous_modules = ["MOVIECHAT", "BOOKCHAT"]
            user_attributes.social_open_question_proposing = True

        # when
        return_value = check_if_not_negative_sentiment_to_topic(return_nlp, minor_features, central_features, propose_module, neg_ans_in_nlu, list_module_detection_with_amazon, user_attributes)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_pos_ans_following_propose_topic(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                    propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu,
                                                           neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_in_phatic_topic(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None,
                                                propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents = {'sys': [],
                                                                        "topic": ["topic_phatic"],
                                                                        'lexical': [],},
                                                      topic_modules = [],
                                                      backstory={"confidence": 0, "module": "NEWS"})
        if None == neg_ans_in_nlu:
            neg_ans_in_nlu = True
        # when
        return_value = check_if_in_phatic_topic(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_positive_sentiment_to_topic(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                    propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_in_saycomfort_topic(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None,
                                                propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents = {'sys': [],
                                                                        "topic": ["topic_saycomfort"],
                                                                        'lexical': [],},
                                                      topic_modules = [],
                                                      backstory={"confidence": 0, "module": "NEWS"})
        # when
        return_value = check_if_in_saycomfort_topic(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_in_phatic_topic(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                    propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_append_backstory_and_set_state(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None,
                                                propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(backstory={"confidence": 0.83})
        # when
        return_value = check_if_append_backstory_and_set_state(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_in_saycomfort_topic(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                    propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_open_question_for_backstory(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None,
                                                propose_module = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents = {'sys': [],
                                                                        "topic": ["topic_backstory"],
                                                                        'lexical': [],},
                                                    dialog_act = "open_question_factual",
                                                    backstory={"confidence": 0, "module": "NEWS"})
        self.test_check_if_append_backstory_and_set_state(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                    propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_pos_sent_to_propose_module(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None, propose_module = None,
                                                 global_senti = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents = {'sys': [],
                                                                        "topic": [],
                                                                        'lexical': [],},
                                                    dialog_act = "dev_command",
                                                    backstory={"confidence": 0, "module": "NEWS"})
        if None == propose_module and False == checking:
            propose_module = "MOVIECHAT"

        if None == global_senti:
            global_senti = "pos"
        if None == neg_ans_in_nlu:
            neg_ans_in_nlu = False
        # when
        return_value = check_if_pos_sent_to_propose_module(propose_module, global_senti, neg_ans_in_nlu)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_open_question_for_backstory(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                    propose_module = propose_module, pos_ans_in_nlu = pos_ans_in_nlu)

    def test_check_if_req_for_a_topic(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None, propose_module = None,
                                        global_senti = None, pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents = {'sys': [],
                                                                        "topic": ["req_topic"],
                                                                        'lexical': [],},
                                                    dialog_act = "dev_command",
                                                    topic_modules = [],
                                                    backstory={"confidence": 0, "module": "NEWS"})
        if None == global_senti:
            global_senti = "neg"
        if None == neg_ans_in_nlu:
            neg_ans_in_nlu = True
        # when
        return_value = check_if_req_for_a_topic(central_features)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_pos_sent_to_propose_module(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                      propose_module = propose_module, global_senti = global_senti,
                                                      pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_unclear_proposed_module(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None,
                                                    propose_module = None, global_senti = None,
                                              pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents = {'sys': [],
                                                                        "topic": [],
                                                                        'lexical': [],},
                                                    dialog_act = "dev_command",
                                                    sentiment = "pos",
                                                    backstory={"confidence": 0, "module": "NEWS"})
        
        if None == pos_ans_in_nlu:
            pos_ans_in_nlu = True

        if None == last_module:
            last_module = "MOVIECHAT"
        if None == user_attributes:
            user_attributes = UserAttributes()
            user_attributes.moviechat_user = {"propose_continue": "UNCLEAR"}
            user_attributes.previous_modules = ["MOVIECHAT", "BOOKCHAT"]
            user_attributes.social_open_question_proposing = True
        # when
        return_value = check_if_unclear_proposed_module(last_module, central_features, user_attributes, pos_ans_in_nlu)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_req_for_a_topic(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                      propose_module = propose_module, global_senti = global_senti,
                                                      pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_neg_sent_to_topic(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None,
                                                    propose_module = None, global_senti = None,
                                              pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents = {'sys': [],
                                                                        "topic": [],
                                                                        'lexical': [],},
                                                    dialog_act = "dev_command",
                                                    sentiment = "pos",
                                                    key_phrase = ["it"],
                                                    backstory={"confidence": 0, "module": "NEWS"})
        if None == last_module:
            last_module = "NEWS"
        # when
        return_value = check_if_pos_sent_to_unhandled_topic(central_features, propose_module)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_unclear_proposed_module(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                      propose_module = propose_module, global_senti = global_senti,
                                                      pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_pos_sent_to_proposed_module(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None,
                                                  propose_module = None, global_senti = None,
                                              pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents = {'sys': [],
                                                                        "topic": [],
                                                                        'lexical': [],},
                                                    dialog_act = "dev_command",
                                                    sentiment = "pos",
                                                    backstory={"confidence": 0, "module": "NEWS"})
        if None == propose_module and False == checking:
            propose_module = "MOVIECHAT"
        if None == neg_ans_in_nlu:
            neg_ans_in_nlu = False
        if None == pos_ans_in_nlu:
            pos_ans_in_nlu = False
        # when
        return_value = check_if_pos_sent_to_proposed_module(propose_module, central_features, neg_ans_in_nlu)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_neg_sent_to_topic(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                      propose_module = propose_module, global_senti = global_senti,
                                                      pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)

    def test_check_if_unclear_last_module(self, central_features=None, checking=False,
                                                  return_nlp = None, last_module = None,
                                                  user_attributes = None, last_response = None,
                                                  propose_module = None, global_senti = None,
                                              pos_ans_in_nlu = None, neg_ans_in_nlu = None):
        # given
        if None == central_features:
            central_features = CentralElementFeatures(custom_intents = {'sys': [],
                                                                        "topic": [],
                                                                        'lexical': [],},
                                                    dialog_act = "dev_command",
                                                    sentiment = "neg",
                                                    backstory={"confidence": 0, "module": "NEWS"})
        if None == last_module:
            last_module = "MOVIECHAT"
        if None == user_attributes:
            user_attributes = UserAttributes()
            user_attributes.moviechat_user = {"propose_continue": "UNCLEAR"}
            user_attributes.previous_modules = ["MOVIECHAT", "BOOKCHAT"]
            user_attributes.social_open_question_proposing = True
        if None == neg_ans_in_nlu:
            neg_ans_in_nlu = True
        if None == pos_ans_in_nlu:
            pos_ans_in_nlu = False
        # when
        return_value = check_if_unclear_last_module(last_module, user_attributes)

        # then
        if False == checking:
            assert return_value == True
        else:
            assert return_value == False
        self.test_check_if_pos_sent_to_proposed_module(central_features=central_features, checking=True,
                                                      return_nlp = return_nlp, last_module = last_module,
                                                      user_attributes = user_attributes, last_response = last_response,
                                                      propose_module = propose_module, global_senti = global_senti,
                                                      pos_ans_in_nlu = pos_ans_in_nlu, neg_ans_in_nlu = neg_ans_in_nlu)
