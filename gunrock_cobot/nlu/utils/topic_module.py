import logging
import random
from typing import Optional, Set

from ..constants import TopicModule


logger = logging.getLogger(__name__)


def module_utterable(module: TopicModule) -> Optional[str]:
    from selecting_strategy.module_selection import topic_name_map

    try:
        utterables = topic_name_map[module.value]  # type: Set[str]
        return random.sample(utterables, k=1)[0]
    except (KeyError, IndexError):
        return None
