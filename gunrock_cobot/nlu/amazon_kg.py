import time
import json
import logging
import threading
from concurrent.futures import ThreadPoolExecutor

from cobot_common.service_client import get_client
from cobot_core.service_module import LocalServiceModule
from util_redis import RedisHelper
from aws_xray_sdk.core import xray_recorder


logger = logging.getLogger(__name__)

_EMPTY = "__EMPTY__"

TYPE_BLACKLIST = {
    'aio:AgreementMakingEntity',
    'aio:ThingWithACreationDate',
    'aio:Thing',
    'aio:VisibleObject',
    'aio:EntityCapableOfDeliberateAction',
    'aio:Animal',
    'aio:ObjectWithAHeight',
    'aio:ObjectWithAMass',
    'aio:ObjectWhichCanHaveAGeographicalLocation',
    'aio:PhysicalObject',
}


def make_kg_item(phrase, name_and_type):
    entity = {'name': phrase}
    entity.update(name_and_type)
    return entity


def _is_valid(kg_item):
    return isinstance(kg_item, dict) and kg_item.get('entity_name', '')


def _assign_module(kg_item):
    return _EMPTY


def latency_trace(default_constructor):
    def wrap(exec_func):
        def wrapped_fn(self):
            start = time.time()
            cls_name = type(self).__name__
            output = default_constructor()

            try:
                xray_recorder.begin_segment(f'[NLP MODULES] {cls_name}')

                output = exec_func(self)

            except Exception as e:
                logger.error(f'[{cls_name}] get error: {e}', exc_info=True)
            finally:
                latency = (time.time() - start) * 1000
                xray_recorder.end_segment()
                self.logger.info(f'Finished {cls_name}, latency:{latency}ms')

            return output

        return wrapped_fn
    return wrap


def cache_query(redis_table_prefix, duration):
    def wrap(query_fn):
        def wrapped_fn(cls_obj, key):
            output = RedisHelper().get(redis_table_prefix, key)
            if output is not None:
                return output
            output = query_fn(cls_obj, key)
            if output:
                RedisHelper().set(redis_table_prefix, key, output, expire_in_sec=duration)
            else:
                RedisHelper().set(redis_table_prefix, key, output, expire_in_sec=24*60*60 * 7)
            return output

        return wrapped_fn
    return wrap


class AmazonKG(LocalServiceModule):
    @latency_trace(list)
    def execute(self):
        output = []
        kps_in_segments = self.input_data['keyphrase']
        amazonkg_cache = self.input_data.get('amazonkgraw', {})
        if kps_in_segments:
            for phrases in kps_in_segments:
                output.append([amazonkg_cache[p] for p in phrases if p in amazonkg_cache])

        return output


class AmazonKGRaw(LocalServiceModule):
    API_KEY = 'EwecQ1fkEd4YMVUOHHpeSam7zbef53gipQsxAUki'
    client = get_client(api_key=API_KEY, timeout_in_millis=350)

    @latency_trace(dict)
    def execute(self):
        output = {}
        phrases = self.input_data['keyphraseraw']
        if phrases:
            descriptions = self.get_description(phrases)
            output = {p: d for p, d in zip(phrases, descriptions)}
        return output

    def get_description(self, phrases):
        if not phrases:
            return []
        cls_name = type(self).__name__
        descriptions = [[] for _ in phrases]

        try:
            descriptions = self.fetch_multithreaded(phrases)
            assert len(descriptions) == len(phrases)

            for kg_items in descriptions:
                for kg_item in kg_items:
                    kg_item['topic_module'] = _assign_module(kg_item)
                    kg_item['entity_type'] = list(set(kg_item['entity_type']) - TYPE_BLACKLIST)

        except Exception as e:
            logger.error(f'[{cls_name}] get_description: {e}', exc_info=True)

        return descriptions

    def fetch_multithreaded(self, phrases: list):
        result = [[] for _ in phrases]
        with ThreadPoolExecutor() as executor:
            futures = [executor.submit(self.fetch, ph) for ph in phrases]
        results = [f.result() for f in futures]
        return results

    @cache_query(RedisHelper.PREFIX_AMAZONKG_NAME, 24*60*60 * 60)
    def resolve_entity(self, phrase):
        response = self.client.get_knowledge_entity_resolution(mention={'text': phrase})
        entities = response['resolvedEntities']

        entity_ids = [e['value'] for e in entities if e['dataType'] == 'aio:Entity']
        return entity_ids

    @cache_query(RedisHelper.PREFIX_AMAZONKG_TYPE, 24*60*60 * 60)
    def resolve_entity_name_type(self, entity_id):
        query = {'text': f'query class, label\n<{entity_id}> <aio:isAnInstanceOf> class\n<{entity_id}> <aio:prefLabel> label'}

        def _unpack(binding):
            return {field['variable']: field['value'] for field in binding['bindingList']}

        response = self.client.get_knowledge_query_answer(query=query)
        output = [_unpack(binding) for binding in response['results']]

        if output:
            types = [o['class'] for o in output if o['class'].startswith('aio')]
            return {'entity_name': output[0]['label'], 'entity_type': types}

        return {'entity_name': '', 'entity_type': []}

    @cache_query(RedisHelper.PREFIX_AMAZONKG_DIRECTMAP, 24*60*60 * 60)
    def fetch(self, phrase):
        output = []
        try:
            ids = self.resolve_entity(phrase)
            with ThreadPoolExecutor() as executor:
                futures = [executor.submit(self.resolve_entity_name_type, id_) for id_ in ids]
            output = [make_kg_item(phrase, f.result()) for f in futures]
            output = [item for item in output if _is_valid(item)]

        except Exception as e:
            logger.error(f'[{type(self).__name__}] fetch: {e}')
        return output
