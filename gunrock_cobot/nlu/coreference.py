import random
import re
import time

import requests
import logging

from cobot_core.service_module import LocalServiceModule
from nlu.server_urls import NER_URL
from util_redis import RedisHelper
from selecting_strategy.topic_mapping import google_kg_description_topic_map

from nlu.google_kg import _old_format, _4item_format, _old_format_descrp, _first_sentence, make_kg_item, _is_valid, \
    _target_module_from_utterance, _assign_module, get_target_module, latency_trace, cache_query


logger = logging.getLogger(__name__)


class Coreference(LocalServiceModule):
    """ex. kg return: [
                    [['Taylor Swift', 'singer', '1000', 'music'],['Tom Hanks',
                        'actor', '1000', 'movie']], #this is for alexa response
                    [['The avengers', 'movie', '999', 'movie'],['holiday',
                        'event', '200', 'None']] #this is for user request
                    ]"""
    # Override the Execute Method
    # pronoun_list = ["one", "another", "more", "it", "that",
    #                 "this", "him", "her", "he", "she", "they", "them"]
    pronoun_list = ["it", "him", "her", "he", "she", "they", "them"]
    remove_ner_list = ["hmm"]
    # pronoun_pattern = r"another one|that|more|another|it|this|him|her|he|she|they|them"
    request_list = ["another one", "another", "more", "1", "one"]
    person_pronoun = ["him", "her", "he", "she", "they", "them", "that"]
    ignore_list = ["you", "i", "me", "another", "s", "my name", "your name",
                   "it", "wow", "yeah", "yes", "ya", "cool", "no", "that", "this", "a bit", "some more",
                   "another one", "another", "anything", "something else", "something", "a lot",
                   "a little", "a little bit", "something interesting"]
    implicit_coref = ["how about you", "what's your favorite"]
    ignore_complex = ["does it", "is it", "was it", "it is", "it was", "what it"]

    timeout = .8
    key1 = "AIzaSyA6rMqYFRo339KPfpj_oJAQ6njOxiju0Hw"
    key2 = "AIzaSyADclYXhTdFWrRlI4aB9BbzcXESHHfbRKw"
    key3 = "AIzaSyCh9JDnPpPs6dDgY7hqCX93-FIEBbfFd48"
    key4 = "AIzaSyDAQFZRMyB0N7ttspg-U_BP1SYihLGwHU8"
    keys = [key1, key2, key3, key4]

    def execute(self):
        start = time.time()
        """replace pronoun to topic from previous turns (ex. if the user says anothe three times)
        also all the other features to use current_state_input instead of input to go through the nlp pipeline"""

        if self.state_manager.user_attributes.topic_to_replace is not None:
            topic_to_replace = self.state_manager.user_attributes.topic_to_replace
        else:
            self.state_manager.user_attributes.topic_to_replace = []
            topic_to_replace = []

        if self.state_manager.user_attributes.last_module is not None:
            last_module = self.state_manager.user_attributes.last_module
        else:
            last_module = ""
        if self.state_manager.user_attributes.propose_topic is not None:
            propose_topic = self.state_manager.user_attributes.propose_topic
        else:
            propose_topic = ""
        if len(propose_topic) > 0:
            potential_current_topic = propose_topic
        elif len(last_module) > 0:
            potential_current_topic = last_module
        else:
            potential_current_topic = ""

        if len(topic_to_replace) > 0 and topic_to_replace[-1] != potential_current_topic:
            topic_to_replace = []
            self.state_manager.user_attributes.topic_to_replace = []
        to_replace = ""
        replace_request = ""
        replace_response = ""
        replace_this_sentence = ""
        last_state_response = ""
        return_replace = []
        ners = []
        response_kg, request_kg = [], []
        this_sentence_kg = []

        # DEBUG
        google_kg = self.input_data['googlekg']

        self.logger.info("[COREFERENCE] google_kg = {}".format(google_kg))
        try:
            ignore_it = False
            if self.state_manager.last_state is not None:
                last_bot_response = self.state_manager.last_state.get('response', "")
                last_bot_response = re.sub(r'\<.*?\>', "", last_bot_response)
                if len(last_bot_response.strip()) > 0 and last_bot_response[-1] == "?":
                    ignore_it = True
                splited_response = re.split('\, |\. |\! |\?', last_bot_response)
                for s_response in splited_response:
                    if len(s_response.strip()) > 0:
                        if s_response.split()[-1] == "it":
                            ignore_it = True
                            break
            else:
                last_bot_response = ""
            if self.input_data["converttext"] is None:
                return {}
            text = self.input_data["converttext"]
            if text is not None:
                text = text[0]
            if not isinstance(text, str):
                return {}
            s_curr_utt = text.split()
            text_list = text.lower().split()
            for pron in self.pronoun_list:
                if pron in text_list:
                    pron_index = text_list.index(pron)
                    # i think that it is cool
                    # who is that
                    if pron == "that" and (not text.endswith("that") or "who" not in text_list):
                        continue
                    # tell me some more about it
                    if pron == "more" and not text.endswith("more"):
                        continue
                    # if sys says "do you want to talk about it" then ignore "it" in coreference
                    if pron == "it" and ignore_it:
                        continue
                    # if "one" is not with "another"
                    if pron == "one" and \
                            (pron_index == 0 or (pron_index > 0 and text_list[pron_index - 1] != "another")):
                        continue
                    if pron_index > 0:
                        if (text_list[pron_index - 1] + " " + text_list[pron_index]) in self.ignore_complex:
                            continue
                    if pron_index < len(text_list) - 1:
                        if (text_list[pron_index] + " " + text_list[pron_index + 1]) in self.ignore_complex:
                            continue
                    to_replace = pron
                    # tell me about that
                    if to_replace == "more" and text_list[-1] == "that":
                        to_replace = "that"
                    break

            # if re.search(self.pronoun_pattern, text):
            #     candidate_to_replace = re.search(self.pronoun_pattern, text).group(0)
            replace_this_sentence = ""

            if len(self.input_data["googlekg"]) > 0 and self.input_data["googlekg"][0] is not None:
                noun_phrases = []
                for gp in self.input_data['googlekg'][0]:
                    for t in gp:
                        self.logger.info("[NOUNPHRASE] {}".format(t))
                        self.logger.info("[NOUNPHRASE] gp {}".format(gp))
                        noun_phrases.append((t[0], float(t[2])))
                noun_phrases.sort(key=lambda t: t[1], reverse=True)
                for np in noun_phrases:
                    if np[0] in self.ignore_list:
                        noun_phrases.remove(np)
                if len(noun_phrases) >= 1:
                    if noun_phrases[0][1] > 700:
                        replace_this_sentence = noun_phrases[0][0].lower()
                google_kg = self.input_data["googlekg"][0]
                if google_kg is not None:  # if googlekg is an empty list, Dynamo will store as None
                    for gp in google_kg:
                        for k in gp:
                            if k[0].lower() == replace_this_sentence:
                                this_sentence_kg.append([k[0], k[1], str(k[2]), k[3]])
                                break
                        if len(this_sentence_kg) > 0:
                            break
            self.logger.info("[COREFERENCE] replace_this_sentence = {}, kg = {}".format(
                replace_this_sentence, this_sentence_kg))
            if to_replace != "":
                # get NER from last response by spacy server
                if to_replace not in self.request_list and "more" not in text:  # tell me more about it
                    last_state = self.state_manager.last_state
                    if last_state is not None:
                        last_state_response = last_state.get('response', "")

                        last_state_response = re.sub(
                            "'", " ", last_state_response)
                        last_state_response = re.sub(
                            r'\<.*?\>', "", last_state_response)  # get rid of speechcon
                        request_url = NER_URL + last_state_response
                        ners = requests.get(
                            request_url, timeout=self.timeout).json()
                    for ent in ners:
                        if ent[0].lower() in self.remove_ner_list:
                            ners.remove(ent)
                    if len(ners) > 1:
                        temp_list = []
                        if to_replace in self.person_pronoun:
                            for ent in ners:
                                if ent[1] == "PERSON":
                                    temp_list.append(ent)
                                    ners.remove(ent)
                        else:
                            for ent in ners:
                                if ent[1] != "PERSON":
                                    temp_list.append(ent)
                                    ners.remove(ent)
                        temp_list += ners
                        ners = temp_list

                    # ex. ners: [['Pride and Prejudice', 'WORK_OF_ART'],
                    #            ['Mark Twain', 'PERSON'], [Jane Austen, 'PERSON']]

                    if len(ners) > 0:
                        replace_response = ners[-1][0]
                        try:
                            response_kg = self._get_description(
                                [replace_response])
                        except Exception as e:
                            response_kg = []
                            self.logger.error(
                                "[COREFERENCE] get_description result error: {}".format(e), exc_info=True)

                if len(self.input_data["npknowledge"]) > 1 and self.input_data["npknowledge"][1] is not None:
                    noun_phrase = self.input_data["npknowledge"][1]["noun_phrase"]
                    for np in noun_phrase:
                        if np in self.ignore_list:
                            noun_phrase.remove(np)
                    if len(noun_phrase) == 1:
                        replace_request = noun_phrase[0]
                    elif len(noun_phrase) > 1:
                        replace_request = max(noun_phrase, key=len)
                    google_kg = self.input_data["npknowledge"][1]["knowledge"]
                    # print("<<<<<<<<<<")
                    # print(google_kg)
                    if google_kg is not None:  # if googlekg is an empty list, Dynamo will store as None
                        for k in google_kg:
                            if k[0].lower() in replace_request:
                                request_kg.append(
                                    [k[0], k[1], str(k[2]), k[3]])
                                break
                replace_ne = ""
                if replace_this_sentence != "":
                    if text.endswith("more"):
                        text = text + " about " + replace_this_sentence
                    else:
                        text = re.sub("\\b" + to_replace + "\\b", replace_this_sentence, text)
                    self.state_manager.user_attributes.topic_to_replace = [
                        replace_this_sentence, this_sentence_kg, potential_current_topic]
                    replace_ne = replace_this_sentence
                elif replace_response != "":
                    if text.endswith("more"):
                        text = text + " about " + replace_response
                    else:
                        text = re.sub("\\b" + to_replace + "\\b", replace_response, text)
                    self.state_manager.user_attributes.topic_to_replace = [
                        replace_response, response_kg, potential_current_topic]
                    replace_ne = replace_response
                elif replace_request != "":
                    if text.endswith("more"):
                        text = text + " about " + replace_request
                    else:
                        text = re.sub("\\b" + to_replace + "\\b", replace_request, text)
                    self.state_manager.user_attributes.topic_to_replace = [
                        replace_request, request_kg, potential_current_topic]
                    replace_ne = replace_request
                else:
                    # get topic_to_replace from context which may be from a different topic module
                    # if potential_current_topic != self.state_manager.user_attributes.topic_to_replace[-1]:
                    #     return {}
                    if len(self.state_manager.user_attributes.topic_to_replace) > 1:
                        # to_replace = "\\b" + to_replace + "\\b"
                        if text.endswith("more"):
                            text = text + " about " + self.state_manager.user_attributes.topic_to_replace[0]
                        else:
                            text = re.sub("\\b" + to_replace + "\\b",
                                          self.state_manager.user_attributes.topic_to_replace[0], text)
                        response_kg = self.state_manager.user_attributes.topic_to_replace[1]
                        replace_ne = self.state_manager.user_attributes.topic_to_replace[0]
                    else:
                        return {}

                # ners/noun_phrase may not be assigned
                try:
                    return_replace = ners + noun_phrase
                except Exception:
                    pass

                if len(return_replace) == 0:
                    if self.state_manager.user_attributes.topic_to_replace is not None and \
                            len(self.state_manager.user_attributes.topic_to_replace) > 0:
                        return_replace = self.state_manager.user_attributes.topic_to_replace[0]

                return_kg = []
                if response_kg != []:
                    return_kg.append(response_kg)
                if request_kg != []:
                    return_kg.append(request_kg)

                self.logger.info("[COREFERENCE] build segmented text")
                segmented_text = self.input_data['segmentation'][0]['segmented_text_raw']
                segmented_text_output = []
                for seg_text in segmented_text:
                    if len(replace_ne) > 0 and re.search("\\b" + to_replace + "\\b", seg_text):
                        if seg_text.endswith("more"):
                            seg_text = seg_text + " about " + replace_ne
                        else:
                            seg_text = re.sub("\\b" + to_replace + "\\b", replace_ne, seg_text)
                    segmented_text_output.append(seg_text)
                self.logger.info("[COREFERENCE] almost finished.")

                return {
                    "text": text.lower(),
                    "replace": {to_replace: return_replace},
                    "kg": return_kg, "replace_ne": replace_ne,
                    "segmented_text": segmented_text_output}

        except Exception as e:
            self.logger.error(
                "[COREFERENCE] nlp get result error: {}".format(e), exc_info=True)

        end = time.time()
        self.logger.info(
            "Finished Coreference, latency:{}ms".format((end - start) * 1000))
        # return a string, list, or dict
        return {
            "text": self.input_data["converttext"],
            "replace": {},
            "kg": [], "replace_ne": "",
            "segmented_text": self.input_data['segmentation'][0]['segmented_text_raw']}

    def _get_description(self, NP):
        descriptions = []
        for ne in NP:
            query = ne.replace(" ", "+")
            key = self.keys[random.randint(0, 3)]

            description = RedisHelper().get(self.get_redis_key(), ne)

            # in case description is topk which will be a list of list
            if description is not None and len(description) > 0 and isinstance(description[0], list):
                description = [description[0][0], description[0][2], description[0][3], description[0][4]]
            # print(description)
            if description is not None and description[-1] is None:
                description[-1] = "__EMPTY__"

            if description is not None:
                descriptions.append(description)
                break

            url = "https://kgsearch.googleapis.com/v1/entities:search?query=" + \
                query + "&key=" + key + "&limit=1&indent=True"
            try:
                response = requests.get(url, timeout=self.timeout).json()
                if "itemListElement" in response and len(response["itemListElement"]) > 0:
                    if "result" in response["itemListElement"][0] and \
                            "description" in response["itemListElement"][0]["result"]:
                        description = response["itemListElement"][0]["result"]["description"]
                        score = response["itemListElement"][0]["resultScore"]
                    else:
                        description = ""
                        score = 0
                    if description != "":
                        logger.info(
                            "[COREFERENCE] get raw description: {}".format(description))
                        module = self._get_module(description)
                        # enable return the knowledge of anything
                        # descriptions.append([ne, description, str(score), module])

                        # only store the knowledge of the first ner
                        full_description = [
                            ne, description, str(score), module]
                        # disable setting redis because this is only top1
                        # utils.set_redis_with_expire(
                        #     self.PREFIX, ne, full_description, expire=24 * 60 * 60 * 150)
                        descriptions = full_description

                        RedisHelper().set(self.get_redis_key(), ne, descriptions, expire=24 * 60 * 60 * 7)

            except Exception as e:
                logger.error(
                    "[COREFERENCE] get description error: {}".format(e), exc_info=True)
                pass

        return descriptions

    def _get_module(self, description):
        for d in description.lower().split():
            if d in google_kg_description_topic_map:
                return google_kg_description_topic_map[d]
        return "__EMPTY__"

    def get_redis_key(self):
        return RedisHelper.PREFIX_NPKNOWLEDGE_BLANK_DESCRIPTION
