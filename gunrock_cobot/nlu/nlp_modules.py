import re
import os
import json
import time
import logging
import requests
from typing import List
from concurrent.futures import ThreadPoolExecutor

from nlu.coreference import Coreference
from nlu.google_kg import GoogleKGRaw, GoogleKG, GoogleKG_v2
from nlu.google_kg import latency_trace
from nlu.amazon_kg import AmazonKG, AmazonKGRaw
from num2words import num2words
from phonetics import dmetaphone
from fuzzywuzzy import fuzz

import utils
from stanfordcorenlp import StanfordCoreNLP
import copy

from cobot_core.service_url_loader import ServiceURLLoader
from cobot_core.service_module import ToolkitServiceModule, LocalServiceModule
from cobot_core.nlp.nlp_modules import Sentiment
from util_redis import RedisHelper
from selecting_strategy.util_selecting_strategy import read_returnnlp
from nlu.intent_classifier import IntentClassifier
from nlu.utils.type_checking import ensure_type, safe_cast
from nlu.server_urls import DIALOG_ACT_V3, DIALOG_ACT_V4, SEGMENTATION_URL, SEGMENTATION_URL_V2, SENTENCE_COMPLETION_URL, SENTIMENT_URL, DEPENDENCY_URL, \
    STANFORD_NLP_URL, NP_URL
from nlu.server_urls import KEYPHRASE_URL_V1
from nlu.server_urls import GENERIC_NER_V1
import xml.etree.ElementTree as ET
from backstory_client import get_backstory_response, get_fallback_backstory_response

from aws_xray_sdk.core import xray_recorder
from aws_xray.aws_xray_config import config as configure_xray
from aws_xray.register_segments import register_segments
from nlu.constants import DialogAct as DialogActEnum

configure_xray()
register_segments()

CENTRAL_ELE_DA_THRESHOLD = 0.6

logger = logging.getLogger(__name__)

def get_noun_phrase_ignore_list():
    absolute_path = os.path.dirname(os.path.abspath(__file__))
    filepath = absolute_path + "/noun_phrase_ignore_list.txt"
    ignore_phrases = []
    with open(filepath, 'r') as f:
        ignore_phrases = [line.strip() for line in f]

    return ignore_phrases

class ProfanityCheck(LocalServiceModule):
    def execute(self):
        return False
        # try:
        #     text = self.input_data['text']
        #     intent_classifier = IntentClassifier(text)
        #     result = intent_classifier.is_profanity()
        #     logger.debug("[ProfanityCheck]: result {}".format(result))
        #     return result
        # except Exception:
        #     logger.error("[ProfanityCheck] processing error in code:",  exc_info=True)
        #     return False # Default to False
        # finally:
        #     xray_recorder.end_segment()


class IntentClassify(LocalServiceModule):
    def execute(self):
        xray_recorder.begin_segment('[NLP MODULES] {}'.format(self.__class__.__name__))
        logger.info('[INTENTCLASSIFY] start processing intent classify')
        segmented_text = self.input_data["segmentation"]["segmented_text_raw"]
        coreference = self.input_data["coreference"]
        if coreference is not None:
            if 'segmented_text' in coreference:
                segmented_text = coreference['segmented_text']
        if len(segmented_text) == 0:
            segmented_text.append("")
        ret_intent_list = []
        for text in segmented_text:
            intent_classifier = IntentClassifier(text.lower())
            ret_intent = intent_classifier.getIntents()
            ret_intent_list.append(ret_intent)
            # print(ret_intent)
        logger.info(
            '[INTENTCLASSIFY] get custom intents list {}'.format(ret_intent_list))
        return ret_intent_list[0]


class IntentClassify2(LocalServiceModule):
    def execute(self):
        try:
            xray_recorder.begin_segment('[NLP MODULES] {}'.format(self.__class__.__name__))
            logger.info('[INTENTCLASSIFY2] start processing intent classify')
            segmented_text = self.input_data["segmentation"]["segmented_text_raw"]
            coreference = self.input_data["coreference"]
            if coreference is not None:
                if 'segmented_text' in coreference:
                    segmented_text = coreference['segmented_text']
            if len(segmented_text) == 0:
                segmented_text.append("")
            ret_intent_list = []
            for text in segmented_text:
                intent_classifier = IntentClassifier(text.lower())
                ret_intent = intent_classifier.getIntents()
                ret_intent_list.append(ret_intent)
                # print(ret_intent)
            logger.info(
                '[INTENTCLASSIFY2] get custom intents list {}'.format(ret_intent_list))
            return ret_intent_list
        except Exception:
            logger.error("[INTENTCLASSIFY2] processing error in code:",  exc_info=True)
            return [] # empty list is checked when constructing returnnlp
        finally:
            xray_recorder.end_segment()


class SpacyNP(LocalServiceModule):
    whnp_list = ["what", "why", "who", "which",
                 "where", "when", "would", "how"]
    timeout = (0.1, 0.1)

    def execute(self):
        try:
            xray_recorder.begin_segment('[NLP MODULES] {}'.format(self.__class__.__name__))
            nps = []
            start = time.time()
            try:
                text = self.input_data["text"]
                if self.input_data["converttext"] is not None:
                    text = self.input_data["converttext"]
                self.logger.info(
                    "SpacyNP get text {}".format(text))
                nps = self.get_np_spacy(text)
            except Exception as e:
                logger.error(
                    "[SpacyNP] get noun phrase error: {}".format(e), exc_info=True)
            end = time.time()
            self.logger.info(
                "SpacyNP noun phrase {}".format(nps))
            self.logger.info(
                "Finished SpacyNP, latency:{}ms".format((end - start) * 1000))
            return nps
        except Exception:
            logger.error("[INTENTCLASSIFY2] processing error in code:",  exc_info=True)
            return [] # empty list is checked when constructing returnnlp
        finally:
            xray_recorder.end_segment()

    def get_np_spacy(self, text):
        return_nps = []
        try:
            request_url = NP_URL + text
            # nps is a list of noun_phrase and they are lower case returned by the api server without post processing
            nps = requests.get(request_url, timeout=self.timeout).json()
            for np in nps:
                if np not in get_noun_phrase_ignore_list() and not (len(np.split()) > 1 and
                                                          np.split()[0].lower() in self.whnp_list):
                    return_nps.append(np)
        except Exception as e:
            logger.error(
                "[SpacyNP] get noun phrase error: {}".format(e), exc_info=True)

        return return_nps


class GenericNer(LocalServiceModule):
    def execute(self):
        out = {'module_selection': {}, 'movie_detail': {}, 'music_detail': {}, 'book_detail': {}}
        try:
            xray_recorder.begin_segment('[NLP MODULES] {}'.format(self.__class__.__name__))

            last_state = self.state_manager.last_state
            current_state = self.state_manager.current_state
            data = {
                'last_bot_response': last_state.get('response', '') if last_state else '',
                'user_utterance': getattr(current_state, 'converttext', '')
            }
            headers = {'Content-Type': 'application/json'}
            data = json.dumps(data)
            resp = requests.post(GENERIC_NER_V1, headers=headers, data=data, timeout=(0.5, 0.5))
            out = resp.json()
        except Exception:
            logger.error("[GenericNer] processing error in code:",  exc_info=True)
        finally:
            xray_recorder.end_segment()

        return out


class KeyPhrase(LocalServiceModule):
    def execute(self):
        tmp = self.input_data.get('keyphraseraw', None)
        keyphrases = tmp if tmp else []
        segments = self.input_data.get('segmentation', {}).get('segmented_text_raw', [])

        pos = 0
        segmented_keyphrases = [[] for _ in segments]
        for i, seg in enumerate(segments):
            while True:
                if pos >= len(keyphrases):
                    break
                if keyphrases[pos] in seg:  # phrase in segment
                    segmented_keyphrases[i].append(keyphrases[pos])
                    pos += 1  # next phrase
                    continue

                if i + 1 < len(segments):
                    start = f'{seg} {segments[i+1]}'.find(keyphrases[pos])
                    if start >= 0 and start < len(seg):  # phrase crossing segment boundary
                        segmented_keyphrases[i].append(keyphrases[pos])
                        segmented_keyphrases[i+1].append(keyphrases[pos])
                        pos += 1  # next phrase

                # phrase not in segment
                break  # next segment
        return segmented_keyphrases


class KeyPhraseRaw(LocalServiceModule):
    def _stem(self, phrase):
        if phrase.endswith("'s"):
            return phrase[:-2]
        return phrase

    @latency_trace(list)
    def execute(self):
        last_state = self.state_manager.last_state
        current_state = self.state_manager.current_state
        data = {
            'last_bot_response': last_state.get("response", '') if last_state else '',
            'user_utterance': getattr(current_state, "converttext", '')
        }
        headers = {'Content-Type': 'application/json'}
        data = json.dumps(data)
        resp = requests.post(KEYPHRASE_URL_V1, headers=headers, data=data, timeout=(0.3, 0.5))

        return [self._stem(kp) for kp in resp.json()]


class NounPhrase2(LocalServiceModule):
    timeout = (0.3, 0.5)
    nlp = StanfordCoreNLP(
        STANFORD_NLP_URL, port=5022, timeout=timeout)


    def execute(self):
        start = time.time()
        # add your own coreference logic here, using input_data['text'][0..2]
        # return a string, list, or dict

        logger.info(
            "[NOUNPHRASE2] nlp get from stanford core nlp: {}".format(self.nlp))

        nps, local_nps = [], []

        try:
            xray_recorder.begin_segment('[NLP MODULES] {}'.format(self.__class__.__name__))
            if self.input_data['segmentation'] is not None:
                segmented_text = self.input_data['segmentation']["segmented_text_raw"]
            else:
                segmented_text = []
            for text in segmented_text:
                np, local_np = self._get_result(text, segmented_text)
                nps += [np]
                local_nps += [local_np]

        except Exception as e:
            logger.error(
                "[NOUNPHRASE2] nlp get result error: {}".format(e), exc_info=True)
        finally:
            xray_recorder.end_segment()
        end = time.time()
        self.logger.info(
            "Finished NOUNPHRASE2, latency:{}ms".format((end - start) * 1000))


        return {"noun_phrase": nps, "local_noun_phrase": local_nps}

    def _get_result(self, utterance, segmented_text):
        logger.info("[NOUNPHRASE2] get result with utt: {}".format(utterance))
        # utterance uses a truecaser
        start = time.time()
        np, local_np = self._get_NP(utterance, segmented_text)
        end = time.time()
        self.logger.info(
            "Finished NOUNPHRASE2 _get_NP, latency:{}ms".format((end - start) * 1000))

        logger.info("[NOUNPHRASE2] get result with np: {}".format(np))
        logger.info(
            "[NOUNPHRASE2] get result with local_np: {}".format(local_np))

        return np, local_np

    def _get_NP(self, text, segmented_text):
        NP = []
        try:
            text = text.lower()
            leng = len(text.split())
            ori_text = text
            text = [i.strip() for i in self.nlp.parse(text).split("\n")]
        except Exception as e:
            logger.error(
                "[NOUNPHRASE2] get_NP get text error: {}".format(e), exc_info=True)
            return NP, []
        np_list = []
        local_np = []
        """ex. text = 'i like harry potter and the sorcerer's stone'
                np_list = ['harry potter, the sorcerer s', the sorcerer's stone, harry potter and the sorcerer s stone]
                local_np = [harry potter, the sorcerer 's]"""

        try:
            i = 0  # i is used to get the local noun phrases
            for chunk in text:
                if chunk[1:3] == "NP":
                    NP.append([0, "", i])
                j = 0
                while j < len(chunk):
                    if chunk[j] == "(":
                        NP = [[b[0] + 1, b[1], b[2]] for b in NP]
                    if chunk[j] == ")":
                        NP = [[b[0] - 1, b[1], b[2]] for b in NP]
                    for b in NP:
                        if b[0] == 0:
                            phrase = b[1].strip()
                            # solve potential empty string issue
                            if phrase not in get_noun_phrase_ignore_list() and len(phrase) > 0:
                                np_list.append(phrase)
                                if i == b[2]:
                                    local_np.append(phrase)
                            NP.remove(b)
                    if not chunk[j].islower():
                        j += 1
                        continue
                    while chunk[j].islower():
                        if not chunk[j - 1].islower():
                            NP = [[b[0], b[1] + " ", b[2]] for b in NP]
                        NP = [[b[0], b[1] + chunk[j], b[2]] for b in NP]
                        j += 1
                i += 1

            # if only one word that is VBG, then add to np (ex. running; farming)
            if leng == 1 and len(np_list) == 0:
                if len(text) > 0 and "VBG" in text[0]:
                    np_list.append(ori_text)
                    local_np.append(ori_text)

            if self.input_data["spacynp"] is not None:
                spacynp = self.input_data["spacynp"]
                # modified_text = " ".join(sent for sent in segmented_text)
                if len(spacynp) > 0:
                    for i in spacynp:
                        if i.split()[-1] in ori_text and i.strip() not in np_list:
                            np_list.append(i)

            # add a new element that removes "the" at the beginning of a noun for entity detection (knowledge) purpose
            np_list = self.filter_my(np_list)
            local_np = self.filter_my(local_np)
            np_list = self.rm_the(np_list)
            local_np = self.rm_the(local_np)
            np_list = self.temp_fix(np_list, " ".join(sent for sent in segmented_text))

        except Exception as e:
            logger.error(
                "[NOUNPHRASE2] get_NP error: {}".format(e), exc_info=True)

        return np_list, local_np

    def temp_fix(self, np_list, ori_text):
        ner_list = ["imagine dragons", "a star is born", "james harden"]
        try:
            for i in ner_list:
                if i in ori_text:
                    np_list.append(i)
        except Exception as e:
            logger.error(
                "[NOUNPHRASE2] temp_fix error: {}".format(e), exc_info=True)
        return np_list

    # https://trello.com/c/1gMzvx8D/1877-launchgreeting-should-go-to-music-not-playmusic
    def filter_my(self, np_list):
        return_np = []
        for np in np_list:
            if np.startswith("my "):
                return_np.append(np[3:])
            else:
                return_np.append(np)
        return return_np

    def rm_the(self, np_list):
        try:
            return_np = np_list
            for i in np_list:
                if i.startswith("the "):
                    if i[4:] not in return_np:
                        return_np.append(i[4:])
                # check for "about" in "let's talk about xxx"
                if i.startswith("about "):
                    if i[6:] not in return_np:
                        return_np.append(i[6:])
            return return_np
        except Exception as e:
            logger.error(
                "[NOUNPHRASE2] rm_the error: {}".format(e), exc_info=True)
        return np_list


class NPKnowledge2(LocalServiceModule):
    def execute(self):
        np, local_np, descriptions = [], [], []
        try:
            xray_recorder.begin_segment('[NLP MODULES] {}'.format(self.__class__.__name__))
            if self.input_data["nounphrase2"] is not None:
                nps = self.input_data["nounphrase2"]["noun_phrase"]
                local_nps = self.input_data["nounphrase2"]["local_noun_phrase"]
            if self.input_data["googlekg"] is not None:
                descriptions = self.input_data["googlekg"]
            np = self.lists_to_one(nps)
            local_np = self.lists_to_one(local_nps)
            descriptions = self.lists_to_one(descriptions)
        except Exception as e:
            logger.error(
                "[NPKnowledge2] get result error: {}".format(e), exc_info=True)
        finally:
            xray_recorder.end_segment()

        return {"knowledge": descriptions, "noun_phrase": np, "local_noun_phrase": local_np}

    def lists_to_one(self, components):
        # noun_phrase: [["tom brady"], ["adele", "taylor swift"], []] to ["tom brady", "adele", "taylor swift"]
        # the original list is a list of all the elements. The new list contains several lists where each one
        # is a list for each segmented sentence. Need to change it back to be system compatible
        return_list = []
        try:
            for component in components:
                for com in component:
                    return_list.append(com)
            # if len(return_list) == 0:
            #     return_list.append("")
        except Exception as e:
            logger.error(
                "[NPKnowledge2] lists to one error: {}".format(e), exc_info=True)
        return return_list


class ASRCorrection(LocalServiceModule):
    np_ignore_list = [
        "'s", 'i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're",
        "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself',
        'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs',
        'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are',
        'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an',
        'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about',
        'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up',
        'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there',
        'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some',
        'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can',
        'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain',
        'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn',
        "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn',
        "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't",
        'wouldn', "wouldn't", "my name", "your name", "wow", "yeah", "yes", "ya", "cool", "okay", "more", "some more",
        "a lot", "a bit", "a little", "another one", "something", "anything", "something else", "someone", "anyone",
        "play", "mean", "a lot", "a little", "a little bit"]
    # category_list = ["SPORT", "MOVIECHAT", "GAMECHAT", "MUSICCHAT", "BOOKCHAT"]
    category_list = ["SPORT", "GAMECHAT", "MOVIECHAT", "MUSICCHAT"]
    category_pattern = (r"sport|movie (titled|called)|game (titled|called)|book (titled|called)|movie|game|book|singer "
                        r"(is|called)|singer|artist|songwriter")

    filter_pattern = r"^\bsome\b|^\banother\b|\btwice\b"
    ssml_pattern = r"\<.*\>.*\<.*\>"
    remove_symbol = r"\:|\-|\~|\(|\)|\%|\$|\#|\@|\&|\*|\+|\=|\^|\<|\>"

    regex_symbol = r"about|movie|book|sport|singer is|artist is|watch|watched|like|love|listen to|my.*is"

    asr_threashold = 0.4
    timeout = (0.3, 0.5)
    fuzzy_match_threshold = 0.75

    # with open("sports_pho.json") as f:
    #     sports_phonetics = json.load(f)

    def execute(self):
        start = time.time()
        match = {}
        if utils.get_working_environment() == "prod":
            try:
                # text = self.input_data['converttext']
                text = self.input_data["text"]
                if self.input_data["converttext"] is not None:
                    text = self.input_data["converttext"]
                """++++++++++++"""
                if self.input_data["nounphrase2"] is not None and "noun_phrase" in self.input_data["nounphrase2"]:
                    nounphrase_list = self.input_data["nounphrase2"]["noun_phrase"]
                    all_noun_phrase = [np for npl in nounphrase_list for np in npl]
                else:
                    all_noun_phrase = []
                """------------"""
                if isinstance(self.state_manager.current_state.asr, list) and \
                        len(self.state_manager.current_state.asr) > 0:
                    confi_score = 0
                    if 'confidence' in self.state_manager.current_state.asr[0]:
                        logger.info(
                            "[ASR_Correction] asr sentence confidence enable")
                        confi_score = self.state_manager.current_state.asr[0]['confidence']
                    if confi_score > 0 and confi_score < self.asr_threashold:
                        if self.state_manager.last_state is not None:
                            # request_nps = copy.deepcopy(self.input_data['npknowledge']['noun_phrase'])
                            request_nps = copy.deepcopy(
                                self.input_data['spacynp'])
                            if request_nps is None:
                                request_nps = []

                            """++++++++++++"""
                            if len(all_noun_phrase) > 0:
                                for np_np in all_noun_phrase:
                                    if np_np not in request_nps:
                                        request_nps.append(np_np)
                            """------------"""

                            """+++"""
                            # use regex to locate entity names
                            if re.search(self.regex_symbol, text.lower()):
                                potential_clean_np = text.lower()[re.search(self.regex_symbol, text.lower()).end() + 1:]
                                potential_clean_np = re.sub(self.filter_pattern, "", potential_clean_np).strip()
                                if potential_clean_np not in request_nps:
                                    request_nps.append(potential_clean_np)
                            """---"""

                            cur_category = ""
                            for np in request_nps:
                                if re.search(self.category_pattern, np):
                                    cur_category = re.search(
                                        self.category_pattern, np).group(0)
                                    break
                            # ex. np: the movie sky scraper. process_np: sky scraper
                            if cur_category != "":
                                request_nps = self.process_np(request_nps)
                            logger.info(
                                '[ASRCorrection] request noun_phrase: {}'.format(request_nps))

                            """++++"""
                            if request_nps is None or len(request_nps) == 0:
                                return match

                            """---"""
                            last_state_response = self.state_manager.last_state.get('response', "")

                            response_nps = self.get_np_spacy(last_state_response)
                            logger.info(
                                "[ASR_Correction] response noun phrases: {}".format(response_nps))
                            request_nps_phonetics = self.get_phonetics(
                                request_nps)
                            logger.info("[ASR_Correction] request noun phrases phonetics: {}".format(
                                request_nps_phonetics))
                            response_nps_phonetics = self.get_phonetics(
                                response_nps)
                            logger.info("[ASR_Correction] response noun phrases phonetics: {}".format(
                                response_nps_phonetics))
                            match = self.match_phonetics(
                                request_nps_phonetics, response_nps_phonetics, only_response=True)
                            if len(match) > 0:
                                logger.info(
                                    "[ASR_Correction] return match: {}".format(match))
                                return match
                            else:
                                if self.state_manager.user_attributes.last_module is not None and \
                                        self.state_manager.user_attributes.last_module in self.category_list or \
                                        cur_category != "":
                                    if self.state_manager.user_attributes.last_module == "SPORT" or \
                                            "sport" in cur_category:
                                        sports_phonetics = RedisHelper().get_asr("sports_phonetics")
                                        match = self.match_phonetics(
                                            request_nps_phonetics, sports_phonetics)
                                        logger.info(
                                            "[ASR_Correction] return match: {}".format(match))
                                    elif self.state_manager.user_attributes.last_module == "MOVIECHAT" or \
                                            "movie" in cur_category:
                                        movies_phonetics = RedisHelper().get_asr("movies_phonetics")
                                        match = self.match_phonetics(
                                            request_nps_phonetics, movies_phonetics)
                                        logger.info(
                                            "[ASR_Correction] return match: {}".format(match))
                                    elif self.state_manager.user_attributes.last_module == "GAMECHAT" or \
                                            "game" in cur_category:
                                        games_phonetics = RedisHelper().get_asr("games_phonetics")
                                        match = self.match_phonetics(
                                            request_nps_phonetics, games_phonetics)
                                        logger.info(
                                            "[ASR_Correction] return match: {}".format(match))
                                    elif self.state_manager.user_attributes.last_module == "MUSICCHAT" or \
                                            "singer" in cur_category or "artist" in cur_category:
                                        artist_phonetics = RedisHelper().get_asr("artist_phonetics")
                                        match = self.match_phonetics(
                                            request_nps_phonetics, artist_phonetics)
                                        logger.info(
                                            "[ASR_Correction] return match: {}".format(match))
                                    # elif self.state_manager.user_attributes.last_module == "MUSICCHAT" or \
                                    #       "music" in cur_category:
                                    #     match = self.match_phonetics(request_nps_phonetics, self.sports_phonetics)
                                    # elif self.state_manager.user_attributes.last_module == "BOOKCHAT" or \
                                    #       "book" in cur_category:
                                    #     match = self.match_phonetics(request_nps_phonetics, self.sports_phonetics)

            except Exception as e:
                logger.error(f"[ASRCorrection] error: {e}", exc_info=True)

        else:
            """
            TESTING ASRCorrection
            """
            try:
                # text = self.input_data['converttext']
                text = self.input_data["text"]
                if self.input_data["converttext"] is not None:
                    text = self.input_data["converttext"]
                """++++++++++++"""
                if self.input_data["nounphrase2"] is not None and "noun_phrase" in self.input_data["nounphrase2"]:
                    nounphrase_list = self.input_data["nounphrase2"]["noun_phrase"]
                    all_noun_phrase = [np for npl in nounphrase_list for np in npl]
                else:
                    all_noun_phrase = []
                """------------"""
                if self.state_manager.last_state is not None and self.state_manager.last_state.get('response', None) \
                        is not None:
                    # request_nps = copy.deepcopy(self.input_data['npknowledge']['noun_phrase'])
                    request_nps = copy.deepcopy(self.input_data['spacynp'])
                    if request_nps is None:
                        request_nps = []

                    """++++++++++++"""
                    if len(all_noun_phrase) > 0:
                        for np_np in all_noun_phrase:
                            if np_np not in request_nps:
                                request_nps.append(np_np)
                    """------------"""

                    """+++"""
                    # use regex to locate entity names
                    if re.search(self.regex_symbol, text.lower()):
                        potential_clean_np = text.lower()[re.search(self.regex_symbol, text.lower()).end() + 1:]
                        potential_clean_np = re.sub(self.filter_pattern, "", potential_clean_np).strip()
                        if potential_clean_np not in request_nps:
                            request_nps.append(potential_clean_np)
                    # request_nps.append(text.lower())
                    """---"""

                    cur_category = ""
                    for np in request_nps:
                        if re.search(self.category_pattern, np):
                            cur_category = re.search(
                                self.category_pattern, np).group(0)
                            break
                    # ex. np: the movie sky scraper. process_np: sky scraper
                    if cur_category != "":
                        request_nps = self.process_np(request_nps)
                    logger.info(
                        '[ASR_Correction] request noun_phrase: {}'.format(request_nps))

                    """++++"""
                    if request_nps is None or len(request_nps) == 0:
                        return match

                    last_state_response = self.state_manager.last_state.get('response', "")
                    """---"""

                    response_nps = self.get_np_spacy(last_state_response)
                    logger.info(
                        "[ASR_Correction] response noun phrases: {}".format(response_nps))
                    request_nps_phonetics = self.get_phonetics(request_nps)
                    logger.info("[ASR_Correction] request noun phrases phonetics: {}".format(
                        request_nps_phonetics))
                    response_nps_phonetics = self.get_phonetics(response_nps)
                    logger.info("[ASR_Correction] response noun phrases phonetics: {}".format(
                        response_nps_phonetics))
                    match = self.match_phonetics(
                        request_nps_phonetics, response_nps_phonetics, only_response=True)
                    if len(match) > 0:
                        logger.info(
                            "[ASR_Correction] return match: {}".format(match))
                        return match
                    else:
                        if self.state_manager.user_attributes.last_module is not None and \
                                self.state_manager.user_attributes.last_module in self.category_list or \
                                cur_category != "":
                            if self.state_manager.user_attributes.last_module == "SPORT" or "sport" in cur_category:
                                sports_phonetics = RedisHelper().get_asr("sports_phonetics")
                                match = self.match_phonetics(
                                    request_nps_phonetics, sports_phonetics)
                                logger.info(
                                    "[ASR_Correction] return match: {}".format(match))
                            elif self.state_manager.user_attributes.last_module == "MOVIECHAT" or \
                                    "movie" in cur_category:
                                movies_phonetics = RedisHelper().get_asr("movies_phonetics")
                                match = self.match_phonetics(
                                    request_nps_phonetics, movies_phonetics)
                                logger.info(
                                    "[ASR_Correction] return match: {}".format(match))
                            elif self.state_manager.user_attributes.last_module == "GAMECHAT" or \
                                    "game" in cur_category:
                                games_phonetics = RedisHelper().get_asr("games_phonetics")
                                match = self.match_phonetics(
                                    request_nps_phonetics, games_phonetics)
                                logger.info(
                                    "[ASR_Correction] return match: {}".format(match))
                            elif self.state_manager.user_attributes.last_module == "MUSICCHAT" or \
                                    "singer" in cur_category or "artist" in cur_category:
                                artist_phonetics = RedisHelper().get_asr("artist_phonetics")
                                match = self.match_phonetics(
                                    request_nps_phonetics, artist_phonetics)
                                logger.info(
                                    "[ASR_Correction] return match: {}".format(match))
                            # elif self.state_manager.user_attributes.last_module == "MUSICCHAT" or \
                            #       "music" in cur_category:
                            #     match = self.match_phonetics(request_nps_phonetics, self.sports_phonetics)
                            # elif self.state_manager.user_attributes.last_module == "BOOKCHAT" or \
                            #       "book" in cur_category:
                            #     match = self.match_phonetics(request_nps_phonetics, self.sports_phonetics)
            except Exception as e:
                logger.error(
                    "[ASRCorrection] error: {}".format(e), exc_info=True)

        end = time.time()
        self.logger.info(
            "Finished ASRCorrection, latency:{}ms".format((end - start) * 1000))

        return match

    def process_np(self, pre_list):
        try:
            process_list = []
            for np in pre_list:
                if re.search(self.category_pattern, np):
                    if len(re.sub(self.category_pattern, "!@#", np).split("!@#")[1].strip()) > 0:
                        process_list.append(
                            re.sub(self.category_pattern, "!@#", np).split("!@#")[1].strip())
                else:
                    process_list.append(np)
            return process_list
        except Exception as e:
            logger.error(
                "[ASRCorrection] process np error: {}".format(e), exc_info=True)
            return pre_list

    def get_np_spacy(self, response):
        return_nps = []
        try:
            # response = re.sub(self.ssml_pattern, "", response).strip()
            response = response.replace("\"", "")
            response = re.sub(r'\<.*?\>', "", response)
            response = re.sub(self.remove_symbol, "", response).strip()
            request_url = NP_URL + response
            # nps is a list of noun_phrase and they are lower case returned by the api server without post processing
            nps = requests.get(request_url, timeout=self.timeout).json()
            clean_nps = [np for np in nps if np not in self.np_ignore_list]
            return_nps = [re.sub(self.filter_pattern, "", np).strip()
                          for np in clean_nps]
        except Exception as e:
            logger.error(
                "[ASRCorrection] get noun phrase error: {}".format(e), exc_info=True)

        return return_nps

    def get_phonetics(self, np_list):
        phonetics_dict = {}
        try:
            for np in np_list:
                phonetics = dmetaphone(np)
                # for cases when the phonetics is too short to compare. ex. "no" vs. "titanic"
                if len(phonetics[0]) <= 1:
                    continue
                pho_2 = ""
                if phonetics[1] != '' and phonetics[0].startswith("J") and phonetics[1].startswith("A") and \
                        (np.startswith("jal") or np.startswith("jai")):
                    # example: dmetaphone("jalapeno") = ('JLPN', 'ALPN'), change it into ("HLPN", "ALPN")
                    if len(phonetics[0]) > 1:
                        pho_2 = "H" + phonetics[0][1:]
                pho_0 = phonetics[0]
                pho_1 = phonetics[1]
                pho_list = [pho_0, pho_1, pho_2]
                for pho in pho_list:
                    if pho != "":
                        if pho in phonetics_dict:
                            phonetics_dict[pho].append(np)
                        else:
                            phonetics_dict[pho] = [np]

        except Exception as e:
            logger.error(
                "[ASRCorrection] get_phonetics error: {}".format(e), exc_info=True)

        return phonetics_dict

    def match_phonetics(self, request_nps, response_nps, only_response=False):
        # for latency reason, we only care of issues like "secure holiday" vs. "obscure holiday"
        # when we are using the previous utterance where the number of response_nps is small
        match = {}
        try:
            for np_pho in request_nps.keys():
                # request_nps[np_pho] is a list. We assume that because of the small number of nps in request,
                # there is only one element (or choose the first one if there are many) in the request np
                if np_pho in response_nps:
                    match[request_nps[np_pho][0]] = response_nps[np_pho]
                elif len(np_pho) > 1 and np_pho[:-1] in response_nps:
                    # ex. request: holidays vs response: holiday
                    match[request_nps[np_pho][0]] = response_nps[np_pho[:-1]]
                # quite common that "s" is dropped in ASR
                elif np_pho + "S" in response_nps:
                    match[request_nps[np_pho][0]] = response_nps[np_pho + "S"]
                elif len(np_pho) > 1 and np_pho[1:] in response_nps:
                    match[request_nps[np_pho][0]] = response_nps[np_pho[1:]]
                elif len(np_pho) > 2 and np_pho[2:] in response_nps:
                    match[request_nps[np_pho][0]] = response_nps[np_pho[2:]]
                elif len(np_pho) > 2 and np_pho[:-2] in response_nps:
                    match[request_nps[np_pho][0]] = response_nps[np_pho[:-2]]
                elif len(np_pho) > 2 and np_pho[1:-1] in response_nps:
                    match[request_nps[np_pho][0]] = response_nps[np_pho[1:-1]]
                elif len(np_pho) > 3 and np_pho[2:-1] in response_nps:
                    match[request_nps[np_pho][0]] = response_nps[np_pho[2:-1]]
                elif len(np_pho) > 3 and np_pho[1:-2] in response_nps:
                    match[request_nps[np_pho][0]] = response_nps[np_pho[1:-2]]
            if only_response:
                for np_pho in response_nps.keys():
                    if len(np_pho) > 1 and np_pho[:-1] in request_nps:
                        # ex. request: holiday vs response: holidays
                        match[request_nps[np_pho[:-1]]
                              [0]] = response_nps[np_pho]
                    elif len(np_pho) > 1 and np_pho[1:] in request_nps:
                        match[request_nps[np_pho[1:]]
                              [0]] = response_nps[np_pho]
                    elif len(np_pho) > 2 and np_pho[2:] in request_nps:
                        match[request_nps[np_pho[2:]]
                              [0]] = response_nps[np_pho]
                    elif len(np_pho) > 2 and np_pho[:-2] in request_nps:
                        match[request_nps[np_pho[:-2]]
                              [0]] = response_nps[np_pho]
                    elif len(np_pho) > 2 and np_pho[1:-1] in request_nps:
                        match[request_nps[np_pho[1:-1]]
                              [0]] = response_nps[np_pho]
                    elif len(np_pho) > 3 and np_pho[2:-1] in request_nps:
                        match[request_nps[np_pho[2:-1]]
                              [0]] = response_nps[np_pho]
                    elif len(np_pho) > 3 and np_pho[1:-2] in request_nps:
                        match[request_nps[np_pho[1:-2]]
                              [0]] = response_nps[np_pho]

            try:
                new_match = {}
                # match ex. {'crazy rich asian': ['crazy rich asians']}
                for asr_input, asr_gold in match.items():
                    mod_asr_input = self.process_match(asr_input)
                    mod_asr_gold = self.process_match(asr_gold[0])
                    match_score = self.match_score(mod_asr_input, mod_asr_gold)
                    if match_score > self.fuzzy_match_threshold:
                        new_match[asr_input] = asr_gold
                match = new_match
            except Exception as e:
                logger.error(
                    "[ASR_Correction] get match score error: {}".format(e), exc_info=True)
        except Exception as e:
            logger.error(
                "[ASR_Correction] match_phonetics error: {}".format(e), exc_info=True)

        return match

    def match_score(self, asr_input, asr_gold):
        # simplified metaphone prodecure to calcuate a dummy match score
        match_score = 0
        try:
            asr_input = "".join(i for i in asr_input.split())
            asr_gold = "".join(i for i in asr_gold.split())
            if asr_input in asr_gold or asr_gold in asr_input:
                match_score = 1
            else:
                mod_asr_input = self.process_match(asr_input)
                mod_asr_gold = self.process_match(asr_gold)
                match_score = fuzz.partial_ratio(mod_asr_input, mod_asr_gold) / 100
                logger.info(
                    "[ASR_Correction] match score: {}".format(match_score))
        except Exception as e:
            logger.error(
                "[ASR_Correction] fuzzy match score error: {}".format(e), exc_info=True)
        return match_score

    def process_match(self, match_input):
        try:
            start_pattern = ["kn", "gn", "pn", "ae", "wr"]
            words = match_input.lower().split()
            mod_words = []

            c_to_s_list = ["i", "e", "y"]
            d_to_t_list = ["ge", "gy", "gi"]
            g_to_j_list = ["i", "e", "y"]
            for w in words:
                mod_w = w
                # Drop duplicate adjacent letters, except for C.
                # not implemented for this
                # If the word begins with 'KN', 'GN', 'PN', 'AE', 'WR', drop the first letter
                for p in start_pattern:
                    if w.startswith(p):
                        mod_w = w[1:]
                        break
                # Drop 'B' if after 'M' at the end of the word.
                if w.endswith("mb"):
                    mod_w = mod_w[:-1]

                cur_word = ""
                for c in range(len(mod_w)):
                    if c < len(mod_w) - 1:
                        # 'C' transforms to 'X' if followed by 'IA' or 'H'
                        # (unless in latter case, it is part of '-SCH-', in which case it transforms to 'K').
                        # 'C' transforms to 'S' if followed by 'I', 'E', or 'Y'. Otherwise, 'C' transforms to 'K'.
                        if mod_w[c] == "c":
                            if mod_w[c + 1] == "h" or (c < len(mod_w) - 2 and mod_w[c + 1:c + 2] == "ia"):
                                # mod_w[c] = "x"
                                cur_word += "x"
                            elif mod_w[c + 1] in c_to_s_list:
                                # mod_w[c] = "s"
                                cur_word += "s"
                            else:
                                # mod_w[c] = "k"
                                cur_word += "k"
                        # 'D' transforms to 'J' if followed by 'GE', 'GY', or 'GI'. Otherwise, 'D' transforms to 'T'.
                        elif mod_w[c] == "d":
                            if c < len(mod_w) - 2 and mod_w[c + 1:c + 2] in d_to_t_list:
                                # mod_w[c] = "t"
                                cur_word += "t"
                        # Drop 'G' if followed by 'H' and 'H' is not at the end or before a vowel.
                        # Drop 'G' if followed by 'N' or 'NED' and is at the end.
                        elif mod_w[c] == "g":
                            if (c < len(mod_w) - 2 and mod_w[c + 1] == "h") or \
                                (c == len(mod_w) - 2 and mod_w[len(mod_w) - 1] == "n") or \
                                    (c == len(mod_w) - 4 and mod_w[-3:] == "ned"):
                                # mod_w[c] = " "
                                cur_word += " "
                            # 'G' transforms to 'J' if before 'I', 'E', or 'Y', and it is not in 'GG'.
                            # Otherwise, 'G' transforms to 'K'.
                            elif mod_w[c + 1] in g_to_j_list and mod_w[c + 1] != "g":
                                # mod_w[c] = "j"
                                cur_word += "j"
                        else:
                            cur_word += mod_w[c]
                    else:
                        cur_word += mod_w[c]
                mod_w = cur_word
                # 'CK' transforms to 'K'.
                mod_w = re.sub("ck", "k", mod_w)
                # 'PH' transforms to 'F'.
                mod_w = re.sub("ph", "f", mod_w)
                # 'WH' transforms to 'W' if at the beginning. Drop 'W' if not followed by a vowel.
                mod_w = re.sub(r"^wh", "w", mod_w)
                mod_w = "".join(i for i in mod_w if len(i.strip()) > 0)

                mod_words.append(mod_w)

            logger.info(
                "[ASR_Correction] process_match: {}".format("".join(w for w in mod_words)))

            return "".join(w for w in mod_words)
        except Exception as e:
            logger.error(
                "[ASR_Correction] process match error: {}".format(e), exc_info=True)
            return match_input


class Segmentation(LocalServiceModule):
    timeout = (0.3, 0.5)

    def execute(self):
        start = time.time()
        segmented_text = {'segmented_text': [], 'segmented_text_raw': []}
        try:
            text = self.input_data["text"].lower()
            coreference = self.input_data["coreference"]
            if self.input_data["converttext"] is not None:
                text = self.input_data["converttext"].lower()
            if coreference is not None:
                if 'text' in coreference:
                    text = coreference['text'].lower()
            logger.info("[Segmentation] get text: {}".format(text))
            # only segment for sentences that are longer than 3 words
            if len(text.split()) > 0:
                segmented_text = self.segment(text)
            else:
                if len(text) > 0:
                    segmented_text = {'segmented_text': [text], 'segmented_text_raw': [text]}
            self.logger.info(
                "[Segmentation] get segmented text: {}".format(segmented_text))

        except Exception as e:
            self.logger.error("[Segmentation] error: {}".format(e), exc_info=True)

        end = time.time()
        self.logger.info(
            "Finished Segmentation, latency:{}ms".format((end - start) * 1000))

        return segmented_text

    def segment(self, text):
        if not re.search(r"\bi do do you\b", text.lower()):
            text = self.stutter_remove(text, 3)
        segmented_text = []
        segmented_text_raw = []
        kg_threshold = 360
        try:
            BREAK = "<BRK>"
            NER = "ner"

            # print(self.input_data)
            # if self.input_data["npknowledge"] is not None:
            #     npknowledge = self.input_data.get(
            #         'npknowledge', {}).get('knowledge')
            # else:
            #     npknowledge = None
            npknowledge = self.input_data["googlekg"]
            text_ner = text
            text_ner0 = text

            if npknowledge:
                check_npknowledge = [npkg for npkg in npknowledge if len(npkg) == 5]
                for idx, (ner, name, _, kg_score, _) in enumerate(check_npknowledge):
                    # if re.search(ner, text).span()[0] == 0:
                    #     # in case the first phrase is incorrectly considered as an NER
                    #     # (ex. what books have you read)
                    #     continue
                    if float(kg_score) < kg_threshold:
                        continue
                    text_ner = re.sub(r"\b{}\b".format(
                        ner), NER, text_ner, flags=re.IGNORECASE)
                    text_ner0 = re.sub(r"\b{}\b".format(
                        ner), NER + str(idx), text_ner0, flags=re.IGNORECASE)

                ner_order = [int(re.sub(r"ner", "", ner.group(0)))
                             for ner in re.finditer(r"\bner[\d]+\b", text_ner0)]
                self.logger.info("[arbit] {} {} ".format(text_ner0, ner_order))

            self.logger.info("[Segmentation] text: {}".format(text))
            request_url, model_id = self.get_url_and_model_id()
            headers = {'content-type': 'application/json'}
            data = [{"src": text_ner, "id": model_id, "timing": self._timing_info()}]
            data = json.dumps(data)
            seg = requests.post(url=request_url, data=data,
                                headers=headers, timeout=self.timeout).json()
            self.logger.info("[Segmentation] seg return: {}".format(str(seg)))
            seg_tgt = seg[0][0]["tgt"].split(BREAK)
            # # handle cornor case ex. what actress do you like the most
            # if seg_tgt[0] == "what actress":
            #     new_seg_tgt = [seg_tgt[0] + " " + seg_tgt[1]]
            #     if len(seg_tgt) > 2:
            #         new_seg_tgt.append(seg_tgt[i] for i in range(len(seg_tgt)) if i > 1)
            #     seg_tgt = new_seg_tgt
            self.logger.info("[Segmentation] segmented text: {}".format(seg_tgt))
            segmented_text = [i.strip() for i in seg_tgt if i != ""]
            segmented_text = self.pend_text(segmented_text)

            # # check timestep for short segmented sentences
            # if utils.get_working_environment() == "prod" or utils.get_working_environment() == "beta":
            #     try:
            #         a_b_test = self.state_manager.current_state.a_b_test.lower()
            #         if isinstance(self.state_manager.current_state.asr, list) and \
            #                 len(self.state_manager.current_state.asr) > 0 and \
            #                 "tokens" in self.state_manager.current_state.asr[0]:
            #             segmented_text = self.timestep_check(
            #                 segmented_text, self.state_manager.current_state.asr[0]["tokens"])
            #     except Exception as e:
            #         self.logger.error(
            #             "[Segmentation] prod timestep checking error: {}".format(e), exc_info=True)

            segmented_text_raw = segmented_text[:]

            if npknowledge:
                # replace segmented text ner tags
                ner_order_idx = 0
                for idx, segment in enumerate(segmented_text_raw):
                    seg_text = segment
                    while True:
                        if re.search(r"\bner\b", seg_text):
                            ner_text = npknowledge[ner_order[ner_order_idx]][0]
                            seg_text = re.sub(
                                r"\bner\b", ner_text, seg_text, count=1)
                            ner_order_idx += 1
                        else:
                            break
                    segmented_text_raw[idx] = seg_text

            logger.info('[Segmentation] segmented and raw: {}; {}'.format(
                segmented_text, segmented_text_raw))

        except Exception as e:
            logger.error(
                "[Segmentation] get segmentation error: {}".format(e), exc_info=True)

        # return {'segmented_text': segmented_text, 'segmented_text_raw': segmented_text_raw}
        return {'segmented_text': segmented_text, 'segmented_text_raw': segmented_text_raw}

    def _timing_info(self):
        asr = self.state_manager.current_state.asr or [{}]
        tokens = asr[0].get('tokens') or []  # first ASR hypothesis
        value = [t.get('value', '') for t in tokens]
        start = [t.get('startOffsetInMilliseconds', 0) for t in tokens]
        end = [t.get('endOffsetInMilliseconds', 0) for t in tokens]

        return {'value': value, 'start': start, 'end': end}

    def get_url_and_model_id(self):
        a_b_test = self.state_manager.current_state.a_b_test.lower()
        # if a_b_test == 'e':
        #     return self.get_new_segmentation_model_url_and_model_igitd()
        return self.get_segmentation_model_url_and_model_id()

    def get_new_segmentation_model_url_and_model_id(self):
        return SEGMENTATION_URL_V2, 110

    def get_segmentation_model_url_and_model_id(self):
        return SEGMENTATION_URL, 100

    def timestep_check(self, segmented_text, asr):
        short_sent_check_threshold = 3
        timestep_threshold = 400
        return_text = []
        index = -1
        try:
            for i in range(len(segmented_text)):
                if i > 0 and len(segmented_text[i].split()) <= short_sent_check_threshold:
                    if index < len(asr) and "startOffsetInMilliseconds" in asr[index] and \
                            "endOffsetInMilliseconds" in asr[index]:
                        end_step = asr[index]["endOffsetInMilliseconds"]
                        start_step = asr[index + 1]["startOffsetInMilliseconds"]
                        if int(start_step) - int(end_step) < timestep_threshold:
                            return_text[-1] = return_text[-1] + " " + segmented_text[i]
                            index += len(segmented_text[i].split())
                            continue
                return_text.append(segmented_text[i])
                index += len(segmented_text[i].split())
            return return_text
        except Exception as e:
            logger.error(
                "[Segmentation] get segmentation error: {}".format(e), exc_info=True)
            return segmented_text

    def pend_text(self, segmented_text):
        return_pend_text = []
        try:
            for i in range(len(segmented_text)):
                # print("^^^^^^^^^^")
                # print(segmented_text)
                # if segmented_text[i] == "ner" and i > 0:
                #     return_pend_text[-1] = return_pend_text[-1] + " ner"
                if segmented_text[i].startswith("ner") and i > 0:
                    return_pend_text[-1] = return_pend_text[-1] + " " + segmented_text[i]
                # for cases like mr. smith, m.i.t. (periods added)
                elif i > 0 and segmented_text[i - 1].endswith("."):
                    return_pend_text[-1] = return_pend_text[-1] + " " + segmented_text[i]
                # above not applicable for now (tokenzied) so do quick fix for mr.
                elif i > 0 and segmented_text[i - 1].endswith(" mr"):
                    return_pend_text[-1] = return_pend_text[-1] + " " + segmented_text[i]
                # tell me what you think about kavanaugh
                elif i > 0 and segmented_text[i - 1].endswith("about") and len(segmented_text[i].split()) <= 3:
                    return_pend_text[-1] = return_pend_text[-1] + " " + segmented_text[i]
                else:
                    return_pend_text.append(segmented_text[i])
        except Exception as e:
            logger.error(
                "[Segmentation] append 'ner' to text error: {}".format(e), exc_info=True)
        return return_pend_text

    def stutter_remove(self, text: str, max_n: int):
        def n_gram_replace(text_spl: list, n: int):
            n_gram = (tuple(final_text[idx:idx + n]) for idx,
                      word in enumerate(final_text) if idx + n <= len(final_text))
            running_text = []
            # iterating each word pair, removing duplicates if found
            for idx, gram in enumerate(n_gram):
                if 0 <= idx - n < len(running_text) and gram == running_text[idx - n]:
                    if n > 1:
                        del running_text[- n + 1:]
                else:
                    running_text.append(gram)
            # update running result
            return running_text

        def join_n_gram(text_gram: list):
            return ' '.join(gram[-1] if idx != 0 else ' '.join(gram) for idx, gram in enumerate(text_gram))

        if max_n <= 0:
            raise ValueError("n-gram cannot have n <= 0")

        text = text.split()
        if max_n > len(text):
            max_n = len(text)

        final_text = text
        # iterate n-grams
        for n in range(1, max_n + 1):
            # generator representing the n-gram
            if n >= len(final_text):
                continue

            running_text = n_gram_replace(final_text, n)
            final_text = join_n_gram(running_text).split()
        return ' '.join(final_text)


class DependencyParsing(LocalServiceModule):
    timeout = (0.3, 0.5)

    def execute(self):
        start = time.time()
        parsing_result = None
        segmented_text = self.input_data["segmentation"]["segmented_text_raw"]

        parsing_result = self.get_parsing_result(segmented_text)
        end = time.time()
        self.logger.info(
            "Finished dependency parsing, latency:{}ms".format((end - start) * 1000))
        return parsing_result

    def get_parsing_result(self, sentences: List[str]) -> List[List[int]]:
        """
        Args:
            sentences: the sentences to be parsed.
        Return:
            A list of dependency parent numbers. Each sentence will be parsed into
            a same-length list of int. Each number indicates the dependency parent
            of the word at the same position.
            A list of dependency relationship label.
            A list of POS Tagging.

        Example Input:  ["great", "i have a good day"]
        Example Output:
          {"dependency_parent": [[0],
                                 [2, 0, 5, 5, 2]],
           "dependency_label": [["amod"],
                                ["nsubj", "parataxis", "det", "amod", "obj"]],
           "pos_tagging": [["ADJ"],
                           ["PRON", "VERB", "DET", "ADJ", "NOUN"]]}⏎
        Example Explaination:
            1. "great" is only one word, so it is the root.
            2. The verb "have" is the root, so its parent is 0. "i" and "day" depend
               on "have", so the numbers are 2. "a" and "good" modify "day", so their
               roots are 5.
        For more details, take https://web.stanford.edu/~jurafsky/slp3/13.pdf as
        a reference.

        Error cases:
            1. If the API return an 'error' field, it will return a list of -1.
            2. If the API is not accessable or something wrong in client code, it
               will raise an exception.
        TODO: add POS taggings as input.
        """
        # API of dependency parsing
        # POST a packed json string in `json_data`, which contains a field named `tokens`
        # `tokens` should be a 2-dimension array where each row is a sentence.
        # the return results are packed in the field of `dependency`
        tokens_mat = [t.split() for t in sentences]
        request_url = DEPENDENCY_URL
        json_data = {"tokens": tokens_mat}

        try:
            dependency_return = requests.post(
                url=request_url, json=json_data, timeout=self.timeout).json()

            if 'error' in dependency_return:
                dependency_return = None

        except Exception as e:
            logger.error("[DependencyParsing] error: {}".format(e), exc_info=True)
            dependency_return = None

        return dependency_return

    def _get_error_pack(self, tokens_mat):
        return [[-1] * len(t) for t in tokens_mat]


class SentenceCompletion(LocalServiceModule):
    timeout = (0.3, 0.5)
    request_url = SENTENCE_COMPLETION_URL

    def remove_xml_tags(self, text: str) -> str:
        """
        Remove all XML tags in the text. Used to preprocess responses.
        Args:
            text: text input with XML tags
        Return:
            text without XML tags while content maintains
        """
        tnode = ET.fromstring("<xtext>" + text + "</xtext>")
        ret = ET.tostring(tnode, encoding='utf8', method='text').decode()
        return ret

    def execute(self):
        start = time.time()

        text = self.input_data["text"]

        # filter out empty input
        if len(text) == 0:
            return text

        last_response = self.get_last_response()
        last_response = self.post_process_last_response(last_response)

        # remove open gunrock ...
        text = text.replace("open gunrock and ", "")

        # pack query text into a OpenNMT style JSON request
        query_json = json.dumps([{'last_response': last_response, 'src': text}])

        try:
            sentence_completion_return = requests.post(
                url=self.request_url, data=query_json, timeout=self.timeout).json()

            # unpack result by access a 2-dimension list
            sentence_completed = sentence_completion_return[0][0]['tgt']
            self.logger.info(
                "[SC] Last response = {}".format(last_response))
            self.logger.info(
                "[SC] Text = {}".format(text))
            self.logger.info(
                "[SC] Text completed = {}".format(sentence_completed))

            result = sentence_completed

        except Exception as e:
            logger.error("[SentenceCompletion] error: {}".format(e), exc_info=True)
            result = text

        end = time.time()

        self.logger.info(
            "Finished SentenceCompletion, latency:{} ms".format((end - start) * 1000))
        return result

    def get_last_response(self):
        last_state = self.state_manager.last_state

        if last_state:
            last_response = last_state.get('response', None)
        else:
            last_response = None
        return last_response

    def post_process_last_response(self, last_response: str):
        if last_response:
            last_response = self.remove_xml_tags(last_response)
        else:
            last_response = "EMPTY"

        return last_response


class DialogAct(LocalServiceModule):
    timeout = (0.3, 0.5)

    def execute(self):
        start = time.time()
        last_bot_response = DialogAct.post_process_last_response(self.get_last_bot_response())
        last_user_input = self.get_last_user_input()

        segmented_text = self.input_data['segmentation'][0]
        segmented_text, segmented_text_raw = segmented_text['segmented_text'], segmented_text['segmented_text_raw']
        if segmented_text is None:
            segmented_text = []

        text = " ".join(segmented_text)
        if len(text.split()) < 5:
            comptext = self.input_data['sentence_completion_text'][0]
        else:
            comptext = text

        formatted_req = self.prep_context(segmented_text)

        self.logger.info(f"[DialogAct] get: "
                         f"comptext: {comptext}, last_bot_response: {last_bot_response} "
                         f"last_user_input: {last_user_input}, segmented_text: {segmented_text} "
                         f"formatted segmented_test: {formatted_req}")

        return_value = []

        # To reduce network time, send a list of all segmented text
        if len(segmented_text) > 0:
            # build context
            dialog_act_context = []
            for item in formatted_req:
                # Currently, it used the whole comptext as the comptext input
                # It cannot be solved now because comptext is computed before
                # segmentation.
                ctx = {'last_bot_response': last_bot_response,
                       'last_user_input': last_user_input,
                       'text': item,
                       'comptext': comptext}
                dialog_act_context.append(ctx)

            try:
                # two label prediction
                xray_recorder.begin_segment('[NLP MODULES] {}'.format(self.__class__.__name__))

                dialog_act_list = self.get_dialog_act(dialog_act_context)
                # e.g. [[["command", 0.73123941234], ["yes_no_question", 0.53142342]], [...]]

                self.logger.info(f"[DialogAct] get dialog act list: {dialog_act_list}")

                for idx, da_segment in enumerate(dialog_act_list):  # iterate dialog_acts of every segment
                    da_segment = ensure_type(da_segment, list, [])
                    dialog_act_info = {'text': segmented_text_raw[idx]}

                    for da_idx, dialog_act in enumerate(da_segment):  # iterate each dialog_act tags per segment
                        dialog_act = ensure_type(dialog_act, list, [])
                        # dialog_act_name_and_confidence example: ['command', '0.3116312']
                        logger.info(f"[da_list] {dialog_act}", exc_info=True)

                        if len(dialog_act) == 2:  # ensure it contains both name and confidence
                            name = dialog_act[0]
                            confidence = dialog_act[1]

                            name = self._convert_dialog_act_name(ensure_type(name, str, "NA"))
                            confidence = safe_cast(ensure_type(confidence, str, "0.0", warnings=None), float, 0.0)
                            key_modifier = "" if da_idx == 0 else str(da_idx + 1)

                            dialog_act_info[f'DA{key_modifier}'] = name
                            dialog_act_info[f'confidence{key_modifier}'] = confidence if name is not None else None

                    # post process format in case DA or DA2 is missing
                    dialog_act_info['DA'] = dialog_act_info.get('DA', "NA")
                    dialog_act_info['confidence'] = dialog_act_info.get('confidence', 0.0)
                    dialog_act_info['DA2'] = dialog_act_info.get('DA2', "NA")
                    dialog_act_info['confidence2'] = dialog_act_info.get('confidence2', 0.0)

                    return_value.append(dialog_act_info)

            except Exception as e:
                logger.error(f"[DialogAct] error: {e}", exc_info=True)
                return_value = []
            finally:
                xray_recorder.end_segment()

        end = time.time()
        self.logger.info(
            f"[DialogAct] Finished execution. dialog_act: {return_value}, latency:{(end - start) * 1000}ms")

        return return_value

    def get_last_bot_response(self):
        last_state = self.state_manager.last_state
        return last_state.get('response', "EMPTY") if last_state else "EMPTY"

    @staticmethod
    def post_process_last_response(last_bot_response):
        # remove speechcon
        last_bot_response = last_bot_response.replace("\"", "")
        last_bot_response = re.sub(r'\<.*?\>', "", last_bot_response)

        # get last segmented sentence from last bot response as the context
        last_bot_response = re.split(r'\?|\.|\!|\n|\.\.\.', last_bot_response)
        last_bot_response = [sentence.strip()
                             for sentence in last_bot_response if sentence.strip() != ""][-1].lower()

        return last_bot_response

    def get_last_user_input(self):
        segmented_text = self.input_data['segmentation'][1]
        if segmented_text is None:
            return "EMPTY"
        segmented_text = segmented_text['segmented_text']
        return " ".join(segmented_text)

    def prep_context(self, segmented_text):
        ret = []
        for i in range(len(segmented_text)):
            if i == 0:
                prev_req = "EMPTY"
            else:
                prev_req = segmented_text[i - 1]
            text = prev_req + " & " + segmented_text[i]
            ret.append(text)
        return ret

    def _convert_dialog_act_name(self, dialog_act_name: str):
        # Convert MIDAS to old dialog act to ensure compatibility with the system
        if dialog_act_name == 'command':
            return 'commands'  # a difference between old DialogAct and MIDAS
        elif dialog_act_name == 'abandon':
            return 'abandoned'
        else:
            return dialog_act_name

    def get_dialog_act(self, dialog_act_context) -> List[List[List[str]]]:
        start = time.time()
        try:
            xray_recorder.begin_segment('[NLP MODULES] {}'.format(self.__class__.__name__))


            response = requests.post(url=self.dialog_act_url(),
                                     json=dialog_act_context,
                                     timeout=self.timeout)
            return ensure_type(response.json(), list, [])
        except json.decoder.JSONDecodeError as e:
            self.logger.warning(f"[DialogAct] API did not return json: {e}\n{response.text}", exc_info=True)
            return []
        except requests.exceptions.RequestException as e:
            self.logger.warning(f"[DialogAct] API error: {e}", exc_info=True)
            return []
        finally:
            end = time.time()
            self.logger.info(f"[DialogAct] API call latency: {(end - start) * 1000}ms")
            xray_recorder.end_segment()

    def dialog_act_url(self):
        # a_b_test = self.state_manager.current_state.a_b_test.lower()
        # if a_b_test == 'c':
        #     url = DIALOG_ACT_V4
        # else:
        #     url = DIALOG_ACT_V3
        # self.logger.info(f"[DialogAct] url: {url}")
        return DIALOG_ACT_V3


class AllennlpSentiment(LocalServiceModule):
    timeout = (0.1, 0.5)

    def execute(self):
        start = time.time()
        sentiment_list = []

        try:
            segmented_text = self.input_data['segmentation']
            segmented_text = segmented_text['segmented_text']
            logger.info(f"[sentiment_allennlp] get segmented text: {segmented_text}")
            if segmented_text is None:
                segmented_text = []

            if len(segmented_text) > 0:
                sentiment_list = self.get_sentiment(segmented_text)

            logger.info(f"[sentiment_allennlp] sentiment list: {sentiment_list}")
        except Exception as e:
            logger.error(f"[sentiment_allennlp] error: {e}", exc_info=True)

        end = time.time()
        self.logger.info(f"Finished AllennlpSentiment, latency: {(end - start) * 1000}ms")
        return sentiment_list

    def format_response(self, sentiment):
        """
        :param sentiment: list of sentiments (in string). ex. ["positive", "neutral"]
        :return: [{'compound': '0.5', 'neg': '0.1', 'neu': '0.0', 'pos': '0.9'},
                {'compound': '0.5', 'neg': '0.05', 'neu': '0.9', 'pos': '0.05'}]
        """
        formatted_return = []
        for senti in sentiment:
            if senti == "positive":
                formatted_return.append({'compound': '0.5', 'neg': '0.1', 'neu': '0.0', 'pos': '0.9'})
            elif senti == 'neutral':
                formatted_return.append({'compound': '0.5', 'neg': '0.05', 'neu': '0.9', 'pos': '0.05'})
            else:
                formatted_return.append({'compound': '0.5', 'neg': '0.9', 'neu': '0.0', 'pos': '0.1'})
        return formatted_return

    def get_sentiment(self, text):
        return_sentiment = []
        try:
            request_url =  SENTIMENT_URL
            headers = {'content-type': 'application/json'}

            if isinstance(text, list):
                data = [{"text": t} for t in text]
            else:
                data = [{"text": text}]
            logger.info(
                "[AllennlpSentiment] get data before json dumps: {}".format(data))
            if not data:
                logger.info("[AllennlpSentiment] empty data input")
                return return_sentiment

            data = json.dumps(data)
            logger.info(
                "[AllennlpSentiment] get sentiment with data: {}, url: {}".format(data, request_url))
            return_sentiment = requests.post(
                url=request_url, data=data, headers=headers, timeout=self.timeout).json()
            return_sentiment = self.format_response(return_sentiment)

        except Exception as e:
            logger.error(
                "[AllennlpSentiment] get sentiment error: {}".format(e), exc_info=True)

        return return_sentiment


class AmazonTopicAnalyzer(ToolkitServiceModule):

    def execute(self):
        self.logger.info("[AMZDIALOGACT] start execution...")
        start = time.time()
        current_utterance = self.state_manager.current_state.text if self.state_manager.current_state.text else ""
        # raw_topic = self.state_manager.current_state.topic
        # current_utterance_topic = ""
        # if raw_topic is not None and len(raw_topic) > 0:
        #     current_utterance_topic = raw_topic[0]["topicClass"]
        # Reason to put 'nan': It's a workaround, since model is converting input to 'nan',
        # and [None] doesn't work with conversation evaluator models.
        past_utterances = [turn.get('text', 'nan')
                           for turn in self.state_manager.session_history]

        past_responses = [turn.get('response', 'nan')
                          for turn in self.state_manager.session_history]

        past_utterances = past_utterances[0: 2] if len(past_utterances) > 2 else past_utterances
        past_responses = past_responses[0: 2] if len(past_responses) > 2 else past_responses

        # get the past utterances in the ascending order of creation time
        past_utterances.reverse()
        # get the past responses in the ascending order of creation time
        past_responses.reverse()

        self.logger.debug(
            "[AMAZONTOPIC] past utterances, result: {}".format(past_utterances))

        conversation = {
            "currentUtterance": current_utterance,
            "pastUtterances": past_utterances,
            "pastResponses": past_responses
        }

        if current_utterance != "":
            try:
                responses = self.toolkit_service_client.batch_get_dialog_act_intents(
                    conversations=[conversation], timeout_in_millis=300)
                topics = responses.get('dialogActIntents', [])
            except Exception as e:
                self.logger.info("Amazon Topic detection timeout")
                topics = []
        else:
            topics = []
        end = time.time()
        self.logger.info(
            "Finished Amazon Topic detection, result: {}, latency:{} ms".format(topics, (end - start) * 1000))

        amazon_response = topics[0] if len(topics) > 0 else {}
        amazon_topic = amazon_response['topic'] if 'topic' in amazon_response else None
        return amazon_topic


class KeywordData(LocalServiceModule):
    RETURN_SIZE = 2
    KEYWORD_CONF_THRESHOLD = 0.8

    def execute(self):
        topic = self.input_data['topic']
        logger.info('[KEYWORDDATA] start execute with topic: {}'.format(topic))
        if topic is None or topic == '' or len(topic) <= 0:
            return None
        try:
            topic_keyword_list = topic[0]['topicKeywords']
            if topic_keyword_list is None or len(topic_keyword_list) <= 0\
                    or topic_keyword_list[0]['confidence']\
                    < self.KEYWORD_CONF_THRESHOLD:

                return None
            keyword = topic_keyword_list[0]['keyword']
            req_data = {
                'query': keyword,
                'numResults': self.RETURN_SIZE
            }
            logger.info('[KEYWORDDATA] request data: {}'.format(req_data))
            resp = requests.post(
                'http://cobot-LoadB-1RHFAJRZOTEI8-1490558674.us-east-1.elb.amazonaws.com/getData',
                data=req_data,
                timeout=(0.3, 0.5))
            if resp.status_code < 500:
                logger.info('[KEYWORDDATA] response successful')
                return resp.json()
            logger.warning('[KEYWORDDATA] response error {}'.format(resp))
            return None
        except Exception as e:
            logger.error('[KEYWORDDATA] Exception in \
                           getting keyword {}'.format(e), exc_info=True)
            return None


class SelfDisclosureClassifier(LocalServiceModule):
    def execute(self):

        last_response = self.get_last_response()
        if not last_response:
            return "factual"

        text = self.input_data['text']
        if not text:
            return "factual"
        try:
            result = requests.post('http://language.cs.ucdavis.edu:2234',
                                   json={
                                       'last_response': last_response,
                                       "current_user_response": text
                                   },
                                   timeout=(0.3, 3500))
            result = result.content.decode("utf-8")
            logging.info(f"Self-Disclosure-level: {result}")
            return result

        except Exception as e:
            logging.error(f"Fail to retrieve self disclosure level. {e}")
            return "factual"

    def get_last_response(self):
        last_state = self.state_manager.last_state

        if last_state:
            last_response = last_state.get('response', None)
        else:
            last_response = None
        return last_response

class ConvertText(LocalServiceModule):
    def execute(self):
        ret = []
        text = self.input_data['text']
        try:
            for word in text.split():
                word = num2words(int(word)) if word.isdigit() else word
                ret.append(word)
            ret_text = " ".join(ret)
        except Exception as e:
            logger.error('[ConvertText] get original text wrong'.format(e), exc_info=True)
            ret_text = text
        return ret_text


def _cumsum(numbers):
    ret = []
    sum_ = 0
    for i in numbers:
        sum_ += i
        ret.append(sum_)
    return ret


def _map_ner2segment(segments, generic_ners):
    if segments is None:
        return []
    ner = [[]] * len(segments)
    if generic_ners is None:
        return ner
    bounds = _cumsum(len(s.split()) for s in segments)
    idx = 0
    for ent in generic_ners:
        start = ent['index']
        end = start + len(ent['entity_in_utterance'].split())
        in_current_segment = start < bounds[idx]
        in_next_segment = (idx + 1 < len(segments)) and end < bounds[idx + 1]
        if in_current_segment and in_next_segment:
            ner[idx].append(ent)
            ner[idx+1].append(ent)
            idx += 1
        elif in_current_segment:
            ner[idx].append(ent)
        else:
            idx += 1
    return ner


class ReturnNLP(LocalServiceModule):
    # ['segmentation', 'intent_classify2', 'sentiment2', 'googlekg', 'dialog_act', 'nounphrase2']
    def execute(self):
        returnnlp = []
        # segmented_text = self.input_data['segmentation']['segmented_text_raw']
        try:
            segmented_text = self.input_data['segmentation']['segmented_text_raw']
            intents = self.input_data['intent_classify2']
            # sentiments = self.input_data['sentiment2']
            ner = _map_ner2segment(segmented_text, self.input_data['genericner']['module_selection'])
            sentiments = self.input_data['sentiment_allennlp']
            googlekg = self.input_data['googlekg_v2']
            amazonkg = self.input_data['amazonkg']
            dialog_act = self.input_data['dialog_act']
            noun_phrase = self.input_data['nounphrase2']
            keyphrase = self.input_data['keyphrase']
            topic = self.input_data['topic2']
            amazon_topic = self.input_data['amazon_topic']
            concept = copy.deepcopy(self.input_data['concept'])
            converttext = self.input_data['text']
            coreference = self.input_data['coreference']
            dependency_parsing = self.input_data['dependency_parsing']
            if dependency_parsing is not None:
                dependency_head = dependency_parsing['dependency_parent']
                dependency_label = dependency_parsing['dependency_label'] if \
                    'dependency_label' in dependency_parsing else None
                pos_tagging = dependency_parsing['pos_tagging'] if 'pos_tagging' in dependency_parsing else None
            else:
                dependency_head = None
                dependency_label = None
                pos_tagging = None

            if self.input_data['converttext'] is not None:
                converttext = self.input_data['converttext']
            if segmented_text is not None:
                for i in range(len(segmented_text)):
                    seg_text = segmented_text[i] if (
                        segmented_text is not None and len(segmented_text) > i) else converttext
                    if coreference is not None:
                        if 'segmented_text' in coreference:
                            seg_text = coreference['segmented_text'][i] if len(
                                coreference['segmented_text']) > i else converttext
                    seg_dialog_act = dialog_act[i]["DA"] if (
                        dialog_act is not None and len(dialog_act) > i) else DialogActEnum.NA.value
                    seg_confidence = str(dialog_act[i]["confidence"]) if (
                        dialog_act is not None and len(dialog_act) > i) else 0
                    seg_dialog_act2 = dialog_act[i]["DA2"] if (
                        dialog_act is not None and len(dialog_act) > i) else DialogActEnum.NA.value
                    seg_confidence2 = str(dialog_act[i]["confidence2"]) if (
                        dialog_act is not None and len(dialog_act) > i) else 0
                    seg_intent = intents[i] if (
                        intents is not None and len(intents) > i) else None
                    seg_sentiment = sentiments[i] if (
                        sentiments is not None and len(sentiments) > i) else None
                    seg_googlekg = googlekg[i] if (
                        googlekg is not None and len(googlekg) > i) else None
                    seg_amazonkg = amazonkg[i] if (
                        amazonkg is not None and len(amazonkg) > i) else None
                    seg_np = noun_phrase["noun_phrase"][i] if (
                        noun_phrase is not None and "noun_phrase" in noun_phrase and
                        len(noun_phrase["noun_phrase"]) > i) else None
                    seg_keyphrase = keyphrase[i] if keyphrase else None
                    seg_topic = topic[i] if (
                        topic is not None and len(topic) > i) else None
                    seg_concept = concept[i] if (
                        concept is not None and len(concept) > i) else None
                    seg_dependency_head = dependency_head[i] if (
                        dependency_head is not None and len(dependency_head) > i) else None
                    seg_dependency_label = dependency_label[i] if (
                        dependency_label is not None and len(dependency_label) > i) else None
                    seg_pos_tagging = pos_tagging[i] if (
                        pos_tagging is not None and len(pos_tagging) > i) else None
                    # replace seg_text to the correct coreference
                    self.logger.info("[RETURNLP POSTAGGING] {}".format(pos_tagging))
                    returnnlp.append({"text": seg_text.lower(), "dialog_act": [seg_dialog_act, seg_confidence],
                                      "dialog_act2": [seg_dialog_act2, seg_confidence2],
                                      "intent": seg_intent, "sentiment": seg_sentiment, "googlekg": seg_googlekg, "amazonkg": seg_amazonkg,
                                      "noun_phrase": seg_np, "topic": seg_topic, "concept": seg_concept,
                                      "generic_ner": ner[i],
                                      "keyphrase": seg_keyphrase,
                                      "amazon_topic": amazon_topic,  # Same Amazon topic for each segmentation
                                      "dependency_parsing": {
                                          'head': seg_dependency_head,
                                          'label': seg_dependency_label},
                                      "pos_tagging": seg_pos_tagging
                                      })

        except Exception as e:
            self.logger.error("[ReturnNLP] error: {}".format(e), exc_info=True)

        self.logger.info("[ReturnNLP] return nlp: {}".format(returnnlp))
        return returnnlp


class NounData(LocalServiceModule):
    RETURN_SIZE = 2

    def _get_data(self, noun, ret):
        req_data = {
            'query': noun,
            'numResults': self.RETURN_SIZE
        }

        try:
            resp = requests.post(
                'http://cobot-LoadB-1RHFAJRZOTEI8-1490558674.us-east-1.elb.amazonaws.com/getData',
                data=req_data,
                timeout=(0.3, 0.5))
            if resp.status_code < 300:
                logger.info('[NOUNDATA] response successful')
                ret.append({
                    'noun': noun,
                    'data': resp.json()
                })
        except Exception as e:
            logger.error('[KEYWORDDATA] Exception in \
                           getting request data {}'.format(e), exc_info=True)

    def execute(self):
        nouns = self.input_data['npknowledge']['noun_phrase']
        logger.info('[NOUNDATA] start execute with topic: {}'.format(nouns))
        if nouns is None or nouns == '' or len(nouns) <= 0:
            return None

        ret = []
        logger.info('[KEYWORDDATA] request data: {}'.format(nouns))
        for noun in nouns:
            self._get_data(noun, ret)
        logger.info('[KEYWORDDATA] request data: {}'.format(ret))
        return ret


class Concept(LocalServiceModule):
    @latency_trace(list)
    def execute(self):
        tmp = self.input_data.get('keyphrase', None)
        phrases = tmp if tmp else []
        concept_cache = self.input_data.get('conceptraw', {})

        logger.info('[CONCEPT] request data: {}'.format(phrases))
        concept_futures = []
        for seg_phrases in phrases:
            concept_futures.append([concept_cache.get(noun, None) for noun in seg_phrases])

        concepts = []
        for futures in concept_futures:
            concepts.append([c for c in futures if c])

        logger.info('[CONCEPT] result: {}'.format(concepts))

        return concepts


class ConceptRaw(LocalServiceModule):
    IGNORE_CONCEPT_LIST = ['object', 'breed']

    def _get_data(self, noun):
        try:
            resp = requests.get('https://concept.research.microsoft.com/api/Concept/ScoreByProb',
                                params={'instance': noun, 'topK': 3}, timeout=(0.3, 0.5))
            if resp.status_code < 300:
                logger.info('[CONCEPTRAW] 1step response successful')
                res = resp.json()
                res = {k: v for k, v in filter(
                    lambda t: t[0] not in self.IGNORE_CONCEPT_LIST, res.items())}
                for k, v in res.items():
                    res[k] = str(round(res[k], 4))
                return {'noun': noun, 'data': res}
        except Exception as e:
            logger.error(f'[CONCEPTRAW] Exception in getting data {e}', exc_info=True)
        return None

    @latency_trace(dict)
    def execute(self):
        tmp = self.input_data.get('keyphraseraw', None)
        phrases = set(tmp) if tmp else set()

        logger.info('[CONCEPTRAW] request data: {}'.format(phrases))
        with ThreadPoolExecutor() as executor:
            concept_futures = {phrase: executor.submit(self._get_data, phrase) for phrase in phrases}

        concepts = {phrase: f.result() for phrase, f in concept_futures.items()}
        concepts = {phrase: result for phrase, result in concepts.items() if result}

        logger.info('[CONCEPTRAW] result: {}'.format(concepts))

        return concepts


class ExtractCentralElement(LocalServiceModule):
    OTHER_KEY = "others"

    def execute(self):
        start = time.time()
        returnnlp = self.input_data["returnnlp"]
        nlu = read_returnnlp(returnnlp)
        user_attributes = self.state_manager.user_attributes
        central_elem = self.extract_central_element_and_add_to_features(nlu, user_attributes)
        end = time.time()
        self.logger.info(
            "Finished extract central element, latency:{}ms".format((end - start) * 1000))
        return central_elem

    def extract_central_element_and_add_to_features(self, nlu, user_attributes):
        central_elem = self.extract_central_intent(nlu)

        # add backstory confidence in central elem
        backstory = get_backstory_response(
            central_elem['text'], self.state_manager.current_state.a_b_test)

        if backstory['text'] is None:
            backstory['text'] = get_fallback_backstory_response(user_attributes)

        central_elem['backstory'] = backstory
        logger.debug("[Central Element] central element: {}".format(central_elem))
        return central_elem

    def extract_central_intent(self, nlu):
        # first level sentences
        first = []
        for index, elem in enumerate(nlu):
            if elem['senti'] != 'neg' and (
                    elem['DA'] in ['commands', 'yes_no_question', 'open_question', 'open_question_opinion',
                                   'open_question_factual'] or
                    elem['regex']['sys']) and float(elem['DA_score']) > CENTRAL_ELE_DA_THRESHOLD and elem['module']:
                first.append(elem)

        # second level
        second = []
        for index, elem in enumerate(nlu):
            if elem['DA'] in ['commands', 'yes_no_question', 'open_question', 'open_question_opinion',
                              'open_question_factual', 'closing'] and \
                    float(elem['DA_score']) > CENTRAL_ELE_DA_THRESHOLD and index not in first:
                second.append(elem)

        # third level
        third = []
        for index, elem in enumerate(nlu):
            if elem['module'] and elem['senti'] != 'neg' and index not in first + second:
                third.append(elem)

        # fourth level
        fourth = []
        for index, elem in enumerate(nlu):
            if elem['DA'] not in ['abandoned', 'hold', 'comment'] and index not in first + second + third:
                fourth.append(elem)

        if first:
            return first[-1]
        if second:
            return second[-1]
        if third:
            return third[-1]
        if fourth:
            return fourth[-1]
        return nlu[-1]


class DummyTopicAnalyzer(ToolkitServiceModule):
    def execute(self):
        DUMMY_OUTPUT = [{
            "topicClass": "",
            "confidence": 0.0,
            "text": "",
            "topicKeywords": [
                {
                    "confidence": 0,
                    "keyword": ""
                }
            ]
        }]

        if "segmentation" in self.input_data:
            segments = self.input_data.get('segmentation', {}).get('segmented_text_raw', [''])
            return DUMMY_OUTPUT * len(segments)
        else:
            return DUMMY_OUTPUT


class NLPModule:

    NLP_Intent_Module = {
        'name': 'intent'
    }

    NLP_NER_Module = {
        'name': 'ner'
    }

    NLP_Sentiment_Module = {
        'name': 'sentiment'
    }

    NLP_Sentiment_Module2 = {
        'name': 'sentiment2',
        'class': Sentiment,
        'url': ServiceURLLoader.get_url_for_module("nlp") + "/sentiment?format=full",
        'context_manager_keys': ['text', 'segmentation']
    }

    NEW_NLP_Sentiment_Module = {
        'name': 'sentiment_allennlp',
        'class': AllennlpSentiment,
        'url': 'local',
        'context_manager_keys': ['text', 'segmentation']
    }

    # NPKNOWLEDGE = {
    #     'name': "npknowledge",
    #     'class': NPKnowledge,
    #     'url': 'local',
    #     'context_manager_keys': ['text'],
    # }

    PROFANITY_CHECK = {
        'name': "profanity_check",
        'class': ProfanityCheck,
        'url': 'local',
        'context_manager_keys': ['text']
    }

    SENTENCE_COMPLETION = {
        'name': "sentence_completion_text",
        'class': SentenceCompletion,
        'url': 'local',
        'context_manager_keys': ['text']
    }

    CONVERTTEXT = {
        'name': "converttext",
        'class': ConvertText,
        'url': 'local',
        'context_manager_keys': ['text']
    }

    NPKNOWLEDGE = {
        'name': "npknowledge",
        'class': NPKnowledge2,
        'url': 'local',
        'context_manager_keys': ['nounphrase2', 'googlekg']
    }

    DEPENDENCY_PARSING = {
        'name': "dependency_parsing",
        'class': DependencyParsing,
        'url': 'local',
        'context_manager_keys': ['text', 'segmentation']
    }

    INTENTCLASSIFY = {
        'name': "intent_classify",
        'class': IntentClassify,
        'url': 'local',
        'context_manager_keys': ['text', 'segmentation', 'coreference'],
    }

    INTENTCLASSIFY2 = {
        'name': "intent_classify2",
        'class': IntentClassify2,
        'url': 'local',
        'context_manager_keys': ['text', 'segmentation', 'converttext', 'coreference'],
    }

    # Define the Module in Lambda
    COREFERENCE = {
        'name': "coreference",
        'class': Coreference,
        'url': 'local',
        'context_manager_keys': ['text', 'segmentation', 'npknowledge', 'converttext', 'googlekg'],
        'history_turns': 1
    }

    KEYWORD_DATA = {
        'name': "keyword_data",
        'class': KeywordData,
        'url': 'local',
        'context_manager_keys': ['topic']
    }

    ASRCORRECTION = {
        'name': "asrcorrection",
        'class': ASRCorrection,
        'url': 'local',
        'context_manager_keys': ['text', 'spacynp', 'converttext', 'nounphrase2']
        # 'history_turns': 1
    }

    CONCEPTRAW = {
        'name': "conceptraw",
        'class': ConceptRaw,
        'url': 'local',
        'context_manager_keys': ['keyphraseraw']
    }

    CONCEPT = {
        'name': "concept",
        'class': Concept,
        'url': 'local',
        'context_manager_keys': ['conceptraw', 'keyphrase']
    }

    NOUN_DATA = {
        'name': "noun_data",
        'class': NounData,
        'url': 'local',
        'context_manager_keys': ['npknowledge']
    }

    TOPIC = {
        'name': "topic",
        'class': DummyTopicAnalyzer,
        'url': 'local',
        'context_manager_keys': ['text'],
        'timeout_in_millis': 300
    }

    TOPIC2 = {
        'name': "topic2",
        'class': DummyTopicAnalyzer,
        'url': 'local',
        'context_manager_keys': ['text', 'segmentation'],
        'timeout_in_millis': 300
    }

    AMAZONTOPIC = {
        'name': "amazon_topic",
        'class': AmazonTopicAnalyzer,
        'url': 'local',
        'context_manager_keys': ['text'],
        'timeout_in_millis': 300
    }

    SELF_DISCLOSURE_LEVEL = {
        'name': "self_disclosure_level",
        'class': SelfDisclosureClassifier,
        'url': 'local',
        'context_manager_keys': ['text'],
        'timeout_in_millis': 3000
    }

    DIALOG_ACT = {
        'name': "dialog_act",
        'class': DialogAct,
        'url': 'local',
        'context_manager_keys': ['text', 'segmentation', 'sentence_completion_text'],
        'history_turns': 1
    }

    SEGMENTATION = {
        'name': "segmentation",
        'class': Segmentation,
        'url': 'local',
        'context_manager_keys': ['googlekg', 'text', 'npknowledge', 'converttext', 'coreference']
    }

    GOOGLEKGRAW = {
        'name': "googlekgraw",
        'class': GoogleKGRaw,
        'url': 'local',
        'context_manager_keys': ['spacynp', 'converttext', 'keyphraseraw']
    }

    GOOGLEKG_V2 = {
        'name': "googlekg_v2",
        'class': GoogleKG_v2,
        'url': 'local',
        'context_manager_keys': ['googlekgraw', 'keyphrase']
    }

    GOOGLEKG = {
        'name': "googlekg",
        'class': GoogleKG,
        'url': 'local',
        'context_manager_keys': ['googlekgraw', 'keyphrase', 'spacynp']
    }

    AMAZONKGRAW = {
        'name': "amazonkgraw",
        'class': AmazonKGRaw,
        'timeout_in_millis': 350,
        'url': 'local',
        'context_manager_keys': ['keyphraseraw']
    }

    AMAZONKG = {
        'name': "amazonkg",
        'class': AmazonKG,
        'timeout_in_millis': 350,
        'url': 'local',
        'context_manager_keys': ['amazonkgraw', 'keyphrase']
    }

    SPACYNP = {
        'name': "spacynp",
        'class': SpacyNP,
        'url': 'local',
        'context_manager_keys': ['text', 'converttext']
    }

    GENERICNER = {
        'name': "genericner",
        'class': GenericNer,
        'url': 'local',
        'context_manager_keys': ['text', 'converttext']
    }

    KEYPHRASERAW = {
        'name': "keyphraseraw",
        'class': KeyPhraseRaw,
        'url': 'local',
        'context_manager_keys': ['text', 'converttext']
    }

    KEYPHRASE = {
        'name': "keyphrase",
        'class': KeyPhrase,
        'url': 'local',
        'context_manager_keys': ['keyphraseraw', 'segmentation']
    }

    NOUNPHRASE2 = {
        'name': "nounphrase2",
        'class': NounPhrase2,
        'url': 'local',
        'context_manager_keys': ['segmentation', 'spacynp']
    }

    RETURNNLP = {
        'name': "returnnlp",
        'class': ReturnNLP,
        'url': 'local',
        'context_manager_keys': ['text', 'converttext', 'segmentation', 'intent_classify2', 'sentiment2', 'googlekg_v2', 'amazonkg',
                                 'dialog_act', 'dialog_act_neo', 'nounphrase2', 'keyphrase', 'topic2', 'amazon_topic', 'concept',
                                 'genericner',
                                 'coreference', 'dependency_parsing', 'sentiment_allennlp']
    }

    CENTRAL_ELEMENT = {
        'name': 'central_elem',
        'class': ExtractCentralElement,
        'url': 'local',
        'context_manager_keys': ['returnnlp']
    }

    # SENTIMENTALEXA = {
    #     'name': "alexa_sentiment",
    #     'class': AlexaPrizeSentiment,
    #     'url': 'local',
    #     'context_manager_keys': ['text']
    # }

    def get_nlp_module_list(self):
        return [
            [self.CONVERTTEXT],
            [self.SPACYNP, self.KEYPHRASERAW],
            [
                self.PROFANITY_CHECK,  # text
                self.GOOGLEKGRAW,  # spacynp, keyphraseraw
                # self.AMAZONKGRAW,  # keyphraseraw
                # self.CONCEPTRAW,  # keyphraseraw
                self.SENTENCE_COMPLETION,
                # self.AMAZONTOPIC,  # text
                # self.SELF_DISCLOSURE_LEVEL
            ],
            [self.GOOGLEKG],  # googlekgraw
            [self.SEGMENTATION],
            # [self.NOUNPHRASE2, self.KEYPHRASE],
            [
                self.DIALOG_ACT,  # 'text', 'segmentation'
                # self.TOPIC,  # text
                # self.TOPIC2,  # text, segmentation
                self.GENERICNER,
                self.NLP_Sentiment_Module,  # 'text'
                self.NLP_Sentiment_Module2,  # 'text', 'segmentation'
                self.NEW_NLP_Sentiment_Module,  # 'text', 'segmentation'
                # self.DEPENDENCY_PARSING,  # 'text', 'segmentation'
                self.ASRCORRECTION,  # 'text', 'spacynp', 'converttext', 'nounphrase2'
                self.NLP_Intent_Module,  # text
                self.NLP_NER_Module,  # 'text'
                # self.GOOGLEKG_V2,  # googlekgraw, keyphrase
                # self.GOOGLEKG,  # googlekgraw, keyphrase
                # self.AMAZONKG,  # amazonkgraw, keyphrase
                # self.CONCEPT,  # conceptraw, keyphrase
            ],
            [self.COREFERENCE],
            [
                self.NPKNOWLEDGE,
                self.INTENTCLASSIFY,  # 'text', 'segmentation', 'coreference'
                self.INTENTCLASSIFY2,  # 'text', 'segmentation', 'converttext', 'coreference'
            ],
            [self.RETURNNLP],
            [self.CENTRAL_ELEMENT]
        ]
