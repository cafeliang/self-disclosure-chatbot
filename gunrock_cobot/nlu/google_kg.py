import re
import math
import time
import json
import logging
import requests
import random
from concurrent.futures import ThreadPoolExecutor

from cobot_core.service_module import LocalServiceModule
from util_redis import RedisHelper
from selecting_strategy.topic_mapping import google_kg_description_topic_map
from selecting_strategy.topic_mapping import google_kg_entity_type_topic_map
from selecting_strategy.topic_mapping import google_kg_entity_name_topic_map
from aws_xray_sdk.core import xray_recorder

from nlu.server_urls import GOOGLE_KG_MATCH_URL
from nlu.server_urls import GOOGLE_KG_MATCH_URL_V2

logger = logging.getLogger(__name__)


_EMPTY = "__EMPTY__"


def _old_format(kg_item):
    return [kg_item['name'], kg_item['entity_name'], kg_item['description'], kg_item['result_score'], _EMPTY]


def _4item_format(kg_item):
    return [kg_item['name'], kg_item['description'], kg_item['result_score'], kg_item['topic_module']]


def _old_format_descrp(kg_item):
    return [kg_item['name'], kg_item['entity_name'], kg_item['description'], kg_item['detailed_description'], _EMPTY]


def _first_sentence(text):
    return text.split('.')[0]


def make_kg_item(phrase, search_result=None):
    ret = {'name': phrase}
    if not search_result:
        return ret

    ret['result_score'] = str(search_result.get('resultScore', 0))
    result = search_result.get('result', {})
    ret['entity_name'] = result.get('name', '')
    ret['description'] = result.get('description', '')
    ret['detailed_description'] = _first_sentence(result.get('detailedDescription', {}).get('articleBody', ''))
    ret['entity_type'] = result.get('@type', [])
    return ret


def _is_valid(kg_item):
    return isinstance(kg_item, dict) and 'result_score' in kg_item


def _target_module_from_utterance(last_response) -> str:
    target_module = ""
    if "music" in last_response or "song" in last_response or "singer" in last_response:
        target_module = "music"
    elif "movie" in last_response or "actor" in last_response or "actress" in last_response:
        target_module = "movie"
    elif "game" in last_response:
        target_module = "game"
    elif "sports" in last_response or "sport" in last_response:
        target_module = "sport"
    elif "book" in last_response or "author" in last_response:
        target_module = "book"
    elif "animal" in last_response or "animals" in last_response:
        target_module = "animal"
    elif "food" in last_response:
        target_module = "food"
    elif "travel" in last_response:
        target_module = "travel"
    elif "news" in last_response:
        target_module = "news"
    return target_module


def _assign_module(kg_item):
    for type_, mod in google_kg_entity_type_topic_map.items():
        if type_ in kg_item['entity_type']:
            return mod

    if kg_item['entity_name'] in google_kg_entity_name_topic_map:
        return google_kg_entity_name_topic_map[kg_item['entity_name']]

    description = kg_item['description'].lower()
    if description in google_kg_description_topic_map:
        return google_kg_description_topic_map[description]

    for d in description.split():
        if d in google_kg_description_topic_map:
            return google_kg_description_topic_map[d]

    return _EMPTY


def get_target_module(user_attributes, last_state, user_response) -> str:
    if user_attributes.propose_topic:
        return user_attributes.propose_topic.lower()

    # text in response
    bot_response = last_state.get('response', "") if last_state else ""
    response_module = _target_module_from_utterance(bot_response)
    if response_module:
        return response_module

    # text in request
    request_module = _target_module_from_utterance(user_response)
    if request_module:
        return request_module

    if user_attributes.last_module:
        return user_attributes.last_module.lower()

    return ""


def latency_trace(default_constructor):
    def wrap(exec_func):
        def wrapped_fn(self):
            start = time.time()
            cls_name = type(self).__name__
            output = default_constructor()

            try:
                xray_recorder.begin_segment(f'[NLP MODULES] {cls_name}')

                output = exec_func(self)

            except Exception as e:
                logger.error(f'[{cls_name}] get error: {e}', exc_info=True)
            finally:
                latency = (time.time() - start) * 1000
                xray_recorder.end_segment()
                self.logger.info(f'Finished {cls_name}, latency:{latency}ms')

            return output

        return wrapped_fn
    return wrap


def cache_query(redis_table_prefix, duration):
    def wrap(query_fn):
        def wrapped_fn(cls_obj, key):
            output = RedisHelper().get(redis_table_prefix, key)
            if output is not None:
                return output
            output = query_fn(cls_obj, key)
            if output:
                RedisHelper().set(redis_table_prefix, key, output, expire_in_sec=duration)
            else:
                RedisHelper().set(redis_table_prefix, key, output, expire_in_sec=24*60*60 * 7)
            return output

        return wrapped_fn
    return wrap


class GoogleKG(LocalServiceModule):
    @latency_trace(list)
    def execute(self):
        kps_in_segments = self.input_data['keyphrase']
        googlekg_cache = self.input_data.get('googlekgraw', {})
        output = []

        if kps_in_segments is None:  # first pass
            nps = self.input_data.get('spacynp', [])
            nps = nps if nps else []
            kg_items = [googlekg_cache[p][0] for p in nps if p in googlekg_cache]
            output = [_old_format(i) for i in kg_items]
        else:
            kps_in_segments = kps_in_segments if kps_in_segments else []
            for phrases in kps_in_segments:
                kg_items = [googlekg_cache[p][0] for p in phrases if p in googlekg_cache]
                output.append([_4item_format(i) for i in kg_items])

        return output


class GoogleKG_v2(LocalServiceModule):
    @latency_trace(list)
    def execute(self):
        output = []
        kps_in_segments = self.input_data['keyphrase']
        kps_in_segments = kps_in_segments if kps_in_segments else []
        googlekg_cache = self.input_data.get('googlekgraw', {})
        for phrases in kps_in_segments:
            output.append([googlekg_cache[p] for p in phrases if p in googlekg_cache])

        return output


class GoogleKGRaw(LocalServiceModule):
    @latency_trace(dict)
    def execute(self):
        nps = self.input_data.get('spacynp', [])
        nps = nps if nps else []

        kps = self.input_data.get('keyphraseraw', [])
        kps = kps if kps else []

        descriptions = self.get_description(nps + kps)
        return {d[0]['name']: d for d in descriptions}

    def get_description(self, phrases):
        if not phrases:
            return []
        cls_name = type(self).__name__
        descriptions = [[] for _ in phrases]

        try:
            descriptions = self.fetch_multithreaded(phrases)
            assert len(descriptions) == len(phrases)
            if self.state_manager.current_state.a_b_test.lower() == "d":
                descriptions = self.filter_by_similarity_v2(descriptions, 0.45)

            for kg_items in descriptions:
                for kg_item in kg_items:
                    if _is_valid(kg_item):
                        kg_item['topic_module'] = _assign_module(kg_item)

            descriptions = self._sort_kg_items(descriptions)  # this step removes items that don't match anything in googleKG

        except Exception as e:
            logger.error(f'[{cls_name}] get_description: {e}', exc_info=True)

        return descriptions

    def fetch_multithreaded(self, phrases: list):
        a_b_test = self.state_manager.current_state.a_b_test.lower()
        fetch_fn = self.fetch_condition_d if a_b_test == 'd' else self.fetch_condition_c
        result = [[] for _ in phrases]
        with ThreadPoolExecutor() as executor:
            futures = [executor.submit(fetch_fn, ph) for ph in phrases]
        results = [f.result() for f in futures]
        return results

    @cache_query(RedisHelper.PREFIX_NPKNOWLEDGE_NEW_STRUCT_SIMILARITY_V1, 24*60*60 * 60)
    def fetch_condition_c(self, noun_phrase):
        return self.fetch_google_api(noun_phrase)

    @cache_query(RedisHelper.PREFIX_NPKNOWLEDGE_NEW_STRUCT_SIMILARITY_V2, 24*60*60 * 60)
    def fetch_condition_d(self, noun_phrase):
        return self.fetch_google_api(noun_phrase)

    def fetch_google_api(self, noun_phrase):
        key1 = "AIzaSyA6rMqYFRo339KPfpj_oJAQ6njOxiju0Hw"
        key2 = "AIzaSyADclYXhTdFWrRlI4aB9BbzcXESHHfbRKw"
        key3 = "AIzaSyCh9JDnPpPs6dDgY7hqCX93-FIEBbfFd48"
        key4 = "AIzaSyDAQFZRMyB0N7ttspg-U_BP1SYihLGwHU8"
        keys = [key1, key2, key3, key4]
        timeout = (0.3, 0.5)
        topk = 5  # maybe 10

        query = noun_phrase.replace(" ", "+")
        key = keys[random.randint(0, 3)]
        url = "https://kgsearch.googleapis.com/v1/entities:search?query=" + \
              query + "&key=" + key + "&limit=" + str(topk) + "&indent=True"
        try:
            response = requests.get(url, timeout=timeout).json()
            items = response.get("itemListElement", [])
            kg_items = [make_kg_item(noun_phrase, i) for i in items]
            kg_items = [i for i in kg_items if _is_valid(i)]
            if not kg_items:
                return []
            if self.state_manager.current_state.a_b_test.lower() != "d":
                return self.filter_by_similarity_v1([kg_items], 0.77)[0]
            else:
                return kg_items
        except Exception as e:
            logger.error(f"[{type(self).__name__}] fetch_google_api: {e}")
        return []

    def filter_by_similarity_v1(self, list_of_kg_items, match_threshold):
        logger.info("[filter_by_similarity_v1]")

        if not list_of_kg_items:
            return []
        cls_name = type(self).__name__
        match_timeout = (0.3, 0.5)
        output = []


        try:
            logger.info(f"[{cls_name}] filter_by_similarity before: {list_of_kg_items}")
            sim_server_input = [[_old_format(i) for i in kg_items] for kg_items in list_of_kg_items]
            similarity_scores = requests.post(url=GOOGLE_KG_MATCH_URL,
                                              json=sim_server_input,
                                              timeout=match_timeout)\
                                        .json()
        except Exception as e:
            # Similarity scores set to 1 if the filter is broken
            similarity_scores = [[1 for i in kg_items] for kg_items in list_of_kg_items]

        logger.info(f"[{cls_name}] filter_by_similarity scores: {similarity_scores}")
        for seg_kg, seg_score in zip(list_of_kg_items, similarity_scores):
            seg_filtered_all_return_descriptions = []
            for kg_item, score_item in zip(seg_kg, seg_score):
                if float(score_item) > match_threshold:
                    seg_filtered_all_return_descriptions.append(kg_item)
            output.append(seg_filtered_all_return_descriptions)


        logger.info(f"[{cls_name}] filter_by_similarity after: {output}")
        return output

    def filter_by_similarity_v2(self, list_of_kg_items, match_threshold):
        logger.info("[filter_by_similarity_v2]")
        if not list_of_kg_items:
            return []
        cls_name = type(self).__name__
        match_timeout = (0.3, 0.5)
        output = []

        # segments = self.input_data.get('segmentation', {}).get('segmented_text_raw', [])
        text = self.input_data["converttext"].lower()

        last_state = self.state_manager.last_state
        context = last_state.get('response', "") if last_state else ""
        # remove speechcon
        context = context.replace("\"", "")
        context = re.sub(r'\<.*?\>', "", context)

        try:
            logger.info(f"[{cls_name}] filter_by_similarity before: {list_of_kg_items}")
            sim_server_input = [[_old_format_descrp(i) for i in kg_items] for kg_items in list_of_kg_items]
            sim_server_request = {"context": context, "text": text, "kg": sim_server_input}

            similarity_scores = requests.post(url=GOOGLE_KG_MATCH_URL_V2,
                                              json=sim_server_request,
                                              timeout=match_timeout)\
                                        .json()

            logger.info(f"[{cls_name}] filter_by_similarity scores: {similarity_scores}")

            for seg_kg, seg_score in zip(list_of_kg_items, similarity_scores):
                seg_filtered_all_return_descriptions = []
                for kg_item, score_item in zip(seg_kg, seg_score):
                    if float(score_item) > match_threshold:
                        seg_filtered_all_return_descriptions.append(kg_item)
                output.append(seg_filtered_all_return_descriptions)

        except Exception as e:
            logger.info(f"[{cls_name}] error: {e}")

        logger.info(f"[{cls_name}] filter_by_similarity after: {output}")
        return output

    def _sort_kg_items(self, descriptions):
        output = []
        text = self.input_data["converttext"].lower()
        sm = self.state_manager
        target_module = get_target_module(sm.user_attributes, sm.last_state, text)
        def _score(_kg_item):
            _in_text = _kg_item['entity_name'].lower() in text
            _module_match = _kg_item['topic_module'] in target_module
            return math.log(float(_kg_item['result_score']), 10) + _in_text + _module_match

        return [sorted(kg_items, key=_score, reverse=True) for kg_items in descriptions if kg_items]
