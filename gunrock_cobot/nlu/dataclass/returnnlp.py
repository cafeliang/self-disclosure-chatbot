import itertools
import logging
import re
import operator
from collections import UserList
from dataclasses import dataclass, field, asdict, astuple
from typing import Any, Dict, List, Iterator, Optional, Set, Tuple, Union

from ..constants import DialogAct as DialogActEnum
from ..constants import Positivity, TopicModule
from ..nlu_metadata import keywords as MODULE_KEYWORDS
from ..utils.type_checking import is_type, ensure_type, safe_cast
from selecting_strategy.topic_mapping import (
    custom_intent_topic_map, amazon_topic_map_2019, concept_map,
    preprocess_concept_key, google_knowledge_map
)

logger = logging.getLogger(__name__)

NLU_METADATA = "nlu_metadata"  # Path to NLU Metadata
HAS_DIALOG_ACT_THRESHOLD = (0.5, 0.2)


class ReturnNLPError(Exception):

    def __init__(self, message: str):
        super().__init__()
        self.message = message

    def __str__(self):
        return f"[RETURNNLP] {self.message}"


class ReturnNLPIndexError(ReturnNLPError):

    def __init__(self, index: int, message: str = None):
        super().__init__(
            message=message or f"<{self.__class__.__name__}> at index={index}",
        )
        self.index = index


class ReturnNLPInvalidSegmentError(ReturnNLPError):

    def __init__(self, index: int, segment_raw=None, message: str = None):
        super().__init__(
            message=(message or
                     (f"<{self.__class__.__name__}> at index={index}" +
                      ("" if not segment_raw else f", segment={segment_raw}"))),
        )
        self.index = index


@dataclass(frozen=True)
class ReturnNLPSegment:

    @dataclass(frozen=True)
    class DialogActItem:
        label: DialogActEnum
        confidence: float

        @classmethod
        def from_dict(cls, d: Dict[str, Any]):
            return cls(
                label=ensure_type(d.get('label'), DialogActEnum, DialogActEnum.NA),
                confidence=safe_cast(ensure_type(d.get('confidence'), float, d.get('confidence')), float, 0.0)
            )

        def __bool__(self):
            return self.label is not DialogActEnum.NA and bool(self.confidence)

    @dataclass(frozen=True)
    class Intent:
        sys: List[str] = field(default_factory=list)
        topic: List[str] = field(default_factory=list)
        lexical: List[str] = field(default_factory=list)

        @classmethod
        def from_dict(cls, d: Dict[str, List[str]]):
            intents = {}
            for intent_str in ('sys', 'topic', 'lexical'):
                intent = ensure_type(d.get(intent_str), list, [])
                intents[intent_str] = list(filter(bool, (ensure_type(i, str, None) for i in intent)))
            return cls(**intents)

    @dataclass(frozen=True)
    class Sentiment:
        neg: float = 0.0
        neu: float = 0.0
        pos: float = 0.0
        compound: float = 0.0

        @classmethod
        def from_dict(cls, d: Dict[str, float]):
            values = {}
            for label in ('neg', 'neu', 'pos', 'compound'):
                # value is currently by default a str
                value = safe_cast(
                    ensure_type(d.get(label), str, default=lambda: ensure_type(d.get(label), float, "0.0")),
                    float, None)
                if value is None:
                    return cls()
                values[label] = value
            return cls(**values)

        def value(self) -> str:
            """return self repr as 'pos' | 'neu' | 'neg'."""
            return sorted(
                (i for i in asdict(self).items() if i[0] != 'compound'),
                key=lambda i: i[1], reverse=True)[0][0]

        def positivity(self) -> Positivity:
            return Positivity[self.value()]

        def __bool__(self):
            return not all(i == 0.0 for i in (self.neg, self.neu, self.pos, self.compound))

        def __eq__(self, other):
            if isinstance(other, ReturnNLPSegment.Sentiment):
                return astuple(self) == astuple(other)
            elif isinstance(other, str):
                return self.value() == other
            elif isinstance(other, Positivity):
                return self.positivity() is other
            else:
                return super().__eq__(other)

    @dataclass(frozen=True)
    class AmazonKG:
        name: str = ""
        entity_name: str = ""
        topic_module: str = ""
        entity_type: List[str] = field(default_factory=list)

        @classmethod
        def from_dict(cls, kg: dict):
            if (
                any(
                    i not in kg or not is_type(kg[i], str)
                    for i in ('name', 'entity_name', 'topic_module')
                ) or
                'entity_type' not in kg
            ):
                return cls()
            return cls(**kg)

        def __eq__(self, other: 'ReturnNLPSegment.AmazonKG'):
            return (self.name == other.name and
                    self.entity_name == other.entity_name and
                    self.entity_type == other.entity_type and
                    self.topic_module == other.topic_module)

        def __bool__(self):
            return all(bool(i) for i in (self.name, self.entity_name, self.topic_module))

    @dataclass(frozen=True)
    class GoogleKG:
        name: str = ""
        entity_name: str = ""
        description: str = ""
        detailed_description: str = ""
        result_score: float = 0.0
        topic_module: str = ""
        entity_type: List[str] = field(default_factory=list)

        @classmethod
        def from_dict(cls, kg: dict):
            if (
                any(
                    i not in kg or not is_type(kg[i], str)
                    for i in ('name', 'entity_name', 'description', 'detailed_description', 'topic_module')
                ) or
                'result_score' not in kg or 'entity_type' not in kg
            ):
                return cls()
            kg['result_score'] = safe_cast(kg['result_score'], float, 0.0)
            return cls(**kg)

        def __eq__(self, other: 'ReturnNLPSegment.GoogleKG'):
            return (self.name == other.name and
                    self.description == other.description and
                    self.detailed_description == other.detailed_description and
                    self.entity_type == other.entity_type and
                    self.topic_module == other.topic_module)

        def __bool__(self):
            return all(bool(i) for i in (self.name, self.topic_module))

    @dataclass(frozen=True)
    class Topic:

        @dataclass(frozen=True)
        class TopicKeywords:
            keyword: str = ""
            confidence: float = 0.0

            @classmethod
            def from_dict(cls, d: Dict[str, Union[str, float]]):
                keyword = ensure_type(d.get('keyword'), str, None)
                confidence = safe_cast(ensure_type(d.get('confidence'), float, d.get('confidence')), float, 0.0)
                if not keyword:
                    return cls()
                return cls(keyword=keyword, confidence=confidence)

            def __bool__(self):
                return bool(self.keyword) and self.confidence > 0

        topic_class: str = ""
        confidence: float = 0.0
        text: str = ""
        topic_keywords: List[TopicKeywords] = field(default_factory=list)

        @classmethod
        def from_dict(cls, d: Dict[str, Any]):
            topic_class = ensure_type(d.get('topicClass'), str, None)
            confidence = safe_cast(ensure_type(d.get('confidence'), float, d.get('confidence')), float, 0.0)
            text = ensure_type(d.get('text'), str, None)
            topic_keywords = ensure_type(d.get('topicKeywords'), list, [])

            if not topic_class or not text:
                return cls()

            topic_keywords = [tk_class
                              for tk_class in (
                                  ReturnNLPSegment.Topic.TopicKeywords.from_dict(d)
                                  for d in (ensure_type(tk, dict, None) for tk in topic_keywords) if d
                              ) if tk_class]

            return cls(topic_class=topic_class, confidence=confidence, text=text, topic_keywords=topic_keywords)

        def get_filtered_topic(self, threshold=0.8):
            if self.confidence < threshold:
                return None

            if self.topic_keywords and self.topic_keywords[0].confidence < threshold:
                return None

            return self

        def get_keywords(self, threshold=0) -> List[str]:
            return [topic_keyword.keyword
                    if topic_keyword.confidence >= threshold else None
                    for topic_keyword in self.topic_keywords]

    @dataclass(frozen=True)
    class Concept:

        @dataclass(frozen=True)
        class ConceptData:
            description: str = ""
            confidence: float = 0.0

            @classmethod
            def from_dict(cls, d: Dict[str, Union[str, float]]):
                description = ensure_type(d.get('description'), str, None)
                confidence = safe_cast(ensure_type(d.get('confidence'), float, d.get('confidence')), float, 0.0)
                if not description:
                    return cls()
                return cls(description=description, confidence=confidence)

            def __bool__(self):
                return bool(self.description) and bool(self.confidence)

        noun: str = ""
        data: List[ConceptData] = field(default_factory=list)

        @classmethod
        def from_dict(cls, concept: Dict[str, Union[str, Dict[str, str]]]):
            noun = ensure_type(concept.get('noun'), str, None)
            data = ensure_type(concept.get('data'), dict, {})

            if not noun:
                return cls()

            data = sorted([cd_class
                           for cd_class in (
                               ReturnNLPSegment.Concept.ConceptData.from_dict(cd)
                               for cd in (
                                   {'description': k, 'confidence': safe_cast(v, float, 0.0)}
                                   for k, v in data.items()
                                   if is_type(k, str) and (is_type(v, str) or is_type(v, float))
                               ) if cd
                           ) if cd_class],
                          key=lambda c: c.confidence, reverse=True)
            return cls(noun=noun, data=data)

        def __bool__(self):
            return bool(self.noun) and bool(self.data)

    @dataclass(frozen=True)
    class DependencyParsing:
        head: List[int] = field(default_factory=list)
        label: List[str] = field(default_factory=list)

        @classmethod
        def from_dict(cls, d: Dict[str, List[Union[int, str]]]):
            head = ensure_type(d.get('head'), list, [])
            label = ensure_type(d.get('label'), list, [])

            if (any(not is_type(i, int) for i in head) or any(not is_type(i, str) for i in label) or
                    len(head) != len(label)):
                return cls()

            return cls(head=head, label=label)

    @dataclass(frozen=True)
    class EntityItem:
        source: str = ""
        entity_in_utterance: str = ""
        entity_detected: str = ""
        entity_type: List[str] = field(default_factory=list)
        score: List[float] = field(default_factory=list)
        module: Optional[TopicModule] = None

        @classmethod
        def from_dict(cls, d: Dict[str, Any]):
            module = None
            try:
                module_name = d.get('module', None)
                module = TopicModule(module_name) if module_name else None
            except (KeyError, ValueError) as e:
                logger.warning(f"[RETURNNLP] failed. d={d}, error={e}", exc_info=True)
            finally:
                return cls(
                    source=ensure_type(d.get('source'), str, ""),
                    entity_in_utterance=ensure_type(d.get('entity_in_utterance'), str, ""),
                    entity_detected=ensure_type(d.get('entity_detected'), str, ""),
                    entity_type=ensure_type(d.get('entity_type'), list, []),
                    score=ensure_type(d.get('score'), list, []),
                    module=module
                )

        def __bool__(self):
            return bool(self.source) and bool(self.module)

        def __eq__(self, other):
            if isinstance(other, self.__class__):
                return self.__dict__ == other.__dict__
            else:
                return False

        def __ne__(self, other):
            return not self.__eq__(other)

    @dataclass(frozen=True)
    class GenericNer:
        entity_in_utterance: str = ""
        entity_detected: str = ""
        entity_type: str = ""
        index: int = 0
        module: str = ""

        @classmethod
        def from_dict(cls, d: Dict[str, Any]):
            return cls(
                entity_in_utterance=ensure_type(d.get('entity_in_utterance'), str, None),
                entity_detected=ensure_type(d.get('entity_detected'), str, None),
                entity_type=ensure_type(d.get('entity_type'), str, None),
                index=ensure_type(d.get('index'), int, 0),
                module=ensure_type(d.get('module'), str, None)
            )

        def __bool__(self):
            return bool(self.entity_in_utterance) and bool(self.entity_detected)

    text: str = field(default="")
    dialog_act: List[Union[str, float]] = field(default_factory=lambda: ['', 0.0])
    dialog_act2: List[Union[str, float]] = field(default_factory=lambda: ['', 0.0])
    dialog_act_label: Tuple[DialogActEnum, float] = field(default=(DialogActEnum.NA, 0.0))
    dialog_acts: List[DialogActItem] = field(default_factory=list)
    intent: Intent = field(default=Intent())
    sentiment: Sentiment = field(default=Sentiment())
    amazonkg: List[AmazonKG] = field(default_factory=list)  # Default: Allow multiple results for each key phrase
    googlekg: List[GoogleKG] = field(default_factory=list)  # Default: Allow multiple results for each key phrase
    googlekg_single_result_for_each_key_phrase: List[GoogleKG] = field(default_factory=list)  # Single result for each key phrase
    noun_phrase: List[str] = field(default_factory=list)
    key_phrase: List[str] = field(default_factory=list)
    topic: Topic = field(default_factory=Topic())
    amazon_topic: str = field(default_factory=str)
    concept: List[Concept] = field(default_factory=list)
    dependency_parsing: DependencyParsing = field(default_factory=DependencyParsing())
    pos_tagging: List[str] = field(default_factory=list)
    generic_ner: List[GenericNer] = field(default_factory=list)

    @classmethod
    def from_dict(cls, ref: Dict[str, Any]):

        text = ensure_type(ref.get('text'), str, "")
        dialog_act = ensure_type(ref.get('dialog_act'), list, [])
        dialog_act2 = ensure_type(ref.get('dialog_act2'), list, [])
        intent = ensure_type(ref.get('intent'), dict, {})
        sentiment = ensure_type(ref.get('sentiment'), dict, {})
        amazonkg_raw = ensure_type(ref.get('amazonkg'), list, [])
        googlekg_raw = ensure_type(ref.get('googlekg'), list, [])
        noun_phrase = ensure_type(ref.get('noun_phrase'), list, [])
        key_phrase = ensure_type(ref.get('keyphrase'), list, [])
        topic = ensure_type(ref.get('topic'), dict, {})
        amazon_topic = ensure_type(ref.get('amazon_topic'), str, "")
        concept = ensure_type(ref.get('concept'), list, [])
        dependency_parsing = ensure_type(ref.get('dependency_parsing'), dict, {})
        pos_tagging = ensure_type(ref.get('pos_tagging'), list, [])
        generic_ner = ensure_type(ref.get('generic_ner'), list, [])

        dialog_act_label = (DialogActEnum.NA, 0.0)
        dialog_acts = []
        for idx, (name, confidence) in enumerate(
            da_list
            if da_list and len(da_list) == 2 else ('NA', 0.0)
            for da_list in (dialog_act, dialog_act2)
        ):
            confidence = safe_cast(confidence, float, 0.0)
            dialog_act_enum = safe_cast(name, DialogActEnum, DialogActEnum.NA)
            if idx == 0:
                dialog_act = [name, confidence]
                dialog_act_label = (dialog_act_enum, confidence)
            elif idx == 1:
                dialog_act2 = [name, confidence]

            da = ReturnNLPSegment.DialogActItem.from_dict({'label': dialog_act_enum, 'confidence': confidence})
            dialog_acts.append(da)

        intent = ReturnNLPSegment.Intent.from_dict(intent)
        sentiment = ReturnNLPSegment.Sentiment.from_dict(sentiment)

        googlekg_single_result_for_each_key_phrase = []
        googlekg = []  # multiple result_for_each key phrase
        for list_ in googlekg_raw:
            kg_items = [ReturnNLPSegment.GoogleKG.from_dict(d) for d in list_]
            kg_items = [item for item in kg_items if item]
            if kg_items:
                googlekg_single_result_for_each_key_phrase.append(kg_items[0])
                googlekg.extend(kg_items)

        amazonkg = []  # multiple result_for_each key phrase
        for list_ in amazonkg_raw:
            kg_items = [ReturnNLPSegment.AmazonKG.from_dict(d) for d in list_]
            kg_items = [item for item in kg_items if item]
            if kg_items:
                amazonkg.extend(kg_items)

        noun_phrase = [np for np in (ensure_type(s, str, None) for s in noun_phrase) if np]
        key_phrase = [kp for kp in (ensure_type(s, str, None) for s in key_phrase) if kp]
        topic = ReturnNLPSegment.Topic.from_dict(topic)
        amazon_topic = ensure_type(amazon_topic, str, "") if amazon_topic is not None else ""
        concept = [
            c_class
            for c_class in (
                ReturnNLPSegment.Concept.from_dict(c)
                for c in (ensure_type(d, dict, None) for d in concept) if c
            ) if c_class
        ]
        dependency_parsing = ReturnNLPSegment.DependencyParsing.from_dict(dependency_parsing)
        pos_tagging = pos_tagging if all(ensure_type(pos, str, None) for pos in pos_tagging) else []
        generic_ner = [ReturnNLPSegment.GenericNer.from_dict(elem) for elem in generic_ner]

        return cls(
            text=text,
            dialog_act=dialog_act,
            dialog_act2=dialog_act2,
            dialog_act_label=dialog_act_label,
            dialog_acts=dialog_acts,
            intent=intent,
            sentiment=sentiment,
            googlekg=googlekg,
            amazonkg=amazonkg,
            googlekg_single_result_for_each_key_phrase=googlekg_single_result_for_each_key_phrase,
            noun_phrase=noun_phrase,
            key_phrase=key_phrase,
            topic=topic,
            amazon_topic=amazon_topic,
            concept=concept,
            dependency_parsing=dependency_parsing,
            pos_tagging=pos_tagging,
            generic_ner=generic_ner
        )

    def has_intent(self, intent: Union[str, List[str], Set[str]]):
        if isinstance(intent, str):
            intent = {intent}
        elif isinstance(intent, list):
            intent = set(intent)
        intents = set(itertools.chain(self.intent.sys, self.intent.topic, self.intent.lexical))
        return bool(intent & intents)

    def has_dialog_act(self,
                       dialog_act: Union[Union[str, DialogActEnum],
                                         List[Union[str, DialogActEnum]],
                                         Set[Union[str, DialogActEnum]]],
                       threshold: Union[float,
                                        Tuple[Optional[float], Optional[float]]] = HAS_DIALOG_ACT_THRESHOLD) -> bool:
        """
        Check whether any segment matches the dialog act name
        :param dialog_act: the name of the dialog act to be matched
        :param threshold: confidence threshold
        :return: True if matches, otherwise False
        """

        # standardize dialog_act query
        if isinstance(dialog_act, str) or isinstance(dialog_act, DialogActEnum):
            dialog_act = {dialog_act}
        elif isinstance(dialog_act, list):
            dialog_act = set(dialog_act)
        dialog_act: Set[Union[str, DialogActEnum]]

        # standardizing query param
        queries = set()
        for d in dialog_act:
            if isinstance(d, DialogActEnum):
                queries.add(d)
            elif isinstance(d, str):
                try:
                    queries.add(DialogActEnum(d))
                except ValueError as e:
                    logger.warning(f"has_dialog_act enum building failed, {e}", exc_info=True)
        queries: Set[DialogActEnum]

        # standardizing threshold
        if isinstance(threshold, float) or (isinstance(threshold, tuple) and len(threshold) < 2):
            threshold = (threshold, None)
        return bool(queries & set(self._dialog_act_filtered(threshold)))

    def _dialog_act_filtered(self, threshold: Tuple[Optional[float], Optional[float]]) -> List[DialogActEnum]:

        dialog_acts: Tuple[Optional[ReturnNLPSegment.DialogActItem], ...] = tuple(
            da if idx < len(threshold) and threshold[idx] and da.confidence >= threshold[idx] else None
            for idx, da in enumerate(self.dialog_acts))

        if not any(dialog_acts) or (len(dialog_acts) >= 1 and dialog_acts[0] is None):
            return []
        elif 1 <= len(dialog_acts) < 2:
            return [dialog_acts[0].label]
        else:
            return [da.label for da in dialog_acts if da]

    def detect_topics_by_regex(self):
        topic_list = []
        if "topic_weather" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_weather'])
        if "topic_holiday" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_holiday'])
        if "topic_game" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_game'])
        if "topic_movietv" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_movietv'])
        if "topic_storymeme" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_storymeme'])
        if "topic_book" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_book'])
        if "topic_newspolitics" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_newspolitics'])
        if "topic_techscience" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_techscience'])
        if "topic_music" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_music'])
        if "topic_outdoor" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_outdoor'])
        if "topic_sport" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_sport'])
        if "topic_animal" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_animal'])
        if "topic_entertainment" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_entertainment'])
        if "topic_travel" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_travel'])
        if "topic_food" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_food'])
        if "topic_fashion" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_fashion'])
        if "topic_dailylife" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_dailylife'])
        # if "topic_election" in self.Intent.topic:
        #     topic_list.append(custom_intent_topic_map['topic_election'])
        if "topic_others" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_others'])
        return topic_list

    def detect_topic_module_from_system_level(
        self, consider_entertainment_ner=False, consider_amazon_topic_2019=False
    ) -> List[str]:
        topic_list = []
        # get entity topic detecion results
        entity_topic_list = self.detect_entities(
            consider_entertainment_ner=consider_entertainment_ner,
            consider_amazon_topic_2019=consider_amazon_topic_2019,
            goookg_single_result_for_each_key_phrase=True)
        for entity_item in entity_topic_list:
            if entity_item.module:
                topic_list.append(entity_item.module.value)
        if "topic_entertainment" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['topic_entertainment'])
        if "say_funny" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['say_funny'])
        if "req_topic" in self.intent.topic:
            topic_list.append(custom_intent_topic_map['req_topic'])

        return topic_list

    def detect_entities(self, consider_entertainment_ner=False, consider_amazon_topic_2019=False, goookg_single_result_for_each_key_phrase: bool = False
    ) -> List['ReturnNLPSegment.EntityItem']:
        """
        detect entities with topic module from a segment in return nlp

        :param consider_entertainment_ner: output includes results from entertainment ner model,
            which detects movie/books/music names
        :param consider_amazon_topic_2019: output includes results from Amazon 2019 topic/DA model,
            which considers context and predicts MODULE/key_word
        :return: List[EntityItem]
        """
        entity_topic_list = []
        # Regex, first priority
        regex_entity_topic_list = self.get_entity_topic_list_from_regex()
        entity_topic_list.extend(regex_entity_topic_list)

        # Amazon topic detector 2019
        if consider_amazon_topic_2019:
            amazon_2019_entity_topic_list = self.get_entity_topic_list_from_amazon_topic_2019()
            entity_topic_list.extend(amazon_2019_entity_topic_list)

        if consider_entertainment_ner:
            entertainment_ner_entity_topic_list = self.get_entity_topic_list_from_ner()
            entity_topic_list.extend(entertainment_ner_entity_topic_list)

        # Concept Net
        concept_entity_topic_list = self.get_entity_topic_list_from_concept_kg()
        entity_topic_list.extend(concept_entity_topic_list)

        # Google knowledge
        google_entity_topic_list = self.get_entity_topic_list_from_google_kg(goookg_single_result_for_each_key_phrase)
        entity_topic_list.extend(google_entity_topic_list)

        return entity_topic_list

    def get_entity_topic_list_from_regex(self) -> List['ReturnNLPSegment.EntityItem']:
        regex_module_list = self.detect_topics_by_regex()
        regex_entity_topic_list = [
            ReturnNLPSegment.EntityItem.from_dict({
                "source": "regex",
                "module": module
            })
            for module in regex_module_list
        ]
        return regex_entity_topic_list

    def get_entity_topic_list_from_amazon_topic_2019(self) -> List['ReturnNLPSegment.EntityItem']:
        amazon_2019_entity_topic_list = []
        if self.amazon_topic is not None and amazon_topic_map_2019.get(self.amazon_topic, "") not in [""]:
            module = amazon_topic_map_2019[self.amazon_topic]
            if module:
                amazon_2019_entity_topic_list = [
                    ReturnNLPSegment.EntityItem.from_dict({"source": "amazon_topic_19", "module": module})
                ]
        return amazon_2019_entity_topic_list

    def get_entity_topic_list_from_ner(self) -> List['ReturnNLPSegment.EntityItem']:
        ner_entity_topic_list = []
        if self.generic_ner is not None and len(self.generic_ner) > 0:
            for ner_item in self.generic_ner:
                ner_entity_topic_list.append(
                    ReturnNLPSegment.EntityItem.from_dict({
                        "source": "entertainment_ner",
                        "entity_in_utterance": ner_item.entity_in_utterance,
                        "entity_detected": ner_item.entity_detected,
                        "entity_type": [ner_item.entity_type],
                        "module": ner_item.module
                    }))
        return ner_entity_topic_list

    def get_entity_topic_list_from_concept_kg(self) -> List['ReturnNLPSegment.EntityItem']:
        concept_kg_entity_topic_dic = {}
        if self.concept is not None and len(self.concept) > 0:
            for concept_item in self.concept:
                # NOTE: faster for now cuz we have a smaller map
                detect_key_pool = concept_map.keys()
                for item in concept_item.data:
                    proccessed_key = preprocess_concept_key(item.description)
                    if proccessed_key in detect_key_pool:
                        # if not in result, create one, otherwise append
                        module = concept_map[proccessed_key]
                        if module in concept_kg_entity_topic_dic.keys():
                            if "entity_type" in concept_kg_entity_topic_dic[module]:
                                concept_kg_entity_topic_dic[module]["entity_type"].append(item.description)
                            if "score" in concept_kg_entity_topic_dic[module]:
                                concept_kg_entity_topic_dic[module]["score"].append(item.confidence)
                        else:
                            concept_kg_entity_topic_dic[module] = {
                                "entity_type": [item.description],
                                "score": [item.confidence],
                                "entity_in_utterance": concept_item.noun
                            }
        concept_kg_entity_topic_list = [
            ReturnNLPSegment.EntityItem.from_dict({
                "source": "concept",
                "entity_in_utterance": value.get("entity_in_utterance", ""),
                "entity_detected": value.get("entity_in_utterance", ""),
                "score": value.get("score", []),
                "entity_type": value.get("entity_type", []),
                "module": module
            }) for module, value in concept_kg_entity_topic_dic.items()
        ]
        return concept_kg_entity_topic_list

    def get_entity_topic_list_from_google_kg(self, single_result_for_each_key_phrase: bool = False) -> List['ReturnNLPSegment.EntityItem']:
        google_kg_entity_topic_list = []

        google_kg_items = self._get_filter_google_kg_items(
            self.googlekg_single_result_for_each_key_phrase if single_result_for_each_key_phrase else
            self.googlekg)

        for kg_item in google_kg_items:
            topic_module = kg_item.topic_module
            if topic_module in google_knowledge_map and google_knowledge_map.get(topic_module, "") not in [""]:
                google_kg_entity_topic_list.append(
                    ReturnNLPSegment.EntityItem.from_dict({
                        "source": "google_kg",
                        "entity_in_utterance": kg_item.name,
                        "entity_detected": kg_item.entity_name,
                        "entity_type": kg_item.entity_type + [kg_item.description],
                        "score": kg_item.result_score,
                        "module": google_knowledge_map[topic_module]
                    }))
        return google_kg_entity_topic_list

    def _get_filter_google_kg_items(self, kg_items):
        filtered_google_kg_items = []
        for kg_item in kg_items:
            if kg_item.result_score > self.get_kg_threshold_for_system(kg_item):
                filtered_google_kg_items.append(kg_item)

        filtered_google_kg_items.sort(key=operator.attrgetter('result_score'), reverse=True)

        return filtered_google_kg_items

    @staticmethod
    def get_kg_threshold_for_system(google_kg_item: GoogleKG):
        topic = google_kg_item.topic_module
        if topic in ['movie']:
            return 1000
        elif topic in ['music', 'animals', 'travel']:
            return 600
        else:
            return 500


class ReturnNLP(UserList):

    def __init__(self, returnnlp_ref: Optional[List[Dict[str, Any]]]):

        def ensure_segment(idx, segment):
            try:
                if isinstance(segment, ReturnNLPSegment):
                    return segment
                elif segment is None:
                    raise ReturnNLPInvalidSegmentError(idx)
                elif not isinstance(segment, dict):
                    raise ReturnNLPInvalidSegmentError(idx, segment)
                else:
                    return ReturnNLPSegment.from_dict(segment)
            except Exception as e:
                logger.warning(e, exc_info=True)
                return ReturnNLPSegment.from_dict({})

        try:
            self.data: List[ReturnNLPSegment] = [ensure_segment(idx, r) for idx, r in enumerate(returnnlp_ref)]
            # logger.debug(f"[RETURNNLP] post_init: {self}")
        except TypeError as e:
            logger.warning(f"[RETURNNLP] __init__ failed: {e}", exc_info=True)
            self.data = []

    @classmethod
    def from_list(cls, returnnlp_ref: Optional[List[Dict[str, Any]]]):
        """
        API compatibility with CentralElement
        """
        return cls(returnnlp_ref)

    def __getitem__(self, i):
        try:
            new_data = self.data[i]
        except IndexError:
            logger.warning(ReturnNLPIndexError(i), exc_info=True)
            new_data = None
        finally:
            if isinstance(new_data, list):
                return self.__class__.from_list(new_data)
            elif isinstance(new_data, ReturnNLPSegment):
                return new_data
            else:
                return ReturnNLPSegment.from_dict({})

    def __iter__(self):
        for item in self.data:
            yield item

    def __str__(self):
        return f"ReturnNLP({super().__str__()})"

    # MARK: - Convenient Iterators for getting ReturnNLPSegments components

    @property
    def text(self) -> List[str]:
        return [data.text for data in self.data]

    @property
    def text_excluding_dislike_segments(self) -> str:
        return " ".join([seg.text for seg in self if not seg.has_intent(["ans_dislike", "opinion_negative"])])

    @property
    def dialog_act_label(self) -> List[Tuple[DialogActEnum, float]]:
        return [data.dialog_act_label for data in self.data]

    @property
    def intent(self) -> List[ReturnNLPSegment.Intent]:
        return [data.intent for data in self.data]

    @property
    def sys_intent(self) -> List[List[str]]:
        return [data.intent.sys for data in self.data]

    @property
    def flattened_sys_intent(self) -> List[str]:
        return list(itertools.chain.from_iterable(self.sys_intent))

    @property
    def topic_intent(self) -> List[List[str]]:
        return [data.intent.topic for data in self.data]

    @property
    def flattened_topic_intent(self) -> List[str]:
        return list(itertools.chain.from_iterable(self.topic_intent))

    @property
    def lexical_intent(self) -> List[List[str]]:
        return [data.intent.lexical for data in self.data]

    @property
    def flattened_lexical_intent(self) -> List[str]:
        return list(itertools.chain.from_iterable(self.lexical_intent))

    @property
    def sentiment(self) -> List[ReturnNLPSegment.Sentiment]:
        return [data.sentiment for data in self.data]

    @property
    def googlekg(self) -> List[List[ReturnNLPSegment.GoogleKG]]:
        return [data.googlekg for data in self.data]

    @property
    def noun_phrase(self) -> List[List[str]]:
        return [data.noun_phrase for data in self.data]

    @property
    def key_phrase(self) -> List[List[str]]:
        return [data.key_phrase for data in self.data]

    @property
    def topic(self) -> List[ReturnNLPSegment.Topic]:
        return [data.topic for data in self.data]

    @property
    def concept(self) -> List[List[ReturnNLPSegment.Concept]]:
        return [data.concept for data in self.data]

    @property
    def answer_positivity(self) -> Positivity:
        # first check positivity from dialog act
        if (
            self.has_dialog_act(DialogActEnum.POS_ANSWER) and
            self.has_dialog_act(DialogActEnum.NEG_ANSWER)
        ):
            # if both, find the last dialog act with pos/neg
            for segment in reversed(self):
                if segment.has_dialog_act({DialogActEnum.POS_ANSWER, DialogActEnum.NEG_ANSWER}):
                    return (
                        Positivity.pos if segment.has_dialog_act(DialogActEnum.POS_ANSWER) else
                        Positivity.neg if segment.has_dialog_act(DialogActEnum.NEG_ANSWER) else
                        Positivity.neu
                    )
        elif (
            self.has_dialog_act(DialogActEnum.POS_ANSWER) and
            not self.has_dialog_act(DialogActEnum.NEG_ANSWER)
        ):
            return Positivity.pos
        elif (
            self.has_dialog_act(DialogActEnum.NEG_ANSWER) and
            not self.has_dialog_act(DialogActEnum.POS_ANSWER)
        ):
            return Positivity.neg

        # if they fail, check lexical
        elif (
            self.has_intent({'ans_positive', 'ans_like'}) and
            not self.has_intent({'ans_negative', 'ans_dislike'})
        ):
            return Positivity.pos
        elif (
            self.has_intent({'ans_negative', 'ans_dislike'}) and
            not self.has_intent({'ans_positive', 'ans_like'})
        ):
            return Positivity.neg

        # if both fail, return neu
        else:
            return Positivity.neu

    @property
    def opinion_positivity(self) -> Positivity:
        # first check positivity from dialog act
        if (
            self.has_dialog_act(DialogActEnum.POS_ANSWER) and
            self.has_dialog_act(DialogActEnum.NEG_ANSWER)
        ):
            # if both, find the last dialog act with pos/neg
            for segment in reversed(self):
                if segment.has_dialog_act({DialogActEnum.POS_ANSWER, DialogActEnum.NEG_ANSWER}):
                    return (
                        Positivity.pos if segment.has_dialog_act(DialogActEnum.POS_ANSWER) else
                        Positivity.neg if segment.has_dialog_act(DialogActEnum.NEG_ANSWER) else
                        Positivity.neu
                    )
        elif (
            self.has_dialog_act(DialogActEnum.POS_ANSWER) and
            not self.has_dialog_act(DialogActEnum.NEG_ANSWER)
        ):
            return Positivity.pos
        elif (
            self.has_dialog_act(DialogActEnum.NEG_ANSWER) and
            not self.has_dialog_act(DialogActEnum.POS_ANSWER)
        ):
            return Positivity.neg

        # if they fail, check lexical
        elif (
            self.has_intent({'opinion_positive'}) and
            not self.has_intent({'opinion_negative'})
        ):
            return Positivity.pos
        elif (
            self.has_intent({'opinion_negative'}) and
            not self.has_intent({'opinion_positive'})
        ):
            return Positivity.neg

        # if those fail, check sentiment for {'OPINION', 'COMMENT'}

        else:
            senti_score = sum(
                segment.sentiment.positivity().as_int()
                for segment in self
                if segment.has_dialog_act({DialogActEnum.OPINION, DialogActEnum.COMMENT})
            )
            return (
                Positivity.pos if senti_score >= 1 else
                Positivity.neg if senti_score <= -1 else
                Positivity.neu
            )

    def has_intent(self, intent: Union[str, List[str], Set[str]]):
        return any(seg.has_intent(intent) for seg in self)

    def has_intent_seg(self, seg, intent: Union[str, List[str], Set[str]]):
        return self[seg].has_intent(intent)

    def has_dialog_act(self,
                       dialog_act: Union[Union[str, DialogActEnum],
                                         List[Union[str, DialogActEnum]],
                                         Set[Union[str, DialogActEnum]]],
                       index: Optional[int] = None,
                       threshold: Union[float,
                                        Tuple[Optional[float], Optional[float]]] = HAS_DIALOG_ACT_THRESHOLD) -> bool:
        """
        Check whether any segment matches the dialog act name
        :param dialog_act: the name of the dialog act to be matched
        :param index (deprecated): Use slicing on ReturnNLP instead.
        :param threshold: confidence threshold
        :return: True if matches, otherwise False
        """
        if not self.data:
            return False

        if index:
            try:
                self.data[index]
            except IndexError as e:
                logger.warning(f"[NLU] ReturnNLP.has_dialog_act() has invalid index: "
                               f"index: {index}, len(self): {len(self.data)}, error: {e}")
                return False

        # standardize dialog_act query
        if isinstance(dialog_act, str) or isinstance(dialog_act, DialogActEnum):
            dialog_act = {dialog_act}
        elif isinstance(dialog_act, list):
            dialog_act = set(dialog_act)
        dialog_act: Set[Union[str, DialogActEnum]]

        # standardizing query param
        queries = set()
        for d in dialog_act:
            if isinstance(d, DialogActEnum):
                queries.add(d)
            elif isinstance(d, str):
                try:
                    queries.add(DialogActEnum(d))
                except ValueError as e:
                    logger.warning(e, exc_info=True)
        queries: Set[DialogActEnum]

        # standardizing threshold
        if isinstance(threshold, float) or (isinstance(threshold, tuple) and len(threshold) < 2):
            threshold = (threshold, None)

        data = self.data if not index else [self.data[index]]
        values = set(itertools.chain.from_iterable(segment._dialog_act_filtered(threshold) for segment in data))
        return bool(queries & values)

    @property
    def topic_intent_modules(self) -> Set[TopicModule]:
        """
        Return the module in Enum from those detected in topic_intent
        """
        from selecting_strategy.topic_mapping import custom_intent_topic_map

        def topic_to_module(topic_intent: str) -> Optional[TopicModule]:
            try:
                module_name = custom_intent_topic_map[topic_intent]
                module = TopicModule(module_name)
                return module if module in TopicModule.proposable else None
            except (KeyError, ValueError):
                return None

        topic_intents: Iterator[str] = itertools.chain.from_iterable(i for i in self.topic_intent)
        topic_intents = filter(None, map(topic_to_module, topic_intents))
        return set(topic_intents)

    def is_incomplete_utterance(self, threshold=0.8):
        NON_INCOMPLETE_UTTERANCE_REGEX = [
            "^when$", "^which$", "^maybe$", "^probably$", "^very$", "^whatever$", "^definitely$", "^same here$",
            "^so do i$", "^we are$", "^love to$", r"^(.+ ){2,}(want|like) to$", "^one$", "^duh$",
            "^na$", "^pop$", "^page$"
        ]

        combined_regex = "(" + ")|(".join(NON_INCOMPLETE_UTTERANCE_REGEX) + ")"

        is_incomplete_utterance = (
            self.data[-1].has_dialog_act(DialogActEnum.ABANDONED, threshold=threshold) and
            not re.match(combined_regex, self.data[-1].text)
        )

        logger.info(f"[returnnlp] is_incomplete_utterance: {is_incomplete_utterance}")

        return is_incomplete_utterance

    def is_hesitant(self, threshold=0.8):
        is_hesitant = self.data[-1].has_dialog_act(DialogActEnum.HOLD, threshold=threshold)

        logger.info("[returnnlp] is_hesitant: {}".format(is_hesitant))

        return is_hesitant

    def detect_module_keywords(self, module) -> List[Dict[str, Any]]:
        """This only works for TECHSCIENCECHAT for now
        Add {your chat}.json to nlu_metadata for different topics
        """
        keywords = MODULE_KEYWORDS.data.get(module, {}).get('keyword_detector')
        if not keywords:
            return []

        def topic_filter(topic: ReturnNLPSegment.Topic):
            return sum(d['weight']
                       for d in keywords
                       if d['source'] == 'topic'
                       and d['description'] == topic.topic_class
                       and d['confidence'] > topic.confidence) > 0

        def concept_net_filter(concept_data: ReturnNLPSegment.Concept.ConceptData):
            return sum(d['weight']
                       for d in keywords
                       if d['source'] == 'concept_net'
                       and d['description'] == concept_data.description
                       and d['confidence'] > concept_data.confidence) > 0

        processed_topic = filter(topic_filter, self.topic)
        processed_concept = (data.noun for data in filter(
            concept_net_filter, (
                concept_data for concept in self.concept
                for concept_item in concept
                for concept_data in concept_item.data)
        ))
        processed_topic_keywords = (keyword.keyword for t in processed_topic for keyword in t.topic_keywords)

        return list(itertools.chain.from_iterable((processed_topic, processed_concept, processed_topic_keywords)))

    def detect_keyword_from_mapping(self, mapping: Dict[str, set]) -> List[str]:

        def get_topics_from_nounphrase(history=0):
            """Detect topic with nounphrase.

            Parameter:
            history: how long to look at the history user utterance.
                    0 to look at the only the current utterance
            Return:
            proposed: a list of nounphrases detected.
            """
            proposed = []
            if history == -1:
                history = len(self.nounphrase)
            if len(self.noun_phrases) > 0:
                nounphrase = self.noun_phrases[:history + 1]
                for current_nounphrase in nounphrase:
                    for np in current_nounphrase:
                        proposed.append(np)
            return proposed

        def get_topics_from_conceptnet(history=0):
            """Detect topic from conceptnet

            Parameter:
            history: how long to look at the history user utterance.
                    0 to look at the only the current utterance
            Return:
            proposed: a list of topics detected.
            """
            proposed = []
            if len(self.concept) > 0:
                if history == -1:
                    history = len(self.nounphrase)
                concepts = self.concept[:history + 1]
                for concept_lst in concepts:
                    for c in concept_lst:
                        topic = c.noun
                        proposed.append(topic)
            return proposed

        def create_regex_for_predefined_keywords(keyword_mapping):
            regex = [r"\b{}\b".format(v) for v in keyword_mapping]
            TOPIC_REGEX = r"|".join(regex)
            return TOPIC_REGEX

        def normalize_topic(raw_text, keyword_mapping: Dict[str, set]):
            for topic in keyword_mapping:
                if raw_text in keyword_mapping[topic]:
                    return topic

        return None

        proposed_nounphrase = get_topics_from_nounphrase()
        result = []
        r = create_regex_for_predefined_keywords(mapping)

        for np in proposed_nounphrase:
            noun_phrase = normalize_topic(np, mapping)
            if noun_phrase is not None and re.search(r, noun_phrase):
                result.append(noun_phrase)
        return result

    def detect_entities(
        self, consider_entertainment_ner=False, consider_amazon_topic_2019=False
    ) -> List[ReturnNLPSegment.EntityItem]:

        return list(itertools.chain.from_iterable(
            seg.detect_entities(
                consider_entertainment_ner=consider_entertainment_ner,
                consider_amazon_topic_2019=consider_amazon_topic_2019)
            for seg in self
        ))
