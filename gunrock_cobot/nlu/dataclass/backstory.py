import logging
import json
from dataclasses import dataclass, asdict
from typing import Any, Dict, Optional
from nlu.constants import TopicModule

logger = logging.getLogger(__name__)


@dataclass
class Backstory:
    text: str = ""
    confidence: float = 0.0
    followup: str = ""
    module: Optional[TopicModule] = None
    bot_propose_module: Optional[TopicModule] = None
    neg: str = ""
    pos: str = ""
    postfix: str = ""
    question: str = ""
    reason: str = ""
    tag: str = ""

    @classmethod
    def from_dict(cls, d: Dict[str, Any]):
        module = None
        try:
            module_name = d.get('module', None)
            module = TopicModule(module_name) if module_name else None
        except (KeyError, ValueError) as e:
            logger.warning(f"[BACKSTORY] failed. d={d}, error={e}", exc_info=True)
        finally:

            try:
                bot_propose_module_name = d.get('bot_propose_module', None)
                bot_propose_module = TopicModule(bot_propose_module_name) if bot_propose_module_name else None
            except (KeyError, ValueError) as e:
                logger.warning(f"[BACKSTORY] failed. d={d}, error={e}", exc_info=True)

            finally:
                return cls(
                    question=d.get('question', ""),
                    text=d.get('text', ""),
                    confidence=d.get('confidence', 0.0),
                    module=module,
                    bot_propose_module=bot_propose_module,
                    reason=d.get('reason', ""),
                    followup=d.get('followup', ""),
                    tag=d.get('tag', ""),
                    neg=d.get('neg', ""),
                    pos=d.get('pos', ""),
                    postfix=d.get('postfix', "")
                )

    def to_json(self):
        json.dumps(asdict(self))
