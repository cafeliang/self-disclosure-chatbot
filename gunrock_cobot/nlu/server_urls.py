STANFORD_NLP_URL = 'http://language.cs.ucdavis.edu'
SEGMENTATION_URL = 'http://169.237.4.21:5003/translator/translate'  # interaction server
DIALOG_ACT_V3 = 'http://169.237.4.21:5001/dialogact/classify' # interaction server
NP_URL = 'http://169.237.4.21:5004/get_np/?sentence=' # interaction server
NER_URL = 'http://169.237.4.21:5002/get_NER/?sentence=' # interaction server


GOOGLE_KNOWLEDGE_GRAPH_URL = 'http://ec2-3-229-17-237.compute-1.amazonaws.com:5010/get_KG/'
# change port number for new dialog act models"

DIALOG_ACT_V4 = 'http://52.3.201.51:9805/dialogact/classify'
GOOGLE_KG_MATCH_URL = 'http://52.3.201.51:5006/googlekg/classify'
GOOGLE_KG_MATCH_URL_V2 = 'http://52.3.201.51:5007/googlekg/classify'
SEGMENTATION_URL_V2 = 'http://52.3.201.51:4022/translator/translate'
KEYPHRASE_URL_V1 = 'http://52.20.135.52:5011/keyphrase'
GENERIC_NER_V1 = 'http://52.20.135.52:5019/ner'


SENTENCE_COMPLETION_URL = 'http://52.3.201.51:5000/translator/translate'
DEPENDENCY_URL = 'http://52.3.201.51:1926/parse'
SENTIMENT_URL = 'http://3.229.52.102:5025/sentiment_analysis/get_sentiment'
BLENDER_QUESTION_HANDLING = "http://169.237.4.21:5007/blender/redis_generate"
BLENDER_RETRIEVAL = "http://35.170.180.156:5008/blender/redis_generate"
