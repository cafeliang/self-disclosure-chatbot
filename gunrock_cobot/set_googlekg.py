import pickle
from num2words import num2words
import string

from util_redis import RedisHelper


def clean_title(text):
    text = text.lower()

    # remove punctuations
    exclude = set(string.punctuation)
    exclude.remove(".")
    s = ''.join(ch for ch in text if ch not in exclude)
    
    # number to word
    ret = []
    for word in s.split():
        word = num2words(int(word), to='year') if word.isdigit() else word
        ret.append(word)
    ret_text = " ".join(ret)

    return ret_text


def main():
    with open("nlu/sorted_movie.txt", "rb") as fp:
        sorted_movies = pickle.load(fp)

    for movie in sorted_movies:
        movie_title, title_type, popularity = movie
        cleaned_movie_title = clean_title(movie_title)
        title_type = "tv" if title_type == "tvSeries" else "movie"

        # special case
        if cleaned_movie_title == "once upon a time ...in hollywood":
            cleaned_movie_title = "once upon a time in hollywood"

        confidence_score = '700'
        redis_kg_format = [[cleaned_movie_title, movie_title, title_type, confidence_score, "movie"]]

        RedisHelper().set(RedisHelper.PREFIX_NPKNOWLEDGE, cleaned_movie_title, redis_kg_format)

        
if __name__ == "__main__":
    main()
