import re
import logging

from cobot_common.service_client import get_client
from constants_api_keys import COBOT_API_KEY
class FactualQuestionHandler:

    response_filter = (
    r"amzn1.ask.skill.|amzn1.alexa-speechlet-client|"
    r"an opinion on that|tough to explain|I didn’t (get|hear|catch) that.|Please try that again|"
    r"As a noun|As an adjective|As a verb|"
    r"('favorite'|'that'|'Next'|'favorite'|'Mind'|'Talking'|'Artist') is usually defined|"
    r"('his last name') is spelled|"
    r"Sorry, I can’t find the answer to the question I heard|"
    r"Alexa Original|is a plural form of|Hi there|\bHi\b|"
    r"Sorry, I can’t find the answer to the question I heard.|I don't have an opinion on that.|"
    r"I'm not sure what you mean|Sorry, I don't understand|"
    r"You could ask me about music or geography.|You can ask me anything you like.|"
    r"I can answer questions about people, places and more.|^As a|I didn't get that|"
    r"The Place I Belong, the movie released|"
    r"usually defined as plural of what|sound is a vibration|"
    r"something that would happen|Say is a town in southwest Niger|An album is a collection of|"
    r"I love dancing|I pronounce that 'would'|I pronounce that 'do'|'Was' is related to a word|"
    r"Dreams aren't well understood|\"Me Too\" or \"#MeToo\"|"
    r"To experience Audible|"
    r"Yours, the chain of women's clothing outlets.|"
    r"I'm Alexa|A socialbot is an Alexa skill that converses|say open wikiHow|"
    r"Cat is a species of living things|"
    r"Special Activities Center|usually defined as as follows, thuswise|"
    r"amzn1.ask.skill.|the number is used in calling|speaker of the United States House of Representatives|"
    r"I pronounce that 'wrong'|is a YouTuber with|the audio publications label|"
    r"by completing Voice Training|citizens of the United Kingdom|"
    r"Bath is the largest city in the county|Playing by Crystal Kaswell|"
    r"real number is any number|Special Occasions is the 18th episode|"
    r"I don't know Harry Potter and the Deathly Hallows|amzn1.alexa-speechlet-client|"
    r"Andorra, summer begins|Real is located in Quezon|played by Tom Kenny as SpongeBob|"
    r"Ashanti, 35, does not have any children|performs as Your Friend|Han shot first|"
    r"Hu's on first|identification card through the Department of Motor Vehicles|"
    r"average MSRP of Ram vehicles|noun 'doing' is usually defined|noun, rating, can have a few meanings|"
    r"'it' is usually defined as|Tokyo Stock Exchange|Ask Me's performer is Elvis Presley|"
    r"telling a good story. To hear one, just say|AirPlay is located in United States|"
    r"International Exhibitions, United States beat Costa Rica|a legal entity representing an association of people|"
    r"Minecraft is Stephen's 5th Let's Play|imported liqueur has about 103 calories|"
    r"noun meaning sol|is a subspecies of living things|operating system, API, video game|"
    r"tell me to turn up the volume|third‐person singular personal pronoun|"
    r"in addition to previously mentioned|just ask for the track|Beatles was released on October 6|"
    r"Something in My Soul|'Toys' is a form of the noun 'toy'|Anything But Love|"
    r"New York is a city in Kings County|census geographic units in New Brunswick|"
    r"Canada is a country in Americas|traditionally venerated as the protomartyr|"
    r"Sleeping, the post-hardcore band from Long Island|find support on Amazon.com|"
    r"noun 'movie' is usually defined|noun 'three' is usually defined|Apple Inc. is an American multinational|"
    r"Someone Else is a 2006 British|Competition arises whenever at least two parties|"
    r"letter of the ISO basic Latin alphabet|Lary died on December|the simple past tense of|"
    r"largest brewing company|Reisigl was a Major League Baseball pitcher|thick white fluid containing spermatozoa|"
    r"I can sing dozens of songs|NHRA News Today|change your wake word|noun 'name' is usually defined|"
    r"icon and select your device|Those People is a 2015 drama film|population of australasian snappers|"
    r"writing an honest character|Confused Destroyer of Planets|W-2 paperless employee|"
    r"Michigan is a state located in the Midwestern|made for TV movie and Feature film|"
    r"nipple is a raised region of tissue|persons older than 65 numbered|book by Sandra Kemayou|"
    r"pronoun 'yours' is usually defined|twenty-four House Lives"
    )

    @staticmethod
    def handle_question(question: str):
        fetched_response = FactualQuestionHandler.fetch_from_evi(question)
        return fetched_response

    @staticmethod
    def fetch_from_evi(question: str):
        question_handler = get_client(api_key=COBOT_API_KEY, timeout_in_millis=1200)

        try:
            r = question_handler.get_answer(question=question, timeout_in_millis=1200)
            response_text = r["response"]

            if not response_text:
                return None

            processed_response_text = FactualQuestionHandler.process_evi_response(response_text)
            logging.debug("[Evi] response: {}. Processed response: {}".format(response_text, processed_response_text))
            if not processed_response_text:
                return None

            if FactualQuestionHandler.should_filter_out_response(response_text):
                logging.debug("[Evi] response ignored by filter: {}".format(response_text))
                return None

            if len(processed_response_text) > 450:
                return None

            return processed_response_text + " "
        except Exception as e:
            logging.warning("[FactualQuestionHandler] error: {}".format(e))
            return None

    @staticmethod
    def should_filter_out_response(response_text):
        return re.search(FactualQuestionHandler.response_filter, response_text, flags=re.IGNORECASE) is not None

    @staticmethod
    def process_evi_response(response_text):
        split_response = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', response_text)

        if len(split_response[0]) > 250:
            final_response = split_response[0]

        elif len(split_response) > 2:
            final_response = ' '.join(split_response[:2])

        else:
            final_response = ' '.join(split_response)

        return final_response


if __name__ == '__main__':
    response = FactualQuestionHandler.handle_question("how long can a dog live")
    print(response)