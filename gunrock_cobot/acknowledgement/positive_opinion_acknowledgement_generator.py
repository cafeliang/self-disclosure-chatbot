import re
import logging
from typing import List, Optional
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from nlu.constants import DialogAct

from nlu.intentmap_scheme import POSITIVE_COMMENT_ADJ_SUITABLE_TO_BE_ECHOED


class PositiveOpinionAcknowledgementGenerator:

    def __init__(self, returnnlp: Optional[List[dict]]):
        self.returnnlp = ReturnNLP.from_list(returnnlp) if returnnlp else None

    def generate_ack(self) -> Optional[dict]:
        if not self.returnnlp:
            return None

        if self.end_with_command_or_question():
            return None

        for seg in self.returnnlp:
            match = self.detect_positive_echoeable_adj(seg.text)
            if match:
                matched_groups = match.groups()
                if matched_groups:
                    matched_groups = list(filter(None, matched_groups))
                    if matched_groups:
                        content = matched_groups[-1].lower()
                        return self.wrap_output("user_positive_opinion/with_keyword", content)

            if "opinion_positive" in seg.intent.lexical:
                return self.wrap_output("user_positive_opinion/without_keyword")

        return None

    @staticmethod
    def detect_positive_echoeable_adj(text):
        result = re.search(POSITIVE_COMMENT_ADJ_SUITABLE_TO_BE_ECHOED, text)
        logging.info(f"[PositiveOpinionAcknowledgementGenerator] detect_positive_echoeable_adj: {result}")
        return result

    def end_with_command_or_question(self):
        return self.returnnlp[-1].has_dialog_act([DialogAct.COMMANDS,
                                                  DialogAct.OPEN_QUESTION,
                                                  DialogAct.OPEN_QUESTION_FACTUAL,
                                                  DialogAct.OPEN_QUESTION_OPINION,
                                                  DialogAct.YES_NO_QUESTION])

    def wrap_output(self, template_key, slots=None):
        if slots is None:
            slots = {}
        return {
            "key": template_key,
            "slots": {"content": slots},
            "tag": "positive_opinion"
        }
