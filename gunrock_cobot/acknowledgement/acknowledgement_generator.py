import itertools
import string
from types import SimpleNamespace
from nlu.util_nlu import in_dialog_act
from typing import List

from nlu.intent_classifier import *  # noqa
from nlu.dataclass.returnnlp import ReturnNLP
from nlu.constants import DialogAct
import utils
import json
import template_manager
import logging
import requests
import re

from cobot_core.log.logger import LoggerFactory  # noqa
from typing import Dict
from nlu.dataclass import ReturnNLP, ReturnNLPSegment
from nlu.dataclass.central_element import CentralElement
from nlu.constants import DialogAct as DialogActEnum
from nlu.stopwords import stopwords
from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier
from acknowledgement.positive_opinion_acknowledgement_generator import PositiveOpinionAcknowledgementGenerator
from response_templates.special_template_keys import open_question_general_templates, \
    open_question_ask_hobbies_templates, open_question_event_templates
from nlg.blender import BlenderResponseGenerator
from utils import trace_latency

ACKNOWLEDGEMENT_URL = "http://ec2-34-193-157-207.compute-1.amazonaws.com:5001/ack"
TEMPLATE_ACKNOWLEDGEMENT = template_manager.Templates.acknowledgement
error_template = template_manager.Templates.error


class LastResponseTag:
    ASK_EVENT = "ask_event"      # eg. "What do you plan to do in the weekend? "
    ASK_HOBBIES = "ask_hobbies"  # eg. "what do you like to do for fun? "
    ASK_WHAT_TO_TALK = "ask_what_to_talk"  # eg. "Is there anything you'd like to talk about? "
    EXPECT_OPINION = "expect_opinion"

class AckServerInputTag:
    ASK_HOBBIES = "ask_hobbies"
    ASK_GENERAL = "ask_general"


class AckServerOutputTag:
    IDK = "ack_question_idk"
    GENERAL = "ack_general"


class UserIntent:
    POSITIVE_OPINION = "positive_opinion"
    THANKING = "thanking"
    APPRECIATION = "appreciation"
    COMPLAINT = "complaint"
    APOLOGY = "apology"
    NOT_SURE = "not_sure"


class LocalOutputTag:
    """
    What's the content being acknowledged
    """
    ACK_HOBBIES = "ack_hobbies"
    ACK_WHAT_TO_TALK = "ack_what_to_talk"
    ACK_GENERAL = "ack_general"
    ACK_OPINION = "ack_opinion"
    ACK_QUESTION_IDK = "ack_question_idk"



class AcknowledgementGenerator:
    def __init__(self, state_manager, user_utterance_features):
        
        self.logger = LoggerFactory.setup(self)
        self.state_manager = state_manager
        self.last_response = state_manager.last_state.get("response", "")
        self.user_attributes = state_manager.user_attributes
        self.user_utterance = state_manager.current_state.text
        self.user_utterance_features = user_utterance_features

        returnnlp_raw = self.__get_returnnlp_raw()
        self.returnnlp = ReturnNLP(returnnlp_raw)
        self.central_element = CentralElement.from_dict(self.state_manager.current_state.features.get("central_elem", {}))

        self.locally_generated_ack = self.get_locally_generate_ack_type_list()

        self.blender_generator = BlenderResponseGenerator(self.state_manager.user_attributes)

    def get_self_disclosure_level(self):
        return getattr(self.user_attributes, "self-disclosure-study-current-level", "factual")


    def get_locally_generate_ack_type_list(self):
        if self.get_self_disclosure_level() == "emotional":
            return [
                self.__generate_ack_if_user_opinion_crazy,
                self.__generate_ack_if_user_opinion_surprising,
                self.__generate_ack_if_user_positive_opinion,
                self.__generate_ack_if_user_appreciation,
                self.__generate_ack_if_user_thanking,
                self.__generate_ack_if_user_complaint,
                self.__generate_ack_if_user_apology,
                self.__generate_ack_if_user_dont_know,
            ]
        elif self.get_self_disclosure_level() == "cognitive":
            return [
                self.__generate_ack_if_user_positive_opinion,
                self.__generate_ack_if_user_opinion_crazy,
                self.__generate_ack_if_user_appreciation,
                self.__generate_ack_if_user_thanking,
                self.__generate_ack_if_user_complaint,
                self.__generate_ack_if_user_apology,
                self.__generate_ack_if_user_dont_know,
            ]
        else:
            return [
                self.__generate_ack_if_user_appreciation,
                self.__generate_ack_if_user_thanking,
                self.__generate_ack_if_user_complaint,
                self.__generate_ack_if_user_apology,
                self.__generate_ack_if_user_dont_know,
            ]
    def __get_returnnlp_raw(self):
        return self.state_manager.current_state.features.get("returnnlp", [])

    @property
    def get_input_tag(self) -> str:
        input_tag = ""
        if self.state_manager.last_state:

            bot_ask_open_question_in_last_state = self.state_manager.last_state.get("bot_ask_open_question", False)
            last_template_keys = self.state_manager.last_state.get("template_keys", [])

            if bot_ask_open_question_in_last_state:
                if set(last_template_keys).intersection(open_question_ask_hobbies_templates):
                    input_tag = LastResponseTag.ASK_HOBBIES
                elif set(last_template_keys).intersection(open_question_event_templates):
                    input_tag = LastResponseTag.ASK_EVENT
                elif set(last_template_keys).intersection(open_question_general_templates):
                    input_tag = LastResponseTag.ASK_WHAT_TO_TALK
            # elif self.expect_user_express_opinion():
            #     input_tag = LastResponseTag.EXPECT_OPINION

        logging.info(f"[ack] input_tag: {input_tag}")

        return input_tag

    def expect_user_express_opinion(self):
        return self._is_last_response_asking_opinion() or self._is_in_expecting_opinion_state()

    def _is_last_response_asking_opinion(self):
        regex_ask_opinion = r"(what do you think|What are your thoughts|any thought|Can you believe that|don't you think|isn't that interesting|do you agree)"
        regex_present_fact = r"(Did you know that|here's something interesting|find this interesting|just learned|I just heard about this|this fact)"
        regex_tell_more = r"(tell me more)"

        all = rf".*\b({regex_ask_opinion}|{regex_present_fact}|{regex_tell_more})"

        match = re.search(all, self.last_response, flags=re.IGNORECASE)

        logging.info(f"[ack] last response : {self.last_response}")

        logger.info(f"[ack] is_last_response_ask_opinion: {match}")
        return match is not None

    def _is_in_expecting_opinion_state(self):
        expect_opinion = getattr(self.user_attributes, "expect_opinion", False)
        return expect_opinion

    @property
    def get_sentiment(self):
        # For now, we will use returnnlp's first element, sentiment
        if len(self.user_utterance_features) == 0:
            return ""
        sentiment = self.user_utterance_features.get("sentiment")
        if sentiment == "neu":
            return ""
        return sentiment

    def generate_acknowledgement(self):
        """Generate Acknowledgement

        Return:
          acknowledgement field:
            - tag:  dictionary of metadata
            - response: acknowledgement response
            - case: user response type detected
        """

        if not self.returnnlp:
            return {}

        # result = {"ack": "", "output_tag": "", "source": ""}

        # todo: remove comment when it's ready
        if self.is_opinion_comment_statement():

            logger.info("[ack] try acknowledge comment with blender")
            ack_from_blender = self.__retrieve_acknowledgement_from_blender()
            print(f"Ack from blender: {ack_from_blender}")

            if ack_from_blender and len(ack_from_blender) > 1:
                result = {"ack": ack_from_blender, "output_tag": LocalOutputTag.ACK_OPINION, "source": "blender"}
                print(f"System acknowledgement: {result}")
                return result


        ack_from_local = self.__retrieve_acknowledgement_from_local()

        if ack_from_local.get("ack", "") != "":
            self.logger.info("[ACK] Retrieved response from local: {}".format(ack_from_local))
            result = ack_from_local
        else:
            ack_from_server = self.__retrieve_acknowledgement_from_server()
            result = ack_from_server

        result = self._convert_output_tag(result)

        self.logger.info("[ack generator]: final ack = {}".format(result))

        print(f"System acknowledgement: {result}")
        return result

    def is_opinion_comment_statement(self):
        return self.returnnlp.has_dialog_act([DialogAct.OPINION, DialogAct.COMMENT, DialogAct.STATEMENT], threshold=0.4)

    def _convert_output_tag(self, ack):
        logging.info(f"[ack] _convert_output_tag: before: ack = {ack}")

        input_tag = self.get_input_tag

        if input_tag in [LastResponseTag.ASK_HOBBIES, LastResponseTag.ASK_EVENT]:
            ack["output_tag"] = LocalOutputTag.ACK_HOBBIES
        elif input_tag in [LastResponseTag.ASK_WHAT_TO_TALK]:
            ack["output_tag"] = LocalOutputTag.ACK_WHAT_TO_TALK
        elif input_tag == LastResponseTag.EXPECT_OPINION:
            ack["output_tag"] = LocalOutputTag.ACK_OPINION
        logging.info(f"[ack] _convert_output_tag: after: ack = {ack}")

        return ack

    ##########################
    # Local acknowledgement
    ##########################

    def __retrieve_acknowledgement_from_local(self) -> Dict[str, str]:
        local_acknowledgements = []
        response = ""
        output_tag = ""

        for local_ack in self.locally_generated_ack:
            if local_ack is not None:
                template_key = local_ack.get("key", "")
                template_slot = local_ack.get("slots", {})
                tag = local_ack.get("tag", "")
                local_acknowledgements.append(
                    [self.__get_template(template_key, template_slot), tag])

        self.logger.info(f"[self.get_input_tag] {self.get_input_tag}")

        # Get the first positive acknowledgement
        if len(local_acknowledgements) > 0:
            local_acknowledgement = local_acknowledgements[0]

            response = local_acknowledgement[0]
            output_tag = local_acknowledgement[1]

        elif self.returnnlp[0].has_dialog_act(DialogActEnum.COMMANDS) and \
                    set(self.returnnlp.sys_intent[0]).intersection(["change_topic", "req_topic_jump", "get_off_topic"]):
                response = " "
        elif self.get_input_tag in [LastResponseTag.ASK_HOBBIES, LastResponseTag.ASK_EVENT, LastResponseTag.ASK_WHAT_TO_TALK]:
            if self.returnnlp[0].has_dialog_act(DialogActEnum.NEG_ANSWER) or self.user_utterance in ["nothing"]:
                response = "That's okay. "
            elif len(self.returnnlp) == 1 and self.returnnlp[0].has_dialog_act(DialogActEnum.POS_ANSWER):
                response = " "  # hacky way to avoid sending request to server later
            elif len(self.returnnlp) == 1 and self.returnnlp[0].has_dialog_act(DialogActEnum.BACK_CHANNELING):
                response = " "  # hacky way to avoid sending request to server later
        else:
            response = ""
            output_tag = ""

        return {"ack": response, "output_tag": output_tag}

    def __get_template(self, key, slots=None):
        if self.get_self_disclosure_level() == "emotional":
            template = template_manager.Templates.acknowledgement_emotional
        else:
            template = template_manager.Templates.acknowledgement

        if not slots:
            slots = {}
        return template.utterance(
            selector=key,
            slots=slots,
            user_attributes_ref=self.user_attributes)

    @property
    def __generate_ack_if_user_positive_opinion(self):
        ack_generator = PositiveOpinionAcknowledgementGenerator(self.__get_returnnlp_raw())
        return ack_generator.generate_ack()

    def __end_with_question(self):
        self.returnnlp.has_dialog_act([DialogActEnum.OPEN_QUESTION,
                                       DialogActEnum.OPEN_QUESTION_FACTUAL,
                                       DialogActEnum.OPEN_QUESTION_OPINION,
                                       DialogActEnum.YES_NO_QUESTION], -1)

    @property
    def __generate_ack_if_user_appreciation(self):
        threshold = 0.5
        if len(self.returnnlp) == 1 and self.returnnlp.has_dialog_act(DialogActEnum.APPRECIATION, threshold=threshold):
            self.logger.info("[ACK] System Acknowledgement detected appreciation")
            return {"key": "user_appreciation", "slots": {},  "tag" : "appreciation"}
        else:
            return None


    @property
    def __generate_ack_if_user_opinion_crazy(self):
        if self.returnnlp[-1].has_intent("opinion_crazy"):
            return {"key": "user_opinion_crazy", "slots": {},  "tag": "opinion_crazy"}
        else:
            return None

    @property
    def __generate_ack_if_user_opinion_surprising(self):
        if self.returnnlp[-1].has_intent("opinion_surprising"):
            return {"key": "user_opinion_surprising", "slots": {},  "tag": "opinion_surprising"}
        else:
            return None

    @property
    def __generate_ack_if_user_apology(self):
        if self.returnnlp.has_dialog_act(DialogActEnum.APOLOGY):
            self.logger.info("[ACK] System Acknowledgement detected apology")

            return {"key": "user_apology", "slots": {}, "tag": "apology"}
        else:
            return None

    @property
    def __generate_ack_if_user_thanking(self):
        threshold = 0.80

        if len(self.returnnlp) == 1 and self.returnnlp.has_dialog_act(DialogActEnum.THANKING, threshold=threshold):
            self.logger.info("[ACK] System Acknowledgement detected thanking")

            return {"key": "user_thanking", "slots": {}, "tag": "thanking"}
        else:
            return None

    @property
    def __generate_ack_if_user_complaint(self):
        intent_classifier = IntentClassifier(self.state_manager.current_state.text)
        intent_match = intent_classifier.is_complaint()
        dialog_act_match = len(self.returnnlp) == 1 and self.returnnlp.has_dialog_act(DialogActEnum.COMPLAINT, threshold=0.9)

        if intent_match or dialog_act_match and self.user_utterance != "what":
            self.logger.info("[ACK] System Acknowledgement detected complaint")

            return {"key": "user_complaint", "slots": {}, "tag": "complaint"}
        return None

    @property
    def __generate_ack_if_user_dont_know(self):
        intent_classifier = IntentClassifier(self.state_manager.current_state.text)
        if intent_classifier.is_not_sure():
            return {"key": "user_dont_know", "slots": {}, "tag": "not_sure"}
        return None

    @property
    def __generate_ack_if_user_made_statement(self):
        # threshold = 0.90
        # dialog_act_true = in_dialog_act("statement",
        #                                 self.state_manager.current_state.features, threshold=threshold)
        # if dialog_act_true:
        #     self.logger.info("[ACK] System Acknowledgement detected statement")
        #
        #     return {"slots": {}, "tag": "statement"}
        # else:
        #     return None
        return None

    def __get_dialog_act(self):
        # TODO: use returnnlp utils
        empty_dialog_act = ["", 0.0]
        if self.__get_returnnlp_raw() is None or len(self.__get_returnnlp_raw()) == 0:
            return empty_dialog_act
        return self.__get_returnnlp_raw()[0].get("dialog_act", empty_dialog_act)

    def __get_custom_intents(self):
        """Wrapper to call intents
        """
        return self.user_utterance_features.get("intent_classify")


    ##########################
    # Acknowledgement Server
    ##########################
    @trace_latency
    def __retrieve_acknowledgement_from_server(self) -> Dict[str, str]:
        """Input Tag
        """
        data = self._construct_server_input()

        if data is None:
            return {"ack": "", "output_tag": ""}

        headers = {
            'Content-Type': 'application/json'
        }
        query_json = json.dumps(data)
        try:
            response = requests.post(url=ACKNOWLEDGEMENT_URL,
                                     headers=headers,
                                     data=query_json,
                                     timeout=0.5).json()

            if "tag" in response:
                response["output_tag"] = response["tag"]
                del response["tag"]
            else:
                response["output_tag"] = ""

            if "text" in response:
                response["ack"] = response["text"]
                del response["text"]

        except Exception as _e:
            self.logger.error("[ACK] REtrieve from Server Failed:  {}".format(_e), exc_info=True)
            response = {"ack": "", "output_tag": ""}

        self.logger.debug("[Server response]: {}".format(response))

        # Fix weird format from ack server
        response["ack"] = AcknowledgementGenerator._post_process_server_ack_format(response["ack"])

        # profanity check
        ack_contains_profanity = BotResponseProfanityClassifier.is_profane(response["ack"])
        if ack_contains_profanity:
            response["ack"] = error_template.utterance(
                selector="profanity_req", slots={}, user_attributes_ref=self.user_attributes)

        return response

    @staticmethod
    def _post_process_server_ack_format(ack: str):
        logging.info("[ACK] _post_process_ack: {}".format(ack))
        if ack:
            ack = ack \
                .replace(' \' ', " ") \
                .replace(' \'s', '\'s')

            NO_NEED_TO_ADD_PERIOD = [">", ".", "!", "?"]
            last_char = ack.rstrip()[-1]

            CHAR_THAT_SHOULD_NOT_BE_IN_THE_END = ["'"]
            if last_char in CHAR_THAT_SHOULD_NOT_BE_IN_THE_END:
                ack = ack[:-1]  # remove last word

            if last_char not in NO_NEED_TO_ADD_PERIOD:
                ack = ack.rstrip() + ". "

            if last_char == ".":
                ack = ack + " "

        return ack


    def _construct_server_input(self):
        dialog_act = ["", 0.0]

        server_tag_map = {
            LastResponseTag.ASK_HOBBIES: AckServerInputTag.ASK_HOBBIES,
            LastResponseTag.ASK_EVENT: AckServerInputTag.ASK_GENERAL,
            LastResponseTag.ASK_WHAT_TO_TALK: AckServerInputTag.ASK_GENERAL
        }

        # Add Last response field
        input_tag = self.get_input_tag
        server_tag = server_tag_map.get(input_tag, None)

        if input_tag is None:
            return None

        try:
            last_response = {"text": self.last_response, "tag": server_tag}
            dialog_act = self.__get_dialog_act()
            return {
                "last_response": last_response,
                "sentiment": self.get_sentiment,
                "user_utterance": self.user_utterance,
                "segmented_features": self.__get_returnnlp_raw(),
                "dialog_act": dialog_act
            }
        except:
            return {
                "last_response":
                    {"text": self.last_response,
                     "tag": server_tag},
                "sentiment": "",
                "dialog_act": dialog_act,
                "user_utterance": self.user_utterance_features.get("text", ""),
                "segmented_features": self.__get_returnnlp_raw()
            }


    ##########################
    # Blender
    ##########################
    @trace_latency
    def __retrieve_acknowledgement_from_blender(self):
        result = self.blender_generator.generate_response()

        if self.get_self_disclosure_level() == "factual":
            if re.search(
                    "|".join([
                        r"\b(agree|think|thought|like|love|prefer|great|good)",
                        r"\b(feel|sad|happy|glad|sorry)\b",
                    ]),
                    self.user_utterance):
                return ""
        elif self.get_self_disclosure_level() == "cognitive":
            if re.search(
                    "|".join([
                        r"\b(feel|sad|happy|glad|sorry)\b",
                    ]),
                    self.user_utterance):
                return ""
        else:
            return result


    def blender_contains_non_stop_words_in_user_utterance(self, blender_response):
        user_utterance_non_stop_tokens = self.get_nonstop_tokens(self.user_utterance)
        for token in user_utterance_non_stop_tokens:
            if token in self.get_tokens(blender_response):
                return True
        return False

    def blender_contains_key_pyrase_in_user_utterance(self, blender_response):
        for phrase in self.get_user_utterance_key_phrases():
            if phrase in self.get_tokens(blender_response):
                return True

    def get_user_utterance_key_phrases(self) -> List:
        return list(itertools.chain.from_iterable(self.returnnlp.key_phrase))

    @staticmethod
    def get_tokens(text: str):
        return [s.translate(str.maketrans('', '', string.punctuation)) for s in text.split()]

    @staticmethod
    def get_nonstop_tokens(text: str):
        return [w.translate(str.maketrans('', '', string.punctuation)) for w in text.split() if
                not w.lower() in stopwords]



