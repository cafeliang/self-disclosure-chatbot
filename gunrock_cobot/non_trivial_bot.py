import os
import sys
import re

file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)

import cobot_core as Cobot  # noqa
from typing import *  # noqa
from cobot_core import ABTest  # noqa

# DialogManager dependency
from cobot_core.log.logger import LoggerFactory  # noqa
from cobot_core.nlp.pipeline import NLPPipeline  # noqa

from cobot_core.offensive_speech_classifier import OffensiveSpeechClassifier  # noqa
from cobot_core.output_responses_converter import OutputResponsesConverter  # noqa
###
from cobot_core.state_manager import StateManager  # noqa
from cobot_core.ranking_strategy import RankingStrategy  # noqa
from cobot_core.asr_processor import ASRProcessor  # noqa
from cobot_core.nlp.pipeline import ResponseGeneratorsRunner  # noqa
from cobot_core.selecting_strategy import SelectingStrategy  # noqa
from cobot_core.global_intent_handler import GlobalIntentHandler  # noqa
from cobot_core.feature_extractor import FeatureExtractor  # noqa
from cobot_core.dialog_manager import DialogManager  # noqa
from cobot_core.service_module_manager import ServiceModuleManager  # noqa
from cobot_core.prompt_constants import Prompt  # noqa
from cobot_core.response_builder import RepromptBuilder  # noqa
from cobot_python_sdk.analyzer import Analyzer  # noqa
from cobot_python_sdk.event import Event  # noqa
from cobot_python_sdk.user_attributes import UserAttributes  # noqa
from cobot_python_sdk.user_attributes_manager import UserAttributesManager  # noqa
# from ranking_strategy.ranking_strategy import *

from utils import _previous_repeat_requests_count, set_logger_level, print_log_for_local_env, \
    save_template_keys_to_current_state_and_reset, process_prosody, get_template_keys, set_ask_open_question_tag, \
    get_proposed_topic, save_proposed_topic_to_current_state, save_qa_response_to_current_state_and_reset
from nlu.nlp_modules import *  # noqa
from nlu.intent_classifier import *  # noqa
from nlu.dataclass.returnnlp import *  # noqa
from nlu.constants import TopicModule
# from response_generator.dialog_modules import *  # noqa
from selecting_strategy.custom_selecting_strategy import *
from selecting_strategy.module_selection import ModuleSelector, GlobalState
from custom_asr_processor import CustomASRProcessor
from nlg_post_process.response_content_post_processor import ResponseContentPostProcessor
from nlg_post_process.response_format_post_processor import ResponseFormatProcessor
from nlg_post_process.response_prosody_post_processor import ResponseProsodyProcessor
from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier
from acknowledgement.acknowledgement_generator import AcknowledgementGenerator
from template_manager.manager import Template, TemplateManager
from user_initializer import UserInitializer
from user_profiler.user_profile import UserProfile
from aws_xray.aws_xray_config import config as configure_xray
from aws_xray.register_segments import register_segments
from response_templates.special_template_keys import open_question_ask_hobbies_templates, \
    open_question_general_templates, open_question_event_templates
import template_manager  # noqa
from constants_api_keys import COBOT_API_KEY
from nlu.server_urls import BLENDER_QUESTION_HANDLING, BLENDER_RETRIEVAL
from selecting_strategy.topic_mapping import AMAZON_TOPIC_MAP_2018

from response_templates.special_template_keys import open_question_ask_hobbies_templates, \
    open_question_general_templates, open_question_event_templates, open_question_from_dailylife_tempaltes

from self_disclosure_study.selecting_strategy import SelfDisclosureSelectingStrategy
from self_disclosure_study.dialog_manager import  SelfDisclosureDialogManager
from self_disclosure_study.dialog_modules import SelfDisclosureDialogModules

configure_xray()
register_segments()

error_template = template_manager.Templates.error
template = template_manager.Templates.sys_rg
APP_ID = 'amzn1.ask.skill.b076f5cb-f4e6-4e47-a75e-e6a723f63533'

logger = logging.getLogger(__name__)

set_logger_level(utils.get_working_environment())

from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core import patch_all

patch_all()
class CustomGlobalIntentHandler(GlobalIntentHandler):
    """
    Handle mandatory global intents, such as AMAZON.StopIntent, as well as special handling logic for LaunchRequest
    """

    @inject
    def __init__(self, state_manager: StateManager):
        self.state_manager = state_manager

    def execute(self, event) -> (str, bool):
        request_type = self.state_manager.current_state.request_type
        intent = self.state_manager.current_state.intent

        logger.info("[GI] start global intent with event: {}, request_type: {}, intent: {}".format(
            event, request_type, intent))

        output, should_end_session = None, False

        if request_type == 'LaunchRequest':
            output, should_end_session = self.handle_launch_request()

        if request_type == 'IntentRequest':
            if intent in ['AMAZON.StopIntent']:
                output, should_end_session = self.handle_stop_intent()
            else:
                self.add_last_state_attributes()

        if request_type == 'SessionEndedRequest':
            output, should_end_session = self.handle_session_end_request()


        logger.info("[GI] output with output: {}, should end session: {}".format(
            output, should_end_session))

        setattr(self.state_manager.user_attributes, "session_id", self.state_manager.current_state.session_id)
        setattr(self.state_manager.user_attributes, "a_b_test", getattr(self.state_manager.current_state, "a_b_test", ""))

        user_profile = UserProfile(self.state_manager.user_attributes)
        user_profile.add_total_turn_count()

        processor = ResponseProsodyProcessor(output, getattr(self.state_manager.current_state, "a_b_test", ""))
        processor.process()
        output = processor.response

        return output, should_end_session

    def add_last_state_attributes(self):
        last_state = self.state_manager.last_state
        if last_state:
            last_turn_creation_date_time = self.state_manager.last_state.get("creation_date_time", "")

            # Set last turn text and response to current state for analysis purpose
            setattr(self.state_manager.current_state, "last_text", last_state.get('text', ""))
            setattr(self.state_manager.current_state, "last_response", last_state.get('response', ""))
            setattr(self.state_manager.current_state, "last_template_keys", last_state.get("template_keys", None))

    def last_turn_was_long_time_ago(self, last_turn_creation_date_time):
        THRESHOLD_IN_SECONDS = 600
        last_turn_creation_date_time = utils.parse_creation_date_time(last_turn_creation_date_time)
        time_diff_from_now = utils.time_diff_from_now(last_turn_creation_date_time)
        logger.info(f"[GI] time_diff_from_now: {time_diff_from_now.seconds}")
        return utils.more_than_mins(time_diff_from_now, THRESHOLD_IN_SECONDS)

    def handle_stop_intent(self):
        logger.info("[GI] request type sesssion amzn stop intent")
        output = Prompt.goodbye_prompt
        should_end_session = True
        return output, should_end_session

    def handle_session_end_request(self):
        logger.info("[GI] request type sesssion end req")
        output = Prompt.goodbye_prompt
        should_end_session = True
        return output, should_end_session

    def handle_launch_request(self):
        # output = Prompt.welcome_prompt
        should_end_session = False

        user_initializer = UserInitializer(self.state_manager)

        if user_initializer.is_new_user():
            logging.info("[User Initializer] is new user")
            output = "Hi, it's nice talking to you! "

            user_initializer.init_for_new_user()
        else:
            logging.info("[User Initializer] is returning user")
            user_initializer.init_for_returning_user()
            output = "Hi, it's nice to talk to you again! "

        return output, should_end_session


class CustomFeatureExtractor(FeatureExtractor):
    """
    Extract intent, ner, sentiment, topic, etc features from inputs
    """

    @inject
    def __init__(self,
                 event: Event,
                 attributes: UserAttributes,
                 user_attributes_manager: UserAttributesManager,
                 state_manager: StateManager,
                 nlp_pipeline: NLPPipeline,
                 service_module_manager: ServiceModuleManager):
        super().__init__(event=event, attributes=attributes,
                         user_attributes_manager=user_attributes_manager)
        self.state_manager = state_manager
        self.nlp_pipeline = nlp_pipeline
        self.service_module_manager = service_module_manager
        self.logger = LoggerFactory.setup(self)

    def analyze(self) -> Optional[Dict]:
        self.logger.info('[FE] start analyzing feature extraction...')
        self.preprocess_text()

        self.query_blender()

        self.nlp_pipeline.run()

        logger.debug('[FE] get dynamo attribute: {}'.format(self._attributes))
        logger.debug('[FE] user attribute before merge: {}'.format(
            self.state_manager.user_attributes))
        self.state_manager.user_attributes = self._attributes
        logger.debug('[FE] user attribute after merge: {}'.format(
            self.state_manager.user_attributes))
        self.state_manager.user_attributes = self._attributes
        setattr(self.state_manager.user_attributes,
                "sys_env", utils.get_working_environment())


        if self.state_manager.user_attributes is not None:
            if not getattr(self.state_manager.user_attributes, 'prev_hash', None):
                setattr(self.state_manager.user_attributes, 'prev_hash', {})
            self.state_manager.user_attributes.prev_hash[
                "sys_env"] = utils.get_working_environment()

        pipeline_names = self.service_module_manager.get_nlp_pipeline_names()
        logger.debug('[FE] pipelines before dedup: {}'.format(pipeline_names))

        # 'intent' feature is used for some selecting strategy in the dialog manager, so always add 'intent' to features
        unique_pipeline_names = set(pipeline_names)
        unique_pipeline_names.add('intent')
        unique_pipeline_names.add('text')

        features = {}
        for pipeline_name in unique_pipeline_names:
            features[pipeline_name] = getattr(
                self.state_manager.current_state, pipeline_name, None)

        self.write_topic_to_feature_and_state_manager(features)
        self.write_knowledge_and_noun_phrase_to_features(features)
        self.write_concept_to_feature(features)

        self.state_manager.current_state.features = features

        if "sentence_completion_text" in features and features['sentence_completion_text']:
            self.state_manager.current_state.set_new_field("sentence_completion_text", features['sentence_completion_text'])
        self.logger.info('[FE] return feature is: {}'.format(features))
        # print(json.dumps(features,indent=4))
        return features

    def preprocess_text(self):
        # Remove wake words from user utterance
        current_text = self.state_manager.current_state.text
        if current_text and len(current_text.split()) > 1:
            text = re.sub(r"\balexa\b|\becho\b", "", current_text).strip()
            text = re.sub(r"open gunrock and", "", text).strip()
            self.state_manager.current_state.text = text

    def query_blender(self):
        session_id = getattr(self.state_manager.current_state, "session_id", "")

        if not session_id:
            return

        last_state = self.state_manager.last_state
        last_input_text = last_state.get('text', "") if last_state else ""
        last_response = last_state.get('response', None) if last_state else ""
        current_text = getattr(self.state_manager.current_state, "text", "")

        request_type = self.state_manager.current_state.request_type

        if request_type != "LaunchRequest" and current_text:
            user_profile = UserProfile(self.state_manager.user_attributes)
            key = "{}_{}".format(session_id, user_profile.total_turn_count)
            request_data = {
                "last_turn_text": last_input_text,
                "last_turn_response": last_response,
                "text": current_text,
                "blender_id": key
            }

            self.logger.info("[Blender] send query: {}".format(request_data))

            ab_setting = getattr(self.state_manager.current_state, "a_b_test", None)

            setattr(self.state_manager.user_attributes, "blender_start_time", time.time())

            try:
                requests.post(url=BLENDER_QUESTION_HANDLING, json=request_data, timeout=(0.1, 0.001))
            except Exception as e:
                logger.info("[blender] question handling request error: {}".format(e))

            try:
                requests.post(url=BLENDER_RETRIEVAL, json=request_data, timeout=(0.1, 0.001))
            except Exception as e:
                logger.info("[blender] retrieval request error: {}".format(e))

    def write_topic_to_feature_and_state_manager(self, features):
        if features.get('topic') and features['topic'][0]:
            raw_topic = features['topic'][0]
            topic = ReturnNLPSegment.Topic.from_dict(raw_topic)
            topic = topic.get_filtered_topic()

            topic_class = topic.topic_class if topic else ''
            self.state_manager.current_state.set_new_field("topic_class", topic_class)

            topic_module = AMAZON_TOPIC_MAP_2018[topic.topic_class] if topic else ''
            features['topic_module'] = topic_module

            topic_keywords = raw_topic['topicKeywords'] if topic else ''
            features['topic_keywords'] = topic_keywords
        else:
            self.state_manager.current_state.set_new_field("topic_class", '')
            features['topic_module'] = ''
            features['topic_keywords'] = ''


    def write_concept_to_feature(self, features):
        # NOTE: save concept to be compatible with the old design
        if 'concept' in features and features['concept'] is not None:
            one_concept = []
            for concepts in features['concept']:
                for concept in concepts:
                    one_concept.append(concept)
            features['concept'] = one_concept

    def write_knowledge_and_noun_phrase_to_features(self, features):
        # NOTE: save to keep backward compatible

        if 'npknowledge' in features and features['npknowledge'] is not None:
            features['knowledge'] = features['npknowledge']['knowledge']
            features['noun_phrase'] = features['npknowledge']['noun_phrase']
        else:
            features['knowledge'] = []
            features['noun_phrase'] = []

# <-------- Custom NLP Pipeline Class --------> #
class CustomDialogManager(DialogManager):
    """
    The default DialogManager class implements a generic flow for response generation in a Cobot implementation.

    The generic flow consists of a response mode selecting strategy, followed by high performance parallel invocation of
    all candidate response generator modules (which may be simple locally-hosted, rule-driven modules, complex retrieval
    systems or generative models hosted in long=lived independent Docker modules, or combinations of both). Finally,
    the list of candidate responses are filtered for offensive content, the best-ranked response is chosen and returned to
    the Handler for packaging and delivery to the Alexa Skills Kit API.

    **IMPORTANT NOTE**
    In many cases, a custom Cobot can be implemented with the default DialogManager flow, simply by injecting a specific
    SelectingStrategy and/or RankingStrategy and implementing and registering appropriate ResponseGenerators. However, it
    is also possible to override the DialogManager itself if, for example, a unified selection and ranking strategy is
    desired that is not amenable to implementation in the default flow.
    """
    # TODO: better mgmnt

    @inject
    def __init__(self,
                 state_manager: StateManager,
                 selecting_strategy: SelectingStrategy,
                 ranking_strategy: RankingStrategy,
                 offensive_speech_classifier: OffensiveSpeechClassifier,
                 output_responses_converter: OutputResponsesConverter,
                 response_generator_runner: ResponseGeneratorsRunner
                 ) -> None:
        self.state_manager = state_manager
        self.selecting_strategy = selecting_strategy
        self.ranking_strategy = ranking_strategy
        self.offensive_speech_classifier = offensive_speech_classifier
        self.output_responses_converter = output_responses_converter
        self.response_generator_runner = response_generator_runner
        self.logger = LoggerFactory.setup(self)
        self.cur_state = self.state_manager.current_state
        self.module_selector = ModuleSelector(self.state_manager.user_attributes)
        self.returnnlp = None

    def select_response(self, features):
        # type: (Dict[str,Any]) -> (str, bool)
        """
        Runs the entire dialog pipeline. In the case of the default DialogManager,
        this uses the currently configured SelectingStrategy, runs the resulting ResponseGenerators,
        and uses the configured RankingStrategy to choose the best candidate response.

        :param features:    optional dict, with a subset of current_state from state_manager,
                            to pass into the selecting_strategy
        """

        if features is None:
            features = self.state_manager.current_state.features

        self.returnnlp = ReturnNLP.from_list(features.get("returnnlp", []))

        try:
            result = self._dialog_manager_handler(features)
            return result
        except Exception as e:
            logging.error('[DM] catch error: {}'.format(e), exc_info=True)
            setattr(self.state_manager.current_state, "resp_type", "sys_crash")

            if utils.get_working_environment() == "local":
                response = "CRASH - SYSTEM LEVEL CRASH"
            else:
                response = error_template.utterance(selector='sys_crash', slots={},
                                                user_attributes_ref=self.state_manager.user_attributes)

            save_template_keys_to_current_state_and_reset(self.state_manager)

            return response, False

    def _dialog_manager_handler(self, features):
        dm_start = time.time()
        should_end_session = False

        self.print_features_for_local_env(features)

        response_in_preprossessing = self.preprocess(features)
        if response_in_preprossessing:
            self.logger.debug("[DM] response handled by preprocess")
            save_template_keys_to_current_state_and_reset(self.state_manager)
            return response_in_preprossessing, should_end_session

        self.update_user_profile()


        response_generators = self.selecting_strategy.select_response_mode(features)
        response_generator_end = time.time()
        self.logger.info('[DM] All selected RGs: {}, with latency: {}ms'.format(
            response_generators, (response_generator_end - dm_start) * 1000))

        self.try_generate_acknowledgement(features)

        self.set_fields_after_selecting_module(response_generators)

        self.print_user_attributes_for_local_env("user attributes (before entering topic module)")

        output_responses_dict, responses_list = self.generate_responses(response_generators)
        self.logger.info(f"output_responses_dict: {output_responses_dict}")

        self.set_user_profile_to_state_manager()

        if responses_list:
            top_ranked_module, top_ranked_response_text = self.rank(output_responses_dict, responses_list)
        else:
            top_ranked_module, top_ranked_response_text = response_generators[0], []

        self.clear_tunnel(top_ranked_module)

        content_post_processor = ResponseContentPostProcessor(
            self.state_manager, top_ranked_module, top_ranked_response_text)
        response = content_post_processor.process()

        response = ResponseFormatProcessor(response).process()

        response = ResponseProsodyProcessor(response,
                                            getattr(self.state_manager.current_state, "a_b_test", ""),
                                            response_generators[0]).process()

        module_selector = ModuleSelector(self.state_manager.user_attributes)
        self.logger.info(f"global state end: {module_selector.global_state}")
        self.print_user_attributes_for_local_env("user attributes (in the end)")

        self.logger.info(f"top ranked module: {top_ranked_module}")
        self.logger.info(f"propose continue: {module_selector.get_propose_continue(top_ranked_module)}")


        set_open_question_tag_to_user_attribute(self.state_manager)
        save_template_keys_to_current_state_and_reset(self.state_manager)
        save_proposed_topic_to_current_state(self.state_manager)
        save_qa_response_to_current_state_and_reset(self.state_manager)

        dm_end = time.time()
        self.logger.info("[DM] final response: {}, latency: {} ms, should end session: {}".format(
            response, (dm_end - dm_start) * 1000, should_end_session))
        return response, should_end_session

    def set_fields_after_selecting_module(self, response_generators):
        current_selected_module = response_generators[0]
        if current_selected_module not in ModuleSelector.interruption_modules:
            module_selector = ModuleSelector(self.state_manager.user_attributes)

            module_selector.propose_topic = None  # reset propose topic

            # Reset open question keys
            if self.state_manager.user_attributes.social_open_question_proposing == True:
                setattr(self.state_manager.user_attributes, "last_turn_propose_open_question", True)
            else:
                setattr(self.state_manager.user_attributes, "last_turn_propose_open_question", None)
            setattr(self.state_manager.user_attributes, "social_open_question_proposing", None)

            # Reset expect opinion key
            setattr(self.state_manager.user_attributes, "expect_opinion", False)

        self.module_selector.current_selected_module = TopicModule(current_selected_module)

        self.state_manager.current_state.set_new_field("selected_modules", response_generators)
        self.state_manager.current_state.set_new_field("propose_keywords", self.module_selector.propose_keywords)
        self.state_manager.current_state.set_new_field("global_state", self.module_selector.global_state.value)

        # Reset global state and propose keywords
        if self.module_selector.current_selected_module not in ModuleSelector.interruption_modules:
            last_module = self.module_selector.last_module
            if self.module_selector.current_selected_module.value == last_module:
                self.module_selector.global_state = GlobalState.NA
                self.module_selector.propose_keywords = None

        print("[DEBUG] selected module: {}".format(self.module_selector.current_selected_module))


    def get_user_profile(self):
        return UserProfile(self.state_manager.user_attributes)

    def update_user_profile(self):
        user_profile = self.get_user_profile()

        if self.is_dominant_turn():
            user_profile.add_dominant_turn_count()

    def is_dominant_turn(self):
        is_dominant_turn = self.returnnlp.has_dialog_act(
            dialog_act=
            [DialogActEnum.YES_NO_QUESTION,
             DialogActEnum.OPEN_QUESTION,
             DialogActEnum.OPEN_QUESTION_OPINION,
             DialogActEnum.OPEN_QUESTION_FACTUAL,
             DialogActEnum.COMMANDS,
             DialogActEnum.OPINION,
             DialogActEnum.COMMENT,
             DialogActEnum.STATEMENT],
            threshold=0.4)

        self.logger.debug(f"is_dominant_turn: {is_dominant_turn}")
        return is_dominant_turn

    def set_user_profile_to_state_manager(self):
        user_profile = UserProfile(self.state_manager.user_attributes)
        name = user_profile.username
        predicted_gender = user_profile.gender if name else ""
        dominant_turn_ratio = user_profile.dominant_turn_ratio

        setattr(self.state_manager.current_state, "user_name", name)
        setattr(self.state_manager.current_state, "user_gender_prediction", predicted_gender)
        setattr(self.state_manager.current_state, "dominant_turn_ratio", dominant_turn_ratio)


    def try_generate_acknowledgement(self, features):
        acknowledgement = AcknowledgementGenerator(self.state_manager, features).generate_acknowledgement()
        setattr(self.state_manager.current_state, "system_acknowledgement", acknowledgement)

    def generate_responses(self, response_generators):
        start = time.time()
        output_responses_dict = self.response_generator_runner.run(response_generators)
        responses_list, converted_output_responses_dict = self.output_responses_converter.convert(
            output_responses_dict)
        setattr(self.state_manager.current_state,
                'candidate_responses', converted_output_responses_dict)

        end = time.time()
        self.logger.info('[DM] Candidate Responses: {}, latency: {}'.format(
            converted_output_responses_dict, (end - start) * 1000))
        return converted_output_responses_dict, responses_list

    def get_empty_response(self):
        response = error_template.utterance(selector='empty_response', slots={},
                                            user_attributes_ref=self.state_manager.user_attributes)
        return response


    def preprocess(self, features):
        is_profanity = features.get("profanity_check", False)
        if is_profanity:
            logging.info(' profanity results": {}'.format(features['text']))
            setattr(self.state_manager.current_state, "resp_type", "profanity_request")
            response = error_template.utterance(selector='profanity_req', slots={},
                                                user_attributes_ref=self.state_manager.user_attributes)
            save_template_keys_to_current_state_and_reset(self.state_manager)
            print_log_for_local_env(
                "Response handled by  Dialog Manager Preprocessor - profanity utterance handler")
            return response

        repeat_requests = _previous_repeat_requests_count(self.state_manager.session_history)
        if self.returnnlp and self.returnnlp.is_incomplete_utterance() and repeat_requests < 2:
            setattr(self.state_manager.current_state,
                    "resp_type", "incomplete")
            print_log_for_local_env(
                "Response handled by Dialog Manager Preprocessor - incomplete utterance handler")
            return template.utterance(selector=['preprocess', 'not_complete'], slots={},
                                      user_attributes_ref=self.state_manager.user_attributes)

        if self.returnnlp and self.returnnlp.is_hesitant():
            setattr(self.state_manager.current_state, "resp_type", "hesitant")
            print_log_for_local_env(
                "Response handled by Dialog Manager Preprocessor - hesitant utterance handler")

            return template.utterance(selector=['preprocess', 'hesitate'], slots={},
                                      user_attributes_ref=self.state_manager.user_attributes)
        return None

    def convert_responses(self, output_responses_dict: dict, response_list: List[str]) -> dict:
        self.logger.info("[DM] start generate ranking strategy input...")
        result = {}
        for response_generator_name, response in output_responses_dict.items():
            if isinstance(response, list):
                new_response = [
                    elem for elem in response if elem in response_list]
                result[response_generator_name] = new_response
            else:
                if response in response_list:
                    result[response_generator_name] = response
        self.logger.info(
            "[DM] generate ranking strategy output: {}".format(result))
        return result

    def generate_ranking_strategy_input(self,
                                        output_responses_dict: dict,
                                        response_list: List[str]) -> dict:
        self.logger.info("[DM] start generate ranking strategy input...")
        result = {}
        for response_generator_name, response in output_responses_dict.items():
            if isinstance(response, list):
                new_response = [
                    elem for elem in response if elem in response_list]
                result[response_generator_name] = new_response
            else:
                if response in response_list:
                    result[response_generator_name] = response
        self.logger.info(
            "[DM] generate ranking strategy output: {}".format(result))
        return result

    def rank(self, output_responses: dict, response_list: list):
        """
        :param output_responses: key: module name, value: response text
        :param response_list: list of response text
        :return: (top ranked module name, top ranked response text)
        """
        ranking_strategy_input_data = self.generate_ranking_strategy_input(
            output_responses, response_list)
        modules = list(ranking_strategy_input_data.keys())
        top_ranked_module = modules[0]
        top_ranked_response_text = ranking_strategy_input_data[
            top_ranked_module]
        return top_ranked_module, top_ranked_response_text

    def clear_tunnel(self, selected_module):
        tunnel = self.state_manager.user_attributes.tunnel
        if isinstance(tunnel, dict) and selected_module in [tunnel.get('target_module'), tunnel.get('source_module')]:
            pass
        else:
            setattr(self.state_manager.user_attributes, "tunnel", None)

    def print_selected_module(self):
        print("[DEBUG] selected module: {}".format(self.state_manager.user_attributes.cur_selected_modules))

    def print_features_for_local_env(self, features):
        print_log_for_local_env("feature: ")
        print_log_for_local_env(json.dumps(features, indent=4))

    def print_user_attributes_for_local_env(self, log_text):
        print_log_for_local_env(log_text)
        print_log_for_local_env(self.state_manager.user_attributes.serialize_to_json() + '\n')


def set_open_question_tag_to_user_attribute(state_manager):
    template_keys = get_template_keys(state_manager)

    if set(template_keys).intersection(open_question_ask_hobbies_templates.union(open_question_event_templates, open_question_general_templates, open_question_from_dailylife_tempaltes)):
        setattr(state_manager.user_attributes, "social_open_question_proposing", True)
        set_ask_open_question_tag(state_manager)

class CustomRepromptBuilder(RepromptBuilder):

    def build(self, speech_output):
        error_template_manager = TemplateManager(Template.error, self.state_manager.user_attributes)
        response = error_template_manager.speak(selector='reprompt', slots={}, _append_selector_history=False)

        logging.info("[REPROMPT] build reprompt")
        return response

# <-------- Define Cobot pipeline --------> #


def overrides(binder):
    binder.bind(Cobot.SelectingStrategy, to=SelfDisclosureSelectingStrategy)
    binder.bind(ASRProcessor, to=CustomASRProcessor)
    binder.bind(Analyzer, to=CustomFeatureExtractor)
    binder.bind(RepromptBuilder, to=CustomRepromptBuilder)
    binder.bind(Cobot.GlobalIntentHandler, to=CustomGlobalIntentHandler)
    binder.bind(Cobot.DialogManager, to=SelfDisclosureDialogManager)


def lambda_handler(event, context):
    logger.info("[LAMBDA][AlexaCobot] lambda handler input with time : {}, event: {}, context: {}".format(
        time.time(), event, context))

    STATE_TABLE_NAME, USER_TABLE_NAME = get_table_names_by_environment()

    logger.info("[LAMBDA] lambda handler user table: {}, state table {}, override: {}".format(
        USER_TABLE_NAME, STATE_TABLE_NAME, overrides))
    # dialog_modules = DialogModules()
    dialog_modules = SelfDisclosureDialogModules()
    nlp_modules = NLPModule()

    # NOTE: total ratio should be 100
    config_a = {
        'name': 'A',  # control condition
        'ratio': 0,
        'nlp_modules': nlp_modules.get_nlp_module_list(),
        'overrides': overrides,
        'response_generators': dialog_modules.get_dialog_modules()
    }

    # config_b = {
    #     'name': 'B',  # use blender in question handling
    #     'ratio': 30,
    #     'nlp_modules': nlp_modules.get_nlp_module_list(),
    #     'overrides': overrides,
    #     'response_generators': dialog_modules.get_dialog_modules()
    # }

    config_c = {
        'name': 'C',  # default
        'ratio': 100,
        'nlp_modules': nlp_modules.get_nlp_module_list(),
        'overrides': overrides,
        'response_generators': dialog_modules.get_dialog_modules()
    }

    config_d = {
        'name': 'D',  # google kg similarity v2
        'ratio': 0,
        'nlp_modules': nlp_modules.get_nlp_module_list(),
        'overrides': overrides,
        'response_generators': dialog_modules.get_dialog_modules()
    }

    config_e = {   # new low asr logic
        'name': 'E',
        'ratio': 0,
        'nlp_modules': nlp_modules.get_nlp_module_list(),
        'overrides': overrides,
        'response_generators': dialog_modules.get_dialog_modules()
    }

    ab_config = [config_c, config_e]


    abtest = ABTest(event=event,
                    context=context,
                    app_id=APP_ID,
                    user_table_name=USER_TABLE_NAME,
                    save_before_response=True,
                    state_table_name=STATE_TABLE_NAME,
                    test_case_config=ab_config,
                    level='conversation',
                    api_key=COBOT_API_KEY
                    )

    start = time.time()
    output = abtest.run()
    end = time.time()
    logger.info("[LAMBDA][AlexaCobot] lambda output with latency: {}, event: {}, context: {}, return {}".format(
        str((end - start) * 1000), event, context, output))
    return output


def get_table_names_by_environment():
    if os.environ.get('STAGE') == 'PROD':
        USER_TABLE_NAME = 'UserTableProd'
        STATE_TABLE_NAME = 'StateTableProd'
    elif os.environ.get('STAGE') == 'BETA':
        USER_TABLE_NAME = 'UserTableBeta'
        STATE_TABLE_NAME = 'StateTableBeta'
    else:
        USER_TABLE_NAME = 'UserTableDev'
        STATE_TABLE_NAME = 'StateTableDev'

    return STATE_TABLE_NAME, USER_TABLE_NAME


if __name__ == '__main__':
    xray_recorder.begin_segment("Migration Segment")
    lambda_handler(event=sample_events.new_session_event, context={})
    xray_recorder.end_segment()
