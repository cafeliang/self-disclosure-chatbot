from cobot_core.service_url_loader import ServiceURLLoader
from cobot_core.service_module import RemoteServiceModule
from response_generator.self_disclosure_study.covid_response_generator import CovidResponseGenerator
from response_generator.special.clarification_response_generator import Clarification_ResponseGenerator

class SelfDisclosureDialogModules:
    # REMOTE MODULES
    MovieTVBot = {
        'name': "MOVIECHAT",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module('MOVIECHAT'),
        'context_manager_keys': ['intent', 'slots', 'text', 'returnnlp', 'features', 'moviechat_context', 'user_id',
                                 'session_id', 'resp_type', 'a_b_test', "last_response", "bot_ask_open_question",
                                 "system_acknowledgement"],
        'input_user_attributes_keys': ['session_id', 'user_profile', 'suggest_keywords', 'last_module',
                                       'previous_modules', 'template_manager', 'moviechat_user', 'response',
                                       'module_selection', 'blender_start_time', "self-disclosure-study-abtest", "self-disclosure-study-current-level"],
        'timeout_in_millis': 3500,
        'history_turns': 1
    }

    CovidBot = {
        'name': "COVIDCHAT",
        'class': CovidResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['text', 'returnnlp', 'system_acknowledgement', "self-disclosure-study-current-level"]
    }


    def get_dialog_modules(self):
        return [
            self.MovieTVBot,
            self.CovidBot
        ]