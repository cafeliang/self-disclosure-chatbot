import sys
import os
import random

import nlu.util_nlu
import selecting_strategy.topic_module_proposal

file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)

import re
from typing import Dict, Any, List
from injector import inject
import logging

import utils
from cobot_core.state_manager import StateManager  # noqa
from cobot_core.selecting_strategy import SelectingStrategy  # noqa
from non_trivial_bot import logger
from selecting_strategy.util_selecting_strategy import getCustomModules
from selecting_strategy.module_selection import ModuleSelector
from self_disclosure_study.dialog_modules import SelfDisclosureDialogModules
from user_profiler.user_profile import UserProfile
from user_initializer import UserInitializer

utils.set_logger_level(utils.get_working_environment())


class SelfDisclosureSelectingStrategy(SelectingStrategy):
    @inject
    def __init__(self, state_manager: StateManager):
        self.state_manager = state_manager
        self.user_profile = UserProfile(self.state_manager.user_attributes)

    def select_response_mode(self, features: Dict[str, Any]) -> List[str]:
        logger.info('[SS] start selecting strategy with feature: {}', features)

        selected_module = self.get_selected_module(features)
        selected_modules = [selected_module]

        logger.info('[Selecting Strategy] Selected module: {}'.format(selected_modules))

        return selected_modules

    @staticmethod
    def get_topic_modules():
        return ['COVIDCHAT', 'MOVIECHAT']

    def get_selected_module(self, features):
        user_attributes = self.state_manager.user_attributes
        module_selector = ModuleSelector(user_attributes)


        amazon_intent = features['intent']
        logger.info(f"Amazon intent: {amazon_intent}")
        returnnlp = features['returnnlp']
        central_elem = features['central_elem']
        custom_intents = features['intent_classify']
        curr_utt = features['text'].lower()

        global_senti = nlu.util_nlu.get_sentiment(
            self.state_manager.current_state, custom_intents["lexical"])
        last_state = self.state_manager.last_state
        last_response = last_state.get('response', None)

        if self.is_launch_intent(amazon_intent):
            logging.info("is_launch_intent")
            last_module = module_selector.last_module

            if self.is_new_user():
                result = self.get_randomly_picked_module()

            # if last_module == "__EMPTY__":
            #     return "COVIDCHAT"  # todo: randomly pick between movie and covidbot
            else:
                if last_module == "MOVIECHAT":
                    result = "COVIDCHAT"
                else:
                    result = "MOVIECHAT"
        else:
            result = module_selector.last_topic_module

        module_selector.add_used_topic_module(result)

        return result

    def is_new_user(self):
        is_new_user = self.user_profile.visit == 1
        logger.info(f"is new user: {is_new_user}")
        return self.user_profile.visit == 1

    def is_launch_intent(self, amazon_intent):
        return 'LaunchRequestIntent' == amazon_intent

    def get_randomly_picked_module(self):
        # return random.choice(self.get_topic_modules())
        # TODO: change back
        return "MOVIECHAT"
        # return "COVIDCHAT"
    # def get_second_conv_module(self):




