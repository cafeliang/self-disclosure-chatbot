#!/usr/bin/env python3

import logging
import os
import sys
import argparse
from pathlib import Path
from typing import List, Tuple

os.chdir(sys.path[0])

import docker_hook  # noqa: E402
import template_manager.preprocess  # noqa: E402
import nlu.dataclass.keyword_hook  # noqa: E402

logger = logging.getLogger()
logger.setLevel(logging.INFO)
console = logging.StreamHandler()
console.setFormatter(logging.Formatter('[%(name)-8s]: %(message)s'))
logger.addHandler(console)


# check script is run in the right dir
if os.path.basename(os.getcwd()) == 'gunrock_cobot':
    working_dir = Path('.')
elif os.path.basename(os.getcwd()) == 'cobot_base':
    working_dir = Path('gunrock_cobot')
else:
    raise IOError("template_manager preprocess only works under '<git_root>' or 'gunrock_cobot/'.")

remote_modules = set(m.name for m in (working_dir).glob('docker/*')) - docker_hook.DISABLED_MODULES


def _parse_args() -> Tuple[argparse.Namespace, List[str]]:
    arg_parser = argparse.ArgumentParser()
    _add_argument(arg_parser)
    return arg_parser.parse_known_args()


def _add_argument(parser: argparse.ArgumentParser):
    parser.add_argument('-t', "--topic", type=str, nargs="*", choices=list(remote_modules), default=[])
    parser.add_argument('-e', "--exclude", type=str, nargs="*", choices=list(remote_modules), default=[])
    parser.add_argument('-l', "--local", action='store_true')


def main(flags: argparse.Namespace):
    template_manager.preprocess.main()
    nlu.dataclass.keyword_hook.main(working_dir)

    if not flags.local:
        logger.info("remote mode is on. Running docker_hook.")
        docker_hook.main(only=flags.topic, exclude=flags.exclude)


if __name__ == '__main__':
    flags, unparsed = _parse_args()
    main(flags)
