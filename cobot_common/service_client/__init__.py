import os
import json
from functools import reduce

from cobot_common.service_client.model import Model


def _model_file_load():
    package_dir = os.path.dirname(os.path.realpath(__file__))
    model_dir = os.path.join(package_dir, 'models')
    model_configs = []
    for file in os.listdir(model_dir):
        if file.endswith('.json'):
            model_path = os.path.join(model_dir, file)
            with open(model_path, 'r') as json_file:
                model_config = json.load(json_file)
                model_configs.append(model_config)
    return model_configs


def _load_model(model_configs, api_key, timeout_in_millis):
    models = [
        Model(model_config, api_key, timeout_in_millis)
        for model_config in model_configs
    ]
    model = reduce(
        lambda x, y: x.merge(y), 
        models
    )
    return model


class Client(object):

    def __init__(self, api_key=None, timeout_in_millis=None):
        model_configs = _model_file_load()
        self.model = _load_model(model_configs, api_key, timeout_in_millis)
        self.timeout_in_millis = timeout_in_millis

    def __getattr__(self, name):
        """
        Expose API name as client interface. 
        API names are retrieved from model.json, an api configure file exported from AWS API Gateway.
        """
        return self.model.call_api(name)


def get_client(api_key=None, timeout_in_millis=None):
    return Client(api_key=api_key, timeout_in_millis=timeout_in_millis)
