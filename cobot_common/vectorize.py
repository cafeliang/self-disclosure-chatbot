from typing import Dict, List, Tuple

import numpy as np
import json
from sklearn.preprocessing import LabelBinarizer

class Vectorizer:
    """
    Creates a one-hot vector representation of a series of intents and response generators for dialog modeling.
    Uses the sklearn LabelBinarizer as a helper class.
    """

    Turn = List[str]
    IndexedTurn = List[int]
    OneHotTensor = List[List[int]]

    def __init__(self,rgs,intents=None,model_file=None):
        self.rgs = rgs
        if model_file!=None:
            self.intents = Vectorizer.get_intent_list_from_model(open(model_file))
        elif intents!=None:
            self.intents = intents
        elif intents==None:
            print("Error: no intents or model_file arguments passed to Vectorizer")
            self.intents = []
        self.lb1 = LabelBinarizer()
        self.lb2 = LabelBinarizer()
        self.lb1.fit(self.intents)
        self.lb2.fit(self.rgs)

    @staticmethod
    def get_intent_list_from_model(modelfile):
        """
        Parse out intents from an ASK model file. If you aren't using ASK, you will need to pass a list of valid intents to the constructor.
        """
        model = json.load(modelfile)
        intent_specs = model['interactionModel']['languageModel']['intents']
        intents = [i['name'] for i in intent_specs]
        return intents
    
    def numerize(self,turns:List[Turn]) -> List[IndexedTurn]:
        """
        Given a list of turns, return a list of indices to the intent and response generator lists
        """
        result = []
        for turn in turns:
            ind0 = 0
            ind1 = 0
            try:
                ind0 = self.intents.index(turn[0])+1
            except:
                pass
            try:
                ind1 = self.rgs.index(turn[1])+1
            except:
                pass
            result.append([ind0,ind1])
        return result


    def vectorize_turns(self,turns:List[Turn])->Tuple[OneHotTensor,OneHotTensor]:
        """
        Given a list of turns (intent/response generator pairs), numerize them, then convert into a pair of one-hot encoded tensors.
        """
        numerized = self.numerize(turns)
        inputs = [self.lb1.transform([turn[0]])[0].tolist() for turn in turns]
        outputs = [self.lb2.transform([turn[1]])[0].tolist() for turn in turns]
        return (inputs,outputs)

    def invert_output(self,output:IndexedTurn):
        return self.lb2.inverse_transform(output)

if __name__=='__main__':
    turns = [ ["greet","GREETER"], ["topic_request","GREETER"], ["QAIntent","EVI"], ["more_info_request",""] ]
    vectorizer = Vectorizer(['EVI','GREETER'],intents=["greet","topic_request","QAIntent","more_info_request"])
                            #model_file="cobot-skill/models/en-US.json")
    result = vectorizer.vectorize_turns(turns)
    print(result)
