#!/usr/bin/env python
# -*- coding: utf-8 -*-
import spacy
import os
import time
import logging
import sys
import truecaser_model

from flask import Flask, request, Response
from flask_restful import reqparse, Api, Resource
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

nlp = spacy.load('en')
truecaser = truecaser_model.truecaser_model()
truecaser.load_model()

app = Flask("nlp")
api = Api(app)

log_handler = logging.StreamHandler(sys.stdout)
log_formatter = logging.Formatter('%(asctime)s %(name)s [%(levelname)s] %(filename)s [line %(lineno)d]: %(message)s')
log_handler.setFormatter(log_formatter)

app.logger.addHandler(log_handler)
app.logger.setLevel(logging.DEBUG)


class Default(Resource):
    def get(self):
        return Response('Welcome', mimetype='text/plain')

    def post(self):
        t0 = time.time()
        args = request.get_json(force=True)

        validation = self.__validate_input(args)
        if validation:
            return validation, 500

        ret = {}
        if args.get('text'):
            app.logger.info("text received: %s", args.get('text'))

            ret.update(
                self.analyze(args) # COBOT 2.0
                # self.analyze(args.get('text')) # COBOT 2.1 update
            )

        ret['performance'] = time.time() - t0,
        ret['error'] = False

        app.logger.info("result: %s", ret)
        return ret, 200

    @staticmethod
    def __validate_input(args):
        message = ""
        if not args.get('text'):
            message = "No input text received."
        if message:
            return {
                'message': message,
                'error': True
            }
        return None

    def analyze(self, text):
        return {}

class NER(Default):

    def post(self):
        return super().post()

    def analyze(self, args):
        text = args.get('text')
        text = truecaser.run_model([text])[0]
        print('capitalized: '+text)
        doc = nlp(text)

        ret = {
            'response': []
        }
        for ent in doc.ents:
            ret['response'].append({
                "text": ent.text,
                "begin_offset": ent.start_char,
                "end_offset": ent.end_char,
                "label": ent.label_
            })
        app.logger.info('NER return: %s', ret)
        return ret


class Sentiment(Default):

    def post(self):
        return super().post()

    def analyze(self, args):
        text = args.get('text')
        # TODO: use segmentation result to do sentiment analysis
        if args.get('segmentation') is None:
          sgmt = [text]
        else:
          sgmt = args.get('segmentation')["segmented_text_raw"]
        # app.logger.info('!!!!!!!!!!!!!')
        # app.logger.info(args)
        # app.logger.info(args.get('segmentation'))
        # app.logger.info(sgmt)
        # app.logger.info(truecaser.run_model(sgmt))
        # app.logger.info(truecaser.run_model([text]))

        truecase_text = truecaser.run_model(sgmt)

        # text = truecaser.run_model(sgmt)[0]
        sentiments = {"response":[]}

        for text in truecase_text:
          ret = {}
          analyzer = SentimentIntensityAnalyzer()
          sentiment = analyzer.polarity_scores(text)
          max_confidence = 0

          response_format = request.args.get('format','simple')

          if response_format == 'simple':
              final_label = None
              for label, confidence in sentiment.items():
                  if confidence > max_confidence:
                      final_label = label
                      max_confidence = confidence
              ret['response'] = final_label
          elif response_format == 'full':
              for k, v in sentiment.items():
                sentiment[k] = str(v)
              ret['response'] = sentiment
          else:
              ret['response'] = 'Invalid response format'
          sentiments['response'].append(ret['response'])
          if response_format == 'simple':
              sentiments["response"] = sentiments["response"][0]

        app.logger.info('Sentiments return: %s', sentiments)
        return sentiments


class NLP(Default):

    def post(self):
        return super().post()

    def analyze(self, args):
        ret = {'response': {}}
        text = args.get("text")
        text = truecaser.run_model([text])[0]
        print('capitalized: '+text)
        ret['response']['sentiment'] = Sentiment().analyze(args).get('response')
        ret['response']['ner'] = NER().analyze(args).get('response')
        app.logger.info('nlp return: %s', ret)
        return ret

class Coreference(Default):

   def post(self):
       return super().post()

   def analyze(self, args):
       ret = {'response': {}}

       #utterance = text[0]
       #history = text[1:] # TODO: reverse the inputs
       #clusters = coref.one_shot_coref(utterances=text[0], context=history[-2:])
       #resolved_utterance = coref.get_resolved_utterances()[0]
       #mentions = coref.get_mentions()
       #most_representative = coref.get_most_representative()
       #scores = coref.get_scores()
       #utterances = coref.get_utterances()

       #ret['response'] = {
       #                      "resolved_utterance": resolved_utterance,
       #                      "clusters": str(clusters),
       #                      "mentions": str(mentions),
       #                      "scores": str(scores),
       #                      "most_representative": str(most_representative),
       #                      "utterances": str(utterances),
       #                      "utterance": str(utterance),
       #                      "history": str(history[-2:]),
       #                  }
       return ret

api.add_resource(Default, '/')
api.add_resource(NER, '/ner')
api.add_resource(Sentiment, '/sentiment')
api.add_resource(NLP, '/nlp')
api.add_resource(Coreference, '/coreference')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=os.environ.get('NLP_PORT') or 5001)
