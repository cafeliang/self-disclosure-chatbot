import os, json, boto3
from datetime import datetime

firehose = boto3.client('firehose')

def process_record(streamRecord):
    data = streamRecord['NewImage']
    outjson = {}
    for key, value in data.items():
        if key == 'creation_date_time':
            # Truncate microseconds and format the date time to something more useful for Athena queries
            try:
                val = str(datetime.strptime(value['S'][:19], '%Y-%m-%dT%H:%M:%S'))
            except:
            # An error occurred formatting the datetime. Let's just return the standard datetime from DDB
                val = value['S']
            outjson[key] = val
        elif 'S' in value:
            outjson[key]= value['S']
        elif 'N'in value:
            outjson[key]= value['N']
        elif 'L'in value:
            outjson[key]= json.dumps(value['L'])
        elif 'M'in value:
            outjson[key]= json.dumps(value['M'])
        elif 'BOOL'in value:
            outjson[key]= json.dumps(value['BOOL'])
        else:
            outjson[key]= json.dumps(value)
    jtoFirehose = json.dumps(outjson) + '\n'
    print ("DONE")
    return jtoFirehose

def lambda_handler(event, context):
    failed= 0
    outrecords = []
    print ('Processing {} records.'.format(len(event['Records'])))
    for record in event['Records']:
        # Ignore Alexa Prize monitoring pings and load test requests
        try:
            session_id = record['dynamodb']['Keys']['session_id']['S']
            if session_id == 'amzn1.echo-api.session.0000000-0000-0000-0000-00000000000'\
                    or session_id.startswith('amzn1.echo-api.session.loadtest'):
                continue
            if (record['eventName']) == 'REMOVE':
                continue 
            outrecords.append({'Data': process_record(record['dynamodb'])})
        except  Exception as e:
            print("exception: " + str(e))
            print ("Exception processing:", record['dynamodb']['NewImage'])
            failed += 1
    if outrecords:
        response = firehose.put_record_batch(
            DeliveryStreamName=os.environ['DeliveryStreamName'],
            Records= outrecords
        )
    print ('Processed {} records. Failed on {}'.format(len(event['Records']), failed))
