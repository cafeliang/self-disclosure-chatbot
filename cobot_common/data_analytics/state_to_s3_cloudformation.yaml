AWSTemplateFormatVersion: 2010-09-09
Description: >-
  AWS CloudFormation: A pipeline from your DynamoDB table to an S3 bucket.
Parameters:
  DynamoDBTableName:
    Description: DynamoDB Table Name
    Type: String
    AllowedPattern: '[a-zA-Z0-9]*'
    MinLength: '1'
    MaxLength: '255'
    ConstraintDescription: must contain only alphanumeric characters
  LambdaCodeBucket:
    Description: S3 bucket containing the Lambda function code
    Type: String
  DynamoDBStreamArn:
    Description: DynamoDB table Stream ARN
    Type: String
  LambdaHandler:
    Type: String
    Default: "state_to_s3_lambda.lambda_handler"
  LambdaS3Key:
    Type: String
    Default: "state_to_s3_lambda.zip"

Resources:
  DynamoDBStreamBucket:
    Type: 'AWS::S3::Bucket'
  FirehoseDeliveryStream:
    DependsOn:
      - deliveryPolicy
    Type: 'AWS::KinesisFirehose::DeliveryStream'
    Properties:
      DeliveryStreamName: !Ref DynamoDBTableName
      ExtendedS3DestinationConfiguration:
        BucketARN: !Join 
          - ''
          - - 'arn:aws:s3:::'
            - !Ref DynamoDBStreamBucket
        BufferingHints:
          IntervalInSeconds: '60'
          SizeInMBs: '1'
        CompressionFormat: UNCOMPRESSED
        Prefix: firehose/
        RoleARN: !GetAtt deliveryRole.Arn
  deliveryRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Sid: ''
            Effect: Allow
            Principal:
              Service: firehose.amazonaws.com
            Action: 'sts:AssumeRole'
            Condition:
              StringEquals:
                'sts:ExternalId': !Ref 'AWS::AccountId'
  deliveryPolicy:
    Type: 'AWS::IAM::Policy'
    Properties:
      PolicyName: firehose_delivery_policy
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Action:
              - 's3:AbortMultipartUpload'
              - 's3:GetBucketLocation'
              - 's3:GetObject'
              - 's3:ListBucket'
              - 's3:ListBucketMultipartUploads'
              - 's3:PutObject'
            Resource:
              - !Join 
                - ''
                - - 'arn:aws:s3:::'
                  - !Ref DynamoDBStreamBucket
              - !Join 
                - ''
                - - 'arn:aws:s3:::'
                  - !Ref DynamoDBStreamBucket
                  - '*'
      Roles:
        - !Ref deliveryRole
  ddbToFirehose: 
      Type: "AWS::Lambda::Function"
      Properties: 
        Handler: !Ref LambdaHandler
        Role: 
          Fn::GetAtt: 
            - "LambdaExecutionRole"
            - "Arn"
        Code: 
          S3Bucket: !Ref LambdaCodeBucket
          S3Key: !Ref LambdaS3Key
        Runtime: "python3.6"
        Timeout: "25"
        Environment:
          Variables:
            DeliveryStreamName: !Ref DynamoDBTableName
        TracingConfig:
          Mode: "Active"
  LambdaExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - lambda.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
      Policies:
      - PolicyName: FirehoseFullAccess
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action:
            - logs:*
            - firehose:*
            - xray:*
            - dynamodb:DescribeStream
            - dynamodb:GetRecords
            - dynamodb:GetShardIterator
            - dynamodb:ListStreams
            Resource: "*"
  EventSourceMapping: 
    Type: "AWS::Lambda::EventSourceMapping"
    Properties: 
      EventSourceArn: !Ref DynamoDBStreamArn
      FunctionName: !GetAtt
      - ddbToFirehose
      - Arn
      StartingPosition: "TRIM_HORIZON"
Outputs:
  BucketName:
    Value: !Ref DynamoDBStreamBucket
    Description: An S3 destination for the DynamoDB stream
