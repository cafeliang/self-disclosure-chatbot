import boto3
import os
import uuid
import zipfile
import tempfile
import argparse
import time
import datetime
from pprint import pprint, pformat


COBOT_HOME = os.environ.get('COBOT_HOME')
MAX_BUCKET_NAME_LENGTH = 63
ARTIFACT_BUCKET_NAME_PREFIX = 'cad-'
LAMBDA_FILE_S3_KEY = 'state_to_s3_lambda.zip'
TEMPLATE_FILE_S3_KEY = 'template.yaml'
TEMPLATE_S3_URL_TEMP = 'https://s3.amazonaws.com/{bucket_name}/template.yaml'
CFN_STACK_NAME = 'cobot-stack-data-analytics'


class bcolors:
    SUCCESS = '\033[92m'
    WARNING = '\033[93m'
    ERROR = '\033[91m'
    ENDC = '\033[0m'

    @classmethod
    def warning(cls, msg):
        return cls.WARNING + msg + cls.ENDC

    @classmethod
    def success(cls, msg):
        return cls.SUCCESS + msg + cls.ENDC

    @classmethod
    def error(cls, msg):
        return cls.ERROR + msg + cls.ENDC


class CloudFormationStackProgress(object):

    SUCCESS_STATES = [
        "CREATE_COMPLETE",
        "UPDATE_COMPLETE",
        "DELETE_COMPLETE"
    ]
    PROGRESS_STATES = [
        "CREATE_IN_PROGRESS",
        "DELETE_IN_PROGRESS",
        "REVIEW_IN_PROGRESS",
        "ROLLBACK_IN_PROGRESS",
        "UPDATE_COMPLETE_CLEANUP_IN_PROGRESS",
        "UPDATE_IN_PROGRESS",
        "UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS",
        "UPDATE_ROLLBACK_IN_PROGRESS"
    ]
    FAILURE_STATES = [
        "CREATE_FAILED",
        "DELETE_FAILED",
        "ROLLBACK_COMPLETE",
        "ROLLBACK_FAILED",
        "UPDATE_ROLLBACK_COMPLETE",
        "UPDATE_ROLLBACK_FAILED"
    ]

    def __init__(self, stack_name):
        self._stack_name = stack_name

    def show_progress(self):
        cfn_client = boto3.client('cloudformation')
        last_event_time = None
        while True:
            time.sleep(1)
            events = cfn_client.describe_stack_events(StackName=self._stack_name).get('StackEvents', [])
            events.reverse()
            for event in events:
                status = event['ResourceStatus']
                resource = event['LogicalResourceId']
                try:
                    reason = " - " + event['ResourceStatusReason']
                except:
                    reason = ""

                event_time = event['Timestamp']

                if last_event_time is None or event_time > last_event_time:
                    # Print output for new event
                    local_time = event_time.replace(tzinfo=datetime.timezone.utc).astimezone().strftime("%H:%M:%S")
                    if status in self.SUCCESS_STATES:
                        print(local_time + ": " + bcolors.success(status) + " \t" + resource + reason)
                    elif status in self.PROGRESS_STATES:
                        print(local_time + ": " + bcolors.warning(status) + " \t" + resource + reason)
                    elif status in self.FAILURE_STATES:
                        print(local_time + ": " + bcolors.error(status) + " \t" + resource + reason)

                    # Check for first failure event
                    first_failure = None
                    if status in self.FAILURE_STATES and first_failure is None:
                        try:
                            first_failure = event
                            fail_message = first_failure['LogicalResourceId']
                            fail_stack_id = first_failure['PhysicalResourceId']
                            fail_events = cfn_client.describe_stack_events(StackName=fail_stack_id).get('StackEvents', [])
                            fail_events.reverse()
                            for fail_event in fail_events:
                                fail_event_status = fail_event['ResourceStatus']
                                if fail_event_status in self.FAILURE_STATES:
                                    try:
                                        fail_message += " - " + fail_event['LogicalResourceId']
                                        fail_message += " - " + fail_event['ResourceStatusReason']
                                        break
                                    except:
                                        fail_message = first_failure['LogicalResourceId']
                        except Exception as e:
                            first_failure = None

                    # Evaluate exit conditions
                    if resource == self._stack_name and status in self.SUCCESS_STATES:
                        print(local_time + ": " + bcolors.success('SUCCESS!'))
                        return True
                    if resource == self._stack_name and status in self.FAILURE_STATES:
                        if fail_message:
                            print(local_time + ": " + bcolor.error("FAILURE\t\t" + fail_message))
                        else:
                            print(local_time + ": " + bcolor.error("FAILURE"))
                        return False

                    if last_event_time is None or event_time > last_event_time:
                        last_event_time = event_time

    def show_output(self):
        stack = boto3.resource('cloudformation').Stack(self._stack_name)
        for output in stack.outputs:
            print(output['Description'] + ':', bcolors.success(output['OutputValue']))


def upload_artifacts(lambda_func_file_path, template_file_path, table_name):
    """ Upload AWS lambda codes and Cloudformation template file to S3 bucket

    :type str
    :param lambda_func_file_path: The file path of AWS lambda function codes

    :type str
    :param template_file_path: Cloudformation template file path
    """
    # Create a S3 bucket
    # Use uuid to avoid bucket name conflict
    bucket_name = ARTIFACT_BUCKET_NAME_PREFIX + table_name.lower() + "-" + str(uuid.uuid1())
    bucket_name = bucket_name[:MAX_BUCKET_NAME_LENGTH] 
    print(len(bucket_name))
    s3_client = boto3.client('s3')
    s3_client.create_bucket(
        Bucket=bucket_name
    )
    # Create a zip file with AWS Lambda codes
    _, temp_zip_file = tempfile.mkstemp()
    with zipfile.ZipFile(temp_zip_file, 'w') as zip_fp:
        zip_fp.write(lambda_func_file_path, arcname=lambda_func_file_path.split('/')[-1])
    # Upload the zip file
    boto3.resource('s3').Bucket(bucket_name).upload_file(temp_zip_file, LAMBDA_FILE_S3_KEY)
    os.remove(temp_zip_file)
    # Upload Cloudformation template file
    boto3.resource('s3').Bucket(bucket_name).upload_file(template_file_path, TEMPLATE_FILE_S3_KEY)
    return bucket_name


def deploy(template_url, table_name, lambda_bucket_name, stream_arn):
    """ Deploy infrastructure by Cloudformation

    :type str
    :param template_url: URL of Cloudformation template file in S3 bucket

    :type str
    :param table_name: The DynamoDB table from which the data are exported to S3 bucket

    :type str
    :param lambda_bucket_name: The name of the Amazon S3 bucket where the .zip file that contains Lambda codes is stored

    :type str
    :param stream_arn: ARN of the DynamoDB stream
    """
    cfn_client = boto3.client('cloudformation')
    response = cfn_client.create_stack(
        StackName=CFN_STACK_NAME + "-" +table_name,
        TemplateURL=template_url,
        Capabilities=['CAPABILITY_IAM'],
        Parameters=[
            {
                'ParameterKey': 'DynamoDBTableName',
                'ParameterValue': table_name
            },
            {
                'ParameterKey': 'LambdaCodeBucket',
                'ParameterValue': lambda_bucket_name
            },
            {
                'ParameterKey': 'DynamoDBStreamArn',
                'ParameterValue': stream_arn
            }
        ]
    )


def delete_bucket(bucket_name):
    bucket = boto3.resource('s3').Bucket(bucket_name)
    bucket.objects.all().delete()
    bucket.delete()


def main(args):
    lambda_bucket_name = upload_artifacts(args.lambda_func_file_path, args.cloudformation_template_file_path, args.table_name)
    template_url = TEMPLATE_S3_URL_TEMP.format(bucket_name=lambda_bucket_name)
    deploy(template_url, args.table_name, lambda_bucket_name, args.stream_arn)
    stack_progress = CloudFormationStackProgress(CFN_STACK_NAME + "-" + args.table_name)
    stack_progress.show_progress()
    stack_progress.show_output()
    delete_bucket(lambda_bucket_name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='cobot dynamodb-to-s3 deploy',
        usage="""%(prog)s -t <state-table-name> -s <state-table-stream-ARN>

        <state-table-name> is the name of the DynamoDB table from which the data are exported to S3 bucket
        <state-table-stream-ARN is the stream ARN of the DynamoDB table
        """,
        description='Deploy the pipline to export data from State table to S3 bucket for AWS Athena', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-t', '--table_name', required='True', help='The name of the DynamoDB table from which the data are exported to S3 bucket')
    parser.add_argument('-s', '--stream_arn', required='True', help='ARN of the DynamoDB stream')
    parser.add_argument('-l', '--lambda_func_file_path', default=os.path.join(COBOT_HOME, 'cobot_common/data_analytics/state_to_s3_lambda.py'), help="The file path of AWS Lambda function codes to process DynamoDB stream")
    parser.add_argument('-c', '--cloudformation_template_file_path', default=os.path.join(COBOT_HOME, 'cobot_common/data_analytics/state_to_s3_cloudformation.yaml'), help="The file path of Cloudformation template")
    args = parser.parse_args()
    main(args)
