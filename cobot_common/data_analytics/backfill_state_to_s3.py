import sys
import boto3
import argparse
from datetime import datetime
from io import BytesIO
import json
import time


def process_arguments():
    parser = argparse.ArgumentParser(usage="""
        cobot dynamodb-to-s3 backfill  -t <state_table_name> -b <output_s3_bucket> 
         The <output_s3_bucket> is the S3 bucket that was created for you in the 
         previous step. The data will be copied to the same folder "firehose" where streaming data
         is getting copied. Athena will be able to use combine both data sources seemlessly.
    """)
    parser.add_argument('-t', "--table", required=True, 
            help = 'DynamoDB table name')
    parser.add_argument('-b', "--bucket", required=True, 
            help = 'T')
    parser.add_argument('-f', "--folder", default='firehose',
            help = 'S3 Folder name to write into, Can be a hierarchical name')

    parser.add_argument('-r', "--region",help = 'aws region')  
    args = parser.parse_args()
    return args


def main():
    args = process_arguments()
    region_name = args.region
    client = boto3.client('dynamodb', region_name=region_name)
    s3_resource = boto3.resource('s3', region_name=region_name)
    #TODO: move scan operation to dynamodb_manager.py
    paginator = client.get_paginator('scan')
    operation_parameters = {
        'Limit':200,
        'TableName': args.table,
    }

    page_counter = 0
    page_iterator = paginator.paginate(**operation_parameters)
    for page in page_iterator:
        page_counter+=1
        ofile= BytesIO()
        print("page: "+str(page_counter))
        for i in page['Items']:
             outjson = {}
             for key, value in i.items():
                 if key == 'creation_date_time':
                     # Truncate microseconds and format the date time to something more useful for Athena queries
                     try:
                         val = str(datetime.strptime(value['S'][:19], '%Y-%m-%dT%H:%M:%S'))
                     except:
                     # An error occurred formatting the datetime. Let's just return the standard datetime from DDB
                         val = value['S']
                     outjson[key] = val 
                 elif 'S' in value: 
                     outjson[key]= value['S']
                 elif 'N'in value:
                     outjson[key]= value['N']
                 elif 'L'in value:
                     outjson[key]= json.dumps(value['L'])
                 elif 'M'in value:
                     outjson[key]= json.dumps(value['M'])
                 elif 'BOOL'in value:
                     outjson[key]= json.dumps(value['BOOL'])
                 else:
                     print(key)
                     print(value)
                     outjson[key]= json.dumps(value)
             ofile.write((json.dumps(outjson) + '\n').encode('utf8'))
        output_name= "BackFillStateTable-{}".format(page_counter)
        try:
            s3_resource.Object(args.bucket, args.folder + '/' +
                output_name).put(Body=ofile.getvalue())

        except Exception as e:
            print("exception: " + str(e))
            print("Exception writing " + output_name)
            sys.exit()
        time.sleep(1) 

if __name__ == '__main__':
    main()
