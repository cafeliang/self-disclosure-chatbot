import time
import os
import sys
from requests.exceptions import HTTPError, ConnectionError, Timeout

try:
    from cobot_common.service_client import get_client
except ModuleNotFoundError:
    # add cobot_common module to module searching path
    base_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(base_path)
    from cobot_common.service_client import get_client

from cobot_common.service_client.exceptions import ParamValidationError, UnknownAPIError
from cobot_common.data_processing.utterance import UtteranceUtil

def _parse_topic_response(response):
    topics = []
    for topic in response.get('topics', []):
        del topic['text']
        topics.append(topic)
    return topics


def _parse_profanity_response(response):
    profanity_result = []
    for profanity in response.get('offensivenessClasses', []):
        profanity_class = profanity.get('overallClass', None)
        if profanity_class == 0:
            profanity_result.append(False)
        elif profanity_class == 1:
            profanity_result.append(True)
        else:
            profanity_result.append(None)
    return profanity_result

def _parse_sentiment_response(response):
    sentiment_result = []
    for sentiment in response.get('sentimentClasses', []):
        sentiment_class = sentiment.get('sentimentClass', None)
        if sentiment_class == 0:
            sentiment_result.append("Negative")
        elif sentiment_class == 2:
            sentiment_result.append("Positive")
        else:
            sentiment_result.append("Neutral")
    return sentiment_result


def retry_request(request):
    retry = 0
    while(retry <= 2):
        try:
            r = request()
            return r
        except ParamValidationError as e:
            break
        except (HTTPError, ConnectionError, Timeout):
            retry += 1
            time.sleep(2**retry * 0.1)


class RemoteChecker(object):

    def __init__(self, api_key):

        self._client = get_client(api_key=api_key)

    @classmethod
    def _retrieve_text(cls, utterances):
        text_list = [
            utterance.text
            for utterance in utterances
        ]
        return text_list

    def detect_topic(self, utterances):
        text_list = UtteranceUtil.retrieve_text(utterances)
        topics = [None] * len(text_list)

        def request():
            r = self._client.batch_detect_topic(utterances=text_list)
            topics = _parse_topic_response(r)
            return topics

        r = retry_request(request)
        if r is not None:
            topics = r

        for index, utterance in enumerate(utterances):
            utterance.topic = topics[index]
        return utterances

    def detect_profanity(self, utterances):
        text_list = UtteranceUtil.retrieve_text(utterances)
        profanity_classes = [None] * len(text_list)

        def request():
            r = self._client.batch_detect_profanity(utterances=text_list)
            profanity_classes = _parse_profanity_response(r)
            return profanity_classes

        r = retry_request(request)
        if r is not None:
            profanity_classes = r

        for index, utterance in enumerate(utterances):
            utterance.profanity_class = profanity_classes[index]
        return utterances

    def detect_sentiment(self, utterances):
        text_list = UtteranceUtil.retrieve_text(utterances)
        sentiment_classes = [None] * len(text_list)

        def request():
            r = self._client.batch_detect_sentiment(utterances=text_list)
            sentiment_classes = _parse_sentiment_response(r)
            return sentiment_classes

        r = retry_request(request)
        if r is not None:
            sentiment_classes = r

        for index, utterance in enumerate(utterances):
            utterance.sentiment_class = sentiment_classes[index]
        return utterances
