import os
import json

from cobot_common.datasets.builder import DatasetBuilder


class TopicalChatBuilder(DatasetBuilder):

    def __init__(self):
        self._name = 'topical_chat'

        super().__init__(self._name)

    def all(self):
        tcs = self._load_tcs()
        self.build_split(self._get_iter(),
                         os.path.join(self.output_data_dir(), 'all'),
                         tcs=tcs)

    def train(self):
        tcs = self._load_tcs()
        self.build_split(self._get_iter('training'),
                         os.path.join(self.output_data_dir(), 'train'),
                         tcs=tcs)

    def test_seen(self):
        tcs = self._load_tcs()
        self.build_split(self._get_iter('testing_seen'),
                         os.path.join(self.output_data_dir(), 'test_seen'),
                         tcs=tcs)

    def test_unseen(self):
        tcs = self._load_tcs()
        self.build_split(self._get_iter('testing_unseen'),
                         os.path.join(self.output_data_dir(), 'test_unseen'),
                         tcs=tcs)

    def valid_seen(self):
        tcs = self._load_tcs()
        self.build_split(self._get_iter('validation_seen'),
                         os.path.join(self.output_data_dir(), 'valid_seen'),
                         tcs=tcs)

    def valid_unseen(self):
        tcs = self._load_tcs()
        self.build_split(self._get_iter('validation_unseen'),
                         os.path.join(self.output_data_dir(), 'valid_unseen'),
                         tcs=tcs)

    def _get_iter(self, key=None):
        dialogs = self._load_dialogs()
        summary = self._load_summary()

        iterator = []
        for id, dialog in dialogs.items():
            if key is None or id in summary.get(key):
                iterator.append((id, dialog))
        return iterator

    def convert(self, item, tcs):
        id, dialog = item
        turns = []
        for i in range(len(dialog.get('content'))):
            if i < len(dialog.get('content')) - 1:
                turns.append(self._convert_turn(dialog.get('content')[i], dialog.get('content')[i + 1]))
            else:
                turns.append(self._convert_turn(dialog.get('content')[i]))

        topical_content = {}
        for agent, tcs_id in dialog.get('topical_content').items():
            topical_content[agent] = tcs.get(tcs_id)

        converted_dialog = {
            'dataset': self._name,
            'version': "1.0",
            'dialog_id': id,
            'turns': turns,
            'annotation': {
                'rating': dialog.get('rating'),
            },
            'metadata': {
                'topical_content': topical_content,
                'config': dialog.get('config')
            }
        }
        return converted_dialog

    @staticmethod
    def _convert_turn(turn, next_turn=None):
        converted_turn = {
            'input': turn.get('message'),
            'response': '' if next_turn is None else next_turn.get('message'),
            'annotation': {
                'rating': turn.get('rating'),
                'sentiment': turn.get('sentiment'),
                'knowledge_source': turn.get('knowledge_source')
            }
        }
        return converted_turn

    def _load_summary(self):
        with open(os.path.join(self.raw_data_dir(), 'summary.json')) as f:
            summary = json.load(f)
        return summary

    def _load_dialogs(self):
        file = self._get_dialog_file()
        with open(os.path.join(self.raw_data_dir(), file)) as f:
            dialogs = json.load(f)
        return dialogs

    def _load_tcs(self):
        file = self._get_tcs_file()
        with open(os.path.join(self.raw_data_dir(), file)) as f:
            tcs = json.load(f)
        return tcs

    def _get_dialog_file(self):
        files = os.listdir(self.raw_data_dir())
        for file in files:
            if file.split('_')[0] == 'dialogs':
                return file

    def _get_tcs_file(self):
        files = os.listdir(self.raw_data_dir())
        for file in files:
            if file.split('_')[0] == 'tcs':
                return file
