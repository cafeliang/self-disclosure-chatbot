"""Process the slack command, get_conversations for 2019 data
"""

import os
import argparse
from data_analytics.athena_metadata import get_today
from data_analytics.data_utils.daily_conversation_generator import main

from data_analytics.analytics_metadata import _2019_START_DATE
from data_analytics.slack_api.slack_bot.slack_utils import send_file, send_prelim_slack_message
from data_analytics.data_utils.integration_test_generator import generate_integration_test


def process_get_integrationtest(args):
    send_prelim_slack_message("Retrieving 2019 Data Integration Tests...", args.channels)
    message, success_files, failed_files = main(args)
    send_prelim_slack_message(message, args.channels)
    if args.conv_id is None:
       send_prelim_slack_message("conv_id field required", args.channels)
       return
    if len(success_files) == 0:
        send_prelim_slack_message("No entries found for conversation ID {0}".format(args.conv_id), args.channels)
        return
    rating_file, no_rating_file = success_files[0]
    rating_file_names = generate_integration_test(rating_file)
    no_rating_filenames = generate_integration_test(no_rating_file)
    if rating_file_names is None and no_rating_filenames is None:
        send_prelim_slack_message("No Integration File Generated.  This may be due to an error.", args.channels)
        return
    if rating_file_names is not None:
        send_file(rating_file_names[0], args.initial_comment, args.channels)
        send_file(rating_file_names[1], args.initial_comment, args.channels)
    if no_rating_filenames is not None:
        send_file(no_rating_filenames[0], args.initial_comment, args.channels)
        send_file(no_rating_filenames[1], args.initial_comment, args.channels)

    send_prelim_slack_message("File Retrieval Complete", args.channels)


def add_args(parser):
    today = get_today(True)
    parser.add_argument('--start', '-s', default=today,
                        help='timerange from (YYYY-mm-dd)')
    parser.add_argument('--end', '-e', default=today,
                        help='timerange to (YYYY-mm-dd)')
    parser.add_argument('--data_table', '-d', default="state_table_prod", help="data_table")
    parser.add_argument('--ratings', '-r', default=None, help="Filter by Ratings, default to 5.0")
    parser.add_argument('--module', '-m', default=None, help="Filter by Modules")
    parser.add_argument('--forced', '-f', default=False, help="Force build joined table")
    parser.add_argument('--conv_id', '-n', default=None, help="Conversation ID")
    parser.add_argument('--updated', '-u', default=False, help="most_updated_conversations")
    parser.add_argument('--user_id', "-i", default=None, help="user_id")
    parser.add_argument('--ratings_only', '-v', default=False),

    parser.add_argument('--initial_comment', '-c', default="",
                        help='initial comment to send')

    parser.add_argument('--channels', '-l', default="#debug",
                        help='channel to send the message')

    return



if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    if args.conv_id is not None:
        args.start = _2019_START_DATE
    process_get_integrationtest(args)