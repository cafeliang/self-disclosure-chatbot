"""Slack Command for refreshing visualization
"""
import argparse
from data_analytics.libs.generate_history_data import get_csv_table as generate_history_data
from data_analytics.analytics_metadata import HISTORY_DATA, ANALYTICS_ROOT
import os
import shutil
def main(args):
    """Generate history data"""
    if args.forced or not os.path.exists(HISTORY_DATA):
        generate_history_data(args.forced)

    visualization_location = os.path.join(ANALYTICS_ROOT, "slack_api", "slack_bot", "static", "result_display_data")
    shutil.copy(HISTORY_DATA, visualization_location)
    return

def add_args(parser):
    parser.add_argument('--forced', '-f', default=False, help="Force build joined table")

if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)
    args = parser.parse_args()
    main(args)