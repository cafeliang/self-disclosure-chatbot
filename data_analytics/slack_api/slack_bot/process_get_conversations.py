"""Process the slack command, get_conversations for 2019 data
"""

import os
import argparse
from data_analytics.athena_metadata import get_today
from data_analytics.data_utils.daily_conversation_generator import main
from data_analytics.analytics_metadata import _2019_START_DATE
from data_analytics.slack_api.slack_bot.slack_utils import send_csv, send_prelim_slack_message



def process_get_conversations(args):
    send_prelim_slack_message("Retrieving 2019 Data...", args.channels)
    if args.updated is True or args.user_id is not None:
        args.forced = True
    message, success_joins, failed_joins = main(args)
    send_prelim_slack_message(message, args.channels)

    for files in success_joins:
        rating_files, no_rating_files = files
        if args.data_table != "state_table_prod":
            send_prelim_slack_message("Sending file - {}".format(no_rating_files), args.channels)
            send_csv(no_rating_files, args.initial_comment, args.channels)
        else:
            send_prelim_slack_message("Sending file - {}".format(rating_files), args.channels)
            send_csv(rating_files, args.initial_comment, args.channels)
        if not args.ratings_only:
            send_prelim_slack_message("Sending file - {}".format(no_rating_files), args.channels)
            send_csv(no_rating_files, args.initial_comment, args.channels)
    send_prelim_slack_message("File Retrieval Complete", args.channels)


def add_args(parser):
    today = get_today(True)
    parser.add_argument('--start', '-s', default=today,
                        help='timerange from (YYYY-mm-dd)')
    parser.add_argument('--end', '-e', default=today,
                        help='timerange to (YYYY-mm-dd)')
    parser.add_argument('--data_table', '-d', default="state_table_prod", help="data_table")
    parser.add_argument('--ratings', '-r', default=None, help="Filter by Ratings, default to 5.0")
    parser.add_argument('--module', '-m', default=None, help="Filter by Modules")
    parser.add_argument('--forced', '-f', default=False, help="Force build joined table")
    parser.add_argument('--conv_id', '-n', default=None, help="Conversation ID")
    parser.add_argument('--updated', '-u', default=False, help="most_updated_conversations")
    parser.add_argument('--user_id', "-i", default=None, help="user_id")
    parser.add_argument('--ratings_only', '-v', default=False),

    parser.add_argument('--initial_comment', '-c', default="",
                        help='initial comment to send')

    parser.add_argument('--channels', '-l', default="#debug",
                        help='channel to send the message')

    return



if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    process_get_conversations(args)