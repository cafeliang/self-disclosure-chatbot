from data_analytics.libs.utils import read_from_json
from data_analytics.athena_metadata import today_string

def read_from_results(daily):
    a_json = read_from_json(daily)
    return a_json


def create_message_json(daily_json, daily_count, daily_error, hardcoded_strings, channel="#debug"):
    json_obj = {}
    # Add header_block
    json_obj["channel"] = "{0}".format(channel)
    title = "*Daily Statistics for {today}*".format(today=today_string)

    total_data = daily_json["ALL"]
    raw_rating = daily_json["CONV_AVG"]
    total_count_avg_rated = daily_count["rated"]["ALL"]
    total_count_avg_unrated = daily_count["no_rated"]["ALL"]
    l1d = "*L1D*\n_{rating:0.2f}_ for total _{count}_ conversations.".format(count=raw_rating[0],rating=raw_rating[1])
    json_obj["blocks"] = [add_one_block(title),\
        add_one_block("*Daily Rating For All Module*\n_{daily_rating:0.2f}_ for total _{count}_ responses.\n \
            average_turns_count {daily_count:0.2f}\n\
            average_turns_count_unrated {unrated_count:0.2f}"
        .format(count=total_data[0],daily_rating=total_data[1], daily_count=total_count_avg_rated, unrated_count=total_count_avg_unrated)), add_one_block(l1d)]

    # module_topic = json_obj["attachments"][0]["blocks"]
    blocks = []
    rated_daily_count = daily_count["rated"]
    non_rated_daily_count = daily_count["no_rated"]
    blocks.append(add_one_block("*Module Daily Ratings*"))
    del daily_json["ALL"]
    del daily_json["CONV_AVG"]
    ordered_topics = list(daily_json)
    ordered_error =list(daily_error)

    ordered_topics.sort(key=lambda x: daily_json[x][1], reverse=True)
    less_count = []
    for topic in ordered_topics:
        data = daily_json[topic]
        total_turns_rated = daily_error[topic].get("total_num_turns_rating", 0)
        total_turns_unrated = daily_error[topic].get("total_num_turns_no_rating", 0)
        # num_errors_rated = daily_error[topic].get("num_turns_rating", 0)
        # num_errors_unrated = daily_error[topic].get("num_turns_no_rating", 0)
        num_errors_rated = daily_error[topic].get("crash_rating", 0)
        num_errors_unrated = daily_error[topic].get("crash_no_rating", 0)
        rdc = rated_daily_count.get(topic, 0)
        nrdc = non_rated_daily_count.get(topic, 0)
        block_data = add_one_block("*{module}* - \n\
            *Rated Conversations*\n  \
            average rating: _{rating:0.2f}_ for total _{rated_count}_ responses\n  \
            average _{rated_avg_count:0.2f}_turns\n  \
            num_turns_with_crashes-{ratings_error} for total _{rated_count}_responses \n\
            *Unrated Conversations*\n   \
            average _{non_rated_avg_count:0.2f}_turns\n  \
            num_turns_with_crashes-{norating_error} for total _{unrated_count}_responses\n\
            ".format(module=topic.lower(), \
                rating=data[1], rated_count=total_turns_rated, \
                unrated_count=total_turns_unrated,\
                    rated_avg_count=rdc,\
                    non_rated_avg_count = nrdc,\
                     ratings_error=num_errors_rated, \
                         norating_error=num_errors_unrated))
        if rated_daily_count[topic] <= 4:
            less_count.append(block_data)
        else:
            blocks.append(block_data)
    for block in less_count:
        blocks.append(block)
    
    blocks.append(add_divider())

    # Add Hardcoded Strings
    for strings in hardcoded_strings:
        blocks.append(add_one_block(strings))
    json_obj.update({"attachments": [{"blocks": blocks}]})
    return json_obj

def add_one_block(message):
    return {"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": message,
			}}

def add_divider():
    return 	{"type": "divider"}

# CI_JOB = os.path.join("https://ucdavisnlp.gitlab.io/-/alexaprize/gunrock/-/jobs/", os.getenv("CI_JOB_ID", ""))
# # Generate Artifacts file
# result_integration = lambda form, date, filename: os.path.join(CI_JOB, "artifacts", "data_analytics", "output_files", \
#                 "custom_integration_tests", form, date, filename)
# with open(INTEGRATIONTEST_LOC, 'r') as l:
#     data = json.loads(l.read())
# json_msg = load_integration_test_link(result_integration, data)
# response = client.api_call('chat.postMessage', json=json_msg)

# cloud_watch = get_logs_for_conversation_ids()

# if cloud_watch:
#     result_cloudwatch = os.path.join(os.getenv("CI_JOB_URL"), "artifacts", "browse", "data_analytics", "output_files", \
#             "cloudwatch_logs")
#     data = {
#         'text': "Get <{}|Cloudwatch logs> with `./get_cloudwatch_log.sh [conv_id] [date] ` \n".format(result_cloudwatch) + 
#                 "Example:  `./get_cloudwatch_log.sh 0e599875ed0c7e90301138bfad47ac87c5aec24924ff5daa5ceace7bf7a6ca4a 2019/11/09` ",
#         'channel': "#debug",            
#     }

#     response = client.api_call('chat.postMessage', json=data)

def load_json_obj(json_msg_path):
    with open(json_msg_path, "r") as msg:
        r = json.loads(msg.read())
    return r

def load_integration_test_link(result_integration, data, channels="#debug"):

    attachments = []
    result_json = {"channel": channels, "text": "_Find your integration test cases here._  \n*Input* - input.txt for debugging.  \n*Output* - ASR/TTS result"}
    for conv_id in data:
        files = data[conv_id]
        input_loc = result_integration("inputs", files[0].split("/")[-2], files[0].split("/")[-1])
        output_loc = result_integration("outputs", files[1].split("/")[-2], files[1].split("/")[-1])
        fallback = "Integration Test Generation Disabled"
        input_button = {"type": "button", "text": "Input Files", "url": input_loc}
        output_button = {"type": "button", "text": "Output Files", "url": output_loc, "style": "primary"}

        attachments.append({"fallback": fallback, "text": "Conversation ID : {}...".format(conv_id[:10]), "actions":[input_button, output_button]})

    result_json["attachments"] = attachments
    return result_json

def add_abtest(daily_json):
    result_string = ["\n *AB Testing Result for {0}* \n".format(today_string)]
    for test_case in daily_json:
        count, d_a = daily_json[test_case]
        result_string.append("*{test_case}*  - _{rating:0.2f}_ for _{count}_ responses\n".format(test_case=test_case, count=count, rating=d_a))
    return "".join(result_string)

def add_resp_type(daily_json):
    result_string = ["\n *Resp_Type Result for {0}* \n".format(today_string)]
    for test_case in daily_json:
        count, d_a = daily_json[test_case]
        result_string.append("*{test_case}*  - _{rating:0.2f}_ for _{count}_ responses\n".format(test_case=test_case, count=count, rating=d_a))
    return "".join(result_string)