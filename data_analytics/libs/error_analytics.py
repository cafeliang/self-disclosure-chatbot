"""Upgraded Daily Rating Metrics
"""

import pandas as pd
import numpy as np
import os
import json


from data_analytics.analytics_metadata import DAILY_DATA, DAILY_AB_TEST, DAILY_COUNT, DAILY_ERROR, get_raw_filepath
from data_analytics.data_utils.daily_conversation_generator import imported_run
from data_analytics.libs.utils import read_raw_csv, \
    clean_df, get_error_texts, \
        process_selected_modules, write_to_json, extract_template_keys, extract_errors

def main(filenames, result_json=DAILY_ERROR):
    result = {}
    for key in filenames:
        fnames = filenames[key]
        df = read_raw_csv(fnames)
        if df is not None:
            cleaned_df = clean_df(df)
            result = count_num_errors(cleaned_df, result, header=key)
    if result_json is not None:
        write_to_json(result, result_json)
    return result




    
def count_num_errors(cleaned_df, result={}, header=""):
    for i, row in cleaned_df.iterrows():
        module = process_selected_modules(row["selected_modules"])
        template_keys = extract_template_keys(row["template_keys"])
        resp_type = row["System Response Type (resp_type)"]
        conv_id_key = "conv_id_{}".format(header)
        num_conv_key = "num_turns_{}".format(header)
        crash_key = "crash_{}".format(header)
        total_num_turns_key = "total_num_turns_{}".format(header)
        if module not in result:
            result[module] = {}
        if num_conv_key not in result[module]:
            result[module][num_conv_key] = 0
            result[module][conv_id_key] = {}
            result[module][crash_key] = 0
        errors = extract_errors(template_keys)
        if resp_type == "empty_response":
            result[module][crash_key] += 1
        if total_num_turns_key not in result[module]:
            result[module][total_num_turns_key] = 0
        result[module][total_num_turns_key] += 1
        if len(errors) > 0:
            conv_id = row["conversation_id"]
            if conv_id not in result[module][conv_id_key]:
                result[module][conv_id_key][conv_id] = [errors]
                result[module][num_conv_key] += 1
            else:
                result[module][conv_id_key][conv_id].append(errors)
                result[module][num_conv_key] += 1
    return result


if __name__ == "__main__":
    """ This is executed when run from the command line """
    ratings_result = get_raw_filepath(include_tail=True)
    not_ratings = get_raw_filepath(include_tail=True, include_no_rating=True)
    if not os.path.exists(ratings_result):
        imported_run()
    if not os.path.exists(ratings_result):
        print("Error Analytics")
    main({"rating":ratings_result, "no_rating":not_ratings})