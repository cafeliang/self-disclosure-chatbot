import os
import argparse
import json
from datetime import datetime as dt
import pandas as pd

from data_analytics.data_utils.daily_conversation_generator import main as conversation_generator
from data_analytics.athena_metadata import RATING_PATH, OUTPUT_ROOT

def get_chitchat_state(raw_dict):
    if "S" in raw_dict:
        return raw_dict["S"]
    res = []
    if "S" in raw_dict:
        return raw_dict["S"]
    if "L" in raw_dict:
        for l in raw_dict["L"]:
            if "S" in l:
                res.append(l["S"])
            if "N" in l:
                res.append(l["N"])
        return "_".join(res)
    return None

def write_to_json(json_obj, data_path):
    with open(data_path, 'w') as outfile:
        json.dump(json_obj, outfile)

def chitchat_rating_generator(filepath, result_folder, module, prev_dict={}):
    df = pd.read_csv(filepath)

    # Default Module right now is techscience
    result_dict = prev_dict
    df = pd.read_csv(filepath)
    for ind in range(len(df['selected_modules'])):
        i = df['selected_modules'][ind]

        if type(i) != str:
            i = '[{"S": "None"}]'
        s = json.loads(i)[0]['S']
        if s != module:
            continue

        response = json.loads(df['module_feedback'][ind])[module]["M"]
        
        if "chitchat_state" in response:
            state = get_chitchat_state(response["chitchat_state"])
            if state is not None:
                if state not in result_dict:
                    result_dict.update({state:[0,[],[]]})
                rating = df['ratings'][ind]
                try:
                    next_response_len = len([k for k in df['text'][ind+1].split(" ") if k != ""])
                except:
                    next_response_len = 0
                result_dict[state][0] += 1
                result_dict[state][1].append(rating)
                result_dict[state][2].append(next_response_len)
    if result_folder is None:
        return result_dict
    filename = filepath.split("/")[-1].replace(".csv", ".json")
    result_file = os.path.join(result_folder, filename)
    write_to_json(result_dict, result_file)
    # df = df[df.apply(lambda row: filter_my_module(row, module), axis=1)]
    # result = df[["text", "selected_modules", "returnnlp", "concept"]]
    # result.to_csv(result_file)

def add_args(parser):
    today = dt.today()
    beginning_date_string = "2019-10-28"

    parser.add_argument('--start', '-s', default=today.strftime("%Y-%m-%d"),
                        help='timerange from (YYYY-mm-dd)')
    parser.add_argument('--end', '-e', default=today.strftime("%Y-%m-%d"),
                        help='timerange to (YYYY-mm-dd)')
    parser.add_argument('--data_table', '-d', default="state_table_prod", help="data_table")
    parser.add_argument('--ratings', '-r', default=None, help="Filter by Ratings, default to 5.0")
    parser.add_argument('--module', '-m', default="TECHSCIENCECHAT", help="Filter by Modules")
    parser.add_argument('--force', '-f', default=False, help="Force build joined table")

def main(args):
    result_folder = os.path.join(OUTPUT_ROOT, "unittest_raw")
    if not os.path.exists(result_folder):
        os.mkdir(result_folder)
    _, success_joins, failed_joins = conversation_generator(args)
    for rating_file, no_rating_file in success_joins:
        chitchat_rating_generator(rating_file, result_folder, args.module)
    return None, None, None

if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    msg, _, _ = main(args)