"""Upgraded Daily Rating Metrics
"""

import pandas as pd
import numpy as np
import os


from data_analytics.analytics_metadata import DAILY_DATA, DAILY_AB_TEST, DAILY_COUNT, DAILY_RESP_TYPE, get_raw_filepath
from data_analytics.data_utils.daily_conversation_generator import imported_run
from data_analytics.libs.utils import read_raw_csv, \
    clean_df, generate_module_average, \
        generate_ab_rating, write_to_json, get_module_count, get_resp_type_json


def rated_only(filename):
    df = read_raw_csv(filename)
    if df is not None:
        cleaned_df = clean_df(df)
        avg_json = generate_module_average(cleaned_df)
        ab_test_json = generate_ab_rating(cleaned_df)
        
        resp_type_json = get_resp_type_json(cleaned_df)
        write_to_json(avg_json, DAILY_DATA)
        write_to_json(resp_type_json, DAILY_RESP_TYPE)
        write_to_json(ab_test_json, DAILY_AB_TEST)
        

def both_file_functions(filenames):
    result = {}
    for key in filenames:
        df = read_raw_csv(filenames[key])
        if df is not None:
            cleaned_df = clean_df(df)
            avg_cnt_json = get_module_count(cleaned_df, {})
            result[key] = avg_cnt_json
    write_to_json(result, DAILY_COUNT)

if __name__ == "__main__":
    """ This is executed when run from the command line """
    get_joined_result = get_raw_filepath(include_tail=True)
    not_ratings = get_raw_filepath(include_tail=True, include_no_rating=True)
    if not os.path.exists(get_joined_result) or not os.path.exists(not_ratings):
        imported_run()
    if not os.path.exists(get_joined_result) or not os.path.exists(not_ratings):
        print("Daily Rating Metric Failed")
    rated_only(get_joined_result)
    both_file_functions({"rated":get_joined_result, "no_rated":not_ratings})