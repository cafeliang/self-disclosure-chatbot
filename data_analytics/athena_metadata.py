
import os 
from datetime import datetime as dt
import datetime
import pytz


def get_today(is_string=False, utc=False, date=None):
  if date is None:
    today = dt.today()
  elif type(date) == str:
    date = date + " -0800"
    today = dt.strptime(date, "%Y-%m-%d %z")
  else:
    today = date
  if utc:
    today = today.astimezone(pytz.UTC)
  else:
    today = today.astimezone(pytz.timezone('US/Pacific'))
  
  if is_string:
    return today.strftime("%Y-%m-%d")
  else:
    return today

today_string = get_today(is_string=True)

ANALYTICS_ROOT = os.path.join(os.getenv("COBOT_HOME"), "data_analytics")
OUTPUT_ROOT = os.path.join(ANALYTICS_ROOT, "output_files")
if not os.path.exists(OUTPUT_ROOT):
  os.mkdir(OUTPUT_ROOT)

metadata_2018 = {"s3_bucket":"gunrock-2018-dialog",
                 "athena_db_name": "gunrock2018",
                 "table_name": "dialog2018_full",
                 "s3_output_query_path":"output_queries",
                 "output_csv": os.path.join(ANALYTICS_ROOT, "output_files", "2018_output.csv")

}


metadata_2018_full = {"s3_bucket":"gunrock-2018-dialog",
                 "athena_db_name": "gunrock2018",
                 "table_name": "dialog2018_full",
                 "s3_output_query_path":"output_queries",
                 "output_csv": os.path.join(ANALYTICS_ROOT, "output_files", "2018_output.csv")

}





user_table_beta = {"firehose_s3": "cobot-stack-data-analytics-u-dynamodbstreambucket-17tsqjy7g04ms",
                    "table_name": "user_table_beta",
                    "s3_output_query_path": "user_table_beta",
                    "output_csv": os.path.join(ANALYTICS_ROOT, "output_files", "user_table_beta" +  today_string +".csv")
                    }

user_table_dev = {"firehose_s3": "cobot-stack-data-analytics-u-dynamodbstreambucket-a89fu901izls",
                    "table_name": "user_table_dev",
                    "s3_output_query_path": "user_table_dev",
                    "output_csv": os.path.join(ANALYTICS_ROOT, "output_files", "user_table_dev" +  today_string +".csv")
                    }

user_table_prod = {"firehose_s3": "cobot-stack-data-analytics-u-dynamodbstreambucket-1x5wcy8zld94c",
                    "table_name": "user_table_prod",
                    "s3_output_query_path": "user_table_prod",
                    "output_csv": os.path.join(ANALYTICS_ROOT, "output_files", "user_table_prod" +  today_string +".csv")
                    }



state_table_beta = {"firehose_s3": "cobot-stack-data-analytics-s-dynamodbstreambucket-wbc0vfj5o5sx",
                    "table_name": "state_table_beta3",
                    "s3_output_query_path": "state_table_beta3",
                    "output_csv": os.path.join(ANALYTICS_ROOT, "output_files", "state_table_beta3" +  today_string +".csv")
                    }

state_table_prod = {"firehose_s3": "cobot-stack-data-analytics-s-dynamodbstreambucket-dhb70daylfwh",
                    "table_name": "state_table_prod3",
                    "s3_output_query_path": "state_table_prod3",
                    "output_csv": os.path.join(ANALYTICS_ROOT, "output_files", "state_table_prod3" +  today_string +".csv")
                    }

state_table_dev = {"firehose_s3": "cobot-stack-data-analytics-s-dynamodbstreambucket-tv25ctbbznw2",
                    "table_name": "state_table_dev",
                    "s3_output_query_path": "state_table_dev",
                    "output_csv": os.path.join(ANALYTICS_ROOT, "output_files", "state_table_dev" +  today_string +".csv")
                    }

state_tables = {"state_table_beta": state_table_beta, "state_table_dev":state_table_dev, "state_table_prod":state_table_prod}

metadata_2019 = {"s3_bucket":"gunrock-2019",
                 "athena_db_name": "gunrock2019",
                 "user_tables": [user_table_dev, user_table_beta, user_table_prod],
                 "state_tables": [state_table_dev, state_table_beta, state_table_prod]}


_2019_STATE_TABLE_KEYS = [
  "a_b_test",
  "asr",
  "amazon_topic",
  "sentiment",
  "last_custom_intents",
  "request_type",
  "returnnlp",
  "concept",
  "profanity_check",
  "sentiment2",
  "converttext",
  "conversation_id",
  "candidate_responses",
  "segmentation",
  "text",
  "module_feedback",
  "knowledge",
  "googlekg",
  "topic2",
  "topic_class",
  "session_id",
  "creation_date_time",
  "dialog_act",
  "intent",
  "central_elem",
  "resp_type",
  "template_keys",
  "intent_classify",
  "intent_classify2",
  "nounphrase2",
  "user_id",
  "response",
  "dependency_parsing",
  "selected_modules",
  "topic",
  "npknowledge",
  "sentiment_allennlp",
  "sentence_completion_text",
  "mode",
  "features",
  "pos_tagging",
  "system_acknowledgement",
]

_2018_FULL_STATE_TABLE_KEYS = ["index","session_id",
  "creation_date_time",
  "candidate_response",
  "conversation_id",
  "user_id",
  "a_b_test",
  "text",
  "response",
  "selected_modules",
  "last_custom_intents",
  "topic_class",
  "sentiment",
  "intent_classify",
  "npkknowledge",
  "knowledge",
  "noun_phrase",
  "topic",
  "ner",
  "slots",
  "request_type",
  "intent",
  "asr",
  "rating",
  "day",
   ]

RATING_PATH = os.path.join(OUTPUT_ROOT, "daily_ratings-" + today_string +".csv")

def get_custom_rating_path(date_string):
  return os.path.join(OUTPUT_ROOT, "daily_ratings-" + date_string +".csv")

FEEDBACK_PATH = os.path.join(OUTPUT_ROOT, "daily_feedback-" + today_string +".csv")


