var dom = document.getElementById("container");
var myChart = echarts.init(dom);
var app = {};
option = null;
myChart.showLoading();
$.get('../static/display_rating.json', function (data) {
    myChart.hideLoading();
    // var ratings = data.series.map(function (item) {
    //     return item.daily_rating;
    // });
    console.log("HELLO");
    var selected = data.modules.map(function (item) { return {item:false}; })
    console.log(selected);
    option = {
        title: {
            text: 'Daily Rating per Modules',
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: data.modules,

        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: data.date
        },
        yAxis: {
            type: 'value'
        },
        series: data.series.map(function(item) {
            return {
                name: item.name,
                type: 'line',
                data: item.daily_rating,
                smooth: true,
                hoverAnimation: false,
                symbolSize: 6,
                showSymbol: false
            }
        })
    };

});
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}