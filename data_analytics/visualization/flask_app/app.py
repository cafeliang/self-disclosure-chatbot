from flask import Flask, redirect, url_for, render_template, request
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def home():
    return render_template('home.html')

@app.route('/test1', methods=['GET', 'POST'])
def test1():
    return render_template('home1.html')


@app.route('/test2', methods=['GET', 'POST'])
def test2():
    return render_template('home2.html')

if __name__ == "__main__":
    app.run(debug=True)