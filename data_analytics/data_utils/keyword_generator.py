"""
"""

import datetime
from datetime import datetime as dt
import os
import argparse

def main(args):
    return
def add_args(parser):
    today = dt.today() + datetime.timedelta(days=1)
    beginning_date_string = "2019-10-28"

    parser.add_argument("--keyword", '-k', default="", help="Keyword to retrieve.")

    parser.add_argument('--start', '-s', default=beginning_date_string,
                        help='timerange from (YYYY-mm-dd)')
    parser.add_argument('--end', '-e', default=today.strftime("%Y-%m-%d"),
                        help='timerange to (YYYY-mm-dd)')
    parser.add_argument('--verbose', '-v', default="False"),

    parser.add_argument('--initial_comment', '-c', default="",
                        help='initial comment to send')

    parser.add_argument('--channels', '-l', default="#debug",
                        help='channel to send the message')
    parser.add_argument('--refresh', '-f', default="False",
                        help='reset data if true')

    return



if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    main(args)