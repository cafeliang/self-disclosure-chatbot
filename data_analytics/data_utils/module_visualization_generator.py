"""Get a csv file (daily statistics)

Creates a json file that highlights the path

"{conversation_id: [{"transition": t_init, "state": "s_intro", text: "", response: ""}]}"
"""
import argparse
import json
import pandas as pd
from data_analytics.athena_metadata import OUTPUT_ROOT
import os

sample_file = "/home/karen/proj/alexa/gunrock/data_analytics/output_files/2019-11-27_state_table_prod_TECHSCIENCECHAT_conversations.csv"
output_file = os.path.join(OUTPUT_ROOT, "sample_visualization.json")

def build_json_file(input_file, output_file):
    df = pd.read_csv(input_file)
    result = get_conversation_json_data(df, "TECHSCIENCECHAT")
    with open(output_file, "w") as o:
        json.dump(result, o)
def build_conversation(row):
    # Get text
    text = row["text"]
    s = row["response"]
    # print(response)
    candidate_responses = json.loads(row["candidate_responses"])
    transition = "t_init"
    # transitions = ["t_init"]
    for r in candidate_responses:
        response = candidate_responses[r]
        if 'M' in response:
            transition = response['M']['user_attributes']['M']['techsciencechat']['M']['current_transition']['S']
            break
    return {"text": text, "response": s, "transition": transition}
def get_conversation_json_data(df, module_name):
    history = []
    result = {}
    prev_conv_id = None
    prev_conversation = None
    start_include = False
    for i, row in df.iterrows():
        conv_id = row["conversation_id"]

        if conv_id != prev_conv_id:
            start_include = False
            if prev_conv_id != None:
                process_history(history)
                result[prev_conv_id] = history
                history = []
        
        m = row['selected_modules']

        if type(m) != str:
            m = '[{"S": "None"}]'
            continue
        s = json.loads(m)[0]['S']

        if s == module_name and not start_include:
            start_include = True
            if prev_conversation is not None:
                history.append(prev_conversation)

        conversation = build_conversation(row)
        if start_include:
            history.append(conversation)
        prev_conversation = conversation
        prev_conv_id = conv_id
    process_history(history)
    result[conv_id] = history
    return result

def process_history(history):
    states = ["s_" + d["transition"].split("_")[1] for d in history]
    for i, h in enumerate(history):
        state = states[i]
        if state == "s_init":
            state = "techscience_entry"
        h["state"] = state
def main():
    build_json_file(sample_file, output_file)
    return

def add_args(parser):
    return

if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    # parser = argparse.ArgumentParser()

    # add_args(parser)

    # args = parser.parse_args()
    main()
