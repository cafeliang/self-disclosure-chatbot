# import pandas as pd
# import json
# import numpy
# import os
# import datetime

# from data_analytics.athena_metadata import state_table_prod, RATING_PATH, OUTPUT_JOINED_FILE, OUTPUT_FEEDBACK_FILE, OUTPUT_ROOT, FEEDBACK_PATH, today_string
# from data_analytics.data_utils.utils import move_column_to_front

# PROD3_FILE = state_table_prod["output_csv"]

# def join_feedbacks_table(feedback_path):
#     """
#     feedback_path is manually set until amazon finishes their pipeline
#     """
#     if not os.path.exists(OUTPUT_JOINED_FILE) or not os.path.exists(feedback_path):
#         print("Join Operation Not Run, Please pull the most recent datasets")
#     else:
#         joined_table = pd.read_csv(OUTPUT_JOINED_FILE, index_col=0)
#         feedback_table = pd.read_csv(feedback_path)
#         # Creating a dictionary to faciliate rating query
#         rating_table_dict = feedback_table.set_index("conversation_id").transpose().to_dict()

#         # Filter the table 
#         joined_table = joined_table[joined_table.conversation_id.isin(rating_table_dict)]
#         if joined_table.empty:
#             print("There is no feedback for today - " + today_string)
#             return
#         conversation_feedback = [rating_table_dict[row['conversation_id']]["Additional Feedback"] for i, row in joined_table.iterrows()]

#         joined_table["feedback"] = conversation_feedback

#         joined_table = joined_table.sort_values(["conversation_id","creation_date_time"])
#         ordered_list = ["conversation_id", "session_id", "user_id", "creation_date_time", "ratings", "feedback", "text", "response", "selected_modules"]
#         joined_table = move_column_to_front(joined_table, ordered_list)
        
#         joined_table.to_csv(OUTPUT_FEEDBACK_FILE)

#         print("Find the final joined file at {0}".format(OUTPUT_FEEDBACK_FILE))
#     return

# if __name__ == "__main__":
#     """ This is executed when run from the command line """
#     # debug_mode()
#     feedbacks = os.path.join(os.getenv("COBOT_HOME"), "data_analytics", "output_files", "feedbacks")
#     if not os.path.exists(feedbacks):
#         os.mkdir(feedbacks)
#     join_feedbacks_table(FEEDBACK_PATH) #TODO: Change this to automated after amazon finishes