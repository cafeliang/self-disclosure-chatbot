"""Master Data Retrieval From Athena.

"""

import argparse
import csv
import os
import time
import re

import boto3
import botocore

from datetime import datetime as dt
import datetime

import logging

from data_analytics.athena_metadata import ANALYTICS_ROOT, \
    metadata_2018_full, metadata_2019, _2019_STATE_TABLE_KEYS, state_tables, _2018_FULL_STATE_TABLE_KEYS, RATING_PATH, FEEDBACK_PATH, metadata_2018

from retrying import retry
import pprint


QUERY_BASE = """
                SELECT {col_names} FROM {db_name}.{table_name} {condition} ORDER BY {key_col} DESC;
             """

DATE_TIME_CONDITION = """WHERE creation_date_time BETWEEN date '{beginning_date}' AND date '{end_date}'"""

rating_map = {}
ath_client = boto3.client('athena', region_name='us-east-1')
s3 = boto3.resource('s3', region_name='us-east-1')

@retry(stop_max_attempt_number=10,
       wait_exponential_multiplier=30 * 1000,
       wait_exponential_max=10 * 60 * 1000)
def poll_status(_id):
    '''
    poll query status
    '''
    print("Polling query status...")
    result = ath_client.get_query_execution(
        QueryExecutionId=_id
    )
    logging.info(pprint.pformat(result['QueryExecution']))
    state = result['QueryExecution']['Status']['State']
    if state == 'SUCCEEDED':
        return result
    elif state == 'FAILED':
        return result
    else:
        raise Exception("Poilling failed")

def get_dialog_file(s3_bucket, bucket_path, ath_id, csv_output):
    """Read the dialog csv file from output_s3 bucket. 

    Parameters:
      s3_bucket - bucket name of output of athena query
      bucket_path - location within the bucket where the query file is stored
      ath_id - part of the file name, athena id.
      csv_output - output path of csv file

    """
    ret = s3.meta.client.download_file(
        s3_bucket, os.path.join(bucket_path, str(ath_id) + '.csv'), csv_output)
    return ret

def run_query(query, database, s3_output_bucket, s3_path, csv_output):
    """Run query to athena to extract query

    Parameters:
      query - SQL Query
      database - athena database name
      s3_output_bucket - bucket to write the output query files
      s3_path - path within s3 output bucket to write query files
      csv_output - output path of csv file

    """
    # ath_client = boto3.client('athena')
    s3_output = "s3://{0}/{1}/".format(s3_output_bucket, s3_path)
    response = ath_client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )

    QueryExecutionId = response['QueryExecutionId']
    print("Get QueryExecutionId: {}\n".format(response['QueryExecutionId']))
        
    result = poll_status(QueryExecutionId)
    if isinstance(result, dict) and "Status" in result["QueryExecution"]:
        if result['QueryExecution']['Status']['State'] == 'SUCCEEDED':
            get_dialog_file(s3_output_bucket, s3_path, QueryExecutionId, csv_output)
            print("Find your output file at " + csv_output)
            return result
        print(result["QueryExecution"]["Status"])
    raise Exception("Run Query Failed" + result)

def get_2018_data(full_data=True):
    """Get data from 2018 Query

    Parameters:
      full_data - True if want to retrieve full data
    """

    if full_data:
        metadata_table = metadata_2018_full
    else:
        metadata_table = metadata_2018

    # Format Query:
    table_name = metadata_table['table_name']
    db_name = metadata_table['athena_db_name']
    output_bucket_name = metadata_table['s3_bucket'] # Note the source and query bucket are the same
    s3_path = metadata_table['s3_output_query_path']

    output_csv =  metadata_table["output_csv"]
    col_names = ", ".join(_2018_FULL_STATE_TABLE_KEYS)
    query = QUERY_BASE.format(db_name=db_name, table_name=table_name, col_names=col_names, condition="", key_col="rating")
    try:
        result = run_query(query, db_name, output_bucket_name, s3_path, output_csv)

    except Exception as e:
        logging.ERROR("Get 2018 Data query failed: \n" + str(e))

def get_2019_data(start_time, end_time, table_name="state_table_prod", output_file=None):
    """Get data from 2019 Dynamo DB

    Parameters:
      start_time - start_time/endtime for creation_data_time
      end_time - start/end time for creation_data_time
      table_name - get from a choice of tables
      output_file - location of output file
    """

    s3_bucket = metadata_2019["s3_bucket"]
    db_name = metadata_2019['athena_db_name']



    if table_name is not None:
        if table_name not in state_tables:
            raise Exception("Table Name: {0} not found ".format(table_name))
        else:
            table_metadata = state_tables[table_name]
            if output_file is not None:
                table_metadata["output_csv"] = output_file
            return get_data(table_metadata, start_time, end_time, db_name, s3_bucket)
    for table_meta in metadata_2019["state_tables"]:
        get_data(table_meta, start_time, end_time, db_name, s3_bucket)

def get_ratings():
    """Get ratings from daily feedback
    """
    
    try:
        ret = s3.meta.client.download_file(
            "alexaprize", os.path.join("132518737161", "Ratings", 'ratings' + '.csv'), RATING_PATH)
        print("Find your rating output file at " + RATING_PATH)
        return ret

    except Exception as e:
        print(e)
        print("Get 2019 Datings ratings failed: \n" + str(e))

def get_feedback():
    """Get ratings from daily feedback
    """
    
    try:
        ret = s3.meta.client.download_file(
            "alexaprize", os.path.join("132518737161", 'conversation_feedback' + '.csv'), FEEDBACK_PATH)
        print("Find your feedback output file at " + FEEDBACK_PATH)
        return ret

    except Exception as e:
        print("Get 2019 Daily feedback failed: \n" + str(e))


def get_data(table_meta, start_time, end_time, db_name, output_s3_bucket):
    """Util function to get data from table

    Parameters:
      table_meta - metadata for dynamodb table
      start_time/end_time - start/end time for creation_data_time
      db_name - name of athena database
      output_s3_bucket - the location of athena query output
    """
    
    # firehose_bucket = table_meta["firehose_s3"]
    table_name = table_meta["table_name"]
    s3_path = table_meta['s3_output_query_path']
    output_csv = table_meta["output_csv"]
    col_names = ", ".join(_2019_STATE_TABLE_KEYS)
    date_condition = DATE_TIME_CONDITION.format(beginning_date=start_time, end_date=end_time)
    query = QUERY_BASE.format(db_name=db_name, table_name=table_name, col_names=col_names, condition=date_condition, key_col="creation_date_time")


    try:
        result = run_query(query, db_name, output_s3_bucket, s3_path, output_csv)
        return True
    except Exception as e:
        print("Get 2019 Data query failed: \n" + str(e))
        return False
def add_args(parser):
    today = dt.today() + datetime.timedelta(days=1)
    # parser = argparse.ArgumentParser(description='Search range.')
    yesterday = dt.today() - datetime.timedelta(days=1)
    yesterday = yesterday.replace(hour=9, minute=0, second=0)

    parser.add_argument("--ratings", '-r', default=True, help="Retrieve daily ratings")

    parser.add_argument('--start', '-s', default=yesterday.strftime("%Y-%m-%d"),
                        help='timerange from (YYYY-mm-dd)')
    parser.add_argument('--end', '-e', default=today.strftime("%Y-%m-%d"),
                        help='timerange to (YYYY-mm-dd)')
    parser.add_argument('--output', '-o', default='output.csv',
                        help='output file')

    # parser.add_argument('--update_feedback', default=True, help="Pull directly from amazon feedback")
    return parser

def main():
    parser = argparse.ArgumentParser()
    subparsers =  parser.add_subparsers(dest="version")

    _2018_parser = subparsers.add_parser("2018")
    _2019_parser = subparsers.add_parser("2019")

    add_args(_2018_parser)
    add_args(_2019_parser)

    args = parser.parse_args()

    if not os.path.exists(os.path.join(ANALYTICS_ROOT, "output_files")):
        os.mkdir(os.path.join(ANALYTICS_ROOT, "output_files"))
    
    if args.version == '2018':
        # run 2018 parser
        subparser_args = _2018_parser.parse_known_args()
        get_2018_data()
    else:
        # run 2019 parser
        subparser_args, _ = _2019_parser.parse_known_args()
        get_2019_data(subparser_args.start, subparser_args.end)
        get_ratings()
        get_feedback()


if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    main()