"""Generate Integration Test Cases Per Module

Read from output_files/daily_statistics and create a folder for integration test cases for each module
"""

import pandas as pd
import numpy as np

import json
import os
from datetime import datetime as dt
import datetime
import pprint

from data_analytics.athena_metadata import OUTPUT_JOINED_FILE, today

def get_daily_statistics():
    """Get the dataframe for staticstics file
    """
    return

def generate_integration_tests(df):
    """Given a dataframe, create integration test files
    """

    # Check output_dir exists

    # Separate the dataframe into modules

    # Group each module with conversation ids

    # Associate the rating, module, conversation id to create a file_name

    # Sort the conversation by timestamp and generate a list

    # Create a file with list of conversations
    return

def create_file(filename, list):
    """Given a filename and a list, print list into the file like integration test
    """
    return 

