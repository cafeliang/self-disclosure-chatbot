class MissingPreferredLabel(Exception):
    pass


class UnresolvableMention(Exception):
    pass


class UnknownEntityClass(Exception):
    pass
