import time
import sys
import os
from requests.exceptions import HTTPError, ConnectionError, Timeout

try:
    from cobot_common.service_client import get_client
except ModuleNotFoundError:
    # add cobot_common module to module searching path
    base_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(base_path)
    from cobot_common.service_client import get_client
    from cobot_common.service_client.exceptions import ParamValidationError, UnknownAPIError

"""
Exceptions from input service_client:
1. `cobot_common.service_client.exceptions.ParamValidationError`
2. `cobot_common.service_client.exceptions.UnknownAPIError`

Http Error Exceptions from request package:
1. requests.exceptions.HTTPError
2. requests.exceptions.ConnectionError
3. requests.exceptions.Timeout

To run this example codes, change `API_KEY` at first.
"""

API_KEY = "<API_KEY>"

print(
"""
Invalid Parameter Examples:
"""
)

client = get_client(api_key=API_KEY)

print('Missed required parameter "question" in get_answer():\n')
try:
    r = client.get_answer()
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Missing required parameter in input: "question"
"""

print("Input an empty string:\n")
try:
    r = client.get_answer(question="")
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Invalid length for parameter "question", value: 0, valid range: 1-inf
Parameter validation failed:
"""

print("Input value with invalid type:\n")
try:
    r = client.get_answer(question=None)
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter question, value: None, type: NoneType, valid types: str
"""

print('Missed required parameter "utterances" in batch_detect_topic():\n')
try:
    r = client.batch_detect_topic()
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Missing required parameter in input: "utterances"
"""

print('input an empty array in batch_detect_topic():\n')
try:
    r = client.batch_detect_topic(utterances=[])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid length for parameter "utterances", value: 0, valid range: 1-inf
"""

print('input an array with invalid element in batch_detect_topic():\n')
try:
    r = client.batch_detect_topic(utterances=[None])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter utterances[0], value: None, type: NoneType, valid types: str
"""

print('input an array with invalid element in batch_detect_topic():\n')
try:
    r = client.batch_detect_topic(utterances=[''])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid length for parameter "utterances[0]", value: 0, valid length: 1-inf
"""

print('input an array with invalid element in batch_detect_topic():\n')
try:
    r = client.batch_detect_topic(utterances=['How are you', None])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter utterances[1], value: None, type: NoneType, valid types: str
"""

print('Missed required parameter "utterances" in batch_detect_profanity():\n')
try:
    r = client.batch_detect_profanity()
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Missing required parameter in input: "utterances"
"""

print('input an empty array in batch_detect_profanity():\n')
try:
    r = client.batch_detect_profanity(utterances=[])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid length for parameter "utterances", value: 0, valid range: 1-inf
"""

print('input invalid parameters in batch_detect_profanity():\n')
try:
    r = client.batch_detect_profanity(utterances=[], minConfidence=1.2)
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid length for parameter "utterances", value: 0, valid range: 1-inf
Invalid length for parameter "minConfidence", value: 1.2, valid range: 0-1
"""

print('input an array with invalid element in batch_detect_profanity():\n')
try:
    r = client.batch_detect_profanity(utterances=[None])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter utterances[0], value: None, type: NoneType, valid types: str
"""

print('input an array with invalid element in batch_detect_profanity():\n')
try:
    r = client.batch_detect_profanity(utterances=[''])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid length for parameter "utterances[0]", value: 0, valid length: 1-inf"""

print('input an array with invalid element in batch_detect_profanity():\n')
try:
    r = client.batch_detect_profanity(utterances=['How are you', None])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter utterances[1], value: None, type: NoneType, valid types: str
"""

print('Missed required parameter "utterances" in batch_detect_sentiment():\n')
try:
    r = client.batch_detect_sentiment()
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Missing required parameter in input: "utterances"
"""

print('input an empty array in batch_detect_sentiment():\n')
try:
    r = client.batch_detect_sentiment(utterances=[])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid length for parameter "utterances", value: 0, valid range: 1-inf
"""

print('input an array with invalid element in batch_detect_sentiment():\n')
try:
    r = client.batch_detect_sentiment(utterances=[None])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter utterances[0], value: None, type: NoneType, valid types: str
"""

print('input an array with invalid element in batch_detect_sentiment():\n')
try:
    r = client.batch_detect_sentiment(utterances=[''])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid length for parameter "utterances[0]", value: 0, valid length: 1-inf"""

print('input an array with invalid element in batch_detect_sentiment():\n')
try:
    r = client.batch_detect_sentiment(utterances=['How are you', None])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter utterances[1], value: None, type: NoneType, valid types: str
"""

print('Missed required parameter "conversations" in batch_get_conversation_scores():\n')
try:
    r = client.batch_get_conversation_scores()
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Missing required parameter in input: "conversations"
"""

print('Conversation equals to None in batch_get_conversation_scores():\n')
try:
    r = client.batch_get_conversation_scores(conversations=[None])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter conversations[0], value: None, type: NoneType, valid types: d, i, c, t
"""

print('Empty list in conversations in batch_get_conversation_scores():\n')
try:
    r = client.batch_get_conversation_scores(conversations=[])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid length for parameter "conversations", value: 0, valid length: 1-inf
"""

print('currentUtterance equals to None in conversations in batch_get_conversation_scores():\n')
conversations = [
    {
        "currentUtterance": None,
        "currentUtteranceTopic": "Phatic",
        "currentResponse": "I see anyway I'll be glad to tell you some more news later if you want but Now I have a question for you Patrick would you help me solve a great mystery",
        "currentResponseTopic": "News",
        "pastUtterances": ["Alexa let's chat",
                           "my name is Patrick"],
        "pastResponses": ["hello this is an Alexa Prize socialbot I don't think we have met what's your name my friend",
                          "hey Patrick it's a pleasure to meet you well how is it going"]
    }
]
try:
    r = client.batch_get_conversation_scores(conversations=conversations)
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter conversations[0].currentUtterance, value: None, type: NoneType, valid types: str
"""

print('Missing currentUtterance in conversations in batch_get_conversation_scores():\n')
conversations = [
    {
        "currentUtteranceTopic": "Phatic",
        "currentResponse": "I see anyway I'll be glad to tell you some more news later if you want but Now I have a question for you Patrick would you help me solve a great mystery",
        "currentResponseTopic": "News",
        "pastUtterances": ["Alexa let's chat",
                           "my name is Patrick"],
        "pastResponses": ["hello this is an Alexa Prize socialbot I don't think we have met what's your name my friend",
                          "hey Patrick it's a pleasure to meet you well how is it going"]
    }
]
try:
    r = client.batch_get_conversation_scores(conversations=conversations)
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Missing required parameter in conversations[0]: "currentUtterance"
"""

print('Missed required parameter "conversations" in batch_get_dialog_act_intents():\n')
try:
    r = client.batch_get_dialog_act_intents()
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Missing required parameter in input: "conversations"
"""

print('Conversation equals to None in batch_get_dialog_act_intents():\n')
try:
    r = client.batch_get_dialog_act_intents(conversations=[None])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter conversations[0], value: None, type: NoneType, valid types: d, i, c, t
"""

print('Empty list in conversations in batch_get_conversation_scores():\n')
try:
    r = client.batch_get_dialog_act_intents(conversations=[])
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid length for parameter "conversations", value: 0, valid length: 1-inf
"""

print('currentUtterance equals to None in conversations in batch_get_dialog_act_intents():\n')
conversations = [
    {
        "currentUtterance": None,
        "currentUtteranceTopic": "Phatic",
        "pastUtterances": ["Alexa let's chat",
                           "my name is Patrick"],
        "pastResponses": ["hello this is an Alexa Prize socialbot I don't think we have met what's your name my friend",
                          "hey Patrick it's a pleasure to meet you well how is it going"]
    }
]
try:
    r = client.batch_get_dialog_act_intents(conversations=conversations)
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Invalid type for parameter conversations[0].currentUtterance, value: None, type: NoneType, valid types: str
"""

print('Missing currentUtterance in conversations in batch_get_dialog_act_intents():\n')
conversations = [
    {
        "currentUtteranceTopic": "Phatic",
        "pastUtterances": ["Alexa let's chat",
                           "my name is Patrick"],
        "pastResponses": ["hello this is an Alexa Prize socialbot I don't think we have met what's your name my friend",
                          "hey Patrick it's a pleasure to meet you well how is it going"]
    }
]
try:
    r = client.batch_get_dialog_act_intents(conversations=conversations)
except ParamValidationError as e:
    print('%s\n-----' % e)
"""
Output:
Parameter validation failed:
Missing required parameter in conversations[0]: "currentUtterance"
"""

# Unknown Api Error
print(
"""
Unknown API Error Examples:
"""
)
client = get_client(api_key=API_KEY)

try:
    r = client.batch_detect_topics(utterances=[
        "Rufus"
    ])
except UnknownAPIError as e:
    print('%s\n-----' % e)
"""
Output:
Unknown API "batch_detect_topics". Must be one of: ['batch_detect_profanity', 'get_answer', 'batch_detect_topic']
"""


# HTTP failure
print(
"""
HTTP Error Examples:
"""
)
client = get_client()

try:
    r = client.get_answer(question="who are you")
except (HTTPError, ConnectionError, Timeout) as e:
    print('%s\n-----' % e)
"""
Output:
Http request failed:
http status code: 403, error message: Forbidden
"""