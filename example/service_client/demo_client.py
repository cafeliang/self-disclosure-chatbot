import requests
import json
import sys
import os
try:
    from cobot_common.service_client import get_client
except ModuleNotFoundError:
    # add cobot_common module to module searching path
    base_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(base_path)
    from cobot_common.service_client import get_client

"""
This module contains a few of examples about how to use toolkit service client.
The source code of service client is found in cobot_common/service_client/

Please change `API_KEY` to your API key before running this example code.
"""

# Initialize service client
# Timeout parameter is optional and the default value is 1000.

client = get_client(api_key='EwecQ1fkEd4YMVUOHHpeSam7zbef53gipQsxAUki', timeout_in_millis=1000)


def test_evi():
    # Call Evi service
    # Input is a text. Timeout parameter is optional and the client timeout value is used if it's not set.
    r = client.get_answer(question="good do you know when is columbus day today", timeout_in_millis=1000)
    print(json.dumps(r, indent=1))
    """
    OUTPUT schema:
    
    {
        'response': "I'm Alexa and I'm designed around your voice. I can provide information, music, news, weather, and more."
    }
    """


def test_topic_detector():
    # Call topic classifier service
    # Input is a list of text
    # All topic label text constant stays at `topic_label.text`
    r = client.batch_detect_topic(utterances=[
        "let's chat about the relationship between north korea and south korea",
    ])
    print(json.dumps(r, indent=1))
    """
    OUTPUT schema:
    
    {
        "version":"2.0",
        "topics":[
            {
                "topicClass":"Politics", // All topic label text constant stays at `topic_label.text`
                "confidence":0.855,      // confidence = [0,1] if it's from statistical model; confidence = 999.0, if it's from rule. 
                "text":"who are you",
                'topicKeywords': [{'confidence': 0.182, 'keyword': 'North Korea'}, {'confidence': 0.235, 'keyword': 'South Korea'}]
            }
        ],
        "requestId":"123456"
    }
    """

def test_profanity_classifier():
    # Call profanity classifier service
    # Input is a list of text
    r = client.batch_detect_profanity(utterances=[
        "i hate you"
    ])
    print(json.dumps(r, indent=1))
    """
    OUTPUT schema:
    
    {
        "version":"1.0",
        "offensivenessClasses":[
            {
                "text":"i hate you",
                "overallClass":0, // 0: not profanity, 1: profanity, The combination OR result of blacklist and statistical model. If one of the source returns "1", it will be "1". 
                "values":[
                    {
                        "offensivenessClass":0,
                        "confidence":"1",
                        "source":"blacklist"
                    },
                    {
                        "offensivenessClass":0,
                        "confidence":"0.35981",
                        "source":"statistical_model"
                    }
                ]
            }
        ],
        "requestId":"123456"
    }
    """

def test_batch_get_conversation_scores():
    # Call conversation evaluation service
    # Input is a json blob
    r = client.batch_get_conversation_scores(conversations=[
        {
            "currentUtterance": "no thank you",
            "currentUtteranceTopic": "Phatic",
            "currentResponse": "I see anyway I'll be glad to tell you some more news later if you want but Now I have a question for you Patrick would you help me solve a great mystery",
            "currentResponseTopic": "News",
            "pastUtterances": ["Alexa let's chat",
                               "my name is Patrick"],
            "pastResponses": ["hello this is an Alexa Prize socialbot I don't think we have met what's your name my friend",
                              "hey Patrick it's a pleasure to meet you well how is it going"]
        },
        {
            "currentUtterance": "Hello",
            "currentUtteranceTopic": "Unknown",
            "currentResponse": "Hi",
            "pastUtterances": [],
            "pastResponses": []
        }
    ], timeout_in_millis=3000)
    print(json.dumps(r, indent=1))
    """
    The scores are the probabilities (0 to 1). 
    The higher the probability the more coherent, engaging or successful the response is for a given user utterance.
    
    OUTPUT schema:
    
    {
      "version": "1.0",
      "conversationEvaluationScores": [
        {
          "isResponseOnTopic": 0.511,
          "isResponseInteresting": 0.137,
          "responseEngagesUser": 0.73,
          "isResponseErroneous": 0.054,
          "isResponseComprehensible": 0.892
        },
        {
          "isResponseOnTopic": 0.324,
          "isResponseInteresting": 0.08,
          "responseEngagesUser": 0.233,
          "isResponseErroneous": 0.327,
          "isResponseComprehensible": 0.649
        }
      ],
      "requestId": "123456"
    }
    """

def test_get_dialog_act_intents():
    r = client.batch_get_dialog_act_intents(conversations=[
        {
            "currentUtterance": "let's talk about the review",
            "pastUtterances": ["i'm fine let's talk about movies",
                              "i want to talk about wonder woman"],
            "pastResponses": ["'here are some movies i would like to recommend, i dream in another language, wonder woman, valerian and the city of a thousand planets, despicable me 3, wolf warrior ii   i still got some most recent movie information from rotten tomatoes, would you like to talk about that",
                               "i have some information about the movie wonder woman, plot, rating, author, review, actor, director  which one would you like to know"]
        },
        {
            "currentUtterance": "i'm fine let's talk about movies",
            "pastUtterances": [],
            "pastResponses": []
        }
    ])
    print(json.dumps(r, indent=1))
    """
    Dialog Acts can be considered as general intents, which are common to Goal-directed or Non-task oriented dialogs, Single or Multi-turn dialogs, or even open-domain or social conversations. 
    Types of dialog acts include a question, a statement, or a request for action. Dialog acts are a type of speech act.
    
    https://en.wikipedia.org/wiki/Dialog_act
    
    Currently, the model predicts one of the following 11 classes:
    1. ClarificationIntent
    2. General_ChatIntent
    3. Information_DeliveryIntent
    4. Information_RequestIntent
    5. InteractiveIntent
    6. Multiple_GoalsIntent
    7. Opinion_ExpressionIntent
    8. Opinion_RequestIntent
    9. OtherIntent
    10. Topic_SwitchIntent
    11. User_InstructionIntent
    
    We are also providing topic classification based on dialog context along with dialogAct Intent
    
    OUTPUT schema:
    
    {
     "version": "2.0",
     "requestId": "bb719b4b-6876-49a0-a865-487575e73de4",
     "dialogActIntents": [
      {
       "topic": "Entertainment_Movies",
       "dialogActIntent": "Information_RequestIntent"
      },
      {
       "topic": "Entertainment_Movies",
       "dialogActIntent": "Information_RequestIntent"
      }
     ]
    }
    """




"""
Knowledge Query API -  AIQL (Alexa Information Query Language Examples)
Use cases - Topic expansion, knowledge graph queries etc.
"""


def test_query_movie_star():
    """""""""""""""""""""""""""""""""
    Example 1
    """""""""""""""""""""""""""""""""""
    """"
    query a
    a <aio:isAStarIn> m
    
    """
    query = {"text": "query a|a <aio:isAStarIn> m"}
    variableBindings = [{"variable": "m", "dataType": "aio:Entity", "value": "wikidata:q104123"}]
    r=client.get_knowledge_query_answer(query=query, variableBindings=variableBindings ,timeout_in_millis=2000)
    print(json.dumps(r, indent=1))

    """
    OUTPUT schema:
    {
        "results": [
            {
                "bindingList": [
                    {
                        "variable": "a",
                        "dataType": "aio:Entity",
                        "value": "aie:bruce_willis"
                    }
                ]
            },
            {
                "bindingList": [
                    {
                        "variable": "a",
                        "dataType": "aio:Entity",
                        "value": "aie:harvey_keitel"
                    }
                ]
            },
            {
                "bindingList": [
                    {
                        "variable": "a",
                        "dataType": "aio:Entity",
                        "value": "aie:tim_roth"
                    }
                ]
            }
        ],
        "status": "COMPLETENESS_UNKNOWN",
        "requestId": "badce4a6-ae86-11e9-a113-b9fc822ced01"
    }

    """

def test_movies_bruce_willis_stars_in():
    """""""""""""""""""""
    Example 2
    """"""""""""""""""""
    """"""""""""""""""""
    query m
    <aie:bruce_willis> <aio:isAStarIn> m
    m <aio:isAnInstanceOf> <aio:Movie>
    """
    query = {"text": "query m|<aie:bruce_willis> <aio:isAStarIn> m| m <aio:isAnInstanceOf> <aio:Movie>"}
    r=client.get_knowledge_query_answer(query=query ,timeout_in_millis=2000)
    print(json.dumps(r, indent=1))


"""
Knowledge Entity Resolution API
Description - The entity resolution endpoint returns the identifiers of the entities that can be referred to by the specified string (mention). 
It enables clients to find out what entity identifiers (resolved entities) to include in their knowledge query.
"""

def test_entity_resolution_director():
    """""""""""""""""""""""""""""""""
    Example 1
    """""""""""""""""""""""""""""""""""

    mention = { "text": "spielberg"}
    classConstraints = [
        {
          "dataType": "aio:Entity",
          "value": "aio:FilmDirector"
        }
      ]
    r=client.get_knowledge_entity_resolution(mention=mention, classConstraints=classConstraints ,timeout_in_millis=2000)
    print(json.dumps(r, indent=1))

    """
    OUTPUT schema:
    {
     "resolvedEntities": [
      {
       "dataType": "aio:Entity",
       "value": "aie:steven_spielberg"
      }
     ],
     "requestId": "ddf8ea7c-bee0-11e9-a6be-614e6738ec23"
    }
    """


def test_book_name():
    # Set the natural language text referring to the unresolved entity
    mention = {"text": "harry potter"}

    # Constrain the search to books
    classConstraints = [
        {
            "dataType": "aio:Entity",
            "value": "aio:Book"
        }
    ]
    # Send the request to the entity resolution service
    response = client.get_knowledge_entity_resolution(mention=mention, classConstraints=classConstraints)

    # Dump the content of the response to standard output
    print(response)


def test_author_name():
    # Set the natural language text referring to the unresolved entity
    mention = {"text": "emily griffin"}

    # Constrain the search to book author
    classConstraints = [
        {
            "dataType": "aio:Entity",
            "value": "aio:BookAuthor"
        }
    ]
    # Send the request to the entity resolution service
    response = client.get_knowledge_entity_resolution(mention=mention, classConstraints=classConstraints)

    # Dump the content of the response to standard output
    print(response)


if __name__ == '__main__':
    test_author_name()
