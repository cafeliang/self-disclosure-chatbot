import sys
from os import path
import re
import json

try:
    from cobot_common.data_processing import Cleaner
except ModuleNotFoundError:
    base_path = path.dirname(path.dirname(path.dirname(path.abspath(__file__))))
    sys.path.append(base_path)

from cobot_common.data_processing.components import ProfanityChecker
from cobot_common.data_processing.annotator import Annotator
from cobot_common.data_processing import Cleaner

source_file_path = path.join(path.dirname(path.abspath(__file__)), 'demo_source_file.txt')
cleaner = Cleaner(file_path=source_file_path)

demo_blacklist_path = path.join(path.dirname(path.abspath(__file__)), 'demo_blacklist.txt')
cleaned_text_list = cleaner.clean_text() \
       .scrub_with_blacklist(demo_blacklist_path) \
       .result()

result = Annotator(text_list=cleaned_text_list, api_key='<API_KEY>') \
    .detect_ner() \
    .detect_topic() \
    .detect_profanity() \
    .detect_sentiment() \
    .output('<Absolute_Output_File_Path>') 