import sys
import os

try:
    from cobot_common.data_processing import Cleaner
except ModuleNotFoundError:
    base_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(base_path)
    from cobot_common.data_processing import Cleaner

source_file_path = path.join(path.dirname(path.abspath(__file__)), 'demo_source_file.txt')
cleaner = Cleaner(file_path=source_file_path)

demo_blacklist_path = path.join(path.dirname(path.abspath(__file__)), 'demo_blacklist.txt')
cleaner.clean_text() \
       .scrub_with_blacklist(demo_blacklist_path) \
       .output('<Absolute_Output_File_Path>')
