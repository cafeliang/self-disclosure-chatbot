import logging
from cobot_core import DialogManager
from cobot_core import ABTest
# from sample_cobot.sample_events import new_session_event


# Add your custom Dialog Manager A here
class CustomDialogManager_a(DialogManager):
    def select_response(self, feature):
        return "custom dialog manager a"


# Add your custom Dialog Manager B here
class CustomDialogManager_b(DialogManager):
    def select_response(self, feature):
        return "custom dialog manager b"


# Add Dialog Manager A to overrides for Cobot A
def overrides_a(binder):
    binder.bind(DialogManager, to=CustomDialogManager_a)


# Add Dialog Manager B to overrides for Cobot B
def overrides_b(binder):
    binder.bind(DialogManager, to=CustomDialogManager_b)


def lambda_handler(event, context):
    # Create config for test case A
    config_a = {'name': 'A',
                'ratio': 50,
                'overrides': overrides_a}

    # Create config for test case B
    config_b = {'name': 'B',
                'ratio': 50,
                'overrides': overrides_b}

    # Create list of configs for each test case
    test_case_config = [config_a, config_b]

    # Create a new AB Test with config_a and config_b
    abtest = ABTest(event=event,
                    context=context,
                    app_id=None,
                    user_table_name='UserTable',
                    save_before_response=True,
                    state_table_name='StateTable',
                    test_case_config=test_case_config,
                    level='conversation',
                    api_key=None
                    )

    # Run the AB Test and return the result
    return abtest.run()


# if __name__ == '__main__':
#     lambda_handler(event=new_session_event, context={})
