import logging
from cobot_core.service_module import LocalServiceModule
from cobot_core import ABTest
# from sample_cobot.sample_events import new_session_event


# Add your custom NLP Module A here
class CustomNLPModule_a(LocalServiceModule):
    def execute(self):
        return "custom nlp module a"


# Add your custom NLP Module B here
class CustomNLPModule_b(LocalServiceModule):
    def execute(self):
        return "custom nlp module b"


def lambda_handler(event, context):
    # Add the definition for NLP Module A here
    NLPModule_a = {
        'name': "NLP_A",
        'class': CustomNLPModule_a,
        'url': 'local'
    }

    # Add the definition for NLP Module B here
    NLPModule_b = {
        'name': "NLP_B",
        'class': CustomNLPModule_b,
        'url': 'local'
    }

    # Add NLP Module A to list of NLP module definitions for Cobot A.
    # Format is a List of List of module definitions.
    # Modules in a previous list run before modules in a later list.
    # [['a', 'b'],['c']] => module a and b are run in parallel before module c,
    # and module c can have access to module a and b's outputs.
    # To include a default module, add a definition with only the 'name' key
    # and one of the default values: “intent”, “ner”, “sentiment”, “topic”.
    nlp_modules_a = [[NLPModule_a]]

    # Add NLP Module B to list of NLP module definitions for Cobot B.
    nlp_modules_b = [[NLPModule_b]]

    # Create config for test case A
    config_a = {'name': 'A',
                'ratio': 50,
                'nlp_modules': nlp_modules_a}

    # Create config for test case B
    config_b = {'name': 'B',
                'ratio': 50,
                'nlp_modules': nlp_modules_b}

    # Create list of configs for each test case
    test_case_config = [config_a, config_b]

    # Create a new AB Test with config_a and config_b
    abtest = ABTest(event=event,
                    context=context,
                    app_id=None,
                    user_table_name='UserTable',
                    save_before_response=True,
                    state_table_name='StateTable',
                    test_case_config=test_case_config,
                    level='conversation',
                    api_key=None
                    )

    # Run the AB Test and return the result
    return abtest.run()


# if __name__ == '__main__':
#     lambda_handler(event=new_session_event, context={})
