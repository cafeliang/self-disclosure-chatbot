import logging
from cobot_core.service_module import LocalServiceModule
from cobot_core import ABTest
# from sample_cobot.sample_events import new_session_event


# Add your custom Response Generator A here
class CustomResponseGenerator_a(LocalServiceModule):
    def execute(self):
        return "custom response generator a"


# Add your custom Response Generator B here
class CustomResponseGenerator_b(LocalServiceModule):
    def execute(self):
        return "custom response generator b"


# Add your custom Response Generator C here
class CustomResponseGenerator_c(LocalServiceModule):
    def execute(self):
        return "custom response generator c"


def lambda_handler(event, context):
    # Add the definition for Response Generator A here
    ResponseGenerator_a = {
        'name': "RG_A",
        'class': CustomResponseGenerator_a,
        'url': 'local'
    }

    # Add the definition for Response Generator B here
    ResponseGenerator_b = {
        'name': "RG_B",
        'class': CustomResponseGenerator_b,
        'url': 'local'
    }

    # Add the definition for Response Generator C here
    ResponseGenerator_c = {
        'name': "RG_C",
        'class': CustomResponseGenerator_c,
        'url': 'local'
    }

    # Add Response Generator A to list of response generator definitions for Cobot A
    response_generators_a = [ResponseGenerator_a]

    # Add Response Generator B to list of response generator definitions for Cobot B
    response_generators_b = [ResponseGenerator_b]

    # Add Response Generator C to list of response generator definitions for Cobot C
    response_generators_c = [ResponseGenerator_c]

    # Create config for test case A
    config_a = {'name': 'A',
                'ratio': 33,
                'response_generators': response_generators_a}

    # Create config for test case B
    config_b = {'name': 'B',
                'ratio': 33,
                'response_generators': response_generators_b}

    # Create config for test case C
    config_c = {'name': 'C',
                'ratio': 34,
                'response_generators': response_generators_c}

    # Create list of configs for each test case
    test_case_config = [config_a, config_b, config_c]

    # Create a new AB Test with config_a, config_b, and config_c
    abtest = ABTest(event=event,
                    context=context,
                    app_id=None,
                    user_table_name='UserTable',
                    save_before_response=True,
                    state_table_name='StateTable',
                    test_case_config=test_case_config,
                    level='conversation',
                    api_key=None
                    )

    # Run the AB Test and return the result
    return abtest.run()


# if __name__ == '__main__':
#     lambda_handler(event=new_session_event, context={})
