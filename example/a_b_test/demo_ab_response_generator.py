import logging
from cobot_core.service_module import LocalServiceModule
from cobot_core import ABTest
# from sample_cobot.sample_events import new_session_event


# Add your custom Response Generator A here
class CustomResponseGenerator_a(LocalServiceModule):
    def execute(self):
        return "custom response generator a"


# Add your custom Response Generator B here
class CustomResponseGenerator_b(LocalServiceModule):
    def execute(self):
        return "custom response generator b"


def lambda_handler(event, context):
    # Add the definition for Response Generator A here
    ResponseGenerator_a = {
        'name': "RG_A",
        'class': CustomResponseGenerator_a,
        'url': 'local'
    }

    # Add the definition for Response Generator B here
    ResponseGenerator_b = {
        'name': "RG_B",
        'class': CustomResponseGenerator_b,
        'url': 'local'
    }

    # Add Response Generator A to list of response generator definitions for Cobot A
    response_generators_a = [ResponseGenerator_a]

    # Add Response Generator B to list of response generator definitions for Cobot B
    response_generators_b = [ResponseGenerator_b]

    # Create config for test case A
    config_a = {'name': 'A',
                'ratio': 50,
                'response_generators': response_generators_a}

    # Create config for test case B
    config_b = {'name': 'B',
                'ratio': 50,
                'response_generators': response_generators_b}

    # Create list of configs for each test case
    test_case_config = [config_a, config_b]

    # Create a new AB Test with config_a and config_b
    abtest = ABTest(event=event,
                    context=context,
                    app_id=None,
                    user_table_name='UserTable',
                    save_before_response=True,
                    state_table_name='StateTable',
                    test_case_config=test_case_config,
                    level='conversation',
                    api_key=None
                    )

    # Run the AB Test and return the result
    return abtest.run()


# if __name__ == '__main__':
#     lambda_handler(event=new_session_event, context={})
