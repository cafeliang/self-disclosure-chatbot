from sagemaker.mxnet import MXNet
import boto3
from cobot_common.vectorize import Vectorizer
import numpy


##########
# Data Preparation
##########
intent_map = { 'QAIntent':'EVI', 'greet':'GREETER', 'yes_intent':'GREETER', 'topic_request':'GREETER', 'more_info_request':'GREETER',
               'no_intent':'GREETER', 'mood_great':'GREETER', 'mood_unhappy':'GREETER', 'love':'GREETER', 'hate':'GREETER', 'opinion_request':'GREETER',
               'frustration':'GREETER', 'error':'GREETER' }
turns = []
for i in intent_map.keys():
    turns.append([i,intent_map[i]])
#turns = [ ["greet","GREETER"], ["topic_request","GREETER"], ["QAIntent","EVI"], ["more_info_request",""] ]
vectorizer = Vectorizer(['EVI','GREETER'],model_file="cobot-skill/models/en-US.json")
(inputs,outputs) = vectorizer.vectorize_turns(turns)
input_arr = numpy.array(inputs)
output_arr = numpy.array(outputs)

numpy.savez('train.npz',inputs=input_arr,outputs=output_arr)
numpy.savez('test.npz',inputs=input_arr,outputs=output_arr)


##########
# Model Training
##########
train = open('train.npz', 'rb')
test = open('test.npz', 'rb')
s3 = boto3.resource('s3')
s3.Bucket('rcg-cobot-sagemaker-data').put_object(Key='train.npz', Body=train)
s3.Bucket('rcg-cobot-sagemaker-data').put_object(Key='test.npz', Body=test)

mxnet_estimator = MXNet("dialog_model.py", role="SageMakerRole", train_instance_type="ml.m4.xlarge", train_instance_count=1)
mxnet_estimator.fit("s3://rcg-cobot-sagemaker-data/")

predictor = mxnet_estimator.deploy(instance_type="ml.m4.xlarge",
                                   initial_instance_count=1,
                                   endpoint_name="sagemaker-dialog-model-test")

data = numpy.load(open('train.npz', 'rb'))
inputs = data['inputs']
outputs = data['outputs']

for row in zip(inputs,outputs):
    print(row[0].tolist())
    print(row[1].tolist())
    print(predictor.predict([row[0].tolist()]))
    
