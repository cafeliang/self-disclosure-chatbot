from rake_nltk import Rake
import random

required_context = ['intent','slots']

def get_required_context():
    return required_context

RAKE = Rake()
REDDIT_DATA_PATH = "data/reddit_retrieval_data_clean.txt"

def process_user_utterance(utterance):
    RAKE.extract_keywords_from_text(utterance)
    keywords = RAKE.get_ranked_phrases()
    to_remove = set(["chat", "talk", "let"])
    result = []
    for item in list(set(keywords) - to_remove):
        result = result + item.split()
    return result

def retrieve_subreddits(utterance, entities=None):
    if not entities:
        entities = process_user_utterance(utterance)
    keywords_set = set(entities)
    # print(keywords_set)
    reddit_data = open(REDDIT_DATA_PATH, "r")
    best_score = 0.5
    best_resp = None
    utterances_with_keywords = set()
    for sent in reddit_data:
        #match_ratio = fuzz.ratio(utterance, sent)
        match_ratio = len(keywords_set.intersection(set(sent.split())))
        if match_ratio > best_score and len(sent.split()) < 25 and len(sent.split()) >5:
            #best_score = match_ratio
            #best_resp = sent
            utterances_with_keywords.add((sent,match_ratio))

    utterances_with_keywords = sorted(utterances_with_keywords,key=lambda x: x[1], reverse=True)[0:20]
    # print(utterances_with_keywords)
    if utterances_with_keywords and len(utterances_with_keywords) > 0:
        return random.choice(list(utterances_with_keywords))[0]
    else:
        # fallback response if it cannot find any utterance matches with keywords
        return "Sorry, I don't know how to respond to that. Can we talk about other topics, like movies, sports, or news?"


def is_black(response, black_list):
    for word in black_list:
        if word in response.split():
            return True
    return False

def handle_message(msg):
    utterance = msg['slots']['text']['value']
    response_is_black = True
    response = None
    entities = None
    black_list = set(["fuck", "fuk", "sex", "idiot", "maniac", "dick", "butt", "fucking"])
    while response_is_black:
        response = retrieve_subreddits(utterance, entities)
        # TODO: replace with data-api's profanity filter code
        if not is_black(response, black_list):
            return response
