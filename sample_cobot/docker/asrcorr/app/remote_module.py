import re
import ast
from phonetics import dmetaphone
from fuzzywuzzy import fuzz
import json

required_context = ['text', 'nounphrases', 'asr']
asr_threshold = 0.4
fuzzy_match_threshold = 0.75

artist_phonetics_file = open("artist_phonetics", "r")
games_phonetics_file = open("games_phonetics", "r")
movies_phonetics_file = open("movies_phonetics", "r")
sports_phonetics_file = open("sports_phonetics", "r")

artist_phonetics = json.loads(ast.literal_eval(artist_phonetics_file.read()))
games_phonetics = json.loads(ast.literal_eval(games_phonetics_file.read()))
movies_phonetics = json.loads(ast.literal_eval(movies_phonetics_file.read()))
sports_phonetics = json.loads(ast.literal_eval(sports_phonetics_file.read()))

def get_required_context():
    return required_context

def handle_message(msg):
    if msg['asr']:
        if msg['asr'][0]['confidence'] < float(asr_threshold) and msg['asr'][0]['confidence'] > 0:
            input_text = msg['text']

            match = {}

            category_pattern = r"(\bsport|\bmovie) (\btitled|\bcalled)|\bgame (\btitled|\bcalled)|\bbook (\btitled|\bcalled)|\bmovie|\bgame|\bbook|\bsinger (\bis|\bcalled)|\bsinger|\bartist|\bsongwriter"

            filter_pattern = r"^\bsome\b|^\banother\b|\btwice\b"

            regex_symbol = r"about|movie|book|sport|singer is|artist is|watch|watched|like|love|listen to|my.*is"

            noun_phrases = None
            # filter empty strings
            try:
                noun_phrases = [noun_phrase for noun_phrase in msg['nounphrases'][0]['user_input_noun_phrases'] if noun_phrase]
            except KeyError:
                noun_phrases = [noun_phrase for noun_phrase in msg['nounphrases']['user_input_noun_phrases'] if noun_phrase]

            try:
                # regex to locate entity names if they weren't caught by noun phrase module
                if re.search(regex_symbol, input_text[0].lower()):
                    potential_clean_np = input_text[0].lower()[re.search(regex_symbol, input_text[0].lower()).end() + 1:]
                    potential_clean_np = re.sub(filter_pattern, "", potential_clean_np).strip()
                    if potential_clean_np not in noun_phrases:
                        noun_phrases.append(potential_clean_np)

                # get phonetics for each NP
                category = ''
                for i in range(len(noun_phrases)):
                    if re.search(category_pattern, noun_phrases[i]):
                        if not category:
                            category = re.search(category_pattern, noun_phrases[i]).group(0)
                            if len(re.sub(category_pattern, "!@#", noun_phrases[i]).split("!@#")[1].strip()) > 0:
                                noun_phrases[i] = re.sub(category_pattern, "!@#", noun_phrases[i]).split("!@#")[1].strip()

                phonetics_dict_current_input = get_phonetics(noun_phrases)

                # first check the phonetics against those of the last turn
                last_state_noun_phrases = None
                try:
                    last_state_noun_phrases = [noun_phrase for noun_phrase in msg['nounphrases'][0]['bot_response_noun_phrases'] if noun_phrase]
                except KeyError:
                    last_state_noun_phrases = [noun_phrase for noun_phrase in msg['nounphrases']['bot_response_noun_phrases'] if noun_phrase]

                phonetics_dict_previous_response = get_phonetics(last_state_noun_phrases)
                match_with_previous = match_phonetics(phonetics_dict_current_input, phonetics_dict_previous_response, only_response=True)
                if match_with_previous:
                    # check if a previous noun phrase matches (because it would be the most likely candidate)
                    return match_with_previous

                # then check that phonetic against the phonetics of artists, games, movies, sports
                match = None
                if "sport" in category:
                    match = remove_match_scores(match_phonetics(phonetics_dict_current_input, sports_phonetics))
                elif "movie" in category:
                    match = remove_match_scores(match_phonetics(phonetics_dict_current_input, movies_phonetics))
                elif "game" in category:
                    match = remove_match_scores(match_phonetics(phonetics_dict_current_input, games_phonetics))
                elif "singer" in category or "artist" in category:
                    match = remove_match_scores(match_phonetics(phonetics_dict_current_input, artist_phonetics))
                else:
                    match = {}
                    matches = [match_phonetics(phonetics_dict_current_input, sports_phonetics),
                            match_phonetics(phonetics_dict_current_input, movies_phonetics),
                            match_phonetics(phonetics_dict_current_input, games_phonetics),
                            match_phonetics(phonetics_dict_current_input, artist_phonetics)]

                    # pick the one with the largest match score from any category if none chosen
                    for match_dict in matches:
                        for key in match_dict.keys():
                            if key not in match:
                                match[key] = match_dict[key]
                            else:
                                if match_dict[key][1] > match[key][1]:
                                    match[key] = match_dict[key]

                # if they match above the fuzzy threshold, replace it in the NP list
                if match:
                    return remove_match_scores(match)

            except Exception as e:
                logger.error("[ASRCorrection] error: {}".format(e), exc_info=True)

            # map each noun phrase to itself otherwise
            mapped_noun_phrases = {}
            
            for noun_phrase in noun_phrases:
                mapped_noun_phrases[noun_phrase] = noun_phrase

            return mapped_noun_phrases

def remove_match_scores(matches):
    for match in matches:
        matches[match] = matches[match][0]
    return matches

def get_phonetics(noun_phrases):
    # phonetic: noun_phrase
    phonetics_dict = {}
    try:
        for noun_phrase in noun_phrases:
            phonetics = dmetaphone(noun_phrase)
            # phonetics is too short
            if len(phonetics[0]) <= 1:
                continue
            pho_2 = ""
            if phonetics[1] != '' and phonetics[0].startswith("J") and phonetics[1].startswith("A") and (noun_phrase.startswith("jal") or noun_phrase.startswith("jai")):
                # example: dmetaphone("jalapeno") = ('JLPN', 'ALPN'), change it into ("HLPN", "ALPN")
                if len(phonetics[0]) > 1:
                    pho_2 = "H" + phonetics[0][1:]
            pho_0 = phonetics[0]
            pho_1 = phonetics[1]
            pho_list = [pho_0, pho_1, pho_2]
            for pho in pho_list:
                if pho != "":
                    if pho in phonetics_dict:
                        phonetics_dict[pho].append(noun_phrase)
                    else:
                        phonetics_dict[pho] = [noun_phrase]
    except Exception as e:
        logger.error("[ASRCorrection] get_phonetics error: {}".format(e), exc_info=True)

    return phonetics_dict

def match_phonetics(request_phonetics, response_phonetics, only_response=False):
    match = {}
    try:
        # for the phonetic of each word
        for phonetic in request_phonetics.keys():
            # usually there will be one word for the phonetic in question, or pick the first one if there are many
            if phonetic in response_phonetics:
                match[request_phonetics[phonetic][0]] = response_phonetics[phonetic]
            elif len(phonetic) > 1 and phonetic[:-1] in response_phonetics:
                # holidays in response vs holiday in category phonetics; to deal with plurals
                match[request_phonetics[phonetic][0]] = response_phonetics[phonetic[:-1]]
            elif (phonetic + "S") in response_phonetics:
                # if the S is dropped in ASR
                match[request_phonetics[phonetic][0]] = response_phonetics[phonetic + "S"]
            # similar phonetics are matched
            elif len(phonetic) > 1 and phonetic[1:] in response_phonetics:
                match[request_phonetics[phonetic][0]] = response_phonetics[phonetic[1:]]
            elif len(phonetic) > 2 and phonetic[2:] in response_phonetics:
                match[request_phonetics[phonetic][0]] = response_phonetics[phonetic[2:]]
            elif len(phonetic) > 2 and phonetic[:-2] in response_phonetics:
                match[request_phonetics[phonetic][0]] = response_phonetics[phonetic[:-2]]
            elif len(phonetic) > 2 and phonetic[1:-1] in response_phonetics:
                match[request_phonetics[phonetic][0]] = response_phonetics[phonetic[1:-1]]
            elif len(phonetic) > 3 and phonetic[2:-1] in response_phonetics:
                match[request_phonetics[phonetic][0]] = response_phonetics[phonetic[2:-1]]
            elif len(phonetic) > 3 and phonetic[1:-2] in response_phonetics:
                match[request_phonetics[phonetic][0]] = response_phonetics[phonetic[1:-2]]
        if only_response:
            for phonetic in response_phonetics.keys():
                if len(phonetic) > 1 and phonetic[:-1] in request_phonetics:
                        # ex. request: holiday vs response: holidays
                    match[request_phonetics[phonetic[:-1]][0]] = response_phonetics[phonetic]
                elif len(phonetic) > 1 and phonetic[1:] in request_phonetics:
                    match[request_phonetics[phonetic[1:]][0]] = response_phonetics[phonetic]
                elif len(phonetic) > 2 and phonetic[2:] in request_phonetics:
                    match[request_phonetics[phonetic[2:]][0]] = response_phonetics[phonetic]
                elif len(phonetic) > 2 and phonetic[:-2] in request_phonetics:
                    match[request_phonetics[phonetic[:-2]][0]] = response_phonetics[phonetic]
                elif len(phonetic) > 2 and phonetic[1:-1] in request_phonetics:
                    match[request_phonetics[phonetic[1:-1]][0]] = response_phonetics[phonetic]
                elif len(phonetic) > 3 and phonetic[2:-1] in request_phonetics:
                    match[request_phonetics[phonetic[2:-1]][0]] = response_phonetics[phonetic]
                elif len(phonetic) > 3 and phonetic[1:-2] in request_phonetics:
                    match[request_phonetics[phonetic[1:-2]][0]] = response_phonetics[phonetic]
        new_match = {}
        # match ex. {'crazy rich asian': ['crazy rich asians']}
        # make sure the matches are close enough to each other
        for asr_input, asr_gold in match.items():
            mod_asr_input = process_match(asr_input)
            mod_asr_gold = process_match(asr_gold[0])
            match_scores = match_score(mod_asr_input, mod_asr_gold)
            if match_scores > fuzzy_match_threshold:
                new_match[asr_input] = (asr_gold, match_scores)
        match = new_match

    except Exception as e:
        logger.error("[ASRCorrection] match_phonetics error: {}".format(e), exc_info=True)

    return match

def process_match(text_input):
    try:
        start_pattern = ["kn", "gn", "pn", "ae", "wr"]
        words = text_input.lower().split()
        modified_words = []

        c_to_s_list = ["i", "e", "y"]
        d_to_t_list = ["ge", "gy", "gi"]
        g_to_j_list = ["i", "e", "y"]
        for word in words:
            modified_word = word
            # if word begins with start_pattern, drop first letter
            for pattern in start_pattern:
                if word.startswith(pattern):
                    modified_word = word[1:]
                    break
            # drop 'B' if after 'M' at the end of the word
            if word.endswith("mb"):
                modified_word = modified_word[:-1]

            current_word = ""
            for c in range(len(modified_word)):
                if c < len(modified_word) - 1:
                    # 'C' transforms to 'X' if followed by 'IA' or 'H' (unless in latter case, it is part of '-SCH-', in which case it transforms to 'K'). 'C' transforms to 'S' if followed by 'I', 'E', or 'Y'. Otherwise, 'C' transforms to 'K'.
                    if modified_word[c] == "c":
                        if modified_word[c + 1] == "h" or (c < len(modified_word) - 2 and modified_word[c + 1:c + 2] == "ia"):
                            current_word += "x"
                        elif modified_word[c + 1] in c_to_s_list:
                            current_word += "s"
                        else:
                            current_word += "k"
                    # 'D' transforms to 'J' if followed by 'GE', 'GY', or 'GI'. Otherwise, 'D' transforms to 'T'.
                    elif modified_word[c] == "d":
                        if c < len(modified_word) - 2 and modified_word[c + 1:c + 2] in d_to_t_list:
                            current_word += "t"
                    # Drop 'G' if followed by 'H' and 'H' is not at the end or before a vowel. Drop 'G' if followed by 'N' or 'NED' and is at the end.
                    elif modified_word[c] == "g":
                        if (c < len(modified_word) - 2 and modified_word[c + 1] == "h") or (c == len(modified_word) - 2 and modified_word[len(modified_word) - 1] == "n") or (c == len(modified_word) - 4 and modified_word[-3:] == "ned"):
                            current_word += " "
                        # 'G' transforms to 'J' if before 'I', 'E', or 'Y', and it is not in 'GG'. Otherwise, 'G' transforms to 'K'.
                        elif modified_word[c + 1] in g_to_j_list and modified_word[c + 1] != "g":
                            current_word += "j"
                    else:
                        current_word += modified_word[c]
                else:
                    current_word += modified_word[c]
            modified_word = current_word
            # 'CK' transforms to 'K'.
            modified_word = re.sub("ck", "k", modified_word)
            # 'PH' transforms to 'F'.
            modified_word = re.sub("ph", "f", modified_word)
            # 'WH' transforms to 'W' if at the beginning. Drop 'W' if not followed by a vowel.
            modified_word = re.sub(r"^wh", "w", modified_word)
            modified_word = "".join(i for i in modified_word if len(i.strip()) > 0)

            modified_words.append(modified_word)

        return "".join(word for word in modified_words)
    except Exception:
        return match_input

def match_score(asr_input, asr_gold):
    match_score = 0
    try:
        asr_input = "".join(i for i in asr_input.split())
        asr_gold = "".join(i for i in asr_gold.split())
        if asr_input in asr_gold or asr_gold in asr_input:
            match_score = 1
        else:
            mod_asr_input = process_match(asr_input)
            mod_asr_gold = process_match(asr_gold)
            match_score = fuzz.partial_ratio(mod_asr_input, mod_asr_gold) / 100
    except Exception as e:
        logger.error("[ASRCorrection] match_score error: {}".format(e), exc_info=True)

    return match_score