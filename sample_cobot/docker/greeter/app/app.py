#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import json
import logging
import sys

from flask import Flask, request, Response
from flask_restful import reqparse, Api, Resource

from rasa_core.agent import Agent
from rasa_core.interpreter import RasaNLUInterpreter, NaturalLanguageInterpreter

# load your trained agent
#agent = Agent.load("models/dialogue", interpreter=RasaNLUInterpreter("models/nlu/current"))
class CustomInterpreter(NaturalLanguageInterpreter):

    def parse(self, text):
        print ("Parsing: "+text)
        obj=json.loads(text)
        intent=obj.get('intent',None)
        entities=[]
        if 'slots' in obj:
            slots=obj['slots']
            #print("Slots: ")
            #print(slots)
            for slot_type, slot_value in slots.items():
                name = slot_value.get('name', None)
                value = slot_value.get('value', None)
                if value:
                    start = text.find(value)
                    end = start+len(value)-1
                    entities.append({
                            "start": start,
                            "end": end,
                            "value": value,
                            "entity": name
                            })
        result = {
            'text': text,
            'intent': {
                'name': intent,
                'confidence': 1.0,
            },
            'intent_ranking': [{
                'name': intent,
                'confidence': 1.0,
            }],
            'entities': entities,
        }
        print(result)
        return result

agent = Agent.load("models/dialogue", interpreter=CustomInterpreter())
app = Flask("domain response generator")
api = Api(app)

app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.DEBUG)


class Default(Resource):
    def get(self):
        return Response('Welcome', mimetype='text/plain')

class Rasa(Resource):

    def get(self):
        return 200

    def post(self):
        t0 = time.time()
        #args = parser.parse_args()
        args = request.get_json(force=True)
        print(args)
        validation = self.__validate_input(args)
        if validation:
            return validation, 500

        ret = {}

        if args.get('intent'):
            inputs = {}
            if args.get('text'):
                inputs['text'] = args.get('text')
            else:
                inputs['text'] = ''
            if args.get('intent'):
                inputs['intent'] = args.get('intent')
            if args.get('slots'):
                inputs['slots'] = args.get('slots')
            input_str = ""
            try:
                input_str = json.dumps(inputs)
                app.logger.info("inputs received: "+input_str)
            except:
                pass
            ret.update(
                self.__get_response(input_str)
            )

        ret['performance'] = time.time() - t0,
        ret['error'] = False

        return ret, 200

    @staticmethod
    def __validate_input(args):
        message = ""
        #if not args.get('text'):
        #    message = "No input text received."
        if message:
            return {
                'message': message,
                'error': True
            }
        return None

    @staticmethod
    def __get_response(text):

        # Convert to unicode
        #text = unicode(text, "utf-8")
        response = agent.handle_message(text)[0]

        ret = {
            'response': response
        }
        app.logger.info("result: %s", ret)
        return ret

api.add_resource(Default, '/')
api.add_resource(Rasa, '/greeter')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=os.environ.get('GREET_PORT') or 5001)
