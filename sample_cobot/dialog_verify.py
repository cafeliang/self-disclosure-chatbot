from sagemaker.mxnet import MXNetPredictor
from cobot_common.vectorize import Vectorizer
import numpy

def verify_training_set():
    predictor = MXNetPredictor('sagemaker-dialog-model-test')

    data = numpy.load(open('train.npz', 'rb'))
    inputs = data['inputs']
    outputs = data['outputs']

    for row in zip(inputs,outputs):
        print(row[0])
        print(row[1])
        print(predictor.predict([row[0].tolist()]))


def infer(intent_name):
    vectorizer = Vectorizer(['EVI','GREETER'],model_file="cobot-skill/models/en-US.json")
    (inputs,outputs) = vectorizer.vectorize_turns([[intent_name,'']])
    print(inputs)
    print(outputs)
    predictor = MXNetPredictor('sagemaker-dialog-model-test')

    input = inputs[0]
    result = int(predictor.predict([input]))
    print(result)
    print(vectorizer.invert_output(numpy.array([result])))

if __name__=='__main__':
    infer('QAIntent')
    infer('greet')
