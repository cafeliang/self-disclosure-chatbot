from datetime import datetime

import cobot_core as Cobot
from cobot_core.service_module import LocalServiceModule, RemoteServiceModule
# from sample_cobot.sample_events import new_session_event,existing_session_event, retrieval_event

class PersonalizedBotRG(LocalServiceModule):
    def execute(self):
        print(self.input_data)

        last_conversation_date = self.input_data.get('last_conversation_date', None)
        favorite_topic = self.input_data.get('favorite_topic', None)
        today_object = datetime.today().date()
        date_format = '%Y-%m-%d'

        if last_conversation_date is None:
            response = "Nice to meet you. I am Alexa, your voice agent. "
        else:

            datetime_object = datetime.strptime(last_conversation_date, date_format).date()

            past_days = (today_object - datetime_object).days

            if past_days < 7:
                if favorite_topic:
                    response = "Hi, welcome back. I remember you enjoy " + favorite_topic + ". Shall we talk more about that topic?"
                else:
                    response = "Hi, welcome back. I remember we chat recently. How is everything going? "
            else:
                response = "Long time no see. How is everything going? "

        response_dict = {
            'response': response,
            'user_attributes': {
                'last_conversation_date': datetime.strftime(today_object, date_format)
            },
            'context_manager': {
                'intent': 'newIntent'
            }
        }
        return response_dict



def overrides(binder):
    pass


def lambda_handler(event, context):
    # app_id: replace with your ASK skill id to validate ask request. None means skipping ASK request validation.
    # user_table_name: replace with a DynamoDB table name to store user preference data. We will auto create the
    #                  DynamoDB table if the table name doesn�t exist.
    #                  None means user preference data won�t be persisted in DynamoDB.
    # save_before_response: If it is true, skill persists user preference data at the end of each turn.
    #                       Otherwise, only at the last turn of whole session.
    # state_table_name: replace with a DynamoDB table name to store session state data. We will auto create the
    #                   DynamoDB table if the table name doesn�t exist.
    #                   None means session state data won�t be persisted in DynamoDB.
    # overrides: provide custom override for dialog manager components.
    # api_key: replace with your api key
    cobot = Cobot.handler(event,
                          context,
                          app_id=None,
                          user_table_name='UserTable',
                          save_before_response=True,
                          state_table_name='StateTable',
                          overrides=overrides,
                          api_key=None)

    cobot.create_nlp_pipeline()
    PersonalizedBot = {
        'name': "PERSONALIZED",
        'class': PersonalizedBotRG,
        'url': 'local',
        'context_manager_keys': ['text'],
        'input_user_attributes_keys': ['last_conversation_date', 'favorite_topic']
    }


    cobot.add_response_generators([PersonalizedBot])
    return cobot.execute()

# if __name__ == '__main__':
#     lambda_handler(event=new_session_event, context={})
