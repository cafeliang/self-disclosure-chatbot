import cobot_core as Cobot
import json
import requests
from injector import inject
from cobot_core import StateManager
from cobot_core.log.logger import LoggerFactory
from cobot_core.service_url_loader import ServiceURLLoader


class CustomRemoteRankingStrategy(Cobot.RankingStrategy):
    @inject
    def __init__(self, state_manager: StateManager):
        self.state_manager = state_manager
        self.url = ServiceURLLoader.get_url_for_module("RANKER")
        self.timeout_in_millis = 1000  # default timeout
        self.logger = LoggerFactory.setup(self)

    def rank(self, responses):
        # Get current state's attribute: i.e.
        intent = self.state_manager.current_state.intent
        # Get session history's attributes: i.e.
        past_topics = [turn.get('topic', None) for turn in self.state_manager.session_history]
        # Get last state's attributes: i.e.
        last_response = self.state_manager.last_state.get('response',
                                                          None) if self.state_manager.last_state is not None else None

        # Get user attributes: i.e.
        user_topic_preference = self.state_manager.user_attributes.favorite_topic

        # package all the selected state information to input_data
        self.input_data = {
            'intent': intent,
            'past_topics': past_topics,
            'last_response': last_response,
            'user_topic_preference': user_topic_preference
        }
        response = requests.post(self.url,
                                 data=json.dumps(self.input_data),
                                 headers={'content-type': 'application/json'},
                                 timeout=self.timeout_in_millis / 1000.0)

        result = response.json()

        # Update modified attributes in state manager
        modified_user_attributes = result.get('user_attributes', None)
        if modified_user_attributes:
            for k, v in modified_user_attributes.items():
                setattr(self.state_manager.user_attributes, k, v)

        modified_state_info = result.get('context_manager', None)
        if modified_state_info:
            for k, v in modified_state_info.items():
                setattr(self.state_manager.current_state, k, v)

        return result.get('response', None)